/*********************************************************************************
* {Controller} Class: {ctrlProfileUserLookupPage}
*  DateCreated : 01/22/2014
----------------------------------------------------------------------------------
* Purpose/Methods:
* - Used for open country lookup in Team Structure Page.
* 
* Unit Test: TestCtrlLookupPage
----------------------------------------------------------------------------------
* History:
* - VERSION  DEVELOPER NAME    DATE            DETAIL FEATURES
     1.0                      01/22/2014      INITIAL DEVELOPMENT
*********************************************************************************/

public with sharing class ctrlProfileUserLookupPage{

    transient public List<CustomLookup> lstCustomlookup{get;set;}    
    public string searchString{get;set;} // search keyword
    Public string strProfileName;
    Public string strCName {get; set;}
    Public string strCId {get; set;}
    //Public string strCountryIds;
    //Public Set<Id> setCoutnryID {get; set;}
    transient public List<sobject> results; // search results
    

    /********************************************************************
        Constructor
    ********************************************************************/
    public ctrlProfileUserLookupPage() {
        lstCustomlookup = new List<Customlookup>();
        strCId = System.currentPageReference().getParameters().get('CId');
        strCName = System.currentPageReference().getParameters().get('Cname');
        strProfileName = System.currentPageReference().getParameters().get('ProfileName');
        if(strProfileName == 'PSM'){
            strProfileName += ' Officer';
        }
        if(strProfileName == 'Finance'){
            strProfileName += ' Officer';
        }
        if(strProfileName == 'M&E'){
            strProfileName += ' Officer';
        }
        /*strCountryIds  = System.currentPageReference().getParameters().get('CountryID');
        setCoutnryID = new Set<Id>();
        String[] ConIDs = strCountryIDs.Split(',');
        for(String objStr : ConIDs){
            setCoutnryID.add(objStr);
        }
        system.debug('@@@setCoutnryID: '+setCoutnryID);*/
        //searchString = System.currentPageReference().getParameters().get('lksrch');    
        runSearch();  
    }
    /********************************************************************
        Name : search
        Purpose : Performs the keyword search
    ********************************************************************/ 
  
    public PageReference search() {
        lstCustomlookup = new List<Customlookup>();
        runSearch();
        return null;
    }
    
    /********************************************************************
        Name : runSearch
        Purpose : Prepare the query and issue the search command
    ********************************************************************/
    
    private void runSearch() {
        results = performSearch(searchString);     
    } 
    
    /********************************************************************
        Name : performSearch
        Purpose : Run the search and return the records found
    ********************************************************************/  
  
    private List<sobject> performSearch(string searchString) {
        
        String soql = '';
        String profileNameAdmin = 'Applicant/CM Admin';
        String profileNameRW = 'Applicant/CM Read Write';
        String profileNameRO = 'Applicant/CM Read-Only';
        String profilePortal = 'LFA Portal User';
        
        if(strProfileName == 'Country Team'){
            soql = 'Select Id,Name From User WHERE IsActive = true And Profile.Name !=: profileNameAdmin and Profile.Name !=: profileNameRW and Profile.Name !=: profileNameRO and Profile.Name !=: profilePortal Order By Name';  
        
            if(searchString != null && searchString != ''){
                searchString += '%';
                soql = 'Select Id,Name From User WHERE Name Like :searchString and IsActive = true And Profile.Name !=: profileNameAdmin and Profile.Name !=: profileNameRW and Profile.Name !=: profileNameRO and Profile.Name !=: profilePortal Order By Name';
            }
        }else{
            soql = 'Select Id,Name From User WHERE  Profile.Name =: strProfileName and IsActive = true Order By Name';  
        
            if(searchString != null && searchString != ''){
                searchString += '%';
                soql = 'Select Id,Name From User WHERE  Profile.Name =: strProfileName and Name Like :searchString and IsActive = true Order By Name';
            }
        }  
             
        results = database.query(soql); 
        
        CustomLookup objlookup = new CustomLookup();
        for(sobject s:results){
            objlookup = new CustomLookup();
            objlookup.Id = s.Id;
            objlookup.strUserName =  string.valueOf(s.get('Name'));
            lstCustomlookup.add(objlookup);        
        }   
        return null; 
    } 

    /********************************************************************
        Name : getFormTag
        Purpose : Used by the visualforce page to send the link to the right dom element
    ********************************************************************/  
  
    public string getFormTag() {
        return System.currentPageReference().getParameters().get('frm');
    }
    
    /********************************************************************
        Name : getTextBox
        Purpose : Used by the visualforce page to send the link to the right dom element for the text box
    ********************************************************************/  
  
    public string getTextBox() {
        return System.currentPageReference().getParameters().get('txt');
    }
    
    /********************************************************************
        Wrapper Class : Customlookup
    ********************************************************************/
    
    public class Customlookup{
        Public Id Id{get;set;}
        Public String strUserName{get;set;}
    }
}