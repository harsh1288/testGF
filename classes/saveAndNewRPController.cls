/*
* $Author:      $ TCS - Rahul Kumar
* $Description: $ Class for controlling new Reporting Period create event redirection
* $Date:        $ 21-Nov-2014 
* $Revision:    $ 
*/
public class saveAndNewRPController{

    private Period__c rp;
    private Map<String, String> keyPrefixMap;
    public String backUrl{get;set;}
    public saveAndNewRPController(ApexPages.StandardController stdController){
        this.rp = (Period__c)stdController.getRecord();
        backUrl = '';         
    }
    
     public PageReference createNewRPUrl()
     {
            boolean statusPF = saveandNewHandler.pfTrue('Period__c');
            keyPrefixMap =  saveandNewHandler.schemaMap();
            String baseUrl;
            string userTypeStr = UserInfo.getUserType();
            baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
            if(userTypeStr == 'Standard') 
            {
                baseUrl = baseUrl+'/';
            }
            else
            { 
                baseUrl = baseUrl+'/GM/';
            }
           if(rp.Performance_Framework__c != null)
           {
             // Creating url according to user type when Save and New is clicked while editing a record
                List<Performance_Framework__c> s = new List<Performance_Framework__c>([Select Name from Performance_Framework__c where Id  =:rp.Performance_Framework__c]);
                baseUrl += String.valueOf(keyPrefixMap.get('Implementation_Period__c'));
                baseUrl += '/e?nooverride=1';
                if(s.size()>0)
                {
                    baseUrl += '&';
                    baseUrl += Label.Rep_Period_PF_Lkup;
                    baseUrl += '=';
                    baseUrl += s[0].Name;            
                    baseUrl += '&retURL=';
                    baseUrl += rp.Performance_Framework__c;
                }
                PageReference saveNewUrl = new PageReference(baseUrl);
                saveNewUrl.setRedirect(true);                
                return saveNewUrl;            
           }
           else
           {
            // Creating url according to user type when Save and New is clicked while creating new record
               if(userTypeStr == 'Standard') 
                {                    
                }
                else
                { 
                    if(ApexPages.currentPage().getParameters().get('retUrl').indexOf('GM') > -1)
                    {
                    
                    }
                    else
                    {
                        backUrl += +'/GM';
                    }
                }
               backUrl += ApexPages.currentPage().getParameters().get('retUrl');
/*               if(backUrl.contains('?'))
               {
                   backUrl += '&nooverride=1';
               }
               else
               {
                   backUrl += '?nooverride=1';
               }
*/               ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Record has been saved <br/> Can\'t use Save and New feature, Please <a id="goback" href="'+backUrl+'"><b>Go back</b></a>'));                           
               return null;
           }         
     }
}