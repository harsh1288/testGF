@isTest
 public with sharing class disaggregationTableController_test{
    
    Public static testMethod void Testdisaggregation(){
    
        List<Profile_Access_Setting__c> customSettingRecList = new List<Profile_Access_Setting__c>();
        Guidance__c gui = new Guidance__c(name='Guidance for Disaggregation');
        insert gui;
        Profile_Access_Setting__c customSettingRec = new Profile_Access_Setting__c();
        customSettingRec.Name = 'dissagg01';
        customSettingRec.Profile_Name__c = 'System Administrator';
        customSettingRec.Salesforce_Item__c = 'Edit Pr Comment';
        customSettingRec.Page_Name__c = 'grantDisaggregatedController';
        customSettingRecList.add(customSettingRec);
        
        Profile_Access_Setting__c customSettingRec1 = new Profile_Access_Setting__c();
        customSettingRec1.Name = 'dissagg02';
        customSettingRec1.Profile_Name__c = 'System Administrator';
        customSettingRec1.Salesforce_Item__c = 'Edit Disaggregation';
        customSettingRec1.Page_Name__c = 'grantDisaggregatedController';
        customSettingRecList.add(customSettingRec1);
        
        Profile_Access_Setting__c customSettingRec2 = new Profile_Access_Setting__c();
        customSettingRec2 .Name = 'dissagg03';
        customSettingRec2 .Profile_Name__c = 'System Administrator';
        customSettingRec2 .Salesforce_Item__c = 'Edit Global Fund Comment';
        customSettingRec2 .Page_Name__c = 'grantDisaggregatedController';
        customSettingRecList.add(customSettingRec2);
        
        Profile_Access_Setting__c customSettingRec3 = new Profile_Access_Setting__c();
        customSettingRec3 .Name = 'dissagg04';
        customSettingRec3.Profile_Name__c = 'System Administrator';
        customSettingRec3.Salesforce_Item__c = 'Read Global Fund Comment';
        customSettingRec3.Page_Name__c = 'grantDisaggregatedController';
        customSettingRecList.add(customSettingRec3);
        
        Profile_Access_Setting__c customSettingRec4 = new Profile_Access_Setting__c();
        customSettingRec4  .Name = 'dissagg05';
        customSettingRec4 .Profile_Name__c = 'System Administrator';
        customSettingRec4 .Salesforce_Item__c = 'Edit record';
        customSettingRec4 .Page_Name__c = 'grantDisaggregatedController';
        customSettingRecList.add(customSettingRec4);
        
        insert customSettingRecList;
        
        Grant_Disaggregated__c gd = TestClassHelper.insertGrantDisaggregated();
        
          
         Test.StartTest();  
         disaggregationTableController dt = new disaggregationTableController();
         dt.recordId = gd.Grant_Indicator__c;
         dt.dataType = Label.NaP_DT;
         dt.getaggreegationWrapperList();
         dt.EditDisaggregated();
         
         dt.ShowHistoryPopup();
         dt.HidePopupHistory();
         dt.checkProfile() ;
         dt.mode = 'edit';
         ApexPages.currentPage().getParameters().put('par5',gd.Grant_Indicator__c);
         ApexPages.currentPage().getParameters().put('par6','Coverage/Output');
         dt.checkForMode();
         dt.ReturnToInd();
         dt.CancelIndicator();
         dt.SaveAction();
        dt.grantdis = gd;
         dt.saveGDComments();
         Test.StopTest();
         
   }
}