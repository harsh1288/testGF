Public Class GM_DocumentUpload{
    Public List<Page__c> lstPages {get;set;}
    Public String strPageId {get;set;}
    Public List<Module__c> lstmodules {get;set;}
    Public String strImplementationPeriodId {get;set;}
    Public FeedItem objFeedItem {get;set;}
    Public String FeedBody {get;set;}
    Public boolean blnExpandSection {get;set;}
    Public List<WrapperDocumentUpdload> lstWrpDocumentUpload {get; set;}
    Public DocumentUpload__c objDocumentUpload {get; set;}
    Public List<DocumentUpload__c> lstDocumentUpload {get; set;}
    Public Set<Id> setFeedItemId {get; set;}
    Public Map<Id,FeedItem> mapIdFeedItem {get; set;}
    Public String SelectedTemplate {get;set;}
    Public String strUserLanguage {get;set;}
    Public boolean fileoverload {get;set;}
    public Integer fileSize {get;set;} 
    Public String strGuidanceId {get;set;}
    public String strLanguage {get;set;} 
    public String strWarning {get;set;}
    
    public GM_DocumentUpload(ApexPages.StandardController controller) {
        strPageId = Apexpages.currentpage().getparameters().get('id');
        strUserLanguage = System.UserInfo.getLanguage();
        system.debug('StrUserLang ' +strUserLanguage);
        if(strUserLanguage == '')
        strUserLanguage = 'en_US';
        fileoverload = false;
        fileSize = 0;
        if(String.isBlank(strPageId) == false){
            List<Page__c> lstPageMain = [Select Implementation_Period__c From Page__c Where Id =: strPageId and Implementation_Period__c != null];
            if(lstPageMain.size() > 0){
                strImplementationPeriodId = lstPageMain[0].Implementation_Period__c;
            }
            if(String.isBlank(strImplementationPeriodId) == false){
                lstWrpDocumentUpload = new List<WrapperDocumentUpdload>();
                lstPages = new List<Page__c>();   
                lstPages = [Select Id,Name,URL_Prefix__c,Order__c,Modular__c From Page__c Where Implementation_Period__c =: strImplementationPeriodId Order by Order__c];
                lstModules = new List<Module__c>();   
                lstModules = [Select Id,Name,Implementation_Period__c From Module__c Where Implementation_Period__c =: strImplementationPeriodID Order by Name];  
                objFeedItem = new FeedItem(ParentId = strImplementationPeriodId);
                
                objDocumentUpload = new DocumentUpload__c(Implementation_Period__c = strImplementationPeriodId,Process_Area__c = 'Grant-making');
                lstDocumentUpload = new List<DocumentUpload__c>();
                lstDocumentUpload = [Select Id,Description__c,Implementation_Period__c,FeedItem_Id__c,Language__c,Language_Code__c,Process_Area__c,GMType__c,Type__c,Name from DocumentUpload__c where Implementation_Period__c =: strImplementationPeriodId];
                setFeedItemId = new Set<Id>();
                System.debug('!!!!lstDocumentUpload ='+lstDocumentUpload +strImplementationPeriodId);
                if(lstDocumentUpload.size() > 0) {
                    for(DocumentUpload__c objDocument : lstDocumentUpload) {
                        setFeedItemId.add(objDocument.FeedItem_Id__c);
                    }
                    mapIdFeedItem = new Map<Id,FeedItem> ([Select Id,ContentDescription,ContentFileName,body,RelatedRecordId,CreatedDate,CreatedBy.Name From FeedItem Where Id In: setFeedItemId]);
                    for(DocumentUpload__c objDocument : lstDocumentUpload) {
                        WrapperDocumentUpdload objWrpDocUpload = new WrapperDocumentUpdload();   
                        objWrpDocUpload.objDocumentUpload = objDocument;
                        objWrpDocUpload.objFeedItem  = mapIdFeedItem.get(objDocument.FeedItem_Id__c);
                        lstWrpDocumentUpload.add(objWrpDocUpload);
                    }
                }
                
                
            }
            FeedBody = null;
        }
        strLanguage = 'ENGLISH';
        if(System.UserInfo.getLanguage() == 'fr'){
            strLanguage = 'FRENCH'; }
        if(System.UserInfo.getLanguage() == 'ru'){
            strLanguage = 'RUSSIAN'; }
        if(System.UserInfo.getLanguage() == 'es'){
            strLanguage = 'SPANISH'; }
            
        List<Guidance__c> lstGuidance = [Select Id from Guidance__c where Name = :label.GM_Manage_Documents];
            if(!lstGuidance.isEmpty()) 
            {
              strGuidanceId = lstGuidance[0].Id;
            }
    }
    Public Integer DeleteIndex {get;set;}
    Public Integer CountUpload {get;set;}
    
    Public void DeleteFile(){        
        if(DeleteIndex != null){
            WrapperDocumentUpdload objWrpDocUpload = lstWrpDocumentUpload[DeleteIndex];
            if(objWrpDocUpload != null){
                if (objWrpDocUpload.objDocumentUpload != null) {
                    List<DocumentUpload__c> lstDocumentUploadToDelete = [Select Id From DocumentUpload__c Where Id =: objWrpDocUpload.objDocumentUpload.Id];
                    if(lstDocumentUploadToDelete != null && lstDocumentUploadToDelete.size() > 0) {
                        delete lstDocumentUploadToDelete;
                    }
                }
                
                if(objWrpDocUpload.objFeedItem != null) {
                    List<FeedItem> lstFeedItemToDelete = [Select Id From FeedItem Where Id =: objWrpDocUpload.objFeedItem.Id];
                    if(lstFeedItemToDelete.size() > 0 && lstFeedItemToDelete != null){
                        Delete lstFeedItemToDelete;
                    }
                }
                
                lstWrpDocumentUpload.remove(DeleteIndex);
            }
        }
    } 
    //upload a feed item and save tho the documentUpload object.
    Public PageReference uploadFile(){
        System.debug('In Upload  objDocumentUpload ' +objDocumentUpload +'Feed = '+ objFeedItem  );
        
        if(objFeedItem != null){
            if(CountUpload != null){
                if(CountUpload == 1) objFeedItem.ContentDescription = 'Detailed Budget XLS';
                if(CountUpload == 2) objFeedItem.ContentDescription = 'PSM Plan XLS';
                if(CountUpload == 3) objFeedItem.ContentDescription = 'Other Document';  
            }
            system.debug('@#@#@#@#'+objFeedItem);
            system.debug('objFeedItem.ContentSize:' +objFeedItem.ContentSize);            
            if(objFeedItem.ContentSize == Null){            					
        		strWarning = 'This file exceeds the maximum size limit of 10MB.';
        		return null; //pgrefnew;  
            }
            	
            //system.debug();
            //objFeedItem.body = FeedBody;
            try {
                insert objFeedItem;
                if(objDocumentUpload != null) {
                    objDocumentUpload.FeedItem_Id__c = objFeedItem.ID;
                    insert objDocumentUpload;            		       
                    //send email
                    /*List<Contact> lstContact = [Select Id from Contact where Id = '003g000000Bheha' ];
                    System.debug('!!!!!!!Contact = lstContact ' +lstContact );
                    if(lstContact.size() > 0) { 
                        if(objFeedItem.ContentDescription != 'Other Document'){
                            List<EmailTemplate> lstEmailTemplate = [Select ID,Subject,Body From EmailTemplate 
                                                            Where DeveloperName = 'FeedItem_alertvf'];
                            if(lstEmailTemplate.size() > 0){
                                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                                //set parameters of the email                
                                mail.setTemplateId(lstEmailTemplate[0].Id);
                                mail.setTargetObjectId(lstContact[0].id); 
                                 
                                //send the email;
                                if(test.isrunningtest() == false)
                                    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                            }
                        }
                    }*/
                    
                    objDocumentUpload = new DocumentUpload__c(Implementation_Period__c = strImplementationPeriodId,Process_Area__c = 'Grant-making');
                    objFeedItem = new FeedItem(ParentId = strImplementationPeriodId);
                }
                FeedBody = null;
            }
            catch (Exception ex) {
                System.debug(ex); 
            }
           
           
        }
        PageReference pgrefnew = new PageReference('/apex/GM_DocumentUpload?id='+strPageId);
        pgrefnew.setRedirect(true);
        return pgrefnew;
    }
    //Wrapper class for insert update both the object and display combined List.
    Public class WrapperDocumentUpdload { 
        Public DocumentUpload__c objDocumentUpload {get; set;} 
        Public FeedItem objFeedItem {get; set;} 
        public WrapperDocumentUpdload() {
            objDocumentUpload = new DocumentUpload__c();
            objFeedItem = new FeedItem();
        }
    }
    
    Public PageReference downloadTemplate(){
    	system.debug('***SelectedTemplate:'+selectedtemplate);
    	system.debug('StrUserLang ' +strUserLanguage);
    	if(SelectedTemplate == Null)
    	SelectedTemplate = Label.Grant_Making_narrative_Template_english;
    	return new PageReference(selectedtemplate);
    }        
    
}