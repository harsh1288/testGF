@isTest
Public class TestctrlCountryLookupPage{
    Public static testMethod Void TestctrlCountryLookupPage(){
    
        Account objAcc = new Account();
        objAcc.Name = 'Test Acc';
        insert objAcc;
        
        Country__c objCountry = new Country__c();
        objCountry.Name = 'Test Country';
        objCountry.Region_Name__c ='Test Region';
        insert objCountry;
    
        Contact objCon = new Contact();
        objCon.AccountId = objAcc.Id;
        objCon.FirstName = 'Test FirstName';
        objCon.LastName = 'Test LastName';        
        objCon.Active_LFA_Member__c = 'yes';
        objCon.Level_of_Experience__c = 'test';
        insert objCon;
        
        System.currentPageReference().getparameters().put('lktp','Contact');
        System.currentPageReference().getParameters().Put('idfield',objAcc.Id);
        System.currentPageReference().getParameters().Put('Country',objCountry.Id);  
        System.currentPageReference().getParameters().put('CountryID',objCountry.Id);
        CtrlCountryLookupPage objCountryLookupPage = new CtrlCountryLookupPage();
        
        objCountryLookupPage.searchString = 'test';
        objCountryLookupPage.getFormTag();
        objCountryLookupPage.getTextBox();
        objCountryLookupPage.search();
    
    }
}