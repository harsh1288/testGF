/*
* $Author:      $ TCS Developer
* $Description: $ handler for All Sobjects related to Performance Framework, to update one field on PF.
* $Date:        $ 07-Nov-2014
* $Revision:    $ 25-Mar-2015
*/

public class PFChangesTrackerHandler
{
   
   public static void CheckPFField(List<Sobject> lstSobject){
    
    Set<string> setImpID = new Set<string>();
    List<Performance_Framework__c> lstToUpdatePF = new List<Performance_Framework__c> ();
    for(sobject objGobj: lstSobject){
        setImpID.add(string.valueOf(objGobj.get('Performance_Framework__c')));
    }
       
    List<Performance_Framework__c> lstPF = [Select id,Is_Changed_Submitted__c,PF_Status__c FROM Performance_Framework__c where id IN :setImpID];
       
       for(Performance_Framework__c objPF : lstPF){
           System.debug('====>'+Label.PF_Status_Submitted_to_MEPH+'====ffff'+objPF.PF_Status__c);
           if(objPF.PF_Status__c  == Label.PF_Status_Submitted_to_MEPH){
               if(objPF.Is_Changed_Submitted__c !=true){
                   objPF.Is_Changed_Submitted__c = true;
                   lstToUpdatePF.add(objPF);
               }
           }
           
           if(objPF.PF_Status__c  == Label.PF_Status_Rejected || objPF.PF_Status__c  == Label.PF_Status_Submitted_to_LFA){
               if(objPF.Is_Changed_Submitted__c ==true){
                   objPF.Is_Changed_Submitted__c = false;
                   lstToUpdatePF.add(objPF);
               }
           }           
           
       }
       if(lstToUpdatePF.size()>0){
           update lstToUpdatePF;
       }
   }
}