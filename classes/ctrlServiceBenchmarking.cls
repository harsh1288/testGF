/*/*********************************************************************************
* {Controller} Class: {ctrlServiceBenchmarking}
*  DateCreated : 07/12/2013
----------------------------------------------------------------------------------
* Purpose/Methods:
* - Used for see the all service type benchmarking details.
* 
* Unit Test: TestctrlServiceBenchmarking
----------------------------------------------------------------------------------
* History:
* - VERSION  DEVELOPER NAME    DATE            DETAIL FEATURES
     1.0                      07/12/2013      INITIAL DEVELOPMENT
*********************************************************************************/

Public class ctrlServiceBenchmarking{

    Public String strServiceId {get; set;}
    Public List<wrapSearchService> lstServiceForSearch {get; set;}
    Public String strWorkPlanId {get; set;}
    Final String STARTYEAR = '2013';
    Public List<SelectOption> CountryOptions {get; private set;}
    //Public List<SelectOption> ServiceOptions {get; private set;}
    Public List<SelectOption> ServiceSubTypeOptions {get; private set;}
    Public List<SelectOption> RegionOptions {get; private set;}
    Public List<SelectOption> YearOptions {get; private set;}
    Public List<SelectOption> FinancialRiskOptions {get; private set;}
    Public List<SelectOption> HealthRiskOptions {get; private set;}
    Public List<SelectOption> ProgramaticRiskOptions {get; private set;}
    Public List<SelectOption> LFAOptions {get; private set;}
    Public String SelectedCountry {get; set;}
    //Public String SelectedService {get; set;}
    Public String SelectedServiceSubType {get; set;}
    Public String SelectedRegion {get; set;}
    Public String SelectedYear {get; set;}
    Public String SelectedFinancialRisk {get; set;}
    Public String SelectedHealthRisk {get; set;}
    Public String SelectedProgramaticRisk {get; set;}
    Public String FinancialRisk {get; set;}
    Public String HealthRisk {get; set;}
    Public String ProgramaticRisk {get; set;}
    Public String SelectedLFA {get; set;}
    
    Public Decimal MinLOE {get; set;}
    Public Decimal MinCost {get; set;}
    Public Decimal MaxLOE {get; set;}
    Public Decimal MaxCost {get; set;}
    Public Decimal AvgLOE {get; set;}
    Public Decimal AvgCost {get; set;}
    Public Decimal MedianLOE {get; set;}
    Public Decimal MedianCost {get; set;}
    
    /********************************************************************
        Constructor
    ********************************************************************/
    
    Public ctrlServiceBenchmarking(ApexPages.StandardController controller) {
        strServiceId = Apexpages.currentpage().getparameters().get('id');
        lstServiceForSearch = new List<wrapSearchService>();
        FillAllRisksOptions();
        //FillService();
        FillServiceType();
        FillCountry();
        FillRegion();
        FillYear();
        FillLFA();
        if(String.IsBlank(strServiceId) == false){
            
            List<LFA_Service__c> lstService = [Select Id,Name,Grant__c, Grant__r.Name, LFA_Work_Plan__r.Country__c, 
                                                LFA_Work_Plan__r.Country__r.Region_name__c, LFA_Work_Plan__r.Country__r.Name,
                                                Grant__r.Financial_and_Fiduciary_Risks__c,Service_Sub_Type__c,
                                                Grant__r.Health_Services_and_Products_Risks__c,
                                                Grant__r.Programmatic_and_Performance_Risks__c,
                                                LFA_Work_Plan__c,LFA_Work_Plan__r.LFA__c,LFA_Work_Plan__r.Include_in_Library__c,
                                                LFA_Work_Plan__r.Year__c From LFA_Service__c 
                                                Where Id=:strServiceId Limit 1];
            strWorkPlanId = lstService[0].LFA_Work_Plan__c;
            SelectedServiceSubType = lstService[0].Service_Sub_Type__c;
            SelectedLFA = lstService[0].LFA_Work_Plan__r.LFA__c;
            SelectedCountry = lstService[0].LFA_Work_Plan__r.Country__c;
            SelectedRegion = lstService[0].LFA_Work_Plan__r.Country__r.Region_name__c;
            SelectedYear = lstService[0].LFA_Work_Plan__r.Year__c;

            if(lstService[0].Grant__c != null){
                SelectedFinancialRisk = lstService[0].Grant__r.Financial_and_Fiduciary_Risks__c;
                SelectedHealthRisk = lstService[0].Grant__r.Health_Services_and_Products_Risks__c;
                SelectedProgramaticRisk = lstService[0].Grant__r.Programmatic_and_Performance_Risks__c;
            }
            AnalyseSearch();
           
        }
    }
    
    /********************************************************************
        Name : FillCountry
        Purpose : Used for fill Country name from Country object.
    ********************************************************************/
    Public void FillCountry(){
        CountryOptions = new List<SelectOption>();
        List<Country__c> lstCountry = [Select Id,Name,Used_for_LFA_Services__c From Country__c Where Used_for_LFA_Services__c=TRUE Order By Name ASC];
        if(lstCountry != null && lstCountry.size() > 0){
            CountryOptions.add(new SelectOption('','All Countries'));
            for(Country__c objContry : lstCountry){
                CountryOptions.add(new SelectOption(objContry.Id,objContry.Name));
            }
        } 
    }
    /*Public void FillService(){
        ServiceOptions = new List<SelectOption>();
        List<LFA_Service__c> lstService = [Select Id,Name From LFA_Service__c];
        if(lstService != null && lstService.size() > 0){
            ServiceOptions.add(new SelectOption('','All Services'));
            Set<String> setServiceName = new Set<String>();
            for(LFA_Service__c objService : lstService){
                if(setServiceName.contains(objService.Name) == false){
                    setServiceName.add(objService.Name);
                    ServiceOptions.add(new SelectOption(objService.Name,objService.Name));
                }
            }
        } 
    }*/
    
    /********************************************************************
        Name : FillServiceType
        Purpose : Used for fill service type.
    ********************************************************************/
    Public void FillServiceType(){
        Set<String> setSubTypeInPickList = new Set<String>();
        ServiceSubTypeOptions = new List<SelectOption>();
        Schema.sObjectType objType = LFA_Service__c.getSObjectType();
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe(); 
        map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap(); 
        List<Schema.PicklistEntry> ServiceSubTypes = fieldMap.get('Service_Sub_Type__c').getDescribe().getPickListValues();
        ServiceSubTypeOptions.add(new SelectOption('','Please select a Service Type'));
        for (Schema.PicklistEntry Entry : ServiceSubTypes){ 
            ServiceSubTypeOptions.add(new SelectOption(Entry.getLabel(), Entry.getValue()));
            setSubTypeInPickList.add(Entry.getValue());
        }
        List<AggregateResult> lstARSubTypeNotInPickList = [Select Service_Sub_Type__c SubType FROM LFA_Service__c 
                                                        Where Service_Sub_Type__c NOT IN :setSubTypeInPickList 
                                                        Group By Service_Sub_Type__c];
        for(AggregateResult objARSubTypeNotInPickList :lstARSubTypeNotInPickList){
            if(objARSubTypeNotInPickList.get('SubType') != null && objARSubTypeNotInPickList.get('SubType') != ''){
                ServiceSubTypeOptions.add(new SelectOption(String.valueOf(objARSubTypeNotInPickList.get('SubType')), String.valueOf(objARSubTypeNotInPickList.get('SubType'))));
            }
        }
    }
    
    /********************************************************************
        Name : FillYear
        Purpose : Used for fill current year and next year.
    ********************************************************************/
    
    Public void FillYear(){
        YearOptions = new List<SelectOption>();
        YearOptions.add(new SelectOption('','All Years'));
        for(integer i=integer.valueof(STARTYEAR); i<=system.today().year()+1; i++){
            YearOptions.add(new SelectOption(String.valueOf(i),String.valueOf(i)));
        }
    }
    
    /********************************************************************
        Name : FillRegion
        Purpose : Used for fill region from country object.
    ********************************************************************/
    
    Public void FillRegion(){
        RegionOptions = new List<SelectOption>();
        Schema.sObjectType objType = Country__c.getSObjectType();
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe(); 
        map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap(); 
        List<Schema.PicklistEntry> Regions = fieldMap.get('Region_name__c').getDescribe().getPickListValues();
        RegionOptions.add(new SelectOption('','All Regions')); 
        for (Schema.PicklistEntry Entry : Regions){ 
            RegionOptions.add(new SelectOption(Entry.getLabel(), Entry.getValue())); 
        }
    }
    
    /********************************************************************
        Name : FillRisks
        Purpose : Used for fill risks.
    ********************************************************************/
    
    Public List<SelectOption> FillRisks(String strFieldName){
        List<SelectOption> RiskOptions = new List<SelectOption>();
        Schema.sObjectType objType = Grant__c.getSObjectType();
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe(); 
        map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap(); 
        List<Schema.PicklistEntry> RiskTypes = fieldMap.get(strFieldName).getDescribe().getPickListValues();
        if(strFieldName == 'Financial_and_Fiduciary_Risks__c') RiskOptions.add(new SelectOption('','Financial & Fiduciary Risk'));
        if(strFieldName == 'Health_Services_and_Products_Risks__c') RiskOptions.add(new SelectOption('','Health Services & Products Risk'));
        if(strFieldName == 'Programmatic_and_Performance_Risks__c') RiskOptions.add(new SelectOption('','Programmatic & Performance Risk'));
        for (Schema.PicklistEntry Entry : RiskTypes){ 
            RiskOptions.add(new SelectOption(Entry.getLabel(), Entry.getValue()));
        }
        return RiskOptions;
    }
    
    /********************************************************************
        Name : FillAllRisksOptions
        Purpose : Used for fill risks .
    ********************************************************************/
    
    Public Void FillAllRisksOptions(){
        FinancialRiskOptions = new List<SelectOption>();
        FinancialRiskOptions = FillRisks('Financial_and_Fiduciary_Risks__c');
        HealthRiskOptions = new List<SelectOption>();
        HealthRiskOptions = FillRisks('Health_Services_and_Products_Risks__c');
        ProgramaticRiskOptions = new List<SelectOption>();
        ProgramaticRiskOptions = FillRisks('Programmatic_and_Performance_Risks__c');
    }
    
    /********************************************************************
        Name : FillLFA
        Purpose : Used for fill account name of LFA record type.
    ********************************************************************/
    
    Public Void FillLFA(){
        LFAOptions = new List<SelectOption>();
        Id strRecordTypeId = [Select Id From RecordType Where SobjectType = 'Account' And RecordType.DeveloperName = 'LFA'].Id;
        List<Account> lstAccount = [Select Id,Name,Short_Name__c From Account Where RecordTypeId =: strRecordTypeId And ParentId = null and test_account__c = false]; 
        LFAOptions.add(new SelectOption('','All LFAs'));
        if(lstAccount != null && lstAccount.size() > 0){
            for(Account objAcc : lstAccount){
            
            System.debug(objAcc.id + ' and ' + objAcc.Short_Name__c);
            if(objAcc.Short_Name__c == null)
            objAcc.Short_Name__c = 'null';
             LFAOptions.add(new SelectOption(objAcc.id,objAcc.Short_Name__c));
            }
        }
    }
    
    Public PageReference ViewReport(){
        
        Integer Index = Integer.valueof(Apexpages.currentpage().getparameters().get('ServiceIndex'));
        if(Index != null){
            String SelectedCountryReport = '';
            String SelectedServiceSubTypeReport = '';
            String SelectedRegionReport = '';
            String SelectedYearReport = '';
            String SelectedFinancialRiskReport = '';
            String SelectedHealthRiskReport = '';
            String SelectedProgramaticRiskReport = '';
            String SelectedLFAReport = '';
            String URL = '/00Og0000000SZ7s';
            
            
            if(String.isBlank(SelectedServiceSubType) == false){
                for(SelectOption Sel : ServiceSubTypeOptions){
                    if(SelectedServiceSubType == Sel.getValue()){
                        SelectedServiceSubTypeReport = Sel.getLabel();
                    }
                }
                //072b0000000soaa
                URL += '?pc0=072b0000000soaa&pn0=eq&pv0='+SelectedServiceSubTypeReport;
            }
            integer i = 0;
            if(String.isBlank(SelectedCountry) == false){
                for(SelectOption Sel : CountryOptions){
                    if(SelectedCountry == Sel.getValue()){
                        SelectedCountryReport = Sel.getLabel();
                    }
                }
                i++;
                //072b0000000soZS
                URL += '&pc'+i+'=072b0000000soZS&pn'+i+'=eq&pv'+i+'='+SelectedCountryReport;
            }
            system.debug('####'+SelectedRegion);
            if(String.isBlank(SelectedRegion) == false){
                system.debug('####'+SelectedRegion);
                for(SelectOption Sel : RegionOptions){
                    if(SelectedRegion == Sel.getValue()){
                        SelectedRegionReport = Sel.getLabel();
                    }
                }
                i++;
                //072g0000000FMQq
                URL += '&pc'+i+'=072g0000000FMQq&pn'+i+'=eq&pv'+i+'='+SelectedRegionReport;
            }
            
            if(String.isBlank(SelectedYear) == false){
                for(SelectOption Sel : YearOptions){
                    if(SelectedYear == Sel.getValue()){
                        SelectedYear = Sel.getLabel();
                    }
                }
                i++;
                //072b0000000soZU
                //Date StartDate = date.valueof('01/01/'+Selectedyear);
                //Date EndDate = date.valueof('31/12/'+Selectedyear);
                
                URL += '&pc'+i+'=072b0000000soZU&pn'+i+'=ge&pv'+i+'=01/01/'+Selectedyear;
                i++;
                URL += '&pc'+i+'=072b0000000soZU&pn'+i+'=le&pv'+i+'=12/31/'+Selectedyear;
            }
            
            if(String.isBlank(SelectedFinancialRisk) == false){
                for(SelectOption Sel : FinancialRiskOptions){
                    if(SelectedFinancialRisk == Sel.getValue()){
                        SelectedFinancialRiskReport = Sel.getLabel();
                    }
                }
                i++;
                //072g0000000FMRZ
                URL += '&pc'+i+'=072g0000000FMRZ&pn'+i+'=eq&pv'+i+'='+SelectedFinancialRiskReport;
            }
            if(String.isBlank(SelectedHealthRisk) == false){
                for(SelectOption Sel : HealthRiskOptions){
                    if(SelectedHealthRisk == Sel.getValue()){
                        SelectedHealthRiskReport = Sel.getLabel();
                    }
                }
                i++;
                //072g0000000FMRb
                URL += '&pc'+i+'=072g0000000FMRb&pn'+i+'=eq&pv'+i+'='+SelectedHealthRiskReport;
            }
            if(String.isBlank(SelectedProgramaticRisk) == false){
                for(SelectOption Sel : ProgramaticRiskOptions){
                    if(SelectedProgramaticRisk== Sel.getValue()){
                        SelectedProgramaticRiskReport= Sel.getLabel();
                    }
                }
                i++;
                //072g0000000FMRa
                URL += '&pc'+i+'=072g0000000FMRa&pn'+i+'=eq&pv'+i+'='+SelectedProgramaticRiskReport;
            }
            if(String.isBlank(SelectedLFA) == false){
                for(SelectOption Sel : LFAOptions){
                    if(SelectedLFA == Sel.getValue()){
                        SelectedLFAReport = Sel.getLabel();
                    }
                }
                i++;
                List<Account> lstAccount = [Select Id,Name,Short_Name__c From Account Where ParentId =: SelectedLFA and test_account__c = false ORDER BY Short_Name__c Limit 1 ];
                if(lstAccount != null && lstAccount.size() > 0){
                    //072b0000000soZR
                    URL += '&pc'+i+'=072b0000000soZR&pn'+i+'=eq&pv'+i+'='+lstAccount[0].Name;
                    
                }else{
                    //072b0000000soZR
                    URL += '&pc'+i+'=072b0000000soZR&pn'+i+'=eq&pv'+i+'='+SelectedLFAReport;
                }
                
            }
            i++;
            if(test.isrunningtest() == false){
                URL += '&pc'+i+'=072b0000000sobJ&pn'+i+'=eq&pv'+i+'='+lstServiceForSearch[Index].RoleName;
            }
            i++;
            URL += '&pc'+i+'=072b0000000soaC&pn'+i+'=eq&pv'+i+'=1';
            
            
            //https://theglobalfund--lfa.cs17.my.salesforce.com/00Og0000000SZ7s/e?pc0=Service_Sub_Type&pn0=eq&pv0=PUDR
            //PageReference P = new PageReference('https://theglobalfund--lfa.cs17.my.salesforce.com/00Og0000000SZ7s?pv0='+SelectedServiceSubTypeReport+'&pv1='+SelectedRegionReport+'&pv2='+SelectedCountryReport+'&pv3='+SelectedYearReport+'&pv4='+SelectedLFAReport+'&pv5='+SelectedFinancialRiskReport+'&pv6='+SelectedHealthRiskReport+'&pv7='+SelectedProgramaticRiskReport+'&pv8='+lstServiceForSearch[Index].RoleName);
            PageReference P = new PageReference(URL);
            return P;
        }
        return null;
    }
    
    /********************************************************************
        Wrapper Class : wrapSearchService
    ********************************************************************/
    Public Class wrapSearchService{
        Public String RoleName {get;set;}
        Public Integer NoOfRecords {get;set;}
        Public Decimal MinValue {get;set;}
        Public Decimal MaxValue {get;set;}
        Public Decimal AvgValue {get;set;}
        Public Decimal MedianValue {get;set;}
    }
    
    /********************************************************************
        Name : ReturnToMainPage
        Purpose : Used for return to AddEditWorkplan page.
    ********************************************************************/
    Public PageReference ReturnToMainPage(){
        if(String.IsBlank(strServiceId) == false){
            PageReference pageRef = new PageReference('/apex/AddEditWorkplan?id='+strWorkPlanId);
            return pageRef;
        }else{
            return null;
        }
        return null;
    }
    
    /********************************************************************
        Name : clearFilter
        Purpose : Used for clear filtering value.
    ********************************************************************/
    Public void clearFilter(){
        SelectedCountry = '';
        SelectedServiceSubType = '';
        SelectedRegion = '';
        SelectedYear = '';
        SelectedFinancialRisk = '';
        SelectedHealthRisk = '';
        SelectedProgramaticRisk = '';
        SelectedLFA = '';
        lstServiceForSearch = new List<wrapSearchService>();
    }
    
    /********************************************************************
        Name : AnalyseSearch
        Purpose : Used for search service benchmarking details
                  for selected service type.
    ********************************************************************/
    
    Public void AnalyseSearch(){ 
        if(FinancialRisk!=null){
            SelectedFinancialRisk = FinancialRisk;
            FinancialRisk = NULL;
        }
        if(HealthRisk!=null){
            SelectedHealthRisk  = HealthRisk;
            HealthRisk = NULL;
        }
        if(ProgramaticRisk!=null){
            SelectedProgramaticRisk = ProgramaticRisk;
            ProgramaticRisk = NULL;
        }
        List<AggregateResult> lstServiceForSearchTemp = new List<AggregateResult>();
        List<AggregateResult> lstServiceTemp = new List<AggregateResult>();
        lstServiceForSearch = new List<wrapSearchService>();
        String Query = '';
        Query = 'Select COUNT(Id) NoOfRecord,Min(po_loe__c) MinLOE,Max(po_loe__c) MaxLOE,AVG(po_loe__c) AvgLOE,Min(po_cost__c) MinCost,Max(po_cost__c) MaxCost,AVG(po_cost__c) AvgCost,LFA_Role__c From LFA_Resource__c Where Id != null and LFA_Service__r.LFA_Work_Plan__r.Include_in_Library__c=TRUE and po_loe__c !=null';
        
        List<LFA_Resource__c> lstLFAResourceLOE = new List<LFA_Resource__c>();
        List<LFA_Resource__c> lstLFAResourceCost = new List<LFA_Resource__c>();
        String MedianLOEQuery = '';
        MedianLOEQuery ='Select ID, LFA_Role__c, po_loe__c From LFA_Resource__c Where Id != null and LFA_Service__r.LFA_Work_Plan__r.Include_in_Library__c=TRUE and po_loe__c != null';
        String MedianCostQuery = '';
        MedianCostQuery ='Select ID, LFA_Role__c,po_loe__c, po_cost__c From LFA_Resource__c Where Id != null and LFA_Service__r.LFA_Work_Plan__r.Include_in_Library__c=TRUE and po_loe__c != null';
        //math.mod(NoOfRecord+1,2)
        //math.floor(ans);
        //math.ceil(ans);
        
        String ServiceQuery = '';
        ServiceQuery = 'Select Min(po_loe__c) MinLOE,Max(po_loe__c) MaxLOE,AVG(po_loe__c) AvgLOE,Min(po_cost__c) MinCost,Max(po_cost__c) MaxCost,AVG(po_cost__c) AvgCost From LFA_Service__c Where Id != null AND po_loe__c>0 And Exclude_from_Library__c != TRUE';
       
        List<LFA_Service__c> lstMedianServiceLOE = new List<LFA_Service__c>();
        List<LFA_Service__c> lstMedianServiceCost = new List<LFA_Service__c>();
        String TotalMedianLOEQuery = '';
        TotalMedianLOEQuery ='Select po_loe__c From LFA_Service__c Where Id != null AND LFA_Work_Plan__r.Include_in_Library__c=TRUE and po_loe__c>0';
        String TotalMedianCostQuery = '';
        TotalMedianCostQuery ='Select po_loe__c,po_cost__c From LFA_Service__c Where Id != null AND LFA_Work_Plan__r.Include_in_Library__c=TRUE and po_loe__c>0';
        
        if(String.IsBlank(SelectedServiceSubType) == false){
            Query += ' And LFA_Service__r.Service_Sub_Type__c =: SelectedServiceSubType';
            MedianLOEQuery += ' And LFA_Service__r.Service_Sub_Type__c =: SelectedServiceSubType';
            MedianCostQuery += ' And LFA_Service__r.Service_Sub_Type__c =: SelectedServiceSubType';
            ServiceQuery += ' And Service_Sub_Type__c =: SelectedServiceSubType';
            TotalMedianLOEQuery += ' And Service_Sub_Type__c =: SelectedServiceSubType';
            TotalMedianCostQuery += ' And Service_Sub_Type__c =: SelectedServiceSubType';
        }
        if(String.Isblank(SelectedCountry) == false){
            Query += ' And LFA_Service__r.LFA_Work_Plan__r.Country__c != null And LFA_Service__r.LFA_Work_Plan__r.Country__r.Id =: SelectedCountry';
            MedianLOEQuery += ' And LFA_Service__r.LFA_Work_Plan__r.Country__c != null And LFA_Service__r.LFA_Work_Plan__r.Country__r.Id =: SelectedCountry';
            MedianCostQuery += ' And LFA_Service__r.LFA_Work_Plan__r.Country__c != null And LFA_Service__r.LFA_Work_Plan__r.Country__r.Id =: SelectedCountry';
            ServiceQuery += ' And LFA_Work_Plan__r.Country__c != null And LFA_Work_Plan__r.Country__r.Id =: SelectedCountry';
            TotalMedianLOEQuery += ' And LFA_Work_Plan__r.Country__c != null And LFA_Work_Plan__r.Country__r.Id =: SelectedCountry';
            TotalMedianCostQuery += ' And LFA_Work_Plan__r.Country__c != null And LFA_Work_Plan__r.Country__r.Id =: SelectedCountry';
        }
        if(String.Isblank(SelectedRegion) == false){
            Query += ' And LFA_Service__r.LFA_Work_Plan__r.Country__c != null And LFA_Service__r.LFA_Work_Plan__r.Country__r.Region_name__c =: SelectedRegion';
            MedianLOEQuery += ' And LFA_Service__r.LFA_Work_Plan__r.Country__c != null And LFA_Service__r.LFA_Work_Plan__r.Country__r.Region_name__c =: SelectedRegion';
            MedianCostQuery += ' And LFA_Service__r.LFA_Work_Plan__r.Country__c != null And LFA_Service__r.LFA_Work_Plan__r.Country__r.Region_name__c =: SelectedRegion';
            ServiceQuery += ' And LFA_Work_Plan__r.Country__c != null And LFA_Work_Plan__r.Country__r.Region_name__c =: SelectedRegion'; //Grant__r.Country__r.Region_name__c
            TotalMedianLOEQuery += ' And LFA_Work_Plan__r.Country__c != null And Grant__r.Country__r.Region_name__c =: SelectedRegion';
            TotalMedianCostQuery += ' And LFA_Work_Plan__r.Country__c != null And Grant__r.Country__r.Region_name__c =: SelectedRegion';
        }
        if(String.Isblank(SelectedYear) == false){
            Date StartDate = date.valueof(Selectedyear+'-01-01');
            Date EndDate = date.valueof(Selectedyear+'-12-31');
            Query += ' And LFA_Service__r.LFA_Work_Plan__r.End_Date__c >=: StartDate And LFA_Service__r.LFA_Work_Plan__r.End_Date__c <=: EndDate';
            MedianLOEQuery += ' And LFA_Service__r.LFA_Work_Plan__r.End_Date__c >=: StartDate And LFA_Service__r.LFA_Work_Plan__r.End_Date__c <=: EndDate';
            MedianCostQuery += ' And LFA_Service__r.LFA_Work_Plan__r.End_Date__c >=: StartDate And LFA_Service__r.LFA_Work_Plan__r.End_Date__c <=: EndDate';
            ServiceQuery += ' And LFA_Work_Plan__r.End_Date__c >=: StartDate And LFA_Work_Plan__r.End_Date__c <=: EndDate';
            TotalMedianLOEQuery += ' And LFA_Work_Plan__r.End_Date__c >=: StartDate And LFA_Work_Plan__r.End_Date__c <=: EndDate';
            TotalMedianCostQuery += ' And LFA_Work_Plan__r.End_Date__c >=: StartDate And LFA_Work_Plan__r.End_Date__c <=: EndDate';
        }
        /*if(String.IsBlank(strServiceId) == false){
            LFA_Service__c objService = [Select LFA_Work_Plan__c From LFA_Service__c Where id =: strServiceId];
            LFA_Work_Plan__c objWP = [Select Id,End_Date__c From LFA_Work_Plan__c Where Id=:objService.LFA_Work_Plan__c limit 1];
            if(String.Isblank(SelectedYear) == false && objWP != null && objWP.End_Date__c != null){
                Date StartDate = date.valueof(Selectedyear+'-01-01');
                Date EndDate = date.valueof(Selectedyear+'-12-31');
                Query += ' And LFA_Service__r.LFA_Work_Plan__r.End_Date__c >=: StartDate And LFA_Service__r.LFA_Work_Plan__r.End_Date__c <=: EndDate';
            }
        }*/
        if(String.Isblank(SelectedFinancialRisk) == false){
            Query += ' And LFA_Service__r.Grant__c != null And LFA_Service__r.Grant__r.Financial_and_Fiduciary_Risks__c =: SelectedFinancialRisk';
            MedianLOEQuery += ' And LFA_Service__r.Grant__c != null And LFA_Service__r.Grant__r.Financial_and_Fiduciary_Risks__c =: SelectedFinancialRisk';
            MedianCostQuery += ' And LFA_Service__r.Grant__c != null And LFA_Service__r.Grant__r.Financial_and_Fiduciary_Risks__c =: SelectedFinancialRisk';
            ServiceQuery += ' And Grant__c != null And Grant__r.Financial_and_Fiduciary_Risks__c =: SelectedFinancialRisk';
            TotalMedianLOEQuery += ' And Grant__c != null And Grant__r.Financial_and_Fiduciary_Risks__c =: SelectedFinancialRisk';
            TotalMedianCostQuery += ' And Grant__c != null And Grant__r.Financial_and_Fiduciary_Risks__c =: SelectedFinancialRisk';
        }
        if(String.Isblank(SelectedHealthRisk) == false){
            Query += ' And LFA_Service__r.Grant__c != null And LFA_Service__r.Grant__r.Health_Services_and_Products_Risks__c =: SelectedHealthRisk';
            MedianLOEQuery += ' And LFA_Service__r.Grant__c != null And LFA_Service__r.Grant__r.Health_Services_and_Products_Risks__c =: SelectedHealthRisk';
            MedianCostQuery += ' And LFA_Service__r.Grant__c != null And LFA_Service__r.Grant__r.Health_Services_and_Products_Risks__c =: SelectedHealthRisk';
            ServiceQuery += ' And Grant__c != null And Grant__r.Health_Services_and_Products_Risks__c =: SelectedHealthRisk';
            TotalMedianLOEQuery += ' And Grant__c != null And Grant__r.Health_Services_and_Products_Risks__c =: SelectedHealthRisk';
            TotalMedianCostQuery += ' And Grant__c != null And Grant__r.Health_Services_and_Products_Risks__c =: SelectedHealthRisk';
        }
        if(String.Isblank(SelectedProgramaticRisk) == false){
            Query += ' And LFA_Service__r.Grant__c != null And LFA_Service__r.Grant__r.Programmatic_and_Performance_Risks__c =: SelectedProgramaticRisk';
            MedianLOEQuery += ' And LFA_Service__r.Grant__c != null And LFA_Service__r.Grant__r.Programmatic_and_Performance_Risks__c =: SelectedProgramaticRisk';
            MedianCostQuery += ' And LFA_Service__r.Grant__c != null And LFA_Service__r.Grant__r.Programmatic_and_Performance_Risks__c =: SelectedProgramaticRisk';
            ServiceQuery += ' And Grant__c != null And Grant__r.Programmatic_and_Performance_Risks__c =: SelectedProgramaticRisk';
            TotalMedianLOEQuery += ' And Grant__c != null And Grant__r.Programmatic_and_Performance_Risks__c =: SelectedProgramaticRisk';
            TotalMedianCostQuery += ' And Grant__c != null And Grant__r.Programmatic_and_Performance_Risks__c =: SelectedProgramaticRisk';
        }
        if(String.Isblank(SelectedLFA) == false){
            system.debug('#$#SelectedLFA $#$'+SelectedLFA);
            List<Account> lstAccount = [Select Id,Name,Short_Name__c From Account Where ParentId =: SelectedLFA and test_account__c = false ORDER BY Short_Name__c Limit 1 ];
            if(lstAccount != null && lstAccount.size() > 0){
                Query += ' And LFA_Service__r.LFA_Work_Plan__r.LFA__r.ParentId =: SelectedLFA';
                MedianLOEQuery += ' And LFA_Service__r.LFA_Work_Plan__r.LFA__r.ParentId =: SelectedLFA';
                MedianCostQuery += ' And LFA_Service__r.LFA_Work_Plan__r.LFA__r.ParentId =: SelectedLFA';
                ServiceQuery += ' And LFA_Work_Plan__r.LFA__r.ParentId =: SelectedLFA';
                TotalMedianLOEQuery += ' And LFA_Work_Plan__r.LFA__r.ParentId =: SelectedLFA';
                TotalMedianCostQuery += ' And LFA_Work_Plan__r.LFA__r.ParentId =: SelectedLFA';
            }else{                
                Query += ' And LFA_Service__r.LFA_Work_Plan__r.LFA__c =: SelectedLFA';
                MedianLOEQuery += ' And LFA_Service__r.LFA_Work_Plan__r.LFA__c =: SelectedLFA';
                MedianCostQuery += ' And LFA_Service__r.LFA_Work_Plan__r.LFA__c =: SelectedLFA';
                ServiceQuery += ' And LFA_Work_Plan__r.LFA__c =: SelectedLFA';
                TotalMedianLOEQuery += ' And LFA_Work_Plan__r.LFA__c =: SelectedLFA';
                TotalMedianCostQuery += ' And LFA_Work_Plan__r.LFA__c =: SelectedLFA';
            }
        }
        Query += ' Group By LFA_Role__c';
        ServiceQuery += ' And LFA_Work_Plan__r.Include_in_Library__c = TRUE';
        TotalMedianLOEQuery += ' order by po_loe__c ASC';
        TotalMedianCostQuery += ' order by po_cost__c ASC';
        MedianLOEQuery += ' order by LFA_Role__c,po_loe__c ASC';
        MedianCostQuery += ' order by LFA_Role__c,po_cost__c ASC';
        
        system.debug('#$#$#$'+Query);
        system.debug('#$#$#$ ServiceQuery ---> '+ServiceQuery);
        lstServiceForSearchTemp = Database.Query(Query);
        lstServiceTemp = Database.Query(ServiceQuery);
        lstMedianServiceLOE = Database.Query(TotalMedianLOEQuery );
     
        lstMedianServiceCost = Database.Query(TotalMedianCostQuery );
        lstLFAResourceLOE = Database.Query(MedianLOEQuery);
        system.debug('#$#$#$'+lstServiceTemp);
        
        Map<String,List<LFA_Resource__c>> mapRoleLFAResourcesLOE =  new Map<String,List<LFA_Resource__c>>();
        for(LFA_Resource__c objLFAResource : lstLFAResourceLOE){
            if(mapRoleLFAResourcesLOE.containsKey(objLFAResource.LFA_Role__c)){
                mapRoleLFAResourcesLOE.get(objLFAResource.LFA_Role__c).add(objLFAResource);
            }else{
                mapRoleLFAResourcesLOE.put(objLFAResource.LFA_Role__c,new List<LFA_Resource__c>{objLFAResource});
            }
        }
        lstLFAResourceCost = Database.Query(MedianCostQuery);
        Map<String,List<LFA_Resource__c>> mapRoleLFAResourcesCost =  new Map<String,List<LFA_Resource__c>>();
        for(LFA_Resource__c objLFAResource : lstLFAResourceLOE){
            if(mapRoleLFAResourcesCost.containsKey(objLFAResource.LFA_Role__c)){
                mapRoleLFAResourcesCost.get(objLFAResource.LFA_Role__c).add(objLFAResource);
            }else{
                mapRoleLFAResourcesCost.put(objLFAResource.LFA_Role__c,new List<LFA_Resource__c>{objLFAResource});
            }
        }
        
        MinLOE = null;
        MinCost = null;
        MaxLOE = null;
        MaxCost = null;
        AvgLOE = null;
        AvgCost = null;
        MedianLOE = null;
        MedianCost = null;
        
        Set<Id> setRoleIds = new Set<Id>();
        for(AggregateResult objResource : lstServiceForSearchTemp){
            setRoleIds.add((ID)objResource.get('LFA_Role__c'));
        }
        List<LFA_Role__c> lstRole = [Select Id,Name,Sort_Order__c From LFA_Role__c Where Id IN : setRoleIds Order By Sort_Order__c];
        Integer MedianPosition;
        for(LFA_Role__c objRole : lstRole){
            for(AggregateResult objResource : lstServiceForSearchTemp){
                if(objRole.id == objResource.get('LFA_Role__c')){
                    wrapSearchService objWrap = new wrapSearchService();
                    objWrap.RoleName = objRole.Name;
                    objWrap.NoOfRecords = (Integer)objResource.get('NoOfRecord');
                    objWrap.MinValue = (Decimal)objResource.get('MinLOE');
                    objWrap.MaxValue = (Decimal)objResource.get('MaxLOE');
                    objWrap.AvgValue = (Decimal)objResource.get('AvgLOE');
                    System.debug('Test1:-' + objWrap.AvgValue);
                   if(objWrap.AvgValue != null)
                     objWrap.AvgValue = objWrap.AvgValue.setScale(2);
                    
                    MedianPosition = Integer.valueOf((objWrap.NoOfRecords + 1)/2);
                    if(Math.Mod((objWrap.NoOfRecords + 1),2)==0){
                      System.debug('hello1:-' + mapRoleLFAResourcesLOE.get(objRole.id)[MedianPosition-1].po_loe__c);
                      
                       objWrap.MedianValue = mapRoleLFAResourcesLOE.get(objRole.id)[MedianPosition-1].po_loe__c; //(Decimal)objResource.get('AvgLOE');
                    }else{
                      System.debug('hello2:-' + mapRoleLFAResourcesLOE.get(objRole.id)[MedianPosition-1].po_loe__c + ' and ' + mapRoleLFAResourcesLOE.get(objRole.id)[MedianPosition].po_loe__c);
                        objWrap.MedianValue = (mapRoleLFAResourcesLOE.get(objRole.id)[MedianPosition-1].po_loe__c + mapRoleLFAResourcesLOE.get(objRole.id)[MedianPosition].po_loe__c)/2; 
                    }
                   if(objWrap.MedianValue != null)
                    objWrap.MedianValue = objWrap.MedianValue.setScale(2);
                    lstServiceForSearch.add(objWrap);
                }
            }
        }
        if(lstServiceTemp.size() > 0){
            MinLOE = (Decimal)lstServiceTemp[0].get('MinLOE');
            MaxLOE = (Decimal)lstServiceTemp[0].get('MaxLOE');
            AvgLOE = (Decimal)lstServiceTemp[0].get('AvgLOE');
            if(AvgLOE != null) AvgLOE = AvgLOE.setScale(2);
            if(lstMedianServiceLOE !=null && lstMedianServiceLOE.size()>0){
                MedianPosition = Integer.valueOf((lstMedianServiceLOE.size() + 1)/2);
                if(Math.Mod((lstMedianServiceLOE.size() + 1),2)==0){
                System.debug('hello3:-' + lstMedianServiceLOE[MedianPosition-1].po_loe__c);
                 
                    MedianLOE = lstMedianServiceLOE[MedianPosition-1].po_loe__c; //(Decimal)objResource.get('AvgLOE');
                }else{
                 System.debug('hello4:-' + lstMedianServiceLOE[MedianPosition-1].po_loe__c + ' and ' + lstMedianServiceLOE[MedianPosition].po_loe__c);
                     
                    MedianLOE = (lstMedianServiceLOE[MedianPosition-1].po_loe__c + lstMedianServiceLOE[MedianPosition].po_loe__c)/2; 
                }
            }
           // MedianLOE = (Decimal)lstServiceTemp[0].get('AvgLOE');            
            if(MedianLOE != null) MedianLOE = MedianLOE.setScale(2);
            MinCost = (Decimal)lstServiceTemp[0].get('MinCost');
            MaxCost = (Decimal)lstServiceTemp[0].get('MaxCost');
            AvgCost = (Decimal)lstServiceTemp[0].get('AvgCost');
            if(lstMedianServiceCost !=null && lstMedianServiceCost.size()>0){
                MedianPosition = Integer.valueOf((lstMedianServiceCost.size() + 1)/2);
                if(Math.Mod((lstMedianServiceCost.size() + 1),2)==0){
                    MedianCost = lstMedianServiceCost[MedianPosition-1].po_cost__c; //(Decimal)objResource.get('AvgLOE');
                }else{
                    MedianCost = (lstMedianServiceCost[MedianPosition-1].po_cost__c + lstMedianServiceCost[MedianPosition].po_cost__c)/2; 
                }
            }            
            //MedianCost = (Decimal)lstServiceTemp[0].get('AvgCost');
        }
    }
    public void ChangeColor(){
        String paramValue= Apexpages.currentpage().getparameters().get('ChangeColorValue');
        if(paramValue == 'Financial & Fiduciary Risk'){
            SelectedFinancialRisk = FinancialRisk;
            FinancialRisk = 'Financial & Fiduciary Risk';
        }
        if(paramValue == 'Health Services & Products Risk'){
            SelectedHealthRisk = HealthRisk;
            HealthRisk = 'Health Services & Products Risk';
        }
        
        if(paramValue == 'Programmatic & Performance Risk'){
           SelectedProgramaticRisk = ProgramaticRisk;
           ProgramaticRisk = 'Programmatic & Performance Risk';
        } 
       
    }
    public void RemoveColor(){
        String paramValue= Apexpages.currentpage().getparameters().get('ClearColorValue');
        if(paramValue == 'Financial & Fiduciary Risk'){
            SelectedFinancialRisk = '';
            FinancialRisk = '';
        }
        if(paramValue == 'Health Services & Products Risk'){
            SelectedHealthRisk = '';
            HealthRisk = '';
        }
        
        if(paramValue == 'Programmatic & Performance Risk'){
           SelectedProgramaticRisk = '';
           ProgramaticRisk = '';
        } 
    }
   
}