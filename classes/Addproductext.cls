public class Addproductext {

  Public List<SelectOption> CostInputOptions {get;set;}
  Public List<Implementation_Period__c> lstimp;
  public string impid{get;set;}
  Public List<SelectOption> CatagoryOptions {get;set;}
  public string strSelectedCatagory{get;set;}
  public string strCostInputName{get;set;}
  public List<Product__C> lstproduct;
  

    public Addproductext(ApexPages.StandardSetController controller) {
    //getting Implementation Period Id
       impid =  Apexpages.currentpage().getparameters().get('Id');
    //getting Imp record        
        lstimp = [select id,Name,Component__c from implementation_period__c where id =:impid]; 
       system.debug('@@@@'+lstimp);
     //Creating Pick list values for select category 
        strSelectedCatagory = null;
        CatagoryOptions = new List<SelectOption>();
        Schema.sObjectType objType = Catalog_Cost_Input__c.getSObjectType();
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe(); 
        map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap(); 
        List<Schema.PicklistEntry> lstCatagory = fieldMap.get('Product_Category__c').getDescribe().getPickListValues();
        CatagoryOptions.add(new SelectOption('','--None--')); 
        for (Schema.PicklistEntry Entry : lstCatagory){ 
            CatagoryOptions.add(new SelectOption(Entry.getLabel(), Entry.getValue())); 
        }
      //creating picklist values for Product category
          
          /*CostInputOptions = new List<SelectOption>();
            List<Catalog_Cost_Input__c> lstCostInput = [Select Id,Name, Cost_Grouping__c, Unit_Cost_Definition__c 
                                                            From Catalog_Cost_Input__c 
                                                            Where PSM__c = true 
                                                            AND Disease_Impact__c INCLUDES(:lstimp[0].Component__c ) 
                                                            And Product_Category__c =: strSelectedCatagory 
                                                           Order by Cost_Input_Number__c];
               if(lstCostInput.size() > 0){
                CostInputOptions.add(new SelectOption('','--None--'));
                
                    for(Catalog_Cost_Input__c objCI : lstCostInput){
                        CostInputOptions.add(new SelectOption(objCI.Id,objCI.Name));
                    }
            }*/
           
   }
   
    public void selectedCatagory(){
     system.debug('@@@@'+strSelectedCatagory );
     system.debug('@@@@'+lstimp[0].Component__c );
        CostInputOptions = new List<SelectOption>();
        if(lstimp.size()>0){
            List<Catalog_Cost_Input__c> lstCostInput = [Select Id,Name, Cost_Grouping__c, Unit_Cost_Definition__c 
                                                            From Catalog_Cost_Input__c 
                                                            Where PSM__c = true 
                                                            AND Disease_Impact__c INCLUDES(:lstimp[0].Component__c ) 
                                                            And Product_Category__c =: strSelectedCatagory 
                                                           Order by Cost_Input_Number__c];
        
               if(lstCostInput.size() > 0){
                CostInputOptions.add(new SelectOption('','--None--'));
                
                    for(Catalog_Cost_Input__c objCI : lstCostInput){
                        CostInputOptions.add(new SelectOption(objCI.Id,objCI.Name));
                    }
            }
         }
    }
    
    public pageReference addProduct(){
    lstproduct = new List<Product__c>();
    Product__c objproduct = new Product__c();
//  Commented below line as implementation_period__c is not needed anymore
//    objproduct.Implementation_Period__c = impid;
    objproduct.Cost_Input__c = strCostInputName;
    lstproduct.add(objproduct);
    insert lstproduct;
     pageReference pr= new pageReference('/'+lstproduct[0].id+'/e?retURL=%2F'+lstproduct[0].id);
     pr.setredirect(true);
     Return pr;
    }
    

}