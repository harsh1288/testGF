/* Purpose: Handler class for AssumptionTrigger
* Created Date: 06th-Apr-2015
* Created By: RK
* Updated On:04th-May-2015
    : implemented logic for exchange rate consideration while calculating unit costs for budget line.  
*/
public class AssumptionHandler
{
  /*  public static void CalculateCostAfterInsert(Map<Id,Assumption__c> assumpMap)
    {
        Set<Id> asIds = new Set<Id>(assumpMap.keyset());
        Set<Id> blIds = new Set<Id>();
        for(Assumption__c a:assumpMap.values())
        {
            blIds.add(a.Budget_Line_Number__c);
        }
        
        // Query for finding all child assumptions in system
        List<Budget_Line__c> blList = new List<Budget_Line__c>([Select Currency_Used__c,Unit_Cost_Y1__c,Unit_Cost_Y2__c,Unit_Cost_Y3__c,Unit_Cost_Y4__c,
                                                                Implementation_Period__r.Grant__r.Country__r.Euro_to_USD_Exchange_rate__c,
                                                                Implementation_Period__r.Grant__r.Country__r.X200DEMA_Exch_Rate_in_EUR__c,
                                                                Implementation_Period__r.Grant__r.Country__r.X200DEMA_Exch_Rate_in_USD__c,
                                                                Implementation_Period__r.Grant__r.Country__r.USD_to_Euro_Exchange_rate__c,                                                                
                                                                Implementation_Period__r.Currency_of_Grant_Agreement__c,Y1_Number_Of_Persons__c,
                                                                Y2_Number_Of_Persons__c,Y3_Number_Of_Persons__c,Y4_Number_Of_Persons__c,
                                                                Cost_Input__r.Cost_Grouping__c,
                                                                (Select Id,Assumption_ETL_ID__c,Cost_Input__c,Payment_Currency__c,
                                                                Y1_Unit_Cost_Payment_Currency__c,Y1_Duration_Days__c,Y1_Level_of_Effort__c,
                                                                Y1_Net_Salary_Payment_Currency__c,Y1_Number_of_Persons__c,
                                                                Y1_Quantity__c,Y1_Social_Security_Obligatory_Charges__c,
                                                                Y2_Unit_Cost_Payment_Currency__c,Y2_Duration_Days__c,Y2_Level_of_Effort__c,
                                                                Y2_Net_Salary_Payment_Currency__c,Y2_Number_of_Persons__c,
                                                                Y2_Quantity__c,Y2_Social_Security_Obligatory_Charges__c,
                                                                Y3_Unit_Cost_Payment_Currency__c,Y3_Duration_Days__c,Y3_Level_of_Effort__c,
                                                                Y3_Net_Salary_Payment_Currency__c,Y3_Number_of_Persons__c,
                                                                Y3_Quantity__c,Y3_Social_Security_Obligatory_Charges__c,
                                                                Y4_Unit_Cost_Payment_Currency__c,Y4_Duration_Days__c,Y4_Level_of_Effort__c,
                                                                Y4_Net_Salary_Payment_Currency__c,Y4_Number_of_Persons__c,
                                                                Y4_Quantity__c,Y4_Social_Security_Obligatory_Charges__c
                                                                 from Budget_Line__c.Assumptions__r where Assumption_ETL_ID__c !=null) from Budget_Line__c where Id IN:blIds]);        
        
        //Start of creating map for Budget Lines in scope with their assumptions
        Map<Id,List<Assumption__c>> blAllAssumptionsMap = new Map<Id,List<Assumption__c>>();
        for(Budget_Line__c b:blList)
        {
            System.debug('******** '+b.Assumptions__r);
            if(b.Assumptions__r.size()>0)
            {                
                if(b.Y1_Number_Of_Persons__c == null) b.Y1_Number_Of_Persons__c=0;
                if(b.Y2_Number_Of_Persons__c ==null) b.Y2_Number_Of_Persons__c=0;
                if(b.Y3_Number_Of_Persons__c ==null) b.Y3_Number_Of_Persons__c=0;
                if(b.Y4_Number_Of_Persons__c ==null) b.Y4_Number_Of_Persons__c=0;
                for(Assumption__c a:b.Assumptions__r)
                {
                    if(a.Y1_Unit_Cost_Payment_Currency__c == null) a.Y1_Unit_Cost_Payment_Currency__c =0;
                    if(a.Y1_Duration_Days__c == null) a.Y1_Duration_Days__c =0;
                    if(a.Y1_Level_of_Effort__c == null) a.Y1_Level_of_Effort__c =0;
                    if(a.Y1_Net_Salary_Payment_Currency__c == null) a.Y1_Net_Salary_Payment_Currency__c =0;
                    if(a.Y1_Number_of_Persons__c == null) a.Y1_Number_of_Persons__c =0;
                    if(a.Y1_Quantity__c == null) a.Y1_Quantity__c=0;
                    if(a.Y1_Social_Security_Obligatory_Charges__c == null) a.Y1_Social_Security_Obligatory_Charges__c =0;
                    if(a.Y2_Unit_Cost_Payment_Currency__c == null) a.Y2_Unit_Cost_Payment_Currency__c=0;
                    if(a.Y2_Duration_Days__c == null) a.Y2_Duration_Days__c =0;
                    if(a.Y2_Level_of_Effort__c == null) a.Y2_Level_of_Effort__c =0;
                    if(a.Y2_Net_Salary_Payment_Currency__c == null) a.Y2_Net_Salary_Payment_Currency__c =0;
                    if(a.Y2_Number_of_Persons__c == null) a.Y2_Number_of_Persons__c =0;
                    if(a.Y2_Quantity__c == null) a.Y2_Quantity__c=0;
                    if(a.Y2_Social_Security_Obligatory_Charges__c == null) a.Y2_Social_Security_Obligatory_Charges__c =0;
                    if(a.Y3_Unit_Cost_Payment_Currency__c == null) a.Y3_Unit_Cost_Payment_Currency__c=0;
                    if(a.Y3_Duration_Days__c == null) a.Y3_Duration_Days__c =0;
                    if(a.Y3_Level_of_Effort__c == null) a.Y3_Level_of_Effort__c =0;
                    if(a.Y3_Net_Salary_Payment_Currency__c == null) a.Y3_Net_Salary_Payment_Currency__c =0;
                    if(a.Y3_Number_of_Persons__c == null) a.Y3_Number_of_Persons__c =0;
                    if(a.Y3_Quantity__c == null) a.Y3_Quantity__c=0;
                    if(a.Y3_Social_Security_Obligatory_Charges__c == null) a.Y3_Social_Security_Obligatory_Charges__c =0;
                    if(a.Y4_Unit_Cost_Payment_Currency__c == null) a.Y4_Unit_Cost_Payment_Currency__c=0;
                    if(a.Y4_Duration_Days__c == null) a.Y4_Duration_Days__c =0;
                    if(a.Y4_Level_of_Effort__c == null) a.Y4_Level_of_Effort__c =0;
                    if(a.Y4_Net_Salary_Payment_Currency__c == null) a.Y4_Net_Salary_Payment_Currency__c =0;
                    if(a.Y4_Number_of_Persons__c == null) a.Y4_Number_of_Persons__c =0;
                    if(a.Y4_Quantity__c == null) a.Y4_Quantity__c=0;
                    if(a.Y4_Social_Security_Obligatory_Charges__c == null) a.Y4_Social_Security_Obligatory_Charges__c =0;
                    if(blAllAssumptionsMap.containsKey(b.Id))
                    {
                        blAllAssumptionsMap.get(b.Id).add(a);
                    }
                    else
                    {
                        blAllAssumptionsMap.put(b.Id,new List<Assumption__c>{a});
                    }
                }
            }
        }
        System.debug('******** '+blAllAssumptionsMap);
        //End of creating map for Budget Lines in scope with their assumptions
        
        //Call method for each budget line to return calculated values. 
        for(Budget_Line__c b:blList)
        {
            Budget_Line__c tempBl = new Budget_Line__c();
            tempBl = updateCalculationOnAssump(b,blAllAssumptionsMap.get(b.Id));
            decimal blExtRate =1;
            System.debug('@@@@@@@@@ '+tempBl.Unit_Cost_Y1__c+'  $$$$$  '+tempBl.Unit_Cost_Y2__c+'  @@  '+tempBl.Unit_Cost_Y3__c+'  $$  '+tempBl.Unit_Cost_Y4__c);
             if(b.Implementation_Period__r.Currency_of_Grant_Agreement__c == 'USD' && b.Currency_Used__c == 'Other Currency')  
                    blExtRate = b.Implementation_Period__r.Grant__r.Country__r.Euro_to_USD_Exchange_rate__c;
                else if(b.Implementation_Period__r.Currency_of_Grant_Agreement__c == 'EUR' && b.Currency_Used__c == 'Other Currency')
                    blExtRate = b.Implementation_Period__r.Grant__r.Country__r.USD_to_Euro_Exchange_rate__c;
                else if (b.Implementation_Period__r.Currency_of_Grant_Agreement__c == 'USD' && b.Currency_Used__c == 'Local Currency')
                    blExtRate = b.Implementation_Period__r.Grant__r.Country__r.X200DEMA_Exch_Rate_in_USD__c ;
                else if(b.Implementation_Period__r.Currency_of_Grant_Agreement__c == 'EUR' && b.Currency_Used__c == 'Local Currency')
                    blExtRate = b.Implementation_Period__r.Grant__r.Country__r.X200DEMA_Exch_Rate_in_EUR__c;     
                else blExtRate = 1;
                
                system.debug('____Exchange rate----- '+blExtRate);
            b.Unit_Cost_Y1__c=(tempBl.Unit_Cost_Y1__c/blExtRate).setscale(0, RoundingMode.HALF_UP);
            system.debug('---Unit Cost Y1---'+ b.Unit_Cost_Y1__c);
            b.Unit_Cost_Y2__c=(tempBl.Unit_Cost_Y2__c/blExtRate).setscale(0, RoundingMode.HALF_UP);
            b.Unit_Cost_Y3__c=(tempBl.Unit_Cost_Y3__c/blExtRate).setscale(0, RoundingMode.HALF_UP);
            b.Unit_Cost_Y4__c=(tempBl.Unit_Cost_Y4__c/blExtRate).setscale(0, RoundingMode.HALF_UP);
        }
        try
        {            
            update blList;  // Update budget line list in scope for Average unit cost per year
        }
        catch(Exception ex)
        {
            System.debug('There was an error in dml: '+ex);
        }
    }
    
    // Start of method to calculate and return calculated Average unit cost for each budget line 
    Public static Budget_Line__c updateCalculationOnAssump (Budget_Line__c bl,List<Assumption__c> listAssump)
    {  
        List<assumpWrap> tempAssumpWrapList = new List<assumpWrap>();
        List<Decimal> NumOfPerson = new List<Decimal>{bl.Y1_Number_Of_Persons__c,bl.Y2_Number_Of_Persons__c,bl.Y3_Number_Of_Persons__c,bl.Y4_Number_Of_Persons__c};
        
        // Start of calculation when Cost grouping is of HR type
                   
        if(bl.Cost_Input__r.Cost_Grouping__c == '1. Human Resources (HR)')
        {
          for(Assumption__c a:listAssump)
          {
               assumpWrap tempaw = new assumpWrap();
               tempaw.assump =a;
               // Conversion Rate Calculation
                Decimal ExcRate=1;
                if(bl.Implementation_Period__r.Currency_of_Grant_Agreement__c == 'USD' && a.Payment_Currency__c== 'Other Currency')                    
                    ExcRate = bl.Implementation_Period__r.Grant__r.Country__r.Euro_to_USD_Exchange_rate__c;
                else if(bl.Implementation_Period__r.Currency_of_Grant_Agreement__c == 'EUR' && a.Payment_Currency__c == 'Other Currency')
                    ExcRate = bl.Implementation_Period__r.Grant__r.Country__r.USD_to_Euro_Exchange_rate__c;
                else if (bl.Implementation_Period__r.Currency_of_Grant_Agreement__c == 'USD' && a.Payment_Currency__c == 'Local Currency')
                    ExcRate = bl.Implementation_Period__r.Grant__r.Country__r.X200DEMA_Exch_Rate_in_USD__c ;
                else if(bl.Implementation_Period__r.Currency_of_Grant_Agreement__c == 'EUR' && a.Payment_Currency__c == 'Local Currency')
                    ExcRate = bl.Implementation_Period__r.Grant__r.Country__r.X200DEMA_Exch_Rate_in_EUR__c;     
                else ExcRate = 1;
                if(ExcRate ==null)ExcRate=1;
                tempaw.TotalSalaryY1 = a.Y1_Net_Salary_Payment_Currency__c + a.Y1_Social_Security_Obligatory_Charges__c;
                tempaw.TotalSalaryY2 = a.Y2_Net_Salary_Payment_Currency__c + a.Y2_Social_Security_Obligatory_Charges__c;
                tempaw.TotalSalaryY3 = a.Y3_Net_Salary_Payment_Currency__c + a.Y3_Social_Security_Obligatory_Charges__c;
                tempaw.TotalSalaryY4 = a.Y4_Net_Salary_Payment_Currency__c + a.Y4_Social_Security_Obligatory_Charges__c;
                 
                tempaw.TotalCostY1 = (((tempaw.TotalSalaryY1 * a.Y1_Level_of_Effort__c * a.Y1_Number_of_Persons__c)/100)*ExcRate).setscale(0, RoundingMode.Half_UP);
                tempaw.TotalCostY2 = (((tempaw.TotalSalaryY2 * a.Y2_Level_of_Effort__c * a.Y2_Number_of_Persons__c)/100)*ExcRate).setscale(0, RoundingMode.Half_UP);
                tempaw.TotalCostY3 = (((tempaw.TotalSalaryY3 * a.Y3_Level_of_Effort__c * a.Y3_Number_of_Persons__c)/100)*ExcRate).setscale(0, RoundingMode.Half_UP);
                tempaw.TotalCostY4 = (((tempaw.TotalSalaryY4 * a.Y4_Level_of_Effort__c * a.Y4_Number_of_Persons__c)/100)*ExcRate).setscale(0, RoundingMode.Half_UP);
              
                tempaw.TotalQuantityY1 = ((a.Y1_Number_of_Persons__c * a.Y1_Level_of_Effort__c)/100);
                tempaw.TotalQuantityY2 = ((a.Y2_Number_of_Persons__c * a.Y2_Level_of_Effort__c)/100);
                tempaw.TotalQuantityY3 = ((a.Y3_Number_of_Persons__c * a.Y3_Level_of_Effort__c)/100);
                tempaw.TotalQuantityY4 = ((a.Y4_Number_of_Persons__c * a.Y4_Level_of_Effort__c)/100);
              
              tempAssumpWrapList.add(tempaw);
          }
            System.debug('***wraplisthr***** '+tempAssumpWrapList);
        }
        
        // End of calculation when Cost grouping is of HR type
        
        // Start of calculation when Cost grouping is of TRC type
        else if(bl.Cost_Input__r.Cost_Grouping__c == '2. Travel related costs (TRC)')
        {
          for(Assumption__c a:listAssump)
          {
              assumpWrap tempaw = new assumpWrap();
              tempaw.assump =a; 
              // Conversion Rate Calculation
                Decimal ExcRate=1;
                if(bl.Implementation_Period__r.Currency_of_Grant_Agreement__c == 'USD' && a.Payment_Currency__c == 'Other Currency')  
                    ExcRate = bl.Implementation_Period__r.Grant__r.Country__r.Euro_to_USD_Exchange_rate__c;
                else if(bl.Implementation_Period__r.Currency_of_Grant_Agreement__c == 'EUR' && a.Payment_Currency__c == 'Other Currency')
                    ExcRate = bl.Implementation_Period__r.Grant__r.Country__r.USD_to_Euro_Exchange_rate__c;
                else if (bl.Implementation_Period__r.Currency_of_Grant_Agreement__c == 'USD' && a.Payment_Currency__c == 'Local Currency')
                    ExcRate = bl.Implementation_Period__r.Grant__r.Country__r.X200DEMA_Exch_Rate_in_USD__c ;
                else if(bl.Implementation_Period__r.Currency_of_Grant_Agreement__c == 'EUR' && a.Payment_Currency__c == 'Local Currency')
                    ExcRate = bl.Implementation_Period__r.Grant__r.Country__r.X200DEMA_Exch_Rate_in_EUR__c;     
                else ExcRate = 1;
                if(ExcRate ==null)ExcRate=1;
                 tempaw.TotalCostY1 = ((a.Y1_Unit_Cost_Payment_Currency__c * a.Y1_Duration_Days__c * a.Y1_Quantity__c*a.Y1_Number_of_Persons__c)*ExcRate).setscale(0, RoundingMode.Half_UP);
                 tempaw.TotalCostY2 = ((a.Y2_Unit_Cost_Payment_Currency__c * a.Y2_Duration_Days__c * a.Y2_Quantity__c*a.Y2_Number_of_Persons__c)*ExcRate).setscale(0, RoundingMode.Half_UP);
                 tempaw.TotalCostY3 = ((a.Y3_Unit_Cost_Payment_Currency__c * a.Y3_Duration_Days__c * a.Y3_Quantity__c*a.Y3_Number_of_Persons__c)*ExcRate).setscale(0, RoundingMode.Half_UP);
                 tempaw.TotalCostY4 = ((a.Y4_Unit_Cost_Payment_Currency__c * a.Y4_Duration_Days__c * a.Y4_Quantity__c*a.Y4_Number_of_Persons__c)*ExcRate).setscale(0, RoundingMode.Half_UP);                                                             
              tempAssumpWrapList.add(tempaw);
          }
            System.debug('***wraplisttrc***** '+tempAssumpWrapList);
        }
        
        // End of calculation when Cost grouping is of TRC type
        
        // Calculation for all other grouping
        else
        {
          for(Assumption__c a:listAssump)
          {
              assumpWrap tempaw = new assumpWrap();
              tempaw.assump =a;
              // Conversion Rate Calculation
                Decimal ExcRate=1;
                if(bl.Implementation_Period__r.Currency_of_Grant_Agreement__c == 'USD' && a.Payment_Currency__c == 'Other Currency')  
                    ExcRate = bl.Implementation_Period__r.Grant__r.Country__r.Euro_to_USD_Exchange_rate__c;
                else if(bl.Implementation_Period__r.Currency_of_Grant_Agreement__c == 'EUR' && a.Payment_Currency__c == 'Other Currency')
                    ExcRate = bl.Implementation_Period__r.Grant__r.Country__r.USD_to_Euro_Exchange_rate__c;
                else if (bl.Implementation_Period__r.Currency_of_Grant_Agreement__c == 'USD' && a.Payment_Currency__c == 'Local Currency')
                    ExcRate = bl.Implementation_Period__r.Grant__r.Country__r.X200DEMA_Exch_Rate_in_USD__c ;
                else if(bl.Implementation_Period__r.Currency_of_Grant_Agreement__c == 'EUR' && a.Payment_Currency__c == 'Local Currency')
                    ExcRate = bl.Implementation_Period__r.Grant__r.Country__r.X200DEMA_Exch_Rate_in_EUR__c;     
                else ExcRate = 1;
                if(ExcRate ==null)ExcRate=1;
              
                tempaw.TotalQuantityY1 = a.Y1_Quantity__c;
                tempaw.TotalQuantityY2 = a.Y2_Quantity__c;
                tempaw.TotalQuantityY3 = a.Y3_Quantity__c;
                tempaw.TotalQuantityY4 = a.Y4_Quantity__c;
                
                 tempaw.TotalCostY1 = ((a.Y1_Unit_Cost_Payment_Currency__c * a.Y1_Quantity__c)*ExcRate).setscale(0, RoundingMode.Half_UP);                 
                 tempaw.TotalCostY2 = ((a.Y2_Unit_Cost_Payment_Currency__c * a.Y2_Quantity__c)*ExcRate).setscale(0, RoundingMode.Half_UP);    
                 tempaw.TotalCostY3 = ((a.Y3_Unit_Cost_Payment_Currency__c * a.Y3_Quantity__c)*ExcRate).setscale(0, RoundingMode.Half_UP);
                 tempaw.TotalCostY4 = ((a.Y4_Unit_Cost_Payment_Currency__c * a.Y4_Quantity__c)*ExcRate).setscale(0, RoundingMode.Half_UP);
              tempAssumpWrapList.add(tempaw);
          }
            System.debug('***wraplistother***** '+tempAssumpWrapList);
        } 
        
        // Average Unit cost calculation for each budget line
        decimal TotalCostY1All=0;
        decimal TotalCostY2All=0;
        decimal TotalCostY3All=0;
        decimal TotalCostY4All=0;
        decimal TotalQuantityY1All=0;
        decimal TotalQuantityY2All=0;
        decimal TotalQuantityY3All=0;
        decimal TotalQuantityY4All=0;
        decimal AvgCostY1=0;
        decimal AvgCostY2=0;
        decimal AvgCostY3=0;
        decimal AvgCostY4=0;
        
        // for TRC type Budget line
        decimal maxNumOfPersonY1=0;
        decimal maxNumOfPersonY2=0;
        decimal maxNumOfPersonY3=0;
        decimal maxNumOfPersonY4=0;
        decimal maxDurationOfDaysY1=0;
        decimal maxDurationOfDaysY2=0;
        decimal maxDurationOfDaysY3=0;
        decimal maxDurationOfDaysY4=0;
        
        Integer divideAmount = 1;       
        if(bl.Cost_Input__r.Cost_Grouping__c == '1. Human Resources (HR)')
        {
            divideAmount = 4;
        }
        else
        {
            divideAmount = 1;
        }
        for(assumpWrap aw:tempAssumpWrapList)
        {
            if(aw.assump.Y1_Number_of_Persons__c > maxNumOfPersonY1)
            {
               maxNumOfPersonY1 = aw.assump.Y1_Number_of_Persons__c; 
            }
            if(aw.assump.Y2_Number_of_Persons__c > maxNumOfPersonY2)
            {
               maxNumOfPersonY2 = aw.assump.Y2_Number_of_Persons__c; 
            }
            if(aw.assump.Y3_Number_of_Persons__c > maxNumOfPersonY3)
            {
               maxNumOfPersonY3 = aw.assump.Y3_Number_of_Persons__c; 
            }
            if(aw.assump.Y4_Number_of_Persons__c > maxNumOfPersonY4)
            {
               maxNumOfPersonY4 = aw.assump.Y4_Number_of_Persons__c; 
            }
            if(aw.assump.Y1_Duration_Days__c > maxDurationOfDaysY1)
            {
               maxDurationOfDaysY1 = aw.assump.Y1_Duration_Days__c; 
            }
            if(aw.assump.Y2_Duration_Days__c > maxDurationOfDaysY2)
            {
               maxDurationOfDaysY2 = aw.assump.Y2_Duration_Days__c; 
            }
            if(aw.assump.Y3_Duration_Days__c > maxDurationOfDaysY3)
            {
               maxDurationOfDaysY3 = aw.assump.Y3_Duration_Days__c; 
            }
            if(aw.assump.Y4_Duration_Days__c > maxDurationOfDaysY4)
            {
               maxDurationOfDaysY4 = aw.assump.Y4_Duration_Days__c; 
            }
            TotalCostY1All +=aw.TotalCostY1;
            TotalCostY2All +=aw.TotalCostY2;
            TotalCostY3All +=aw.TotalCostY3;
            TotalCostY4All +=aw.TotalCostY4;            
            TotalQuantityY1All +=aw.TotalQuantityY1;
            TotalQuantityY2All +=aw.TotalQuantityY2;
            TotalQuantityY3All +=aw.TotalQuantityY3;
            TotalQuantityY4All +=aw.TotalQuantityY4;
        }
        if(bl.Cost_Input__r.Cost_Grouping__c != '2. Travel related costs (TRC)')
        {
            if(TotalQuantityY1All >0)
            {
                bl.Unit_Cost_Y1__c=((TotalCostY1All/TotalQuantityY1All)/divideAmount);
            }
            if(TotalQuantityY2All >0)
            {
                bl.Unit_Cost_Y2__c=((TotalCostY2All/TotalQuantityY2All)/divideAmount);
            }
            if(TotalQuantityY3All >0)
            {                
                bl.Unit_Cost_Y3__c=((TotalCostY3All/TotalQuantityY3All)/divideAmount);
            }
            if(TotalQuantityY4All >0)
            {
                bl.Unit_Cost_Y4__c=((TotalCostY4All/TotalQuantityY4All)/divideAmount);
            }
        }
        else
        {
            if(maxNumOfPersonY1>0 && maxDurationOfDaysY1>0)
            {
                bl.Unit_Cost_Y1__c=((TotalCostY1All/(maxNumOfPersonY1*maxDurationOfDaysY1)));
            }
            if(maxNumOfPersonY2>0 && maxDurationOfDaysY2>0)
            {
                bl.Unit_Cost_Y2__c=((TotalCostY2All/(maxNumOfPersonY2*maxDurationOfDaysY2)));
            }
            if(maxNumOfPersonY3>0 && maxDurationOfDaysY3>0)
            {
                bl.Unit_Cost_Y3__c=((TotalCostY3All/(maxNumOfPersonY3*maxDurationOfDaysY3)));
            }
            if(maxNumOfPersonY4>0 && maxDurationOfDaysY4>0)
            {
                bl.Unit_Cost_Y4__c=((TotalCostY4All/(maxNumOfPersonY4*maxDurationOfDaysY4)));            
            }
        }
        if(TotalQuantityY1All ==0)
            {
                bl.Unit_Cost_Y1__c=0;
            }
            if(TotalQuantityY2All ==0)
            {
                bl.Unit_Cost_Y2__c=0;
            }
            if(TotalQuantityY3All ==0)
            {                
                bl.Unit_Cost_Y3__c=0;
            }
            if(TotalQuantityY4All ==0)
            {
                bl.Unit_Cost_Y4__c=0;
            }
        return bl;
    }
    // End of method to calculate and return calculated Average unit cost for each budget line
    
    // Wrapper class to hold per year total cost and quantity.
    Public class assumpWrap
    {
        Assumption__c assump;
        decimal TotalCostY1=0;
        decimal TotalCostY2=0;
        decimal TotalCostY3=0;
        decimal TotalCostY4=0;
        decimal TotalSalaryY1=0;
        decimal TotalSalaryY2=0;
        decimal TotalSalaryY3=0;
        decimal TotalSalaryY4=0;
        decimal TotalQuantityY1=0;
        decimal TotalQuantityY2=0;
        decimal TotalQuantityY3=0;
        decimal TotalQuantityY4=0;        
    }
    */
}