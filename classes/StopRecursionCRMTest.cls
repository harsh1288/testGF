@isTest
public class StopRecursionCRMTest{

 public static testMethod void checkAfterCon(){

     StopRecursionCRM.setAlreadyRunAfterCon(); 
     System.assertEquals(StopRecursionCRM.hasAlreadyRunAfterCon(), true);
    }
 public static testMethod void checkBeforeConInsert(){

    StopRecursionCRM.setAlreadyRunBeforeInsertCon();
    System.assertEquals(StopRecursionCRM.hasAlreadyRunBeforeInsertCon(), true);
    }    
 public static testMethod void checkAfterConInsert(){

    StopRecursionCRM.setAlreadyRunAfterInsertCon();
    System.assertEquals(StopRecursionCRM.hasAlreadyRunAfterInsertCon(), true);
    }   
 public static testMethod void checkBeforeConUpdate(){

    StopRecursionCRM.setAlreadyRunBeforeUpdateCon();
    System.assertEquals(StopRecursionCRM.hasAlreadyRunBeforeUpdateCon(), true);
    }      
  public static testMethod void checkAfterUpdateCon(){

    StopRecursionCRM.setAlreadyRunAfterUpdateCon();
    System.assertEquals(StopRecursionCRM.hasAlreadyRunAfterUpdateCon(), true);
    }   
  public static testMethod void checkBeforeCon(){

    StopRecursionCRM.setAlreadyRunBeforeCon();
    System.assertEquals(StopRecursionCRM.hasAlreadyRunBeforeCon(), true);
    }  
    
    
            
            
}