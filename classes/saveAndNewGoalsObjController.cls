/*
* $Author:      $ TCS - Rahul Kumar
* $Description: $ Class for controlling new Goal/Objective create event redirection
* $Date:        $ 13-Nov-2014 
* $Revision:    $
*/
public class saveAndNewGoalsObjController{

    private Goals_Objectives__c gObj;
    private Map<String, String> keyPrefixMap;
    public String backUrl{get;set;}
    public string Rectype ;
    public saveAndNewGoalsObjController(ApexPages.StandardController stdController){
        this.gObj = (Goals_Objectives__c)stdController.getRecord();
        backUrl = '';         
    }
    
     public PageReference createNewGobjurl()
     {
            boolean statusPF = saveandNewHandler.pfTrue('Goals_Objectives__c');
            keyPrefixMap =  saveandNewHandler.schemaMap();
            String baseUrl;
            string userTypeStr = UserInfo.getUserType();
            baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
            if(userTypeStr == 'Standard') 
            {
                baseUrl = baseUrl+'/';
            }
            else
            { 
                baseUrl = baseUrl+'/GM/';
            }            
           if(ApexPages.currentPage().getParameters().get('type') !=null)
           {
                If(ApexPages.currentPage().getParameters().get('type')=='Goal') Rectype = Label.Goal_IP_Record_Type;
                else 
                    Rectype = Label.Objective_IP_Record_Type; 
                // Creating url according to user type when Save and New is clicked while editing a record
                String pfName = ApexPages.currentPage().getParameters().get('pfId');
                String ipName = ApexPages.currentPage().getParameters().get('ipId');
                baseUrl += String.valueOf(keyPrefixMap.get('Goals_Objectives__c'));
                baseUrl += '/e?nooverride=1';
                baseUrl += '&RecordType=';
                baseUrl += Rectype;
                if(ipName !=null && pfName !=null)
                {
                    baseUrl += '&';
                    baseUrl += Label.Go_Obj_Type_Id;
                    baseUrl += '=';
                    baseUrl += ApexPages.currentPage().getParameters().get('type');                    
                    baseUrl += '&';
                    baseUrl += Label.GO_Obj_GIP_Id;
                    baseUrl += '=';
                    baseUrl += ipName;
                    baseUrl += '&';                    
                    baseUrl += Label.GO_Obj_PE_Id;
                    baseUrl += '=';
                    baseUrl += pfName;            
                    baseUrl += '&retURL=';
                    if(userTypeStr != 'Standard') 
                    {  
                        baseUrl +='GM%2F';                  
                    }
                    else
                    {
                    }
                    baseUrl += ApexPages.currentPage().getParameters().get('ret');
                }
                PageReference saveNewUrl = new PageReference(baseUrl);
                saveNewUrl.setRedirect(true);                
                return saveNewUrl;            
           }
           else
           {
               // Creating url according to user type when Save and New is clicked while creating new record
               if(userTypeStr == 'Standard') 
                {                    
                }
                else
                { 
                    if(ApexPages.currentPage().getParameters().get('retURL').indexOf('GM') > -1)
                    {
                        
                    }
                    else
                    {
                        backUrl += +'/GM';
                    }
                }
               backUrl += ApexPages.currentPage().getParameters().get('retURL');
               if(backUrl.contains('?'))
               {
                   backUrl += '&nooverride=1';
               }
               else
               {
                   backUrl += '?nooverride=1';
               }
               ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Record has been saved <br/> Can\'t use Save and New feature, Please <a id="goback" href="'+backUrl+'"><b>Go back</b></a>'));                           
               return null;
           }         
     }
}