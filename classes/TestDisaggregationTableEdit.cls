@isTest
public class TestDisaggregationTableEdit {
	Public static testMethod void TestDisaggregationTableEdit(){
        Account objAcc = TestClassHelper.insertAccount();
    
        Grant__c objgrant = TestClassHelper.insertGrant(objAcc);
    
        Implementation_Period__c objip = TestClassHelper.createIPWithConceptNote(objGrant, objAcc);
        insert objip;
        
        Performance_Framework__c objpf = TestClassHelper.createPF(objIP);
        objpf.PF_Status__c = 'Not Yet Submitted';
        insert objpf;
        
        Module__c objmod = TestClassHelper.createModule();
        objmod.Performance_Framework__c = objpf.Id;
        insert objmod;
        
        Indicator__c objind = TestClassHelper.insertCatalogIndicator();
        	
        Grant_Indicator__c objgiimp = TestClassHelper.createGrantIndicator();
        objgiimp.Indicator__c = objind.id;
        objgiimp.Indicator_Type__c = 'Impact';
        objgiimp.Performance_Framework__c = objpf.Id;
        objgiimp.Data_Type__c = 'Number';
        objgiimp.Decimal_Places__c = '2';
        insert objgiimp;
        
        ApexPages.currentPage().getParameters().put('par1',objgiimp.Id);
        ApexPages.currentPage().getParameters().put('par2',objpf.PF_Status__c);
        ApexPages.currentPage().getParameters().put('par3',objgiimp.Data_Type__c);
        ApexPages.currentPage().getParameters().put('par4',objgiimp.Decimal_Places__c);
        ApexPages.currentPage().getParameters().put('par5',objpf.Id);
        ApexPages.currentPage().getParameters().put('par6',objgiimp.Indicator_Type__c);
        ApexPages.currentPage().getParameters().put('par7',objgiimp.Name);
        
        List<Grant_Indicator__c> lstind = new List<Grant_Indicator__c>();
        lstind.add(objgiimp);
       
        ApexPages.StandardSetController sc = new ApexPages.StandardSetController(lstind);
        DisaggregationTableEdit objDTE = new DisaggregationTableEdit(sc);
        objDTE.ReturnToInd();
        
        Grant_Indicator__c objgicov = TestClassHelper.createGrantIndicator();
        objgicov.Indicator_Type__c = 'Coverage/Output';
        objgicov.Performance_Framework__c = objpf.Id;
        objgicov.Data_Type__c = 'Number';
        objgicov.Decimal_Places__c = '2';
        objgicov.Parent_Module__c = objmod.Id;
        insert objgicov;
        
        ApexPages.currentPage().getParameters().put('par6',objgicov.Indicator_Type__c);
        
        lstind.add(objgicov);
        
        ApexPages.StandardSetController sc1 = new ApexPages.StandardSetController(lstind);
        DisaggregationTableEdit objDTE1 = new DisaggregationTableEdit(sc1);
        objDTE1.ReturnToInd();
        
	}
    
}