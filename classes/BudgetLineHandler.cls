/* Purpose: Handler class for BudgetLineTrigger
* Created Date: 06th-Apr-2015
* Created By: RK
* Updated On:
*/
public class BudgetLineHandler
{
    public static void CalculateBudgetTrackerAfterInsert(List<Budget_Line__c> bllst)
    {
        Id GIPId=bllst[0].Implementation_Period__c;
        if(GIPId != null)
        {
            Set<Id> blIds = new Set<Id>();
            double ExcRate;
            List<budgetLineWrap> objwrapList = new List<budgetLineWrap>();
            List<Budget_Line__c> ListBudLineToUpdate = new List<Budget_Line__c>();
            List<IP_Detail_Information__c> lstDB = new List<IP_Detail_Information__c>();
            Map<Id,IP_Detail_Information__c> mapDB = new Map<Id,IP_Detail_Information__c>();
            Decimal Tempsum =0, tempPSMSum = 0;
            for(Budget_Line__c bl:bllst)
            {
                blIds.add(bl.Id);
                
            }
            List<Budget_Line__c> blList = new List<Budget_Line__c>([Select Detailed_Budget_Framework__r.Total_Budgeted_Amount__c,Implementation_Period__r.Currency_of_Grant_Agreement__c,Implementation_Period__r.High_level_budget_GAC_1_USD__c,Implementation_Period__r.High_level_budget_GAC_1_EUR__c,
                                                                    Implementation_Period__r.Grant__r.Country__r.X200DEMA_Exch_Rate_in_USD__c,Implementation_Period__r.Grant__r.Country__r.X200DEMA_Exch_Rate_in_EUR__c,
                                                                    Implementation_Period__r.Grant__r.Country__r.Euro_to_USD_Exchange_rate__c,Implementation_Period__r.Grant__r.Country__r.USD_to_Euro_Exchange_rate__c,
                                                                    Unit_Cost_Y1__c,Unit_Cost_Y2__c,Unit_Cost_Y3__c,Unit_Cost_Y4__c,Q1_Quantity__c,Q1_Amount__c,Q2_Quantity__c,Q2_Amount__c,Q3_Quantity__c,Q3_Amount__c,
                                                                    Q4_Quantity__c,Q4_Amount__c,Q5_Quantity__c,Q5_Amount__c,Q6_Quantity__c,Q6_Amount__c,Q7_Quantity__c,Q7_Amount__c,Q8_Quantity__c,Q8_Amount__c,
                                                                    Q9_Quantity__c,Q9_Amount__c,Q10_Quantity__c,Q10_Amount__c,Q11_Quantity__c,Q11_Amount__c,Q12_Quantity__c,Q12_Amount__c,Q13_Quantity__c,Q13_Amount__c,
                                                                    Q14_Quantity__c,Q14_Amount__c,Q15_Quantity__c,Q15_Amount__c,Q16_Quantity__c,Q16_Amount__c,Q1_Grant_Amount__c,Q2_Grant_Amount__c,Q3_Grant_Amount__c,
                                                                    Q4_Grant_Amount__c,Q5_Grant_Amount__c,Q6_Grant_Amount__c,Q7_Grant_Amount__c,Q8_Grant_Amount__c,Q9_Grant_Amount__c,Q10_Grant_Amount__c,Detailed_Budget_Framework__r.HPC_Amount__c,
                                                                    Q11_Grant_Amount__c,Q12_Grant_Amount__c,Q13_Grant_Amount__c,Q14_Grant_Amount__c,Q15_Grant_Amount__c,Q16_Grant_Amount__c,Currency_Used__c, Cost_Input__r.PSM__c
                                                                    From Budget_Line__c where Implementation_Period__c =: GIPId /*and Cost_Input__r.PSM__c = false*/ ]);
            IP_Detail_Information__c dbToUpdate = [Select Name,Remaining_Budget_Amount__c,HPC_Amount__c,Total_Budgeted_Amount__c FROM IP_Detail_Information__c where Implementation_Period__c =: GIPId];
            
            for (Budget_Line__c objBL : blList){
                 system.debug('OBJ Budget Lines');
                 budgetLineWrap objwrap = new budgetLineWrap();
                objwrap.budgetLine = objBL;
                objwrapList.add(objwrap);
              /*  for(IP_Detail_Information__c objDB :dbList ){
                    if(objBL.Implementation_Period__c == objDB.Implementation_period__c)mapDB.put(objBL.Id,objDB);
                }*/
            }
            
                for(budgetLineWrap objwrap : objwrapList){
                 /*Define the values if it is null */
                    if(objwrap.budgetLine.Unit_Cost_Y1__c == null) objwrap.budgetLine.Unit_Cost_Y1__c = 0;
                    if(objwrap.budgetLine.Unit_Cost_Y2__c == null) objwrap.budgetLine.Unit_Cost_Y2__c = 0;
                    if(objwrap.budgetLine.Unit_Cost_Y3__c == null) objwrap.budgetLine.Unit_Cost_Y3__c = 0;
                    if(objwrap.budgetLine.Unit_Cost_Y4__c == null) objwrap.budgetLine.Unit_Cost_Y4__c = 0;
                    
                    if(objwrap.budgetLine.Q1_Quantity__c == null) objwrap.budgetLine.Q1_Quantity__c = 0;
                    if(objwrap.budgetLine.Q2_Quantity__c == null) objwrap.budgetLine.Q2_Quantity__c = 0;
                    if(objwrap.budgetLine.Q3_Quantity__c == null) objwrap.budgetLine.Q3_Quantity__c = 0;
                    if(objwrap.budgetLine.Q4_Quantity__c == null) objwrap.budgetLine.Q4_Quantity__c = 0;
                    if(objwrap.budgetLine.Q5_Quantity__c == null) objwrap.budgetLine.Q5_Quantity__c = 0;
                    if(objwrap.budgetLine.Q6_Quantity__c == null) objwrap.budgetLine.Q6_Quantity__c = 0;
                    if(objwrap.budgetLine.Q7_Quantity__c == null) objwrap.budgetLine.Q7_Quantity__c = 0;
                    if(objwrap.budgetLine.Q8_Quantity__c == null) objwrap.budgetLine.Q8_Quantity__c = 0;
                    if(objwrap.budgetLine.Q9_Quantity__c == null) objwrap.budgetLine.Q9_Quantity__c = 0;
                    if(objwrap.budgetLine.Q10_Quantity__c == null) objwrap.budgetLine.Q10_Quantity__c = 0;
                    if(objwrap.budgetLine.Q11_Quantity__c == null) objwrap.budgetLine.Q11_Quantity__c = 0;
                    if(objwrap.budgetLine.Q12_Quantity__c == null) objwrap.budgetLine.Q12_Quantity__c = 0;
                    if(objwrap.budgetLine.Q13_Quantity__c == null) objwrap.budgetLine.Q13_Quantity__c = 0;
                    if(objwrap.budgetLine.Q14_Quantity__c == null) objwrap.budgetLine.Q14_Quantity__c = 0;
                    if(objwrap.budgetLine.Q15_Quantity__c == null) objwrap.budgetLine.Q15_Quantity__c = 0;
                    if(objwrap.budgetLine.Q16_Quantity__c == null) objwrap.budgetLine.Q16_Quantity__c = 0;
                    
                    /* Starting Calculation of Outflow values */
                    objwrap.budgetLine.Q1_Amount__c = (objwrap.budgetLine.Unit_Cost_Y1__c * objwrap.budgetLine.Q1_Quantity__c );
                    objwrap.budgetLine.Q2_Amount__c = (objwrap.budgetLine.Unit_Cost_Y1__c * objwrap.budgetLine.Q2_Quantity__c);
                    objwrap.budgetLine.Q3_Amount__c = (objwrap.budgetLine.Unit_Cost_Y1__c * objwrap.budgetLine.Q3_Quantity__c);
                    objwrap.budgetLine.Q4_Amount__c = (objwrap.budgetLine.Unit_Cost_Y1__c * objwrap.budgetLine.Q4_Quantity__c);
                    objwrap.budgetLine.Q5_Amount__c = (objwrap.budgetLine.Unit_Cost_Y2__c * objwrap.budgetLine.Q5_Quantity__c);
                    objwrap.budgetLine.Q6_Amount__c = (objwrap.budgetLine.Unit_Cost_Y2__c * objwrap.budgetLine.Q6_Quantity__c);
                    objwrap.budgetLine.Q7_Amount__c = (objwrap.budgetLine.Unit_Cost_Y2__c * objwrap.budgetLine.Q7_Quantity__c);
                    objwrap.budgetLine.Q8_Amount__c = (objwrap.budgetLine.Unit_Cost_Y2__c * objwrap.budgetLine.Q8_Quantity__c);
                    objwrap.budgetLine.Q9_Amount__c = (objwrap.budgetLine.Unit_Cost_Y3__c * objwrap.budgetLine.Q9_Quantity__c);
                    objwrap.budgetLine.Q10_Amount__c = (objwrap.budgetLine.Unit_Cost_Y3__c * objwrap.budgetLine.Q10_Quantity__c);
                    objwrap.budgetLine.Q11_Amount__c = (objwrap.budgetLine.Unit_Cost_Y3__c * objwrap.budgetLine.Q11_Quantity__c);
                    objwrap.budgetLine.Q12_Amount__c = (objwrap.budgetLine.Unit_Cost_Y3__c * objwrap.budgetLine.Q12_Quantity__c);
                    objwrap.budgetLine.Q13_Amount__c = (objwrap.budgetLine.Unit_Cost_Y4__c * objwrap.budgetLine.Q13_Quantity__c);
                    objwrap.budgetLine.Q14_Amount__c = (objwrap.budgetLine.Unit_Cost_Y4__c * objwrap.budgetLine.Q14_Quantity__c);
                    objwrap.budgetLine.Q15_Amount__c = (objwrap.budgetLine.Unit_Cost_Y4__c * objwrap.budgetLine.Q15_Quantity__c);
                    objwrap.budgetLine.Q16_Amount__c = (objwrap.budgetLine.Unit_Cost_Y4__c * objwrap.budgetLine.Q16_Quantity__c);
                    system.debug('Amount Calculation '+ objwrap.budgetLine.Q1_Amount__c);
                    /*Calculation of Exchange Rate for calculating outflow values*/
                    if(objwrap.budgetLine.Implementation_Period__r.Currency_of_Grant_Agreement__c == 'USD' && objwrap.budgetLine.Currency_Used__c == 'Other Currency')  ExcRate = objwrap.budgetLine.Implementation_Period__r.Grant__r.Country__r.Euro_to_USD_Exchange_rate__c;
                    else if(objwrap.budgetLine.Implementation_Period__r.Currency_of_Grant_Agreement__c == 'EUR' && objwrap.budgetLine.Currency_Used__c == 'Other Currency')  ExcRate = objwrap.budgetLine.Implementation_Period__r.Grant__r.Country__r.USD_to_Euro_Exchange_rate__c;
                    else if (objwrap.budgetLine.Implementation_Period__r.Currency_of_Grant_Agreement__c == 'USD' && objwrap.budgetLine.Currency_Used__c == 'Local Currency')   ExcRate = objwrap.budgetLine.Implementation_Period__r.Grant__r.Country__r.X200DEMA_Exch_Rate_in_USD__c ;
                    else if(objwrap.budgetLine.Implementation_Period__r.Currency_of_Grant_Agreement__c == 'EUR' && objwrap.budgetLine.Currency_Used__c == 'Local Currency')   ExcRate = objwrap.budgetLine.Implementation_Period__r.Grant__r.Country__r.X200DEMA_Exch_Rate_in_EUR__c;     
                    else ExcRate = 1;
                    System.debug('=====COST PSM==='+objwrap.budgetLine.Cost_Input__r.PSM__c);
                    /*Calculation of Outflow values*/
                    if( !objwrap.budgetLine.Cost_Input__r.PSM__c ){
                        objwrap.Q1Case = (objwrap.budgetLine.Unit_Cost_Y1__c * objwrap.budgetLine.Q1_Quantity__c * ExcRate);
                        objwrap.Q2Case = (objwrap.budgetLine.Unit_Cost_Y1__c * objwrap.budgetLine.Q2_Quantity__c * ExcRate);
                        objwrap.Q3Case = (objwrap.budgetLine.Unit_Cost_Y1__c * objwrap.budgetLine.Q3_Quantity__c * ExcRate);
                        objwrap.Q4Case = (objwrap.budgetLine.Unit_Cost_Y1__c * objwrap.budgetLine.Q4_Quantity__c * ExcRate);
                        objwrap.Q5Case = (objwrap.budgetLine.Unit_Cost_Y2__c * objwrap.budgetLine.Q5_Quantity__c * ExcRate);
                        objwrap.Q6Case = (objwrap.budgetLine.Unit_Cost_Y2__c * objwrap.budgetLine.Q6_Quantity__c * ExcRate);
                        objwrap.Q7Case = (objwrap.budgetLine.Unit_Cost_Y2__c * objwrap.budgetLine.Q7_Quantity__c * ExcRate);
                        objwrap.Q8Case = (objwrap.budgetLine.Unit_Cost_Y2__c * objwrap.budgetLine.Q8_Quantity__c * ExcRate);
                        objwrap.Q9Case = (objwrap.budgetLine.Unit_Cost_Y3__c * objwrap.budgetLine.Q9_Quantity__c * ExcRate);
                        objwrap.Q10Case = (objwrap.budgetLine.Unit_Cost_Y3__c * objwrap.budgetLine.Q10_Quantity__c * ExcRate);
                        objwrap.Q11Case = (objwrap.budgetLine.Unit_Cost_Y3__c * objwrap.budgetLine.Q11_Quantity__c * ExcRate);
                        objwrap.Q12Case = (objwrap.budgetLine.Unit_Cost_Y3__c * objwrap.budgetLine.Q12_Quantity__c * ExcRate);
                        objwrap.Q13Case = (objwrap.budgetLine.Unit_Cost_Y4__c * objwrap.budgetLine.Q13_Quantity__c * ExcRate);
                        objwrap.Q14Case = (objwrap.budgetLine.Unit_Cost_Y4__c * objwrap.budgetLine.Q14_Quantity__c * ExcRate);
                        objwrap.Q15Case = (objwrap.budgetLine.Unit_Cost_Y4__c * objwrap.budgetLine.Q15_Quantity__c * ExcRate);
                        objwrap.Q16Case = (objwrap.budgetLine.Unit_Cost_Y4__c * objwrap.budgetLine.Q16_Quantity__c * ExcRate);
                        
                    }else{
                        objwrap.Q1PSMCase = (objwrap.budgetLine.Unit_Cost_Y1__c * objwrap.budgetLine.Q1_Quantity__c * ExcRate);
                        objwrap.Q2PSMCase = (objwrap.budgetLine.Unit_Cost_Y1__c * objwrap.budgetLine.Q2_Quantity__c * ExcRate);
                        objwrap.Q3PSMCase = (objwrap.budgetLine.Unit_Cost_Y1__c * objwrap.budgetLine.Q3_Quantity__c * ExcRate);
                        objwrap.Q4PSMCase = (objwrap.budgetLine.Unit_Cost_Y1__c * objwrap.budgetLine.Q4_Quantity__c * ExcRate);
                        objwrap.Q5PSMCase = (objwrap.budgetLine.Unit_Cost_Y2__c * objwrap.budgetLine.Q5_Quantity__c * ExcRate);
                        objwrap.Q6PSMCase = (objwrap.budgetLine.Unit_Cost_Y2__c * objwrap.budgetLine.Q6_Quantity__c * ExcRate);
                        objwrap.Q7PSMCase = (objwrap.budgetLine.Unit_Cost_Y2__c * objwrap.budgetLine.Q7_Quantity__c * ExcRate);
                        objwrap.Q8PSMCase = (objwrap.budgetLine.Unit_Cost_Y2__c * objwrap.budgetLine.Q8_Quantity__c * ExcRate);
                        objwrap.Q9PSMCase = (objwrap.budgetLine.Unit_Cost_Y3__c * objwrap.budgetLine.Q9_Quantity__c * ExcRate);
                        objwrap.Q10PSMCase = (objwrap.budgetLine.Unit_Cost_Y3__c * objwrap.budgetLine.Q10_Quantity__c * ExcRate);
                        objwrap.Q11PSMCase = (objwrap.budgetLine.Unit_Cost_Y3__c * objwrap.budgetLine.Q11_Quantity__c * ExcRate);
                        objwrap.Q12PSMCase = (objwrap.budgetLine.Unit_Cost_Y3__c * objwrap.budgetLine.Q12_Quantity__c * ExcRate);
                        objwrap.Q13PSMCase = (objwrap.budgetLine.Unit_Cost_Y4__c * objwrap.budgetLine.Q13_Quantity__c * ExcRate);
                        objwrap.Q14PSMCase = (objwrap.budgetLine.Unit_Cost_Y4__c * objwrap.budgetLine.Q14_Quantity__c * ExcRate);
                        objwrap.Q15PSMCase = (objwrap.budgetLine.Unit_Cost_Y4__c * objwrap.budgetLine.Q15_Quantity__c * ExcRate);
                        objwrap.Q16PSMCase = (objwrap.budgetLine.Unit_Cost_Y4__c * objwrap.budgetLine.Q16_Quantity__c * ExcRate);        
                    }
                    /*Caculation of Total values and Outflow of total values*/
                    objwrap.TotalQuantityYear1 = objwrap.budgetLine.Q1_Quantity__c + objwrap.budgetLine.Q2_Quantity__c + objwrap.budgetLine.Q3_Quantity__c + objwrap.budgetLine.Q4_Quantity__c;
                    objwrap.TotalQuantityYear2 = objwrap.budgetLine.Q5_Quantity__c + objwrap.budgetLine.Q6_Quantity__c + objwrap.budgetLine.Q7_Quantity__c + objwrap.budgetLine.Q8_Quantity__c;
                    objwrap.TotalQuantityYear3 = objwrap.budgetLine.Q9_Quantity__c + objwrap.budgetLine.Q10_Quantity__c + objwrap.budgetLine.Q11_Quantity__c + objwrap.budgetLine.Q12_Quantity__c;
                    objwrap.TotalQuantityYear4 = objwrap.budgetLine.Q13_Quantity__c + objwrap.budgetLine.Q14_Quantity__c + objwrap.budgetLine.Q15_Quantity__c + objwrap.budgetLine.Q16_Quantity__c;
                    objwrap.TotalCaseYear1 = objwrap.Q1Case + objwrap.Q2Case + objwrap.Q3Case + objwrap.Q4Case;
                    objwrap.TotalCaseYear2 = objwrap.Q5Case + objwrap.Q6Case + objwrap.Q7Case + objwrap.Q8Case;
                    objwrap.TotalCaseYear3 = objwrap.Q9Case + objwrap.Q10Case + objwrap.Q11Case + objwrap.Q12Case;
                    objwrap.TotalCaseYear4 = objwrap.Q13Case + objwrap.Q14Case + objwrap.Q15Case + objwrap.Q16Case;
                    objwrap.GrantTotalQuantity = objwrap.TotalQuantityYear1 + objwrap.TotalQuantityYear2 + objwrap.TotalQuantityYear3 + objwrap.TotalQuantityYear4;
                    objwrap.GrantTotalAmount = objwrap.TotalCaseYear1+ objwrap.TotalCaseYear2+ objwrap.TotalCaseYear3+ objwrap.TotalCaseYear4;
                    
                    Tempsum = Tempsum+ objwrap.Q1Case;
                    Tempsum = Tempsum+ objwrap.Q2Case;
                    Tempsum = Tempsum+ objwrap.Q3Case;
                    Tempsum = Tempsum+ objwrap.Q4Case;
                    Tempsum = Tempsum+ objwrap.Q5Case;
                    Tempsum = Tempsum+ objwrap.Q6Case;
                    Tempsum = Tempsum+ objwrap.Q7Case;
                    Tempsum = Tempsum+ objwrap.Q8Case;
                    Tempsum = Tempsum+ objwrap.Q9Case;
                    Tempsum = Tempsum+ objwrap.Q10Case;
                    Tempsum = Tempsum+ objwrap.Q11Case;
                    Tempsum = Tempsum+ objwrap.Q12Case;
                    Tempsum = Tempsum+ objwrap.Q13Case;
                    Tempsum = Tempsum+ objwrap.Q14Case;
                    Tempsum = Tempsum+ objwrap.Q15Case;
                    Tempsum = Tempsum+ objwrap.Q16Case;
                    System.debug('----Without PSM'+Tempsum );
                    dbToUpdate.Total_Budgeted_Amount__c = Tempsum; 
                    
                    TempPSMsum = TempPSMsum + objwrap.Q1PSMCase;
                    TempPSMsum = TempPSMsum + objwrap.Q2PSMCase;
                    TempPSMsum = TempPSMsum + objwrap.Q3PSMCase;
                    TempPSMsum = TempPSMsum + objwrap.Q4PSMCase;
                    TempPSMsum = TempPSMsum + objwrap.Q5PSMCase;
                    TempPSMsum = TempPSMsum + objwrap.Q6PSMCase;
                    TempPSMsum = TempPSMsum + objwrap.Q7PSMCase;
                    TempPSMsum = TempPSMsum + objwrap.Q8PSMCase;
                    TempPSMsum = TempPSMsum + objwrap.Q9PSMCase;
                    TempPSMsum = TempPSMsum + objwrap.Q10PSMCase;
                    TempPSMsum = TempPSMsum + objwrap.Q11PSMCase;
                    TempPSMsum = TempPSMsum + objwrap.Q12PSMCase;
                    TempPSMsum = TempPSMsum + objwrap.Q13PSMCase;
                    TempPSMsum = TempPSMsum + objwrap.Q14PSMCase;
                    TempPSMsum = TempPSMsum + objwrap.Q15PSMCase;
                    TempPSMsum = TempPSMsum + objwrap.Q16PSMCase;
                    
                   
                    
                    
                if(objwrap.budgetLine.Implementation_Period__r.Currency_of_Grant_Agreement__c=='USD') { 
                if(objwrap.budgetLine.Implementation_Period__r.High_level_budget_GAC_1_USD__c != null)
                 dbToUpdate.Remaining_Budget_Amount__c = objwrap.budgetLine.Implementation_Period__r.High_level_budget_GAC_1_USD__c -  dbToUpdate.Total_Budgeted_Amount__c;
                }else{
                if(objwrap.budgetLine.Implementation_Period__r.High_level_budget_GAC_1_EUR__c != null)
                 dbToUpdate.Remaining_Budget_Amount__c = objwrap.budgetLine.Implementation_Period__r.High_level_budget_GAC_1_EUR__c - dbToUpdate.Total_Budgeted_Amount__c;
                }            
                if(objwrap.budgetLine.Detailed_Budget_Framework__r.HPC_Amount__c !=NULL){
                  dbToUpdate.Remaining_Budget_Amount__c = dbToUpdate.Remaining_Budget_Amount__c - dbToUpdate.HPC_Amount__c;  
                }
                ListBudLineToUpdate.add(objwrap.budgetLine);
                //lstDB.add(objwrap.budgetLine.Detailed_Budget_Framework__c);
                system.debug('UPdated budget line '+ListBudLineToUpdate);
            }
            System.debug('----With PSM'+TempPSMsum +'===='+dbToUpdate.Total_Budgeted_Amount__c); 
            TempPSMsum = TempPSMsum  + Tempsum;
            dbToUpdate.Total_amount_budgeted__c = TempPSMsum;
            try{
                update ListBudLineToUpdate;
                update dbToUpdate;
            }catch(exception e){
                system.debug('Exception while updating ----'+e);
                
            }
        }        
    }
    
    Public class budgetLineWrap{
        Budget_Line__c budgetLine ;
        Public Decimal Q1Case=0 ;
        Public Decimal Q2Case=0 ;
        Public Decimal Q3Case=0 ;
        Public Decimal Q4Case=0 ;
        Public Decimal Q5Case=0 ;
        Public Decimal Q6Case=0 ;
        Public Decimal Q7Case=0 ;
        Public Decimal Q8Case=0 ;
        Public Decimal Q9Case=0 ;
        Public Decimal Q10Case=0 ;
        Public Decimal Q11Case=0 ;
        Public Decimal Q12Case=0 ;
        Public Decimal Q13Case=0 ;
        Public Decimal Q14Case=0 ;
        Public Decimal Q15Case=0 ;
        Public Decimal Q16Case=0 ;
        //for PSM  
        Public Decimal Q1PSMCase=0 ;
        Public Decimal Q2PSMCase=0 ;
        Public Decimal Q3PSMCase=0;
        Public Decimal Q4PSMCase=0 ;
        Public Decimal Q5PSMCase =0;
        Public Decimal Q6PSMCase=0 ;
        Public Decimal Q7PSMCase =0;
        Public Decimal Q8PSMCase =0;
        Public Decimal Q9PSMCase =0;
        Public Decimal Q10PSMCase=0 ;
        Public Decimal Q11PSMCase=0 ;
        Public Decimal Q12PSMCase=0 ;
        Public Decimal Q13PSMCase=0 ;
        Public Decimal Q14PSMCase=0 ;
        Public Decimal Q15PSMCase=0 ;
        Public Decimal Q16PSMCase=0 ;
        Public Decimal TotalQuantityYear1=0;
        Public Decimal TotalQuantityYear2=0;
        Public Decimal TotalQuantityYear3=0;
        Public Decimal TotalQuantityYear4=0;
        Public Decimal TotalCaseYear1=0;
        Public Decimal TotalCaseYear2=0;
        Public Decimal TotalCaseYear3=0;
        Public Decimal TotalCaseYear4=0;
        Public Decimal GrantTotalQuantity=0;
        Public Decimal GrantTotalAmount=0;
        }
}