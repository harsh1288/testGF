public class ctrlDetailPageLink{ 
    public String obj;
    public List<Module__c> ModList {get;set;}
    IP_Detail_Information__c IpDetailInfo {get;set;}
    public Boolean blnExternalPro  {get;set;}
    public id IP {get;set;}
    
    public ctrlDetailPageLink(ApexPages.StandardController controller){    
        //ModList = [Select id,Name From Module__c where Implementation_Period__c=: Apexpages.currentpage().getparameters().get('Id')];
        IpDetailInfo =[Select id,Budget_Status__c,Implementation_Period__c from IP_Detail_Information__c where id=: Apexpages.currentpage().getparameters().get('Id')];
        Performance_Framework__c PF = [Select id from Performance_Framework__c where Implementation_Period__c=: IpDetailInfo.Implementation_Period__c ];
        IP = IpDetailInfo.Implementation_Period__c ;
        ModList = [Select id,Name From Module__c where Implementation_Period__c=: IpDetailInfo.Implementation_Period__c AND Performance_Framework__c=: PF.id];
        checkProfile();
    }      
    
    public void checkProfile(){
        Id profileId=userinfo.getProfileId();
         String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
     
        List<Profile_Access_Setting__c> checkpage = [Select Salesforce_Item__c,Status__c from Profile_Access_Setting__c where Page_Name__c ='DetailPageLink' and Profile_Name__c =: profilename];
        system.debug(checkpage);
        for (Profile_Access_Setting__c check : checkpage){
            if (check.Salesforce_Item__c == 'External Profile') blnExternalPro = true;
    } 
    }
}