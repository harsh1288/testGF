@isTest 

Public Class Test_PerformanceEvalutionHandler
{

    Public static testMethod void myTest1()
    {
        ///*
        Account objAcc = new Account();
        objAcc.Name = 'Test Acc';
        objAcc.Short_Name__c= 'Test Acc';
        //RecordType.Name = 
        insert objAcc;
        
        Contact objcnct = new Contact();
        objcnct.AccountId = objAcc.id;
        objcnct.LastName = 'test';
        insert objcnct;
        
        Country__c objCountry = new Country__c();
        objCountry.Name = 'Test Country';
        objCountry.Region_Name__c = 'Test Region';
        objCountry.Used_for_LFA_Services__c = TRUE;
        insert objCountry;
        
        LFA_Work_Plan__c objWp = new LFA_Work_Plan__c();
        objWp.Name = 'Test Wp';
        objWp.LFA__c = objAcc.Id;
        objWp.Country__c = objCountry.id;
        objWP.Include_In_Library__c = TRUE;
        insert objWp;
        
        LFA_Service__c objService = new LFA_Service__c();
        objService.Name = 'PUDR';
        objService.LFA_Work_Plan__c = objWp.id;
        objService.Service_Type__c = 'Key Services';
        objService.Service_Sub_Type__c = 'PUDR';
        objService.Exclude_from_Library__c=FALSE;        
        insert objService;
        
        LFA_Resource__c objResource = new LFA_Resource__c();
        //objResource.Name = 'Test Resource';
        objResource.LFA_Service__c = objService.id;
        objResource.Rate__c = 1;
        //objResource.LFA_Role__c = objRole1.Id;
        objResource.LOE__c = 1;
        objResource.CT_Planned_Cost__c = 1;
        objResource.CT_Planned_LOE__c = 1;
        objResource.TGF_Proposed_Cost__c = 1;
        objResource.TGF_Proposed_LOE__c = 1;
        insert objResource;
        
        LFA_Service__c objService1 = new LFA_Service__c();
        objService1.Name = 'PUDR';
        objService1.LFA_Work_Plan__c = objWp.id;
        objService1.Service_Type__c = 'Key Services';
        objService1.Service_Sub_Type__c = 'PUDR';
        insert objService1;
        
        LFA_Resource__c objResource1 = new LFA_Resource__c();
        //objResource1.Name = 'Test Resource1';
        objResource1.LFA_Service__c = objService1.id;
        objResource1.Rate__c = 2;
        objResource1.LOE__c = 2;
        objResource1.CT_Planned_Cost__c = 2;
        objResource1.CT_Planned_LOE__c = 2;
        objResource1.TGF_Proposed_Cost__c = 2;
        objResource1.TGF_Proposed_LOE__c = 2;
        insert objResource1;
        
        LFA_Service__c objService2 = new LFA_Service__c();
        objService2.Name = 'PUDR';
        objService2.LFA_Work_Plan__c = objWp.id;
        objService2.Service_Type__c = 'Other LFA Services';
        objService2.Service_Sub_Type__c = 'PUDR';
        insert objService2;
        
        LFA_Role__c objRole = new LFA_Role__c();
        objRole.Name = 'Test Role';
        insert objRole;
        
        LFA_Resource__c objResource2 = new LFA_Resource__c();
        //objResource2.Name = 'Test Resource1';
        objResource2.LFA_Service__c = objService2.id;
        objResource2.LFA_Role__c = objRole.id;
        objResource2.Rate__c = 2;
        objResource2.LOE__c = 2;
        objResource2.CT_Planned_Cost__c = 2;
        objResource2.CT_Planned_LOE__c = 2;
        objResource2.TGF_Proposed_Cost__c = 2;
        objResource2.TGF_Proposed_LOE__c = 2;
        insert objResource2;
        
        Contact objCon = new Contact();
        objCon.LastName = 'Test LName';
        objCon.AccountId = objAcc.Id;
        insert objCon;
        
        Rate__c objRate = new Rate__c();
        objRate.LFA_Role__c = objRole.Id;
        objRate.Contact__c = objCon.Id;
        objRate.Country__c = objCountry.Id;
        objRate.Rate__c = 1;
        objRate.Active__c = true;
        insert objRate;
        
        Grant__c objGrant = new Grant__c();
        objGrant.Name = 'Test Grant';
        objGrant.Country__c = objCountry.id;
        objGrant.Principal_Recipient__c = objAcc.id;
        objGrant.Financial_and_Fiduciary_Risks__c = 'Low';
        objGrant.Health_Services_and_Products_Risks__c = 'High';
        objGrant.Programmatic_and_Performance_Risks__c = 'Medium';
        insert objGrant;
        
        LFA_Service__c objService3 = new LFA_Service__c();
        objService3.Name = 'PUDR';
        objService3.LFA_Work_Plan__c = objWp.id;
        objService3.Service_Type__c = 'Key Services';
        objService3.Service_Sub_Type__c = 'PUDR';
        insert objService3;
        
        LFA_Role__c objRole1 = new LFA_Role__c();
        objRole1.Name = 'Test Role1';
        insert objRole1;
        
        LFA_Resource__c objResource3 = new LFA_Resource__c();
        //objResource.Name = 'Test Resource';
        objResource3.LFA_Service__c = objService3.id;
        objResource3.Rate__c = 1;
        objResource3.LFA_Role__c = objRole1.Id;
        objResource3.LOE__c = 1;
        objResource3.CT_Planned_Cost__c = 1;
        objResource3.CT_Planned_LOE__c = 1;
        objResource3.TGF_Proposed_Cost__c = 1;
        objResource3.TGF_Proposed_LOE__c = 1;
        insert objResource3;
        
        Rate__c objRate1 = new Rate__c();
        objRate1.LFA_Role__c = objRole1.Id;
        objRate1.Contact__c = objCon.Id;
        objRate1.Country__c = objCountry.Id;
        objRate1.Rate__c = 1;
        objRate1.Active__c = true;
        insert objRate1; 
       //*/
       Performance_Evaluation__c objPE = new Performance_Evaluation__c();
       objPE.Name = 'PE';
       //objPE.Start_Date__c = system.today();
       objPE.Start_Date__c = system.today()+1;
       objPE.LFA_Work_Plan__c = objWp.id;
       //objPE.LFA_Work_Plan__c = 'a0Qg0000000dAuy';
       objPE.begin_alerts__c =true;
       objPE.LFA_Contact_1__c = objcnct.id;
       insert objPE; 
       
       List<Performance_Evaluation__c> lstPEC = new List<Performance_Evaluation__c>();
       lstPEC.add(objPE);
       
       objPE.Status__c ='Begin Alerts';
       update objPE;
       
        /*Contact objContact= new Contact();
        //objContact.Receive_PET_Alerts__c = true;
        objContact.LastName = 'TestLFA';
        insert objContact;   */
        
          PET_Access__c objPETAccess =new PET_Access__c();
         objPETAccess.LFA_Work_Plan__c = objWp.Id;
        //objPETAccess.LFA_Work_Plan__c ='a0Qg0000000dAuy';
        objPETAccess.PET_Access__c = 'Read';
        objPETAccess.Receive_PET_Alerts__c = true;
        //objPETAccess.User__c =;
        insert objPETAccess ;
        
        
        PET_Response__c objPETResponse = new PET_Response__c();
        //objPETResponse.Name='aaa';
        objPETResponse.Performance_Evaluation__c=objPE .id;
        objPETResponse.Type__c = 'LFA PSM';
        objPETResponse.pet_status__c ='Begin Alerts';
        insert objPETResponse ;
        
        List<PET_Response__c> lstPETResponse = new List<PET_Response__c>();
        lstPETResponse.add(objPETResponse);
        
        Performance_Evaluation__c perf2 = new Performance_Evaluation__c();  
        perf2.Name = 'PE_2';
        perf2.Start_Date__c = system.today()+1;
        perf2.LFA_Work_Plan__c = objWp.id;
        perf2.begin_alerts__c =true;
        perf2.LFA_Contact_1__c = objcnct.id;  
        perf2.Status__c='Finished';
        insert perf2;
               
        
        //Map<Id,Performance_Evaluation__c> mpPerf = new Map<Id,Performance_Evaluation__c>();
        //mpPerf.put(perf2.Id, perf2);
        
        List<PET_Access__c> lstPETAccess = new List<PET_Access__c>();
        lstPETAccess.add(objPETAccess);
        
        /*Performance_Evaluation__Share Share = new Performance_Evaluation__share();
        Share.ParentId = objPE.Id;
        //Share.UserOrGroupId = objPETAccess.User__c; 
        Share.AccessLevel = 'read';
        insert Share ;*/
        
        PerformanceEvalutionHandler.aiCreatePETResponse(lstPEC);
        PerformanceEvalutionHandler.auUpdatePetResponse(lstPETResponse);
        //PerformanceEvalutionHandler.auUpdatePETandResponse(lstPEC,mpPerf);
        //PerformanceEvalutionHandler.auUpdatePETandResponseStatus(lstPEC,mpPerf);
        
    }
    Public static testMethod void myTest2(){
      
        Country__c cntry = new Country__c();
        cntry.Name='India';
        cntry.CT_Public_Group_ID__c ='00Gb0000000tjtJ';
        insert cntry;   
                
        LFA_Work_Plan__c lfwp = new LFA_Work_Plan__c();
        lfwp.Name = 'Test';
        lfwp.Country__c = cntry.id; 
        insert lfwp;
        
        PET_Access__c petAcc = new PET_Access__c();
        petAcc.LFA_Work_Plan__c = lfwp.Id;
        insert petAcc;
        
        LFA_Service__c lfsv = new LFA_Service__c();
        lfsv.Name='Test1';
        lfsv.LFA_Work_Plan__c = lfwp.Id;
        lfsv.Service_Type__c = 'Key Services';
        insert lfsv;
        
        LFA_Resource__c lfr = new LFA_Resource__c();
        lfr.LFA_Service__c = lfsv.id;
        insert lfr;
        
    
        Performance_Evaluation__c perf = new Performance_Evaluation__c();        
        perf.LFA_Work_Plan__c = lfwp.Id;
        perf.Status__c='Finished';
        insert perf;
        
        
        Service_PET_Jxn__c petjxn = new Service_PET_Jxn__c();
        petjxn.LFA_Service__c = lfsv.id;
        petjxn.Performance_Evaluation__c = perf.id;
        insert petjxn;
        
        PET_Response__c petres1 = new PET_Response__c();
        petres1.name='test';
        petres1.Performance_Evaluation__c = perf.Id;
        petres1.Status__c='Completed';
        petres1.Rating__c = '3';
        insert petres1; 
        
        perf.Status__c='Begin Alerts';
        update perf;
        
        Performance_Evaluation__c perf2 = new Performance_Evaluation__c();    
        perf2.Status__c='Finished';
        insert perf2;
               
        
        Map<Id,Performance_Evaluation__c> mpPerf = new Map<Id,Performance_Evaluation__c>();
        mpPerf.put(perf2.Id, perf2);
        
        perf2.Status__c='Begin Alerts';
        update perf2;
        
        PET_Response__c petres2 = new PET_Response__c();
        petres2.name='test';
        petres2.Performance_Evaluation__c = perf2.Id;
        petres2.Status__c='Completed';
        petres2.Rating__c = '3';
        insert petres2; 
        
        Performance_Evaluation__c perf3 = new Performance_Evaluation__c();             
        insert perf3;
    
        
        //lstPerf.add(perf2);   
        
        
        
        List<Performance_Evaluation__c> lstPerf = new List<Performance_Evaluation__c>();
        lstPerf.add(perf2);
        
                
        List<PET_Response__c> lstPeResp = new List<PET_Response__c>();
        lstPeResp.add(petres1);
        lstPeResp.add(petres2);
        

        
        ApexPages.StandardController ctrl = new ApexPages.StandardController(perf);
        PerformanceEvalutionHandler extController = new PerformanceEvalutionHandler();
        
        PerformanceEvalutionHandler.aiCreatePETResponse(lstPerf);
        PerformanceEvalutionHandler.auUpdatePETandResponse(lstPerf,mpPerf);
        PerformanceEvalutionHandler.auUpdatePETandResponseStatus(lstPerf,mpPerf);
        PerformanceEvalutionHandler.auUpdatePetResponse(lstPeResp);        
        
    
    }



}