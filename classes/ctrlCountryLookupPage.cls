/*********************************************************************************
* {Controller} Class: {ctrlCountryLookupPage}
*  DateCreated : 01/22/2014
----------------------------------------------------------------------------------
* Purpose/Methods:
* - Used for open country lookup in Team Structure Page.
* 
* Unit Test: TestCtrlLookupPage
----------------------------------------------------------------------------------
* History:
* - VERSION  DEVELOPER NAME    DATE            DETAIL FEATURES
     1.0                      01/22/2014      INITIAL DEVELOPMENT
*********************************************************************************/

public with sharing class ctrlCountryLookupPage{

    transient public List<CustomLookup> lstCustomlookup{get;set;}    
    public string searchString{get;set;} // search keyword
    Public string strCountryIds;
    Public string strAccId;
    Public string strParentAccountId;
    Public  String strWorkPlanId;
    Public Set<Id> setCoutnryID {get; set;}
    transient public List<sobject> results; // search results
    

    /********************************************************************
        Constructor
    ********************************************************************/
    public ctrlCountryLookupPage() {
        lstCustomlookup = new List<Customlookup>();
        strAccId = System.currentPageReference().getParameters().get('idfield');
        strParentAccountId = System.currentPageReference().getParameters().get('ParentId');
        strWorkPlanId = System.currentPageReference().getParameters().get('WpId');
        strCountryIds  = System.currentPageReference().getParameters().get('CountryID');
        setCoutnryID = new Set<Id>();
        String[] ConIDs = strCountryIDs.Split(',');
        for(String objStr : ConIDs){
            setCoutnryID.add(objStr);
        }
        system.debug('@@@setCoutnryID: '+setCoutnryID);
        //searchString = System.currentPageReference().getParameters().get('lksrch');    
        runSearch();  
    }
    /********************************************************************
        Name : search
        Purpose : Performs the keyword search
    ********************************************************************/ 
  
    public PageReference search() {
        lstCustomlookup = new List<Customlookup>();
        runSearch();
        return null;
    }
    
    /********************************************************************
        Name : runSearch
        Purpose : Prepare the query and issue the search command
    ********************************************************************/
    
    private void runSearch() {
        results = performSearch(searchString);     
    } 
    
    /********************************************************************
        Name : performSearch
        Purpose : Run the search and return the records found
    ********************************************************************/  
  
    private List<sobject> performSearch(string searchString) {
        String soql = '';
        soql = 'select id,Name From Country__c Where Id NOT IN : setCoutnryID AND (LFA__c =:strAccId OR LFA__c =:strParentAccountId OR (LFA__r.ParentId != null AND LFA__r.ParentId =:strParentAccountId))';  
        
        if(searchString != null && searchString != ''){
            searchString += '%';
            soql = 'select id,Name From Country__c Where Name Like : searchString AND Id NOT IN : setCoutnryID AND (LFA__c =:strAccId OR LFA__c =:strParentAccountId OR (LFA__r.ParentId != null AND LFA__r.ParentId =:strParentAccountId))';
        }  
             
        results = database.query(soql); 
        
        CustomLookup objlookup = new CustomLookup();
        for(sobject s:results){
            objlookup = new CustomLookup();
            objlookup.Id = s.Id;
            objlookup.strCountryName =  string.valueOf(s.get('Name'));
            lstCustomlookup.add(objlookup);        
        }   
        return null; 
    } 

    /********************************************************************
        Name : getFormTag
        Purpose : Used by the visualforce page to send the link to the right dom element
    ********************************************************************/  
  
    public string getFormTag() {
        return System.currentPageReference().getParameters().get('frm');
    }
    
    /********************************************************************
        Name : getTextBox
        Purpose : Used by the visualforce page to send the link to the right dom element for the text box
    ********************************************************************/  
  
    public string getTextBox() {
        return System.currentPageReference().getParameters().get('txt');
    }
    
    /********************************************************************
        Wrapper Class : Customlookup
    ********************************************************************/
    
    public class Customlookup{
        Public Id Id{get;set;}
        Public String strCountryName{get;set;}
    }
}