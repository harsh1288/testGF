@istest
public with sharing class testtrgDuplicateCustomGIName {
    
    
    public static testMethod void testupdateGIPwithUserDetail(){
       
           Account objacct = TestClassHelper.insertAccount();
          
          Grant__c objGrant =  TestClassHelper.createGrant(objacct);
          objGrant.Principal_Recipient__c = objacct.Id;
          
          Implementation_Period__c objimp = TestClassHelper.createIP(objGrant, objacct);
          objimp.Principal_Recipient__c = objacct.Id;
          objimp.Grant__c = objGrant.Id;
          insert objimp;
          
          Performance_Framework__c objpf = TestClassHelper.createPF(objimp);
          objpf.Implementation_Period__c = objimp.Id;
          insert objpf;
      
       Module__c ObjModule = TestClassHelper.createModule();
       insert ObjModule;
       
       Grant_Intervention__c objgint = new Grant_Intervention__c();
       objgint.Custom_Intervention_Name__c = 'test';
       objgint.Module__c = ObjModule.Id;
       objgint.Performance_Framework__c = objpf.id;
       objgint.Implementation_Period__c = objimp.Id;
       insert objgint;
       
       objgint.Custom_Intervention_Name__c = 'test intervention';
       update objgint;
       
       Grant_Intervention__c objgint1 = new Grant_Intervention__c();
       objgint1.Custom_Intervention_Name__c = 'test intervention';
       objgint1.Module__c = ObjModule.Id;
       objgint1.Performance_Framework__c = objpf.id;
       objgint1.Implementation_Period__c = objimp.Id;
       insert objgint1;
       
       delete objgint;
       
       
    }
}