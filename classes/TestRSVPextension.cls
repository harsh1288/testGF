@isTest
Public Class TestRSVPextension{
    Public static testMethod void TestRSVPextension(){
    
        campaign Camp = new campaign();
        Camp.name = 'Name Campaign';
        camp.Guests_Permitted__c = true;
        camp.Confirmation_Text_Accepted__c = 'test';
        camp.Confirmation_Text_Declined__c = 'test2';
        camp.maximum_Number_of_Guests_Permitted__c = 10;
        insert Camp;
        
        Account Acc = new Account();
        Acc.Name= 'Test Acc';
        insert Acc;
        
        Contact c = new Contact();
        c.LastName = 'Mishra';
        c.Email = 'Test@abc.com';
        c.Accountid = Acc.id;
        insert c;
        
        CampaignMember CampMember =  new CampaignMember();
        CampMember.Status = 'Sent';
        CampMember.contactid = c.id;
        CampMember.Campaignid = camp.id;
        insert CampMember;
        
        ApexPages.currentPage().getParameters().put('Id',Camp.id);
        ApexPages.currentPage().getParameters().put('contactid',c.id);
        RSVPextension objCampaign = new RSVPextension();
        
        test.startTest();
            objCampaign.getCampaignMember();
            objCampaign.getResponses();
            objCampaign.getAnyGuests();
            objCampaign.getMaxNumberOfGuests();
            objCampaign.strResponse = 'Declined';
            objCampaign.SaveNew();
            objCampaign.strResponse = 'Accepted';
            objCampaign.strAnyGuests = 'Yes';
            objCampaign.showMember();
            objCampaign.totalmembers();
            objCampaign.SaveNew();
            
        test.stopTest();
    }
    
}