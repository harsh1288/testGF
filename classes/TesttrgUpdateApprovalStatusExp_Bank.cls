@isTest
public class TesttrgUpdateApprovalStatusExp_Bank {
    public static testMethod void TesttrgUpdateApprovalStatusExp_Bank(){
        Bank__c objBank = TestClassHelper.createBank();
        insert objBank;
        
        Account objAcct = TestClassHelper.createAccount();
        insert objAcct;
        
        Bank_Account__c objBankAcc = TestClassHelper.createBankAcc();
        objBankAcc.Account__c = objAcct.id;
        insert objBankAcc;
        
        objBankAcc.SF_Approval_Status_Change__c = true;
        objBankAcc.Locked__c = false;
        update objBankAcc;
        
    }
    
 }