@isTest
Public class TestctrlChangeRequestReview{
    Public static testMethod Void TestctrlChangeRequestReview(){
    
        User objUser = [Select ID,name from User where id=:userinfo.getUserId()];
        
        Country__c objCountry = new Country__c();
        objCountry.Name = 'TestCountry';
        insert objCountry;
        
        LFA_Role__c objLFARole = new LFA_Role__c();
        objLFARole.Name = 'TestLFARole';
        insert objLFARole;
        
        Rate__c objRate = new Rate__c();
        objRate.Country__c = objCountry.id;
        objRate.LFA_Role__c = objLFARole.id;
        objRate.Rate__c = 200;
        insert objRate;
        
        Change_Request__c objChangeRequest = new Change_Request__c();
        objChangeRequest.Rate__c =objRate.id;
        insert objChangeRequest;
        
        Change_Request_Review__c objChangeRequestReview = new Change_Request_Review__c();
        objChangeRequestReview.Change_Request__c = objChangeRequest.id;
        objChangeRequestReview.Type__c = 'Country Team';
        objChangeRequestReview.User__c = objUser.id;
        insert objChangeRequestReview;
        
        Change_Request_Review__c objChangeRequestReview1 = new Change_Request_Review__c();
        objChangeRequestReview1.Change_Request__c = objChangeRequest.id;
        objChangeRequestReview1.Type__c = 'Country Team';
        objChangeRequestReview1.User__c = objUser.id;
        insert objChangeRequestReview1;

        System.currentPageReference().getParameters().put('id',objChangeRequest.id);
        
        ApexPages.StandardController objStandardController = new ApexPages.StandardController(objChangeRequest);
        ctrlChangeRequestReview objctrlChangeRequestReview = new ctrlChangeRequestReview(objStandardController);
        
        Apexpages.currentpage().getparameters().put('Name','Purchasing');
        objctrlChangeRequestReview.SaveUsers();
        Apexpages.currentpage().getparameters().put('Name','Purchasing');
        objctrlChangeRequestReview.ShowPopup();
        
        objctrlChangeRequestReview.SaveUsers();
        objctrlChangeRequestReview.SaveAndSend();
        objctrlChangeRequestReview.CancelCRR();
        objctrlChangeRequestReview.HidePopup();
        objctrlChangeRequestReview.strPopupName = 'PSM';
        objChangeRequestReview.Type__c = 'PSM';
        update objChangeRequestReview;
        objctrlChangeRequestReview.FillPSMUsers();
        
        objctrlChangeRequestReview.SaveUsers();
        objctrlChangeRequestReview.SaveAndSend();
        objctrlChangeRequestReview.CancelCRR();
        objctrlChangeRequestReview.HidePopup();
        
        Apexpages.currentpage().getparameters().put('PName','Purchasing');
        objctrlChangeRequestReview.SendReminder();
        objctrlChangeRequestReview.LastRemDate();
    }
    
    Public static testMethod Void TestctrlChangeRequestReview1(){
    
        User objUser = [Select ID,name from User where id=:userinfo.getUserId()];
        
        Country__c objCountry = new Country__c();
        objCountry.Name = 'TestCountry';
        insert objCountry;
        
        LFA_Role__c objLFARole = new LFA_Role__c();
        objLFARole.Name = 'TestLFARole';
        insert objLFARole;
        
        Rate__c objRate = new Rate__c();
        objRate.Country__c = objCountry.id;
        objRate.LFA_Role__c = objLFARole.id;
        objRate.Rate__c = 200;
        insert objRate;
        
        Change_Request__c objChangeRequest = new Change_Request__c();
        objChangeRequest.Rate__c =objRate.id;
        insert objChangeRequest;
        
        Change_Request_Review__c objChangeRequestReview = new Change_Request_Review__c();
        objChangeRequestReview.Change_Request__c = objChangeRequest.id;
        objChangeRequestReview.Type__c = 'Country Team';
        objChangeRequestReview.User__c = objUser.id;
        insert objChangeRequestReview;
        
        Change_Request_Review__c objChangeRequestReview1 = new Change_Request_Review__c();
        objChangeRequestReview1.Change_Request__c = objChangeRequest.id;
        objChangeRequestReview1.Type__c = 'Country Team';
        //objChangeRequestReview1.User__c = objUser.id;
        insert objChangeRequestReview1;
        
        System.currentPageReference().getParameters().put('id',objChangeRequest.id);
        
        ApexPages.StandardController objStandardController = new ApexPages.StandardController(objChangeRequest);
        ctrlChangeRequestReview objctrlChangeRequestReview = new ctrlChangeRequestReview(objStandardController);
            
        objctrlChangeRequestReview.FillCountryTeamUsers();
        Apexpages.currentpage().getparameters().put('Name','Country Team');
        objctrlChangeRequestReview.ShowPopup();
        
        objctrlChangeRequestReview.strPopupName = 'Country Team';
        objChangeRequestReview.Type__c = 'Country Team';
        update objChangeRequestReview;
        objctrlChangeRequestReview.FillPSMUsers();
        
        objctrlChangeRequestReview.SaveUsers();
        objctrlChangeRequestReview.SaveAndSend();
        objctrlChangeRequestReview.CancelCRR();
        objctrlChangeRequestReview.HidePopup();
    
    }
    
    Public static testMethod Void TestctrlChangeRequestReview2(){
    
        User objUser = [Select ID,name from User where id=:userinfo.getUserId()];
        
        Country__c objCountry = new Country__c();
        objCountry.Name = 'TestCountry';
        insert objCountry;
        
        LFA_Role__c objLFARole = new LFA_Role__c();
        objLFARole.Name = 'TestLFARole';
        insert objLFARole;
        
        Rate__c objRate = new Rate__c();
        objRate.Country__c = objCountry.id;
        objRate.LFA_Role__c = objLFARole.id;
        objRate.Rate__c = 200;
        insert objRate;
        
        Change_Request__c objChangeRequest = new Change_Request__c();
        objChangeRequest.Rate__c =objRate.id;
        insert objChangeRequest;
        
        Change_Request_Review__c objChangeRequestReview = new Change_Request_Review__c();
        objChangeRequestReview.Change_Request__c = objChangeRequest.id;
        objChangeRequestReview.Type__c = 'Finance';
        objChangeRequestReview.User__c = objUser.id;
        insert objChangeRequestReview;
        
        Change_Request_Review__c objChangeRequestReview1 = new Change_Request_Review__c();
        objChangeRequestReview1.Change_Request__c = objChangeRequest.id;
        objChangeRequestReview1.Type__c = 'Finance';
        objChangeRequestReview1.User__c = objUser.id;
        insert objChangeRequestReview1;
        
        System.currentPageReference().getParameters().put('id',objChangeRequest.id);
        
        ApexPages.StandardController objStandardController = new ApexPages.StandardController(objChangeRequest);
        ctrlChangeRequestReview objctrlChangeRequestReview = new ctrlChangeRequestReview(objStandardController);
        
        objctrlChangeRequestReview.FillPurchasingUsers();
        Apexpages.currentpage().getparameters().put('Name','Finance');
        objctrlChangeRequestReview.ShowPopup();
        
        objctrlChangeRequestReview.strPopupName = 'Finance';
        objChangeRequestReview.Type__c = 'Finance';
        update objChangeRequestReview;

        objctrlChangeRequestReview.FillPSMUsers();
        
        objctrlChangeRequestReview.SaveUsers();
        objctrlChangeRequestReview.SaveAndSend();
        objctrlChangeRequestReview.CancelCRR();
        objctrlChangeRequestReview.HidePopup();
    
    }
    
    Public static testMethod Void TestctrlChangeRequestReview3(){
    
        User objUser = [Select ID,name from User where id=:userinfo.getUserId()];
        
        Country__c objCountry = new Country__c();
        objCountry.Name = 'TestCountry';
        insert objCountry;
        
        LFA_Role__c objLFARole = new LFA_Role__c();
        objLFARole.Name = 'TestLFARole';
        insert objLFARole;
        
        Rate__c objRate = new Rate__c();
        objRate.Country__c = objCountry.id;
        objRate.LFA_Role__c = objLFARole.id;
        objRate.Rate__c = 200;
        insert objRate;
        
        Change_Request__c objChangeRequest = new Change_Request__c();
        objChangeRequest.Rate__c =objRate.id;
        insert objChangeRequest;
        
        Change_Request_Review__c objChangeRequestReview = new Change_Request_Review__c();
        objChangeRequestReview.Change_Request__c = objChangeRequest.id;
        objChangeRequestReview.Type__c = 'M&E';
        objChangeRequestReview.User__c = objUser.id;
        insert objChangeRequestReview;
        
        Change_Request_Review__c objChangeRequestReview1 = new Change_Request_Review__c();
        objChangeRequestReview1.Change_Request__c = objChangeRequest.id;
        objChangeRequestReview1.Type__c = 'M&E';
        objChangeRequestReview1.User__c = objUser.id;
        insert objChangeRequestReview1;
        
        System.currentPageReference().getParameters().put('id',objChangeRequest.id);
        
        ApexPages.StandardController objStandardController = new ApexPages.StandardController(objChangeRequest);
        ctrlChangeRequestReview objctrlChangeRequestReview = new ctrlChangeRequestReview(objStandardController);
        
        objctrlChangeRequestReview.FillMandEUsers();
        Apexpages.currentpage().getparameters().put('Name','M&E');
        objctrlChangeRequestReview.ShowPopup();
        objctrlChangeRequestReview.SaveUsers();
        objctrlChangeRequestReview.SaveCRR();
        objctrlChangeRequestReview.SaveAndSend();
        objctrlChangeRequestReview.CancelCRR();
        objctrlChangeRequestReview.HidePopup();
        
    }
    
    Public static testMethod Void TestctrlChangeRequestReview4(){
    
        User objUser = [Select ID,name from User where id=:userinfo.getUserId()];
        
        Country__c objCountry = new Country__c();
        objCountry.Name = 'TestCountry';
        insert objCountry;
        
        LFA_Role__c objLFARole = new LFA_Role__c();
        objLFARole.Name = 'TestLFARole';
        insert objLFARole;
        
        Rate__c objRate = new Rate__c();
        objRate.Country__c = objCountry.id;
        objRate.LFA_Role__c = objLFARole.id;
        objRate.Rate__c = 200;
        insert objRate;
        
        Change_Request__c objChangeRequest = new Change_Request__c();
        objChangeRequest.Rate__c =objRate.id;
        insert objChangeRequest;
        
        Change_Request_Review__c objChangeRequestReview = new Change_Request_Review__c();
        objChangeRequestReview.Change_Request__c = objChangeRequest.id;
        objChangeRequestReview.Type__c = 'Purchasing';
        objChangeRequestReview.User__c = objUser.id;
        insert objChangeRequestReview;
        
        Change_Request_Review__c objChangeRequestReview1 = new Change_Request_Review__c();
        objChangeRequestReview1.Change_Request__c = objChangeRequest.id;
        objChangeRequestReview1.Type__c = 'Purchasing';
        objChangeRequestReview1.User__c = objUser.id;
        insert objChangeRequestReview1;
        
        System.currentPageReference().getParameters().put('id',objChangeRequest.id);
        
        ApexPages.StandardController objStandardController = new ApexPages.StandardController(objChangeRequest);
        ctrlChangeRequestReview objctrlChangeRequestReview = new ctrlChangeRequestReview(objStandardController);
        
        objctrlChangeRequestReview.FillPurchasingUsers();
        Apexpages.currentpage().getparameters().put('Name','Purchasing');
        objctrlChangeRequestReview.ShowPopup();
    
    }
    
    Public static testMethod Void TestctrlChangeRequestReview5(){
    
        User objUser = [Select ID,name from User where id=:userinfo.getUserId()];
        
        Country__c objCountry = new Country__c();
        objCountry.Name = 'TestCountry';
        insert objCountry;
        
        LFA_Role__c objLFARole = new LFA_Role__c();
        objLFARole.Name = 'TestLFARole';
        insert objLFARole;
        
        Rate__c objRate = new Rate__c();
        objRate.Country__c = objCountry.id;
        objRate.LFA_Role__c = objLFARole.id;
        objRate.Rate__c = 200;
        insert objRate;
        
        Change_Request__c objChangeRequest = new Change_Request__c();
        objChangeRequest.Rate__c =objRate.id;
        insert objChangeRequest;
        
        Change_Request_Review__c objChangeRequestReview = new Change_Request_Review__c();
        objChangeRequestReview.Change_Request__c = objChangeRequest.id;
        objChangeRequestReview.Type__c = 'PSM';
        objChangeRequestReview.User__c = objUser.id;
        insert objChangeRequestReview;
        
        Change_Request_Review__c objChangeRequestReview1 = new Change_Request_Review__c();
        objChangeRequestReview1.Change_Request__c = objChangeRequest.id;
        objChangeRequestReview1.Type__c = 'PSM';
        objChangeRequestReview1.User__c = objUser.id;
        insert objChangeRequestReview1;
        
        System.currentPageReference().getParameters().put('id',objChangeRequest.id);
        
        ApexPages.StandardController objStandardController = new ApexPages.StandardController(objChangeRequest);
        ctrlChangeRequestReview objctrlChangeRequestReview = new ctrlChangeRequestReview(objStandardController);
        
        objctrlChangeRequestReview.FillPurchasingUsers();
        Apexpages.currentpage().getparameters().put('Name','PSM');
        objctrlChangeRequestReview.ShowPopup();
    
    }
}