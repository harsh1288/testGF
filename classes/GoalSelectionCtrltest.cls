@isTest
 public with sharing class GoalSelectionCtrltest{
    
   Public static testMethod void Testoverrideext(){
    
      Account objacc = TestClassHelper.insertAccount();
      
      Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
         insert objGrant;
      
      Implementation_period__c objimp = TestClassHelper.createIPWithConceptNote(objGrant,objAcc);
        objimp.Reporting_Frequency__c = 'Half-yearly';
       insert objimp;
      
      Implementation_period__c objimp1 = TestClassHelper.createIPWithConceptNote(objGrant,objAcc);
        objimp1.Reporting_Frequency__c = 'Half-yearly';
       insert objimp1;
      
      Performance_Framework__c objpf = TestClassHelper.createPF(objimp);
          insert objpf;
      
      Indicator__c objind = TestClassHelper.createCatalogIndicator();
         insert objind;
       
       Catalog_Module__c objcatmod = TestClassHelper.createCatalogModule();
           insert objcatmod;
     
       Indicator__c objindcoverage = TestClassHelper.createCatalogIndicator();
         objindcoverage.Indicator_Type__c ='Impact';
         objindcoverage.Full_Name_En__c = 'test';
         objindcoverage.Is_Disaggregated__c = false;
         objindcoverage.Disaggregated_Name__c = 'testname';
         objindcoverage.Catalog_Module__c = objcatmod.id;
         insert objindcoverage;
         
        Grant_Indicator__c objgind = TestClassHelper.createGrantIndicator();
          objgind.Grant_Implementation_Period__c = objimp.id;
          objgind.Performance_Framework__c = objpf.id;
          objgind.Standard_or_Custom__c = 'Standard';
          objgind.Indicator__c =  objind.id;
          objgind.Data_Type__c =  'Ratio' ;
          objgind.Decimal_Places__c = '2';
          objgind.Indicator_Type__c = 'Impact';
          objgind.Baseline_Value1__c = '20';
           insert objgind;
           
        Goals_Objectives__c objgoal = TestClassHelper.createGoalsObjectives();
         objgoal.Implementation_Period__c = objimp.id;
         objgoal.Type__c = 'Goal';
         insert objgoal;
         
         Goals_Objectives__c objgoal1 = TestClassHelper.createGoalsObjectives();
         objgoal1.Implementation_Period__c = objimp.id;
         objgoal1.Type__c = 'Goal';
         insert objgoal1;
         
         Goals_Objectives__c objgoal2 = TestClassHelper.createGoalsObjectives();
         objgoal2.Implementation_Period__c = objimp1.id;
         objgoal2.Type__c = 'Goal';
         insert objgoal2;
           
       objindcoverage = [select id,name,Indicator_Type__c,component__c,Catalog_Module__c from Indicator__c where id =:objindcoverage.Id ];
       system.debug('@@@catind'+objindcoverage.Name);
      
       Ind_Goal_Jxn__c objjxn =  new Ind_Goal_Jxn__c ();
       objjxn.Indicator__c = objgind.id;
       objjxn.Goal_Objective__c = objgoal.id;
       insert objjxn ;
       
       ApexPages.StandardController stdctrlr = new ApexPages.StandardController(objgind);
       ApexPages.currentPage().getParameters().put('id',objgind.id);
           
         GoalSelectionCtrl testoverrideext = new GoalSelectionCtrl(stdctrlr);
            
       Test.StartTest();  
         
       testoverrideext.save();
       testoverrideext.cancel();
       Test.StopTest();
      
   }
   
   Public static testMethod void Testoverrideext2(){
    
      Account objacc = TestClassHelper.insertAccount();
      
      Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
         insert objGrant;
      
      Implementation_period__c objimp = TestClassHelper.createIPWithConceptNote(objGrant,objAcc);
        objimp.Reporting_Frequency__c = 'Half-yearly';
       insert objimp;
       
       Implementation_period__c objimp1 = TestClassHelper.createIPWithConceptNote(objGrant,objAcc);
        objimp1.Reporting_Frequency__c = 'Half-yearly';
       insert objimp1;
      
      Performance_Framework__c objpf = TestClassHelper.createPF(objimp);
          insert objpf;
      
      Indicator__c objind = TestClassHelper.createCatalogIndicator();
         insert objind;
       
       Catalog_Module__c objcatmod = TestClassHelper.createCatalogModule();
           insert objcatmod;
     
       Indicator__c objindcoverage = TestClassHelper.createCatalogIndicator();
         objindcoverage.Indicator_Type__c ='Outcome';
         objindcoverage.Full_Name_En__c = 'test';
         objindcoverage.Is_Disaggregated__c = false;
         objindcoverage.Disaggregated_Name__c = 'testname';
         objindcoverage.Catalog_Module__c = objcatmod.id;
         insert objindcoverage;
         
        Grant_Indicator__c objgind = TestClassHelper.createGrantIndicator();
          objgind.Grant_Implementation_Period__c = objimp.id;
          objgind.Performance_Framework__c = objpf.id;
          objgind.Standard_or_Custom__c = 'Standard';
          //objgind.Indicator__c =  objind.id;
          objgind.Data_Type__c =  'Ratio' ;
          objgind.Decimal_Places__c = '2';
          objgind.Indicator_Type__c = 'Outcome';
          objgind.Baseline_Value1__c = '20';
           insert objgind;
           
           Grant_Indicator__c objgind1 = TestClassHelper.createGrantIndicator();
          objgind1.Grant_Implementation_Period__c = objimp.id;
          objgind1.Performance_Framework__c = objpf.id;
          objgind1.Standard_or_Custom__c = 'Standard';
          //objgind.Indicator__c =  objind.id;
          objgind1.Data_Type__c =  'Ratio' ;
          objgind1.Decimal_Places__c = '2';
          objgind1.Indicator_Type__c = 'Impact';
          objgind1.Baseline_Value1__c = '20';
           insert objgind1;
           
         Concept_Note__c objCN = TestClassHelper.createCN();
         insert objCN;
           
         Goals_Objectives__c objgoal = TestClassHelper.createGoalsObjectives();
         objgoal.Implementation_Period__c = objimp.id;
         objgoal.Performance_Framework__c = objpf.id;
         objgoal.Type__c = 'Objective'; 
         insert objgoal;
         
         Goals_Objectives__c objgoal1 = TestClassHelper.createGoalsObjectives();
         objgoal1.Implementation_Period__c = objimp.id;
         objgoal1.Performance_Framework__c = objpf.id;
         objgoal1.Type__c = 'Objective'; 
         insert objgoal1;
         
         Goals_Objectives__c objgoal2 = TestClassHelper.createGoalsObjectives();
         objgoal2.Implementation_Period__c = objimp1.id;
         objgoal2.Performance_Framework__c = objpf.id;
         objgoal2.Type__c = 'Objective'; 
         insert objgoal2;
         
       objindcoverage = [select id,name,Indicator_Type__c,component__c,Catalog_Module__c from Indicator__c where id =:objindcoverage.Id ];
       system.debug('@@@catind'+objindcoverage.Name);
      
       Ind_Goal_Jxn__c objjxn =  new Ind_Goal_Jxn__c ();
       objjxn.Indicator__c = objgind.id;
        objjxn.Goal_Objective__c = objgoal.id;
       insert objjxn ;
       
       ApexPages.StandardController stdctrlr = new ApexPages.StandardController(objgind);
            ApexPages.currentPage().getParameters().put('id',objgind.id);
           
         GoalSelectionCtrl testoverrideext = new GoalSelectionCtrl(stdctrlr);
            
       Test.StartTest();  
           // testoverrideext.lstid.add(objgind.Id);
            testoverrideext.lstwgoalobj[0].selected =true;
            //testoverrideext.ConNoteId = objimp.id;
            testoverrideext.save();
            testoverrideext.cancel();
           
       Test.StopTest();
      
   }
   Public static testMethod void Testoverrideext3(){
    
      Account objacc = TestClassHelper.insertAccount();
      
      Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
         insert objGrant;
      
      Implementation_period__c objimp = TestClassHelper.createIPWithConceptNote(objGrant,objAcc);
        objimp.Reporting_Frequency__c = 'Half-yearly';
       insert objimp;
       
       Implementation_period__c objimp1 = TestClassHelper.createIPWithConceptNote(objGrant,objAcc);
        objimp1.Reporting_Frequency__c = 'Half-yearly';
       insert objimp1;
      
      Performance_Framework__c objpf = TestClassHelper.createPF(objimp);
          insert objpf;
      
      Indicator__c objind = TestClassHelper.createCatalogIndicator();
         insert objind;
       
       Catalog_Module__c objcatmod = TestClassHelper.createCatalogModule();
           insert objcatmod;
     
       Indicator__c objindcoverage = TestClassHelper.createCatalogIndicator();
         objindcoverage.Indicator_Type__c ='Outcome';
         objindcoverage.Full_Name_En__c = 'test';
         objindcoverage.Is_Disaggregated__c = false;
         objindcoverage.Disaggregated_Name__c = 'testname';
         objindcoverage.Catalog_Module__c = objcatmod.id;
         insert objindcoverage;
         
        Grant_Indicator__c objgind = TestClassHelper.createGrantIndicator();
          objgind.Grant_Implementation_Period__c = objimp.id;
          objgind.Performance_Framework__c = objpf.id;
          objgind.Standard_or_Custom__c = 'Standard';
          //objgind.Indicator__c =  objind.id;
          objgind.Data_Type__c =  'Ratio' ;
          objgind.Decimal_Places__c = '2';
          objgind.Indicator_Type__c = 'Outcome';
          objgind.Baseline_Value1__c = '20';
           insert objgind;
           
           Grant_Indicator__c objgind1 = TestClassHelper.createGrantIndicator();
          objgind1.Grant_Implementation_Period__c = objimp.id;
          objgind1.Performance_Framework__c = objpf.id;
          objgind1.Standard_or_Custom__c = 'Standard';
          //objgind.Indicator__c =  objind.id;
          objgind1.Data_Type__c =  'Ratio' ;
          objgind1.Decimal_Places__c = '2';
          objgind1.Indicator_Type__c = 'Impact';
          objgind1.Baseline_Value1__c = '20';
           insert objgind1;
           
         Concept_Note__c objCN = TestClassHelper.createCN();
         insert objCN;
           
         Goals_Objectives__c objgoal = TestClassHelper.createGoalsObjectives();
         objgoal.Implementation_Period__c = objimp.id;
         objgoal.Performance_Framework__c = objpf.id;
         objgoal.Type__c = 'Objective'; 
         insert objgoal;
         
         Goals_Objectives__c objgoal1 = TestClassHelper.createGoalsObjectives();
         objgoal1.Implementation_Period__c = objimp.id;
         objgoal1.Performance_Framework__c = objpf.id;
         objgoal1.Type__c = 'Goal'; 
         insert objgoal1;
         
         
       objindcoverage = [select id,name,Indicator_Type__c,component__c,Catalog_Module__c from Indicator__c where id =:objindcoverage.Id ];
       system.debug('@@@catind'+objindcoverage.Name);
      
       Ind_Goal_Jxn__c objjxn =  new Ind_Goal_Jxn__c ();
       objjxn.Indicator__c = objgind.id;
        objjxn.Goal_Objective__c = objgoal.id;
       insert objjxn ;
       
       Ind_Goal_Jxn__c objjxn1 =  new Ind_Goal_Jxn__c ();
       objjxn1.Indicator__c = objgind1.id;
        objjxn1.Goal_Objective__c = objgoal1.id;
       insert objjxn1 ;
       
       ApexPages.StandardController stdctrlr = new ApexPages.StandardController(objgind);
            ApexPages.currentPage().getParameters().put('id',objgind.id);
           
         GoalSelectionCtrl testoverrideext = new GoalSelectionCtrl(stdctrlr);
            
       Test.StartTest();  
           // testoverrideext.lstid.add(objgind.Id);
            //testoverrideext.lstwgoalobj[0].selected =true;
            //testoverrideext.ConNoteId = objimp.id;
            testoverrideext.save();
            testoverrideext.cancel();
           
       Test.StopTest();
      
   }
 }