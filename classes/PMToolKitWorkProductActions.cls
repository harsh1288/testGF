/**
* @author       Mark Baker      
* @date         06/18/2013
* @description  Class that checks the State of a work product. When all 
*               child tasks have been set to completed, it sets WP state to 
*               Complete.  When at least one task is In-Progress, sets the
*               WP to In-Progress. When no tasks are In-Progress, or there are
*               no tasks (e.g. when the only task was deleted), sets the WP to  
*               Defined.
*
*    --------------------------------------------------------------------------
*    Developer                  Date                Description
*    --------------------------------------------------------------------------
*   
*    Mark Baker                 06/11/2013          Created
*/

public with sharing class PMToolKitWorkProductActions {
  
    public static void setWorkProductState(List<Work_Product_Task__c> triggerList) {
        
        map<Id, string> WorkProductStates = new map<Id, string>();
        List<Id> WpTriggerList = new List<Id>();
        String CurrentWPState, TaskState;
        Integer I;
        
        //throw the parent user stories from the trigger into a list so we can query on it
        for(Work_Product_Task__c task : triggerList) {
           WpTriggerList.add(task.Work_Product__c);
        }

        //go through the tasks that have a Work Product in the trigger list
        for (Work_Product_Task__c T : [SELECT Id, State__c,Work_Product__c FROM Work_Product_Task__c WHERE Work_Product__c IN :WpTriggerList]) {
            CurrentWPState = WorkProductStates.get(t.Work_Product__c);
            TaskState = T.State__c;
        //if the current tasks's work product is not in the map of wpstates, add the task's state to it
        //as if there is only one task under a WP we know we can set WP state = Task state
            if (!(WorkProductStates.containsKey(T.Work_Product__c))) {
                WorkProductStates.put(T.Work_Product__c,TaskState);
        //if the work product is already in the map, then we know there is more than one task under that WP
        //in that case look at what the WP State is set to in the map
        //and then based on the State in the current task we are looking at, adjust the State value in the map accordingly. 
            } else {
                if (CurrentWPState == 'Completed') {
                    if (TaskState == 'In-Progress') {
                        WorkProductStates.put(T.Work_Product__c,TaskState);
                    }
                    else if (TaskState == 'Defined') {
                        WorkProductStates.put(T.Work_Product__c,'In-Progress');
                    }
                }
                else if (CurrentWPState == 'Defined' || CurrentWPState == NULL) {
                    if (TaskState == 'Completed') {
                        WorkProductStates.put(T.Work_Product__c,'In-Progress');
                    }
                    else if (TaskState == 'In-Progress') {
                        WorkProductStates.put(T.Work_Product__c,'In-Progress');
                    }
                }
            }                
        }

        Set<Work_Product__c> WPUpdateSet = new Set<Work_Product__c>();        
        List<Work_Product__c> WPUpdateList = new List<Work_Product__c>();
        Work_Product__c UpdatedWP;
        Map<ID,Datetime> WPDefinedDateMap = new Map<ID,Datetime>();
        
        //now find WorkProducts that don't have tasks assigned and so won't be represented in the map
        //do a query to find if those WPs ever had State set to Defined by checking Defined_Date_Time__c.
        //if it's not null we put it in a map for later comparison.  
        for (Work_Product__c wp : [SELECT ID, Defined_Date_Time__c FROM Work_Product__c WHERE ID IN :WpTriggerList]) {
            if (wp.Defined_Date_Time__c <> NULL ) {
                WPDefinedDateMap.put(wp.ID, wp.Defined_Date_Time__c);
            }
        }
        
        //now we go through the trigger list again 
        for(Work_Product_Task__c task : triggerList) {
            
            //if those work products have Defined_Date_Time set to something, set the state back to defined.
            //else set it to nothing.           
            if (!(WorkProductStates.containsKey(task.Work_Product__c)))  {
                UpdatedWP = new Work_Product__c();
                UpdatedWP.Id = task.Work_Product__c;
                //compare this wp to the map we made above.  If we find it we know
                //Defined_Date_Time__c is not null, and therefore in the absence of tasks
                //the WP State should be set to Defined. 
                if (WPDefinedDateMap.containsKey(task.Work_Product__c)) {
                    UpdatedWP.State__c='Defined';   
                } else {
                    UpdatedWP.State__c=NULL;
                }
                
                WPUpdateSet.add(UpdatedWP);
            }
            //for the work products that are in the map i.e. they have tasks under them
            //add it to a list of Work Products we will use to update
            else {
                UpdatedWP = new Work_Product__c();
                UpdatedWP.Id = task.Work_Product__c;
                UpdatedWP.State__c=WorkProductStates.get(task.Work_Product__c); 
                WPUpdateSet.add(UpdatedWP);
            }
            
        }
        
        //if there are work products to update, then update the Work Products        
        if (WPUpdateSet.size() > 0) { 
            for ( Work_Product__c LoopingWP : WPUpdateSet) {
                WPUPdateList.add(LoopingWP);
                }
                
            update WPUpdateList; 
        }
    }
    
    /**
    * @author       Kim Roth
    * @date         06/26/2013
    * @description  Sums the tasks estimates and to dos on every task for an iteration and release
    */
     public static void sumTaskEstimatesToDos(List<Work_Product_Task__c> triggerList) {
        
        List<ID> iterationIDList = new List<ID>();
        List<ID> releaseIDList = new List<ID>();
        List<ID> parentIDList = new List<ID>();
        List<ID> workProductIDList = new List<ID>();
        List<Work_Product_Task__c> workProductTasksForIteration = new List<Work_Product_Task__c>();
        List<Work_Product_Task__c> workProductTasksForRelease = new List<Work_Product_Task__c>();
        List<Work_Product_Task__c> workProductTasksForParent = new List<Work_Product_Task__c>();
        List<Iteration__c> iterationList = new List<Iteration__c>();
        List<SS_Release__c> releaseList = new List<SS_Release__c>();
        List<Work_Product__c> parentList = new List<Work_Product__c>();
        List<Work_Product__c> workProductList = new List<Work_Product__c>();
        
        //Create a list of work product IDs related to the tasks
        for(Work_Product_Task__c task : triggerList){
            workProductIDList.add(task.Work_Product__c);
        }
        
        //query for all work products related to the updated tasks
        workProductList = [SELECT id, iteration__c, ss_release__c, parent__c FROM Work_Product__c WHERE id IN :workProductIDList];
        
        //query for list of iteration, release and parent IDs related to the updated tasks
        for(Work_Product__c workProd : workProductList){
            if(workProd.iteration__c <> NULL){
                iterationIDList.add(workProd.iteration__c);
            }
            if(workProd.ss_release__c <> NULL){
                releaseIDList.add(workProd.ss_release__c);
            }
            if(workProd.parent__c <> NULL){
                parentIDList.add(workProd.parent__c);
            }
        }
        
        //Update iterations
        if(iterationIDList.size() > 0){       
            //query for all of the tasks related to the iterations in the list
            workProductTasksForIteration = [SELECT id, estimate__c, to_do__c, work_product__r.iteration__c from Work_Product_Task__c WHERE work_product__r.iteration__c IN :iterationIDList];        
            
            //query for all the iterations that need to be updated
            iterationList = [SELECT id, task_estimate__c, to_do__c FROM Iteration__c WHERE id IN :iterationIDList];
            
            //For each task, update iteration by adding the to do/estimate
            for(Iteration__c iteration : iterationList){
                iteration.task_estimate__c = 0;
                iteration.to_do__c = 0;
                for(Work_Product_Task__c task : workProductTasksForIteration){
                    if(task.work_product__r.iteration__c == iteration.id){
                        if(task.estimate__c <> NULL){
                            iteration.task_estimate__c+=task.estimate__c;
                        }
                        if(task.to_do__c <> NULL){
                            iteration.to_do__c+=task.to_do__c;
                        }
                    }
                }
            }
            update iterationList; 
        }
        
        //Update releases
        if(releaseIDList.size() > 0){ 
            
            //query for all the releases that need to be updated
            releaseList = [SELECT id, task_estimate__c, to_do__c FROM SS_Release__c WHERE id IN :releaseIDList];
                 
            //query for all of the tasks related to the releases in the list
            workProductTasksForRelease = [SELECT id, estimate__c, to_do__c, work_product__r.ss_release__c from Work_Product_Task__c WHERE work_product__r.ss_release__c IN :releaseIDList];
            
            //For each task, update release by adding the to do/estimate
            for(SS_Release__c release : releaseList){
                release.task_estimate__c = 0;
                release.to_do__c = 0;
                for(Work_Product_Task__c task : workProductTasksForRelease){
                    if(task.work_product__r.ss_release__c == release.id){
                        if(task.estimate__c <> NULL){
                            release.task_estimate__c+=task.estimate__c;
                        }
                        if(task.to_do__c <> NULL){
                            release.to_do__c+=task.to_do__c;
                        }
                    }
                }
            }
            update releaseList; 
        } 
        
        
        //Update parent work products
        if(parentIDList.size() > 0){
            
            //query for all parent work products
          //RecordType parentWPRecordType = [SELECT Id, Name FROM RecordType WHERE Name = 'Activity' AND SobjectType = 'Work_Product__c'];
            RecordType parentWPRecordType = [SELECT Id, Name FROM RecordType WHERE Name = 'User Story' AND SobjectType = 'Work_Product__c'];
            //KR
            parentList = [SELECT id, plan_estimate__c, accepted__c, to_do_parent__c, task_estimate_parent__c FROM Work_Product__c WHERE id IN :parentIDList AND recordTypeID = :parentWPRecordType.id];
            
            //query for all of the tasks related to the parents in the list
            workProductTasksForParent = [SELECT id, estimate__c, to_do__c, work_product__r.parent__c from Work_Product_Task__c WHERE work_product__r.parent__c IN :parentIDList];
            
            //For each task, update parent by adding the to do/estimate
            for(Work_Product__c parent : parentList){
                parent.task_estimate_parent__c = 0;
                parent.to_do_parent__c = 0;
                for(Work_Product_Task__c task : workProductTasksForParent){
                    if(task.work_product__r.parent__c == parent.id){
                        if(task.estimate__c <> NULL){
                            parent.task_estimate_parent__c+=task.estimate__c;
                        }
                        if(task.to_do__c <> NULL){
                            parent.to_do_parent__c+=task.to_do__c;
                        }
                    }
                }
            }
            update parentList; 
            
        }
        
     }
    
    /**
    * @author       Kim Roth
    * @date         06/27/2013
    * @description  Updates the iteration with information from the work product
    */
    public static void updateIterationFromWorkProduct(List<Work_Product__c> triggerList){
     if(checkRecursiveforUpdateReleaseSprint.runOnce()){
        
        List<ID> iterationIDList = new List<ID>();
        List<Iteration__c> iterationList = new List<Iteration__c>();
        List<Work_Product__c> workProductList = new List<Work_Product__c>();
        
        //Create a list of iteration IDs related to the updated work products
        for(Work_Product__c workProduct : triggerList){
            if(workProduct.iteration__c <> NULL){
                iterationIDList.add(workProduct.iteration__c);
            }
        }
        
        if(iterationIDList.size() > 0){
            //query for all iterations related to the updated work products
            iterationList = [SELECT id, plan_estimate__c, accepted__c FROM Iteration__c WHERE id IN :iterationIDList];
               
            //query for list of work products related to the iterations
            workProductList = [SELECT id, iteration__c, plan_estimate__c, accepted_date_time__c, state__c FROM Work_Product__c WHERE iteration__c IN :iterationIDList];
       
            //For each work product, update iteration field by adding the # accepted and sum of plan estimate
            for(Iteration__c iteration : iterationList){
                
                iteration.accepted__c = 0;
                iteration.plan_estimate__c = 0;
                
                for(Work_Product__c workProduct : workProductList){
                    if(workProduct.iteration__c == iteration.id){
                        if(workProduct.plan_estimate__c <> NULL){
                            iteration.plan_estimate__c+=integer.valueOf(workProduct.plan_estimate__c);//JV 11-7-13: Added integer.valueOf() to convert String to Integer
                        
                            //sum plan estimate for accepted work products
                            if(workProduct.state__c == 'Accepted'){
                                iteration.accepted__c+=integer.valueOf(workProduct.plan_estimate__c); //JV 11-7-13: Added integer.valueOf() to convert String to Integer
                            }
                        }
                    }
                }
            }
            update iterationList; 
        } 
       } 
    }
    
    
    /**
    * @author       Kim Roth
    * @date         06/27/2013
    * @description  Updates the release with information from the work product
    */
    public static void updateReleaseFromWorkProduct(List<Work_Product__c> triggerList){
     if(checkRecursiveforUpdateReleaseSprint.runOnce()){   
        List<ID> releaseIDList = new List<ID>();
        List<SS_Release__c> releaseList = new List<SS_Release__c>();
        List<Work_Product__c> workProductList = new List<Work_Product__c>();
        
        //Create a list of release IDs related to the updated work products
        for(Work_Product__c workProduct : triggerList){
            if(workProduct.ss_release__c <> NULL){
                releaseIDList.add(workProduct.ss_release__c);
            }
        }
        
        if(releaseIDList.size() > 0){
            //query for all releases related to the updated work products
            releaseList = [SELECT id, plan_estimate__c, accepted__c FROM SS_Release__c WHERE id IN :releaseIDList];
            
            //query for list of work products related to the releases
            workProductList = [SELECT id, ss_release__c, plan_estimate__c, accepted_date_time__c, state__c FROM Work_Product__c WHERE ss_release__c IN :releaseIDList];
       
            //For each work product, update release field by adding the # accepted and sum of plan estimate
            for(SS_Release__c release : releaseList){
                
                release.accepted__c = 0;
                release.plan_estimate__c = 0;
                
                for(Work_Product__c workProduct : workProductList){
                    if(workProduct.ss_release__c == release.id){
                        if(workProduct.plan_estimate__c <> NULL){
                            release.plan_estimate__c+=integer.valueOf(workProduct.plan_estimate__c); //JV 11-7-13: Added integer.valueOf() to convert String to Integer
                        
                            //sum plan estimate for accepted work products
                            if(workProduct.state__c == 'Accepted'){
                                release.accepted__c+=integer.valueOf(workProduct.plan_estimate__c); //JV 11-7-13: Added integer.valueOf() to convert String to Integer
                            }
                        }
                    }
                }
            }
            update releaseList;
        } }      
    }
    
    /**
    * @author       Mark Baker
    * @date         06/27/2013
    * @description  Validates that closed Defects cannot be switched to User Story record types.
    */
    /*
    //Created a validation rule instead of a trigger
     public static void preventClosedDefectsChanging (list<Work_Product__c> TriggerNewList,list<Work_Product__c> TriggerOldList) {
        
        integer I;
        RecordType DefectRecordType = [SELECT Id FROM RecordType WHERE Name = 'Defect' AND SobjectType = 'Work_Product__c'];
        
        for (I=0; I<TriggerNewList.Size(); I++) {
            if (TriggerOldList[I].Defect_State__c == 'Closed' && TriggerOldList[I].RecordTypeId == DefectRecordType.Id && TriggerNewList[I].RecordTypeId <> TriggerOldList[I].RecordTypeId) {
                TriggerNewList[I].addError('You cannot convert a Closed Defect to a User Story.');
            }
        }
        
     }
     */
     
    /**
    * @author       Kim Roth
    * @date         07/02/2013
    * @description  Updates the parent work product with information from the work product
    */
    public static void updateParentWorkProductFromChildWorkProduct(List<Work_Product__c> triggerList){
      if(checkRecursiveforUpdateReleaseSprint.runOnce()){   
        RecordType parentWPRecordType;
        List<ID> parentIDList = new List<ID>();
        List<Work_Product__c> parentWPList = new List<Work_Product__c>();
        List<Work_Product__c> workProductList = new List<Work_Product__c>();
        Integer planEstimateSum = 0;
        
        //Get parent user story record type id
        //parentWPRecordType = [SELECT Id, Name FROM RecordType WHERE Name = 'Parent User Story' AND SobjectType = 'Work_Product__c'];
        
        //Create a list of parent IDs related to the updated work products
        for(Work_Product__c workProduct : triggerList){
            if(workProduct.parent__c <> NULL){
                parentIDList.add(workProduct.parent__c);
            }
        }
        
        if(parentIDList.size() > 0){
            //query for all parent work products
            //Kr
            parentWPRecordType = [SELECT Id, Name FROM RecordType WHERE Name = 'User Story' AND SobjectType = 'Work_Product__c'];
            parentWPList = [SELECT id, plan_estimate__c, accepted__c FROM Work_Product__c WHERE id IN :parentIDList AND recordTypeID = :parentWPRecordType.id];
            
            //query for a list of work products related to the parent work product
            workProductList = [SELECT id, parent__c, plan_estimate__c, accepted_date_time__c FROM Work_Product__c WHERE parent__c IN :parentIDList];
            
            //for each work product, update the parent work product by adding the sum of plan estimate 
            for(Work_Product__c parent : parentWPList){
                parent.plan_estimate__c = '0';  
                parent.accepted__c = 0;             
                for(Work_Product__c workProduct : workProductList){
                    if(workProduct.parent__c == parent.id) {
                        //kr
                        if(workProduct.plan_estimate__c <> NULL){
                            //parent.plan_estimate_parent__c+=integer.valueOf(workProduct.plan_estimate__c);
                            planEstimateSum +=integer.valueOf(workProduct.plan_estimate__c);
                            parent.plan_estimate__c = string.valueOf(planEstimateSum);
                            
                             //sum plan estimate for accepted work products
                            if(workProduct.accepted_date_time__c <> NULL){
                                parent.accepted__c+=integer.valueOf(workProduct.plan_estimate__c);
                            }
                        }
                    }
                }
            }

            update parentWPList;        
        }}
    
    }   
    
    /**
    * @author       Kim Roth
    * @date         09/5/2013
    * @description  Update Rank when a work product is changed
    */
    public static void updateRankFromWorkProductAfterUpdate(List<Work_Product__c> triggerOldList, List<Work_Product__c> triggerNewList){
       if(checkRecursiveforUpdateReleaseSprint.runOnce()){  
        //Get old value, new value, update ranks for all of those where there is a change
        List<Id> updatedReleaseIDs = new List<Id>();
        List<Id> updatedIterationIDs = new List<Id>();
        Boolean ProductBacklogUpdated = false;
        List<Id> updatedProjectIDs = new List<Id>();
        
        //Query through new work products to determine if release or iteration has changed
        for(Work_Product__c workProductNew : triggerNewList){
            for(Work_Product__c workProductOld : triggerOldList){
                if(workProductNew.id == workProductOld.id){
                    //Compare releases, if different add to updated release list
                    if(workProductNew.ss_release__c <> workProductOld.ss_release__c){
                        
                        //Checking to make sure that the new release isn't NULL
                        if(workProductNew.ss_release__c <> NULL){
                            updatedReleaseIDs.add(workProductNew.ss_release__c);
                        }
                        
                        //Checking to make sure that the old release isn't NULL
                        if(workProductOld.ss_release__c <> NULL){
                            updatedReleaseIDs.add(workProductOld.ss_release__c);
                        }
                        
                        //Check to see if the product backlog was updated (work products moved in or out of it)
                        if((workProductNew.iteration__c == NULL && workProductNew.ss_release__c == NULL) || (workProductOld.iteration__c == NULL && workProductOld.ss_release__c == NULL)){
                            ProductBacklogUpdated = true;
                            
                            if(workProductNew.project_overview__c <> workProductOld.project_overview__c){
                                updatedProjectIDs.add(workProductNew.project_overview__c);
                                updatedProjectIDs.add(workProductOld.project_overview__c); 
                            }else{
                                updatedProjectIDs.add(workProductNew.project_overview__c);
                            }

                        }
                                                
                    }
                    //Compare iterations, if different add to updated iteration list
                    if(workProductNew.iteration__c <> workProductOld.iteration__c) {
                        
                        if(workProductNew.iteration__c <> NULL){
                            updatedIterationIDs.add(workProductNew.iteration__c);
                        }
                        
                        if(workProductOld.iteration__c <> NULL){
                            updatedIterationIDs.add(workProductOld.iteration__c);    
                        }
                        
                        //Account for scenario where iterations change but the release stays the same, need to update release backlog ranks
                        if(workProductNew.ss_release__c <> NULL && (workProductOld.iteration__c == NULL || workProductNew.iteration__c == NULL)){
                            updatedReleaseIDs.add(workProductNew.ss_release__c);
                        }
                        
                        //Check to see if the product backlog was updated (work products moved in or out of it)
                        if((workProductNew.iteration__c == NULL && workProductNew.ss_release__c == NULL) || (workProductOld.iteration__c == NULL && workProductOld.ss_release__c == NULL)){
                            ProductBacklogUpdated = true;
                            
                            if(workProductNew.project_overview__c <> workProductOld.project_overview__c){
                                updatedProjectIDs.add(workProductNew.project_overview__c);
                                updatedProjectIDs.add(workProductOld.project_overview__c); 
                            }else{
                                updatedProjectIDs.add(workProductNew.project_overview__c);
                            }
                        }
                    }
                }
            }
        } //end of for loop
        
        //Update ranks
        if(productBacklogUpdated || updatedReleaseIDs.size() > 0 || updatedIterationIDs.size() > 0){
            updateRank(productBacklogUpdated, updatedProjectIDs, updatedReleaseIDs, updatedIterationIDs);
        }}
    } //End of class updateRankFromWorkProductAfterUpdate
    
    
    /**
    * @author       Kim Roth
    * @date         09/5/2013
    * @description  Update Rank when a work product is deleted
    */
    public static void updateRankFromWorkProduct(List<Work_Product__c> triggerList){
       if(checkRecursiveforUpdateReleaseSprint.runOnce()){ 
        List<Id> updatedReleaseIDs = new List<Id>();
        List<Id> updatedIterationIDs = new List<Id>();
        Boolean ProductBacklogUpdated = false;
        List<Id> updatedProjectIDs = new List<Id>();
        
        for(Work_Product__c workProduct : triggerList){
            
            //Check if deleted work products are in product backlog
            if(workProduct.ss_release__c == NULL && WorkProduct.iteration__c == NULL){
                ProductBacklogUpdated = true;
                updatedProjectIDs.add(workProduct.project_overview__c); 
            }
            
            //Adding updated releases
            if(workProduct.ss_release__c <> NULL){
                updatedReleaseIDs.add(workProduct.ss_release__c);
            }
            
            //Adding updated iterations
            if(workProduct.iteration__c <> NULL){
                updatedIterationIDs.add(workProduct.iteration__c);    
            }            
            
        } //End of for loop
        
        //Update ranks
        if(productBacklogUpdated || updatedReleaseIDs.size() > 0 || updatedIterationIDs.size() > 0){
            updateRank(productBacklogUpdated, updatedProjectIDs, updatedReleaseIDs, updatedIterationIDs);
        }}
    } //End of class updateRankFromWorkProduct
    
    
    
    /**
    * @author       Kim Roth
    * @date         09/9/2013
    * @description  Update Rank for all work products
    */
    public static void updateRank(Boolean productBacklogUpdated, List<Id> updatedProjectIDs, List<Id> updatedReleaseIDs, List<Id> updatedIterationIDs){
       if(checkRecursiveforUpdateReleaseSprint.runOnce()){ 
        List<Work_Product__c> updatedWorkProducts = new List<Work_Product__c>();
        List<SS_Release__c> updatedReleases = new List<SS_Release__c>();
        List<Iteration__c> updatedIterations = new List<Iteration__c>();
        List<project_overview__c> updatedProjects = new List<project_overview__c>();
        RecordType parentUserStoryRecordType = new RecordType();
        Integer count = 0;   
        
      //parentUserStoryRecordType = [SELECT id FROM RecordType WHERE name = 'Activity' AND sobjectType = 'Work_Product__c'];
         parentUserStoryRecordType = [SELECT id FROM RecordType WHERE name = 'User Story' AND sobjectType = 'Work_Product__c'];
            
        //Update product backlog rank (release and iteration are null)
        if(productBacklogUpdated){
            updatedWorkProducts = [SELECT id, rank__c, project_overview__c 
                                    FROM Work_Product__c 
                                    WHERE ss_release__c = NULL AND iteration__c = NULL AND project_overview__c IN :updatedProjectIDs AND recordtypeid <> :parentUserStoryRecordType.id
                                    ORDER BY rank__c ASC];
            
            //Get list of updated projects       
            updatedProjects = [SELECT id FROM project_overview__c WHERE id IN :updatedProjectIDs];
            
            //For each work product in the product backlog by project, update rank
            for(project_overview__c project : updatedProjects){
                count = 0;
                for(Work_Product__c workProduct : updatedWorkProducts){
                    if(project.id == workProduct.project_overview__c) {
                        workProduct.rank__c = count;
                        count++;

                    }                   
                }
                update updatedWorkProducts;
            }                
        } //end of if statement, updating product backlog
        
        
        //Update release backlog rank (iteration is null)
        if(updatedReleaseIDs.size() > 0){
           updatedWorkProducts = [SELECT id, rank__c, ss_release__c 
                                    FROM Work_Product__c 
                                    WHERE ss_release__c IN :updatedReleaseIDs AND iteration__c = NULL AND recordtypeid <> :parentUserStoryRecordType.id
                                    ORDER BY rank__c ASC]; 
           
           //Get a list of updated releases
           updatedReleases = [SELECT id FROM SS_Release__c WHERE id IN :updatedReleaseIDs];
           
           //For each work product in the release backlog, by release, update rank
           for(SS_Release__c release :updatedReleases){
               count = 0;
               for(Work_Product__c workProduct : updatedWorkProducts){
                   if(release.id == workProduct.ss_release__c){
                        workProduct.rank__c = count;
                        count++;
                   }
               }
               update updatedWorkProducts;
           }              
        } //end of if statement, updating release backlog
        
        
        //Update iteration backlog rank (iteration <> null)
        if(updatedIterationIDs.size() > 0){
           updatedWorkProducts = [SELECT id, name, rank__c, iteration__c 
                                    FROM Work_Product__c 
                                    WHERE iteration__c IN :updatedIterationIDs AND recordtypeid <> :parentUserStoryRecordType.id 
                                    ORDER BY rank__c ASC]; 
           
           //Get a list of updated iterations
           updatedIterations = [SELECT id FROM iteration__c WHERE id IN :updatedIterationIDs];
           
           //For each work product in the iteration backlog, by iteration, update rank
          for(Iteration__c iteration :updatedIterations){
               count = 0;
               for(Work_Product__c workProduct : updatedWorkProducts){
                   if(iteration.id == workProduct.iteration__c){
                        workProduct.rank__c = count;
                        count++;
                   }
               }
               update updatedWorkProducts;
           }            
        } }//end of if statement, updating iteration backlog
        
    } //End of class updateRank
   
   Public static void updateReleaseSprintd(List<Work_Product__c> triggerlist){
   if(checkRecursiveforUpdateReleaseSprint.runOnce()){
   system.debug('incheckRecursiveforUpdateReleaseSprint');
   List<Work_Product__c> listUserStories     = new List<Work_Product__c>();
   List<Work_Product__c> listReleaseUserStories     = new List<Work_Product__c>();
   List<Iteration__c>    sprintRecord        = new List<Iteration__c>();
   List<SS_Release__c>   releaseRecord       = new List<SS_Release__c>();
   
   Set<Id> sprintIds = new Set<Id>();
   Set<Id> releaseIds = new Set<Id>();
   system.debug('tgrlist size ' + triggerlist.size());
   for(Work_Product__c wpUser : triggerlist){
       if(wpUser.Iteration__c != null) {
           sprintIds.add(wpUser.Iteration__c);
       } 
       if(wpUser.SS_Release__c!= null) {
       releaseIds.add(wpUser.SS_Release__c); 
       }
   }
   system.debug('sprint ids' + sprintIds);
   listUserStories = [Select Id,Iteration__c,SS_Release__c,Task_Effort__c,Plan_Estimate__c 
                               from Work_Product__c where Iteration__c IN : sprintIds];
   
   listReleaseUserStories = [Select Id,Iteration__c,SS_Release__c,Task_Effort__c,Plan_Estimate__c 
                               from Work_Product__c where SS_Release__c IN : releaseIds];
                               
   sprintRecord    = [Select Id,Task_Effort__c,Release__c,Planned_Velocity__c 
                             from Iteration__c where Id IN : sprintIds];
   
   
   releaseRecord   = [Select Id,Task_Effort__c,Target_Size_of_Release__c,Plan_Estimate__c from SS_Release__c 
                               where Id IN : releaseIds];
                               
   Double sumStoryPoints = 0 ;
   Double sumTaskEffort  = 0 ;
   
   for(Work_Product__c wp : listUserStories){
      System.debug('@@@ '+ wp);
      System.debug('@@@ '+ wp.Plan_Estimate__c);
    if(!String.isEmpty(wp.Plan_Estimate__c)){
         System.debug('@@@ '+ wp.Plan_Estimate__c);
     sumStoryPoints += Double.valueof(wp.Plan_Estimate__c) ;
    }
    sumTaskEffort  += wp.Task_Effort__c ;
   }
   
   for(Iteration__c sprint   : sprintRecord){
    sprint.Task_Effort__c      = sumTaskEffort;
    sprint.Planned_Velocity__c = sumStoryPoints;
    System.debug('###sprint.Task_Effort__c'+sprint.Task_Effort__c+'### sprint.Planned_Velocity__c'+ sprint.Planned_Velocity__c);
   }
   
   Double sumReleaseStoryPoints = 0 ;
   Double sumReleaseTaskEffort  = 0 ;
   
   for(Work_Product__c wp : listReleaseUserStories){
      System.debug('@@@ '+ wp);
      System.debug('@@@ '+ wp.Plan_Estimate__c);
    if(wp.Plan_Estimate__c != null){
         System.debug('@@@ '+ wp.Plan_Estimate__c);
     sumReleaseStoryPoints += Double.valueof(wp.Plan_Estimate__c) ;
    }
     sumReleaseTaskEffort  += wp.Task_Effort__c ;
   }
   
   for(SS_Release__c release : releaseRecord){
    release.Target_Size_of_Release__c = sumReleaseStoryPoints;
    release.Task_Effort__c   = sumReleaseTaskEffort;
   }
   
     update sprintRecord;
     update releaseRecord;
    }
   
   } 
}