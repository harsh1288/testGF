public class editGIController {

    public Id profileId;
    public String profileName;
    public string status;
    public String getBackUrl {get; set;}
    public String moduleUrl;
    public boolean page_redirect;
  
   public editGIController (ApexPages.StandardController stdController) {
       getBackUrl = null;                
        page_redirect = false;
        moduleUrl = null;
        profileId=userinfo.getProfileId();
        profileName=[Select Id,Name from Profile where Id=:profileId].Name;
   }
   public PageReference reDirect(){ 
     return null;
   }
}