@isTest
private class WPTMKeyActivityReportExtension_Test {

    static testmethod void createData(){      
        
        Account tAcc=TestClassHelper.insertAccount();     
        
        Contact con = TestClassHelper.insertContact();             
        
        Grant__c tGrant=TestClasshelper.createGrant(tAcc);
        insert tGrant;
        
        Implementation_Period__c tImpPeriod=TestClassHelper.createIP(tGrant,tAcc);
        insert tImpPeriod;
        
        Performance_Framework__c tPF = new Performance_Framework__c ();
        tPF.Implementation_Period__c = tImpPeriod.id; 
        tPF.PF_Status__c = 'Not yet Submitted';
        insert tPF;
        
        Module__c modObj = TestClassHelper.createModule();
        insert modObj ;
        
        
        Grant_Intervention__c tGi=TestClassHelper.createGrantIntervention(tImpPeriod);
        tGi.Performance_Framework__c = tPF.id;        
        insert tGi;
        
        
        
        Key_Activity__c tKact=new Key_Activity__c();

        tKact.Grant_Intervention__c=tGi.Id;
        tKact.Activity_Description__c = 'Test';
        insert tKact;  
        
        Milestone_Target__c tMtc=new Milestone_Target__c();
        tMtc.Key_Activity__c=tKact.id;
        tMtc.MilestoneTitle__c='TestTitle';
        tMtc.Criteria__c ='testCriteria';
        tMtc.Reporting_Period__c ='testreportingperiod';
        tMtc.Linked_To__c ='testlink';
        insert tMtc;
        
        Milestone_Target__c tMtc1=new Milestone_Target__c();
        tMtc1.Key_Activity__c=tKact.id;
        tMtc1.Criteria__c ='testCriteria';
        tMtc1.MilestoneTitle__c='TestTitle2';
        tMtc1.Reporting_Period__c ='testreportingperiod';
        tMtc1.Linked_To__c ='testlink2';
        insert tMtc1;
        
        
        Test.startTest();
        WPTMKeyActivityReportExtension obj = new WPTMKeyActivityReportExtension(new ApexPages.StandardController(tPF));
        List<Milestone_Target__c>  milelst = [Select id,MilestoneTitle__c,Key_Activity__c,Criteria__c,Linked_To__c,Reporting_Period__c  from Milestone_Target__c ];
        obj.addKeyActivity();
        obj.backToPerformanceFramework();
        obj.openKeyActivity();
        obj.interventionName ='testintervention';
        obj.moduleName='testmodule';
        obj.activityName ='testactivity';
        obj.activityId =tKact.id;
        WPTMKeyActivityReportExtension.grantInterventionWrapper wrapObj = new WPTMKeyActivityReportExtension.grantInterventionWrapper(obj.interventionName,obj.activityName ,obj.moduleName,obj.activityId ,milelst);
        obj.addKeyActivity();
        obj.getRecordsToDisplay();
        Test.stopTest();
        
    }
   
}