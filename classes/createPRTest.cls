@isTest
public class createPRTest

{

public static testMethod void createPREn()
{
Account objAcc = TestClassHelper.createAccount();
//insert objAcc;

Concept_Note__c objCN = TestClassHelper.createCN();
//objAcc
insert objCN;

Page__c objPg = TestClassHelper.createPage();
objPg.Concept_Note__c = objCN.Id;
insert objPg;

Apexpages.currentpage().getparameters().put('cnId', objCN.Id);
Apexpages.currentpage().getparameters().put('pageId', objCN.Id);
Apexpages.currentpage().getparameters().put('language', 'English');
//Apexpages.currentpage().getparameters().put('GM','GM');
ApexPages.StandardController stdController = new ApexPages.StandardController(objAcc);
CreatePR objCreatePR = new CreatePR(stdController);
objCreatePR.saveRecord();


}

public static testMethod void createPREs()
{
Account objAcc = TestClassHelper.createAccount();
//insert objAcc;

Concept_Note__c objCN = TestClassHelper.createCN();
//objAcc
insert objCN;

Page__c objPg = TestClassHelper.createPage();
objPg.Concept_Note__c = objCN.Id;
insert objPg;

Apexpages.currentpage().getparameters().put('cnId', objCN.Id);
Apexpages.currentpage().getparameters().put('pageId', objCN.Id);
Apexpages.currentpage().getparameters().put('language', 'es');
//Apexpages.currentpage().getparameters().put('GM','GM');
ApexPages.StandardController stdController = new ApexPages.StandardController(objAcc);
CreatePR objCreatePR = new CreatePR(stdController);
objCreatePR.saveRecord();


}

public static testMethod void createPRFr()
{
Account objAcc = TestClassHelper.createAccount();
//insert objAcc;

Concept_Note__c objCN = TestClassHelper.createCN();
//objAcc
insert objCN;

Page__c objPg = TestClassHelper.createPage();
objPg.Concept_Note__c = objCN.Id;
insert objPg;

Apexpages.currentpage().getparameters().put('cnId', objCN.Id);
Apexpages.currentpage().getparameters().put('pageId', objCN.Id);
Apexpages.currentpage().getparameters().put('language', 'fr');
//Apexpages.currentpage().getparameters().put('GM','GM');
ApexPages.StandardController stdController = new ApexPages.StandardController(objAcc);
CreatePR objCreatePR = new CreatePR(stdController);
objCreatePR.saveRecord();


}
public static testMethod void createPRRu()
{
Account objAcc = TestClassHelper.createAccount();
//insert objAcc;

Concept_Note__c objCN = TestClassHelper.createCN();
//objAcc
insert objCN;

Page__c objPg = TestClassHelper.createPage();
objPg.Concept_Note__c = objCN.Id;
insert objPg;

Apexpages.currentpage().getparameters().put('cnId', objCN.Id);
Apexpages.currentpage().getparameters().put('pageId', objCN.Id);
Apexpages.currentpage().getparameters().put('language', 'ru');
//Apexpages.currentpage().getparameters().put('GM','GM');
ApexPages.StandardController stdController = new ApexPages.StandardController(objAcc);
CreatePR objCreatePR = new CreatePR(stdController);
objCreatePR.saveRecord();


}




}