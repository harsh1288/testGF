public class InsertDynamicSobjectController  
{  
    public transient blob csvdata {get; set;}
    public String fileName {get; set;}        
    public String ObjectName {get; set;}        
    public InsertDynamicSobjectController()  
    { 
        ObjectName = '' ;  
    }        
    public void UploadData()  
    { 
        ObjectName = fileName.split('_')[0];
        UploadDataBatch ubat = new UploadDataBatch(csvdata,ObjectName);
        Id batprocessId = database.executebatch(ubat,10);   
    }  
}