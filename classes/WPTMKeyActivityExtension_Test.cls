@IsTest
public with sharing class WPTMKeyActivityExtension_Test{

    static testmethod void testmethod_A(){ 
        
        Account tAcc=TestClassHelper.createAccount();
        insert tAcc;
        
        Grant__c tGrant=TestClasshelper.createGrant(tAcc);
        insert tGrant;
        
        Implementation_Period__c tImpPeriod=TestClassHelper.createIP(tGrant,tAcc);
        insert tImpPeriod;
        
        Module__c moduleRec = TestClassHelper.createModule();
        insert moduleRec ;
        
        Catalog_Intervention__c catalogInterventionRec = TestClassHelper.createCatalogIntervention();
        catalogInterventionRec.Is_Other_Intervrntion__c = FALSE;
        insert catalogInterventionRec;
        
        Performance_Framework__c tPF = new Performance_Framework__c ();
        tPF.PF_Status__c = 'Accepted';
        tPF.Implementation_Period__c = tImpPeriod.id; 
        insert tPF;
     
        Grant_Intervention__c tGi=TestClassHelper.createGrantIntervention(tImpPeriod);
        tGi.Performance_Framework__c = tPF.id;
        tGi.Module__c = moduleRec.Id;
        tGi.Catalog_Intervention__c = catalogInterventionRec.Id;
        insert tGi;     
        
        Key_Activity__c tKact = new Key_Activity__c();
        tKact.Activity_Description__c = 'Test';
        tKact.Grant_Intervention__c= tGi.id;        
        insert tKact;
        
        Test.startTest();
        ApexPages.StandardController sc = new ApexPages.standardController(tKact);
        
        //When performanceId!=null        
        WPTMKeyActivityExtension obj = new WPTMKeyActivityExtension(sc);
        apexpages.currentpage().getparameters().put('Performance' , tPF.id);
        obj.saveRecord();
        obj.cancelRecord();        
        obj.refreshGrantInterventionList();
        obj.showModules() ;      
        tGi.Custom_Intervention_Name__c = 'Test custom Name';    
        update tGi;             
        obj = new WPTMKeyActivityExtension(sc);
        
        
        
        obj.saveRecord();
        obj.cancelRecord();
        obj.refreshGrantInterventionList();
        
        Test.stopTest();

    }
}