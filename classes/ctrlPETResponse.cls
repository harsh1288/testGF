public class ctrlPETResponse {

Public String PETid;
Public List<PET_REsponse__c> lstResponse {get;set;}
Public List<Performance_Evaluation__c> PET {get;set;}
Public boolean displayPopup {get; set;}
Public boolean displayPopup1 {get; set;}
Public String message {get;set;}
Public String message1 {get;set;}
Public String ProfileName {get;set;}

List<Performance_Evaluation__c> lstPE = new List<Performance_Evaluation__c>();

    public ctrlPETResponse(ApexPages.StandardController controller) 
    {
        List<Profile> PROFILE = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
        ProfileName = PROFILE[0].Name;
        
        lstResponse = new List<PET_Response__c>();
        PET = new List<Performance_Evaluation__c>();
        PETid = Apexpages.currentpage().getparameters().get('Id');       
        lstServiceId = new Set<id>();
        
        lstPE = [Select Id,LFA_Work_Plan__c, LFA_Service_Submission_Date__c From Performance_Evaluation__c Where id =: PETid];
       
        if(ProfileName=='Finance Officer'){
            lstResponse = [Select Id, Name, Comments__c , Comments_to_LFA__c,Being_edited__c,Mandatory__c, Performance_Evaluation__c, Rating__c,Rating_to_LFA__c,  Type__c from PET_REsponse__c where Performance_Evaluation__c  =:PETid and Type__c =:'LFA Finance'];
        }else if(ProfileName=='M&E Officer'){
            lstResponse = [Select Id, Name, Comments__c , Comments_to_LFA__c,Being_edited__c,Mandatory__c, Performance_Evaluation__c, Rating__c,Rating_to_LFA__c,  Type__c from PET_REsponse__c where Performance_Evaluation__c  =:PETid and Type__c =:'LFA Programmatic/M&E'];
        }else if(ProfileName=='PSM Officer'){
            lstResponse = [Select Id, Name, Comments__c , Comments_to_LFA__c,Being_edited__c,Mandatory__c, Performance_Evaluation__c, Rating__c,Rating_to_LFA__c,  Type__c from PET_REsponse__c where Performance_Evaluation__c  =:PETid and Type__c =:'LFA PSM'];
        }else if(ProfileName=='Legal Officer'){
            lstResponse = [Select Id, Name, Comments__c , Comments_to_LFA__c,Being_edited__c,Mandatory__c, Performance_Evaluation__c, Rating__c,Rating_to_LFA__c,  Type__c from PET_REsponse__c where Performance_Evaluation__c  =:PETid and Type__c =:'LFA Legal'];
        }
        
        /*if(ProfileName=='System Administrator'){
            lstResponse = [Select Id, Name, Comments__c , Comments_to_LFA__c,Being_edited__c,Mandatory__c, Performance_Evaluation__c, Rating__c,Rating_to_LFA__c,  Type__c from PET_REsponse__c where Performance_Evaluation__c  =:PETid and Type__c =:'LFA Finance'];
        }else if(ProfileName=='M&E Officer'){
            lstResponse = [Select Id, Name, Comments__c , Comments_to_LFA__c,Being_edited__c,Mandatory__c, Performance_Evaluation__c, Rating__c,Rating_to_LFA__c,  Type__c from PET_REsponse__c where Performance_Evaluation__c  =:PETid and Type__c =:'LFA Programmatic/M&E'];
        }else if(ProfileName=='PSM Officer'){
            lstResponse = [Select Id, Name, Comments__c , Comments_to_LFA__c,Being_edited__c,Mandatory__c, Performance_Evaluation__c, Rating__c,Rating_to_LFA__c,  Type__c from PET_REsponse__c where Performance_Evaluation__c  =:PETid and Type__c =:'LFA PSM'];
        }else if(ProfileName=='Legal Officer'){
            lstResponse = [Select Id, Name, Comments__c , Comments_to_LFA__c,Being_edited__c,Mandatory__c, Performance_Evaluation__c, Rating__c,Rating_to_LFA__c,  Type__c from PET_REsponse__c where Performance_Evaluation__c  =:PETid and Type__c =:'LFA Legal'];
        }*/
        
        if(lstPE.size() > 0){
            strWorkPlanId = lstPE[0].LFA_Work_Plan__c;
        }
                

        
        if(lstResponse.isEmpty())
        {
            displayPopup1 = true;
            message1 = 'No response for this technical team exists';
        }
        
        for (PET_REsponse__c res1:lstResponse)
        {
        
            if(res1.Being_edited__c==True)
            {
            
                displayPopup = true;
                message = 'This is currently being reviewed by the FPM/PO and cannot be edited.';
            
            }
        
        }
        
         List<Service_PET_Jxn__c> lstPetJunc = new List<Service_PET_Jxn__c>();
        if(PETid != null)
        {
           lstPetJunc = [Select Id, Performance_Evaluation__c,LFA_Service__c from Service_PET_Jxn__c where Performance_Evaluation__c = :PETid];
          // lstPE = [Select Id,LFA_Work_Plan__c From Performance_Evaluation__c Where id =: PETid];
          
        }   
      
      System.debug ('The lstPetJunc is '+ lstPetJunc);   
        
      if(!lstPetJunc.isEmpty())
      {  
        for(Service_PET_Jxn__c junc : lstPetJunc)
        {
            System.debug ('The values is ' + junc.LFA_Service__c);
            lstServiceId.add(junc.LFA_Service__c);
            
            System.debug ('The lstServiceId is ' + lstServiceId);
        }
      } 
        
         PET = [Select Add_Services_Comment__c,Id, Name, start_date__c, end_date__c, LFA_Work_Plan__r.Name, LFA_Work_Plan__r.Start_Date__c,  LFA_Work_Plan__r.End_Date__c,LFA_Service_Submission_Date__c from Performance_Evaluation__c where id =:PETid]; 

            objServiceForType = new LFA_Service__c();
            lstwrapServicesType = new List<wrapServiceType>();
            lstServiceForSearch = new List<wrapSearchService>();
            intIndex = null;
            intIndexST = null;
            blnHasChild = false;
            blnExpandSectionSearch = false;
            blnExpandAll = false;
            blnCollapseAll = true;
            blnShowCTPlanned = false;
            blnShowTGF = true;
            blnCheckBoxExist = true;
            intTemp = 1;
            FillServiceType();
            if(String.IsBlank(strWorkPlanId) == false){
                lstWorkPlan = new List<LFA_Work_Plan__c>();
                lstWorkPlanToUpdate = new List<LFA_Work_Plan__c>();
                lstWorkPlan = [Select id,Name,Country__c,LFA__c,Country__r.Region_name__c,Budgeted_ODC__C,Actual_Labor__c,Budgeted_Labor__C,Total_Budget__C,Country_Team_can_edit__c From LFA_Work_Plan__c Where id =: strWorkPlanId Limit 1];
                if(lstWorkPlan.size() > 0) lstWorkPlanToUpdate.AddAll(lstWorkPlan);
                AnalyseMainGrid();
                         
            }
    


    }
    
    
    public pagereference closePopup()
    {
    
        displayPopup=false;
        return new PageReference('/'+PETid);
        
    
    }
    
    
    public PageReference SaveResponse()
    {
                       
        if(!lstResponse.isEmpty())
        {
        
            try
            {
                for (PET_REsponse__c res: lstResponse)
                {
                
                    res.Status__c = 'In Progress';
                    
                }
                
                update lstResponse;
            }
            catch (exception e)
            {             
            }
            
            return new PageReference('/'+PETid);
        
        }
        
        else
        {
        return null;
        }        
                           
    }
    
    
    
  public PageReference SubmitResponse()
    {
                       
        if(!lstResponse.isEmpty())
        {
        
            try
            {
                for (PET_REsponse__c res: lstResponse)
                {
                System.debug('Rating:' + res.rating__c);
                  if((res.Mandatory__c == 'Yes') && (res.rating__c == null || res.Comments__c == null))
                  {
                    res.rating__c.addError('You must Select a rating and comment before clicking "Send To FPM"');
                    return null;
                  
                  }
                  
                  if((res.Mandatory__c == null || res.Mandatory__c == 'NO') && (res.rating__c == '1' || res.rating__c == '2')&&(res.Comments__c == null))
                   {
                    res.Comments__c.addError('Comment is required before clicking "Send To FPM"');
                    return null;
                  
                  }
                
                    res.Status__c = 'Completed';
                    
                }
                
                update lstResponse;
            }
            catch (exception e)
            {             
            }
            
            return new PageReference('/'+PETid);
        
        }
        
        else
        {
        return null;
        }        
                           
    }
    
     //------------------------------------------------------------------------------------------------------------------------------------------------//
     
   public pageReference viewService()
   {
    blnHasChild = true;
    return null;
   }
   
   public pageReference HideService()
   {
    blnHasChild = false;
    return null;
   }
        
     
     Final String STARTYEAR = '2013';
    Public String strWorkPlanId;
    
    Public List<wrapServiceType> lstwrapServicesType {get; set;}
    Public Decimal CurrentLOE {get; set;}
    Public Decimal ActualLOE {get; set;}
    Public Decimal LFAProposedLOE {get; set;}
    Public Decimal CurrentCost {get; set;}
    Public Decimal CTPlannedLOE {get; set;}
    Public Decimal CTPlannedCost {get; set;}
    Public Decimal TGFProposedLOE {get; set;}
    Public Decimal TGFProposedCost {get; set;}
    Public List<SelectOption> ServiceTypeOptions {get; private set;}
    Public List<SelectOption> ServiceSubTypeOptions {get; private set;}
    Public List<SelectOption> ServiceSubTypeFilterOptions {get; private set;}
    Public List<SelectOption> ServiceSubTypeSearchingOptions {get; private set;}
    Public List<SelectOption> GrantOptions {get; private set;}
    Public List<SelectOption> RoleOptions {get; private set;}
    
   
    Public List<SelectOption> LFAOptions {get; private set;}
 
    Public String SelectedLFA {get; set;}
     
    Public String SelectedGrantMain {get; set;}
   
    Public List<wrapSearchService> lstServiceForSearch {get; set;}
 
    Public Boolean blnHasChild {get; set;}
    Public Boolean blnExpandSectionSearch  {get; set;}
    Public Integer intIndex {get; set;}
    Public Integer intIndexST {get; set;}
    
    Public List<String> lstServiceType {get; set;}
    
    Public Boolean blnExpandAll {get; set;}
    Public Boolean blnCollapseAll {get; set;}
    Public Boolean blnShowCTPlanned {get; set;}
    Public Boolean blnShowTGF {get; set;}
    Public Integer intTemp {get;set;}
    Public String strMessage {get;set;}
    Public List<LFA_Work_Plan__c> lstWorkPlanToUpdate {get;set;}
    Public List<LFA_Work_Plan__c> lstWorkPlan;
    Public LFA_Service__c objServiceForType{get;set;}
    Public String  strPEId {get;set;}
    Public Boolean blnCheckBoxExist {get;set;}
    public set<Id> lstServiceId {get;set;}
   
    
    /********************************************************************
        Name : AnalyseMainGrid
        Purpose : Used for search services in main grid.
    ********************************************************************/
        
    Public Void AnalyseMainGrid(){
        List<LFA_Service__c> lstService = new List<LFA_Service__c>();
        
        Set<Id> setServiceId = new Set<id>();
        List<Service_PET_Jxn__c> lstServicePET = [Select LFA_Service__c,Performance_Evaluation__c 
                                            From Service_PET_Jxn__c Where Performance_Evaluation__c =: PETid];
        if(lstServicePET.size() > 0){
            for(Service_PET_Jxn__c obj : lstServicePET){
                if(obj.LFA_Service__c != null){
                    setServiceId.add(obj.LFA_Service__c);
                }
            }          
        } 
        
        lstwrapServicesType = new List<wrapServiceType>();
         String Query = 'Select Id, Actual_LOE__c,LFA_Proposed_LOE__c,Name,Alert_check_grant__c,LastmodifiedDate,LastmodifiedBy.Name,Has_Resorces__c,CT_planned_LOE_rollup__c,CT_planned_cost_rollup__c,num_resources__c,has_TBD_contact__c,TBD_LOE_Zero__c,Grant__c, Grant__r.Start_Date__c, Grant__r.End_Date__c, Grant__r.Name,LOE__c,Cost__c,CT_Planned_Cost__c, CT_Planned_LOE__c,LFA_Work_Plan__r.Country__r.Name,Service_Type__c,Service_Sub_Type__c,LFA_Work_Plan__r.Country__c,TGF_Proposed_Cost__c,TGF_Proposed_LOE__c,Status__c,Period_Start__c, Period_End__c, Period_End_Year__c,Period_Start_Year__c,Due_Date__c,Forecasted_Start_Date__c,Forecasted_End_Date__c,Grant__r.Grant_Status__c, Grant__r.Principal_Recipient__r.Name, Grant__r.Financial_and_Fiduciary_Risks__c,Grant__r.Health_Services_and_Products_Risks__c,Grant__r.Programmatic_and_Performance_Risks__c,Other_Sub_Category__c,Submission_Date__c,PU_DR_Number__c,Previously_Selected_for_PET__c,(Select Id,Actual_Cost__c,Actual_LOE__c,LFA_Proposed_LOE__c,Cost__c,LFA_Role__c,LFA_Role__r.Name,CT_Planned_LOE__c,CT_Planned_Cost__c,LFA_Service__c, LOE__c,Rate__c,Contact__c,Contact__r.Name From LFA_Resources__r Order By LFA_Role__r.Sort_Order__c ASC) From LFA_Service__c where LFA_Work_Plan__c = :strWorkPlanId';
        
       
       if(objServiceForType != null && objServiceForType.Service_Sub_Type__c != null){
            system.debug('#######'+objServiceForType.Service_Sub_Type__c);
            Query += ' And Service_Sub_Type__c =\''+ objServiceForType.Service_Sub_Type__c+'\'';
        }
        if(objServiceForType != null && objServiceForType.Service_Type__c != null){
            system.debug('#######'+objServiceForType.Service_Type__c);
            Query += ' And Service_Type__c =\''+ objServiceForType.Service_Type__c+'\'';
        }
        
        if(String.Isblank(SelectedGrantMain) == false){
            Query += ' And Grant__c =: SelectedGrantMain';
        }
        
        Query += ' And Id in : lstServiceId';
        
        
        Query += ' Order By Service_Type__c,Grant__c';
        lstService = Database.Query(Query);
        
        System.debug('The list of services is ' + lstService);
        
      //  lstService = [Select Id, Name,Alert_check_grant__c,(Select Id,Actual_Cost__c,Actual_LOE__c,Cost__c,LFA_Role__c,LFA_Role__r.Name,CT_Planned_LOE__c,CT_Planned_Cost__c,LFA_Service__c, LOE__c,Rate__c,Contact__c,Contact__r.Name From LFA_Resources__r Order By LFA_Role__r.Sort_Order__c ASC),LastmodifiedDate,LastmodifiedBy.Name,Has_Resorces__c,CT_planned_LOE_rollup__c,CT_planned_cost_rollup__c,num_resources__c,has_TBD_contact__c,TBD_LOE_Zero__c,Grant__c, Grant__r.Start_Date__c, Grant__r.End_Date__c, Grant__r.Name,LOE__c,Cost__c,CT_Planned_Cost__c, CT_Planned_LOE__c,LFA_Work_Plan__r.Country__r.Name,Service_Type__c,Service_Sub_Type__c,LFA_Work_Plan__r.Country__c,TGF_Proposed_Cost__c,TGF_Proposed_LOE__c,Status__c,Period_Start__c, Period_End__c, Period_End_Year__c,Period_Start_Year__c,Due_Date__c,Forecasted_Start_Date__c,Forecasted_End_Date__c,Grant__r.Grant_Status__c, Grant__r.Principal_Recipient__r.Name, Grant__r.Financial_and_Fiduciary_Risks__c,Grant__r.Health_Services_and_Products_Risks__c,Grant__r.Programmatic_and_Performance_Risks__c,Other_Sub_Category__c From LFA_Service__c where id in :lstServiceId and LFA_Work_Plan__c = : strWorkPlanId];
        
        //---------------jl-----------------------------------//
        
       Map<String,DateTime> mapLfaFeed = new Map<String,DateTime>();
         for(LFA_Service__Feed lstserv : [Select Id,ParentId,LastModifiedDate from LFA_Service__Feed order by LastModifiedDate asc])
          {
               mapLfaFeed.put(lstserv.ParentId,lstserv.LastModifiedDate);
               System.debug('Check Map:-' + mapLfaFeed);
          
          }
        
        if(setServiceId.size() > 0)
        {
            if(lstService != null && lstService.size() > 0){
                for(LFA_Service__c objS : lstService){
                    if(setServiceId.contains(objS.Id)){
                        blnCheckBoxExist = false;
                    }
                }
            }
        }
        
        
        //----------------------------------------------------//
        
        for(String strServiceType:lstServiceType){
            wrapServiceType objwrapST = new wrapServiceType();
            objwrapST.lstwrapServices = new List<wrapServices>();
            objwrapST.ServiceType = strServiceType;
            if(lstService != null && lstService.size() > 0){
                for(LFA_Service__c objS : lstService){
                    if(objS.Service_Type__c == strServiceType){
                        objwrapST.blnExpandSection = true;
                        wrapServices objWrap = new wrapServices();
                        objWrap.objService = new LFA_Service__c();
                        objWrap.objService = objS;
                        objWrap.lstwrapResource = new List<wrapResource>();
                        objWrap.blnExpandResourceSection = false;
                        objWrap.blnNoResourceInfo = true;
                        
                        if(mapLfaFeed.containskey(objS.Id))
                        {
                          objWrap.blnServCommentFeed = true;
                          objWrap.ServCommentLM_date = mapLfaFeed.get(objS.Id);
                        }  
                        else
                          objWrap.blnServCommentFeed = false;
                        
                        if(objS.LFA_Resources__r.size() > 0){
                            objWrap.blnNoResourceInfo = false;
                            for(LFA_Resource__c objRes :objS.LFA_Resources__r){
                                wrapResource objWarpResource = new wrapResource();
                                objWarpResource.objResource = new LFA_Resource__c();
                                objWarpResource.objResource = objRes;
                                if(objRes.Contact__c != null)
                                    objWarpResource.ContactName = objRes.Contact__r.Name;
                                else
                                    objWarpResource.ContactName = 'TBD';
                                objWarpResource.ContactID = objRes.Contact__c;
                                objWarpResource.RoleName = objRes.LFA_Role__r.Name;
                                objWarpResource.ContactOptions = new List<SelectOption>();
                                /*if(objWarpResource.ContactName == 'TBD' && objRes.LOE__c != 0)
                                     objWarpResource.blnTBDContactLOEZeroInfo = true;
                                else objWarpResource.blnTBDContactLOEZeroInfo = true;*/
                                if(objWarpResource.ContactName == 'TBD')
                                    objWarpResource.ContactOptions.add(new SelectOption('',objWarpResource.ContactName));
                                else
                                    objWarpResource.ContactOptions.add(new SelectOption(objWarpResource.ContactID,objWarpResource.ContactName));
                                objWrap.lstwrapResource.add(objWarpResource);
                            }
                        }
                        objwrapST.lstwrapServices.add(objWrap);
                    }
                }
            }
            lstwrapServicesType.add(objwrapST);
        }
        CalculateTotal();
        blnExpandAll = false;
        blnCollapseAll = true;
    }
    
  
    /********************************************************************
        Name : ExapndAllServiceType
        Purpose : Used for expand all service type.
    ********************************************************************/
    
    Public Void ExapndAllServiceType(){
        for(wrapServiceType objWrapST : lstwrapServicesType){
            objwrapST.blnExpandSection = true;
            for(wrapServices objS : objWrapST.lstwrapServices){
                if(objS.lstWrapResource.size() > 0) objS.blnExpandResourceSection = true;
            }
        }
        blnExpandAll = true;
        blnCollapseAll = false;
    }
    
    /********************************************************************
        Name : CollepseAllServiceType
        Purpose : Used for collepse all service type.
    ********************************************************************/
    
    Public Void CollepseAllServiceType(){
        for(wrapServiceType objWrapST : lstwrapServicesType){
            objwrapST.blnExpandSection = true;
            for(wrapServices objS : objWrapST.lstwrapServices){
                objS.blnExpandResourceSection = false;
            }
        }
        blnExpandAll = false;
        blnCollapseAll = true;
    }
    
    /********************************************************************
        Name : CalculateTotal
        Purpose : Used for calculate total of all services.
    ********************************************************************/
    
    Public void CalculateTotal(){
        if(lstwrapServicesType.size() > 0){
            CurrentLOE = null;
            ActualLOE = null;
            LFAProposedLOE = null;
            CurrentCost = null;
            CTPlannedLOE = null;
            CTPlannedCost = null;
            TGFProposedLOE = null;
            TGFProposedCost = null;
            for(wrapServiceType objWrapST : lstwrapServicesType){
                objWrapST.TotalCurrentLOE = null;
                objWrapST.TotalActualLOE = null;
                objWrapST.TotalLFAProposedLOE = null;
                objWrapST.TotalCurrentCost = null;
                objWrapST.TotalCTPlannedLOE = null;
                objWrapST.TotalCTPlannedCost = null;
                objWrapST.TotalTGFProposedLOE = null;
                objWrapST.TotalTGFProposedCost = null;
                for(wrapServices objS : objWrapST.lstwrapServices){
                    if(objS.objService.LOE__c != null){
                        if(objWrapST.TotalCurrentLOE == null) objWrapST.TotalCurrentLOE = objS.objService.LOE__c;
                        else objWrapST.TotalCurrentLOE += objS.objService.LOE__c;
                        if(objWrapST.TotalActualLOE  == null) objWrapST.TotalActualLOE  = objS.objService.Actual_LOE__c;
                        else objWrapST.TotalActualLOE  += objS.objService.Actual_LOE__c;
                        if(objWrapST.TotalLFAProposedLOE == null) objWrapST.TotalLFAProposedLOE = objS.objService.LFA_Proposed_LOE__c;
                        else objWrapST.TotalLFAProposedLOE  += objS.objService.LFA_Proposed_LOE__c;
                    }
                    if(objS.objService.Cost__c != null){
                        if(objWrapST.TotalCurrentCost == null) objWrapST.TotalCurrentCost = objS.objService.Cost__c;
                        else objWrapST.TotalCurrentCost += objS.objService.Cost__c;
                    }
                    if(objS.objService.CT_Planned_Cost_rollup__c != null){
                        if(objWrapST.TotalCTPlannedCost == null) objWrapST.TotalCTPlannedCost = objS.objService.CT_Planned_Cost_rollup__c;
                        else objWrapST.TotalCTPlannedCost += objS.objService.CT_Planned_Cost_rollup__c;
                    }
                    if(objS.objService.CT_Planned_LOE_rollup__c != null){
                        if(objWrapST.TotalCTPlannedLOE == null) objWrapST.TotalCTPlannedLOE = objS.objService.CT_Planned_LOE_rollup__c;
                        else objWrapST.TotalCTPlannedLOE += objS.objService.CT_Planned_LOE_rollup__c;
                    }
                    if(objS.objService.TGF_Proposed_Cost__c != null){
                        if(objWrapST.TotalTGFProposedCost == null) objWrapST.TotalTGFProposedCost = objS.objService.TGF_Proposed_Cost__c;
                        else objWrapST.TotalTGFProposedCost += objS.objService.TGF_Proposed_Cost__c;
                    }
                    if(objS.objService.TGF_Proposed_LOE__c != null){
                        if(objWrapST.TotalTGFProposedLOE == null) objWrapST.TotalTGFProposedLOE = objS.objService.TGF_Proposed_LOE__c;
                        else objWrapST.TotalTGFProposedLOE += objS.objService.TGF_Proposed_LOE__c;
                    }
                }
                if(objWrapST.TotalCurrentLOE != null){
                    if(CurrentLOE == null) CurrentLOE = objWrapST.TotalCurrentLOE;
                    else CurrentLOE += objWrapST.TotalCurrentLOE;
                }
                if(objWrapST.TotalActualLOE != null){
                    if(ActualLOE == null) ActualLOE = objWrapST.TotalActualLOE;
                    else ActualLOE += objWrapST.TotalActualLOE;
                }
                if(objWrapST.TotalLFAProposedLOE != null){
                    if(LFAProposedLOE == null) LFAProposedLOE = objWrapST.TotalLFAProposedLOE;
                    else LFAProposedLOE += objWrapST.TotalLFAProposedLOE;
                }
                if(objWrapST.TotalCurrentCost != null){
                    if(CurrentCost == null) CurrentCost = objWrapST.TotalCurrentCost;
                    else CurrentCost += objWrapST.TotalCurrentCost;
                }
                if(objWrapST.TotalCTPlannedLOE != null){
                    if(CTPlannedLOE == null) CTPlannedLOE = objWrapST.TotalCTPlannedLOE;
                    else CTPlannedLOE += objWrapST.TotalCTPlannedLOE;
                }
                if(objWrapST.TotalCTPlannedCost != null){
                    if(CTPlannedCost == null) CTPlannedCost = objWrapST.TotalCTPlannedCost;
                    else CTPlannedCost += objWrapST.TotalCTPlannedCost;
                }
                if(objWrapST.TotalTGFProposedLOE != null){
                    if(TGFProposedLOE == null) TGFProposedLOE = objWrapST.TotalTGFProposedLOE;
                    else TGFProposedLOE += objWrapST.TotalTGFProposedLOE;
                }
                if(objWrapST.TotalTGFProposedCost != null){
                    if(TGFProposedCost == null) TGFProposedCost = objWrapST.TotalTGFProposedCost;
                    else TGFProposedCost += objWrapST.TotalTGFProposedCost;
                }
            }
        }
    }
    
   
    /********************************************************************
        Name : FillServiceType
        Purpose : Used for fill service type picklist.
    ********************************************************************/
    
    Public void FillServiceType(){
        ServiceTypeOptions = new List<SelectOption>();
        ServiceSubTypeOptions = new List<SelectOption>();
        ServiceSubTypeFilterOptions = new List<SelectOption>();
        ServiceSubTypeSearchingOptions  = new List<SelectOption>();
        lstServiceType = new List<String>();
        Schema.sObjectType objType = LFA_Service__c.getSObjectType();
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe(); 
        map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap(); 
        List<Schema.PicklistEntry> ServiceTypes = fieldMap.get('Service_Type__c').getDescribe().getPickListValues();
        List<Schema.PicklistEntry> ServiceSubTypes = fieldMap.get('Service_Sub_Type__c').getDescribe().getPickListValues();
        ServiceTypeOptions.add(new SelectOption('','All Service Types'));
        ServiceSubTypeSearchingOptions.add(new SelectOption('','All Service Sub Types'));
        ServiceSubTypeFilterOptions.add(new SelectOption('','All Service Sub Types'));
        for (Schema.PicklistEntry Entry : ServiceTypes){ 
            ServiceTypeOptions.add(new SelectOption(Entry.getLabel(), Entry.getValue()));
            lstServiceType.add(Entry.getValue());
        }
        for (Schema.PicklistEntry Entry : ServiceSubTypes){ 
            ServiceSubTypeOptions.add(new SelectOption(Entry.getLabel(), Entry.getValue()));
            ServiceSubTypeFilterOptions.add(new SelectOption(Entry.getLabel(), Entry.getValue()));
            ServiceSubTypeSearchingOptions.add(new SelectOption(Entry.getLabel(), Entry.getValue()));
        }
    }
    
     
    Public Integer intOffsetHeight {get; set;}
    Public Integer intHeight {get; set;}
    
   
    /********************************************************************
        Name :  RetriveResourceValues
        Purpose : Used for retrive resource values after save resource from pop-up
    ********************************************************************/
    
    Public void RetriveResourceValues(){
        if(intIndex != null){
            List<LFA_Service__c> lstService = [Select Id, Name, Grant__c, Grant__r.Name,LOE__c,Cost__c,LFA_Proposed_LOE__c,
                                                    CT_Planned_Cost__c, CT_Planned_LOE__c,LFA_Work_Plan__r.Country__r.Name, 
                                                    Service_Type__c,Service_Sub_Type__c,LFA_Work_Plan__r.Country__c,Period_Start__c,Period_End__c,Period_Start_Year__c, Period_End_Year__c, Due_Date__c, Forecasted_Start_Date__c,Forecasted_End_Date__c,
                                                    TGF_Proposed_Cost__c,TGF_Proposed_LOE__c,Status__c,Alert_check_grant__c,Other_Sub_Category__c,
                                                    Has_Resorces__c,num_resources__c,has_TBD_contact__c,TBD_LOE_Zero__c,Grant__r.Grant_Status__c,
                                                    Grant__r.Financial_and_Fiduciary_Risks__c,Grant__r.Health_Services_and_Products_Risks__c,CT_planned_LOE_rollup__c,CT_planned_cost_rollup__c,
                                                    Grant__r.Programmatic_and_Performance_Risks__c,Grant__r.Principal_Recipient__r.Name,Submission_Date__c,PU_DR_Number__c,Previously_Selected_for_PET__c,
                                                    (Select Id,Actual_Cost__c,Actual_LOE__c,LFA_Proposed_LOE__c,Cost__c,LFA_Role__c,LFA_Role__r.Name,CT_Planned_LOE__c,CT_Planned_Cost__c,
                                                        LFA_Service__c, LOE__c,Rate__c,Contact__c,Contact__r.Name From LFA_Resources__r Order By LFA_Role__r.Sort_Order__c ASC) 
                                                    From LFA_Service__c Where id=:lstwrapServicesType[intIndexST].lstwrapServices[intIndex].objService.id 
                                                    and LFA_Work_Plan__c = :strWorkPlanId Order By Service_Type__c,Grant__c];
            
            lstwrapServicesType[intIndexST].lstwrapServices[intIndex].objService = new LFA_Service__c();
            lstwrapServicesType[intIndexST].lstwrapServices[intIndex].objService = lstService[0];
            lstwrapServicesType[intIndexST].lstwrapServices[intIndex].lstwrapResource = new List<wrapResource>();
            lstwrapServicesType[intIndexST].lstwrapServices[intIndex].blnNoResourceInfo = true;
            
            if(lstService[0].LFA_Resources__r.size() > 0){
                lstwrapServicesType[intIndexST].lstwrapServices[intIndex].blnNoResourceInfo = false;
                for(LFA_Resource__c objR:lstService[0].LFA_Resources__r){
                    wrapResource objw = new wrapResource();
                    objw.objResource = objR;
                    objw.RoleName = objR.LFA_Role__r.Name;
                    if(objR.Contact__c != null)
                        objw.ContactName = objR.Contact__r.Name;
                    else
                        objw.ContactName = 'TBD';
                    if(objw.ContactName == 'TBD' && objR.LOE__c != 0)
                         objw.blnTBDContactLOEZeroInfo = true;
                    else objw.blnTBDContactLOEZeroInfo = false;
                    objw.ContactID = objR.Contact__c;
                    lstwrapServicesType[intIndexST].lstwrapServices[intIndex].lstwrapResource.add(objw);
                }
            }
        }
    }  
    
    Public String serviceId {get;set;}    
   
  
 
    /********************************************************************
        Name :  getFieldsName
        Purpose : Used for retrive fields name as selected Object.
    ********************************************************************/
    
    Public String getFieldsName(String objName){
        String FieldNames = null;
        Map<String, Schema.SObjectField> mapFields = Schema.getGlobalDescribe().get(objName).getDescribe().fields.getMap();
        for(Schema.SObjectField sfield : mapFields.Values()){
            schema.describefieldresult field = sfield.getDescribe();
            if(field.iscustom() == true){
                if(FieldNames == null) FieldNames = field.getName()+',';
                else FieldNames += field.getName()+',';
            }
        }
        return FieldNames;
    }
    
  
    /********************************************************************
        Wrapper Class : wrapServices
    ********************************************************************/
    
    Public Class wrapServices{
        Public LFA_Service__c objService {get; set;}
        Public List<wrapResource> lstwrapResource {get; set;}
        Public Boolean blnExpandResourceSection {get; set;}
        Public Boolean blnNoResourceInfo {get; set;}
        public Boolean blnServCommentFeed {get;set;}
        public DateTime ServCommentLM_date {get;set;}
        Public Boolean blnHasServicePETJxn {get;set;}
    }
    
    /********************************************************************
        Wrapper Class : wrapResource
    ********************************************************************/
    Public Class wrapResource{
        Public LFA_Resource__c objResource {get; set;}
        Public List<SelectOption> ContactOptions {get; set;}
        Public String ContactID {get; set;}
        Public String ContactName {get; set;}
        Public String RoleName {get; set;}
        Public Boolean blnTBDContactLOEZeroInfo {get; set;}
    }
    
    /********************************************************************
        Wrapper Class : wrapServiceType
    ********************************************************************/
    
    Public Class wrapServiceType{
        Public String ServiceType {get;set;}
        Public List<wrapServices> lstwrapServices {get; set;}
        Public Boolean blnExpandSection {get; set;}
        Public Decimal TotalCurrentLOE {get; set;}
        Public Decimal TotalActualLOE {get; set;}
        Public Decimal TotalLFAProposedLOE {get; set;}
        Public Decimal TotalCurrentCost {get; set;}
        Public Decimal TotalCTPlannedLOE {get; set;}
        Public Decimal TotalCTPlannedCost {get; set;}
        Public Decimal TotalTGFProposedLOE {get; set;}
        Public Decimal TotalTGFProposedCost {get; set;}
    }
    ////////////////////// Search Grid /////////////////////////////
    
    /********************************************************************
        Name :  wrapSearchService
        Purpose : Used for search service.
    ********************************************************************/
    
    Public Class wrapSearchService{
        Public LFA_Service__c objSearchService {get; set;}
        Public Boolean IsChecked {get; set;}
    }
    


    
    

}