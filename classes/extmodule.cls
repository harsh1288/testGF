public  class extmodule {
     public string moduleid{get;set;}
     public List<Catalog_Intervention__c> lstintrvntn{get;set;} 
     public List<wrapcatintervention> wlstintrvntn{get;set;}
     public List<Module__c> lstmodule;
     public List<Id> mid = new List<Id>();
     public List<Catalog_Intervention__c> selectedintrvntns{get;set;} 
     public List<Intervention__c> lstcnintrvntn = new List<Intervention__c>();
     public List<id> catintrvntns = new List<id>();
     public List<Intervention__c> lstcnintrvntns = new List<Intervention__c>();
     public List<id> mids = new List<ID>();
     public List<Grant_Intervention__c> lstgintr= new List<Grant_Intervention__c>();
     public List<Grant_Intervention__c> lstgintrs= new List<Grant_Intervention__c>();
     public List<Id> gintrid = new List<Id>();
     public string message{get;set;}
     public boolean display{get;set;}
     public boolean display1{get;set;}
     public list<RecordType> lstrec = new List<RecordType>();
     public List<selectoption> intrname{get;set;} 
     public string strnintrnvntnname{get;set;}
     public List<Implementation_Period__c> lstgip;
     public list<Grant_Intervention__c > lstgin = new List<Grant_Intervention__c >();
     public string cnid;
     list<string> prname = new List<string>();
     public List<Grant_Intervention__c> lstgins = new List<Grant_Intervention__c>();
     public List<selectoption> lstprs {get;set;}
     public List<string> selectpr{get;set;} 
     public list<implementation_period__c> selectedprs= new List<implementation_period__c>();
     //public List<RecordType> lstrec = new List<RecordType>();
     List<RecordType> lstrecmod = new List<RecordType>();
     public List<RecordType> lstreccn = new List<RecordType>();
     public List<Module__c> newlstmod = new List<Module__c>();
     public list<Grant_Intervention__c> lstgincn = new List<Grant_Intervention__C>();
     public string impid{get;set;}
     public boolean displayintrvntn{get;set;}
     public list<Module__c> lstmodip = new List<Module__c>();
     List<Id> impidnew = new List<Id>();
     public string intrvntnname{get;set;}
     public boolean displayothername{get;set;}
     public Map<String,Grant_Intervention__c> mcustintrnametogint = new Map<String,Grant_Intervention__c>();

     
     public extmodule(ApexPages.StandardController controller) {
        displayothername= false;
       //getting Moduleid
         moduleid =ApexPages.currentpage().getparameters().get('id');
         lstmodule = new List<Module__c>();
       //Listing Module record
         lstmodule = [select id,Name,Catalog_Module__c,Catalog_Module__r.id,Concept_note__c,Concept_Note__r.Component__c,Implementation_Period__c,Performance_Framework__c from Module__c where id =: moduleid];
       //getting Concept Note Id 
         cnid=lstmodule[0].Concept_note__c;
         if(cnid == Null){
          displayintrvntn = true;
       //getting Implementation Period Id if it is from Imp
          impid =  lstmodule[0].Implementation_Period__c; 
         }
         
         for(Module__c mod : lstmodule){
             mid.add(mod.Catalog_Module__r.id);
             mids.add(mod.id);
         }   
         /*lstcnintrvntns = [select id,Name,Intervention__c from Intervention__c where Module_rel__c In: mids ];
         for(Intervention__c  intr :lstcnintrvntns ){
         catintrvntns.add(intr.Intervention__c);
         }*/
       //Getting list of Intervetions already available for this Module
         lstgintrs = [select id,Name,Catalog_Intervention__c,Custom_Intervention_Name__c from Grant_Intervention__c where Module__c in:mids and Performance_Framework__c =: lstmodule[0].Performance_Framework__c and Catalog_Intervention__r.Is_Other_Intervrntion__c !=: true ];
         for(Grant_Intervention__c  gintr : lstgintrs ){
              gintrid.add(gintr.Catalog_Intervention__c);
         }
         
        //Getting List Of Other Interventions 
        lstgintrs = [select id,Name,Catalog_Intervention__c,Custom_Intervention_Name__c from Grant_Intervention__c where Module__c in:mids and Performance_Framework__c =: lstmodule[0].Performance_Framework__c and Catalog_Intervention__r.Is_Other_Intervrntion__c =: true ];
        for(Grant_Intervention__c  gintr : lstgintrs ){
              mcustintrnametogint.Put(gintr.Custom_Intervention_Name__c,gintr);
         }
         
        //Getting List of catalog Intervetions which had same catalog module 
         lstintrvntn =[select id,Name,Catalog_Module__c,Description__c,Component_Multi__c,French_Description__c,French_Name__c,Russian_Description__c,Russian_Name__c,Spanish_Description__c,Spanish_Name__c,Is_Other_Intervrntion__c from Catalog_Intervention__c where Catalog_Module__c In:mid AND id NOT IN:catintrvntns And id NOT IN:gintrid  ];
         
         system.debug('@@@@@'+lstintrvntn);
         if(lstintrvntn.size()== 0){
            display=true;
            message = 'No Interventions available';
         }
       
         //lstimp=[select id,Name,Principal_Recipient__c,Principal_Recipient__r.Name from Implementation_Period__c where id =:lstmodule[0].Implementation_Period__c];
       
         else{
       //If it is from Concept Note
            display=false;
          //Intervention select List
            if(impid == Null){ 
              intrname = new List<selectoption>();
              intrname.add(new SelectOption('','--None--'));
              if(lstintrvntn.size() > 0){
                 for(Catalog_Intervention__c objCat : lstintrvntn){
                     intrname.add(new SelectOption(objCat.id,objCat.Name));
                 }
              }
           // prs selectlist
             lstgins =[select id,name,Pr1__c,Module__c from Grant_Intervention__c where Concept_Note__c=:cnid and Module__c=:moduleid];
             if(lstgins.size()>0){
               for(Grant_Intervention__c objgin:lstgins){
                   prname.add(objgin.Pr1__c);
               }
             }
             lstgip = [select id,Principal_Recipient__c,Principal_Recipient__r.Name from Implementation_Period__c where Concept_Note__c =: cnid ];
                if(lstgip.size()==0){
                   display1=true;
                   message='No PRs available for this Concept Note';
                }
                else{
                   display1=false;
                   lstprs = new List<selectoption>();
                   lstprs.add(new SelectOption('','--None--'));
                   
                   if(lstprs.size() > 0){
                    
                      for(Implementation_period__c objimp : lstgip){
                          lstprs.add(new SelectOption(objimp.Principal_Recipient__c,objimp.Principal_Recipient__r.Name));
                      system.debug('lstprs'+lstprs);
                      }
                   }
                }
              }
             else{
          //If it is from Grant-Making
               if(wlstintrvntn == Null){
                  wlstintrvntn = new List<wrapcatintervention>(); 
                    for(Catalog_Intervention__c r : lstintrvntn){
                        wlstintrvntn.add(new wrapcatintervention(r));
                    }
               }
             }
         }
      
        // system.Debug('@@@@@'+wlstintrvntn);
       // lstrec=[Select Id,SobjectType,Name From RecordType where Name = 'Grant-Making' and SobjectType = 'Module__c'  limit 1];   
        lstrec = [Select Id,SobjectType,Name From RecordType where Name = 'Grant-Making' and SobjectType = 'Grant_Intervention__c'  limit 1];
      lstreccn = [Select Id,SobjectType,Name From RecordType where Name = 'Concept Note' and SobjectType = 'Grant_Intervention__c'  limit 1];
      lstrecmod = [Select Id,SobjectType,Name From RecordType where Name = 'Grant-Making' and SobjectType = 'Module__c'  limit 1];
    }
    
    public pageReference save(){
        system.debug('@@@@'+mcustintrnametogint);
        system.debug('@@@@'+intrvntnname);
       selectedintrvntns = new List<Catalog_Intervention__c>();
        if(cnid==Null){
           for(wrapcatintervention objwintrvntn : wlstintrvntn){
              if((objwintrvntn.catintrvntn.Is_Other_Intervrntion__c == true) && intrvntnname == '' && objwintrvntn.selected == true){
                 ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please fill Intervention name');
                 ApexPages.addMessage(myMsg); 
                 displayothername = true;
                 return null; 
              }
              else if((objwintrvntn.catintrvntn.Is_Other_Intervrntion__c == true) && intrvntnname != '' && objwintrvntn.selected == true && mcustintrnametogint.containsKey(intrvntnname)){
                      ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Duplicate Custom Intervention Name.Please Enter different custom Intervention Name.');
                      ApexPages.addMessage(myMsg); 
                      displayothername = true;
                      return null;
              }
              else if(objwintrvntn.selected == true){
                  selectedintrvntns.add(objwintrvntn.catintrvntn);
               }
            } 
            if(selectedintrvntns.size()==0){
              displayothername = false;
              ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please select atleast one Intervention');
              ApexPages.addMessage(myMsg); 
              return null;
            }
            else{
            //creating Interventions for Implementation Period
                system.debug('@@'+intrvntnname);
                system.debug('@@'+impid);
                system.debug('@@'+Moduleid);
                system.debug('@@'+selectedintrvntns[0].id);
                system.debug('@@'+lstrec[0].id);
                system.debug('@@'+lstmodule[0].Performance_Framework__c);
            for(Catalog_Intervention__c cin :selectedintrvntns ){
                Grant_Intervention__c objgintr = new Grant_Intervention__c();
                     if(cin.Is_Other_Intervrntion__c == true ){
                        objgintr.Custom_Intervention_Name__c = intrvntnname;
                        objgintr.Name = cin.Name;
                     }
                     else{
                        objgintr.Name = cin.Name;
                     }
                     objgintr.Implementation_Period__c = impid;
                     objgintr.Module__c = Moduleid;
                     objgintr.Catalog_Intervention__c = cin.id;
                     objgintr.Recordtypeid=lstrec[0].id;
                     objgintr.Performance_Framework__c =lstmodule[0].Performance_Framework__c ;
                     lstgintr.add(objgintr);
             }
             insert lstgintr;
             pageReference pr = new pageReference('/'+Moduleid);
             pr.setRedirect(true);
             return pr;
             }
             } 
        //creating Interventions for Concep Note
        else{
         selectedintrvntns=[select id,Name,Description__c,French_Name__c,Russian_Name__c,Spanish_Name__c,Spanish_Description__c,Russian_Description__c,French_Description__c from Catalog_Intervention__c where id=:strnintrnvntnname];
        //prselectedlist
        
        selectedprs =[select id,Principal_Recipient__c,Principal_Recipient__r.Name from Implementation_Period__c where Principal_Recipient__c in:selectpr and concept_Note__c=:cnid ];
        //for(Implementation_Period__c objimp:selectedprs){
           // impidnew.add(objimp.id);
        //}
        //system.debug('@@@@'+selectedprs[0].id );
        //system.debug('@@@@'+selectedprs[1].id );
        if(selectedintrvntns.size()==0){
           ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please select atleast one Intervention');
           ApexPages.addMessage(myMsg); 
           return null;
        }
        if(selectedprs.size()==0){
           ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please select atleast one PR');
           ApexPages.addMessage(myMsg); 
           return null;
        }
        
        
        else{
              for(Implementation_Period__c intr :selectedprs ){
                     Module__c objmod = new Module__c();
                       objmod.Name = lstmodule[0].Name;
                       objmod.Implementation_Period__c = intr.id;
                       objmod.Component__c = lstmodule[0].Concept_Note__r.Component__c;
                       objmod.Catalog_Module__c = lstmodule[0].Catalog_Module__c;
                       objmod.RecordTypeId = lstrecmod[0].id;
                       newlstmod.add(objmod);
                     }  
                     system.debug('@@@@'+newlstmod.size());
                     insert newlstmod;
          //lstmodip = [select id,name,Implementation_period__c from Module__c where id =: ];  
             for(Module__c objmod:newlstmod ){
                 Grant_Intervention__c objgin = new Grant_Intervention__c();
                    objgin.Name = selectedintrvntns[0].Name;
                    objgin.Implementation_Period__c=objmod.Implementation_period__c;
                    objgin.Module__c = objmod.id;
                    objgin.Catalog_Intervention__c = selectedintrvntns[0].id;
                    objgin.Recordtypeid=lstrec[0].id;
                    lstgin.add(objgin);
                }
                system.debug('@@@@'+lstgin.size());
                insert lstgin;
               
               
               for(Implementation_Period__c intr :selectedprs ){
                      Grant_Intervention__c objgin = new Grant_Intervention__c();
                           objgin.Name = selectedintrvntns[0].Name;
                           objgin.Concept_Note__c=lstmodule[0].Concept_Note__c;
                           objgin.Module__c = moduleid;
                          objgin.Catalog_Intervention__c = selectedintrvntns[0].id;
                          objgin.Recordtypeid=lstreccn[0].id;
                          objgin.Pr1__c=intr.Principal_Recipient__r.Name;
                           lstgincn .add(objgin);
                  }
                  system.debug('@@@@'+lstgincn .size());
                  insert lstgincn;
      
           /* if(lstmodule[0].Concept_Note__c != Null )  {
                  for(Catalog_Intervention__c cin :selectedintrvntns ){
                         Intervention__c p = new Intervention__c();
                           p.Name = cin.Name;
                           p.Description_of_Intervention__c = cin.Description__c;
                           p.French_Name__c = cin.French_Name__c;
                           p.Russian_Name__c  = cin.Russian_Name__c;
                           p.Spanish_Name__c = cin.Spanish_Name__c ;
                           p.Description_of_Intervention_Spanish__c = cin.Spanish_Description__c;
                           p.Description_of_Intervention_Russian__c = cin.Russian_Description__c;
                           p.Description_of_Intervention_French__c = cin.French_Description__c;  
                           p.Module_rel__c = Moduleid;
                           p.Concept_Note__c = lstmodule[0].Concept_Note__c ;
                           p.Intervention__c =cin.id;
                           lstcnintrvntn.add(p);
                   }
                   insert lstcnintrvntn;
            }
            else if(lstmodule[0].Concept_Note__c == Null){
                    for(Catalog_Intervention__c cin :selectedintrvntns ){
                         Grant_Intervention__c objgintr = new Grant_Intervention__c();
                         objgintr.Name = cin.Name;
                         objgintr.Implementation_Period__c = lstmodule[0].Implementation_Period__c ;
                         objgintr.Module__c = Moduleid;
                         objgintr.Catalog_Intervention__c = cin.id;
                         lstgintr.add(objgintr);
                   }
                   insert lstgintr;*/
           // }
             pageReference pr = new pageReference('/'+cnid);
             pr.setRedirect(true);
             return pr;
       
        }
        }
     
    }
    public void otherontervention(){
    	system.debug('enter otherintervention');
           for(wrapcatintervention objwintrvntn : wlstintrvntn){
           	system.debug('@@@intr'+objwintrvntn.selected);
           	system.debug('@@@intr'+objwintrvntn.catintrvntn.Is_Other_Intervrntion__c);
               if(objwintrvntn.selected == true && (objwintrvntn.catintrvntn.Is_Other_Intervrntion__c == true)){
                  system.debug('@@@@1'+objwintrvntn.catintrvntn.Name);
                  displayothername= true;
               }
               else if(objwintrvntn.selected == false && (objwintrvntn.catintrvntn.Is_Other_Intervrntion__c == true)){
                  displayothername= false;
               }
            } 
    }
   
 public Class wrapcatintervention{
        public Catalog_Intervention__c catintrvntn{get;set;}
         public boolean selected{get;set;}
         public wrapcatintervention(Catalog_Intervention__c r){
         catintrvntn = r;
         selected= false;
     }
      }
}