@istest
public class TestCNTargets {
    public static  testmethod void TestCNTargets(){
        Account objAcc = TestClassHelper.insertAccount();
    
        Grant__c objgrant = TestClassHelper.insertGrant(objAcc);
    
        Implementation_Period__c objip = TestClassHelper.createIPWithConceptNote(objGrant, objAcc);
        insert objip;
        
        Performance_Framework__c objpf = TestClassHelper.createPF(objIP);
        objpf.PF_Status__c = 'Not Yet Submitted';
        insert objpf;
        
        Module__c objmod = TestClassHelper.createModule();
        objmod.Performance_Framework__c = objpf.Id;
        insert objmod;
        
        Indicator__c objind = TestClassHelper.insertCatalogIndicator();
            
        Grant_Indicator__c objgiimp = TestClassHelper.createGrantIndicator();
        objgiimp.Indicator__c = objind.id;
        objgiimp.Indicator_Type__c = 'Impact';
        objgiimp.Performance_Framework__c = objpf.Id;
        objgiimp.Data_Type__c = 'Number';
        objgiimp.Decimal_Places__c = '2';
        insert objgiimp;
        
      /*  CNTargets objcnt1 = new CNTargets();
        objcnt1.strGID = '';
        objcnt1.strGID = objgiimp.Id;
        objcnt1.CNTarget = objgiimp;*/
        
       ApexPages.currentpage().getparameters().put('id',objgiimp.Id);
        
       ApexPages.StandardController sc = new ApexPages.StandardController(objgiimp);
        CNTargets objcnt = new CNTargets(sc);
        objcnt.strGID = objgiimp.Id;
        
        CNTargets objcnt1 = new CNTargets();
        objcnt1.strGID = objgiimp.Id;
        objcnt1.GI=objgiimp;
        objcnt1.CNID =objgiimp.Id;
    }
   
   
}