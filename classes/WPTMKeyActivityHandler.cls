// Purpose     :  This Class is handler for Trigger-KeyActivityTrigger.    
// Date        :  04-April-2015
// Created By  :  TCS 
// Author      :  Ayush Pradhan
// Description :  This class restricts the LFA Portal User with Read Only Access through Affiliation.

public with sharing class WPTMKeyActivityHandler{
            
    
    public static void restrictLFAUser(List<Key_Activity__c> keyActivityList){
        
        list<Implementation_Period__c> implList =  new list<Implementation_Period__c>();
        list<Id> acntList =  new list<Id>();
        Set<Id> setGrantId=  new Set<Id>();
        Set<Id> objLFAAccId =  new Set<Id>();
        Set<Id> objLFAParentId =  new Set<Id>();
        List<npe5__Affiliation__c> lstAffLFA = new List<npe5__Affiliation__c>();
        list<npe5__Affiliation__c> affiltionList = new list<npe5__Affiliation__c>();
        Boolean bCheck=false;
        
        Id profileId=userinfo.getProfileId();
        User usr = [Select id,ContactId, Profile.name from User where id=:UserInfo.getUserId()];
        String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
        
        Set<String> implSet= new Set<String>(); 
        for(Key_Activity__c kactObj : keyActivityList){
            implSet.add(kactObj.Grant_Implementation_Period__c );
        }
        
        ImplList = new list<Implementation_Period__c>([select Id, Principal_Recipient__c,Grant__c from Implementation_Period__c where id =:implSet]); 
        
         
        For(Implementation_Period__c impObj : ImplList){
            acntList.add(impObj.Principal_Recipient__c);
            setGrantId.add(impObj.Grant__c);
        }
        
        if(profileName.contains('LFA')){
             List<Grant__c> lstgrant= [Select id,Country__r.LFA__c, Country__r.LFA__r.ParentID from Grant__c where ID IN: setGrantId]; 
            
             for(Grant__c objGrant : lstgrant){
                objLFAAccId.add(objgrant.Country__r.LFA__c);
                objLFAParentId.add(objgrant.Country__r.LFA__r.ParentID);
            } 
            
            lstAffLFA = [Select Access_Level__c, id,npe5__Contact__c,npe5__Organization__c FROM npe5__Affiliation__c where npe5__Contact__c=: usr.ContactId and (npe5__Organization__c IN:objLFAAccId OR npe5__Organization__c IN:objLFAParentId) and npe5__Status__c='Current' order by LastModifiedDate desc ];                     
            
            For(npe5__Affiliation__c a :lstAffLFA){            
                if(a.Access_Level__c =='Read'){
                  // a.adderror('You are Not Authorised');
                     bCheck=true;
                 }           
            }
         }else{
             
            affiltionList = [select Id, npe5__Contact__c, Access_Level__c ,npe5__Organization__c from npe5__Affiliation__c where npe5__Organization__c=:acntList and npe5__Contact__c =:usr.ContactId];
            
            
            For(npe5__Affiliation__c a :affiltionList){            
                if(a.Access_Level__c =='Read'){
                  // a.adderror('You are Not Authorised');
                     bCheck=true;
                 }           
            }
        }
        For(Key_Activity__c kactObj : keyActivityList){
            if(bCheck){
               kactObj.adderror(' You are Not Authorised to insert/update this Key Activity');               
            }
        }       
    }           
}