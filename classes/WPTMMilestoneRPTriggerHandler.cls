//****************************************************************************************

// Purpose     :  This Class is used to delete records in Junction object between Milestone & Reporting Period
// Date        :  23-Mar-2015
// Created By  :  TCS 
// Author      :  Bhavya Mehta
// Description :  This Class is used to delete records in Junction object between Milestone & Reporting Period if correspoing Mileston record is also getting deleted with help of WPTM_MilestoneRPTrigger, trigger on Milestone object.

public with sharing class WPTMMilestoneRPTriggerHandler{     
    
    public static void deleteMileStoneRPRecords(Map<Id,Milestone_Target__c> mileStoneRecord){
       Set<Id> mileTarg = new Set<Id>();  
       mileTarg=mileStoneRecord.KeySet();
       List<Milestone_RP_Junction__c> deleteMileRelatedRecords = new List<Milestone_RP_Junction__c>();
       
       deleteMileRelatedRecords = [SELECT Id,Milestone_Target__c FROM Milestone_RP_Junction__c WHERE Milestone_Target__c IN :mileTarg];
       
       if(deleteMileRelatedRecords != null)        
           delete deleteMileRelatedRecords;
    }

}