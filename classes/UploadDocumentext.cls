public with sharing class UploadDocumentext {
    Public DocumentUpload_GM__c objDocumentUpload {get;set;}
    public integer linkedfilesize {get;set;}
    Public FeedItem objFeedItem {get;set;}
    public Id strImplementationPeriodId {get;set;}
    public String strWarning {get;set;}
    public boolean blnIsExtUser {get;set;}
    public boolean fileUploaded;
    public Profile profilename;   
    public List<selectoption> securityoptions{get;set;} 
    
    //public string  strsecurityoptn{get;set;}    
    Public boolean blnvisibleall{get;set;}  
    Public boolean blnvisiblelfa{get;set;}  
    Public boolean blnvisibleexntrnl{get;set;} 
    
    public UploadDocumentext (ApexPages.StandardController controller) {                
        strImplementationPeriodId = ApexPages.currentpage().getparameters().get('id');
        objDocumentUpload = new DocumentUpload_GM__c (Implementation_Period__c = strImplementationPeriodId,Process_Area__c = 'Grant-making');
        objFeedItem = new FeedItem();
        fileUploaded = false;
        id s = Userinfo.getUserId();
        User objUser = [Select ProfileId,Id,Name,ContactId From User where Id =:s];
        if(objUser.ContactId!=null){
            blnIsExtUser = true;
        }
        else{
            blnIsExtUser = false;
        } 
         profilename = [select Name from Profile where id =:objUser.ProfileId];
        if(objUser.ContactId!=null){
            blnIsExtUser = true;
            //externalRT = [SELECT Id from RecordType where name = 'Grant Making Public'  AND SobjectType ='DocumentUpload_GM__c ' limit 1];
        }
        else{
            blnIsExtUser = false;
            //internalRT = [SELECT Id from RecordType where name = 'Grant Making'  AND SobjectType ='DocumentUpload_GM__c ' limit 1];
        }
        checkprofile(); 
        fillsecurity();         
    }
    
    public void fillsecurity(){
        securityoptions = new List<Selectoption>(); 
        Schema.DescribeFieldResult fieldResult = DocumentUpload_GM__c.Security__c.getDescribe();
        system.debug('@@'+fieldResult);
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
       
        if(blnvisibleall){
           for(Schema.PicklistEntry f : ple){
               securityoptions.add(new SelectOption(f.getLabel(), f.getValue()));
           }
           objDocumentUpload.recordTypeId = Schema.SObjectType.DocumentUpload_GM__c.getRecordTypeInfosByName().get('Grant Making Public').getRecordTypeId();
        }
        
        if(blnvisiblelfa){
           securityoptions = new List<Selectoption>();
           integer i =0;
           
           for(Schema.PicklistEntry f : ple){
              if( f.getLabel().contains('Visible to all users') ){
                securityoptions.add(new SelectOption(f.getLabel(), f.getValue()));
              } 
              if( f.getLabel().contains('Visible to LFA and TGF internal only')){
                objDocumentUpload.security__c = f.getValue();
                securityoptions.add(new SelectOption(f.getLabel(), f.getValue()));
              } 
           }
           objDocumentUpload.recordTypeId = Schema.SObjectType.DocumentUpload_GM__c.getRecordTypeInfosByName().get('Grant Making').getRecordTypeId();
                
        }
        if(blnvisibleexntrnl){
           securityoptions = new List<Selectoption>();
           for(Schema.PicklistEntry f : ple){
              if( f.getLabel().contains('Visible to all users') ){
                securityoptions.add(new SelectOption(f.getLabel(), f.getValue()));
              } 
           }
           objDocumentUpload.recordTypeId = Schema.SObjectType.DocumentUpload_GM__c.getRecordTypeInfosByName().get('Grant Making').getRecordTypeId();     
        }
        
    }
    
    public void checkprofile(){
        blnvisibleall = false;
        blnvisiblelfa = false;
        blnvisibleexntrnl = false;
        
        List<Profile_Access_Setting__c> checkpage = [Select Salesforce_Item__c,Status__c from Profile_Access_Setting__c where Page_Name__c ='UploadDocument' and (Profile_Name__c =: profilename.Name)];
        System.debug('----checkPage---'+checkpage+'Profile Name=='+profilename.Name );
        for (Profile_Access_Setting__c check : checkpage){
             if(check.Salesforce_Item__c == 'Visibletoall')blnvisibleall = true;
             if(check.Salesforce_Item__c == 'Visibletolfa')blnvisiblelfa = true;
             if(check.Salesforce_Item__c == 'Visibletoexternal')blnvisibleexntrnl = true;
        }
    }
    
    public PageReference uploadFile () {
        if( objDocumentUpload.security__c.contains('Visible to all users') ){
            objDocumentUpload.recordTypeId = Schema.SObjectType.DocumentUpload_GM__c.getRecordTypeInfosByName().get('Grant Making Public').getRecordTypeId();   
        }else if(objDocumentUpload.security__c.contains('Visible to LFA and TGF internal only') ){
            objDocumentUpload.recordTypeId = Schema.SObjectType.DocumentUpload_GM__c.getRecordTypeInfosByName().get('Grant Making').getRecordTypeId();      
        }else{
            objDocumentUpload.recordTypeId = Schema.SObjectType.DocumentUpload_GM__c.getRecordTypeInfosByName().get('Grant Making').getRecordTypeId();  
        }
        System.debug('In Upload  objDocumentUpload ' +objDocumentUpload +'Feed = '+ objFeedItem  );
                if(objFeedItem != null)
                {          
                    if(linkedfilesize == Null || linkedfilesize > 10485760){                              
                        strWarning = 'This file exceeds the maximum size limit of 10MB.';
                        return null; //pgrefnew;  
                    }
                    if(linkedfilesize == 0)
                    {   
                        strWarning = 'File to be uploaded cannot be empty.';
                        return null;                 
                    }   
                    Savepoint sp = Database.setSavepoint();
                    try {
                            if(objDocumentUpload != null)
                            {
                                objDocumentUpload.File_Name__c = objFeedItem.ContentFileName;
                                objDocumentUpload.Description__c = objFeedItem.body;
                                insert objDocumentUpload;
                                objFeedItem.ParentId = objDocumentUpload.Id;
                            }                            
                            objFeedItem.Type = 'ContentPost';
                            objFeedItem.Visibility = 'AllUsers';
                            insert objFeedItem;
                            
                            FeedItem lstFeedItem = [Select RelatedRecordId from FeedItem where id=:objFeedItem.Id];                
                            system.debug(lstFeedItem);
                            objDocumentUpload.FeedItem_Id__c = objFeedItem.ID;
                            objDocumentUpload.DownloadId__c = lstFeedItem.RelatedRecordId;                                                                                 
                            system.debug(objDocumentUpload.DownloadId__c);  
                            update objDocumentUpload;
                            fileUploaded = true;
                        }                
                    
                        catch (Exception ex) {                        
                            fileUploaded = false;
                            Database.rollback(sp);
                            strWarning = 'Some error occured.';                
                            System.debug(ex); 
                        }                             
                }
                PageReference pgrefnew = new PageReference('/'+objDocumentUpload.Id);
                pgrefnew.setRedirect(true);
                return pgrefnew;
    }
    
    public PageReference returntoIP () {
        PageReference pgrefnew; 
        if(blnIsExtUser)
        {
            if( blnvisiblelfa ){
                pgrefnew = new PageReference('/LFA/'+strImplementationPeriodId);
            }else{
                pgrefnew = new PageReference('/GM/'+strImplementationPeriodId);
            }
        }
        else
        {
            pgrefnew = new PageReference('/'+strImplementationPeriodId);
        }
        pgrefnew.setRedirect(true);
        return pgrefnew;
    }
}