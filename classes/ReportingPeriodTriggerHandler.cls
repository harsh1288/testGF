public with sharing class ReportingPeriodTriggerHandler{
    
    //Delete related Milestone RP Junction record(s) when Milestone Target is inactive
    public static void UpdateMileStoneReportingPeriod(List<Period__c> lstPeriodAfterUpdate)
    {      
            List<Milestone_RP_Junction__c> mileRPjunclisttobeDeleted = new List<Milestone_RP_Junction__c>();
             //Set Objects           
            Set<string> setPerFramewrkIds=new Set<string>();
            Set<Id> setRptPeriodIdsToRemove=new Set<Id>();
            
            for(Period__c oPeriod:lstPeriodAfterUpdate)
            {
                if(oPeriod.Is_Active__c==false){                               
                    setRptPeriodIdsToRemove.add(oPeriod.Id);
                }
            }             
            mileRPjunclisttobeDeleted = [Select id,Reporting_Period__c from Milestone_RP_Junction__c where Reporting_Period__c in:setRptPeriodIdsToRemove];
            delete mileRPjunclisttobeDeleted;
       
    }
}