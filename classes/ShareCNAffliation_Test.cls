@isTest
(seeAllData = false)
public class ShareCNAffliation_Test{

public static testmethod void shareCNRecords(){

Profile objExtProfile = [select id from profile WHERE UserLicense.Name=:'Partner Community Login' Limit 1]; 

Test.startTest();
Account parentAccount = TestClassHelper.createAccount();
insert parentAccount;

    
    
Account objAccountCMAdmin = TestClassHelper.createAccount();
objAccountCMAdmin.Name = 'Test for ShareCNAffiliationCMAdmin';
insert objAccountCMAdmin;

Account objAccountCMRead = TestClassHelper.createAccount();
objAccountCMRead.Name = 'Test for ShareCNAffiliationCMRead';
insert objAccountCMRead;

Account objAccountPRAdmin = TestClassHelper.createAccount();
objAccountPRAdmin.Name = 'Test for ShareCNAffiliationPRRead';
insert objAccountPRAdmin;


Account objAccountPRRead = TestClassHelper.createAccount();
objAccountPRRead.Name = 'Test for ShareCNAffiliationPRAdmin';
insert objAccountPRRead;

Contact objContact = TestClassHelper.createContact();
objContact.FirstName = 'ShareCN';
objContact.LastName = 'Affiliation';
objContact.AccountId = parentAccount.Id;
objContact.External_User__c = true;
objContact.Email = 'ShareCNAffiliation@tgf.exp.com';
insert objContact;

npe5__Affiliation__c objAff = new npe5__Affiliation__c();
objAff.npe5__Organization__c = objAccountCMAdmin.Id;
objAff.npe5__Contact__c = objContact.Id;
objAff.npe5__Status__c = 'Current';
objAff.Access_Level__c = 'Admin';
objAff.RecordTypeId = label.CM_Affiliation_RT;
insert objAff;

npe5__Affiliation__c objAffCMRead = new npe5__Affiliation__c();
objAffCMRead.npe5__Organization__c = objAccountCMRead.Id;
objAffCMRead.npe5__Contact__c = objContact.Id;
objAffCMRead.npe5__Status__c = 'Current';
objAffCMRead.Access_Level__c = 'Read';
objAffCMRead.RecordTypeId = label.CM_Affiliation_RT;
insert objAffCMRead;

npe5__Affiliation__c objAffPRAdmin = new npe5__Affiliation__c();
objAffPRAdmin.npe5__Organization__c = objAccountPRAdmin.Id;
objAffPRAdmin.npe5__Contact__c = objContact.Id;
objAffPRAdmin.npe5__Status__c = 'Current';
objAffPRAdmin.Access_Level__c = 'Admin';
objAffPRAdmin.RecordTypeId = label.PR_Affiliation_RT;
insert objAffPRAdmin;

npe5__Affiliation__c objAffPRRead = new npe5__Affiliation__c();
objAffPRRead.npe5__Organization__c = objAccountPRRead.Id;
objAffPRRead.npe5__Contact__c = objContact.Id;
objAffPRRead.npe5__Status__c = 'Current';
objAffPRRead.Access_Level__c = 'Read';
objAffPRRead.RecordTypeId = label.PR_Affiliation_RT;
insert objAffPRRead;

User objExtAdminUser = new User(alias = 'SysAdmin', email='ExtTestUserfortesting@testorg.com',
            emailencodingkey='UTF-8', lastname='TestingAdmin', languagelocalekey='es',
            localesidkey='en_US', profileid = objExtProfile.Id, CommunityNickname = 'ExtTestUserfortesting', contactId = objContact.Id,
            timezonesidkey='America/Los_Angeles', username='ExtTestUserfortesting@testorg.com');
    //lstUsers.add(objSystemAdminUser);
    insert objExtAdminUser;

Test.stopTest();

Concept_Note__c objCNCMAdmin = TestClassHelper.createCN();
objCNCMAdmin.CCM_New__c = objAccountCMAdmin.Id;
insert objCNCMAdmin;

Concept_Note__c objCNCMRead = TestClassHelper.createCN();
objCNCMRead.CCM_New__c = objAccountCMRead.Id;
insert objCNCMRead;

Concept_Note__c objCNPRAdmin = TestClassHelper.createCN();
objCNPRAdmin.CCM_New__c = objAccountPRAdmin.Id;
insert objCNPRAdmin;

Concept_Note__c objCNPRRead = TestClassHelper.createCN();
objCNPRRead.CCM_New__c = objAccountPRRead.Id;
insert objCNPRRead;

/*User objExtUser = [Select id, contactid from User where contactid =: objContact.Id limit 1];
Concept_Note__Share objCNShare = [Select parentid, UserOrGroupId, AccessLevel from Concept_Note__Share where parentId =: objCN.Id AND UserOrGroupId =: objExtUser.Id limit 1];
System.assertEquals(objCNShare.UserOrGroupId,objExtUser.Id);*/

    }


}