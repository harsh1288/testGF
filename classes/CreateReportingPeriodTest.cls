@isTest(seealldata=false)
public class CreateReportingPeriodTest{

public static testMethod void myctrlReportingPeriodsTest1(){

        User objUser = [Select ID, country from User where id=:userinfo.getUserId()];     
        Account objAcc = TestClassHelper.insertAccount();
        
        Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
        insert objGrant;
       
        Implementation_Period__c objIP = TestClassHelper.createIP(objGrant,objAcc);
        objIP.Reporting__c = 'Quarterly';
        objIP.Status__c = 'Concept Note';
        //objIP.PR_Fiscal_Cycle__c = 'January-December';
        objIP.PR_Fiscal_Cycle__c = 'October-September';
        insert objIP;        
        
        /*objIP.PR_Fiscal_Cycle__c = 'January-December';
        objIP.GenerateReportingPeriod__c = true;
        objIP.Status__c = 'Grant-Making';
        objIP.Reporting__c = 'Yearly';
        objIP.start_Date__c = System.today().addDays(-20);
        objIP.end_Date__c = System.today().addDays(360);
        update objIP;*/
        
        Performance_Framework__c objPF = TestClassHelper.createPF(objIP);
        //objPF.PF_Status__c = 'Accepted';
        insert objPF; 
        system.debug('**objPF'+objPF); 
        
        Test.startTest();
        HPC_Framework__c  objHPC = TestClassHelper.createHPC();
        objHPC.Grant_Implementation_Period__c = objIP.Id;
        objHPC.HPC_Status__c = 'Accepted';
        Insert objHPC;
        system.debug('**objHPC'+objHPC); 
        IP_Detail_Information__c objDB = TestClassHelper.createIPDetail();
        objDB.Budget_Status__c = 'Accepted';
        objDB.Implementation_Period__c = objIP.Id;
        system.debug('**objDB'+objDB); 
        insert objDB;
        Test.StopTest();
        
        List<Implementation_Period__c>lstIP = new List<Implementation_Period__c>();
        lstIP.add(objIP);
        
        //Implementation_Period__c objIP2 = TestClassHelper.createIP(objGrant,objAcc);
        
        Reporting_Period_Detail__c detailObj = TestClassHelper.createReportingDetailPeriod(objip.id, true); 
        detailObj.PR_Reporting_Cycle__c = objIP.PR_Fiscal_Cycle__c;
        detailObj.Reporting_Frequency__c= objIP.Reporting__c;
        //detailObj.Is_Active__c = true;
        detailObj.Last_Active__c = True;
        detailObj.Implementation_Period_Start_Date__c = objIp.start_Date__c.addDays(3);
        detailObj.Implementation_Period_End_Date__c = objIp.End_Date__c;
        detailObj.Grant_Implementation_Period__c = objIp.Id;
        insert detailObj;
        
                    
        Period__c ObjRP = TestClassHelper.createPeriod();
        ObjRP.Implementation_Period__c = objIP.Id;
        ObjRP.Start_Date__c = Date.today();
        ObjRP.EFR__c=true;
        ObjRP.EFR_Due_Date__c = system.today();
        ObjRP.PU__c=true;
        ObjRP.PU_Due_Date__c =  system.today();
        ObjRP.Audit_Report__c=true;
        ObjRP.AR_Due_Date__c=system.today();
        ObjRP.DR__c=true;
        ObjRp.is_Active__c = true;
        ObjRP.Type__c = 'Reporting';
        ObjRp.End_Date__c = System.today().addDays(365);
        ObjRP.Due_Date__c=system.today();
        ObjRP.Performance_Framework__c = objPF.id;
        objRP.period_Number__c = 1;
        insert ObjRP;
        
        ApexPages.StandardController stdctrl = new ApexPages.StandardController(oBJrP);
        ApexPages.currentPage().getParameters().put('sid',objIP.id);
        changeReportingPeriodExt extObj = new changeReportingPeriodExt(stdctrl);
        extObj.lstPeriods.add(ObjRP);
        extObj.CheckTOCreateRPD();
        extObj.GeneratePeriod();
        extObj.CreateNewRP();
        
     }
     
  
     
   public static testMethod void myctrlReportingPeriodsTest2(){
         Test.startTest();   

        User objUser = [Select ID, country from User where id=:userinfo.getUserId()];
               
        Account objAcc = TestClassHelper.insertAccount();        
        Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
        insert objGrant;
                       
        Implementation_Period__c objIP = TestClassHelper.createIP(objGrant,objAcc);
        objIP.Reporting__c = 'Half-yearly';
        //objIP.PR_Fiscal_Cycle__c = 'January-December';
        objIP.PR_Fiscal_Cycle__c = 'July-June';
        objIP.Start_Date__c = System.today().addDays(-3);
        insert objIP;
        
        Performance_Framework__c objPF = TestClassHelper.createPF(objIP);
        insert objPF; 
        system.debug('**objPF'+objPF); 
        
        
        List<Implementation_Period__c>lstIP = new List<Implementation_Period__c>();
        lstIP.add(objIP);
     
        
        Reporting_Period_Detail__c detailObj = TestClassHelper.createReportingDetailPeriod(objip.id, true); 
        detailObj.PR_Reporting_Cycle__c = objIP.PR_Fiscal_Cycle__c;
        detailObj.Reporting_Frequency__c= 'Quaterly';//objIP.Reporting__c;
        detailObj.Is_Active__c = true;
        detailObj.Implementation_Period_Start_Date__c = objIp.start_Date__c;
        detailObj.Implementation_Period_End_Date__c = objIp.End_Date__c;
        detailObj.Grant_Implementation_Period__c = objIp.Id;
        insert detailObj;
        
                    
        Period__c ObjRP = TestClassHelper.createPeriod();
        ObjRP.Implementation_Period__c = objIP.Id;
        ObjRP.Start_Date__c = Date.today();
        ObjRP.EFR__c=true;
        ObjRP.EFR_Due_Date__c = system.today();
        system.debug('**ObjRP.EFR_Due_Date__c'+ObjRP.EFR_Due_Date__c);
        ObjRP.PU__c=true;
        ObjRP.PU_Due_Date__c =  system.today();
        ObjRP.Audit_Report__c=true;
        ObjRP.AR_Due_Date__c=system.today().addDays(10);
        ObjRP.DR__c=true;
        objRP.Is_Active__c = true;
        ObjRP.Type__c = 'Reporting';
        ObjRP.Due_Date__c=system.today().addDays(2);
        ObjRp.End_Date__c = System.today().addDays(365);
        ObjRP.Performance_Framework__c = objPF.id;
        objRP.period_Number__c = 1;
        insert ObjRP;
        
        ApexPages.StandardController stdctrl = new ApexPages.StandardController(objIP);
        ApexPages.currentPage().getParameters().put('sid',objIP.id);
        changeReportingPeriodExt extObj = new changeReportingPeriodExt(stdctrl);
        extObj.GeneratePeriod();
        extObj.CreateNewRP();
        Test.stopTest();
     }
     
     public static testMethod void myctrlReportingPeriodsTest3(){


        User objUser = [Select ID, country from User where id=:userinfo.getUserId()];
     
          
        Account objAcc = TestClassHelper.insertAccount();
        
        Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
        insert objGrant;
       
        Implementation_Period__c objIP = TestClassHelper.createIP(objGrant,objAcc);
        objIP.Reporting__c = 'Half-yearly';
        objIP.Status__c = 'Concept Note';
        objIP.PR_Fiscal_Cycle__c = 'October-September';
        objIP.Start_Date__c = System.today().addDays(-3);
        objIP.End_Date__c = System.today().addDays(3);
        objIP.Reporting__c = 'Quarterly';
        insert objIP;
        
       /* objIP.GenerateReportingPeriod__c = true;
        objIP.Reporting__c = 'Yearly';
        objIP.PR_Fiscal_Cycle__c = 'January-December';        
        objIP.Status__c = 'Grant-Making';
        update objIP;*/
        
        Performance_Framework__c objPF = TestClassHelper.createPF(objIP);
        insert objPF; 
        system.debug('**objPF'+objPF); 
        
        List<Implementation_Period__c>lstIP = new List<Implementation_Period__c>();
        lstIP.add(objIP);
        

        Grant_Indicator__c indicator = TestclassHelper.createGrantIndicator();
        insert indicator;
        List<Period__c> periodList = new List<Period__c>();
        Period__c ObjRP1 = TestClassHelper.createPeriod();
        ObjRP1.Implementation_Period__c = objIP.Id;
        ObjRP1.Start_Date__c = Date.today();
        ObjRP1.EFR__c=true;
        ObjRP1.EFR_Due_Date__c = system.today();
        ObjRP1.PU__c=true;
        ObjRP1.PU_Due_Date__c =  system.today();
        ObjRP1.Audit_Report__c=true;
        ObjRP1.AR_Due_Date__c=system.today();
        ObjRP1.DR__c=true;
        ObjRP1.Type__c = 'Reporting';
        ObjRP1.Due_Date__c=system.today();
        ObjRp1.End_Date__c = System.today().addDays(185);
        ObjRP1.Performance_Framework__c = objPF.id;
        objRP1.Period_Number__c = 2;
        periodList.add(ObjRP1);
                         
        Period__c ObjRP = TestClassHelper.createPeriod();
        ObjRP.Implementation_Period__c = objIP.Id;     
        ObjRP.Start_Date__c = Date.today();
        ObjRP.EFR__c=true;
        ObjRP.EFR_Due_Date__c = system.today();
        ObjRP.PU__c=true;
        ObjRP.PU_Due_Date__c =  system.today();
        ObjRP.Audit_Report__c=true;
        ObjRP.AR_Due_Date__c=system.today();
        ObjRP.DR__c=true;
        ObjRP.Type__c = 'Reporting';
        ObjRP.Due_Date__c=system.today();
        ObjRp.End_Date__c = System.today().addDays(365);
        ObjRP.Performance_Framework__c = objPF.id;
        ObjRP.is_Active__c = true;
        objRP.Period_Number__c = 1;
        periodList.add(ObjRP);
        insert periodList;
        
        
        
        Result__c resultObj = TestClassHelper.createResult(ObjRP, indicator.id);
        insert resultObj;
              
         Reporting_Period_Detail__c detailObj = TestClassHelper.createReportingDetailPeriod(objip.id, true); 
        detailObj.PR_Reporting_Cycle__c = 'April-March';//objIP.PR_Fiscal_Cycle__c;
        detailObj.Reporting_Frequency__c= objIP.Reporting__c;
        detailObj.Last_Active__c = True;
        detailObj.Implementation_Period_Start_Date__c = objIp.start_Date__c;
        detailObj.Implementation_Period_End_Date__c = objIp.End_Date__c;
        detailObj.Grant_Implementation_Period__c = objIp.Id;
        insert detailObj;
        
        ApexPages.StandardController stdctrl = new ApexPages.StandardController(objIP);
        ApexPages.currentPage().getParameters().put('sid',objIP.id);
        changeReportingPeriodExt extObj = new changeReportingPeriodExt(stdctrl);
        extobj.showMergeList();
        extobj.showUnMergeList();        
     }
     
     public static testMethod void myctrlReportingPeriodsTest4(){


        User objUser = [Select ID, country from User where id=:userinfo.getUserId()];
     
          
        Account objAcc = TestClassHelper.insertAccount();
        
        Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
        insert objGrant;
       
        Implementation_Period__c objIP = TestClassHelper.createIP(objGrant,objAcc);
        objIP.Reporting__c = 'Quarterly';
        objIP.PR_Fiscal_Cycle__c = 'July-June';
        objIP.Start_Date__c = System.today().addDays(3);
        objIP.End_Date__c = System.today().addYears(3);
        
        objIP.Reporting__c = 'Quarterly';
        insert objIP;
        
       /* objIP.GenerateReportingPeriod__c = true;
        objIP.Reporting__c = 'Yearly';
        objIP.PR_Fiscal_Cycle__c = 'January-December';        
        objIP.Status__c = 'Grant-Making';
        update objIP;*/
        
        Performance_Framework__c objPF = TestClassHelper.createPF(objIP);
        insert objPF; 
        system.debug('**objPF'+objPF); 
        
        List<Implementation_Period__c>lstIP = new List<Implementation_Period__c>();
        lstIP.add(objIP);
        
        Reporting_Period_Detail__c detailObj = TestClassHelper.createReportingDetailPeriod(objip.id, true); 
        detailObj.PR_Reporting_Cycle__c = objIP.PR_Fiscal_Cycle__c;
        detailObj.Reporting_Frequency__c= objIP.Reporting__c;
        detailObj.Is_Active__c = true;
        detailObj.Implementation_Period_Start_Date__c = objIp.start_Date__c;
        detailObj.Implementation_Period_End_Date__c = objIp.End_Date__c;
        detailObj.Grant_Implementation_Period__c = objIp.Id;
        insert detailObj;
        
        Grant_Indicator__c indicator = TestclassHelper.createGrantIndicator();
        insert indicator;
        List<Period__c> periodList = new List<Period__c>();
        Period__c ObjRP1 = TestClassHelper.createPeriod();
        ObjRP1.Implementation_Period__c = objIP.Id;
        ObjRP1.Start_Date__c = Date.today();
        ObjRP1.EFR__c=true;
        ObjRP1.EFR_Due_Date__c = system.today();
        ObjRP1.PU__c=true;
        ObjRP1.PU_Due_Date__c =  system.today();
        ObjRP1.Audit_Report__c=true;
        ObjRP1.AR_Due_Date__c=system.today();
        ObjRP1.DR__c=true;
        ObjRP1.Type__c = 'Reporting';
        ObjRP1.Due_Date__c=system.today();
        ObjRp1.End_Date__c = System.today().addDays(185);
        ObjRP1.Performance_Framework__c = objPF.id;
        objRP1.Period_Number__c = 2;
        objRP1.Reporting_Period_Detail__c = detailObj.Id;
        periodList.add(ObjRP1);
                         
        Period__c ObjRP = TestClassHelper.createPeriod();
        ObjRP.Implementation_Period__c = objIP.Id;     
        ObjRP.Start_Date__c = Date.today();
        ObjRP.EFR__c=true;
        ObjRP.EFR_Due_Date__c = system.today();
        ObjRP.PU__c=true;
        ObjRP.PU_Due_Date__c =  system.today();
        ObjRP.Audit_Report__c=true;
        ObjRP.AR_Due_Date__c=system.today();
        ObjRP.DR__c=true;
        ObjRP.Type__c = 'Reporting';
        ObjRP.Due_Date__c=system.today();
        ObjRp.End_Date__c = System.today().addDays(365);
        ObjRP.Performance_Framework__c = objPF.id;
        ObjRP.is_Active__c = true;
        objRP.Period_Number__c = 1;
        objRP.Base_Frequency__c = objIP.Reporting__c;
        objRP.Reporting_Period_Detail__c = detailObj.Id;
        periodList.add(ObjRP);
        insert periodList;
        
        
        
        Result__c resultObj = TestClassHelper.createResult(ObjRP, indicator.id);
        insert resultObj;
              
        
        
        ApexPages.StandardController stdctrl = new ApexPages.StandardController(objIP);
        ApexPages.currentPage().getParameters().put('sid',objIP.id);
        changeReportingPeriodExt extObj = new changeReportingPeriodExt(stdctrl);
        extobj.showMergeList();
        extobj.showUnMergeList();
                
     } 
     public static testMethod void myctrlReportingPeriodsTest5(){


        User objUser = [Select ID, country from User where id=:userinfo.getUserId()];
     
          
        Account objAcc = TestClassHelper.insertAccount();
        
        Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
        insert objGrant;
        
        String str = '2016-03-25';
        Date dateip = Date.valueof(str);
        
        String strend = '2024-02-01';
        Date dateipend = Date.valueof(strend);
        
        Implementation_Period__c objIP = TestClassHelper.createIP(objGrant,objAcc);
        objIP.Reporting__c = 'Quarterly';
        objIP.PR_Fiscal_Cycle__c = 'January-December';
        objIP.Status__c = 'Concept Note';
        objIP.Start_Date__c = dateip; //System.today().addDays(3);
        objIP.End_Date__c = dateipend ; //System.today().addYears(3);
        insert objIP;
        
       /* objIP.GenerateReportingPeriod__c = true;
        objIP.Reporting__c = 'Yearly';
        objIP.PR_Fiscal_Cycle__c = 'January-December';        
        objIP.Status__c = 'Grant-Making';
        update objIP;*/
        
        Performance_Framework__c objPF = TestClassHelper.createPF(objIP);
        insert objPF; 
        system.debug('**objPF'+objPF); 
        
        Test.startTest();
        HPC_Framework__c  objHPC = TestClassHelper.createHPC();
        objHPC.Grant_Implementation_Period__c = objIP.Id;
        objHPC.HPC_Status__c = 'Accepted';
        Insert objHPC;
        system.debug('**objHPC'+objHPC); 
        IP_Detail_Information__c objDB = TestClassHelper.createIPDetail();
        objDB.Budget_Status__c = 'Accepted';
        objDB.Implementation_Period__c = objIP.Id;
        system.debug('**objDB'+objDB); 
        insert objDB;
        Test.StopTest();
        
        List<Implementation_Period__c>lstIP = new List<Implementation_Period__c>();
        lstIP.add(objIP);
        
        Reporting_Period_Detail__c detailObj = TestClassHelper.createReportingDetailPeriod(objip.id, true); 
        detailObj.PR_Reporting_Cycle__c = objIP.PR_Fiscal_Cycle__c;
        detailObj.Reporting_Frequency__c= objIP.Reporting__c;
        detailObj.Is_Active__c = true;
        detailObj.Implementation_Period_Start_Date__c = objIp.start_Date__c;
        detailObj.Implementation_Period_End_Date__c = objIp.End_Date__c;
        detailObj.Grant_Implementation_Period__c = objIp.Id;
        insert detailObj;
        
        Grant_Indicator__c indicator = TestclassHelper.createGrantIndicator();
        insert indicator;
        List<Period__c> periodList = new List<Period__c>();
        Period__c ObjRP1 = TestClassHelper.createPeriod();
        ObjRP1.Implementation_Period__c = objIP.Id;
        ObjRP1.Start_Date__c = Date.today();
        ObjRP1.EFR__c=true;
        ObjRP1.EFR_Due_Date__c = system.today();
        ObjRP1.PU__c=true;
        ObjRP1.PU_Due_Date__c =  system.today();
        ObjRP1.Audit_Report__c=true;
        ObjRP1.AR_Due_Date__c=system.today();
        ObjRP1.DR__c=true;
        ObjRP1.Type__c = 'Reporting';
        ObjRP1.Due_Date__c=system.today();
        ObjRp1.End_Date__c = System.today().addDays(185);
        ObjRP1.Performance_Framework__c = objPF.id;
        objRP1.Period_Number__c = 2;
        objRP1.Reporting_Period_Detail__c = detailObj.Id;
        periodList.add(ObjRP1);
                         
        Period__c ObjRP = TestClassHelper.createPeriod();
        ObjRP.Implementation_Period__c = objIP.Id;     
        ObjRP.Start_Date__c = Date.today();
        ObjRP.EFR__c=true;
        ObjRP.EFR_Due_Date__c = system.today();
        ObjRP.PU__c=true;
        ObjRP.PU_Due_Date__c =  system.today();
        ObjRP.Audit_Report__c=true;
        ObjRP.AR_Due_Date__c=system.today();
        ObjRP.DR__c=true;
        ObjRP.Type__c = 'Reporting';
        ObjRP.Due_Date__c=system.today();
        ObjRp.End_Date__c = System.today().addDays(365);
        ObjRP.Performance_Framework__c = objPF.id;
        ObjRP.is_Active__c = true;
        objRP.Period_Number__c = 1;
        objRP.Base_Frequency__c = objIP.Reporting__c;
        objRP.Reporting_Period_Detail__c = detailObj.Id;
        periodList.add(ObjRP);
        insert periodList;
        
        
        
        Result__c resultObj = TestClassHelper.createResult(ObjRP, indicator.id);
        insert resultObj;
              
        
        
        ApexPages.StandardController stdctrl = new ApexPages.StandardController(objIP);
        ApexPages.currentPage().getParameters().put('sid',objIP.id);
        changeReportingPeriodExt extObj = new changeReportingPeriodExt(stdctrl);
        extobj.GeneratePeriod();  
        extobj.CheckTOCreateRPD();
        extobj.CreateRPDetail();
        extobj.NewReportingPeriod();
        extobj.CreateNewRP();
        extobj.showMergeList();
        extobj.showUnMergeList();
        extobj.isExecuted = true;
       
     } 
     
     public static testMethod void myctrlReportingPeriodsTest6(){

        User objUser = [Select ID, country from User where id=:userinfo.getUserId()];     
        Account objAcc = TestClassHelper.insertAccount();
        
        Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
        insert objGrant;
       
        Implementation_Period__c objIP = TestClassHelper.createIP(objGrant,objAcc);
        objIP.Reporting__c = 'Quarterly';
        objIP.Status__c = 'Concept Note';
        //objIP.PR_Fiscal_Cycle__c = 'January-December';
        objIP.PR_Fiscal_Cycle__c = 'October-September';
        insert objIP;        
        
        /*objIP.PR_Fiscal_Cycle__c = 'January-December';
        objIP.GenerateReportingPeriod__c = true;
        objIP.Status__c = 'Grant-Making';
        objIP.Reporting__c = 'Yearly';
        objIP.start_Date__c = System.today().addDays(-20);
        objIP.end_Date__c = System.today().addDays(360);
        update objIP;*/
        
        Performance_Framework__c objPF = TestClassHelper.createPF(objIP);
        //objPF.PF_Status__c = 'Accepted';
        insert objPF; 
        system.debug('**objPF'+objPF); 
        
        Test.startTest();
        HPC_Framework__c  objHPC = TestClassHelper.createHPC();
        objHPC.Grant_Implementation_Period__c = objIP.Id;
        objHPC.HPC_Status__c = 'Accepted';
        Insert objHPC;
        system.debug('**objHPC'+objHPC); 
        IP_Detail_Information__c objDB = TestClassHelper.createIPDetail();
        objDB.Budget_Status__c = 'Accepted';
        objDB.Implementation_Period__c = objIP.Id;
        system.debug('**objDB'+objDB); 
        insert objDB;
        Test.StopTest();
        
        List<Implementation_Period__c>lstIP = new List<Implementation_Period__c>();
        lstIP.add(objIP);
        
        //Implementation_Period__c objIP2 = TestClassHelper.createIP(objGrant,objAcc);
        
        Reporting_Period_Detail__c detailObj = TestClassHelper.createReportingDetailPeriod(objip.id, true); 
        detailObj.PR_Reporting_Cycle__c = objIP.Reporting__c;
        detailObj.Reporting_Frequency__c= objIP.PR_Fiscal_Cycle__c;
        //detailObj.Is_Active__c = true;
        detailObj.Last_Active__c = True;
        detailObj.Implementation_Period_Start_Date__c = objIp.start_Date__c.addDays(3);
        detailObj.Implementation_Period_End_Date__c = objIp.End_Date__c;
        detailObj.Grant_Implementation_Period__c = objIp.Id;
        insert detailObj;
        
                    
        Period__c ObjRP = TestClassHelper.createPeriod();
        ObjRP.Implementation_Period__c = objIP.Id;
        ObjRP.Start_Date__c = Date.today();
        ObjRP.EFR__c=true;
        ObjRP.EFR_Due_Date__c = system.today();
        ObjRP.PU__c=true;
        ObjRP.PU_Due_Date__c =  system.today();
        ObjRP.Audit_Report__c=true;
        ObjRP.AR_Due_Date__c=system.today();
        ObjRP.DR__c=true;
        ObjRp.is_Active__c = true;
        ObjRP.Type__c = 'Reporting';
        ObjRp.End_Date__c = System.today().addDays(365);
        ObjRP.Due_Date__c=system.today();
        ObjRP.Performance_Framework__c = objPF.id;
        objRP.period_Number__c = 1;
        insert ObjRP;
        
        ApexPages.StandardController stdctrl = new ApexPages.StandardController(oBJrP);
        ApexPages.currentPage().getParameters().put('sid',objIP.id);
        changeReportingPeriodExt extObj = new changeReportingPeriodExt(stdctrl);
        extObj.lstPeriods.add(ObjRP);
        extObj.CheckTOCreateRPD();
        extObj.GeneratePeriod();
        extObj.CreateNewRP();
        
     }
      
 }