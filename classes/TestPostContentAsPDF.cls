@isTest (seeAllData = true)
Public Class TestPostContentAsPDF{
    Public static testMethod Void TestPostContentAsPDFMethod(){
        Account objAcc = new Account();
        objAcc.Name = 'Test Acc';
        objAcc.Short_Name__c= 'Test Acc';
        insert objAcc;
        
        Country__c objCountry = new Country__c();
        objCountry.Name = 'Test Country';
        objCountry.Region_Name__c = 'Test Region';
        objCountry.Used_for_LFA_Services__c = TRUE;
        insert objCountry;
        
        LFA_Work_Plan__c objWp = new LFA_Work_Plan__c();
        objWp.Name = 'Test Wp';
        objWp.LFA__c = objAcc.Id;
        objWp.Country__c = objCountry.id;
        objWP.Include_In_Library__c = TRUE;
        insert objWp;
        
        LFA_Service__c objService = new LFA_Service__c();
        objService.Name = 'PUDR';
        objService.LFA_Work_Plan__c = objWp.id;
        objService.Service_Type__c = 'Key Services';
        objService.Service_Sub_Type__c = 'PUDR';
        objService.Exclude_from_Library__c=FALSE;        
        insert objService;
        
        LFA_Resource__c objResource = new LFA_Resource__c();
        //objResource.Name = 'Test Resource';
        objResource.LFA_Service__c = objService.id;
        objResource.Rate__c = 1;
        //objResource.LFA_Role__c = objRole1.Id;
        objResource.LOE__c = 1;
        objResource.CT_Planned_Cost__c = 1;
        objResource.CT_Planned_LOE__c = 1;
        objResource.TGF_Proposed_Cost__c = 1;
        objResource.TGF_Proposed_LOE__c = 1;
        insert objResource;
        
        LFA_Service__c objService1 = new LFA_Service__c();
        objService1.Name = 'PUDR';
        objService1.LFA_Work_Plan__c = objWp.id;
        objService1.Service_Type__c = 'Key Services';
        objService1.Service_Sub_Type__c = 'PUDR';
        insert objService1;
        
        LFA_Resource__c objResource1 = new LFA_Resource__c();
        //objResource1.Name = 'Test Resource1';
        objResource1.LFA_Service__c = objService1.id;
        objResource1.Rate__c = 2;
        objResource1.LOE__c = 2;
        objResource1.CT_Planned_Cost__c = 2;
        objResource1.CT_Planned_LOE__c = 2;
        objResource1.TGF_Proposed_Cost__c = 2;
        objResource1.TGF_Proposed_LOE__c = 2;
        insert objResource1;
        
        LFA_Service__c objService2 = new LFA_Service__c();
        objService2.Name = 'PUDR';
        objService2.LFA_Work_Plan__c = objWp.id;
        objService2.Service_Type__c = 'Other LFA Services';
        objService2.Service_Sub_Type__c = 'PUDR';
        insert objService2;
        
        LFA_Role__c objRole = new LFA_Role__c();
        objRole.Name = 'Test Role';
        insert objRole;
        
        LFA_Resource__c objResource2 = new LFA_Resource__c();
        //objResource2.Name = 'Test Resource1';
        objResource2.LFA_Service__c = objService2.id;
        objResource2.LFA_Role__c = objRole.id;
        objResource2.Rate__c = 2;
        objResource2.LOE__c = 2;
        objResource2.CT_Planned_Cost__c = 2;
        objResource2.CT_Planned_LOE__c = 2;
        objResource2.TGF_Proposed_Cost__c = 2;
        objResource2.TGF_Proposed_LOE__c = 2;
        insert objResource2;
        
        Contact objCon = new Contact();
        objCon.LastName = 'Test LName';
        objCon.AccountId = objAcc.Id;
        insert objCon;
        
        Rate__c objRate = new Rate__c();
        objRate.LFA_Role__c = objRole.Id;
        objRate.Contact__c = objCon.Id;
        objRate.Country__c = objCountry.Id;
        objRate.Rate__c = 1;
        objRate.Active__c = true;
        insert objRate;
        
        Grant__c objGrant = new Grant__c();
        objGrant.Name = 'Test Grant';
        objGrant.Country__c = objCountry.id;
        objGrant.Principal_Recipient__c = objAcc.id;
        objGrant.Financial_and_Fiduciary_Risks__c = 'Low';
        objGrant.Health_Services_and_Products_Risks__c = 'High';
        objGrant.Programmatic_and_Performance_Risks__c = 'Medium';
        insert objGrant;
        
        LFA_Service__c objService3 = new LFA_Service__c();
        objService3.Name = 'PUDR';
        objService3.LFA_Work_Plan__c = objWp.id;
        objService3.Service_Type__c = 'Key Services';
        objService3.Service_Sub_Type__c = 'PUDR';
        insert objService3;
        
        LFA_Role__c objRole1 = new LFA_Role__c();
        objRole1.Name = 'Test Role1';
        insert objRole1;
        
        LFA_Resource__c objResource3 = new LFA_Resource__c();
        //objResource.Name = 'Test Resource';
        objResource3.LFA_Service__c = objService3.id;
        objResource3.Rate__c = 1;
        objResource3.LFA_Role__c = objRole1.Id;
        objResource3.LOE__c = 1;
        objResource3.CT_Planned_Cost__c = 1;
        objResource3.CT_Planned_LOE__c = 1;
        objResource3.TGF_Proposed_Cost__c = 1;
        objResource3.TGF_Proposed_LOE__c = 1;
        insert objResource3;
        
        Rate__c objRate1 = new Rate__c();
        objRate1.LFA_Role__c = objRole1.Id;
        objRate1.Contact__c = objCon.Id;
        objRate1.Country__c = objCountry.Id;
        objRate1.Rate__c = 1;
        objRate1.Active__c = true;
        insert objRate1;
       
       
        string strresult;
        strresult = PostContentAsPDF.postPDFContentToChatter(String.valueOf(objWp.id),objWp.name,String.valueOf(User.FirstName) + String.valueOf(User.LastName));
     //  !$User.FirstName} {!$User.LastName}
        strresult = PostContentAsPDF.postPDFContentToChatterSubmitProposal(String.valueOf(objWp.id),objWp.name,String.valueOf(User.FirstName),String.valueOf(User.LastName));
        strresult = PostContentAsPDF.UpdateResourceToCreatingPurchaseOrder(String.valueOf(objWp.id));
        strresult = PostContentAsPDF.UpdateResourceStatusBack(String.valueOf(objWp.id));
        strresult = PostContentAsPDF.UpdateResourceStatusGMSupport(String.valueOf(objWp.id));
        strresult = PostContentAsPDF.UpdateResourceGeneratePdf(String.valueOf(objWp.id),objWp.name,String.valueOf(User.FirstName) + String.valueOf(User.LastName));
        strresult = PostContentAsPDF.UpdateResourceBackGeneratePdf(String.valueOf(objWp.id),objWp.name,String.valueOf(User.FirstName) + String.valueOf(User.LastName));
        
        strresult = PostContentAsPDF.GenerateAgreedXLS(String.valueOf(objWp.id),objWp.name,String.valueOf(User.FirstName),String.valueOf(User.LastName));
        
        strresult = PostContentAsPDF.FollowServices(String.valueOf(objWp.id));
        
        
        
        
     
    }
}