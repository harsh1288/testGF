public with sharing class Help_Grant_Disaggregated{
    public String strLanguage {get; set;}
    public Guidance__c guidance {get; set;}
    
    public Help_Grant_Disaggregated(){
        strLanguage = 'ENGLISH';
        if(System.UserInfo.getLanguage() == 'fr'){
            strLanguage = 'FRENCH'; 
        }
        if(System.UserInfo.getLanguage() == 'ru'){
            strLanguage = 'RUSSIAN'; 
        }
        if(System.UserInfo.getLanguage() == 'es'){
            strLanguage = 'SPANISH'; 
        }
        Grant_Disaggregated();
    }
    
    public void Grant_Disaggregated(){
        guidance = [SELECT Name, English_Guidance__c, French_Guidance__c,GM_Guidance_Name__c, Russian_Guidance__c, Spanish_Guidance__c, 
                    English_Guidance_Overflow__c, French_Guidance_Overflow__c, Russian_Guidance_Overflow__c, Spanish_Guidance_Overflow__c
                    FROM Guidance__c WHERE Name = 'Grant Disaggregation Guidance'];
        
    }

}