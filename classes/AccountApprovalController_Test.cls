@isTest
public with sharing class AccountApprovalController_Test {

public Static testMethod void AccountApprovalController1(){
        
        Profile profileId=[Select Id,Name from Profile where Name = 'Country Team'];
        
        User user1 = new User(Alias = 'standt', Email='standarduserDC1@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='fr', 
        LocaleSidKey='en_US', ProfileId = profileId.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='standarduserDC1@testorg.com');
        insert user1;
        
        Country__c objCountry = TestClasshelper.insertCountry();
        
        Vendor__c objVendor = new Vendor__c();
        objVendor.Country__c = objCountry.Id;
        insert objVendor;
        
        
        Account objAcc = TestClassHelper.createAccount();
        objAcc.Approval_Status__c = 'Finance Officer verification';
        objAcc.CT_Finance_Officer__c = user1.id;
        objAcc.Vendor_ID__c = objVendor.Id;
        //objAcc.CT_ID__c = CTPublicGrup.Id;
        insert objAcc;
              
        Account_Approval__c objAccountApproval = new Account_Approval__c ();
        objAccountApproval.Account__c = objAcc.Id;
        objAccountApproval.Status__c = 'Pending';
        insert objAccountApproval;
        
        Contact con = new Contact(FirstName = 'Test', LastName = 'PR Admin', AccountId =objAcc.Id, Primary_Contact__c= true );
        insert con;
        
        Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
        objGrant.Country__c = objCountry.Id;
        objGrant.Grant_Status__c = 'Accepted';
        insert objGrant;
     
        Implementation_Period__c ObjImplementationPeriod = TestClassHelper.createIP(objGrant,objAcc);
        ObjImplementationPeriod.Currency_of_Grant_Agreement__c = 'USD';
        ObjImplementationPeriod.Local_Currency__c = 'COP';
        ObjImplementationPeriod.Other_Currency__c = 'EUR';
        ObjImplementationPeriod.Exchange_Rate__c = 50;
        ObjImplementationPeriod.Budget_Status__c = 'Draft';
        ObjImplementationPeriod.Auth_Sig_for_Grant_Agreement__c = con.Id;
        ObjImplementationPeriod.Auth_Sig_for_Disbursement_Request_1__c = con.id;
        ObjImplementationPeriod.Auth_Sig_for_Disbursement_Request_2__c = con.id;
        ObjImplementationPeriod.CCM_Chair__c = con.id;
        ObjImplementationPeriod.CCM_Civil_Society_Representative__c =con.id;
        insert ObjImplementationPeriod;
     
        ApexPages.currentPage().getParameters().put('accId',objAcc.id);
       // ApexPages.StandardController sc = new ApexPages.StandardController(objAcc);
        AccountApprovalController acctApprovalController = new AccountApprovalController();
        acctApprovalController.approve();
        //acctApprovalController.reject();
        acctApprovalController.getGIP(ObjImplementationPeriod.id);
}
public Static testMethod void AccountApprovalController2(){
        
        Profile profileId=[Select Id,Name from Profile where Name = 'Country Team'];
        
        User user1 = new User(Alias = 'standt', Email='standarduserDC1@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='fr', 
        LocaleSidKey='en_US', ProfileId = profileId.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='standarduserDC1@testorg.com');
        insert user1;
        
        Country__c objCountry = TestClasshelper.insertCountry();
        
        Vendor__c objVendor = new Vendor__c();
        objVendor.Country__c = objCountry.Id;
        insert objVendor;
        
        
        Account objAcc = TestClassHelper.createAccount();
        objAcc.Approval_Status__c = 'Finance Officer verification';
        objAcc.CT_Finance_Officer__c = user1.id;
        objAcc.Vendor_ID__c = objVendor.Id;
        //objAcc.CT_ID__c = CTPublicGrup.Id;
        insert objAcc;
              
        Account_Approval__c objAccountApproval = new Account_Approval__c ();
        objAccountApproval.Account__c = objAcc.Id;
        objAccountApproval.Status__c = 'Pending';
        insert objAccountApproval;
        
        Contact con = new Contact(FirstName = 'Test', LastName = 'PR Admin', AccountId =objAcc.Id, Primary_Contact__c= true );
        insert con;
        
        Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
        objGrant.Country__c = objCountry.Id;
        objGrant.Grant_Status__c = 'Accepted';
        insert objGrant;
     
        Implementation_Period__c ObjImplementationPeriod = TestClassHelper.createIP(objGrant,objAcc);
        ObjImplementationPeriod.Currency_of_Grant_Agreement__c = 'USD';
        ObjImplementationPeriod.Local_Currency__c = 'COP';
        ObjImplementationPeriod.Other_Currency__c = 'EUR';
        ObjImplementationPeriod.Exchange_Rate__c = 50;
        ObjImplementationPeriod.Budget_Status__c = 'Draft';
        ObjImplementationPeriod.Auth_Sig_for_Grant_Agreement__c = con.Id;
        ObjImplementationPeriod.Auth_Sig_for_Disbursement_Request_1__c = con.id;
        ObjImplementationPeriod.Auth_Sig_for_Disbursement_Request_2__c = con.id;
        ObjImplementationPeriod.CCM_Chair__c = con.id;
        ObjImplementationPeriod.CCM_Civil_Society_Representative__c =con.id;
        insert ObjImplementationPeriod;
     
        ApexPages.currentPage().getParameters().put('accId',objAcc.id);
       // ApexPages.StandardController sc = new ApexPages.StandardController(objAcc);
        AccountApprovalController acctApprovalController = new AccountApprovalController();
        acctApprovalController.reject();
        
}
}