/*
* $Author:      $ TCS - Rahul Kumar / Debarghya Sen
* $Description: $ handler for ImplementerTrigger /inserts sub recipent and assigns it to implementer record when it's blank
* $Date:        $ 07-Nov-2014
* $Revision:    $ 
*/

public class ImplementerTriggerHandler
{
    public ImplementerTriggerHandler()
    {
        //Constructor
    }
    public static void insertSubRecipient(List<Implementer__c> impList)
    {
      /*  Id pRId = [SELECT Id FROM RecordType WHERE Name = 'PR' AND SobjectType = 'Account' limit 1].Id;
        List<Account> newSRList = new List<Account>();
        for(Implementer__c imp:impList )
        {
           if(imp.Add_Implementer__c ==null)
           {
               imp.addError('Please select an Add Implementer type');
           }
           else if(imp.Implementer_Name__c ==null)
           {
               imp.addError('Please enter a value for Name');
           }
           else
           {
               if(imp.Account__c == null)
               {               
                   Account newSR = new Account();               
                   if(imp.Implementer_Name__c !=null)
                   {
                       newSR.Name = imp.Implementer_Name__c;
                   }               
                   if(imp.Implementer_Short_Name__c !=null)
                   {
                       newSR.Short_Name__c = imp.Implementer_Short_Name__c;                   
                   }
                   if(imp.Implementer_Type__c !=null)
                   {
                       newSR.Type__c = imp.Implementer_Type__c;
                   }
                   if(imp.Implementer_Sub_Type__c !=null)
                   {
                       newSR.Sub_Type__c = imp.Implementer_Sub_Type__c;
                   }
                   if(imp.Country__c !=null)
                   {
                       newSR.Country__c = imp.Country__c;
                   }
                   if(pRId !=null)
                   {
                       newSR.RecordTypeId = pRId;
                   }
                   newSRList.add(newSR);
               }
           }
        }
        if(newSRList.size()>0)
        {
            try
            {
                insert newSRList;
            }
            catch(Exception e)
            {
                System.debug('Insertion of subRecipient failed due to: '+e);
            }        
            Map<String,Account> impSrMap =new Map<String,Account>();
            //Considering Name is a unique field and not blank for implementer
            for( Account a:newSRList)
            {
                impSrMap.put(String.valueOf(a.Name),a);
            }
            for(Implementer__c imp:impList )
            {
                if(impSrMap.Containskey(String.valueOf(imp.Implementer_Name__c)))
                {
                    imp.Account__c = impSrMap.get(String.valueOf(imp.Implementer_Name__c)).Id;
                }
            }
        }  */           
    }
    
    public static void SelectRecordType(List<Implementer__c> lstImp){
        //List<Performance_Framework__c> lstInsertImp = new List<Performance_Framework__c>();
       // Set<Id> pfid = new Set<Id>();
        /*
        RecordType NYS = [SELECT Id from RecordType where name = 'Not Yet Submitted or Returned' AND SobjectType ='Implementer__c' limit 1];
        RecordType Accpt = [SELECT Id from RecordType where name = 'Accepted' AND SobjectType ='Implementer__c' limit 1];
        RecordType Subm = [SELECT Id from RecordType where name = 'Submitted to MEPH - PR' AND SobjectType ='Implementer__c' limit 1];
        //List<RecordType> lstRt = [Select Id, Name From RecordType where sobjecttype = 'Implementer__c'];
        */
      /*  for(Implementer__c imp: lstImp){            
            pfid.add(imp.Performance_Framework__c);
        }
        
        lstInsertImp = [SELECT id,Implementation_Period__c, Implementation_Period__r.Principal_Recipient__c, Implementation_Period__r.Principal_Recipient__r.Country__c FROM Performance_Framework__c WHERE id IN:pfid];
        
        //Filling up Implementation Period and Principal Recipient for Implementer
        for(Implementer__c imp: lstImp){
            for(Performance_Framework__c pf: lstInsertImp){
                imp.Performance_Framework__c = pf.id;
                imp.Grant_Implementation_Period__c = pf.Implementation_Period__c;
                imp.Principal_Recipient__c = pf.Implementation_Period__r.Principal_Recipient__c;
                imp.Country__c = pf.Implementation_Period__r.Principal_Recipient__r.Country__c;
            }
        }*/
    }
}