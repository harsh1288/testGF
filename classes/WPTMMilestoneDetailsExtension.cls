//****************************************************************************************

// Purpose     :  This Class is used to Show milestone corresponding to the Key Activity    
// Date        :  12-Mar-2015
// Created By  :  TCS 
// Author      :  Ayush Pradhan
// Description :  This Class is used to Show milestone corresponding to the Key Activity with the help WPTMMilestoneDetailsVFP embeded on Standard Key Activity page.

Public class WPTMMilestoneDetailsExtension {
      
      Key_Activity__c kact;
      Milestone_Target__c mile1 {get; set;}
      Public string selectedMilestone{get; set;}
      public list<Milestone_Target__c> milstonelst{get;set;}
      public Milestone_Target__c milelist{get;set;}
      public list<Period__c > rplist{get;set;}
      public list<SelectOption> Countries{get;set;}
      public Key_Activity__c kactrecord;
      public Implementation_Period__c ImplObj;
      public string selectedCountry{get;set;}      
      public boolean displaymilepopup{get;set;}
      public boolean linktoBoolean{get;set;}
      public String profileName{get;set;}
      public List<WrapperMilestoneRPCls> wrapperList{get;set;}
      public List<WrapperMilestoneRPCls> wrapperMileList {get;set;}  
      public list<Milestone_RP_Junction__c> junclist; 
      Public List<Milestone_Target__c> milestoneToDelete;
      public Performance_Framework__c  performanceFrameworkRec {get;set;} 
      public WPTMSecurityObject  securityMatrixObj{get;set;}
      public String performanceFrameworkstatus{get;set;}
      public User userRec ;
      Public Id loggedInUserId;
      public String contactId; 
      public List<npe5__Affiliation__c> affiliationList;
      public String performanceFrameworkaccountId;

      //Constructor
        public WPTMMilestoneDetailsExtension(ApexPages.StandardController controller) {
          //securityMatrixObj= new WPTMSecurityObject  ();
          //performanceFrameworkRec = new Performance_Framework__c  ();
          kact=(Key_Activity__c)controller.getRecord();                
          fetchmilestonesandRperiodList();
          Countries=getMileStoneCountries();
          performanceFrameworkRec =[SELECT Implementation_Period__r.GIP_Type__c,PF_Status__c FROM Performance_Framework__c 
                                WHERE id=:kactrecord.Performance_Framework__c] ;
          performanceFrameworkstatus =performanceFrameworkRec.PF_Status__c;
          
          loggedInUserId= userinfo.getUserId() ;                            
          Id profileId=userinfo.getProfileId();
          profileName=[Select Id,Name from Profile where Id=:profileId].Name;
          
          
          securityMatrixObj = WPTMSecurityObject.XMLDomDocument(profileName,performanceFrameworkstatus);
        }
        
        //This function is to fetch the milestone and corresponding Reporting period(from junction object Milestone_RP_Junction__c)
        public void fetchmilestonesandRperiodList(){
            wrapperMileList = new List<WrapperMilestoneRPCls>();
            milstonelst = new list<Milestone_Target__c>();
            kactrecord = [select id,Performance_Framework__c,Grant_Implementation_Period__c from Key_Activity__c where id=:kact.id];
            milstonelst=[select id, MilestoneTitle__c,Criteria__c,Linked_To__c,Reporting_Period__c from Milestone_Target__c where Key_Activity__c =:kact.id ORDER BY CreatedDate ASC];      
            list<Milestone_RP_Junction__c> mileJunclst = new list<Milestone_RP_Junction__c>([select id,Milestone_Target__c,Reporting_Period__c,Period_Number__c from Milestone_RP_Junction__c where Milestone_Target__c=:milstonelst ORDER BY Period_Number__c]);
            for(Milestone_Target__c m : milstonelst){
                string temp ='';
                if(!mileJunclst.isEmpty()){
                    for(Milestone_RP_Junction__c mj : mileJunclst){
                        if(m.id ==mj.Milestone_Target__c){
                            if(temp==''){
                              temp=String.valueOf(mj.Period_Number__c) ;
                            }else{
                              temp=temp+';'+String.valueOf(mj.Period_Number__c);  
                            }
                        }
                    }
                }
             WrapperMilestoneRPCls obj = new WrapperMilestoneRPCls(temp,m); 
             wrapperMileList.add(obj); //Wrapper list to display the record in Grid on page
            }
        }       
        
        //This function is to fetch list to Grant Countries and PR from Grant Implementation Period to associate it with 'Linked to' on Milestone
        public List<SelectOption> getMileStoneCountries(){
           Countries= new List<SelectOption>();
           ImplObj = [select id, GIP_Type__c, Principal_Recipient__r.name, (select Country__r.name from Grant_Multi_Countries__r) from Implementation_Period__c where id =:kactrecord .Grant_Implementation_Period__c];                    
           
           Countries.add(new SelectOption(' ','--None--'));
           list<string>linklist = new list<String>();
           if(ImplObj.Principal_Recipient__r.name != null){
               linklist.add(ImplObj.Principal_Recipient__r.name);
           }
           if(ImplObj.GIP_Type__c == 'Multi-Country' || ImplObj.GIP_Type__c == 'Regional'){
               linktoBoolean=true;    
               for(Grant_Multi_Country__c impl : ImplObj.Grant_Multi_Countries__r){
                   if(impl.Country__r.name != null){
                       linklist.add(impl.Country__r.name); 
                   }                
               }
           }    
           linklist.sort();
           for(String s : linklist){
               Countries.add(new SelectOption(s,s));
           }
           return Countries;           
        }
        
        //This function is to close the Reporting Period Popup on the Visualforce page.
        public void cancelPopUp(){
            displaymilepopup= false;    
        }   
        
        //This function is to fetch the Reporting period with respect to the Grant Implementation Period
        public void fetchReportingPeriod(){
            displaymilepopup=true;
            string mid= ApexPages.currentPage().getParameters().get('selectedMilestone');
            junclist = new  list<Milestone_RP_Junction__c>([select id,Milestone_Target__c,Reporting_Period__c,Period_Number__c from Milestone_RP_Junction__c where Milestone_Target__c=:mid]);
            milelist = [select id,MilestoneTitle__c,Reporting_Period__c from Milestone_Target__c where id=:mid];
            rpList = new list<Period__c>();
            rplist=[select id, Period_Number__c,End_Date__c,Start_Date__c from Period__c where Performance_Framework__c =:kactrecord.Performance_Framework__c];
            wrapperList = new List<WrapperMilestoneRPCls>();
              
            for(Period__c p :rplist){
                Boolean check=false;
                for(Milestone_RP_Junction__c mj : junclist){                
                    if(p.id == mj.Reporting_Period__c){
                        check=true;
                      }  
                }
              
              WrapperMilestoneRPCls obj = new WrapperMilestoneRPCls(check,p,milelist);
              wrapperList.add(obj);//Wrapper list to bind the checkbox.
            }
         }
        
        //This Function is to save the milestone record on inline edit or while adding new milestone.
        public PageReference savingCustomRecord(){
          list<Milestone_Target__c> mlist = new list<Milestone_Target__c> ();
          try{
              for (WrapperMilestoneRPCls w :wrapperMileList ){
                     mlist.add(w.mobj);
                 }
                 upsert mlist;
             PageReference nextpage= new PageReference('/apex/WPTMMilestoneDetailsVFP?id='+kactrecord .id);
               return nextpage;
          }
          catch(Exception e){
                ApexPages.addMessages(e);
                return null;
            }
        }
        
        //This function is to create a Record on the Junction object(Milestone_RP_Junction__c) when user select the Checkbox corresponding to Reporting Period
        Public void saveReportingPeriod(){
          list<Milestone_RP_Junction__c> juncObjlist = new list<Milestone_RP_Junction__c>();
          list<Milestone_RP_Junction__c> newjuncObjlist = new list<Milestone_RP_Junction__c>();
          Milestone_RP_Junction__c junctionRec;
          list<Milestone_RP_Junction__c> deleteMileList = new list<Milestone_RP_Junction__c>();
          Set<Id> mileTarg = new Set<Id>();
          Set<Id> repoPerd = new Set<Id>();    
          for(WrapperMilestoneRPCls objWrapper : wrapperList){
              if(objWrapper.Checked==true){
                           junctionRec = new Milestone_RP_Junction__c();
                           junctionRec.Reporting_Period__c = objWrapper.pobj.id;
                           repoPerd.add(objWrapper.pobj.id);
                           mileTarg.add(objWrapper.mobj.id);
                           junctionRec.Milestone_Target__c = objWrapper.mobj.id;
                           juncObjlist.add(junctionRec);
                }else{ 
                    for(Milestone_RP_Junction__c jnObj : junclist){
                        if(jnObj.Reporting_Period__c==objWrapper.pobj.id && jnObj.Milestone_Target__c == objWrapper.mobj.id){
                            deleteMileList.add(jnObj);
                        }
                    }
                }
            }
           Set<string> strUnique=new Set<string>();
           list<Milestone_RP_Junction__c> milestoneRPJunclst = new list<Milestone_RP_Junction__c>([SELECT Id,Name,Milestone_Target__c,Reporting_Period__c FROM Milestone_RP_Junction__c WHERE Milestone_Target__c in:mileTarg AND Reporting_Period__c in:repoPerd]); 
           if(!milestoneRPJunclst.isempty()){
               for(Milestone_RP_Junction__c mjobj : milestoneRPJunclst){
                    strUnique.add(mjobj.Milestone_Target__c+'&'+mjobj.Reporting_Period__c);
                }
            }
           
            for(Milestone_RP_Junction__c jObj: juncObjlist){
                         if(!strUnique.contains(jObj.Milestone_Target__c+'&'+jObj.Reporting_Period__c))
                         {
                            newjuncObjlist.add(jObj);
                         }
            }
           
           if(!newjuncObjlist.isempty()){    
               insert newjuncObjlist;
           }   
           //insert newjuncObjlist;
           
           delete deleteMileList;
           displaymilepopup= false;   
           fetchmilestonesandRperiodList();                  
        }
        
        //This function is to Add a dummy Row on page when user clicks on 'Add Milestone' button.
        public void AddRow() {        
            mile1= new Milestone_Target__c ();         
            mile1.Key_Activity__c= kactrecord.Id;
            WrapperMilestoneRPCls obj = new WrapperMilestoneRPCls('',mile1);           
            wrapperMileList.add(obj);              
        }     
        
        //This function is to delete the Milestone.
        public PageReference confirmDeleteMilestone(){               
            String mileId = ApexPages.currentPage().getParameters().get('contextItem');           
             try{
                 if(mileId != null){
                     milestoneToDelete = [Select Id from Milestone_Target__c where id=:mileId];
                     }
                  if(milestoneToDelete != null){                 
                   delete milestoneToDelete ;
                   }              
                   return new Pagereference('/apex/WPTMMilestoneDetailsVFP?id='+kact.id).setRedirect(True);
           }catch(Exception ex){
                ApexPages.addMessages(ex);
            return null;
            }
        }     
             
        //Wrapper Class
         Public class WrapperMilestoneRPCls{
              
            public Period__c pobj{get;set;}
            public String periodNumber{get;set;}
            public Boolean checked{get;set;}
            public Boolean Original_selection {get; set;} 
            public Milestone_Target__c mobj{get;set;}
            public Milestone_RP_Junction__c mrpobj{get;set;}

            
            public WrapperMilestoneRPCls(boolean c, Period__c p,Milestone_Target__c m){
                pobj= p;
                checked=c;
                mobj=m;
            }
            
            public WrapperMilestoneRPCls(String rp,Milestone_Target__c m){
                periodNumber=rp;
                mobj = m;
            }

        }
}