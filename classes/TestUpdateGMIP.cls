@isTest
public class TestUpdateGMIP{
Public static testMethod void TesttrgUpdateGMIP(){

    Account objAcc= TestClassHelper.createAccount();
    insert objAcc;
    test.startTest();
    Grant__c objGrant = TestClassHelper.createGrant(objAcc);
    insert objGrant;
        
        Concept_Note__c objCN = TestClassHelper.createCN();
        objCN.CCM_new__c = objAcc.id;
        insert objCN;
        
        Implementation_Period__c objIP= TestClassHelper.createIP(objGrant,objAcc);
        objIP.Concept_Note__c = objCN.Id;
        objIP.Status__c = 'Concept-note';
        objIP.PR_Fiscal_Cycle__c = 'January-December';
        insert objIP;

        Page__c objPage = TestClassHelper.createPage();
        objPage.Implementation_Period__c = objIP.Id;
        insert objPage;
        
        Goals_Objectives__c objGoal = TestClassHelper.createGoalsObjectives();
        objGoal.Concept_note__c = objCN.Id;
        insert objGoal;
        
        Catalog_Module__c objCM = TestClassHelper.createCatalogModule();
        insert objCM;
        
        Catalog_Module__c objCM1 = TestClassHelper.createCatalogModule();
        objCM1.Name = 'Test CM1';
        objCM1.Component__c = 'Malaria';
        objCM1.component_multi__c = 'Malaria';
        insert objCM1;
        
        Module__c objModule = TestClassHelper.createModule();
        //objModule.Implementation_Period__c = objIP.Id;
        objModule.Catalog_Module__c = objCM.id;
        objModule.Component__c = 'Malaria';
        insert objModule;
        
        Catalog_Intervention__c objCI = TestClassHelper.createCatalogIntervention();
        objCI.Catalog_Module__c  = objCM.Id;
        insert objCI; 
        
        Indicator__c objCatInd = TestClassHelper.createCatalogIndicator();
        objCatInd.Indicator_Type__c = 'Coverage/Output';
        insert objCatInd;
        
        Indicator__c objCatInd2 = TestClassHelper.createCatalogIndicator();
        objCatInd2.Indicator_Type__c = 'Impact';
        insert objCatInd2;
        
        Grant_Indicator__c objInd = TestClassHelper.createGrantIndicator();
        objInd.Concept_note__c = objCN.Id;
        objInd.Indicator_Type__c = 'Impact';
        objInd.Standard_or_Custom__c = 'Standard';
        objInd.Indicator__c = objCatInd2.Id;
        objInd.Goal_Objective__c = objGoal.Id;
        insert objInd;
        
        Module__c objIPModule = TestClassHelper.createModule();
        objIPModule.Implementation_Period__c = objIP.Id;
        //objIPModule.Catalog_Module__c = objCM.id;
        objIPModule.Component__c = 'Malaria';
        insert objIPModule;
        
        Grant_Indicator__c objInd1 = TestClassHelper.createGrantIndicator();
        objInd1.Grant_Implementation_Period__c = objIP.Id;
        objInd1.Indicator_Type__c = 'Coverage/Output';
        objInd1.Standard_or_Custom__c = 'Standard';
        objInd1.Parent_Module__c = objIPModule.Id;
        objInd1.Indicator__c = objCatInd.Id;
        insert objInd1;
        
        Grant_Intervention__c objInt = TestClassHelper.createGrantIntervention(objIP);
        objInt.Name = 'TestIntervention';
        objInt.Module__c = objIPModule.Id;
        insert objInt;
        test.stopTest();
        Ind_Goal_Jxn__c objJxn = TestClassHelper.insertIndicatorGoalJxn(objGoal,objInd);
        
        objIP.Status__c = 'Grant-Making';
        
        update objIP;
}

Public static testMethod void TesttrgUpdateGMIP2(){
    
        Country__c objCon = TestClassHelper.createCountry();
        User objUser = [Select ID, country from User where id=:userinfo.getUserId()];
        objCon.MEPH_Specialist__c = objUser.ID;
        objCon.HPM_Specialist__c = objUser.ID;
        insert objCon;
        
        Account objAcc= TestClassHelper.createAccount();
        insert objAcc;
        test.startTest();
        Grant__c objGrant = TestClassHelper.createGrant(objAcc);
        objGrant.Disease_Component__c = 'HIV/TB';
        objGrant.Country__c = objCon.Id;
        insert objGrant;
            
        Concept_Note__c objCN = TestClassHelper.createCN();
        objCN.CCM_new__c = objAcc.id;
        insert objCN;
        
        Implementation_Period__c objIP= TestClassHelper.createIP(objGrant,objAcc);
        objIP.Concept_Note__c = objCN.Id;
        objIP.PR_Fiscal_Cycle__c = 'January-December';
        insert objIP;

        Page__c objPage = TestClassHelper.createPage();
        objPage.Implementation_Period__c = objIP.Id;
        insert objPage;
        
        Goals_Objectives__c objGoal = TestClassHelper.createGoalsObjectives();
        objGoal.Concept_note__c = objCN.Id;
        insert objGoal;
        
        Catalog_Module__c objCM = TestClassHelper.createCatalogModule();
        insert objCM;
        
        Catalog_Module__c objCM1 = TestClassHelper.createCatalogModule();
        objCM1.Name = 'Test CM1';
        objCM1.Component__c = 'Malaria';
        objCM1.component_multi__c = 'Malaria';
        insert objCM1;
        
        Module__c objModule = TestClassHelper.createModule();
        //objModule.Implementation_Period__c = objIP.Id;
        objModule.Catalog_Module__c = objCM.id;
        objModule.Component__c = 'Malaria';
        insert objModule;
        
        Catalog_Intervention__c objCI = TestClassHelper.createCatalogIntervention();
        objCI.Catalog_Module__c  = objCM.Id;
        insert objCI; 
        
        Indicator__c objCatInd = TestClassHelper.createCatalogIndicator();
        objCatInd.Indicator_Type__c = 'Coverage/Output';
        insert objCatInd;
        
        Indicator__c objCatInd2 = TestClassHelper.createCatalogIndicator();
        objCatInd2.Indicator_Type__c = 'Impact';
        insert objCatInd2;
        
        Grant_Indicator__c objInd = TestClassHelper.createGrantIndicator();
        objInd.Concept_note__c = objCN.Id;
        objInd.Indicator_Type__c = 'Impact';
        objInd.Standard_or_Custom__c = 'Standard';
        objInd.Indicator__c = objCatInd2.Id;
        objInd.Goal_Objective__c = objGoal.Id;
        insert objInd;
        
        Module__c objIPModule = TestClassHelper.createModule();
        objIPModule.Implementation_Period__c = objIP.Id;
        //objIPModule.Catalog_Module__c = objCM.id;
        objIPModule.Component__c = 'Malaria';
        insert objIPModule;
        
        Grant_Indicator__c objInd1 = TestClassHelper.createGrantIndicator();
        objInd1.Grant_Implementation_Period__c = objIP.Id;
        objInd1.Indicator_Type__c = 'Coverage/Output';
        objInd1.Standard_or_Custom__c = 'Standard';
        objInd1.Parent_Module__c = objIPModule.Id;
        objInd1.Indicator__c = objCatInd.Id;
        insert objInd1;
        
        Grant_Intervention__c objInt = TestClassHelper.createGrantIntervention(objIP);
        objInt.Name = 'TestIntervention';
        objInt.Module__c = objIPModule.Id;
        insert objInt;
        test.stopTest();
        Ind_Goal_Jxn__c objJxn = TestClassHelper.insertIndicatorGoalJxn(objGoal,objInd);
        
        objIP.Status__c = 'Grant-Making';
        
        update objIP;
    }
}