/*********************************************************************************
* Controller Class: CancelInvoice
* Created by Vera Solutions, DateCreated:  02/14/2014
----------------------------------------------------------------------------------
* Purpose/Methods:
* - 
----------------------------------------------------------------------------------
* Unit Test: 
----------------------------------------------------------------------------------
* History:
* - VERSION     DATE            DETAIL FEATURES
    1.0         14 Feb 2014     INITIAL DEVELOPMENT   
*********************************************************************************/


global Class NotifyLFA{
    WebService static String setNotification(String strchangeRequestId) {
        try{
            if(String.isBlank(strchangeRequestId) == false){
                List<Change_Request__c> lstChangeRequest = [Select ID,Notify_LFA__c from Change_Request__c
                                                        where id = :strchangeRequestId];
                
            if(lstChangeRequest != null && lstChangeRequest.size() > 0){
                lstChangeRequest[0].notify_LFA__c=true;
                Update lstChangeRequest;
            }
            //Chatter Code
            List<User> lstuser = [Select Firstname,Lastname from User where id=: UserInfo.getUserId()];
            String strblob='';
            strblob+= lstuser[0].Firstname + ' ' + lstuser[0].Lastname + ' notified the LFA on '+ DateTime.Now().Format('yyyy-MM-dd') + '.';
            // UserFirst Name UserLast Name on today'sdatetime
            FeedItem document = new FeedItem();
            document.Body = strblob;
            //document.ContentData = blob.valueof(strblob);
            document.ParentId = lstChangeRequest[0].id;
            document.Type = 'TextPost';
            //document.ContentFileName = 'Test.txt';
            //document.Title = strFileName;
            insert document;
            }
            
        }catch(exception ex){
           return ex.getMessage();
        }            
        return '';
    }
}