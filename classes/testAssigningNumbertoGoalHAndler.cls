@istest
public with sharing class testAssigningNumbertoGoalHAndler {
   static testMethod void testAssigningNumbertoGoalHAndler(){
   	  Account objacct = TestClassHelper.createAccount();
   	  insert objacct;
   	  
   	  Grant__c objGrant = TestClassHelper.createGrant(objacct);
   	  objGrant.Principal_Recipient__c = objacct.id;
   	  insert objGrant;
   	  
   	  Implementation_Period__c objIP = TestClassHelper.createIPWithConceptNote(objGrant,objacct);
   	  objIP.Grant__c = objGrant.Id;
   	  objIP.Principal_Recipient__c = objacct.Id;
   	  insert objIP;
   	  
   	  Performance_Framework__c objPF = TestClassHelper.createPF(objIP);
   	  objPF.Implementation_Period__c = objIP.id;
   	  insert objPF;
   	  
   	  Goals_Objectives__c objGoal = new Goals_Objectives__c();
   	  objGoal.Type__c = 'Goal';
   	  objGoal.Performance_Framework__c = objPF.Id;
   	  objGoal.Implementation_Period__c = objIP.Id;
   	  insert objGoal;
   	  
   	  Goals_Objectives__c objobj = new Goals_Objectives__c();
   	  objobj.Type__c = 'Objective';
   	  objobj.Performance_Framework__c = objPF.Id;
   	  objobj.Implementation_Period__c = objIP.Id;
   	  insert objobj;
   	   
   	  List<Goals_Objectives__c> lstgoal = new  List<Goals_Objectives__c>();
   	  lstgoal.add(objGoal);
   	  
   	  List<Goals_Objectives__c> lstobj = new List<Goals_Objectives__c>();
   	  lstobj.add(objobj);
   	  
   	  AssigningNumbertoGoalhandler.AssigningNumberinsert(lstgoal);
   	  AssigningNumbertoGoalhandler.AssigningNumberinsert(lstobj);
   	  AssigningNumbertoGoalhandler.AssigningNumberdelete(lstgoal);
   	  AssigningNumbertoGoalhandler.AssigningNumberdelete(lstobj);
   }
}