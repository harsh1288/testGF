@isTest
public class ImplementationPeriodTriggerHandler_test {
      Public static testMethod void Testgeneratereportingperiods(){
       Test.startTest();
        Account objAcc = TestClassHelper.insertAccount();
       
        Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
        insert objGrant;
        
        Concept_Note__c objCN = TestClassHelper.createCN();
        objCN.CCM_New__c = objAcc.id;
        insert objCN;
        
        List<Implementation_Period__c> implementationPeriodList = new List<Implementation_Period__c>();
       
        Implementation_Period__c objIP = TestClassHelper.createIP(objGrant,objAcc);
        objIP.Concept_Note__c = objCN.Id;
        objIP.GenerateReportingPeriod__c = true;
        objIP.Start_Date__c = Date.today();
        objIP.End_Date__c = Date.today().addYears(3);
        objIP.Reporting__c =  'Yearly';
        objIP.PR_Fiscal_Cycle__c = 'January-December';
        insert objIP;
        
        implementationPeriodList.add(objIP);
       
        Contact Con = new Contact();
        con.LastName = 'Testing';
        Con.email = 'standarduser@testorg.com';
        con.accountid= objAcc.id;
        
        insert con;
              
       Performance_Framework__c objPF = TestClassHelper.createPF(objIP);
       objPF.Implementation_Period__c = objIP.Id;
       objPF.PF_Status__c = 'Accepted';
       insert objPF;
       
       
       Period__c ObjRP = TestClassHelper.createPeriod();
        ObjRP.Implementation_Period__c = objIP.Id;
        ObjRP.Start_Date__c = Date.today();
        ObjRP.EFR__c=true;
        ObjRP.EFR_Due_Date__c = system.today();
        ObjRP.PU__c=true;
        ObjRP.PU_Due_Date__c =  system.today();
        ObjRP.Audit_Report__c=true;
        ObjRP.AR_Due_Date__c=system.today().addDays(10);
        ObjRP.DR__c=true;
        ObjRP.Type__c = 'Reporting';
        ObjRP.Due_Date__c=system.today().addDays(2);
        insert ObjRP;
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Applicant/CM Admin']; 
       
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id,TimeZoneSidKey='America/Los_Angeles', 
        contactid=con.id,UserName='s@testorg.com');
        insert u;
       
        con.External_User__c = TRUE;
        con.Firstname  = 'TestFname';
        update con;

        list<npe5__Affiliation__c> AffAccCon = [select Id,npe5__Status__c from npe5__Affiliation__c where npe5__Organization__c =:objAcc.id AND npe5__Contact__c =:con.id];
        AffAccCon[0].npe5__Status__c = 'former';
        update AffAccCon[0];
                
        npe5__Affiliation__c objAff = new npe5__Affiliation__c();
        objAff.Is_Inactive_c__c = false;
        objAff.Access_Level__c = 'Admin';
        objAff.npe5__Organization__c  = objAcc.Id;
        objAff.npe5__Contact__c  = con.Id;
        objAff.RecordTypeId = label.CM_Affiliation_RT;
        objAff.npe5__Status__c = 'Current';
        insert objAff;        
        Test.stopTest();
        ImplementationPeriodTriggerHandler.shareIPRecord(implementationPeriodList);             
   }
   Public static testMethod void TestUpdateOnApprovalStatusChangeExp_Imp(){
	   test.startTest();
       Country__c objcountry =  TestClassHelper.createCountry();
       objcountry.CT_Public_Group_ID__c = '00Gb0000000tjuUEAQ';
       insert objcountry;
       
       Account objAcc = TestClassHelper.createAccount();
       objAcc.Country__c = objcountry.Id;
       insert  objAcc;      
       
        Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
        insert objGrant;
        
        Concept_Note__c objCN = TestClassHelper.createCN();
        objCN.CCM_New__c = objAcc.id;
        insert objCN;
        
        Contact Con = new Contact();
        con.LastName = 'Testing';
        Con.email = 'standarduser@testorg.com';
        con.accountid= objAcc.id;
        insert con;
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Applicant/CM Admin']; 
       
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id,TimeZoneSidKey='America/Los_Angeles', 
        contactid=con.id,UserName='s@testorg.com');
        insert u;
        
        Map<id, Implementation_Period__c> implMap = new Map<id, Implementation_Period__c>();
        
        List<Implementation_Period__c> implementationPeriodList = new List<Implementation_Period__c>();
       
        Implementation_Period__c objIP = TestClassHelper.createIP(objGrant,objAcc);
        objIP.Concept_Note__c = objCN.Id;
        objIP.GenerateReportingPeriod__c = true;
        objIP.Start_Date__c = Date.today();
        objIP.End_Date__c = Date.today().addYears(3);
        objIP.Reporting__c =  'Yearly';
        objIP.PR_Fiscal_Cycle__c = 'January-December';
        objIP.SF_Approval_Status_Change__c = true;
        objIP.Locked__c = false;
        objIP.Approval_Status__c = 'Send to finance system for approval';
        objIP.CT_Finance_Officer__c = u.Id;        
        insert objIP;
        ImpTrgRecursionController.resetAlreadyRunAfterImp();
        objIP.Approval_Status__c = 'Finance Officer verification';
        update objIP;
        ImpTrgRecursionController.setAlreadyRunAfterImp();
        implementationPeriodList.add(objIP);
                system.debug('@@'+implementationPeriodList);

        
        Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
        req1.setComments('Submitting request for approval.');
        req1.setObjectId(objIP.id);
		implMap.put(objIP.Id, objIP);
        test.stopTest();
        ImplementationPeriodTriggerHandler.UpdateOnApprovalStatusChangeExp_Imp(implementationPeriodList, implMap,true , true);
    }
    Public static testMethod void GIPApprovalUpdateBankAccount(){
       Country__c objcountry =  TestClassHelper.createCountry();
       objcountry.CT_Public_Group_ID__c = '00Gb0000000tjuUEAQ';
       insert objcountry;
       
       Account objAcc = TestClassHelper.createAccount();
       objAcc.Country__c = objcountry.Id;
       insert  objAcc;      
       
       Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
       insert objGrant;
       
       Bank_Account__c objbac = TestClassHelper.createBankAcc();
       objbac.Bank_Account_Validity_Period_Start_Date__c = date.today();
       objbac.Bank_Account_Validity_Period_End_Date__c = date.today().addyears(3);
       objbac.Account__c = objAcc.id;
       insert objbac; 
       
       Bank_Account__c objbac1 = TestClassHelper.createBankAcc();
       objbac1.Bank_Account_Validity_Period_Start_Date__c = date.today();
       objbac1.Bank_Account_Validity_Period_End_Date__c = date.today().addyears(3);
       objbac1.Account__c = objAcc.id;
       insert objbac1; 
       
       Implementation_Period__c objimp = TestClassHelper.createIPWithConceptNote(objGrant, objAcc);
       objimp.Approval_Status__c = 'Approved';
       objimp.Bank_Account__c = objbac.Id;
       insert objimp;
        
       Implementation_Period__c objimp1 = TestClassHelper.createIPWithConceptNote(objGrant, objAcc);
       objimp1.Approval_Status__c = 'Update information';
       objimp1.Bank_Account__c = objbac.Id;
       objimp1.Name = 'Test 1';
       insert objimp1;
       
       objimp1.Name = 'Test 123';
       objimp1.Approval_Status__c = 'Approved';
       objimp1.Bank_Account__c = objbac1.Id;
       update objimp1;
       
       
    }
    Public static testMethod void UpdateGMIP(){
        test.startTest();
       Country__c objcountry =  TestClassHelper.createCountry();
       objcountry.CT_Public_Group_ID__c = '00Gb0000000tjuUEAQ';
       insert objcountry;
       
       Account objAcc = TestClassHelper.createAccount();
       objAcc.Country__c = objcountry.Id;
       insert  objAcc;      
       
       Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
       insert objGrant;
       
       Profile p = [select Id from Profile where Name =:'Super User' limit 1];
       User objuser = new user();
       objuser.ProfileId = p.Id;
       objuser.username = 'newUser@yahoo.com';
       objuser.email = 'pb@ff.com';
       objuser.emailencodingkey = 'UTF-8';
       objuser.localesidkey = 'en_US';
       objuser.languagelocalekey = 'en_US';
       objuser.timezonesidkey = 'America/Los_Angeles';
       objuser.alias='nuser';
       objuser.LastName = 'lastname';
       insert objuser;      
       
       List<Implementation_Period__c> implementationPeriodList = new List<Implementation_Period__c>();
       
       Implementation_Period__c objimp = TestClassHelper.createIPWithConceptNote(objGrant, objAcc); 
       objimp.Status__c = 'Grant-Making';
       objimp.Grant__c = objGrant.Id;
       objimp.Principal_Recipient__c = objAcc.Id;
       insert objimp; 
       Performance_Framework__c objPF = TestClassHelper.createPF(objimp);
       insert objPF;
       HPC_Framework__c objHpc = TestClassHelper.createHPC();
       objHpc.Grant_Implementation_Period__c = objimp.id;
	   insert objHpc;
       IP_Detail_Information__c objIpd = TestClassHelper.createIPDetail();
       objIpd.Implementation_Period__c = objimp.id;
	   insert objIpd;       
       ImpTrgRecursionController.resetAlreadyRunBeforeUpdateImp();
       objimp.Status__c = 'Grant-Making'; 
       objimp.Name = 'NGA-T-UNDP'; 
       update objimp;
       ImpTrgRecursionController.setAlreadyRunBeforeUpdateImp();
       test.stopTest();
    }
Public static testMethod void UpdateGMIPonlyName(){
        test.startTest();
       Country__c objcountry =  TestClassHelper.createCountry();
       objcountry.CT_Public_Group_ID__c = '00Gb0000000tjuUEAQ';
       insert objcountry;
       
       Account objAcc = TestClassHelper.createAccount();
       objAcc.Country__c = objcountry.Id;
       insert  objAcc;      
       
       Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
       insert objGrant;
       
       Profile p = [select Id from Profile where Name =:'Super User' limit 1];
       User objuser = new user();
       objuser.ProfileId = p.Id;
       objuser.username = 'newUser@yahoo.com';
       objuser.email = 'pb@ff.com';
       objuser.emailencodingkey = 'UTF-8';
       objuser.localesidkey = 'en_US';
       objuser.languagelocalekey = 'en_US';
       objuser.timezonesidkey = 'America/Los_Angeles';
       objuser.alias='nuser';
       objuser.LastName = 'lastname';
       insert objuser;      
       
       List<Implementation_Period__c> implementationPeriodList = new List<Implementation_Period__c>();
       
       Implementation_Period__c objimp = TestClassHelper.createIPWithConceptNote(objGrant, objAcc); 
       objimp.Status__c = 'Grant-Making';
       objimp.Grant__c = objGrant.Id;
       objimp.Principal_Recipient__c = objAcc.Id;
       insert objimp; 
       Performance_Framework__c objPF = TestClassHelper.createPF(objimp);
       insert objPF;
       HPC_Framework__c objHpc = TestClassHelper.createHPC();
       objHpc.Grant_Implementation_Period__c = objimp.id;
	   insert objHpc;
       IP_Detail_Information__c objIpd = TestClassHelper.createIPDetail();
       objIpd.Implementation_Period__c = objimp.id;
	   insert objIpd;       
       ImpTrgRecursionController.resetAlreadyRunBeforeUpdateImp();
       objimp.Status__c ='Grant-Making';
       objimp.Name = 'NGA-M-UNDP'; 
       update objimp;
       ImpTrgRecursionController.setAlreadyRunBeforeUpdateImp();
       test.stopTest();
    }
    Public static testMethod void reopenFramework(){
       Country__c objcountry =  TestClassHelper.createCountry();
       objcountry.CT_Public_Group_ID__c = '00Gb0000000tjuUEAQ';
       insert objcountry;
       
       Account objAcc = TestClassHelper.createAccount();
       objAcc.Country__c = objcountry.Id;
       insert  objAcc;      
       
       Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
       insert objGrant;
       
       Implementation_Period__c objimp = TestClassHelper.createIPWithConceptNote(objGrant, objAcc);
       objimp.Approval_Status__c = 'Approved';
       insert objimp;
       
       Implementation_Period__c objimp1 = [select Approval_Status__c from  Implementation_Period__c where id=:objimp.Id ];
       objimp1.Approval_Status__c = 'Update information';
       update objimp1;
      
    }
    
    Public static testMethod void TestUpdateGMIP(){
        Account objAcc = TestClassHelper.insertAccount();
       
        Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
        insert objGrant;
        
        Contact Con = new Contact();
        con.LastName = 'Testing';
        Con.email = 'standarduser@testorg.com';
        con.accountid= objAcc.id;
        insert con;
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Super User']; 
       
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id,TimeZoneSidKey='America/Los_Angeles', 
        UserName='s@testorg.com');
        insert u;
        
        Concept_Note__c objCN = TestClassHelper.createCN();
        objCN.CCM_New__c = objAcc.id;
        insert objCN;
        
        Map<id, Implementation_Period__c> implMap = new Map<id, Implementation_Period__c>();
        
        List<Implementation_Period__c> newimplst = new List<Implementation_Period__c>();
        List<Implementation_Period__c> implstnew = new List<Implementation_Period__c>();
        
        Implementation_Period__c objIP = TestClassHelper.createIP(objGrant,objAcc);
        objIP.Concept_Note__c = objCN.Id;
        objIP.GenerateReportingPeriod__c = true;
        objIP.Start_Date__c = Date.today();
        objIP.End_Date__c = Date.today().addYears(3);
        objIP.Reporting__c =  'Yearly';
        objIP.PR_Fiscal_Cycle__c = 'January-December';
        objIP.SF_Approval_Status_Change__c = true;
        objIP.Locked__c = false;
        objIP.Approval_Status__c = 'Approved';
        objIP.CT_Finance_Officer__c = u.Id;
        objIP.Status__c = 'Concept Note';
        objIP.Name = 'test';
        insert objIP;
        
        implMap.put(objIP.Id, objIP);
        
        objIP.Name = 'testing';
        objIP.Status__c = 'Grant-Making';
        objIP.Approval_Status__c = 'Approved';
        newimplst.add(objIP);
        update newimplst;
        
        system.debug('@@@'+newimplst);
        system.debug('@@@'+objIP.Status__c);
        
        ImplementationPeriodTriggerHandler.UpdateGMIP(newimplst, implMap);
        Test.startTest();
        Performance_Framework__c objPF = TestClassHelper.createPF(objIP);
        objPF.PF_Status__c = 'Accepted';
        insert objPF;
        
        HPC_Framework__c objHPC = TestClassHelper.createHPC();
        objHPC.Grant_Implementation_Period__c = objIP.Id;
        objHPC.HPC_Status__c = 'Accepted';
        insert objHPC;
        
        IP_Detail_Information__c objDB = TestClassHelper.createIPDetail();
        objDB.Implementation_Period__c =  objIP.Id;
        objDB.Budget_Status__c = 'Accepted';
        insert objDB;
        Test.stoptest();

           
        objIP = TestClassHelper.createIP(objGrant,objAcc);
        objIP.Concept_Note__c = objCN.Id;
        objIP.GenerateReportingPeriod__c = true;
        objIP.Start_Date__c = Date.today();
        objIP.End_Date__c = Date.today().addYears(3);
        objIP.Reporting__c =  'Yearly';
        objIP.PR_Fiscal_Cycle__c = 'January-December';
        objIP.SF_Approval_Status_Change__c = true;
        objIP.Locked__c = false;
        objIP.Approval_Status__c = 'Approved';
        objIP.CT_Finance_Officer__c = u.Id;
        objIP.Status__c = 'Concept Note';
        objIP.Name = 'test';
        insert objIP;
        
        implMap.put(objIP.Id, objIP);
        
        objIP.Name = 'testing';
        objIP.Status__c = 'Grant-Making';
        objIP.Approval_Status__c = 'Update information';
        newimplst.add(objIP);
        update newimplst;
         
        system.debug(newimplst[0].Approval_Status__c +' ****');
        ImplementationPeriodTriggerHandler.reopenFramework(newimplst, implMap);

    }
    
}