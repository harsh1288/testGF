/*********************************************************************************
* {Test} Class: {TestCancelInvoice}
* Created by {DeveloperName},  {DateCreated 05/09/2013}
----------------------------------------------------------------------------------
* Purpose/Methods:
* - Used for cover all methods of CancelInvoice Controller.* 
*
----------------------------------------------------------------------------------
* History:
* - VERSION  DEVELOPER NAME    DATE            DETAIL FEATURES
   1.0                        05/09/2013
*********************************************************************************/

@isTest
Public class TestCancelInvoice{
    Public static testMethod Void TestCancelInvoice(){
        Account objAcc = new Account();
        objAcc.Name = 'Test Acc';
        insert objAcc;
        
        Country__c objCountry = new Country__c();
        objCountry.Name = 'Test Country';
        objCountry.Region_Name__c ='Test Region';
        insert objCountry;
        
        LFA_Work_Plan__c objWp = new LFA_Work_Plan__c();
        objWp.Name = 'Test Wp';
        objWp.LFA__c = objAcc.Id;
        objWp.Country__c = objCountry.id;
        insert objWp;
        
        LFA_Service__c objService = new LFA_Service__c();
        objService.Name='Test1';
        objService.LFA_Work_Plan__c = objWp.Id;
        objService.Service_Type__c = 'Key Services';
        objService.Status__c = 'TGF Agreed';
        insert objService;
        
        LFA_Invoice__c objIn = new LFA_Invoice__c();
        objIn.Name = 'Test1';
        objIn.LFA_Work_Plan__c = objWp.Id;
        objIn.Type__c = 'Services';
        objIn.Status__c ='Cancelled';
        Insert objIn;
        
        LFA_Invoice__c objIn2 = new LFA_Invoice__c();
        objIn2.Name = 'Test2';
        objIn2.LFA_Work_Plan__c = objWp.Id;
        objIn2.Type__c = 'Draft';
        objIn2.Status__c ='Cancelled';
        Insert objIn2;
        
        LFA_Invoice__c objIn1 = new LFA_Invoice__c();
        objIn1.Name = 'Test1';
        objIn1.LFA_Work_Plan__c = objWp.Id;
        objIn1.Type__c = 'Services';
        objIn1.Status__c ='Cancelled';
        insert objIn1;
        
        Service_Invoice__c objSI = new Service_Invoice__c();
        objSI.LFA_Invoice__c = objIn1.Id;
        objSI.LFA_Service__c = objService.Id;
        Insert objSI;
       
        Apexpages.currentpage().getparameters().put('InvoiceId',objIn1.Id);
        
        CancelInvoice objCI = new CancelInvoice();
        CancelInvoice.updateServiceInvoiceStatus(objIn1.Id);
    }
}