/*********************************************************************************
* Test Class: {gippdfext_Test}
*  DateCreated : 04/30/2015
----------------------------------------------------------------------------------
* Purpose/Methods:
* Purpose/Methods:
* - Used for cover all methods of gippdfext.
----------------------------------------------------------------------------------
* History:
* - VERSION  DEVELOPER NAME    DATE            DETAIL FEATURES
     1.0     Jaideep Khare     04/30/2015      INITIAL DEVELOPMENT
*********************************************************************************/

@isTest
public class gippdfext_Test {
   
     Public static testMethod void myUnitTest(){ 
     
     Account objAcc = TestClassHelper.insertAccount();
     
     Country__c country = TestClassHelper.createCountry();
     country.X200DEMA_Exch_Rate_in_EUR__c = 10.0000;
     country.X200DEMA_Exch_Rate_in_USD__c = 10.000;
     insert country;
     
     Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
     objGrant.Country__c = country.Id;
     insert objGrant;     
     test.startTest();
         
     Concept_Note__c objConcept_Note = TestClassHelper.createCN();
     objConcept_Note.CCM_new__c = objAcc.id;
     objConcept_Note.Concept_Note_Type__c = 'Multi-Country';
     insert objConcept_Note;  
         
     Implementation_Period__c ObjImplementationPeriod = TestClassHelper.createIP(objGrant,objAcc);
     ObjImplementationPeriod.Currency_of_Grant_Agreement__c = 'EUR';
     ObjImplementationPeriod.Local_Currency__c = 'COP';
     ObjImplementationPeriod.Other_Currency__c = 'USD';
     ObjImplementationPeriod.Exchange_Rate__c = 50;
     ObjImplementationPeriod.Budget_Status__c = 'Draft';
     ObjImplementationPeriod.Concept_Note__c = objConcept_Note.Id;
     // ObjImplementationPeriod.GIP_Type__c == 'Multi-Country';
     ObjImplementationPeriod.High_level_budget_GAC_1_EUR__c = 10000000;
     ObjImplementationPeriod.High_level_budget_GAC_1_USD__c = 10000000;
     insert ObjImplementationPeriod;       
     
     Performance_Framework__c objPF = TestClassHelper.createPF(ObjImplementationPeriod);     
     insert objPF;
      
     IP_Detail_Information__c ipde = new IP_Detail_Information__c();
     ipde.Budget_Status__c = 'Not yet submitted by PR';
     ipde.Implementation_Period__c = ObjImplementationPeriod.Id;
     ipde.High_level_budget_GAC_1_EUR__c = 1000;
     ipde.Total_Budgeted_Amount__c = 10;     
     insert ipde;
      
     HPC_Framework__c objHPC = TestClassHelper.createHPC();
     objHPC.Grant_Implementation_Period__c = ObjImplementationPeriod.Id;
     objHPC.Total_HPC_Amount_EUR__c = 1000;
     objHPC.Total_HPC_Amount_USD__c = 1000;
     insert objHPC; 
      
     Module__c ObjModule = TestClassHelper.createModule();
     ObjModule.Implementation_Period__c = ObjImplementationPeriod.id;
     ObjModule.Performance_Framework__c  = objPF.id;
     insert ObjModule; 
     
     Grant_Intervention__c ObjGrantIntervention = TestClassHelper.createGrantIntervention(ObjImplementationPeriod);
     ObjGrantIntervention.Module__c = ObjModule.id;
     ObjGrantIntervention.Performance_Framework__c = objPF.Id;
     insert ObjGrantIntervention;
     
     Key_Activity__c tKact=new Key_Activity__c();  
     tKact.Grant_Intervention__c=ObjGrantIntervention.Id;
     tKact.Activity_Description__c = 'Test';   
     insert tKact;
      
     Milestone_Target__c tMtc=new Milestone_Target__c();
     tMtc.Key_Activity__c=tKact.id;
     tMtc.MilestoneTitle__c='TestTitle';
     tMtc.Criteria__c ='testCriteria';
     tMtc.Reporting_Period__c ='testreportingperiod';
     tMtc.Linked_To__c ='testlink';
     insert tMtc;    
         
     Catalog_Cost_Input__c ObjCatalogCostInput = TestClassHelper.createCostInput();
     ObjCatalogCostInput.Cost_Grouping__c = '2. Travel related costs (TRC)';
     ObjCatalogCostInput.Name = 'All';
     ObjCatalogCostInput.Disease_Impact__c = 'Malaria';
     insert ObjCatalogCostInput;
     
     test.stopTest();
      
     Implementer__c objImp = TestClassHelper.createImplementor();
     objImp.Grant_Implementation_Period__c = ObjImplementationPeriod.Id;
     objImp.Performance_Framework__c = objPF.Id;
     insert objImp;
     
     Catalog_Module__c objCM = TestClassHelper.createCatalogModule();
     objCM.CurrencyIsoCode = 'EUR';
     insert objCM; 
         
     Indicator__c objIndicator = new Indicator__c();
     objIndicator.Catalog_Module__c = objCM.id;
     objIndicator.Available_for_PG__c = true;
     objIndicator.Programme_Area__c = 'Malaria';   
     objIndicator.Indicator_Type__c = 'Impact';     
     objIndicator.Catalog_Module__c = objCM.Id;
     objIndicator.Full_Name_En__c = 'test';
     objIndicator.Component__c = objConcept_Note.Component__c;            
     insert objIndicator;
      
     Period__c objPeriod = TestClassHelper.createPeriod();
     objPeriod.Concept_Note__c = objConcept_Note.Id;
     objPeriod.Implementation_Period__c =  ObjImplementationPeriod.Id;
     objPeriod.Type__c = 'Reporting';
     objPeriod.Performance_Framework__c = objPF.Id;
     objPeriod.Start_Date__c = date.today();
     objPeriod.Base_Frequency__c = 'Yearly';
     objPeriod.is_Active__c = true;    
     insert objPeriod;
      
     Grant_Indicator__c objGrant_Indicator1 = new Grant_Indicator__c();
     objGrant_Indicator1.Indicator_Type__c= 'Impact';
     objGrant_Indicator1.Parent_Module__c = objModule.id;
     objGrant_Indicator1.Data_Type__c = 'Number';
     objGrant_Indicator1.Indicator__c = objIndicator.Id;  
     objGrant_Indicator1.Concept_Note__c = objConcept_Note.Id; 
     objGrant_Indicator1.Grant_Intervention__c = ObjGrantIntervention.Id;     
     objGrant_Indicator1.Grant_Implementation_Period__c = ObjImplementationPeriod.Id; 
     objGrant_Indicator1.Performance_Framework__c = objPF.Id; 
     insert objGrant_Indicator1;    
         
     Result__c objResult1 = new Result__c();
     objResult1.Indicator__c = objGrant_Indicator1.Id;
     objResult1.Period__c= objPeriod.Id;
     objResult1.Actual_Amount__c = 100; 
     insert objResult1;    
    
     Goals_Objectives__c objGoals_Objectives = new Goals_Objectives__c();
     objGoals_Objectives.Concept_Note__c     = objConcept_Note.id;
     objGoals_Objectives.Type__c             = 'Goal';
     insert objGoals_Objectives;
         
     Ind_Goal_Jxn__c objInd_Goal_Jxn1   = new Ind_Goal_Jxn__c();
     objInd_Goal_Jxn1.Goal_Objective__c = objGoals_Objectives.id;
     objInd_Goal_Jxn1.Indicator__c      = objGrant_Indicator1.id;
     insert objInd_Goal_Jxn1;
     
     Assumption_Set__c ObjAssumptionSet = new Assumption_Set__c();
     ObjAssumptionSet.Name = 'test';
     ObjAssumptionSet.CurrencyIsoCode = 'EUR';
     ObjAssumptionSet.Implementation_Period__c = ObjImplementationPeriod.id;
     ObjAssumptionSet.Cost_Input__c = ObjCatalogCostInput.Id;
     insert ObjAssumptionSet;     
     
     Budget_Line__c ObjBudgetLine = TestClassHelper.createBudgetLine();
     ObjBudgetLine.Grant_Intervention__c = ObjGrantIntervention.id;
     ObjBudgetLine.Cost_input__c = ObjCatalogCostInput.id;
     ObjBudgetLine.Detailed_Budget_Framework__c = ipde.id;   
     ObjBudgetLine.Cost_Grouping__c = ObjCatalogCostInput.Id;
     ObjBudgetLine.Currency_Used__c = 'Grant Currency';
     ObjBudgetLine.Q1_Amount__c = 1;
     ObjBudgetLine.Q2_Amount__c = 2;
     ObjBudgetLine.Q3_Amount__c = 3;
     ObjBudgetLine.Q4_Amount__c = 4;
     ObjBudgetLine.Q5_Amount__c = 2;
     ObjBudgetLine.Q6_Amount__c = 3;
     ObjBudgetLine.Q7_Amount__c = 2;
     ObjBudgetLine.Q8_Amount__c = 4;
     ObjBudgetLine.Q9_Amount__c = 2;
     ObjBudgetLine.Q10_Amount__c =5;
     ObjBudgetLine.Q11_Amount__c = 1;
     ObjBudgetLine.Q12_Amount__c = 1;
     ObjBudgetLine.Q13_Amount__c = 7;
     ObjBudgetLine.Q14_Amount__c = 1;
     ObjBudgetLine.Q15_Amount__c = 2;
     ObjBudgetLine.Q16_Amount__c = 2;
     // insert ObjBudgetLine ;
     
     Page__c objPage = new Page__c();
     objPage.Name = 'Principal Recipients';
     objPage.Implementation_Period__c = ObjImplementationPeriod.id;
     insert objPage;
     
     PSM__c OBjPSM = new PSM__c();
     OBjPSM.Name = 'Test111';
     OBjPSM.CurrencyIsoCode = 'EUR';
     insert OBjPSM;
     
     Apexpages.currentpage().getparameters().put('id',ObjImplementationPeriod.Id);
     ApexPages.StandardController sc = new ApexPages.StandardController(ObjImplementationPeriod);
     gippdfext  AssCtrl= new gippdfext(sc);
     gippdfext  AssCtr2 = new gippdfext(); 
     AssCtrl.year1 = '2015';
     AssCtrl.year2 = '2015';
     AssCtrl.year3 = '2015';
     AssCtrl.year4 = '2015';    
     AssCtrl.sectionb();
     AssCtrl.sectionc(); 
     AssCtrl.fetchimpactindicators();     
     gippdfext.BLModule bm1 = new gippdfext.BLModule('a','b',true,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,1.10,1.11,1.12,1.13,1.14,1.15,1.16,1.17,1.18,1.19,1.20);
     gippdfext.BLModule bm2 = new gippdfext.BLModule();
     gippdfext.BLCost   bc1 = new gippdfext.BLCost(); 
     bc1.CostY1 = 1.1;    
     bc1.CostY2 = 1.1;
     bc1.CostY3 = 1.1;
     bc1.CostY4 = 1.1;    
     bc1.CostQ1 = 1.1;
     bc1.CostQ2 = 1.1;
     bc1.CostQ3 = 1.1;   
     bc1.CostQ4 = 1.1;
     bc1.CostQ5 = 1.1;
     bc1.CostQ6 = 1.1;    
     bc1.CostQ7 = 1.1;
     bc1.CostQ8 = 1.1;   
     bc1.CostQ9 = 1.1;
     bc1.CostQ10 = 1.1;
     bc1.CostQ11 = 1.1;
     bc1.CostQ12 = 1.1;
     bc1.CostQ13 = 1.1;   
     bc1.CostQ14 = 1.1;
     bc1.CostQ15 = 1.1;
     bc1.CostQ16 = 1.1;     
     bc1.TotalCost = 1.1;
     bc1.CostGrouping = 'abc';    
     gippdfext.BLPayee  bp1 = new gippdfext.BLPayee();
     bp1.PayeeCostY1 = 1.1;   
     bp1.PayeeCostY2 = 1.1;
     bp1.PayeeCostY3 = 1.1;
     bp1.PayeeCostY4 = 1.1; 
     bp1.PayeeCostQ1 = 1.1;
     bp1.PayeeCostQ1 = 1.1;
     bp1.PayeeCostQ2 = 1.1;
     bp1.PayeeCostQ3 = 1.1;
     bp1.PayeeCostQ4 = 1.1;
     bp1.PayeeCostQ5 = 1.1;
     bp1.PayeeCostQ6 = 1.1;
     bp1.PayeeCostQ7 = 1.1;
     bp1.PayeeCostQ8 = 1.1;
     bp1.PayeeCostQ9 = 1.1;
     bp1.PayeeCostQ10 = 1.1;
     bp1.PayeeCostQ11 = 1.1;
     bp1.PayeeCostQ12 = 1.1;
     bp1.PayeeCostQ13 = 1.1;
     bp1.PayeeCostQ14 = 1.1;
     bp1.PayeeCostQ15 = 1.1;
     bp1.PayeeCostQ16 = 1.1;  
     bp1.TotalPayeeCost = 1.1;
     bp1.PayeeName   = 'abc';  
         
         
     // bm1.BLModule('a','b',true,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,1.10,1.11,1.12,1.13,1.14,1.15,1.16,1.17,1.18,1.19,1.20);    
     }  
    
     Public static testMethod void myUnitTest1(){ 
     
     Account objAcc = TestClassHelper.insertAccount();
     
     Country__c country = TestClassHelper.createCountry();
     country.X200DEMA_Exch_Rate_in_EUR__c = 10.0000;
     country.X200DEMA_Exch_Rate_in_USD__c = 10.000;
     insert country;
     
     Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
     objGrant.Country__c = country.Id;
     insert objGrant;
     
     test.startTest();
         
     Concept_Note__c objConcept_Note = TestClassHelper.createCN();
     objConcept_Note.CCM_new__c = objAcc.id;
     objConcept_Note.Concept_Note_Type__c = 'Multi-Country';
     insert objConcept_Note;  
         
     Implementation_Period__c ObjImplementationPeriod = TestClassHelper.createIP(objGrant,objAcc);
     ObjImplementationPeriod.Currency_of_Grant_Agreement__c = 'EUR';
     ObjImplementationPeriod.Local_Currency__c = 'COP';
     ObjImplementationPeriod.Other_Currency__c = 'USD';
     ObjImplementationPeriod.Exchange_Rate__c = 50;
     ObjImplementationPeriod.Budget_Status__c = 'Draft';
     ObjImplementationPeriod.Concept_Note__c = objConcept_Note.Id;
     // ObjImplementationPeriod.GIP_Type__c == 'Multi-Country';
     ObjImplementationPeriod.High_level_budget_GAC_1_EUR__c = 10000000;
     ObjImplementationPeriod.High_level_budget_GAC_1_USD__c = 10000000;
     insert ObjImplementationPeriod;        
     
     Performance_Framework__c objPF = TestClassHelper.createPF(ObjImplementationPeriod);     
     insert objPF;
      
     IP_Detail_Information__c ipde = new IP_Detail_Information__c();
     ipde.Budget_Status__c = 'Not yet submitted by PR';
     ipde.Implementation_Period__c = ObjImplementationPeriod.Id;
     ipde.High_level_budget_GAC_1_EUR__c = 1000;
     ipde.Total_Budgeted_Amount__c = 10;     
     insert ipde;
      
     HPC_Framework__c objHPC = TestClassHelper.createHPC();
     objHPC.Grant_Implementation_Period__c = ObjImplementationPeriod.Id;
     objHPC.Total_HPC_Amount_EUR__c = 1000;
     objHPC.Total_HPC_Amount_USD__c = 1000;
     insert objHPC; 
      
     Module__c ObjModule = TestClassHelper.createModule();
     ObjModule.Implementation_Period__c = ObjImplementationPeriod.id;
     ObjModule.Performance_Framework__c  = objPF.id;
     insert ObjModule; 
     
     Grant_Intervention__c ObjGrantIntervention = TestClassHelper.createGrantIntervention(ObjImplementationPeriod);
     ObjGrantIntervention.Module__c = ObjModule.id;
     ObjGrantIntervention.Performance_Framework__c = objPF.Id;
     insert ObjGrantIntervention;
     
     Key_Activity__c tKact=new Key_Activity__c();  
     tKact.Grant_Intervention__c=ObjGrantIntervention.Id;
     tKact.Activity_Description__c = 'Test';   
     insert tKact;
      
     Milestone_Target__c tMtc=new Milestone_Target__c();
     tMtc.Key_Activity__c=tKact.id;
     tMtc.MilestoneTitle__c='TestTitle';
     tMtc.Criteria__c ='testCriteria';
     tMtc.Reporting_Period__c ='testreportingperiod';
     tMtc.Linked_To__c ='testlink';
     insert tMtc;    
         
     Catalog_Cost_Input__c ObjCatalogCostInput = TestClassHelper.createCostInput();
     ObjCatalogCostInput.Cost_Grouping__c = '2. Travel related costs (TRC)';
     ObjCatalogCostInput.Name = 'All';
     ObjCatalogCostInput.Disease_Impact__c = 'Malaria';
     insert ObjCatalogCostInput;
     
     test.stopTest();
      
     Implementer__c objImp = TestClassHelper.createImplementor();
     objImp.Grant_Implementation_Period__c = ObjImplementationPeriod.Id;
     objImp.Performance_Framework__c = objPF.Id;
     insert objImp;
     
     Catalog_Module__c objCM = TestClassHelper.createCatalogModule();
     objCM.CurrencyIsoCode = 'EUR';
     insert objCM; 
         
     Indicator__c objIndicator = new Indicator__c();
     objIndicator.Catalog_Module__c = objCM.id;
     objIndicator.Available_for_PG__c = true;
     objIndicator.Programme_Area__c = 'Malaria';
     objIndicator.Indicator_Type__c = 'Coverage/Output';   
     objIndicator.Catalog_Module__c = objCM.Id;
     objIndicator.Full_Name_En__c = 'test';
     objIndicator.Component__c = objConcept_Note.Component__c;     
     objIndicator.Indicator_Type__c = 'Coverage/Output';
     objIndicator.Reporting_Frequency__c = 'Based on Reporting Frequency';    
     insert objIndicator;
      
     Period__c objPeriod = TestClassHelper.createPeriod();
     objPeriod.Concept_Note__c = objConcept_Note.Id;
     objPeriod.Implementation_Period__c =  ObjImplementationPeriod.Id;
     objPeriod.Type__c = 'Reporting';
     objPeriod.Performance_Framework__c = objPF.Id;
     objPeriod.Start_Date__c = date.today();
     objPeriod.Base_Frequency__c = 'Yearly';
     objPeriod.is_Active__c = true;    
     insert objPeriod;
      
     Period__c objPeriod1 = TestClassHelper.createPeriod();
     objPeriod1.Concept_Note__c = objConcept_Note.Id;
     objPeriod1.Implementation_Period__c =  ObjImplementationPeriod.Id;
     objPeriod1.Type__c = 'Reporting';
     objPeriod1.Performance_Framework__c = objPF.Id;
     objPeriod1.Start_Date__c = date.today();
     objPeriod1.Base_Frequency__c = 'Yearly';
     objPeriod1.is_Active__c = true;    
     insert objPeriod1;   
      
     Grant_Indicator__c objGrant_Indicator1 = new Grant_Indicator__c();
     objGrant_Indicator1.Parent_Module__c = objModule.id;
     objGrant_Indicator1.Data_Type__c = 'Number';
     objGrant_Indicator1.Indicator__c = objIndicator.Id;  
     objGrant_Indicator1.Concept_Note__c = objConcept_Note.Id; 
     objGrant_Indicator1.Grant_Intervention__c = ObjGrantIntervention.Id;  
     objGrant_Indicator1.Indicator_Type__c = 'Coverage/Output';
     objGrant_Indicator1.Grant_Implementation_Period__c = ObjImplementationPeriod.Id; 
     objGrant_Indicator1.Performance_Framework__c = objPF.Id; 
     objGrant_Indicator1.Reporting_Frequency__c = 'Based on Reporting Frequency';
     insert objGrant_Indicator1;    
         
     Result__c objResult1 = new Result__c();
     objResult1.Indicator__c = objGrant_Indicator1.Id;
     objResult1.Period__c= objPeriod.Id;
     objResult1.Actual_Amount__c = 100;        
     insert objResult1;
        
     Goals_Objectives__c objGoals_Objectives = new Goals_Objectives__c();
     objGoals_Objectives.Concept_Note__c     = objConcept_Note.id;
     objGoals_Objectives.Type__c             = 'Goal';
     insert objGoals_Objectives;
         
     Ind_Goal_Jxn__c objInd_Goal_Jxn1   = new Ind_Goal_Jxn__c();
     objInd_Goal_Jxn1.Goal_Objective__c = objGoals_Objectives.id;
     objInd_Goal_Jxn1.Indicator__c      = objGrant_Indicator1.id;
     insert objInd_Goal_Jxn1;
     
     Assumption_Set__c ObjAssumptionSet = new Assumption_Set__c();
     ObjAssumptionSet.Name = 'test';
     ObjAssumptionSet.CurrencyIsoCode = 'EUR';
     ObjAssumptionSet.Implementation_Period__c = ObjImplementationPeriod.id;
     ObjAssumptionSet.Cost_Input__c = ObjCatalogCostInput.Id;
     insert ObjAssumptionSet;
         
     Page__c objPage = new Page__c();
     objPage.Name = 'Principal Recipients';
     objPage.Implementation_Period__c = ObjImplementationPeriod.id;
     insert objPage;
     
     PSM__c OBjPSM = new PSM__c();
     OBjPSM.Name = 'Test111';
     OBjPSM.CurrencyIsoCode = 'EUR';
     insert OBjPSM;
     
     Apexpages.currentpage().getparameters().put('id',ObjImplementationPeriod.Id);
     ApexPages.StandardController sc = new ApexPages.StandardController(ObjImplementationPeriod);
     gippdfext  AssCtrl= new gippdfext(sc);
     gippdfext  AssCtr2 = new gippdfext();
     objPeriod1.Start_Date__c = date.today() -1;       
     upsert objPeriod1; 
     gippdfext  AssCtr3= new gippdfext(sc);
       
    }
     Public static testMethod void myUnitTest2(){ 
     
     Account objAcc = TestClassHelper.insertAccount();
     
     Country__c country = TestClassHelper.createCountry();
     country.X200DEMA_Exch_Rate_in_EUR__c = 10.0000;
     country.X200DEMA_Exch_Rate_in_USD__c = 10.000;
     insert country;
     
     Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
     objGrant.Country__c = country.Id;
     insert objGrant;
     
     test.startTest();
         
     Concept_Note__c objConcept_Note = TestClassHelper.createCN();
     objConcept_Note.CCM_new__c = objAcc.id;
     objConcept_Note.Concept_Note_Type__c = 'Multi-Country';
     insert objConcept_Note;  
         
     Implementation_Period__c ObjImplementationPeriod = TestClassHelper.createIP(objGrant,objAcc);
     ObjImplementationPeriod.Currency_of_Grant_Agreement__c = 'EUR';
     ObjImplementationPeriod.Local_Currency__c = 'COP';
     ObjImplementationPeriod.Other_Currency__c = 'USD';
     ObjImplementationPeriod.Exchange_Rate__c = 50;
     ObjImplementationPeriod.Budget_Status__c = 'Draft';
     ObjImplementationPeriod.Concept_Note__c = objConcept_Note.Id;
     // ObjImplementationPeriod.GIP_Type__c == 'Multi-Country';
     ObjImplementationPeriod.High_level_budget_GAC_1_EUR__c = 10000000;
     ObjImplementationPeriod.High_level_budget_GAC_1_USD__c = 10000000;
     insert ObjImplementationPeriod;       
     
     Performance_Framework__c objPF = TestClassHelper.createPF(ObjImplementationPeriod);     
     insert objPF;
      
     IP_Detail_Information__c ipde = new IP_Detail_Information__c();
     ipde.Budget_Status__c = 'Not yet submitted by PR';
     ipde.Implementation_Period__c = ObjImplementationPeriod.Id;
     ipde.High_level_budget_GAC_1_EUR__c = 1000;
     ipde.Total_Budgeted_Amount__c = 10;
     insert ipde;
      
     HPC_Framework__c objHPC = TestClassHelper.createHPC();
     objHPC.Grant_Implementation_Period__c = ObjImplementationPeriod.Id;
     objHPC.Total_HPC_Amount_EUR__c = 1000;
     objHPC.Total_HPC_Amount_USD__c = 1000;
     insert objHPC; 
      
     Module__c ObjModule = TestClassHelper.createModule();
     ObjModule.Implementation_Period__c = ObjImplementationPeriod.id;
     ObjModule.Performance_Framework__c  = objPF.id;
     insert ObjModule; 
     
     Grant_Intervention__c ObjGrantIntervention = TestClassHelper.createGrantIntervention(ObjImplementationPeriod);
     ObjGrantIntervention.Module__c = ObjModule.id;
     ObjGrantIntervention.Performance_Framework__c = objPF.Id;
     ObjGrantIntervention.Custom_Intervention_Name__c = 'abc';
     insert ObjGrantIntervention;
     
     Key_Activity__c tKact=new Key_Activity__c();
     tKact.Grant_Intervention__c=ObjGrantIntervention.Id;
     tKact.Activity_Description__c = 'Test';  
     insert tKact;
      
     Milestone_Target__c tMtc=new Milestone_Target__c();
     tMtc.Key_Activity__c= tKact.id;
     tMtc.MilestoneTitle__c='TestTitle';
     tMtc.Criteria__c ='testCriteria';
     tMtc.Reporting_Period__c ='testreportingperiod';
     tMtc.Linked_To__c ='testlink';
     insert tMtc;           
         
     Catalog_Cost_Input__c ObjCatalogCostInput = TestClassHelper.createCostInput();
     ObjCatalogCostInput.Cost_Grouping__c = '2. Travel related costs (TRC)';
     ObjCatalogCostInput.Name = 'All';
     ObjCatalogCostInput.Disease_Impact__c = 'Malaria';
     insert ObjCatalogCostInput;
     
     test.stopTest();
      
     Implementer__c objImp = TestClassHelper.createImplementor();
     objImp.Grant_Implementation_Period__c = ObjImplementationPeriod.Id;
     objImp.Performance_Framework__c = objPF.Id;
     insert objImp;
     
     Catalog_Module__c objCM = TestClassHelper.createCatalogModule();
     objCM.CurrencyIsoCode = 'EUR';
     insert objCM; 
         
     Indicator__c objIndicator = new Indicator__c();
     objIndicator.Catalog_Module__c = objCM.id;
     objIndicator.Available_for_PG__c = true;
     objIndicator.Programme_Area__c = 'Malaria';
     objIndicator.Indicator_Type__c = 'Coverage/Output';
     objIndicator.Catalog_Module__c = objCM.Id;
     objIndicator.Full_Name_En__c = 'test';
     objIndicator.Component__c = objConcept_Note.Component__c;     
     objIndicator.Indicator_Type__c = 'Coverage/Output';
     objIndicator.Reporting_Frequency__c = 'Based on Reporting Frequency';    
     insert objIndicator;
      
     Period__c objPeriod = TestClassHelper.createPeriod();
     objPeriod.Concept_Note__c = objConcept_Note.Id;
     objPeriod.Implementation_Period__c =  ObjImplementationPeriod.Id;
     objPeriod.Type__c = 'Reporting';
     objPeriod.Performance_Framework__c = objPF.Id;
     objPeriod.Start_Date__c = date.today();
     objPeriod.Base_Frequency__c = 'Yearly';
     objPeriod.is_Active__c = true;    
     insert objPeriod;
      
     Period__c objPeriod1 = TestClassHelper.createPeriod();
     objPeriod1.Concept_Note__c = objConcept_Note.Id;
     objPeriod1.Implementation_Period__c =  ObjImplementationPeriod.Id;
     objPeriod1.Type__c = 'Reporting';
     objPeriod1.Performance_Framework__c = objPF.Id;
     objPeriod1.Start_Date__c = date.today();
     objPeriod1.Base_Frequency__c = 'Yearly';
     objPeriod1.is_Active__c = true; 
     objPeriod1.Period_Number__c = 1;
     insert objPeriod1; 
        
     Milestone_RP_Junction__c mrp1 = new Milestone_RP_Junction__c(); 
     mrp1.Reporting_Period__c = objPeriod1.Id; 
     mrp1.Milestone_Target__c = tMtc.Id;
     insert mrp1;    
      
     Grant_Indicator__c objGrant_Indicator1 = new Grant_Indicator__c();
     objGrant_Indicator1.Parent_Module__c = objModule.id;
     objGrant_Indicator1.Data_Type__c = 'Number';
     objGrant_Indicator1.Indicator__c = objIndicator.Id;  
     objGrant_Indicator1.Concept_Note__c = objConcept_Note.Id; 
     objGrant_Indicator1.Grant_Intervention__c = ObjGrantIntervention.Id;  
     objGrant_Indicator1.Indicator_Type__c = 'Coverage/Output';
     objGrant_Indicator1.Grant_Implementation_Period__c = ObjImplementationPeriod.Id; 
     objGrant_Indicator1.Performance_Framework__c = objPF.Id; 
     objGrant_Indicator1.Reporting_Frequency__c = '12 Months';        
     insert objGrant_Indicator1;    
         
     Result__c objResult1 = new Result__c();
     objResult1.Indicator__c = objGrant_Indicator1.Id;
     objResult1.Period__c= objPeriod.Id;
     objResult1.Actual_Amount__c = 100;        
     insert objResult1;
    
    
     Goals_Objectives__c objGoals_Objectives = new Goals_Objectives__c();
     objGoals_Objectives.Concept_Note__c     = objConcept_Note.id;
     objGoals_Objectives.Type__c             = 'Goal';
     insert objGoals_Objectives;
         
     Ind_Goal_Jxn__c objInd_Goal_Jxn1   = new Ind_Goal_Jxn__c();
     objInd_Goal_Jxn1.Goal_Objective__c = objGoals_Objectives.id;
     objInd_Goal_Jxn1.Indicator__c      = objGrant_Indicator1.id;
     insert objInd_Goal_Jxn1;
   
     Product__c pc1 = new Product__c();
     pc1.Health_Products_and_Costs__c = objHPC.Id;
     pc1.Grant_Intervention__c = ObjGrantIntervention.Id; 
     insert pc1;    
      
     Budget_Line__c ObjBudgetLine = TestClassHelper.createBudgetLine();
     ObjBudgetLine.Grant_Intervention__c = ObjGrantIntervention.Id;
     ObjBudgetLine.Cost_input__c = ObjCatalogCostInput.id;
     ObjBudgetLine.Detailed_Budget_Framework__c = ipde.id;
   //  ObjBudgetLine.Assumption_Set__c = ObjAssumptionSet.Id;
     ObjBudgetLine.Cost_Grouping__c = ObjCatalogCostInput.Id;
     ObjBudgetLine.Currency_Used__c = 'Grant Currency';
     ObjBudgetLine.Q1_Amount__c = 1;
     ObjBudgetLine.Q2_Amount__c = 2;
     ObjBudgetLine.Q3_Amount__c = 3;
     ObjBudgetLine.Q4_Amount__c = 4;
     ObjBudgetLine.Q5_Amount__c = 2;
     ObjBudgetLine.Q6_Amount__c = 3;
     ObjBudgetLine.Q7_Amount__c = 2;
     ObjBudgetLine.Q8_Amount__c = 4;
     ObjBudgetLine.Q9_Amount__c = 2;
     ObjBudgetLine.Q10_Amount__c =5;
     ObjBudgetLine.Q11_Amount__c = 1;
     ObjBudgetLine.Q12_Amount__c = 1;
     ObjBudgetLine.Q13_Amount__c = 7;
     ObjBudgetLine.Q14_Amount__c = 1;
     ObjBudgetLine.Q15_Amount__c = 2;
     ObjBudgetLine.Q16_Amount__c = 2;
     ObjBudgetLine.Payee__c   = objImp.Id;
     insert ObjBudgetLine ;
     Page__c objPage = new Page__c();
     objPage.Name = 'Principal Recipients';
     objPage.Implementation_Period__c = ObjImplementationPeriod.id;
     insert objPage;
     
     PSM__c OBjPSM = new PSM__c();
     OBjPSM.Name = 'Test111';
     OBjPSM.CurrencyIsoCode = 'EUR';
     insert OBjPSM;
     
     Apexpages.currentpage().getparameters().put('id',ObjImplementationPeriod.Id);
     ApexPages.StandardController sc = new ApexPages.StandardController(ObjImplementationPeriod);
     gippdfext  AssCtrl= new gippdfext(sc);
     gippdfext  AssCtr2 = new gippdfext();
     objPeriod1.Start_Date__c = date.today() -1;       
     upsert objPeriod1; 
     gippdfext  AssCtr3= new gippdfext(sc);
       
    }
}