/*
    Created By: RK
    Date Created : 13th-Mar-2015
    Purpose : Controller for CovOutputIndicatorsTable vf
    Date Updated :
*/
public class CovOutputIndicatorsTableController{
    String pfId;
    public List<Module__c> moduleList{get;set;}
    public List<Grant_Indicator__c> IndicatorList{get;set;}
    public List<SelectOption>ModuleOptionsList{get;set;}
    public string selectedMod{get;set;}
    public List<Result__c> tempres{get;set;}
    public List<Grant_Disaggregated__c> tempgd{get;set;}    
    public void fillModuleList(){
        fetchData();
        List<SelectOption> optns = new List<Selectoption>();
        optns.add(new selectOption('none','None'));
        for(Module__c obj : moduleList){           
           ModuleOptionsList.add(new selectOption(obj.Id, obj.Name));
        }
    }
    
    public CovOutputIndicatorsTableController(ApexPages.StandardSetController controller) {
        tempres = new List<Result__c>{new Result__c()};
        tempgd = new List<Grant_Disaggregated__c>{new Grant_Disaggregated__c()};        
        ModuleOptionsList = new List<SelectOption>();
        ModuleOptionsList.add(new selectOption('none','All'));
        fetchData();
        pfId=Apexpages.currentpage().getparameters().get('id');    
        selectedMod='none';
        fillModuleList();            
    }    

    public void fetchData()
    {
       moduleList = new List<Module__c>();
       if(selectedMod !='none')
       {
           moduleList = [Select Id,Name from Module__c where Performance_Framework__c =:pfId and Id=:selectedMod];
           IndicatorList =[Select Indicator_Full_Name__c,Standard_or_Custom__c,Grant_Status__c,Reporting_Frequency__c,Data_Type__c,
                           Is_Disaggregated__c,Component_Multi__c,Country__c,PR_Responsible_for_Reporting__c,Parent_Module__r.Name,
                           (Select Target__c,Period_Short_Date__c,Actual__c,isNew__c,Achievement__c,Data_Source__c,Verification_Method__c,
                           Verified_Result__c,is_Updated__c,Reporting_Category__c,Target_Ratio_per_100k__c,Actual_Ratio_per_100k__c
                            from Grant_Indicator__c.Results__r),
                           (Select Disaggregation_Value__c,Disaggregation_Category__c,Disaggregated_Baseline_Sources__c,
                           Disaggregated_Baseline_Year__c,Disaggregated_Baseline_Value__c,Component__c
                            from Grant_Indicator__c.Grant_Indicator__r) from Grant_Indicator__c where Indicator_Type__c='Coverage/Output' and Performance_Framework__c =:pfId and Parent_Module__c=:selectedMod];
       }
       else
       {
           moduleList = [Select Id,Name from Module__c where Performance_Framework__c =:pfId];
           IndicatorList =[Select Indicator_Full_Name__c,Standard_or_Custom__c,Grant_Status__c,Reporting_Frequency__c,Data_Type__c,
                            Is_Disaggregated__c,Component_Multi__c,Country__c,PR_Responsible_for_Reporting__c,Parent_Module__r.Name,
                           (Select Target__c,Period_Short_Date__c,Actual__c,isNew__c,Achievement__c,Data_Source__c,Verification_Method__c,
                           Verified_Result__c,is_Updated__c,Reporting_Category__c,Target_Ratio_per_100k__c,Actual_Ratio_per_100k__c
                            from Grant_Indicator__c.Results__r),
                           (Select Disaggregation_Value__c,Disaggregation_Category__c,Disaggregated_Baseline_Sources__c,
                           Disaggregated_Baseline_Year__c,Disaggregated_Baseline_Value__c,Component__c
                             from Grant_Indicator__c.Grant_Indicator__r) from Grant_Indicator__c where Indicator_Type__c='Coverage/Output' and Performance_Framework__c =:pfId]; 
       }
    }
}