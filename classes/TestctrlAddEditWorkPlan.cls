/*********************************************************************************
* Test Class: {TestctrlAddEditWorkPlan}
*  DateCreated : 07/16/2013
----------------------------------------------------------------------------------
* Purpose/Methods:
* - Used for cover all methods of ctrlAddEditWorkPlan.
----------------------------------------------------------------------------------
* History:
* - VERSION  DEVELOPER NAME    DATE            DETAIL FEATURES
     1.0                      07/16/2013      INITIAL DEVELOPMENT
*********************************************************************************/
@isTest
Public Class TestctrlAddEditWorkPlan{
   Public static testMethod Void TestctrlAddEditWorkPlan(){
        Account objAcc = new Account();
        objAcc.Name = 'Test Acc';
        objAcc.Short_Name__c= 'Test Acc';
        insert objAcc;
        
        Country__c objCountry = new Country__c();
        objCountry.Name = 'Test Country';
        objCountry.Region_Name__c = 'Test Region';
        objCountry.Used_for_LFA_Services__c = TRUE;
        insert objCountry;
        
        //For different workplan for Analyse Search
        List<LFA_Work_Plan__c> lstWorkPlans = new List<LFA_Work_Plan__c>();
        LFA_Work_Plan__c objWp2 = new LFA_Work_Plan__c();
        objWp2.Name = 'Test Wp';
        objWp2.LFA__c = objAcc.Id;
        objWp2.Country__c = objCountry.id;
        objWP2.Include_In_Library__c = TRUE;
        objWP2.Year__c = '2014';
        //insert objWp2;
        lstWorkPlans.add(objWp2);
        
        LFA_Work_Plan__c objWp = new LFA_Work_Plan__c();
        objWp.Name = 'Test Wp';
        objWp.LFA__c = objAcc.Id;
        objWp.Country__c = objCountry.id;
        objWP.Include_In_Library__c = TRUE;
        objWP.Year__c = '2014';
        //insert objWp;
        lstWorkPlans.add(objWp);
        
        LFA_Work_Plan__c objWp1 = new LFA_Work_Plan__c();
        objWp1.Name = 'Test Wp';
        objWp1.LFA__c = objAcc.Id;
        objWp1.Country__c = objCountry.id;
        objWp1.End_Date__c = system.today();
        //insert objWp1;
        lstWorkPlans.add(objWp1);
        insert lstWorkPlans;
                        
        List<LFA_Service__c> lstSerivce = new List<LFA_Service__c>();
        
        LFA_Service__c objService = new LFA_Service__c();
        objService.Name = 'PUDR';
        objService.LFA_Work_Plan__c = objWp.id;
        objService.Service_Type__c = 'Key Services';
        objService.Service_Sub_Type__c = 'PUDR';
        objService.Exclude_from_Library__c=FALSE;        
        lstSerivce.add(objService);
        
        LFA_Service__c objService1 = new LFA_Service__c();
        objService1.Name = 'PUDR';
        objService1.LFA_Work_Plan__c = objWp.id;
        objService1.Service_Type__c = 'Key Services';
        objService1.Service_Sub_Type__c = 'PUDR';
        lstSerivce.add(objService1);
        
        LFA_Service__c objService2 = new LFA_Service__c();
        objService2.Name = 'PUDR';
        objService2.LFA_Work_Plan__c = objWp.id;
        objService2.Service_Type__c = 'Other LFA Services';
        objService2.Service_Sub_Type__c = 'PUDR';
        lstSerivce.add(objService2);
        
        LFA_Service__c objService3 = new LFA_Service__c();
        objService3.Name = 'PUDR';
        objService3.LFA_Work_Plan__c = objWp.id;
        objService3.Service_Type__c = 'Key Services';
        objService3.Service_Sub_Type__c = 'PUDR';
        lstSerivce.add(objService3);
        
        LFA_Service__c objService4 = new LFA_Service__c();
        objService4.Name = 'PUDR';
        objService4.LFA_Work_Plan__c = objWp1.id;
        objService4.Service_Type__c = 'Key Services';
        objService4.Service_Sub_Type__c = 'PUDR';
        lstSerivce.add(objService4);
        
        LFA_Service__c objService5 = new LFA_Service__c();
        objService5.Name = 'PUDR';
        objService5.LFA_Work_Plan__c = objWp1.id;
        objService5.Service_Type__c = 'Key Services';
        objService5.Service_Sub_Type__c = 'PUDR';
        lstSerivce.add(objService5);
        
        LFA_Service__c objService6 = new LFA_Service__c();
        objService6.Name = 'PUDR';
        objService6.LFA_Work_Plan__c = objWp2.id;
        objService6.Service_Type__c = 'Key Services';
        objService6.Service_Sub_Type__c = 'PUDR';
        objService6.Exclude_from_Library__c=FALSE;        
        lstSerivce.add(objService6);
        
        
        insert lstSerivce;
        
        List<LFA_Role__c> lstRoles =  new List<LFA_Role__c>();
        LFA_Role__c objRole = new LFA_Role__c();
        objRole.Name = 'Test Role1';
        //insert objRole;
        lstRoles.add(objRole);
        
        Contact objCon = new Contact();
        objCon.LastName = 'Test LName';
        objCon.AccountId = objAcc.Id;
        insert objCon;
        List<LFA_Resource__c> lstResources= new List<LFA_Resource__c>();
       
        LFA_Resource__c objResource = new LFA_Resource__c();
        //objResource.Name = 'Test Resource';
        objResource.Contact__c = objCon.Id;
        objResource.LFA_Service__c = objService.id;
        objResource.Rate__c = 1;
        objResource.LFA_Role__c = objRole.Id;
        objResource.LOE__c = 1;
        objResource.CT_Planned_Cost__c = 1;
        objResource.CT_Planned_LOE__c = 1;
        objResource.TGF_Proposed_Cost__c = 1;
        objResource.TGF_Proposed_LOE__c = 1;
        //sert objResource;
        lstResources.add(objResource);
        
        List<Rate__c> lstRates = new List<Rate__c>();
        Rate__c objRate = new Rate__c();
        objRate.LFA_Role__c = objRole.Id;
        objRate.Contact__c = objCon.Id;
        objRate.Country__c = objCountry.Id;
        objRate.Rate__c = 1;
        objRate.Active__c = true;
        objRate.Gf_approved__c = 'Approved';
        //insert objRate;
        lstRates.add(objRate);
       
        
        Grant__c objGrant = new Grant__c();
        objGrant.Name = 'Test Grant';
        objGrant.Country__c = objCountry.id;
        objGrant.Principal_Recipient__c = objAcc.id;
        objGrant.Financial_and_Fiduciary_Risks__c = 'Low';
        objGrant.Health_Services_and_Products_Risks__c = 'High';
        objGrant.Programmatic_and_Performance_Risks__c = 'Medium';
        insert objGrant;
        
        
        LFA_Resource__c objResource1 = new LFA_Resource__c();
        //objResource1.Name = 'Test Resource1';
        objResource1.LFA_Service__c = objService1.id;
        objResource1.Rate__c = 2;
        objResource1.LOE__c = 2;
        objResource1.CT_Planned_Cost__c = 2;
        objResource1.CT_Planned_LOE__c = 2;
        objResource1.TGF_Proposed_Cost__c = 2;
        objResource1.TGF_Proposed_LOE__c = 2;
        //insert objResource1;
        lstResources.add(objResource1);
        
        
        
        LFA_Role__c objRole1 = new LFA_Role__c();
        objRole1.Name = 'Test Role';
        //insert objRole1;
        lstRoles.add(objRole1);
        
        LFA_Resource__c objResource2 = new LFA_Resource__c();
        //objResource2.Name = 'Test Resource1';
        objResource2.LFA_Service__c = objService2.id;
        objResource2.LFA_Role__c = objRole.id;
        objResource2.Rate__c = 2;
        objResource2.LOE__c = 2;
        objResource2.CT_Planned_Cost__c = 2;
        objResource2.CT_Planned_LOE__c = 2;
        objResource2.TGF_Proposed_Cost__c = 2;
        objResource2.TGF_Proposed_LOE__c = 2;
        //insert objResource2;
        lstResources.add(objResource2);
        Test.startTest();
        Contact objCon1 = new Contact();
        objCon1.LastName = 'Test LName';
        objCon1.AccountId = objAcc.Id;
        insert objCon1;
        
        
       
        Rate__c objRate1 = new Rate__c();
        objRate1.LFA_Role__c = objRole1.Id;
        objRate1.Contact__c = objCon1.Id;
        objRate1.Country__c = objCountry.Id;
        objRate1.Rate__c = 1;
        objRate1.Active__c = true;
        objRate1.Gf_approved__c = 'Approved';
        //insert objRate1;
        lstRates.add(objRate1);
        
        Grant__c objGrant1 = new Grant__c();
        objGrant1.Name = 'Test Grant';
        objGrant1.Country__c = objCountry.id;
        objGrant1.Principal_Recipient__c = objAcc.id;
        objGrant1.Financial_and_Fiduciary_Risks__c = 'Low';
        objGrant1.Health_Services_and_Products_Risks__c = 'High';
        objGrant1.Programmatic_and_Performance_Risks__c = 'Medium';
        insert objGrant1;
        
        
        
        LFA_Role__c objRole2 = new LFA_Role__c();
        objRole2.Name = 'Test Role1';
        //insert objRole2;
        lstRoles.add(objRole2);
        insert lstRoles;
       
        LFA_Resource__c objResource3 = new LFA_Resource__c();
        //objResource.Name = 'Test Resource';
        objResource3.LFA_Service__c = objService3.id;
        objResource3.Rate__c = 1;
        objResource3.LFA_Role__c = objRole2.Id;
        objResource3.LOE__c = 1;
        objResource3.CT_Planned_Cost__c = 1;
        objResource3.CT_Planned_LOE__c = 1;
        objResource3.TGF_Proposed_Cost__c = 1;
        objResource3.TGF_Proposed_LOE__c = 1;
        //insert objResource3;
        lstResources.add(objResource3);
        
        Rate__c objRate2 = new Rate__c();
        objRate2.LFA_Role__c = objRole2.Id;
        objRate2.Contact__c = objCon.Id;
        objRate2.Country__c = objCountry.Id;
        objRate2.Rate__c = 1;
        objRate2.Active__c = true;
        objRate2.Gf_approved__c = 'Approved';
        //insert objRate2;
        lstRates.add(objRate2);
        insert lstRates;
        
        
        LFA_Resource__c objResource4 = new LFA_Resource__c();
        //objResource.Name = 'Test Resource';
        objResource4.LFA_Service__c = objService4.id;
        objResource4.Rate__c = 1;
        objResource4.LFA_Role__c = objRole1.Id;
        objResource4.LOE__c = 1;
        objResource4.CT_Planned_Cost__c = 1;
        objResource4.CT_Planned_LOE__c = 1;
        objResource4.TGF_Proposed_Cost__c = 1;
        objResource4.TGF_Proposed_LOE__c = 1;
        //insert objResource4;
        lstResources.add(objResource4);
        
        
        LFA_Resource__c objResource5  = new LFA_Resource__c();
        //objResource.Name = 'Test Resource';
        objResource5.LFA_Service__c = objService5.id;
        objResource5.Rate__c = 1;
        objResource5.LFA_Role__c = objRole1.Id;
        objResource5.LOE__c = 1;
        objResource5.CT_Planned_Cost__c = 1;
        objResource5.CT_Planned_LOE__c = 1;
        objResource5.TGF_Proposed_Cost__c = 1;
        objResource5.TGF_Proposed_LOE__c = 1;
        //insert objResource5;
        lstResources.add(objResource5);
        insert lstResources;
        
        Test.stopTest();
        
        
        
        Apexpages.currentpage().getparameters().put('id',objWp.id);
        ApexPages.StandardController sc = new ApexPages.StandardController(objWp);
        ctrlAddEditWorkPlan objCls = new ctrlAddEditWorkPlan(sc);
        
        objCls.ClearFilterMainGrid();
        objCls.ExapndAllServiceType();
        objCls.CollepseAllServiceType();
        
        Apexpages.currentpage().getparameters().put('AddIndex','0');
        objCls.AddService();
        
        Apexpages.currentpage().getparameters().put('SaveServiceIndex','3');
        Apexpages.currentpage().getparameters().put('SaveServiceTypeIndex','0');
        objCls.SaveServiceAndAddResources();
        
        Apexpages.currentpage().getparameters().put('newServiceId',objService.id);
        objCls.AddResource();
        objCls.CancelResources();
      
        Apexpages.currentpage().getparameters().put('DuplicateIndex','0');
        Apexpages.currentpage().getparameters().put('DuplicateSTIndex','0');
        Apexpages.currentpage().getparameters().put('ServiceId',objService.id);
        objCls.DuplicateService();
        
        Apexpages.currentpage().getparameters().put('DeleteIndex','1');
        Apexpages.currentpage().getparameters().put('DeleteSTIndex','0');
        objCls.DeleteService();
        
        Apexpages.currentpage().getparameters().put('EditSTIndex','0');
        Apexpages.currentpage().getparameters().put('EditIndex','3');
        objCls.EditService();
        Apexpages.currentpage().getparameters().put('ResourceIndex','0');
        objCls.FillContactsOnChangeRole();
        Apexpages.currentpage().getparameters().put('ResourceIndexForRate','0');
        objCls.FillRateOnChangeContact();
        Apexpages.currentpage().getparameters().put('intIndexST','0');
        Apexpages.currentpage().getparameters().put('intIndex','3');
        objCls.SaveResources();
        
        Apexpages.currentpage().getparameters().put('EditSTIndex','0');
        Apexpages.currentpage().getparameters().put('EditIndex','3');
        objCls.EditService();
        Apexpages.currentpage().getparameters().put('DeleteResourceIndex','0');
        objCls.DeleteResource();
        
        /*objCls.SelectedCountry = '';
        objCls.SelectedRegion = '';
        objCls.AnalyseSearch();
        if(objCls.lstServiceForSearch.size() > 0){
            objCls.lstServiceForSearch[0].IsChecked = true;
        }
        objCls.AddServiceToWorkPlan();*/
        
        objCls.sortDataMainGrid();
        objCls.sortExpression = 'Country__c';
        objCls.sortDirection = 'ASC';
        objCls.SelectedCountry = '';
        objCls.SelectedRegion = '';
        objCls.getSortDirection();
        objCls.setSortDirection('ASC');
        objCls.rerenderaction();
        //objCls.AnalyseSearch();
        objCls.CancelReadOnlyResources();
        objCls.getSortDirectionMainGrid();
        objCls.DeleteMultipleServices();
        objCls.ClearFilterSearchGrid();
        objCls.BackToWorkPlan();
        objCls.getResorcesForPDF();
        
    }
    
    Public static testMethod Void TestctrlAddEditWorkPlan1(){
        Account objAcc = new Account();
        objAcc.Name = 'Test Acc';
        objAcc.Short_Name__c= 'Test Acc';
        insert objAcc;
        
        Country__c objCountry = new Country__c();
        objCountry.Name = 'Test Country';
        objCountry.Region_Name__c = 'Test Region';
        objCountry.Used_for_LFA_Services__c = TRUE;
        insert objCountry;
        
        //For different workplan for Analyse Search
        List<LFA_Work_Plan__c> lstWorkPlans = new List<LFA_Work_Plan__c>();
        LFA_Work_Plan__c objWp2 = new LFA_Work_Plan__c();
        objWp2.Name = 'Test Wp';
        objWp2.LFA__c = objAcc.Id;
        objWp2.Country__c = objCountry.id;
        objWP2.Include_In_Library__c = TRUE;
        objWP2.Year__c = '2014';
        //insert objWp2;
        lstWorkPlans.add(objWp2);
        
        LFA_Work_Plan__c objWp = new LFA_Work_Plan__c();
        objWp.Name = 'Test Wp';
        objWp.LFA__c = objAcc.Id;
        objWp.Country__c = objCountry.id;
        objWP.Include_In_Library__c = TRUE;
        objWP.Year__c = '2014';
        //insert objWp;
        lstWorkPlans.add(objWp);
        
        LFA_Work_Plan__c objWp1 = new LFA_Work_Plan__c();
        objWp1.Name = 'Test Wp';
        objWp1.LFA__c = objAcc.Id;
        objWp1.Country__c = objCountry.id;
        objWp1.End_Date__c = system.today();
        //insert objWp1;
        lstWorkPlans.add(objWp1);
        insert lstWorkPlans;
               
        List<LFA_Service__c> lstSerivce = new List<LFA_Service__c>();
        
        LFA_Service__c objService = new LFA_Service__c();
        objService.Name = 'PUDR';
        objService.LFA_Work_Plan__c = objWp.id;
        objService.Service_Type__c = 'Key Services';
        objService.Service_Sub_Type__c = 'PUDR';
        objService.Exclude_from_Library__c=FALSE;        
        lstSerivce.add(objService);
        
        LFA_Service__c objService1 = new LFA_Service__c();
        objService1.Name = 'PUDR';
        objService1.LFA_Work_Plan__c = objWp.id;
        objService1.Service_Type__c = 'Key Services';
        objService1.Service_Sub_Type__c = 'PUDR';
        lstSerivce.add(objService1);
        
        LFA_Service__c objService2 = new LFA_Service__c();
        objService2.Name = 'PUDR';
        objService2.LFA_Work_Plan__c = objWp.id;
        objService2.Service_Type__c = 'Other LFA Services';
        objService2.Service_Sub_Type__c = 'PUDR';
        lstSerivce.add(objService2);
        
        LFA_Service__c objService3 = new LFA_Service__c();
        objService3.Name = 'PUDR';
        objService3.LFA_Work_Plan__c = objWp.id;
        objService3.Service_Type__c = 'Key Services';
        objService3.Service_Sub_Type__c = 'PUDR';
        lstSerivce.add(objService3);
        
        LFA_Service__c objService4 = new LFA_Service__c();
        objService4.Name = 'PUDR';
        objService4.LFA_Work_Plan__c = objWp1.id;
        objService4.Service_Type__c = 'Key Services';
        objService4.Service_Sub_Type__c = 'PUDR';
        lstSerivce.add(objService4);
        
        LFA_Service__c objService5 = new LFA_Service__c();
        objService5.Name = 'PUDR';
        objService5.LFA_Work_Plan__c = objWp1.id;
        objService5.Service_Type__c = 'Key Services';
        objService5.Service_Sub_Type__c = 'PUDR';
        lstSerivce.add(objService5);
        
        LFA_Service__c objService6 = new LFA_Service__c();
        objService6.Name = 'PUDR';
        objService6.LFA_Work_Plan__c = objWp2.id;
        objService6.Service_Type__c = 'Key Services';
        objService6.Service_Sub_Type__c = 'PUDR';
        objService6.Exclude_from_Library__c=FALSE;        
        lstSerivce.add(objService6);
        
        
        insert lstSerivce;
        
        List<LFA_Role__c> lstRoles = new List<LFA_Role__c>();
        LFA_Role__c objRole = new LFA_Role__c();
        objRole.Name = 'Test Role1';
        //insert objRole;
        lstRoles.add(objRole);
        
        Contact objCon = new Contact();
        objCon.LastName = 'Test LName';
        objCon.AccountId = objAcc.Id;
        insert objCon;
        
        List<LFA_Resource__c> lstResources = new List<LFA_Resource__c>();
        
        LFA_Resource__c objResource = new LFA_Resource__c();
        //objResource.Name = 'Test Resource';
        objResource.Contact__c = objCon.Id;
        objResource.LFA_Service__c = objService.id;
        objResource.Rate__c = 1;
        objResource.LFA_Role__c = objRole.Id;
        objResource.LOE__c = 1;
        objResource.CT_Planned_Cost__c = 1;
        objResource.CT_Planned_LOE__c = 1;
        objResource.TGF_Proposed_Cost__c = 1;
        objResource.TGF_Proposed_LOE__c = 1;
        //insert objResource;
        lstResources.add(objResource);
        
        LFA_Resource__c objResource6 = new LFA_Resource__c();
        //objResource.Name = 'Test Resource';
        objResource6.Contact__c = objCon.Id;
        objResource6.LFA_Service__c = objService.id;
        objResource6.Rate__c = 1;
        objResource6.LFA_Role__c = objRole.Id;
        objResource6.LOE__c = 1;
        objResource6.CT_Planned_Cost__c = 1;
        objResource6.CT_Planned_LOE__c = 1;
        objResource6.TGF_Proposed_Cost__c = 1;
        objResource6.TGF_Proposed_LOE__c = 1;
        //insert objResource6;
        lstResources.add(objResource6);
        
        LFA_Resource__c objResource7 = new LFA_Resource__c();
        //objResource.Name = 'Test Resource';
        objResource7.Contact__c = objCon.Id;
        objResource7.LFA_Service__c = objService.id;
        objResource7.Rate__c = 1;
        objResource7.LFA_Role__c = objRole.Id;
        objResource7.LOE__c = 1;
        objResource7.CT_Planned_Cost__c = 1;
        objResource7.CT_Planned_LOE__c = 1;
        objResource.TGF_Proposed_Cost__c = 1;
        objResource.TGF_Proposed_LOE__c = 1;
        //insert objResource7;
        lstResources.add(objResource7);
        
        Rate__c objRate = new Rate__c();
        objRate.LFA_Role__c = objRole.Id;
        objRate.Contact__c = objCon.Id;
        objRate.Country__c = objCountry.Id;
        objRate.Rate__c = 1;
        objRate.Active__c = true;
        objRate.Gf_approved__c = 'Approved';
        insert objRate;
        
        Grant__c objGrant = new Grant__c();
        objGrant.Name = 'Test Grant';
        objGrant.Country__c = objCountry.id;
        objGrant.Principal_Recipient__c = objAcc.id;
        objGrant.Financial_and_Fiduciary_Risks__c = 'Low';
        objGrant.Health_Services_and_Products_Risks__c = 'High';
        objGrant.Programmatic_and_Performance_Risks__c = 'Medium';
        insert objGrant;
        
        
        LFA_Resource__c objResource1 = new LFA_Resource__c();
        //objResource1.Name = 'Test Resource1';
        objResource1.LFA_Service__c = objService1.id;
        objResource1.Rate__c = 2;
        objResource1.LOE__c = 2;
        objResource1.CT_Planned_Cost__c = 2;
        objResource1.CT_Planned_LOE__c = 2;
        objResource1.TGF_Proposed_Cost__c = 2;
        objResource1.TGF_Proposed_LOE__c = 2;
        //insert objResource1;
        lstResources.add(objResource1);
        
        
        LFA_Role__c objRole1 = new LFA_Role__c();
        objRole1.Name = 'Test Role';
        //insert objRole1;
        lstRoles.add(objRole1);
        
        LFA_Resource__c objResource2 = new LFA_Resource__c();
        //objResource2.Name = 'Test Resource1';
        objResource2.LFA_Service__c = objService2.id;
        objResource2.LFA_Role__c = objRole.id;
        objResource2.Rate__c = 2;
        objResource2.LOE__c = 2;
        objResource2.CT_Planned_Cost__c = 2;
        objResource2.CT_Planned_LOE__c = 2;
        objResource2.TGF_Proposed_Cost__c = 2;
        objResource2.TGF_Proposed_LOE__c = 2;
        //insert objResource2;
        lstResources.add(objResource2);
        
        Contact objCon1 = new Contact();
        objCon1.LastName = 'Test LName';
        objCon1.AccountId = objAcc.Id;
        insert objCon1;
        
        Rate__c objRate1 = new Rate__c();
        objRate1.LFA_Role__c = objRole1.Id;
        objRate1.Contact__c = objCon1.Id;
        objRate1.Country__c = objCountry.Id;
        objRate1.Rate__c = 1;
        objRate1.Active__c = true;
        objRate1.Gf_approved__c = 'Approved';
        insert objRate1;
        
        Grant__c objGrant1 = new Grant__c();
        objGrant1.Name = 'Test Grant';
        objGrant1.Country__c = objCountry.id;
        objGrant1.Principal_Recipient__c = objAcc.id;
        objGrant1.Financial_and_Fiduciary_Risks__c = 'Low';
        objGrant1.Health_Services_and_Products_Risks__c = 'High';
        objGrant1.Programmatic_and_Performance_Risks__c = 'Medium';
        insert objGrant1;
        
        
        
        LFA_Role__c objRole2 = new LFA_Role__c();
        objRole2.Name = 'Test Role1';
        //insert objRole2;
        lstRoles.add(objRole2);
        insert lstRoles;
        
        LFA_Resource__c objResource3 = new LFA_Resource__c();
        //objResource.Name = 'Test Resource';
        objResource3.LFA_Service__c = objService3.id;
        objResource3.Rate__c = 1;
        objResource3.LFA_Role__c = objRole2.Id;
        objResource3.LOE__c = 1;
        objResource3.CT_Planned_Cost__c = 1;
        objResource3.CT_Planned_LOE__c = 1;
        objResource3.TGF_Proposed_Cost__c = 1;
        objResource3.TGF_Proposed_LOE__c = 1;
        //insert objResource3;
        lstResources.add(objResource3);
        
        Rate__c objRate2 = new Rate__c();
        objRate2.LFA_Role__c = objRole2.Id;
        objRate2.Contact__c = objCon.Id;
        objRate2.Country__c = objCountry.Id;
        objRate2.Rate__c = 1;
        objRate2.Active__c = true;
        objRate2.Gf_approved__c = 'Approved';
        insert objRate2;
        
        
        
        LFA_Resource__c objResource4 = new LFA_Resource__c();
        //objResource.Name = 'Test Resource';
        objResource4.LFA_Service__c = objService4.id;
        objResource4.Rate__c = 1;
        objResource4.LFA_Role__c = objRole1.Id;
        objResource4.LOE__c = 1;
        objResource4.CT_Planned_Cost__c = 1;
        objResource4.CT_Planned_LOE__c = 1;
        objResource4.TGF_Proposed_Cost__c = 1;
        objResource4.TGF_Proposed_LOE__c = 1;
        //insert objResource4;
        lstResources.add(objResource4);
        
        LFA_Resource__c objResource5  = new LFA_Resource__c();
        //objResource.Name = 'Test Resource';
        objResource5.LFA_Service__c = objService5.id;
        objResource5.Rate__c = 1;
        objResource5.LFA_Role__c = objRole1.Id;
        objResource5.LOE__c = 1;
        objResource5.CT_Planned_Cost__c = 1;
        objResource5.CT_Planned_LOE__c = 1;
        objResource5.TGF_Proposed_Cost__c = 1;
        objResource5.TGF_Proposed_LOE__c = 1;
        //insert objResource5;
        lstResources.add(objResource5);
        
        LFA_Resource__c objResource8 = new LFA_Resource__c();
        //objResource.Name = 'Test Resource';
        objResource8.Contact__c = objCon.Id;
        objResource8.LFA_Service__c = objService6.id;
        objResource8.Rate__c = 1;
        objResource8.LFA_Role__c = objRole.Id;
        objResource8.LOE__c = 1;
        objResource8.CT_Planned_Cost__c = 1;
        objResource8.CT_Planned_LOE__c = 1;
        objResource8.TGF_Proposed_Cost__c = 1;
        objResource8.TGF_Proposed_LOE__c = 1;
        //insert objResource8;
        lstResources.add(objResource8);
        
        LFA_Resource__c objResource9 = new LFA_Resource__c();
        //objResource.Name = 'Test Resource';
        objResource9.Contact__c = objCon.Id;
        objResource9.LFA_Service__c = objService6.id;
        objResource9.Rate__c = 1;
        objResource9.LFA_Role__c = objRole.Id;
        objResource9.LOE__c = 1;
        objResource9.CT_Planned_Cost__c = 1;
        objResource9.CT_Planned_LOE__c = 1;
        objResource9.TGF_Proposed_Cost__c = 1;
        objResource9.TGF_Proposed_LOE__c = 1;
        //insert objResource9;
        lstResources.add(objResource9);
        insert lstResources;
        
        Apexpages.currentpage().getparameters().put('id',objWp.id);
        ApexPages.StandardController sc = new ApexPages.StandardController(objWp);
        ctrlAddEditWorkPlan objCls = new ctrlAddEditWorkPlan(sc);
        
        objCls.SelectedCountry = '';
        objCls.SelectedRegion = '';
        objCls.AnalyseSearch();
        if(objCls.lstServiceForSearch.size() > 0){
            for(Integer i=0; i<objCls.lstServiceForSearch.size(); i++){
                objCls.lstServiceForSearch[i].IsChecked = true;
            }
            //objCls.lstServiceForSearch[0].IsChecked = true;
        }
        objCls.AddServiceToWorkPlan();
        objCls.AnalyseMainGridAction();
    }
    
    Public static testMethod Void TestctrlAddEditWorkPlan2(){
        Account objAcc = new Account();
        objAcc.Name = 'Test Acc';
        objAcc.Short_Name__c= 'Test Acc';
        insert objAcc;
        
        Country__c objCountry = new Country__c();
        objCountry.Name = 'Test Country';
        objCountry.Region_Name__c = 'Test Region';
        objCountry.Used_for_LFA_Services__c = TRUE;
        insert objCountry;
        
        //For different workplan for Analyse Search
        
        List<LFA_Work_Plan__c> lstWorkPlans = new List<LFA_Work_Plan__c>();
        LFA_Work_Plan__c objWp2 = new LFA_Work_Plan__c();
        objWp2.Name = 'Test Wp';
        objWp2.LFA__c = objAcc.Id;
        objWp2.Country__c = objCountry.id;
        objWP2.Include_In_Library__c = TRUE;
        objWP2.Year__c = '2014';
        //insert objWp2;
        lstWorkPlans.add(objWp2);
        
        LFA_Work_Plan__c objWp = new LFA_Work_Plan__c();
        objWp.Name = 'Test Wp';
        objWp.LFA__c = objAcc.Id;
        objWp.Country__c = objCountry.id;
        objWP.Include_In_Library__c = TRUE;
        objWP.Year__c = '2014';
        //insert objWp;
        lstWorkPlans.add(objWp);
        
        LFA_Work_Plan__c objWp1 = new LFA_Work_Plan__c();
        objWp1.Name = 'Test Wp';
        objWp1.LFA__c = objAcc.Id;
        objWp1.Country__c = objCountry.id;
        objWp1.End_Date__c = system.today();
        //insert objWp1;
        lstWorkPlans.add(objWp1);
        insert lstWorkPlans;
        
        List<LFA_Service__c> lstSerivce = new List<LFA_Service__c>();
        
        LFA_Service__c objService = new LFA_Service__c();
        objService.Name = 'PUDR';
        objService.LFA_Work_Plan__c = objWp.id;
        objService.Service_Type__c = 'Key Services';
        objService.Service_Sub_Type__c = 'PUDR';
        objService.Exclude_from_Library__c=FALSE;        
        lstSerivce.add(objService);
        
        LFA_Service__c objService1 = new LFA_Service__c();
        objService1.Name = 'PUDR';
        objService1.LFA_Work_Plan__c = objWp.id;
        objService1.Service_Type__c = 'Key Services';
        objService1.Service_Sub_Type__c = 'PUDR';
        lstSerivce.add(objService1);
        
        LFA_Service__c objService2 = new LFA_Service__c();
        objService2.Name = 'PUDR';
        objService2.LFA_Work_Plan__c = objWp.id;
        objService2.Service_Type__c = 'Other LFA Services';
        objService2.Service_Sub_Type__c = 'PUDR';
        lstSerivce.add(objService2);
        
        LFA_Service__c objService3 = new LFA_Service__c();
        objService3.Name = 'PUDR';
        objService3.LFA_Work_Plan__c = objWp.id;
        objService3.Service_Type__c = 'Key Services';
        objService3.Service_Sub_Type__c = 'PUDR';
        lstSerivce.add(objService3);
        
        LFA_Service__c objService4 = new LFA_Service__c();
        objService4.Name = 'PUDR';
        objService4.LFA_Work_Plan__c = objWp1.id;
        objService4.Service_Type__c = 'Key Services';
        objService4.Service_Sub_Type__c = 'PUDR';
        lstSerivce.add(objService4);
        
        LFA_Service__c objService5 = new LFA_Service__c();
        objService5.Name = 'PUDR';
        objService5.LFA_Work_Plan__c = objWp1.id;
        objService5.Service_Type__c = 'Key Services';
        objService5.Service_Sub_Type__c = 'PUDR';
        lstSerivce.add(objService5);
        
        LFA_Service__c objService6 = new LFA_Service__c();
        objService6.Name = 'PUDR';
        objService6.LFA_Work_Plan__c = objWp2.id;
        objService6.Service_Type__c = 'Key Services';
        objService6.Service_Sub_Type__c = 'PUDR';
        objService6.Exclude_from_Library__c=FALSE;        
        lstSerivce.add(objService6);
        
        
        insert lstSerivce;
        
        LFA_Role__c objRole = new LFA_Role__c();
        objRole.Name = 'Test Role1';
        insert objRole;
        
        Contact objCon = new Contact();
        objCon.LastName = 'Test LName';
        objCon.AccountId = objAcc.Id;
        insert objCon;
        
        List<LFA_Resource__c> lstResources = new List<LFA_Resource__c>();
        
        LFA_Resource__c objResource = new LFA_Resource__c();
        //objResource.Name = 'Test Resource';
        objResource.Contact__c = objCon.Id;
        objResource.LFA_Service__c = objService.id;
        objResource.Rate__c = 1;
        objResource.LFA_Role__c = objRole.Id;
        objResource.LOE__c = 1;
        objResource.CT_Planned_Cost__c = 1;
        objResource.CT_Planned_LOE__c = 1;
        objResource.TGF_Proposed_Cost__c = 1;
        objResource.TGF_Proposed_LOE__c = 1;
        //insert objResource;
        lstResources.add(objResource);
        
        LFA_Resource__c objResource1 = new LFA_Resource__c();
        //objResource.Name = 'Test Resource';
        objResource1.Contact__c = objCon.Id;
        objResource1.LFA_Service__c = objService.id;
        objResource1.Rate__c = 1;
        objResource1.LFA_Role__c = objRole.Id;
        objResource1.LOE__c = 1;
        objResource1.CT_Planned_Cost__c = 1;
        objResource1.CT_Planned_LOE__c = 1;
        objResource1.TGF_Proposed_Cost__c = 1;
        objResource1.TGF_Proposed_LOE__c = 1;
        //insert objResource1;
        lstResources.add(objResource1);
        
        LFA_Resource__c objResource2 = new LFA_Resource__c();
        //objResource.Name = 'Test Resource';
        objResource2.Contact__c = objCon.Id;
        objResource2.LFA_Service__c = objService.id;
        objResource2.Rate__c = 1;
        objResource2.LFA_Role__c = objRole.Id;
        objResource2.LOE__c = 1;
        objResource2.CT_Planned_Cost__c = 1;
        objResource2.CT_Planned_LOE__c = 1;
        objResource2.TGF_Proposed_Cost__c = 1;
        objResource2.TGF_Proposed_LOE__c = 1;
        //insert objResource2;
        lstResources.add(objResource2);
        insert lstResources;
        
        List<Rate__c> lstRates = new List<Rate__c>();
        
        Rate__c objRate = new Rate__c();
        objRate.LFA_Role__c = objRole.Id;
        //objRate.Contact__c = objCon.Id;
        objRate.Country__c = objCountry.Id;
        objRate.Rate__c = 1;
        objRate.Active__c = true;
        objRate.Gf_approved__c = 'Approved';
        //insert objRate;
        lstRates.add(objRate);
        
        Rate__c objRate1 = new Rate__c();
        objRate1.LFA_Role__c = objRole.Id;
        //objRate1.Contact__c = objCon.Id;
        objRate1.Country__c = objCountry.Id;
        objRate1.Rate__c = 1;
        objRate1.Active__c = true;
        objRate1.Gf_approved__c = 'Approved';
        //insert objRate1;
        lstRates.add(objRate1);
        
        insert lstRates;
        
        Grant__c objGrant = new Grant__c();
        objGrant.Name = 'Test Grant';
        objGrant.Country__c = objCountry.id;
        objGrant.Principal_Recipient__c = objAcc.id;
        objGrant.Financial_and_Fiduciary_Risks__c = 'Low';
        objGrant.Health_Services_and_Products_Risks__c = 'High';
        objGrant.Programmatic_and_Performance_Risks__c = 'Medium';
        insert objGrant;
        
        Apexpages.currentpage().getparameters().put('id',objWp.id);
        ApexPages.StandardController sc = new ApexPages.StandardController(objWp);
        ctrlAddEditWorkPlan objCls = new ctrlAddEditWorkPlan(sc);
        
        Apexpages.currentpage().getparameters().put('AddIndex','0');
        objCls.AddService();
        
        Apexpages.currentpage().getparameters().put('SaveServiceIndex','3');
        Apexpages.currentpage().getparameters().put('SaveServiceTypeIndex','0');
        objCls.SaveServiceAndAddResources();
        
        Apexpages.currentpage().getparameters().put('newServiceId',objService.id);
        objCls.AddResource();
        objCls.CancelResources();
      
        Apexpages.currentpage().getparameters().put('DuplicateIndex','0');
        Apexpages.currentpage().getparameters().put('DuplicateSTIndex','0');
        Apexpages.currentpage().getparameters().put('ServiceId',objService.id);
        objCls.DuplicateService();
        
        Apexpages.currentpage().getparameters().put('DeleteIndex','1');
        Apexpages.currentpage().getparameters().put('DeleteSTIndex','0');
        objCls.DeleteService();
        
        Apexpages.currentpage().getparameters().put('EditSTIndex','0');
        Apexpages.currentpage().getparameters().put('EditIndex','3');
        objCls.EditService();
        
        Apexpages.currentpage().getparameters().put('ResourceIndex','0');
        objCls.FillContactsOnChangeRole();
        Apexpages.currentpage().getparameters().put('ResourceIndexForRate','0');
        objCls.FillRateOnChangeContact();
    }
}