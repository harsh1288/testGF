/*********************************************************************************
* {Test} Class: {saveAndNewDocuploadControllerTest}
* Created by {Rahul Kumar},  {DateCreated 20-Nov-2014}
----------------------------------------------------------------------------------
* Purpose/Methods:
* - Used for cover all methods of saveAndNewDocuploadController Controller.* 
*
----------------------------------------------------------------------------------
* History:
* - VERSION    DEVELOPER NAME     DATE      DETAIL FEATURES
      1.0       Rahul Kumar    20-Nov-2014
*********************************************************************************/
@isTest
public class saveAndNewDocuploadControllerTest {
    static saveAndNewDocuploadController ext;
    static saveAndNewDocuploadController extW;
    static DocumentUpload_GM__c masterObject;
    static PageReference pref;
    static PageReference prefNull;
    static User testUser = TestClassHelper.createExtUser();    
   
    

    Public static testMethod Void TestSaveAndNew(){
     testUser.username = 'welc@example.com';
     insert testUser;
     
            Test.startTest();
            //Concept_Note__c cn = TestClassHelper.createCN();
            Account acc = TestClassHelper.insertAccount();
            Implementation_Period__c ip = TestClassHelper.createIP(TestClassHelper.insertGrant(acc),TestClassHelper.insertAccount());
            insert ip;
            masterObject = new DocumentUpload_GM__c();
            //masterObject.Concept_Note__c=cn.id;
            pref = Page.newDocumentUploadRedirection;
            Test.setCurrentPage(pref);
            ApexPages.StandardController con = new ApexPages.StandardController(masterObject);
            ext = new saveAndNewDocuploadController(con);           
            pref.getParameters().put('retUrl','GM%2F');
            Test.setCurrentPage(pref);
            System.runAs(testUser) {                                    
                ext.createNewDocurl();            
            }
            pref.getParameters().put('retUrl','?');
            Test.setCurrentPage(pref);
            System.runAs(testUser) {                                    
            ext.createNewDocurl();            
        }
            masterObject.Implementation_Period__c = ip.id;
            pref = Page.newDocumentUploadRedirection;
            Test.setCurrentPage(pref);
            ApexPages.StandardController conW = new ApexPages.StandardController(masterObject);
            extW = new saveAndNewDocuploadController(conW);
            extW.createNewDocurl();
            System.runAs(testUser) {                                    
                extW.createNewDocurl();            
            }
            Test.stopTest();
    }
}