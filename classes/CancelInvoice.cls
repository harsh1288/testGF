/*********************************************************************************
* Controller Class: CancelInvoice
* Created by Vera Solutions, DateCreated:  02/14/2014
----------------------------------------------------------------------------------
* Purpose/Methods:
* - 
----------------------------------------------------------------------------------
* Unit Test: 
----------------------------------------------------------------------------------
* History:
* - VERSION     DATE            DETAIL FEATURES
    1.0         14 Feb 2014     INITIAL DEVELOPMENT   
*********************************************************************************/


global Class CancelInvoice{
    WebService static String updateServiceInvoiceStatus(String strInvoiceId) {
        try{
            if(String.isBlank(strInvoiceId) == false){
                List<Service_Invoice__c> lstServiceInvoice = [Select LFA_Service__c,ID from Service_Invoice__c 
                                                        where LFA_Invoice__c = :strInvoiceId];
                Set<Id> setServiceIds = new Set<Id>();
                for(Service_Invoice__c objSI : lstServiceInvoice){
                    setServiceIds.add(objSI.LFA_Service__c);
                }
                List<LFA_Service__c> lstServiceToUpdate = [Select ID,Status__c from LFA_Service__c 
                                                        where Id IN : setServiceIds];
                for(LFA_Service__c objService : lstServiceToUpdate){
                    if(objService.Status__c != 'TGF Agreed'){
                        objService.Status__c = 'TGF Agreed';
                    }
                }
                update lstServiceToUpdate;
                
                List<LFA_Invoice__c> lstInvoiceToUpdate = [Select ID,Status__c from LFA_Invoice__c 
                                                        where Id =: strInvoiceId Limit 1];
                if(lstInvoiceToUpdate.size() > 0){
                    lstInvoiceToUpdate[0].Status__c = 'Cancelled';
                    update lstInvoiceToUpdate;
                }
                Delete lstServiceInvoice;
            }
            
        }catch(exception ex){
           return ex.getMessage();
        }            
        return '';
    }
}