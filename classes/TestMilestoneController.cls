@isTest
public class TestMilestoneController{
    public static Milestone_Target__c milestone;
    public static void init(){
        Account a = TestClassHelper.insertAccount();
        insert a;
        Grant__c grantObj = TestClassHelper.createGrant(a);
        insert GrantObj;
        Implementation_Period__c ipObj = TestClassHelper.createIP(grantObj, a);
        insert ipObj;
        Performance_Framework__c pf = TestClassHelper.createPF(ipObj);
        insert pf;
        Module__c module = TestClassHelper.createModule();
        module.Implementation_Period__c = ipObj.id;
        insert module;
        Grant_Intervention__c  inter = TestClassHelper.createGrantIntervention(ipObj);
        insert inter;
        Period__c period = TestClassHelper.createPeriod();
        period.is_Active__c = true;
        period.performance_Framework__c = pf.id;
        insert period;
        //Key_Activity__c act = TestClassHelper.createKeyActivity(inter);
        //insert act;
        //milestone = TestClassHelper.createMilestone(act);
        //insert milestone;
        
    }
    
     public static testMethod void milestoneControllerTest(){
         init();
         
         Milestone_Target__c milestone1 = new Milestone_Target__c();
         ApexPages.StandardController stdctrl = new ApexPages.StandardController(milestone1);
         //ApexPages.currentPage().getParameters().put('id',milestone.id);
         milestoneController ctrl = new milestoneController(stdctrl);    
     }
    
}