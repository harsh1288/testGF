@isTest 
private class testCampaignAfterInsert {
    static testMethod void testCampaignAfterInsert() {
      
      RecordType EventRT = [Select id,name from RecordType where Name=: 'Event' AND SobjectType=: 'Campaign'];
      RecordType EmailRT = [Select id,name from RecordType where Name=: 'Email' AND SobjectType=: 'Campaign'];
      
      /*Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
      User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = p.Id, 
      TimeZoneSidKey='America/Los_Angeles', UserName='s@testorg.com');
      insert u;*/
      
      campaign Cam =  new campaign();
      cam.name = 'Event Campaign test';
      cam.recordtypeid =EventRT.id;
      insert cam;
      
      
      campaign Cam1 =  new campaign();
      cam1.name = 'Email Campaign test';
      cam1.Confirmation_Text_Accepted__c = 'test';
      cam1.Confirmation_Text_Declined__c = 'test2';
      cam1.recordtypeid = EmailRT.id;
      insert cam1;
       
    }
}