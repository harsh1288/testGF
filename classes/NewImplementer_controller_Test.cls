/*********************************************************************************
* {Test} Class: {NewImplementer_controller_Test}
* Created by {Rahul Kumar},  {DateCreated 18-Nov-2014}
----------------------------------------------------------------------------------
* Purpose/Methods:
* - Used for cover all methods of NewImplementer_controller Controller.* 
*
----------------------------------------------------------------------------------
* History:
* - VERSION    DEVELOPER NAME     DATE      DETAIL FEATURES
      1.0       Rahul Kumar    21-Nov-2014
*********************************************************************************/

@isTest
public class NewImplementer_controller_Test {
    
    Public static testMethod Void TestSaveAndNew(){
       Country__c objCountry = TestClassHelper.createCountry();
       insert objCountry;
       
       Account objAcc = TestClassHelper.createAccount();
       objAcc.RecordTypeId = [select id from Recordtype where SobjectType =: 'Account' and name =: 'PR'].Id;
       objAcc.Country__c = objCountry.Id;
       insert objAcc;
       
       Account objAcc1 = TestClassHelper.createAccount();
       objAcc1.RecordTypeId = [select id from Recordtype where SobjectType =: 'Account' and name =: 'PR'].Id;
       objAcc1.Country__c = objCountry.Id;
       insert objAcc1;
       
       Account objAcc2 = TestClassHelper.createAccount();
       objAcc2.RecordTypeId = [select id from Recordtype where SobjectType =: 'Account' and name =: 'PR'].Id;
       objAcc2.Country__c = objCountry.Id;
       insert objAcc2;
       
       User testUser = TestClassHelper.createExtUser();
       testUser.username = 'NIC123@test.org';
       insert testUser;
       
       Grant__c objGrant = TestClassHelper.createGrant(objAcc);
       objGrant.Principal_Recipient__c = objAcc.Id;
       objGrant.Country__c = objCountry.Id;
       insert objGrant;
       
       Implementation_Period__c objIP = TestClassHelper.createIPWithConceptNote(objGrant, objAcc);
       objIP.Principal_Recipient__c = objAcc.Id;
       insert objIP;
       
       Performance_Framework__c objPF = TestClassHelper.createPF(objIP);
       objPF.Implementation_Period__c = objIP.Id;
       insert objPF;
       
       Implementer__c objimp = new Implementer__c();
       objimp.Performance_Framework__c = objPF.Id;
       objimp.Grant_Implementation_Period__c = objIP.Id;
       objimp.Principal_Recipient__c = objAcc.Id;
       insert objimp;
       
       Implementer__c objimp1 = new Implementer__c();
       objimp1.Performance_Framework__c = objPF.Id;
       objimp1.Grant_Implementation_Period__c = objIP.Id;
       objimp1.Principal_Recipient__c = objAcc.Id;
       objimp1.Add_Implementer__c = 'New';
       insert objimp1;
       
       Implementer__c objimp2 = new Implementer__c();
       objimp2.Performance_Framework__c = objPF.Id;
       objimp2.Grant_Implementation_Period__c = objIP.Id;
       objimp2.Principal_Recipient__c = objAcc.Id;
       objimp2.Country__c = objCountry.Id;
       objimp2.Add_Implementer__c = 'Existing';
       objimp2.Account__c = objAcc1.Id;
       insert objimp2;
       
       List<Implementer__c> imps = new List<Implementer__c>();
       imps = [select id from Implementer__c where Performance_Framework__c =: objPF.Id];
       
       PageReference pageRef = Page.NewImplementer;
       pageRef.getParameters().put('Id',objPF.Id);
       Test.setCurrentPageReference(pageRef);
       
       NewImplementer_controller objnewimplementer1 = new NewImplementer_controller(new ApexPages.StandardSetController(imps));
       objnewimplementer1.checkError();
      
       objnewimplementer1.objImp.Add_Implementer__c = 'New';
       objnewimplementer1.objImp.Implementer_Name__c = '';
       objnewimplementer1.checkError();
       
       objnewimplementer1.objImp.Add_Implementer__c = 'Existing';
       objnewimplementer1.strsubrecpient = '';
       objnewimplementer1.checkError();
       
       NewImplementer_controller objnewimplementer = new NewImplementer_controller(new ApexPages.StandardSetController(imps));
       ApexPages.currentPage().getParameters().put('save_new','1');
       ApexPages.currentPage().getParameters().put('retURL','');
       objnewimplementer.saveNnewCheck();
       objnewimplementer.objImp.Add_Implementer__c = 'Existing';
       objnewimplementer.strsubrecpient = objAcc2.Id;
       objnewimplementer.SaveInfo();
       System.runAs(testUser) {   
          objnewimplementer.saveNnewCheck(); 
          objnewimplementer.SaveInfo();           
       }
       
       PageReference pageRef1 = Page.NewImplementer;
       pageRef1.getParameters().put('Id',objPF.Id);
       Test.setCurrentPageReference(pageRef1);
       
       ApexPages.StandardController stdController = new ApexPages.StandardController(objimp);
       NewImplementer_controller ext = new NewImplementer_controller(stdController);
       
    }
}