/**
 PUBLIC Class: {LFAWorkPlanHandler }
* Created by {LRT}, {DateCreated 01/23/2014}
----------------------------------------------------------------------------------
*Trigger Handler Class For Update All LFA Service Based on 
    the LFAWork Plan Status To 'Service Delivery.'
---------------------------------------------------------------------------------
*----------------------------------------------------------------------------------
* History:
* - VERSION  DEVELOPER NAME    DATE            DETAIL FEATURES
*    1.0      LRT              01/23/2014      INITIAL DEVELOPMENT
*/

public class LFAWorkPlanHandler {
    //Set the LFA Service Status based on the LFAWorkplan Status Update.
    public static void auLFAWorkPlanStatus(List<LFA_Work_Plan__c> lstLFAWorkPlanNew,Map<Id,LFA_Work_Plan__c> mapLFAWorkPlanOld) {
        System.debug('!!!!!! in Method');
        if(lstLFAWorkPlanNew.size() > 0){
            //System.debug('!!!!!! in Method lstLFAWorkPlanNew' +lstLFAWorkPlanNew);
            Set<Id> setLFAWorkPlanIdToUpdate= new Set<Id>();
            for(LFA_Work_Plan__c objLFAWorkPlan : lstLFAWorkPlanNew) {
                if(objLFAWorkPlan.Status__C != mapLFAWorkPlanOld.get(objLFAWorkPlan.Id).Status__C) {
                    if(objLFAWorkPlan.Status__C == 'Service Delivery') {
                        setLFAWorkPlanIdToUpdate.add(objLFAWorkPlan.ID);
                    }
                
                }
             }
             if(setLFAWorkPlanIdToUpdate.size() > 0) {
                 //System.Debug('!!!!!!!!!setLFAWorkPlanIdToUpdate'+setLFAWorkPlanIdToUpdate);
                 string strQuery = 'select Id,Status__c,LFA_Work_Plan__c from LFA_Service__c where LFA_Work_Plan__c in: setLFAWorkPlanIdToUpdate';
                 Id batchUpdateLFAWorkPlanStatus = Database.executeBatch(new BatchUpdateWorkPlanStatus(strQuery,setLFAWorkPlanIdToUpdate), 100); 
                 System.Debug('Batch Job Id : batchUpdateLFAWorkPlanStatus  '+ batchUpdateLFAWorkPlanStatus );
             }
             
        }
                                                   
    }
    //Called From a BatchClass Update All LFAService Status
    public static void auUpdateLFAService(List<LFA_Service__c> lstLFAServiceUpdate) {
        List<LFA_Service__c> lstLFAServiceToUpdate = new List<LFA_Service__c>();
        System.debug('To Update   lstLFAServiceUpdate'+lstLFAServiceUpdate);
        if(lstLFAServiceUpdate.size() > 0) {
            LFA_Service__c objNewLFAService;
            for(LFA_Service__c objLFAService : lstLFAServiceUpdate) {
                objNewLFAService = new LFA_Service__c(id = objLFAService.ID, Status__C = objLFAService.Status__c);
                //If Status is Already Set No Need To Update.
                if(objLFAService.Status__C != 'Not Started') {
                    objNewLFAService.Status__c = 'Not Started';
                    lstLFAServiceToUpdate.add(objNewLFAService);
                }
            }
            //System.debug('Final Update   lstLFAServiceToUpdate'+lstLFAServiceToUpdate);
            //Update All the LFAServices.
            if(lstLFAServiceToUpdate.size() > 0 ) update lstLFAServiceToUpdate;
        }                                         
    }
}