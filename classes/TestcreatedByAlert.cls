@isTest 
private class TestcreatedByAlert{
    static testMethod void TestcreatedByAlert() {
      
       Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
      User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = p.Id, 
      TimeZoneSidKey='America/Los_Angeles', UserName='s@testorg.com');
      insert u;
      
      Account a= new Account();
      a.name= 'Test Acc';
      insert a;
       
       task t = new task();
       t.subject= 'Reminder';
       t.whatid = a.id;
       t.ownerid = u.id;
       t.status = 'In-progress';
       insert t;
       
     
       t.status = 'Completed';
       update t;
    }
}