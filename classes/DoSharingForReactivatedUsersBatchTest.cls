@isTest
public class DoSharingForReactivatedUsersBatchTest {
	
    public static testMethod void testBatch(){
     	AffiliationTriggerTest.setup();
        Account parentAccount = CrmTestUtil.insertAccount('Test 1',true);
        Contact affiliatedContact = CrmTestUtil.insertContact(parentAccount.id,true);
        Account affiliatedAccount = CrmTestUtil.insertAccount('Test 2',true);
        npe5__Affiliation__c testAffiliation = CrmTestUtil.insertAffiliation(affiliatedAccount.Id,affiliatedContact.id, label.CM_Affiliation_RT , 'Read',false);
        testAffiliation.user_reactivated_on__c =System.today();
        insert testAffiliation;
        DoSharingForReactivatedUsersBatch job = new DoSharingForReactivatedUsersBatch();
        Test.startTest();
        Database.executeBatch(job);
        Test.stopTest();
    }
        public static testMethod void testBatch2(){
     	AffiliationTriggerTest.setup();
        Account parentAccount = CrmTestUtil.insertAccount('Test 1',true);
        Contact affiliatedContact = CrmTestUtil.insertContact(parentAccount.id,true);
        Account affiliatedAccount = CrmTestUtil.insertAccount('Test 2',true);
        npe5__Affiliation__c testAffiliation = CrmTestUtil.insertAffiliation(affiliatedAccount.Id,affiliatedContact.id, label.CM_Affiliation_RT , 'Read',false);
        testAffiliation.user_reactivated_on__c =System.today();
        insert testAffiliation;
        DoSharingForReactivatedUsersBatch job = new DoSharingForReactivatedUsersBatch(4);
        Test.startTest();
        Database.executeBatch(job);
        Test.stopTest();
    }
}