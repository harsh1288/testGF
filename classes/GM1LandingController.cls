/**
 * An apex page controller that takes the user to the right start page based on credentials or lack thereof
 */
public with sharing class GM1LandingController {
    
    // Code we will invoke on page load.
    public PageReference forwardToStartPage() {
        
        //if(UserInfo.getUserType()==)
        
        if('{!$User.ContactId}'== ''){
        return new PageReference('/apex/DynamicChangeProfile');
        }
        else{
        return Network.communitiesLanding();
        }
    }
    
    public GM1LandingController() {}
}