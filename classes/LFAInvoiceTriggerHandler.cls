Public class LFAInvoiceTriggerHandler{
    //public static boolean blnStopRecursion = false;
    
    Public static void UpdateStatus(List<LFA_Invoice__c> lstLFAInvoiceUpdated, Map<id,LFA_Invoice__c> oldmap){
        
        set<ID> setOfId = new set<Id>();
        
        if(lstLFAInvoiceUpdated!=null && lstLFAInvoiceUpdated.size() > 0 ){
            for( LFA_Invoice__c objLFAInvoice : lstLFAInvoiceUpdated){
               if(objLFAInvoice.Status__c != null && (objLFAInvoice.Status__c != oldmap.get(objLFAInvoice.id).Status__c) && objLFAInvoice.Status__c == 'Payment Approved' ){
                   setOfId.add(objLFAInvoice.id);
                }
            }
        }
        
        
        List<LFA_Service__c> lstLFAService = new List<LFA_Service__c>();
        
        if(setOfId.size() > 0){
            List<Service_Invoice__c> lstServiceInvoice = new List<Service_Invoice__c>([select id,LFA_Service__c from Service_Invoice__c where LFA_Invoice__c in : setOfId]);
            
            if(lstServiceInvoice != null && lstServiceInvoice.size() > 0){
                for(Service_Invoice__c objSInvoice : lstServiceInvoice){
                    LFA_Service__c objLFAService = new LFA_Service__c(id = objSInvoice.LFA_Service__c);                
                    objLFAService.Status__c = 'Payment Approved';
                    lstLFAService.add(objLFAService );
                }
            }
            if(lstLFAService.size() > 0) update lstLFAService;
        }
    }
}