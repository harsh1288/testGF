/**
 PUBLIC Class: {BatchUpdateWorkPlanStatus }
* Created by {LRT}, {DateCreated 01/23/2014}
----------------------------------------------------------------------------------
* Update All LFA Service Based on the LFAWork Plan Status To 'Service Delivery.'
---------------------------------------------------------------------------------
*----------------------------------------------------------------------------------
* History:
* - VERSION  DEVELOPER NAME    DATE            DETAIL FEATURES
*    1.0      LRT              01/23/2014      INITIAL DEVELOPMENT
*/

global class BatchUpdateWorkPlanStatus implements Database.Batchable<sObject> {
   global final String Query;
   global final Set<ID> setLFAWorkPlanIdToUpdate;
   global BatchUpdateWorkPlanStatus(String q,Set<ID> setLFAWorkPlanId){
      // System.debug('@@@@@@@setLFAWorkPlanIdToUpdate' + setLFAWorkPlanId);
       setLFAWorkPlanIdToUpdate=new Set<Id>();
       setLFAWorkPlanIdToUpdate.addAll(setLFAWorkPlanId);
       Query = q;
     
   }
   global Database.QueryLocator start(Database.BatchableContext BC){
      // System.debug('@@@@@@@@@@@@BatchStarted');
       return Database.getQueryLocator(Query);
   }

   global void execute(Database.BatchableContext BC, List<sObject> scope){
      // System.debug('@@@@@@@@@@@@BatchExecuted');
       if(scope != null && scope.size() > 0){
          //call method to update a all LFA Services.
          LFAWorkPlanHandler.auUpdateLFAService((List<LFA_Service__c>)scope);
          System.debug('@@@@@@@@@@@@ (List<LFA_Service__c>)scope' + (List<LFA_Service__c>)scope);
            
       }
   }

   global void finish(Database.BatchableContext BC){
      // System.debug('@@@@@@@@@@@@BatchFinish');
   }
}