/*
* $Author:      $ TCS - Debarghya Sen
* $Description: $ TriggerHandler Class for Indicator_Goal_Junction object's trigger
* $Date:        $ 11-Nov-2014
* $Revision:    $ 
*/

Public Class Indicator_Goal_junction_Trigger_handler{
    Public Indicator_Goal_junction_Trigger_handler(){
    
    }
    
    public static void preventdeleteIndGoalJunc(List<Ind_Goal_Jxn__c> lstIndGoal){
        //list<Grant_Indicator__c> lstGI;
        list<Ind_Goal_Jxn__c> lstGI;
        //Grant_Indicator__c[] lstGI;
        Set<Id> idGI = new Set<Id>();
        Integer countIGJ; 
        
        for(Ind_Goal_Jxn__c igj: lstIndGoal){
            idGI.add(igj.Indicator__c);
        }
        
        lstGI = [SELECT id FROM Ind_Goal_Jxn__c WHERE Indicator__c IN: idGI];
        //lstGI = [SELECT id, Name, (SELECT id, Name FROM Indicator_Goal_Junctions__r) FROM Grant_Indicator__c WHERE id IN: idGI];
        system.debug('List value:'+lstGI.size());
        system.debug('List content:'+lstGI);
        
        for(Ind_Goal_Jxn__c igj: lstIndGoal){
            if(lstGI.size() == 1)
                igj.addError('You cannot have an Indicator without a Goal linked to it.');
        }
    }
}