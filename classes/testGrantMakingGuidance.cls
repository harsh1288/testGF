@istest
public class testGrantMakingGuidance {
    Public static testMethod void testdisaggregationhistorycontroller(){
        Guidance__c objgd = new Guidance__c();
        objgd.Name = 'test';        
        insert objgd;        
        
        Guidance__c objgd1 = new Guidance__c();
        objgd1.Name = 'test2'; 
        insert objgd1;
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User user1 = new User(Alias = 'standt', Email='standarduser122@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='fr', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='standarduser122@testorg.com');
        System.runAs(user1) {
        PageReference pageRef = Page.GrantMakingGuidance;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('Id2',objgd1.Id);
        GrantMakingGuidance objguidance = new GrantMakingGuidance();
        }
        
        User user2 = new User(Alias = 'standta', Email='standarduser122@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testingd', LanguageLocaleKey='ru', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='standarduser12231@testorg.com');
        System.runAs(user2) {
        PageReference pageRef1 = Page.GrantMakingGuidance;
        Test.setCurrentPage(pageRef1);
        ApexPages.currentPage().getParameters().put('Id',objgd.Id);
        ApexPages.currentPage().getParameters().put('Id2',objgd1.Id);
        GrantMakingGuidance objguidance1 = new GrantMakingGuidance();
        }
       
        User user3 = new User(Alias = 'stan', Email='standarduser122@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Tes', LanguageLocaleKey='es', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='standardus12231@testorg.com');
        System.runAs(user3) {
        PageReference pageRef2 = Page.GrantMakingGuidance;
        Test.setCurrentPage(pageRef2);
        ApexPages.currentPage().getParameters().put('Id',objgd.Id);
        ApexPages.currentPage().getParameters().put('Id2',objgd1.Id);
        GrantMakingGuidance objguidance2 = new GrantMakingGuidance();
        }
        
    }
}