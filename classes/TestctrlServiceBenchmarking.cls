/*********************************************************************************
* Test Class: {TestctrlServiceBenchmarking}
*  DateCreated : 07/31/2013
----------------------------------------------------------------------------------
* Purpose/Methods:
* - Used for cover all methods of ctrlServiceBenchmarking.
----------------------------------------------------------------------------------
* History:
* - VERSION  DEVELOPER NAME    DATE            DETAIL FEATURES
     1.0                      07/31/2013      INITIAL DEVELOPMENT
*********************************************************************************/
@isTest
Public Class TestctrlServiceBenchmarking{
    Public static testMethod Void TestctrlServiceBenchmarking(){
        Account objAcc = new Account();
        objAcc.Name = 'Test Acc1';
        objAcc.Short_Name__c = 'Test';
        insert objAcc;
        
        Account objAcc1 = new Account();
        objAcc1.Name = 'Test Acc2';
        objAcc1.Short_Name__c = 'Test1';
        objAcc1.ParentId = objAcc.Id;
        insert objAcc1;
        
        Contact objCon = new Contact();
        objCon.LastName = 'Test LName';
        objCon.AccountId = objAcc.Id;
        insert objCon;
        
        Country__c objCountry = new Country__c();
        objCountry.Name = 'Test Country1';
        objCountry.Region_Name__c = 'Test Region1';
        objCountry.Used_for_LFA_Services__c = TRUE;
        insert objCountry;
        
        LFA_Work_Plan__c objWp = new LFA_Work_Plan__c();
        objWp.Name = 'Test Wp';
        objWp.LFA__c = objAcc.Id;
        objWp.Country__c = objCountry.id;
        objWP.Include_in_Library__c = TRUE;
        insert objWp;
        
        Grant__c objGrant = new Grant__c();
        objGrant.Name = 'Test Grant';
        objGrant.Country__c = objCountry.id;
        objGrant.Principal_Recipient__c = objAcc.id;
        objGrant.Financial_and_Fiduciary_Risks__c = 'Low';
        objGrant.Health_Services_and_Products_Risks__c = 'High';
        objGrant.Programmatic_and_Performance_Risks__c = 'Medium';
        insert objGrant;
        
        LFA_Service__c objService = new LFA_Service__c();
        objService.Name = 'PUDR1';
        objService.LFA_Work_Plan__c = objWp.id;
        objService.Service_Type__c = 'Key Services';
        objService.Service_Sub_Type__c = 'PUDR1';
        objService.Grant__c = objGrant.Id;
        insert objService;
        
        LFA_Role__c objRole = new LFA_Role__c();
        objRole.Name = 'Test Role3';
        insert objRole;
        
        LFA_Resource__c objResource = new LFA_Resource__c();
        //objResource.Name = 'Test Resource';
        objResource.LFA_Service__c = objService.id;
        objResource.Rate__c = 1;
        objResource.LFA_Role__c = objRole.Id;
        objResource.LOE__c = 1;
        objResource.CT_Planned_Cost__c = 1;
        objResource.CT_Planned_LOE__c = 1;
        objResource.TGF_Proposed_Cost__c = 1;
        objResource.TGF_Proposed_LOE__c = 1;
        insert objResource;
        
        Rate__c objRate = new Rate__c();
        objRate.LFA_Role__c = objRole.Id;
        objRate.Contact__c = objCon.Id;
        objRate.Country__c = objCountry.Id;
        objRate.Rate__c = 1;
        objRate.Active__c = true;
        insert objRate;
        
        Apexpages.currentpage().getparameters().put('id',objService.id);
        ApexPages.StandardController sc = new ApexPages.StandardController(objService);
        ctrlServiceBenchmarking objCls = new ctrlServiceBenchmarking(sc);
        objCls.clearFilter();
        objCls.ReturnToMainPage();
        
        objCls.SelectedServiceSubType = 'Test';
        objCls.SelectedCountry = 'Test';
        objCls.SelectedRegion = 'Test';
        objCls.SelectedYear = '2014';
        objCls.SelectedFinancialRisk = 'Test';
        objCls.SelectedHealthRisk = 'Test';
        objCls.SelectedProgramaticRisk = 'Test';
        objCls.SelectedLFA = 'Test';
        Apexpages.currentpage().getparameters().put('ServiceIndex','0');
        objCls.ViewReport();
        
        Apexpages.currentpage().getparameters().put('ChangeColorValue','Financial & Fiduciary Risk');
        objCls.ChangeColor();
        objCls.RemoveColor();
        
        Apexpages.currentpage().getparameters().put('ChangeColorValue','Health Services & Products Risk');
        objCls.ChangeColor();
        objCls.RemoveColor();
        
        Apexpages.currentpage().getparameters().put('ChangeColorValue','Programmatic & Performance Risk');
        objCls.ChangeColor();
        objCls.RemoveColor();
        
        objCls.SelectedYear = '2014';
        objCls.AnalyseSearch();
    }
}