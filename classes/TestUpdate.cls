@isTest
public class TestUpdate {    
    Public static testMethod void TestUpdateGMIP(){
        Account objAcc = TestClassHelper.insertAccount();
       
        Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
        insert objGrant;
        
        Concept_Note__c objCN = TestClassHelper.createCN();
        objCN.CCM_New__c = objAcc.id;
        insert objCN;
        
        Map<id, Implementation_Period__c> implMap = new Map<id, Implementation_Period__c>();
        
        List<Implementation_Period__c> newimplst = new List<Implementation_Period__c>();
        List<Implementation_Period__c> implstnew = new List<Implementation_Period__c>();
        
        Implementation_Period__c objIP = TestClassHelper.createIP(objGrant,objAcc);
        //objIP.Approval_Status__c = 'Approved';
        //objIP.CT_Finance_Officer__c = u.Id;
        objIP.Status__c = 'Concept Note';
        objIP.Name = 'test';
        
        insert objIP;
        system.debug('@@@1'+objIP);
        implMap.put(objIP.Id, objIP);
        List<Implementation_Period__c> lstimp  = new List<Implementation_Period__c>();
        
        Implementation_Period__c objIP1 = [Select name,Status__c from Implementation_Period__c limit 1];
        objIP1.Name = 'testing';
        objIP1.Status__c = 'Grant-Making';
        update objIP1;
        
        /*objIP.Name = 'testing';
        objIP.Status__c = 'Grant-Making';
        update objIP1;
        
        objIP.Name = 'testin';
        objIP.Status__c = 'Grant-Making';
        update objIP;
        
        lstimp.add(objIP);
        ImplementationPeriodTriggerHandler.UpdateGMIP(lstimp,implMap); */      
         
        system.debug('@@@'+objIP);
        system.debug('@@@'+objIP.Status__c);
        
        //ImplementationPeriodTriggerHandler.UpdateGMIP(newimplst, implMap);
        Test.startTest();
        Performance_Framework__c objPF = TestClassHelper.createPF(objIP);
        objPF.PF_Status__c = 'Accepted';
        insert objPF;
        
        HPC_Framework__c objHPC = TestClassHelper.createHPC();
        objHPC.Grant_Implementation_Period__c = objIP.Id;
        objHPC.HPC_Status__c = 'Accepted';
        insert objHPC;
        
        IP_Detail_Information__c objDB = TestClassHelper.createIPDetail();
        objDB.Implementation_Period__c =  objIP.Id;
        objDB.Budget_Status__c = 'Accepted';
        insert objDB;
        Test.stoptest();
        

    }
    
  }