@isTest
public class TestctrlGrantMaking{
    public static testMethod void TestctrlGrantMaking(){
        
        User objUser = [Select ID, country from User where id=:userinfo.getUserId()];
        objUser.country = 'Test';
        update objUser;
        
        Country__c objCon = new Country__c();
        objCon.Name = objUser.Country;
        insert objCon;
        
        Contact objContactEs = TestClassHelper.insertContact();
        Contact objContactRu = TestClassHelper.insertContact();
        Contact objContactFr = TestClassHelper.insertContact();
        
        //To cover scenario with different language
        
            List<Profile> objProfile = [select id from profile WHERE name in ('PR Admin', 'PR Read Only') Limit 1]; 
            
            User objEsUser = new User(alias = 'u1', email='eS@testorg.com',
            emailencodingkey='UTF-8', lastname='TestingEs', languagelocalekey='es',
            localesidkey='en_US', profileid = objProfile[0].Id, country = objCon.Id, CommunityNickname = 'u1',contactId = objContactEs.Id,
            timezonesidkey='America/Los_Angeles', username='eS@testorg.com');
            insert objEsUser;        
            
            User objRuUser = new User(alias = 'u2', email='rU@testorg.com',
            emailencodingkey='UTF-8', lastname='TestingRu', languagelocalekey='ru',
            localesidkey='en_US', profileid = objProfile[0].Id, country = objCon.Id, CommunityNickname = 'u2',contactId = objContactRu.Id,
            timezonesidkey='America/Los_Angeles', username='rU@testorg.com');
            insert objRuUser; 
        
            User objFrUser = new User(alias = 'u3', email='fR@testorg.com',
            emailencodingkey='UTF-8', lastname='TestingFr', languagelocalekey='fr',
            localesidkey='en_US', profileid = objProfile[0].Id, country = objCon.Id, CommunityNickname = 'u3',contactId = objContactFr.Id,
            timezonesidkey='America/Los_Angeles', username='fR@testorg.com');
            insert objFrUser;
            
        Country__c objCon1 = new Country__c();
        
        Guidance__c objGuidance = new Guidance__c();
        objGuidance.Name = 'Grant-Making Home';
        insert objGuidance;
        test.starttest();
        Account objAcc = TestClassHelper.createAccount();
        objAcc.Country__c = objCon.Id ;
        insert objAcc;     
        
        Grant__c objGrant = TestClassHelper.createGrant(objAcc);
        objGrant.Grant_Status__c = 'Not yet submitted';
        insert objGrant;
        
        Implementation_Period__c objIP = TestClassHelper.createIP(objGrant,objAcc);
   //     objIP.Status__c =  'Grant-Making';
        objIP.Status__c =  'Concept Note';
        objIP.CurrencyIsoCode='USD';
        objIP.Grant_Making_submission_date__c=Date.today().addYears(3);
        objIP.High_level_budget_GAC_1_EUR__c = 1000.00;
        objIP.High_level_budget_GAC_1_USD__c = 10002.99;
        objIP.High_level_budget_TRP_USD__c= 10003.77;
        objIP.High_level_budget_TRP_EUR__c= 10003.67;
        insert objIP;
        
        Profile_Access_Setting__c objProfCust = TestClassHelper.createProfileSetting();
        objProfCust.Page_Name__c ='GrantMakingHome';
        objProfCust.Salesforce_Item__c = 'External Profile';
        
        insert objProfCust;
        
        Apexpages.currentpage().getparameters().put('id',objCon.Id);       
        ApexPages.StandardController sc = new ApexPages.StandardController(objCon);
        ctrlGrantMaking objGMO = new ctrlGrantMaking(sc);
        objGMO.CountryId=objCon.Id;
        objGMO.strCountry = objCon.Name;            
        objGMO.strGuidanceId  = objGuidance.Id;
   //     objGMO.lstIP[0]=objIP;
        objGMO.lstIP.add(objIP);
        objGMO.IPByCountry();
        objGMO.FillCountry(objCon.Name);
        objGMO.SaveChanges();

        ApexPages.StandardController sc1 = new ApexPages.StandardController(objCon1);
        ctrlGrantMaking objGMO1 = new ctrlGrantMaking(sc1);
        
        objGMO1.CountryId=objCon1.Id;
        objGMO1.strCountry = objCon.Name;
            
        objGMO1.strGuidanceId  = objGuidance.Id;
  //      objGMO1.lstIP[0]=objIP;
        objGMO1.lstIP.add(objIP);
        objGMO1.IPByCountry();
        objGMO1.FillCountry(objCon1.Name);
        objGMO1.SaveChanges();
        
        System.RunAs(objRuUser)
        {
         Implementation_Period__c objImpObject = objIP;       
        Apexpages.currentpage().getparameters().put('id',objCon.Id);       
        ApexPages.StandardController scRu = new ApexPages.StandardController(objCon);
        ctrlGrantMaking objGMORu = new ctrlGrantMaking(scRu);
        objGMORu.CountryId=objCon.Id;
        objGMORu.strCountry = objCon.Name;
            
        objGMORu.strGuidanceId  = objGuidance.Id;
        if(objGMORu.lstIP.size()>0)
        {
  //      objGMORu.lstIP[0]=objImpObject;
        objGMORu.lstIP.add(objImpObject);
        }
        objGMORu.checkProfile();
        objGMORu.IPByCountry();
        objGMORu.FillCountry(objCon.Name);
        objGMORu.SaveChanges();
                
        }
        
        System.RunAs(objFrUser)
        {
         Implementation_Period__c objImpObject = objIP;       
        Apexpages.currentpage().getparameters().put('id',objCon.Id);       
        ApexPages.StandardController scFr = new ApexPages.StandardController(objCon);
        ctrlGrantMaking objGMOFr = new ctrlGrantMaking(scFr);
        objGMOFr.CountryId=objCon.Id;
        objGMOFr.strCountry = objCon.Name;
            
        objGMOFr.strGuidanceId  = objGuidance.Id;
        if(objGMOFr.lstIP.size()>0)
        {
       // objGMOFr.lstIP[0]=objImpObject;
        objGMOFr.lstIP.add(objImpObject);
        }
        objGMOFr.checkProfile();
        objGMOFr.IPByCountry();
        objGMOFr.FillCountry(objCon.Name);
        objGMOFr.SaveChanges();
                
        }
        
        System.RunAs(objEsUser)
        {
         Implementation_Period__c objImpObject = objIP;       
        Apexpages.currentpage().getparameters().put('id',objCon.Id);       
        ApexPages.StandardController scEs = new ApexPages.StandardController(objCon);
        ctrlGrantMaking objGMOEs = new ctrlGrantMaking(scEs);
        objGMOEs.CountryId=objCon.Id;
        objGMOEs.strCountry = objCon.Name;
            
        objGMOEs.strGuidanceId  = objGuidance.Id;
        if(objGMOEs.lstIP.size()>0)
        {
       // objGMOEs.lstIP[0]=objImpObject;
          objGMOEs.lstIP.add(objImpObject);
        }
        objGMOEs.checkProfile();
        objGMOEs.IPByCountry();
        objGMOEs.FillCountry(objCon.Name);
        objGMOEs.SaveChanges();
                
        }
     test.stoptest();
    }
    
    
}