/*
* Test class for PRUdateFormExtension
*/
@isTest
public class PRUdateFormExtension_Test {
    public static testMethod void TestPRUdateForm1(){
        
        createData();
        Test.startTest();
        Implementation_Period__c ip = [SELECT Id,Principal_Recipient__c FROM Implementation_Period__c where Principal_Recipient__r.Approval_Status__c =:'Reject' and Principal_Recipient__r.Different_Primary_Contact_Address__c= true];        
        ApexPages.StandardController cltr = new ApexPages.StandardController(ip);
        PRUdateFormExtension con = new PRUdateFormExtension(cltr);
        Pagereference pgExp = con.renderReadOnly();
        Pagereference pg = new Pagereference('/apex/PR_Update_Form_Readonly?id='+ip.Id);
        //System.assert(pgExp.getURL() == pg.getURL());
        pgExp = con.editPrInformation();
        pgExp = con.renderEditable();
        //List<Selectoption> lstContactRole = con.getContactRoleList();
        con.currentPrimary = '0';
        //Test.startTest();
        pgExp = con.updatePrimary();
        Contact other_2 = new Contact(LastName = 'other_2', AccountId = ip.Principal_Recipient__c, FirstName = 'other_2',
                         Email = 'other_2@test.com',Role__c = 'Other');
        con.LFAContact = other_2;
        con.getLFAContactList();
        con.getGrantList();
        //Test.startTest();
        //pgExp = con.updateAccountApprovalStatus();
        //con.submittForApproval();
        //Test.stopTest();
        //con.saveChangesAndRedirect();
        //
        con.attachfile();
        con.checkPhone('1');
        con.deleteRowCon();
        con.showHideAddress();
        Test.stopTest();
    }
    
    public static testMethod void TestPRUdateForm2(){
        Test.startTest();
        createData();
        Implementation_Period__c ip = [SELECT Id,Principal_Recipient__c FROM Implementation_Period__c where Principal_Recipient__r.Approval_Status__c =:'Reject' and Principal_Recipient__r.Different_Primary_Contact_Address__c= true];
        
        PR_Update_Mode__c objPAS = new PR_Update_Mode__c();
         objPAS.Name='1';
         objPAS.Finance_Mode__c = false;
         insert objPAS;
         
        ApexPages.StandardController cltr = new ApexPages.StandardController(ip);
        PRUdateFormExtension con = new PRUdateFormExtension(cltr);
        con.addNewPRContact();
        //con.submittForApproval();
        //con.saveChangesAndRedirect();
        // Clear the Grant Rowes and add new Grant Contacts
        con.selectedIndex = 'grant';
        con.clearPRRow();
        con.selectedIndex = 'dr1';
        con.clearPRRow();
        con.selectedIndex = 'dr2';
        con.clearPRRow();
        //Test.startTest();
        Contact other_3 = new Contact(LastName = 'other_3', AccountId = ip.Principal_Recipient__c, FirstName = 'other_3',
                                 Email = 'other_3@test.com',Role__c = 'other_3');
        insert other_3;
        
        con.setGrantAgreement(other_3);
        // Clear the CCM Rowes and choose new CCM Contacts
        
        con.CCMContactType = 'ccmchair';
        con.CCMContactId = other_3.id;
        con.updateCCMContact();
        con.CCMContactType = 'ccmcivil';
        con.CCMContactId = other_3.id;
        con.updateCCMContact();
        con.CCMContactType = 'grant';
        con.CCMContactId = other_3.id;
        con.updateCCMContact();
        con.CCMContactType = 'request1';
        con.CCMContactId = other_3.id;
        con.updateCCMContact();
        con.CCMContactType = 'request2';
        con.CCMContactId = other_3.id;
        con.updateCCMContact();
        
        List<Attachment> attachments = [select id, name from Attachment where parent.id=:ip.Principal_Recipient__c];
        con.attachmentIdToDelete = attachments[0].Id;
        con.removeRow();
        Test.stopTest();
        con.editPRInformation();
        con.submittForApproval();
        //Test.stopTest();
        //con.saveChangesAndRedirect();
        
    }
    
    public static testMethod void TestPRUdateForm3(){
       Test.startTest();
        createData();
        Implementation_Period__c ip = [SELECT Id,Principal_Recipient__c FROM Implementation_Period__c where Principal_Recipient__r.Approval_Status__c =:'Approved' ];
        Implementation_Period__c implementationPeriod = new Implementation_Period__c(); 
        implementationPeriod.Principal_Recipient__c = ip.Principal_Recipient__c;
        insert implementationPeriod;
        
        Performance_Framework__c objPF = TestClassHelper.createPF(implementationPeriod);
        objPF.PF_Status__c = 'Accepted';
        insert objPF;
        
        HPC_Framework__c objHPC = TestClassHelper.createHPC();
        objHPC.Grant_Implementation_Period__c = implementationPeriod.Id;
        objHPC.HPC_Status__c = 'Accepted';
        insert objHPC;
        
        IP_Detail_Information__c objDB = TestClassHelper.createIPDetail();
        objDB.Implementation_Period__c =  implementationPeriod.Id;
        objDB.Budget_Status__c = 'Accepted';
        insert objDB;
        
        Test.stopTest();
        PR_Update_Mode__c objPAS = new PR_Update_Mode__c();
         objPAS.Name='1';
         objPAS.Finance_Mode__c = true;
         insert objPAS;
         
        ApexPages.StandardController cltr = new ApexPages.StandardController(implementationPeriod);
        PRUdateFormExtension con = new PRUdateFormExtension(cltr);
        con.editPRInformation();
        con.submittForApproval();
        con.updateAccountApprovalStatus();
        con.saveChangesAndRedirect();
        
    }
     public static testMethod void TestPRUdateForm4(){
       Test.startTest();
        createData();
        Implementation_Period__c ip = [SELECT Id,Principal_Recipient__c FROM Implementation_Period__c where Principal_Recipient__r.Approval_Status__c =:'Reject' and Principal_Recipient__r.Different_Primary_Contact_Address__c= false ];
        Implementation_Period__c implementationPeriod = new Implementation_Period__c(); 
        implementationPeriod.Principal_Recipient__c = ip.Principal_Recipient__c;
        insert implementationPeriod;
        
        Performance_Framework__c objPF = TestClassHelper.createPF(implementationPeriod);
        objPF.PF_Status__c = 'Accepted';
        insert objPF;
        
        HPC_Framework__c objHPC = TestClassHelper.createHPC();
        objHPC.Grant_Implementation_Period__c = implementationPeriod.Id;
        objHPC.HPC_Status__c = 'Accepted';
        insert objHPC;
        
        IP_Detail_Information__c objDB = TestClassHelper.createIPDetail();
        objDB.Implementation_Period__c =  implementationPeriod.Id;
        objDB.Budget_Status__c = 'Accepted';
        insert objDB;
        
         
        Test.stopTest();
        PR_Update_Mode__c objPAS = new PR_Update_Mode__c();
         objPAS.Name='1';
         objPAS.Finance_Mode__c = false;
         insert objPAS;
         
        ApexPages.StandardController cltr = new ApexPages.StandardController(implementationPeriod);
        PRUdateFormExtension con = new PRUdateFormExtension(cltr);
        con.editPRInformation();
        con.submittForApproval();
        con.updateAccountApprovalStatus();
        con.saveChangesAndRedirect();
         con.deleteRowCon();
        con.showHideAddress();
        
    }
    
    public static void createData() {
        
        Country__c objCountry = new Country__c();
        objCountry.Name = 'TestCountry';
        //objCountry.CT_Public_Group_ID__c = objGroup.Id;
        insert objCountry;
        
        //Test.startTest();
        Account objAccount = new Account();
        objAccount.Name = 'TestAccount';
        objAccount.Country__c = objCountry.Id;
        objAccount.Approval_Status__c = 'Reject';
        objAccount.Type__c = 'GOV';
        objAccount.Different_Primary_Contact_Address__c = true;
        insert objAccount;
        
        Account objAccount1 = new Account();
        objAccount1.Name = 'TestAccount1';
        objAccount1.Country__c = objCountry.Id;
        objAccount1.Approval_Status__c = 'Approved';
        insert objAccount1;
        
        Account objAccount2 = new Account();
        objAccount2.Name = 'TestAccount2';
        objAccount2.Country__c = objCountry.Id;
        objAccount2.Approval_Status__c = 'Reject';
        objAccount2.Different_Primary_Contact_Address__c = false;
        insert objAccount2;
        
        Grant__c grant = new Grant__c(Name = 'Grant',Principal_Recipient__c = objAccount.id);
        insert grant;
        
         Grant__c grant1 = new Grant__c(Name = 'Grant1',Principal_Recipient__c = objAccount1.id);
        insert grant1;
        
        Grant__c grant2 = new Grant__c(Name = 'Grant2',Principal_Recipient__c = objAccount2.id);
        insert grant2;

        
        List<Contact> lstContact = new List<Contact>();
        Contact c1 = new Contact(LastName = 'c1', AccountId = objAccount.Id, FirstName = 'c1',
                                 Email = 'c1@test.com',Role__c = 'Authorized Signatory for Grant Agreement',Phone='123456');                                 
        Contact c2 = new Contact(LastName = 'c2', AccountId = objAccount.Id, FirstName = 'c2',
                                 Email = 'c2@test.com',Role__c = 'Authorized Signatory for Disbursement Request');
        Contact c3 = new Contact(LastName = 'c1', AccountId = objAccount.Id, FirstName = 'c3',
                                 Email = 'c3@test.com',Role__c = 'Authorized Signatory for Disbursement Request');
        Contact ccm1 = new Contact(LastName = 'ccm1', AccountId = objAccount.Id, FirstName = 'ccm1',
                                 Email = 'ccm1@test.com',Role__c = 'Other');                                 
        Contact ccm2 = new Contact(LastName = 'ccm2', AccountId = objAccount.Id, FirstName = 'ccm2',
                                 Email = 'ccm2@test.com',Role__c = 'Other');
        Contact other1 = new Contact(LastName = 'other1', AccountId = objAccount.Id, FirstName = 'other1',
                                 Email = 'other1@test.com',Role__c = 'Other',Phone='123456',Primary_Contact__c=true);
        Contact other2 = new Contact(LastName = 'other2', AccountId = objAccount.Id, FirstName = 'other2',
                                 Email = 'other2@test.com',Role__c = 'Other');
        lstContact.add(c1);    
        lstContact.add(c2); 
        lstContact.add(c3);
        lstContact.add(ccm1);
        lstContact.add(ccm2);       
        lstContact.add(other1);     
        lstContact.add(other2);
        insert lstContact;
        
        Implementation_Period__c implementationPeriod = new Implementation_Period__c(); 
        implementationPeriod.Principal_Recipient__c = objAccount.Id;
        implementationPeriod.Auth_Sig_for_Grant_Agreement__c = c1.Id;  
        implementationPeriod.Auth_Sig_for_Disbursement_Request_1__c = c2.Id;  
        implementationPeriod.Auth_Sig_for_Disbursement_Request_2__c = c3.Id;  
        implementationPeriod.CCM_Civil_Society_Representative__c = ccm1.Id;  
        implementationPeriod.CCM_Chair__c = ccm2.Id;  
        insert implementationPeriod;
        
        Implementation_Period__c implementationPeriod1 = new Implementation_Period__c(); 
        implementationPeriod1.Principal_Recipient__c = objAccount1.Id;
        implementationPeriod1.Auth_Sig_for_Grant_Agreement__c = c1.Id;  
        implementationPeriod1.Auth_Sig_for_Disbursement_Request_1__c = c2.Id;  
        implementationPeriod1.Auth_Sig_for_Disbursement_Request_2__c = c3.Id;  
        implementationPeriod1.CCM_Civil_Society_Representative__c = ccm1.Id;  
        implementationPeriod1.CCM_Chair__c = ccm2.Id;  
        insert implementationPeriod1;
        
        Implementation_Period__c implementationPeriod2 = new Implementation_Period__c(); 
        implementationPeriod2.Principal_Recipient__c = objAccount2.Id;
        implementationPeriod2.Auth_Sig_for_Grant_Agreement__c = c1.Id;  
        implementationPeriod2.Auth_Sig_for_Disbursement_Request_1__c = c2.Id;  
        implementationPeriod2.Auth_Sig_for_Disbursement_Request_2__c = c3.Id;  
        implementationPeriod2.CCM_Civil_Society_Representative__c = ccm1.Id;  
        implementationPeriod2.CCM_Chair__c = ccm2.Id;  
        insert implementationPeriod2;
        
        //Test.stopTest();
        Attachment attach=new Attachment(); 
            
        attach.Name = 'Unit Test Attachment';
        Blob bodyBlob = Blob.valueOf('Unit Test Attachment Body');
        attach.body = bodyBlob;
        attach.parentId = objAccount.id;
        insert attach;
        
         Performance_Framework__c objPF = TestClassHelper.createPF(implementationPeriod1);
        objPF.PF_Status__c = 'Accepted';
        insert objPF;
        
        HPC_Framework__c objHPC = TestClassHelper.createHPC();
        objHPC.Grant_Implementation_Period__c = implementationPeriod1.Id;
        objHPC.HPC_Status__c = 'Accepted';
        insert objHPC;
        
        IP_Detail_Information__c objDB = TestClassHelper.createIPDetail();
        objDB.Implementation_Period__c =  implementationPeriod1.Id;
        objDB.Budget_Status__c = 'Accepted';
        insert objDB;
       
    }
}