@IsTest
public with sharing class WPTMKeyActivityHandler_Test
{
    static testmethod void testmethod_A(){ 
        Account tAcc=TestClassHelper.createAccount();
        insert tAcc;
        
        Grant__c tGrant=TestClasshelper.createGrant(tAcc);
        insert tGrant;
        
        Implementation_Period__c tImpPeriod=TestClassHelper.createIP(tGrant,tAcc);
        insert tImpPeriod;
        
        Grant_Intervention__c tGi=TestClassHelper.createGrantIntervention(tImpPeriod);
        insert tGi;
        
        Key_Activity__c tKact = new Key_Activity__c();
        tKact.Activity_Description__c = 'Test';
        tKact.Grant_Intervention__c = tGi.id;           
        insert tKact;
          
        Key_Activity__c tKact2 = new Key_Activity__c();
        tKact2.Activity_Description__c = 'Test2';
        tKact2.Grant_Intervention__c = tGi.id;        
        insert tKact2;
        
        //SELECT Criteria__c,Id,Key_Activity__c,Linked_To__c,MilestoneTitle__c,Name,Reporting_Period__c FROM Milestone_Target__c
        Milestone_Target__c tMtc=new Milestone_Target__c();
        tMtc.Key_Activity__c=tKact.id;
        tMtc.MilestoneTitle__c='TestTitle';
        insert tMtc;       
        
        Test.startTest(); 
        List<Key_Activity__c> lstKeyActivity=[SELECT Global_Fund_Comments_to_LFA__c,Global_Fund_Comments_to_PR__c,Grant_Implementation_Period__c,Grant_Intervention__c,Id,LFA_Comments__c,Performance_Framework__c,PF_Status__c,PR_Comments__c,RecordTypeId FROM Key_Activity__c];
        //WPTM_MilestoneRPTriggerHandler WPTMTriggerHandler = new WPTM_MilestoneRPTriggerHandler();
        WPTMKeyActivityHandler.restrictLFAUser(lstKeyActivity);
        Test.stopTest();
        
    }
}