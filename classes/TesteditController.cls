/*********************************************************************************
* Test class:   TesteditController
  Class: editController 
*  DateCreated : 22/11/2014
----------------------------------------------------------------------------------
* Purpose/Methods:
* - To test editController class 
----------------------------------------------------------------------------------
* History:
* - VERSION  DEVELOPER NAME    DATE            DETAIL FEATURES
     1.0        TCS           22/11/2014      INITIAL DEVELOPMENT
*********************************************************************************/ 

@isTest
public class TesteditController {

public static testMethod void grantIndRedirect() {

    test.startTest();
Account objAcc = TestClassHelper.insertAccount();

Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
insert objGrant;

Implementation_Period__c objIP = TestClassHelper.createIP(objGrant, objAcc);
insert objIP;
    
Performance_Framework__c objPF = TestClassHelper.createPF(objIP);
objPF.PF_Status__c = Label.IP_Return_back_to_PR;
insert objPF;

Grant_Indicator__c objGI = TestClassHelper.insertGrantIndicator();
objGI.Performance_Framework__c = objPF.Id;
update objGI;
    
     Profile_Access_Setting__c objPAGInd = TestClassHelper.createProfileSetting();
     objPAGInd.Profile_Name__c = 'System Administrator';//
     objPAGInd.Salesforce_Item__c = 'Edit';
     objPAGInd.Status__c =  Label.IP_Return_back_to_PR;
     objPAGInd.Page_name__c = 'Grant_Indicator__c';
     insert objPAGInd;
test.stopTest();    
    ApexPages.currentPage().getParameters().put('id', objGI.Id);
    ApexPages.StandardController scgint = new ApexPages.StandardController(objGI);
    editController objECGInd = new editController(scgint);
    objECGInd.redirect();

}
public static testMethod void grantIndRedirectCT() {

test.startTest();

Account objAcc = TestClassHelper.insertAccount();
Country__c cntry = TestClassHelper.createCountry();
cntry.LFA__c = objAcc.Id;
Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
objGrant.Country__c = cntry.id;
insert objGrant;

Implementation_Period__c objIP = TestClassHelper.createIP(objGrant, objAcc);
insert objIP;
    
Performance_Framework__c objPF = TestClassHelper.createPF(objIP);
objPF.PF_Status__c = Label.IP_Return_back_to_PR;
insert objPF;

User u = TestClassHelper.createExtUser();
insert u;
npe5__Affiliation__c sampleAff = new  npe5__Affiliation__c(Access_Level__c='Read',npe5__Contact__c=u.contactId,npe5__Organization__c = objAcc.Id,npe5__Status__c='Current');
insert sampleAff;
    System.runAs(u)
    {        
        Grant_Indicator__c objGI = TestClassHelper.insertGrantIndicator();
        objGI.Performance_Framework__c = objPF.Id;
        update objGI;
        test.stopTest();    
        ApexPages.currentPage().getParameters().put('id', objGI.Id);
        ApexPages.StandardController scgint = new ApexPages.StandardController(objGI);
        editController objECGInd = new editController(scgint);
        objECGInd.redirect();
    }
}
public static testMethod void grantIndRedirectExt() {

test.startTest();
User u = TestClassHelper.insertExtLFAUser();
System.runAs(u)
{
Account objAcc = TestClassHelper.insertAccount();

Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
insert objGrant;

Implementation_Period__c objIP = TestClassHelper.createIP(objGrant, objAcc);
insert objIP;
    
Performance_Framework__c objPF = TestClassHelper.createPF(objIP);
objPF.PF_Status__c = Label.IP_Return_back_to_PR;
insert objPF;

Grant_Indicator__c objGI = TestClassHelper.insertGrantIndicator();
objGI.Performance_Framework__c = objPF.Id;
objGI.Grant_Implementation_Period__c = objIP.id;
update objGI;
 
npe5__Affiliation__c sampleAff = new  npe5__Affiliation__c(Access_Level__c='Read',npe5__Contact__c=u.contactId,npe5__Organization__c = objAcc.Id,npe5__Status__c='Current');
insert sampleAff;

EditDeleteControllerbyAffiliation__c  edca = new EditDeleteControllerbyAffiliation__c(Name ='Grant_Indicator__c',Imp_Period_Lkup_APIName__c='Grant_Implementation_Period__c');
insert edca;    
     Profile_Access_Setting__c objPAGInd = TestClassHelper.createProfileSetting();
     objPAGInd.Profile_Name__c = 'System Administrator';//
     objPAGInd.Salesforce_Item__c = 'Edit';
     objPAGInd.Status__c =  Label.IP_Return_back_to_PR;
     objPAGInd.Page_name__c = 'Grant_Indicator__c';
     insert objPAGInd;
test.stopTest();    
    ApexPages.currentPage().getParameters().put('id', objGI.Id);
    ApexPages.currentPage().getParameters().put('retUrl', objGI.Id);
    ApexPages.StandardController scgint = new ApexPages.StandardController(objGI);
    editController objECGInd = new editController(scgint);
    objECGInd.redirect();
  }
}
public static testMethod void goalObjectiveRedirect() {
test.startTest();
Account objAcc = TestClassHelper.insertAccount();

Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
insert objGrant;

Implementation_Period__c objIP = TestClassHelper.createIP(objGrant, objAcc);
insert objIP;
    
Performance_Framework__c objPF = TestClassHelper.createPF(objIP);
objPF.PF_Status__c = Label.IP_Return_back_to_PR;
insert objPF;

Goals_Objectives__c objGO = TestClassHelper.createGoalsObjectives();
objGO.Performance_Framework__c = objPF.Id;
insert objGO ;
     
     Profile_Access_Setting__c objPASGO = TestClassHelper.createProfileSetting();
     objPASGO.Profile_Name__c = 'System Administrator';//
     objPASGO.Salesforce_Item__c = 'Edit';
     objPASGO.Status__c =  Label.IP_Return_back_to_PR;
     objPASGO.Page_name__c = 'Goals_Objectives__c';
     insert objPASGO;
test.stopTest();
    ApexPages.currentPage().getParameters().put('id', objGO.Id);
    ApexPages.StandardController scGO = new ApexPages.StandardController(objGO);
    editController objECGo = new editController(scGO);
    objECGo.redirect();

    }
public static testMethod void IndGoalJnxRedirect() {
test.startTest();
Account objAcc = TestClassHelper.insertAccount();

Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
insert objGrant;

Implementation_Period__c objIP = TestClassHelper.createIP(objGrant, objAcc);
insert objIP;

Performance_Framework__c objPF = TestClassHelper.createPF(objIP);
objPF.PF_Status__c = Label.IP_Return_back_to_PR;
insert objPF;

Grant_Indicator__c objGI = TestClassHelper.insertGrantIndicator();
update objGI;

Goals_Objectives__c objGO = TestClassHelper.createGoalsObjectives();
objGO.Performance_Framework__c = objPF.Id;
insert objGO ;

Ind_Goal_Jxn__c objJxn = TestClassHelper.insertIndicatorGoalJxn(objGO , objGI);
    
	Profile_Access_Setting__c objPAS = TestClassHelper.createProfileSetting();
     objPAS.Profile_Name__c = 'System Administrator';//'System Administrator'
     objPAS.Salesforce_Item__c = 'Edit';
     objPAS.Status__c =  Label.IP_Return_back_to_PR;
     objPAS.Page_name__c = 'Ind_Goal_Jxn__c';
     insert objPAS;
test.stopTest();
ApexPages.currentPage().getParameters().put('id', objJxn.Id);
ApexPages.StandardController sc = new ApexPages.StandardController(objJxn);
editController objEC = new editController(sc);
objEC.redirect();

}
public static testMethod void grantIntRedirect() {
test.startTest();
Account objAcc = TestClassHelper.insertAccount();

Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
insert objGrant;

Implementation_Period__c objIP = TestClassHelper.createIP(objGrant, objAcc);
insert objIP;

Performance_Framework__c objPF = TestClassHelper.createPF(objIP);
objPF.PF_Status__c = Label.IP_Return_back_to_PR;
insert objPF;

Grant_Intervention__c objGI = TestClassHelper.createGrantIntervention(objIP);
objGI.Performance_Framework__c = objPF.Id;
insert objGI ;

	Profile_Access_Setting__c objGin = TestClassHelper.createProfileSetting();
     objGin.Profile_Name__c = 'System Administrator';
     objGin.Salesforce_Item__c = 'Edit';
     objGin.Status__c =  Label.IP_Return_back_to_PR;
     objGin.Page_name__c = 'Grant_Intervention__c';
     insert objGin;
test.stopTest();
    ApexPages.currentPage().getParameters().put('id', objGI.Id);
    ApexPages.currentPage().getParameters().put('retUrl', objGI.Id);
    ApexPages.StandardController scPF = new ApexPages.StandardController(objGI);
    editController objECPF = new editController(scPF);
    objECPf.redirect();
    }
public static testMethod void grantIntRedirectWithoutCS() {
test.startTest();
Account objAcc = TestClassHelper.insertAccount();

Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
insert objGrant;

Implementation_Period__c objIP = TestClassHelper.createIP(objGrant, objAcc);
insert objIP;

Performance_Framework__c objPF = TestClassHelper.createPF(objIP);
objPF.PF_Status__c = Label.IP_Return_back_to_PR;
insert objPF;

Grant_Intervention__c objGI = TestClassHelper.createGrantIntervention(objIP);
objGI.Performance_Framework__c = objPF.Id;
insert objGI ;
test.stopTest();
    ApexPages.currentPage().getParameters().put('id', objGI.Id);
    ApexPages.currentPage().getParameters().put('retUrl', objGI.Id);
    ApexPages.StandardController scPF = new ApexPages.StandardController(objGI);
    editController objECPF = new editController(scPF);
    objECPf.redirect();
    }
public static testMethod void grantIntRedirectWithCI() {
test.startTest();
Account objAcc = TestClassHelper.insertAccount();

Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
insert objGrant;

Implementation_Period__c objIP = TestClassHelper.createIP(objGrant, objAcc);
insert objIP;

Performance_Framework__c objPF = TestClassHelper.createPF(objIP);
objPF.PF_Status__c = Label.IP_Return_back_to_PR;
insert objPF;

Grant_Intervention__c objGI = TestClassHelper.createGrantIntervention(objIP);
objGI.Performance_Framework__c = objPF.Id;
objGI.Custom_Intervention_Name__c = 'test';
insert objGI ;
test.stopTest();
    ApexPages.currentPage().getParameters().put('id', objGI.Id);
    ApexPages.currentPage().getParameters().put('retUrl', objGI.Id);
    ApexPages.StandardController scPF = new ApexPages.StandardController(objGI);
    editController objECPF = new editController(scPF);
    objECPf.redirect();
    }
public static testMethod void grantMultCountryRedirect() {
test.startTest();
Country__c cntry = TestClassHelper.insertCountry();
Account objAcc = TestClassHelper.insertAccount();

Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
insert objGrant;

Implementation_Period__c objIP = TestClassHelper.createIP(objGrant, objAcc);
insert objIP;

Performance_Framework__c objPF = TestClassHelper.createPF(objIP);
objPF.PF_Status__c = Label.IP_Return_back_to_PR;
insert objPF;
    
Grant_Multi_Country__c objgmC = TestClassHelper.createGrntMultiCountry(cntry.id,objIP.id);
objgmC.Grant_Implementation_Period__c = objIP.id;
insert objgmc;
	Profile_Access_Setting__c objPAGmc = TestClassHelper.createProfileSetting();
     objPAGmc.Profile_Name__c = 'System Administrator';//'System Administrator'
     objPAGmc.Salesforce_Item__c = 'Edit';
     objPAGmc.Status__c =  Label.IP_Return_back_to_PR;
     objPAGmc.Page_name__c = 'Grant_Multi_Country__c';
insert objPAGmc;
test.stopTest();
    ApexPages.currentPage().getParameters().put('id', objgmC.Id);
    ApexPages.currentPage().getParameters().put('retUrl', objgmC.Id);
    ApexPages.StandardController scPF = new ApexPages.StandardController(objgmC);
    editController objECPF = new editController(scPF);
    objECPF.status = Label.IP_Return_back_to_PR;
    objECPf.redirect();
    }
public static testMethod void perfFrameRedirect() {
test.startTest();
Account objAcc = TestClassHelper.insertAccount();

Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
insert objGrant;

Implementation_Period__c objIP = TestClassHelper.createIP(objGrant, objAcc);
insert objIP;

Performance_Framework__c objPF = TestClassHelper.createPF(objIP);
objPF.PF_Status__c = Label.IP_Return_back_to_PR;
insert objPF;
        
	Profile_Access_Setting__c objGpf = TestClassHelper.createProfileSetting();
     objGpf.Profile_Name__c = 'System Administrator';//'System Administrator'
     objGpf.Salesforce_Item__c = 'Edit';
     objGpf.Status__c =  Label.IP_Return_back_to_PR;
     objGpf.Page_name__c = 'Performance_Framework__c';
     insert objGpf;
test.stopTest();
    ApexPages.currentPage().getParameters().put('id', objPF.Id);
    ApexPages.currentPage().getParameters().put('retUrl', objPF.Id);
    ApexPages.StandardController scPF = new ApexPages.StandardController(objPF);
    editController objECPF = new editController(scPF);
    objECPf.redirect();
    }
public static testMethod void HPCRedirect() {
test.startTest();
Account objAcc = TestClassHelper.insertAccount();

Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
insert objGrant;

Implementation_Period__c objIP = TestClassHelper.createIP(objGrant, objAcc);
insert objIP;

Performance_Framework__c objPF = TestClassHelper.createPF(objIP);
objPF.PF_Status__c = Label.IP_Return_back_to_PR;
insert objPF;
HPC_Framework__c objHpc = TestClassHelper.createHPC();
objHpc.Status__c = 'Returned to DASH';
insert objHpc;    
	Profile_Access_Setting__c objPAS = TestClassHelper.createProfileSetting();
     objPAS.Profile_Name__c = 'System Administrator';
     objPAS.Salesforce_Item__c = 'Edit';
     objPAS.Status__c =  'Returned to PR';
     objPAS.Page_name__c = 'HPC_Framework__c';
     insert objPAS;
test.stopTest();
ApexPages.currentPage().getParameters().put('id', objHpc.Id);
ApexPages.currentPage().getParameters().put('retUrl', objHpc.Id);
ApexPages.StandardController sc = new ApexPages.StandardController(objHpc);
editController objEC = new editController(sc);
objEC.redirect();
}
public static testMethod void DocRedirect() {
test.startTest();
Account objAcc = TestClassHelper.insertAccount();

Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
insert objGrant;

Implementation_Period__c objIP = TestClassHelper.createIP(objGrant, objAcc);
insert objIP;

Performance_Framework__c objPF = TestClassHelper.createPF(objIP);
objPF.PF_Status__c = Label.IP_Return_back_to_PR;
insert objPF;

EditDeleteControllerbyAffiliation__c  edca = new EditDeleteControllerbyAffiliation__c(Name ='DocumentUpload_GM__c',Imp_Period_Lkup_APIName__c='Implementation_Period__c');
insert edca;
    
User u = TestClassHelper.insertIntUser();
System.runAs(u)
    {
    DocumentUpload_GM__c objDoc = new DocumentUpload_GM__c(Implementation_Period__c=objIP.id);
    insert objDoc;
    test.stopTest();
    
    ApexPages.currentPage().getParameters().put('id', objDoc.Id);
    ApexPages.currentPage().getParameters().put('retUrl', objDoc.Id);
    ApexPages.StandardController sc = new ApexPages.StandardController(objDoc);
    editController objEC = new editController(sc);
    objEC.redirect();
    }
}    
}