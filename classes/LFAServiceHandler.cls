/**
 PUBLIC Class: {LFAServiceHandler}
* Created by {}, {DateCreated 02/25/2014}
* updated by {}, {DateUpdated 03/10/2014}
----------------------------------------------------------------------------------
*Trigger Handler Class For Update All LFA Resource LOE values Based on 
    the LFA Service Change Request To 'Approved'.
*Chatter stamp at approval/rejection of each LFA service approvals process:
    - "LFA Services Change Request" approvals process, is rejected or approved,
      stamp message to chatter feed of LFA service record.
    - "LFA Services Completion" approvals process, is rejected or approved,
      stamp message to chatter feed of LFA service record.
    - "LFA Services Addition" approvals process, is rejected or approved,
      stamp message to chatter feed of LFA service record.
---------------------------------------------------------------------------------
*----------------------------------------------------------------------------------
* History:
* - VERSION  DEVELOPER NAME    DATE            DETAIL FEATURES
*    1.0                    02/25/2014      INITIAL DEVELOPMENT
*/

Public class LFAServiceHandler{
    public static boolean blnStopRecursion = false;
    Public static void UpdateServiceResourceLOE(List<LFA_Service__c> lstLFAServiceNew,Map<Id,LFA_Service__c> mapLFAServiceOld){
        Set<Id> setServiceIds = new Set<Id>();
        Set<Id> setServiceIdsToUpdateTGFAgreed = new Set<Id>();
        Set<Id> setChangeRequestServiceIds = new Set<Id>();
        Set<Id> setCompletionServiceIds = new Set<Id>();
        Set<Id> setAdditionServiceIds = new Set<Id>();
        Set<Id> setCancellationServiceIds = new Set<Id>();
        Set<Id> setServiceToTrackFieldChange = new Set<Id>();
        
        String strConvertedDate = system.now().format('yyyy-MM-dd HH:mm:ss',string.valueof(UserInfo.getTimeZone()));
        
        if(lstLFAServiceNew.size() > 0){
            
            Map<Id,LFA_Service__c> mapServiceForUserProfile = new Map<id,LFA_Service__c>(
                        [Select LastModifiedBy.Profile.Name From LFA_Service__c Where Id IN : lstLFAServiceNew]);
            LFA_Service__c objServiceToUpdateFieldTrack;
            List<LFA_Service__c> lstServiceToUpdateFieldTrack = new List<LFA_Service__c>();
            List<LFA_Service__c> lst = [Select Status__c From LFA_Service__C where Id IN: lstLFAServiceNew];
            
            for(LFA_Service__c objLFAService : lstLFAServiceNew){
                /*if(objLFAService.Change_Request_Approval__c != mapLFAServiceOld.get(objLFAService.Id).Change_Request_Approval__c && objLFAService.Change_Request_Approval__c == 'Approved'){
                    setServiceIds.add(objLFAService.Id);
                }*/
                /*if(objLFAService.Completion_Approval__c != mapLFAServiceOld.get(objLFAService.Id).Completion_Approval__c && objLFAService.Completion_Approval__c == 'Submitted' && objLFAService.Change_Request_Approval__c == null && objLFAService.Addition_Approval__c == null){
                    setServiceIds.add(objLFAService.Id);
                }else if(objLFAService.Change_Request_Approval__c != mapLFAServiceOld.get(objLFAService.Id).Change_Request_Approval__c && objLFAService.Change_Request_Approval__c == 'Submitted' && objLFAService.Completion_Approval__c == null && objLFAService.Addition_Approval__c == null){
                    setServiceIds.add(objLFAService.Id);
                }*/
                //system.debug('#$#$#$#$$#$'+mapServiceForUserProfile.get(objLFAService.id).LastModifiedBy.Profile.Name);
                
                if(objLFAService.Status__c == 'TGF Agreed' && objLFAService.Status__c != mapLFAServiceOld.get(objLFAService.id).Status__c){
                    setServiceIdsToUpdateTGFAgreed.add(objLFAService.Id);
                }
                
                if((mapLFAServiceOld.get(objLFAService.id).Status__c == 'Not Started' || mapLFAServiceOld.get(objLFAService.id).Status__c == 'Ongoing') && 
                    mapServiceForUserProfile.get(objLFAService.id) != null && 
                    mapServiceForUserProfile.get(objLFAService.id).LastModifiedBy.Profile.Name == 'LFA Portal User' && 
                    objLFAService.status__c == 'LFA Change Request'){
                    objServiceToUpdateFieldTrack = new LFA_Service__c(id = objLFAService.id);
                    
                    if(objLFAService.Period_End__c != mapLFAServiceOld.get(objLFAService.id).Period_End__c || 
                        objLFAService.Period_End_Year__c != mapLFAServiceOld.get(objLFAService.id).Period_End_Year__c ||
                        objLFAService.Period_Start__c != mapLFAServiceOld.get(objLFAService.id).Period_Start__c ||
                        objLFAService.Period_Start_Year__c != mapLFAServiceOld.get(objLFAService.id).Period_Start_Year__c){
                        
                        objServiceToUpdateFieldTrack.Programmatic_period_is_changed__c = true;
                        
                    }else{
                        objServiceToUpdateFieldTrack.Programmatic_period_is_changed__c = false;
                    }
                    if(objLFAService.Forecasted_Start_Date__c != mapLFAServiceOld.get(objLFAService.id).Forecasted_Start_Date__c || 
                        objLFAService.Forecasted_End_Date__c != mapLFAServiceOld.get(objLFAService.id).Forecasted_End_Date__c){
                        objServiceToUpdateFieldTrack.Forecasted_service_delivery_is_changed__c = true;
                    }else{
                        objServiceToUpdateFieldTrack.Forecasted_service_delivery_is_changed__c = false;
                    }
                    if(objLFAService.Comments__c != mapLFAServiceOld.get(objLFAService.id).Comments__c){
                        objServiceToUpdateFieldTrack.Service_details_is_changed__c = true;
                    }else{
                        objServiceToUpdateFieldTrack.Service_details_is_changed__c = false;
                    }
                    if(objLFAService.Proposed_LOE__c != objLFAService.LOE__c){
                        objServiceToUpdateFieldTrack.Proposed_LOE_is_changed__c = true;
                    }else{
                        objServiceToUpdateFieldTrack.Proposed_LOE_is_changed__c = false;
                    }
                    lstServiceToUpdateFieldTrack.add(objServiceToUpdateFieldTrack);
                }
                
                if(objLFAService.Change_Request_Approval__c != mapLFAServiceOld.get(objLFAService.Id).Change_Request_Approval__c && (objLFAService.Change_Request_Approval__c == 'Approved' || objLFAService.Change_Request_Approval__c == 'Rejected')){
                    if(objLFAService.Change_Request_Approval__c == 'Approved'){
                        setServiceIds.add(objLFAService.Id);
                    }
                    setChangeRequestServiceIds.add(objLFAService.Id);
                }
                if(objLFAService.cancellation_approval__c != mapLFAServiceOld.get(objLFAService.Id).cancellation_approval__c && (objLFAService.cancellation_approval__c == 'Approved' || objLFAService.cancellation_approval__c == 'Rejected')){
                    setCancellationServiceIds.add(objLFAService.Id);
                }
                if(objLFAService.Completion_Approval__c != mapLFAServiceOld.get(objLFAService.Id).Completion_Approval__c && (objLFAService.Completion_Approval__c == 'Approved' || objLFAService.Completion_Approval__c == 'Rejected')){
                    setCompletionServiceIds.add(objLFAService.Id);
                }
                if(objLFAService.Addition_Approval__c != mapLFAServiceOld.get(objLFAService.Id).Addition_Approval__c && (objLFAService.Addition_Approval__c == 'Approved')){
                    system.debug('########'+objLFAService.Id);
                    setAdditionServiceIds.add(objLFAService.Id);
                    if(objLFAService.Addition_Approval__c == 'Approved'){
                        setServiceIds.add(objLFAService.Id); // Added new for accept TGF offer button
                    }
                }
            }
            if(lstServiceToUpdateFieldTrack.size() > 0) Update lstServiceToUpdateFieldTrack;
        }
        
        if(setServiceIdsToUpdateTGFAgreed.size() > 0){
            List<LFA_Resource__c> lstLFAResourcesTGFAgreed = [Select Id,service_status__c From LFA_Resource__c Where LFA_Service__c IN :setServiceIdsToUpdateTGFAgreed];
            if(lstLFAResourcesTGFAgreed.size() > 0){
                for(LFA_Resource__c objLFAResource : lstLFAResourcesTGFAgreed){
                    objLFAResource.Service_Status__c = 'TGF Agreed';
                }
                update lstLFAResourcesTGFAgreed;
            }
        }
        
        if(setServiceIds.size() > 0){
            List<LFA_Resource__c> lstLFAResources = [Select Id,Proposed_LOE__c,LFA_Proposed_LOE__c,LOE__c,LFA_Service__c,LFA_Service__r.status__c From LFA_Resource__c Where LFA_Service__c IN :setServiceIds];
            if(lstLFAResources.size() > 0){
                for(LFA_Resource__c objLFAResource : lstLFAResources){
                    /*if(objLFAResource.LFA_Service__r.status__c == 'LFA Change Request'){
                        objLFAResource.LOE__c = objLFAResource.LFA_Proposed_LOE__c;
                    } else if(objLFAResource.LFA_Service__r.status__c == 'TGF Counter-Proposal'){
                        //objLFAResource.LOE__c = objLFAResource.CT_Proposed_LOE__c;
                        objLFAResource.LOE__c = objLFAResource.Proposed_LOE__c;
                    }*/
                    objLFAResource.LOE__c = objLFAResource.Proposed_LOE__c;
                    system.debug('##### objLFAResource.LOE__c ###'+objLFAResource.LOE__c);
                }
                system.debug('$%$%$%$%'+lstLFAResources);
                update lstLFAResources;
            }
            /*List<LFA_Service__c> lstService = [Select Id,Status__c From LFA_Service__c Where Completion_Approval__c = 'Submitted'];
            if(lstService.size() > 0){
                for(LFA_Service__c obj : lstService){
                    obj.Status__c = 'Completed';
                }
                update lstService;
            } */
        }
        
        if(setChangeRequestServiceIds.size() > 0){
            List<User> lstuser = [Select Firstname,Lastname from User where id=: UserInfo.getUserId()];
            //String strblob = lstuser[0].Firstname + ' ' + lstuser[0].Lastname + ' ';
            String strblob = '';
            List<LFA_Service__c> lstLFAServices = [Select Id, Name, Change_Request_Approval__c,Change_Request_Rationale__c,
                                                User_submitted_date_time__c,User_submitted_name__c,Actual_LOE__c,Status__c,
                                                User_submission_status__c From LFA_Service__c 
                                                Where Id IN :setChangeRequestServiceIds];
            List<FeedItem> lstFeedItem = new List<FeedItem>();
            List<LFA_Service__c> lstServiceToUpdate = new List<LFA_Service__c>();
            LFA_Service__c objServiceToUpdate;
            
            if(lstLFAServices.size() > 0){
                for(LFA_Service__c objService : lstLFAServices){
                    //String TodayDate = String.valueOf(Date.Today());
                    
                    strblob = 'Change request submitted by '+objService.User_submitted_name__c+' on '+objService.User_submitted_date_time__c+' has been '+objService.Change_Request_Approval__c+' by '+lstuser[0].Firstname + ' ' + lstuser[0].Lastname+' on '+strConvertedDate+'. Change request rationale: '+objService.Change_Request_Rationale__c+'.';
                    
                    /*String UserSubmittedChangeRequestName;
                    if(objService.User_Submitted_Change_Request__c != null){
                        UserSubmittedChangeRequestName = objService.User_Submitted_Change_Request__r.Name;
                    }else{
                        UserSubmittedChangeRequestName = '';
                    }
                    String SubmissionDate;
                    if(objService.Submission_Date__c != null){
                        SubmissionDate = String.valueOf(objService.Submission_Date__c);
                    }else{
                        SubmissionDate = '';
                    }
                    strblob += objService.Change_Request_Approval__c + ' Change Request On '+ TodayDate + ' Submitted by ' + UserSubmittedChangeRequestName + ' On ' + SubmissionDate;*/
                    FeedItem document = new FeedItem();
                    document.Body = strblob;
                    document.Visibility = 'AllUsers';
                    document.ParentId = objService.Id;
                    document.Type = 'TextPost';
                    lstFeedItem.add(document);
                    
                    if(objService.Actual_LOE__c != null && objService.Actual_LOE__c > 0){
                        objServiceToUpdate = new LFA_Service__c(id=objService.id,Status__c = 'Ongoing',change_request_rationale__c = null);
                    }else if(objService.User_submission_status__c == 'Ongoing' && (objService.Actual_LOE__c == null || objService.Actual_LOE__c == 0)){
                        objServiceToUpdate = new LFA_Service__c(id=objService.id,Status__c = 'Ongoing',change_request_rationale__c = null);
                    }else{
                        objServiceToUpdate = new LFA_Service__c(id=objService.id,Status__c = 'Not Started',change_request_rationale__c = null);
                    } 
                    lstServiceToUpdate.add(objServiceToUpdate);
                }
                insert lstFeedItem;
                if(lstServiceToUpdate.size() > 0) update lstServiceToUpdate;
            }
        }
        
        if(setCancellationServiceIds.size() > 0){
            List<User> lstuser = [Select Firstname,Lastname from User where id=: UserInfo.getUserId()];
            //String strblob = lstuser[0].Firstname + ' ' + lstuser[0].Lastname + ' ';
            String strblob = '';
            List<LFA_Service__c> lstLFAServices = [Select Id, Name, cancellation_approval__c,service_cancellation_rationale__c ,
                                                User_submitted_date_time__c,User_submitted_name__c,Actual_LOE__c
                                                From LFA_Service__c Where Id IN :setCancellationServiceIds];
            List<FeedItem> lstFeedItem = new List<FeedItem>();
            List<LFA_Service__c> lstServiceToUpdate = new List<LFA_Service__c>();
            LFA_Service__c objServiceToUpdate;
            if(lstLFAServices.size() > 0){
                for(LFA_Service__c objService : lstLFAServices){
                    //String TodayDate = String.valueOf(Date.Today());
                    
                    strblob = 'Service cancellation submitted by '+lstLFAServices[0].User_submitted_name__c +' on '+lstLFAServices[0].User_submitted_date_time__c+' has been '+objService.cancellation_approval__c+' by '+lstuser[0].Firstname + ' ' + lstuser[0].Lastname+' on '+strConvertedDate+'. service cancellation rationale: '+objService.service_cancellation_rationale__c +'.';
                    
                    /*String UserSubmittedChangeRequestName;
                    if(objService.User_Submitted_Change_Request__c != null){
                        UserSubmittedChangeRequestName = objService.User_Submitted_Change_Request__r.Name;
                    }else{
                        UserSubmittedChangeRequestName = '';
                    }
                    String SubmissionDate;
                    if(objService.Submission_Date__c != null){
                        SubmissionDate = String.valueOf(objService.Submission_Date__c);
                    }else{
                        SubmissionDate = '';
                    }
                    strblob += objService.Change_Request_Approval__c + ' Change Request On '+ TodayDate + ' Submitted by ' + UserSubmittedChangeRequestName + ' On ' + SubmissionDate;*/
                    FeedItem document = new FeedItem();
                    document.Body = strblob;
                    document.Visibility = 'AllUsers';
                    document.ParentId = objService.Id;
                    document.Type = 'TextPost';
                    lstFeedItem.add(document);
                    if(objService.cancellation_approval__c == 'Rejected'){
                        if(objService.Actual_LOE__c != null && objService.Actual_LOE__c > 0){
                            objServiceToUpdate = new LFA_Service__c(id=objService.id,Status__c = 'Ongoing',service_cancellation_rationale__c  = null);
                        }else{
                            objServiceToUpdate = new LFA_Service__c(id=objService.id,Status__c = 'Not Started',service_cancellation_rationale__c  = null);
                        } 
                        lstServiceToUpdate.add(objServiceToUpdate);
                    }
                }
                insert lstFeedItem;
                if(lstServiceToUpdate.size() > 0) update lstServiceToUpdate;
            }
        }
        
        if(setCompletionServiceIds.size() > 0){
            List<User> lstuser = [Select Firstname,Lastname from User where id=: UserInfo.getUserId()];
            String strblob = '';
            List<LFA_Service__c> lstLFAServices = [Select Id, Name, Completion_Approval__c,
                                                User_submitted_date_time__c,User_submitted_name__c
                                                From LFA_Service__c Where Id IN :setCompletionServiceIds];
            List<FeedItem> lstFeedItem = new List<FeedItem>();
            
            if(lstLFAServices.size() > 0){
                for(LFA_Service__c objService : lstLFAServices){
                    
                    strblob = 'Service completion submitted by '+objService.User_submitted_name__c+' on '+objService.User_submitted_date_time__c+' has been '+objService.Completion_Approval__c+' by '+lstuser[0].Firstname + ' ' + lstuser[0].Lastname+' on '+strConvertedDate+'.';
                    
                    FeedItem document = new FeedItem();
                    document.Body = strblob;
                    document.Visibility = 'AllUsers';
                    document.ParentId = objService.Id;
                    document.Type = 'TextPost';
                    lstFeedItem.add(document);
                    
                }
                insert lstFeedItem;
                
            }
        }
        
        if(setAdditionServiceIds.size() > 0){
            List<User> lstuser = [Select Firstname,Lastname From User where id =: UserInfo.getUserId()];
            String strblob = '';
            
            List<LFA_Service__c> lstLFAServices = [Select Id, Name, Addition_Approval__c, 
                            User_submitted_date_time__c,User_submitted_name__c,
                            Submission_Date__c From LFA_Service__c Where Id IN :setAdditionServiceIds];
            List<FeedItem> lstFeedItem = new List<FeedItem>();
            if(lstLFAServices.size() > 0){
                for(LFA_Service__c objService : lstLFAServices){
                    strblob = 'Service addition submitted by '+objService.User_submitted_name__c+' on '+objService.User_submitted_date_time__c+' has been '+objService.Addition_Approval__c+' by '+lstuser[0].Firstname + ' ' + lstuser[0].Lastname+' on '+strConvertedDate+'.';
                    
                    FeedItem document = new FeedItem();
                    document.Body = strblob;
                    document.Visibility = 'AllUsers';
                    document.ParentId = objService.Id;
                    document.Type = 'TextPost';
                    lstFeedItem.add(document);
                    
                }
                insert lstFeedItem;
            }
        }
    }
    
    Public static void UpdateServiceResourceStatus(List<LFA_Service__c> lstLFAServiceNew,Map<Id,LFA_Service__c> mapLFAServiceOld){
        Set<Id> setServiceIds = new Set<Id>();
        if(lstLFAServiceNew.size() > 0){
            for(LFA_Service__c objLFAService : lstLFAServiceNew){
                if(objLFAService.Status__c != mapLFAServiceOld.get(objLFAService.Id).Status__c && objLFAService.Status__c == 'Cancelled'){
                    setServiceIds.add(objLFAService.Id);
                }
            }
        }
        
        if(setServiceIds != null && setServiceIds.size() > 0){
            List<LFA_Resource__c> lstServiceResources = [Select Id,Name,Service_Status__c From LFA_Resource__c Where LFA_Service__c IN :setServiceIds];
            if(lstServiceResources != null && lstServiceResources.size() > 0){
                List<LFA_Resource__c> lstUpdateResources = new List<LFA_Resource__c>();
                for(LFA_Resource__c objR :lstServiceResources){
                    objR.Service_Status__c = 'Cancelled';
                    lstUpdateResources.add(objR);
                }
                update lstUpdateResources;
            }
        }
    }
     //THIS IS FIRED ON UPDATE OF A LFA SERVICE RECORD//
    Public static void aiauUpdateLFAService(List<LFA_Service__c> lstLFAServiceNew,
                                               Map<Id,LFA_Service__c> LFAServiceOld) {
        if(lstLFAServiceNew.size() > 0){
        Set<Id> setServiceIds = new Set<Id>();
        if(lstLFAServiceNew.size() > 0){
            for(LFA_Service__c objLFAService : lstLFAServiceNew){                
                    setServiceIds.add(objLFAService.Id);                
            }
        }
                     
            List<LFA_Service__c> lstLS = [Select Id,LFA_Work_Plan__r.Country__r.CT_Public_Group_ID__c,
                                            LFA_Work_Plan__r.LFA__r.ParentId,
                                            LFA_Work_Plan__r.LFA__c,Approver_1__c,Approver_2__c,Approver_3__c,
                                            Approver_4__c,Approver_5__c
                                            From LFA_Service__c Where Id IN : setServiceIds
                                            And LFA_Work_Plan__r.Country__c != null 
                                            And LFA_Work_Plan__r.Country__r.CT_Public_Group_ID__c != null];
            
            Map<Id,Set<Id>> mapGroupIdToUsers = new Map<Id,Set<Id>>();
            Map<String,Set<Id>> mapGroupNameToUsers = new Map<String,Set<Id>>();
            Map<Id,List<Contact>> mapLFAToContacts = new Map<Id,List<Contact>>();
            Map<Id,List<Contact>> mapLFAParentToContacts = new Map<Id,List<Contact>>();
            Set<Id> setLSGroupIds = new Set<Id>();
            Set<Id> setLFA = new Set<Id>();
            if(lstLS.size() > 0){ 
                for(LFA_Service__c objLS : lstLS){
                    if(objLS.LFA_Work_Plan__r.Country__c != null)
                        setLSGroupIds.add(Id.valueof(objLS.LFA_Work_Plan__r.Country__r.CT_Public_Group_ID__c));
                    if(objLS.LFA_Work_Plan__r.LFA__c != null)   
                        setLFA.add(objLS.LFA_Work_Plan__r.LFA__c);
                }
            }
            if(setLSGroupIds.size() > 0){
                List<GroupMember> lstGroupMember = [Select UserOrGroupId,Group.DeveloperName From GroupMember 
                                Where GroupId IN : setLSGroupIds];
                for(GroupMember objGM : lstGroupMember){
                    if(mapGroupIdToUsers.containsKey(objGM.GroupId)){
                        mapGroupIdToUsers.get(objGM.GroupId).add(objGM.UserOrGroupId);
                    }else{
                        mapGroupIdToUsers.put(objGM.GroupId,new Set<Id>{objGM.UserOrGroupId});
                    }
                }
                List<GroupMember> lstGroupMemberExisting = [Select UserOrGroupId,Group.DeveloperName From GroupMember 
                                Where Group.DeveloperName = 'CT_All_PO' 
                                OR Group.DeveloperName = 'CT_All_FPM'];
                for(GroupMember objGM : lstGroupMemberExisting){
                    if(mapGroupNameToUsers.containsKey(objGM.Group.DeveloperName)){
                        mapGroupNameToUsers.get(objGM.Group.DeveloperName).add(objGM.UserOrGroupId);
                    }else{
                        mapGroupNameToUsers.put(objGM.Group.DeveloperName,new Set<Id>{objGM.UserOrGroupId});
                    }
                }
            }
       
            if(lstLS != null && lstLS.size() > 0){
                List<LFA_Service__c> lstLSToUpdate = new List<LFA_Service__c>();
                LFA_Service__c objLSNew;
                 for(LFA_Service__c objLS : lstLS){
                    System.debug('@@@@@@@@@In Main Condition  mapGroupIdToUsers=='+mapGroupIdToUsers+'mapGroupNameToUsers==='+mapGroupNameToUsers);
                    Id LSGroupId = objLS.LFA_Work_Plan__r.Country__r.CT_Public_Group_ID__c;
                    objLSNew = new LFA_Service__c(id = objLS.id,Approver_1__c = null,Approver_2__c = null,Approver_3__c = null,Approver_4__c = null,Approver_5__c = null);
                    String strFinalUserId = null;
                    if(mapGroupIdToUsers.get(LSGroupId) != null && mapGroupIdToUsers.get(LSGroupId).size() > 0){
                        if(mapGroupNameToUsers.get('CT_All_FPM') != null && mapGroupNameToUsers.get('CT_All_FPM').size() > 0){
                            if (mapGroupIdToUsers.get(LSGroupId).size() <  mapGroupNameToUsers.get('CT_All_FPM').size()){
                                Integer count = 0;
                                for(Id UserId : mapGroupIdToUsers.get(LSGroupId)){
                                    if(count == 2) break;
                                    if(mapGroupNameToUsers.get('CT_All_FPM').contains(UserId)){
                                        if(count == 0) objLSNew.Approver_1__c = UserId;
                                        if(count == 1) objLSNew.Approver_2__c = UserId;
                                        strFinalUserId = UserId;
                                        count++;
                                    }
                                }
                            }else{
                                Integer count = 0;
                                for(Id UserId : mapGroupNameToUsers.get('CT_All_FPM')){
                                    if(count == 2) break;
                                    if(mapGroupIdToUsers.get(LSGroupId).contains(UserId)){
                                        if(count == 0) objLSNew.Approver_1__c = UserId;
                                        if(count == 1) objLSNew.Approver_2__c = UserId;
                                        strFinalUserId = UserId;
                                        count++;
                                    }
                                }
                            }
                        }
                        if(mapGroupNameToUsers.get('CT_All_PO') != null && mapGroupNameToUsers.get('CT_All_PO').size() > 0){
                            if (mapGroupIdToUsers.get(LSGroupId).size() <  mapGroupNameToUsers.get('CT_All_PO').size()){
                                Integer count = 0;
                                for(Id UserId : mapGroupIdToUsers.get(LSGroupId)){
                                    if(count == 3) break;
                                    if(mapGroupNameToUsers.get('CT_All_PO').contains(UserId)){
                                        if(count == 0) objLSNew.Approver_3__c = UserId;
                                        if(count == 1) objLSNew.Approver_4__c = UserId;
                                        if(count == 2) objLSNew.Approver_5__c = UserId;
                                        strFinalUserId = UserId;
                                        count++;
                                    }
                                }
                            }else{
                                Integer count = 0;
                                for(Id UserId : mapGroupNameToUsers.get('CT_All_PO')){
                                    if(count == 3) break;
                                    if(mapGroupIdToUsers.get(LSGroupId).contains(UserId)){
                                        if(count == 0) objLSNew.Approver_3__c = UserId;
                                        if(count == 2) objLSNew.Approver_4__c = UserId;
                                        if(count == 1) objLSNew.Approver_5__c = UserId;
                                        system.debug('$#$#$#$'+strFinalUserId);
                                        strFinalUserId = UserId;
                                        system.debug('$#$#$#$'+strFinalUserId);
                                        count++;
                                    }
                                }
                            }
                        } 
                        if(String.isBlank(strFinalUserId) == false){
                            if(objLSNew.Approver_1__c == null) objLSNew.Approver_1__c = strFinalUserId;
                            if(objLSNew.Approver_2__c == null) objLSNew.Approver_2__c = strFinalUserId;
                            if(objLSNew.Approver_3__c == null) objLSNew.Approver_3__c = strFinalUserId;
                            if(objLSNew.Approver_4__c == null) objLSNew.Approver_4__c = strFinalUserId;
                            if(objLSNew.Approver_5__c == null) objLSNew.Approver_5__c = strFinalUserId;
                        }
                        lstLSToUpdate.add(objLSNew);
                        
                    }
                }       
                if(!blnStopRecursion) {
                    blnStopRecursion = true;                
                    if(lstLSToUpdate.size() > 0) update lstLSToUpdate;
                    blnStopRecursion = false;    
                }
                
            }
           
        }
    }
}