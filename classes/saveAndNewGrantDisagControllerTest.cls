/*********************************************************************************
* {Test} Class: {saveAndNewGrantDisagControllerTest}
* Created by {Rahul Kumar},  {DateCreated 19-Nov-2014}
----------------------------------------------------------------------------------
* Purpose/Methods:
* - Used for cover all methods of saveAndNewGrantDisagController Controller.* 
*
----------------------------------------------------------------------------------
* History:
* - VERSION    DEVELOPER NAME     DATE      DETAIL FEATURES
      1.0       Rahul Kumar    19-Nov-2014
*********************************************************************************/
@isTest
public class saveAndNewGrantDisagControllerTest {
    static saveAndNewGrantDisagController ext;
    static saveAndNewGrantDisagController extW;
    static Grant_Disaggregated__c masterObject;
    static Grant_Disaggregated__c masterObjectWithoutGI;
    static PageReference pref;
    static PageReference prefNull;
    static User testUser = TestClassHelper.createExtUser();
    static User testUser1 = TestClassHelper.createExtUser();    
    Public static testMethod Void TestSaveAndNew(){
    
    Contact con = TestClassHelper.createContact();
    con.lastName = 'Test Last Name';
    con.email = '123@email.com';
    con.firstName = '123@email.com';
    insert con;
    
    testUser.username = 'savedisag12345@example.com';
    testUser.contactId = con.id;
    //testUser.email = 'test@abctest.com';
     insert testUser;
      Test.startTest();
            masterObjectWithoutGI =TestClassHelper.createGrantDisaggregatedWithoutGI();
            pref = Page.newGrantDisaggregatedRedirection;
            Test.setCurrentPage(pref);            
            ApexPages.StandardController conW = new ApexPages.StandardController(masterObjectWithoutGI);
            extW = new saveAndNewGrantDisagController(conW);
           
            extW.createNewGDurl();            
            pref.getParameters().put('retUrl','GM%2F');
            Test.setCurrentPage(pref);
            
            System.runAs(testUser) {                                    
                extW.createNewGDurl();            
            }
            pref.getParameters().put('retUrl','?');
            Test.setCurrentPage(pref);
            System.runAs(testUser) {                                    
            extW.createNewGDurl();   
            Test.stopTest();         
        }
            
    }
    
    Public static testMethod Void TestSaveAndNew1(){
         Contact con1 = TestClassHelper.createContact();
        con1.lastName = 'Test1 Last Name';
        con1.email = '1231@email.com';
        con1.firstName = '1123@email.com';
        insert con1;
        testUser1.username = 'savedisag54321@example.com';
        testUser1.contactId = con1.id;
         insert testUser1;
        Test.startTest();
        masterObject = TestClassHelper.createGrantDisaggregated();
        pref = Page.newGrantDisaggregatedRedirection;
        Test.setCurrentPage(pref);
        ApexPages.StandardController con = new ApexPages.StandardController(masterObject);
        ext = new saveAndNewGrantDisagController(con);
        
        ext.createNewGDurl();
        pref.getParameters().put('retUrl','GM%2F');
        Test.setCurrentPage(pref);
        System.runAs(testUser1) {                                    
                ext.createNewGDurl();            
        }
        Test.stopTest();
    }
}