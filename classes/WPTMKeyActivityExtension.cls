//****************************************************************************************
// Purpose     :  This Class is used to create Key Activity from Performance framework page
// Date        :  3 April 2015
// Created By  :  TCS 
//Revision by  :  24 April 2015: F-01155
//**************************************************************************************** 
public class WPTMKeyActivityExtension{
    public Key_Activity__c keyAcc{get; set;}
    public List<selectOption> options {get; set;}
    public List<selectOption> moduleOptions{get; set;}
    public String performanceId{get; set;}
    public Performance_Framework__c  performanceFrameworkRec {get;set;} 
    public WPTMSecurityObject  securityMatrixObj{get;set;}
    public String performanceFrameworkstatus{get;set;}    
    Public Id loggedInUserId;    
    public String profileName{get;set;}
    public boolean blfaprofile{get;set;}
    public String moduleId{get;set;}    
    public WPTMKeyActivityExtension(ApexPages.StandardController controller){
        options= new List<selectOption>(); 
        moduleOptions = new List<selectOption>(); 
        keyAcc = new Key_Activity__c();
        performanceId= ApexPages.currentPage().getParameters().get('Performance');      
        if(performanceId == null){
            keyAcc = (Key_Activity__c)controller.getRecord();
            keyAcc = [select id, Performance_Framework__c,Activity_Description__c,Global_Fund_Comments_to_LFA__c,
                    Global_Fund_Comments_to_PR__c, Grant_Implementation_Period__c, Grant_Intervention__r.name,Grant_Intervention__r.Module__c,
                    LFA_Comments__c, Number_of_Milestones__c, PF_Status__c, PR_Comments__c,Grant_Intervention__r.Module__r.name,Module__c
                    from Key_Activity__c where id=: keyAcc.id];            
            performanceId = keyAcc.Performance_Framework__c;            
        } 
        else{
            performanceFrameworkRec =[SELECT Implementation_Period__r.GIP_Type__c,PF_Status__c FROM Performance_Framework__c 
                                        WHERE id=:performanceId] ;
            performanceFrameworkstatus =performanceFrameworkRec.PF_Status__c;
        }
        loggedInUserId= userinfo.getUserId() ;                            
        Id profileId=userinfo.getProfileId();
        profileName=[Select Id,Name from Profile where Id=:profileId].Name;
        if(profileName=='LFA Portal User'){
            blfaprofile = true;
        }else{
            blfaprofile = false;    
        }
        //moduleId = keyAcc.Grant_Intervention__r.Module__c;
        moduleId = keyAcc.Grant_Intervention__r.Module__r.Name;
       
        showModules();
        showGrantInterventions();  
        if(profileName!=NULL && performanceFrameworkstatus!=NULL){      
            securityMatrixObj = WPTMSecurityObject.XMLDomDocument(profileName,performanceFrameworkstatus); 
        }
    }
    //Fill Modules Pick List
    public void showModules(){
        if(keyAcc.Id==NULL){
            moduleOptions.add(new selectOption('','--Select Module--'));
        }
        for(Module__c moduleRec : Database.Query('select Id,Name  from Module__c where Performance_Framework__c =: performanceId')){
            moduleOptions.add(new selectOption(moduleRec.id, moduleRec.Name));
        } 
    }
    //Fill Grant Intervention List
    public void showGrantInterventions(){
        if(keyAcc.Id!=NULL){
            for(Grant_Intervention__c giObj : [SELECT Id,Name,Custom_Intervention_Name__c,Catalog_Intervention__r.Is_Other_Intervrntion__c from Grant_Intervention__c where Module__c=:keyAcc.Grant_Intervention__r.Module__c] ){
                if(giObj.Catalog_Intervention__r.Is_Other_Intervrntion__c == false){
                    options.add(new selectOption(giObj.id, giObj.Name));
                }else{
                    if(giObj.Custom_Intervention_Name__c!= null){
                        options.add(new selectOption(giObj.id, giObj.Custom_Intervention_Name__c));
                    }
                }
            }
        }         
    }
    public void refreshGrantInterventionList(){       
        system.debug('@@moduleId'+moduleId);
        if(moduleId==NULL||moduleId=='' ||moduleId=='--Select Module--'){
            options = new List<SelectOption>();
        } 
        else{
        options.clear();
        for(Grant_Intervention__c gin:Database.query('SELECT Id,Name, Custom_Intervention_Name__c, Catalog_Intervention__r.Is_Other_Intervrntion__c from Grant_Intervention__c where Module__c=:moduleId LIMIT 50000')){
            if(gin.Catalog_Intervention__r.Is_Other_Intervrntion__c == false){
                options.add(new selectOption(gin.id, gin.Name));                
            }
            else if(gin.Custom_Intervention_Name__c!= null){
                options.add(new selectOption(gin.id, gin.Custom_Intervention_Name__c));
            }
        }
        }
        system.debug('@@in First Line');
        
                  
    }
    public PageReference saveRecord(){     
       system.debug('@@moduleId '+moduleId );
       
        try{  
            if(moduleId == null){
               ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please select Module'));  
               return null;               
                 
           }else{
            Performance_Framework__c prfObj = [select id, PF_status__c from Performance_Framework__c where id=:performanceId];
            Map<string,id> rtypeMap = new Map<string,id>();
            for(RecordType rtObj : [Select Id,Name FROM RecordType WHERE SobjectType='Key_Activity__c']){
                rtypeMap.put(rtObj.Name,rtObj.Id);
            }
                keyAcc.RecordTypeId = rtypeMap.get(prfObj.PF_Status__c);        
                upsert keyAcc;       
                PageReference recDetail = new PageReference('/' + keyAcc.id);
                return recDetail;  
            } 
            
        }    
        catch (Exception ex) {
            ApexPages.addMessages(ex);
            return null;
        }
    }
    public PageReference cancelRecord(){
        PageReference prefDetail;
        if(keyAcc.id!= null){
            prefDetail = new PageReference('/'+keyAcc.id);
        }else{
            prefDetail = new PageReference('/apex/WPTMKeyActivityReport?id='+performanceId);
        }    
        return prefDetail ;
    }
}