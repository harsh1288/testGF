@isTest
(SeeAllData=true)
Public Class SwitchProfileTest {
    Public static testMethod void TestCN_Guidance_New (){
         
         User currentUser = [Select Id,Profile.UserLicenseId,
                       Profile.Name,Username from user 
                       where Profile.Name='System Administrator' limit 1];
         Profile targetProfile = [Select Name,Id from profile where 
                                UserLicenseId =:currentUser.Profile.UserLicenseId and Id !=:currentUser.Profile.Id limit 1];
         PageReference pageRef = Page.SelectProfile;
         Test.setCurrentPage(pageRef);                              
         Switch_User_Profile_Controller switch_user = new Switch_User_Profile_Controller();
         switch_user.initialize();                           
         ApexPages.currentPage().getParameters().put('profileId',targetProfile.Id);
         ApexPages.currentPage().getParameters().put('userId',currentUser.Id);
         switch_user.selectProfile();
         switch_user.initializeUpdate();
             
    }
}