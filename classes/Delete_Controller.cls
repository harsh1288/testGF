public  class Delete_Controller {
    private final Sobject sObj;
    public Id recordId;
    public String returnUrlId;
    public String sObjName;
    public boolean page_redirect;
    public boolean goal_objective;
    public String query;
    public String moduleUrl;
    public Goals_Objectives__c GoalObj;
    public Id profileId;
    public String profileName;
    public String status;
    public String getBackUrl {get; set;}
    public String error {get; set;}
    
    public Delete_Controller(ApexPages.StandardController stdController) {
        profileId=userinfo.getProfileId();
        profileName=[Select Id,Name from Profile where Id=:profileId].Name;
        getBackUrl = null;
        error = '';
        returnUrlId = ApexPages.currentPage().getParameters().get('retUrl');
        recordId = ApexPages.currentPage().getParameters().get('id');
        sObjName = recordId.getsObjectType().getDescribe().getName();
        query = 'SELECT Grant_Status__c,CreatedById FROM ' + sObjName + ' WHERE id =: recordId';
        system.debug('query:'+query);
        sObj = Database.query(query);
        system.debug('Sobject Record:'+sObj);
        system.debug('Sobject Name:'+sObjName);
        status = String.valueOf(sObj.get('Grant_Status__c'));
        system.debug('Sobject Grant Status:'+status);
        if(sObjName == 'Goals_Objectives__c' && (profileName == 'PR Admin' || profileName == 'PR Read Write Edit')){
            GoalObj = [SELECT id, Name, Type__c FROM Goals_Objectives__c WHERE id =: recordId LIMIT 1];
            if(GoalObj.Type__c == 'Goal')
                goal_objective = true;
            else
                goal_objective = false;
            //system.debug('GoalObj:'+GoalObj.id);
        }
        else
            goal_objective = false;
        
        page_redirect = false;
        moduleUrl = null;
        }
    
    public PageReference reDirect() {  
      List<npe5__Affiliation__c> conAffltn = new List<npe5__Affiliation__c>();     
        PageReference deleteAction;
        String baseUrl;
        //Id profileId=userinfo.getProfileId();
        string userTypeStr = UserInfo.getUserType();
        System.debug('>>>user type'+userTypeStr);
        baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
        if(userTypeStr == 'Standard') {
            baseUrl = baseUrl+'/';
        }
        else
        {
        	system.debug('In Else');
            EditDeleteControllerbyAffiliation__c csdata = EditDeleteControllerbyAffiliation__c.getInstance(sObjName);
            if(csdata != null)
            {
                String fldName = csdata.Imp_Period_Lkup_APIName__c;
                System.debug(' ######## '+fldName);
                String fetchimpId = 'Select '+fldName+' from ' +sObjName+ ' where Id = \''+recordId+'\'';
                System.debug('%%%%% '+fetchimpId);
                LIST<SObject> impList = Database.query(fetchimpId);
                String impId='';
                if(impList.size()>0)
                {
                    impId += ( impList[0].get(fldName));                    
                }
            System.debug('#### '+impid);
            String prName = [Select Principal_Recipient__r.Name from Implementation_Period__c where Id =:impId limit 1].Principal_Recipient__r.Name;
            System.debug('##### '+prName);
            Id profileId=userinfo.getProfileId();
            Id usrId=UserInfo.getUserId();
            Id conId =[SELECT ContactId FROM User where Id=:usrId limit 1].ContactId;
            conAffltn =[Select Id from npe5__Affiliation__c where npe5__Status__c='Current' and npe5__Contact__c=:conId and Access_Level__c='Read' and npe5__Organization__r.Name=:prName];
            }
            
            if(profileName.Contains('PR') || profileName.Contains('Applicant/CM') )
            baseUrl = baseUrl+'/GM/';
            else
            baseUrl = baseUrl+'/LFA/';
        }
         boolean childOfPF = false;
         getBackUrl ='';                 
         Map<String, SobjectField> fieldMap = recordId.getsObjectType().getDescribe().Fields.getMap(); 
         if(sObjName != 'Grant_Intervention__c'){
         for(String f :  fieldMap.keySet())
             {
                if(fieldMap.get(f).getDescribe().getName() == 'Performance_Framework__c')
                {
                    childOfPF = true;
                }
             }
         }
         if(childOfPF)
         {
             String fetchpfId = 'Select Performance_Framework__c from ' +sObjName+ ' where Id = \''+recordId+'\'';
             LIST<SObject> sObjectList = Database.query(fetchpfId);
             if(sObjectList.size()>0)
             {
                 getBackUrl += baseUrl+String.valueOf( sObjectList[0].get('Performance_Framework__c') );                    
             }             
             else
             {
                  getBackUrl += baseUrl+'apex/GrantMakingHome?sfdc.tabName=01rb0000000b9g6';
             }
         }
         else
         {
             if(sObjName == 'Grant_Multi_Country__c'){
                 /***Added For Restricting Edit Controller**/
                 String fetchpGMId = 'Select Grant_Implementation_Period__c from ' +sObjName+ ' where Id = \''+recordId+'\'';
                 LIST<SObject> GMsObjectList = Database.query(fetchpGMId);
                 system.debug('Sobject List is greateer than one '+GMsObjectList );
                 if(GMsObjectList.size()>0)
                 {
                     getBackUrl += baseUrl+String.valueOf( GMsObjectList[0].get('Grant_Implementation_Period__c') );                    
                 }
             }
             else if(sObjName == 'Ind_Goal_Jxn__c'){
                     String fetchpfId = 'Select Indicator__c from ' +sObjName+ ' where Id = \''+recordId+'\'';
                     LIST<SObject> sObjectList = Database.query(fetchpfId);
                     if(sObjectList.size()>0)
                     {
                         getBackUrl += baseUrl+String.valueOf( sObjectList[0].get('Indicator__c') );                    
                     }
                     else
                         getBackUrl += baseUrl+'apex/GrantMakingHome?sfdc.tabName=01rb0000000b9g6';
             }
             else if(sObjName == 'Performance_Framework__c'){
                         String fetchpfId = 'Select Implementation_Period__c from ' +sObjName+ ' where Id = \''+recordId+'\'';
                         LIST<SObject> sObjectList = Database.query(fetchpfId);
                         getBackUrl += baseUrl+String.valueOf( sObjectList[0].get('Implementation_Period__c') );
                         //getBackUrl += baseUrl+recordId;
             }
             else if(sObjName == 'Grant_Intervention__c'){
                         String fetchpfId = 'Select Module__c from ' +sObjName+ ' where Id = \''+recordId+'\'';
                         LIST<SObject> sObjectList = Database.query(fetchpfId);
                         getBackUrl += baseUrl+String.valueOf( sObjectList[0].get('Module__c') );
             }   
             else if(sObjName =='DocumentUpload_GM__c') {
             			String fetchpfId = 'Select Implementation_Period__c from ' +sObjName+ ' where Id = \''+recordId+'\'';
                         LIST<SObject> sObjectList = Database.query(fetchpfId);
                         getBackUrl += baseUrl+String.valueOf( sObjectList[0].get('Implementation_Period__c') );
             }        
             else {              
                getBackUrl += baseUrl+'apex/GrantMakingHome?sfdc.tabName=01rb0000000b9g6';
             }
         }
         System.debug('@@@@@@@@@@@@@@' +!(profileName.Contains('PR'))+' $$$$$$ '+!(conAffltn.size()>0));
        if(!(conAffltn.size()>0))
        {
            //baseUrl = 'https://support-theglobalfund.cs20.force.com/GM/';
        //String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
        List<Profile_Access_Setting__c> lstProAcc = new List<Profile_Access_Setting__c>();
        //lstProAcc = Profile_Access_Setting__c.getall().values();
        lstProAcc = [SELECT id, Name, Salesforce_Item__c, Status__c, Page_Name__c, Profile_Id__c, Profile_Name__c 
                     FROM Profile_Access_Setting__c 
                     WHERE Profile_Name__c =: profileName AND Page_Name__c =: sObjName AND Status__c =: status AND Salesforce_Item__c = 'Delete']; /* label.Submitted */
        system.debug('Custom Setting data:'+lstProAcc);
        if(sObjName =='DocumentUpload_GM__c' && profileName != 'Super User' && profileName != 'System Administrator')
        {            
            if(String.valueOf(sObj.get('CreatedById')) != String.valueOf(UserInfo.getUserId()))
            {
                getBackUrl ='';
                system.debug('list size is zero and object is Document Upload');
                system.debug('$$rec '+recordId+' $$ret '+returnUrlId);
                if(returnUrlId.indexOf('/') != -1)
                {
                   returnUrlId = returnUrlId.substring(returnUrlId.lastIndexOf('/')+1,returnUrlId.length()); 
                }
                if(returnUrlId.length()>=15)
                {
                   getBackUrl += baseUrl+returnUrlId; 
                }
                else
                {
                    getBackUrl += baseUrl+recordId;
                }
                ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.Warning,'You are not allowed to delete this record.');
                ApexPages.addMessage(errorMsg);
                page_redirect = false;
            }
            else
            {
                 string docUrl;
                 Id GMId = [Select Implementation_Period__c from DocumentUpload_GM__c where Id =:recordId].Implementation_Period__c;
                 try {
                    delete sObj;
                    docUrl = baseUrl+GMId;
                    deleteAction = new PageReference(docUrl);
                    deleteAction.setRedirect(true);
                    page_redirect = true;
                 }
                 catch(exception e)
                 {
                    System.debug('Error occured '+e);
                    ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.Warning,'Record couldn\'t be deleted.');
                    ApexPages.addMessage(errorMsg);
                    page_redirect = false;
                 }                 
            }
        }

        else if(lstProAcc.size() > 0) {
            system.debug('list size is greater than zero');
            //getBackUrl = baseUrl+recordId;
            System.debug('Base URL: ' + getBackUrl);       
            error = 'You are not allowed to delete the record';            
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.Warning, label.GM_Delete_Message1);
            ApexPages.addMessage(errorMsg);
            page_redirect = false;
            //return null;
        }
        else if(sObjName != 'Implementer__c' && sObjName != 'Module__c'){
             //moduleUrl = baseUrl+recordId+'/e?retUrl=%2F'+returnUrlId+'&nooverride=1';
             //String moduleUrl = baseUrl+recordId+'/e?retUrl=%2F'+recordId;
             //if(goal_objective != true){                 
                 //https://support-theglobalfund.cs20.force.com/GM/apex/GrantMakingHome?sfdc.tabName=01rb0000000b9g6
                 //String navigationURL = baseUrl+ip_pf;
                 System.debug('Base URL1: ' + getBackUrl + ' SObject Name ' +sObjName); 
                 try {
                    delete sObj;
                 }
                 catch (exception e){
                    ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.Warning, label.GM_Delete_Message2);
                    ApexPages.addMessage(errorMsg);
                    page_redirect = false;
                    return NULL;
                 }  
                 deleteAction = new PageReference(getBackUrl);
                 deleteAction.setRedirect(true);
                 page_redirect = true;
                 //return deleteAction;                 
             /*}
             else{                 
                 system.debug('I am in 2nd Else');
                 //getBackUrl = baseUrl+recordId;
                 System.debug('Base URL: ' + getBackUrl);       
                 error = 'You are not allowed to delete the record';            
                 ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.Warning, 'You are not allowed to delete the record!');
                 ApexPages.addMessage(errorMsg);
                 page_redirect = false;
                 //return null;
             }*/
        }        
        else{
            system.debug('In Else Condition');
            if(sObjName == 'Implementer__c'){
                 Implementer__c ip = [Select Id, Implementer_Role__c from Implementer__c where Id =: recordId LIMIT 1];
                 system.debug('Implementer Role:'+ip.Implementer_Role__c);
                 if(ip.Implementer_Role__c != 'Principal Recipient'){
                     try {
                     delete sObj;
                     }
                     catch (exception e){
                        ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.Warning, label.GM_Delete_Message2);
                        ApexPages.addMessage(errorMsg);
                        page_redirect = false;
                        return NULL;
                     }  
                     deleteAction = new PageReference(getBackUrl);
                     deleteAction.setRedirect(true);
                     page_redirect = true;
                 }
                 else{
                     ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.Warning, label.GM_Delete_Message3);
                     ApexPages.addMessage(errorMsg);
                     page_redirect = false;
                 }
             }
             /** PP:23/3/2015: Adding code to restrict modules craeted from CN **/ 
                 else if(sObjName == 'Module__c'){
                 system.debug('In Module');
                 Module__c module = [Select Id, PF_is_Null__c from Module__c where Id =: recordId LIMIT 1];
                 system.debug('Implementer Role:'+module.PF_is_Null__c );
                 try {
                     if(module.PF_is_Null__c == true){
                         delete module;
                      }else{
                         module.Performance_Framework__c = null;
                          update module;
                      }
                  }catch (exception e){
                        ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.Warning, label.GM_Delete_Message2);
                        ApexPages.addMessage(errorMsg);
                        page_redirect = false;
                        return NULL;
                     }  
                     deleteAction = new PageReference(getBackUrl);
                     deleteAction.setRedirect(true);
                     page_redirect = true;
                 }
        }
         } else{
         	ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.Warning, label.GM_Delete_Message2);
            ApexPages.addMessage(errorMsg);
            page_redirect = false;   
            return null;
         }     
        if(page_redirect){
            try {
                return deleteAction;
            }
            catch (exception e) {
                ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.Warning, label.GM_Delete_Message2);
                ApexPages.addMessage(errorMsg);
                page_redirect = false;
                return NULL;
            }
       }
        else 
            return null;
    }
}