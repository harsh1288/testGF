public class WPTMSecurityObject{

    // Pass in the URL for the request
    // For the purposes of this sample,assume that the URL
    // returns the XML shown above in the response body
    public String accessViewWPTM{get;set;}
    public String addActivityMilestone{get;set;}
    public String editActivityMilestone{get;set;}
    public String editTGFCommenttoLFA{get;set;}
    public String editTGFCommenttoPR{get;set;}
    public String editLFAComment{get;set;}
    public String editPRComment{get;set;}

    public  static WPTMSecurityObject XMLDomDocument(string profileName,string pfStatus){
        //Change profile name to lower case and remove spaces between       
        profileName=profileName.toLowerCase();
        profileName = profileName.replaceAll( '\\s+', '');
        profileName = profileName.replaceAll( '&', '');
        profileName = profileName.replaceAll( '/', '');
        //Change performance Framework status to lower case and remove spaces between
        pfStatus=    pfStatus.toLowerCase();
        pfStatus =  pfStatus.replaceAll( '\\s+', '');
        WPTMSecurityObject sobj=new WPTMSecurityObject();
    
        StaticResource docs = [select id, name, body from StaticResource where name = 'WPTMSecuritySetting' limit 1];     
        string xml=docs.body.toString();
    
        Dom.Document doc = new Dom.Document();
        doc.load(xml);
    
    
        Dom.XMLNode firstNode = doc.getRootElement();
        system.debug('@@###ProfileNamepfStatus'+profileName+'and'+pfStatus);
        Dom.XMLNode selectedNode=firstNode.getChildElement(profileName,null).getChildElement(pfStatus,null);
        sobj.accessViewWPTM=selectedNode.getAttributeValue('accessViewWPTM',null);
        sobj.addActivityMilestone=selectedNode.getAttributeValue('addActivityMilestone',null);
        sobj.editActivityMilestone=selectedNode.getAttributeValue('editActivityMilestone',null);
        sobj.editTGFCommenttoLFA=selectedNode.getAttributeValue('editTGFCommenttoLFA',null);
        sobj.editTGFCommenttoPR=selectedNode.getAttributeValue('editTGFCommenttoPR',null);
        sobj.editLFAComment=selectedNode.getAttributeValue('editLFAComment',null);
        sobj.editPRComment=selectedNode.getAttributeValue('editPRComment',null);
        return sobj;
    }
}