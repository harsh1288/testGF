/*********************************************************************************
* Test Class: {TestLFAWorkPlanTrigger }
*  DateCreated : 02-13-2014
----------------------------------------------------------------------------------
* Purpose/Methods:
* - Used for cover all code of TestLFAWorkPlanTrigger .
----------------------------------------------------------------------------------
* History:
* - VERSION  DEVELOPER NAME    DATE            DETAIL FEATURES
     1.0                       02-13-2014     INITIAL DEVELOPMENT
     1.1     TCS(Jaideep Khare - JK01)  06/04/2015    Modified DEVELOPMENT
*********************************************************************************/
@isTest
Public Class TestLFAWorkPlanTrigger {
    Public static testMethod Void TestLFAWorkPlanTrigger() {
        Account objAcc = new Account();
        objAcc.Name = 'Test Acc';
        objAcc.Short_Name__c= 'Test Acc';
        objAcc.Boolean_Duplicate__c = true;  // JK01        
        insert objAcc;
        
        Account objAcc2 = new Account();
        objAcc2.Name = 'Test Acc1';
        objAcc2.Short_Name__c= 'Test Acc1';
        objAcc2.ParentId = objAcc.ID;
        objAcc2.Boolean_Duplicate__c = true;  // JK01       
        insert objAcc2;
        
        Account objAcc3 = new Account();
        objAcc3.Name = 'Test Acc2';
        objAcc3.Short_Name__c= 'Test Acc2';
        objAcc3.ParentId = objAcc2.ID;
        objAcc3.Boolean_Duplicate__c = true;  // JK01
        insert objAcc3;
        
        Account objAcc1 = new Account();
        objAcc1.Name = 'Test Acc3';
        objAcc1.Short_Name__c= 'Test Acc3';
        objAcc1.ParentId = objAcc.ID;
        objAcc1.Boolean_Duplicate__c = true;  // JK01
        insert objAcc1;
        
        Country__c objCountry = new Country__c();
        objCountry.Name ='India';
       // objCountry.CT_Public_Group_ID__c = '00Gb0000000tjtJ';
        insert objCountry ;
       
        Country__c objCountry1 = new Country__c();
        objCountry1.Name ='India1';
        //objCountry1.CT_Public_Group_ID__c = '00Gb0000000tjtJ';
        insert objCountry1 ;
        
        LFA_Work_Plan__c objWp = new LFA_Work_Plan__c();
        objWp.Name = 'Test Wp';
        objWp.LFA__c = objAcc.Id;
        objWp.Country__c = objCountry.id;
        objWP.Include_In_Library__c = TRUE;
        insert objWp;
        LFA_Service__c objService = new LFA_Service__c();
        objService.Name = 'PUDR';
        objService.LFA_Work_Plan__c = objWp.id;
        objService.Service_Type__c = 'Key Services';
        objService.Service_Sub_Type__c = 'PUDR';
        objService.Exclude_from_Library__c=FALSE;        
        insert objService;
        
        LFA_Resource__c objResource = new LFA_Resource__c();
        //objResource.Name = 'Test Resource';
        objResource.LFA_Service__c = objService.id;
        objResource.Rate__c = 1;
        //objResource.LFA_Role__c = objRole1.Id;
        objResource.LOE__c = 1;
        objResource.CT_Planned_Cost__c = 1;
        objResource.CT_Planned_LOE__c = 1;
        objResource.TGF_Proposed_Cost__c = 1;
        objResource.TGF_Proposed_LOE__c = 1;
        insert objResource;
        
        LFA_Service__c objService1 = new LFA_Service__c();
        objService1.Name = 'PUDR';
        objService1.LFA_Work_Plan__c = objWp.id;
        objService1.Service_Type__c = 'Key Services';
        objService1.Service_Sub_Type__c = 'PUDR';
        insert objService1;
        
        LFA_Resource__c objResource1 = new LFA_Resource__c();
        //objResource1.Name = 'Test Resource1';
        objResource1.LFA_Service__c = objService1.id;
        objResource1.Rate__c = 2;
        objResource1.LOE__c = 2;
        objResource1.CT_Planned_Cost__c = 2;
        objResource1.CT_Planned_LOE__c = 2;
        objResource1.TGF_Proposed_Cost__c = 2;
        objResource1.TGF_Proposed_LOE__c = 2;
        insert objResource1;
        
        LFA_Service__c objService2 = new LFA_Service__c();
        objService2.Name = 'PUDR';
        objService2.LFA_Work_Plan__c = objWp.id;
        objService2.Service_Type__c = 'Other LFA Services';
        objService2.Service_Sub_Type__c = 'PUDR';
        insert objService2;
        objWp.Status__c = 'Service Delivery';
        update objWp;
        
        
        LFA_Work_Plan__c objWP2 = new LFA_Work_Plan__c(name='Test WO',LFA__c= objAcc1.ID,LFA_Access__c = 'Read Access',Country__c=objCountry1.ID,Country_Team_can_edit__c=FALSE);
        insert objWP2;
        LFA_Work_Plan__c objWP3 = new LFA_Work_Plan__c(name='Test WO',LFA__c= objAcc3.ID,LFA_Access__c = 'No Access',Country__c=objCountry.ID,Country_Team_can_edit__c=True);
        insert objWP3;
        objWP3.LFA__c= objAcc1.ID;
        objWP3.LFA_Access__c = 'Read Access';
        update objWP3;
    }
}