@isTest
public class UpdateFedIdOnUser_Test {

    static testMethod void updateFedId() {
    
    Profile objProfile = [select id from profile WHERE UserLicense.Name=:'Partner Community Login' Limit 1]; 
    Profile objSystemAdminProfile = [select id from profile WHERE Name = 'System Administrator' Limit 1]; 
    //User objSystemAdminUser = [select id from user WHERE profileid =: objSystemAdminProfile.Id AND isactive =: True Limit 1]; 

    
    //System.runAs(objSystemAdminUser){  
    Test.startTest();
    Account objAccount = TestClassHelper.createAccount();
    objAccount.Name = 'AccountForFedId';
    insert objAccount;
    
    Contact objContact = TestClassHelper.createContact();
    objContact.LastName = 'ContactForFedId';
    objContact.AccountId = objAccount.Id;
    insert objContact;
    
    List<User> lstUsers = new List<User>();
    
    User objUserCRM = new User(alias = 'u1', email='eS@testorg.com',
            emailencodingkey='UTF-8', lastname='TestingEs', languagelocalekey='es',
            localesidkey='en_US', profileid = objProfile.Id, CommunityNickname = 'u1',contactId = objContact.Id,
            timezonesidkey='America/Los_Angeles', username='eS@testorg.com');
    lstUsers.add(objUserCRM);
    //insert objUserCRM;
    
    User objSystemAdminUser = new User(alias = 'SysAdmin', email='AdminforCRMtesting@testorg.com',
            emailencodingkey='UTF-8', lastname='TestingAdmin', languagelocalekey='es',
            localesidkey='en_US', profileid = objSystemAdminProfile.Id, CommunityNickname = 'Admin',
            timezonesidkey='America/Los_Angeles', username='adminforCRMtesting@testorg.com');
    lstUsers.add(objSystemAdminUser);
    //insert objSystemAdminUser;    
        
    if(lstUsers.size()>0){
    insert lstUsers;
    }
    Test.stopTest();
    //}
    
    System.runAs(objSystemAdminUser){  
    objContact.Federation_Id__c = '123498';
    Update objContact;
    }
    
    objUserCRM = [Select id, contactid, FederationIdentifier from User where contactId =: objContact.Id LIMIT 1];
    
    System.assertEquals('123498', objUserCRM.FederationIdentifier);
    
    }
}