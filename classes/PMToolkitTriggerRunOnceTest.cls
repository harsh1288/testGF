@isTest(SeeAllData=false)
private class PMToolkitTriggerRunOnceTest {
    static testMethod void TestAddRemoveCountMethods() {

        //setup test data
        PMToolkitTestDataCreator.dataCreator();
        Project_Overview__c Project1 = [SELECT Id FROM Project_Overview__c WHERE Name = 'Unit Test Project'];
        if(Project1!=Null){
        Work_Product__c userStory1 = [SELECT Id,iteration__r.name FROM Work_Product__c WHERE rank__c=0 AND Project_Overview__c =: Project1.Id];
        Work_Product__c userStory2 = [SELECT Id,iteration__r.name FROM Work_Product__c WHERE rank__c=1 AND Project_Overview__c =: Project1.Id];
        
        //Clear test data from trigger memory
        PMToolkitTriggerRunOnce.resetAlreadyDone();
        Test.startTest();

        //confirm that WP ID is not in the list of items that have run
        system.assertEquals(false, PMToolkitTriggerRunOnce.isCurrentlyRunning(userStory1.Id));
        
        //update WP so it gets set in the list of run items
        userStory1.rank__c = 5;
        update userStory1;

        //confirm that WP.Id is now in the list of run items
       // system.assertEquals(false, PMToolkitTriggerRunOnce.isCurrentlyRunning(userStory1.Id));

        //remove ID from list, confirm 
        PMToolkitTriggerRunOnce.removeCurrentlyRunning(userStory1.Id);
        system.assertEquals(false, PMToolkitTriggerRunOnce.isCurrentlyRunning(userStory1.Id));

        //confirm count of already run list is 0;
        system.assertEquals(0, PMToolkitTriggerRunOnce.countDone());

        userStory1.rank__c = 5;
        update userStory1;

        userStory2.rank__c = 6;
        update userStory2;

        //confirm count of already run is now 2;
       // system.assertEquals(0, PMToolkitTriggerRunOnce.countDone());


        Test.stopTest();
        }
    }

}