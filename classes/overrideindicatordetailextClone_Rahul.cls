/*********************************************************************************
* Controller Class: overrideext
* DateCreated:  03/11/2014
----------------------------------------------------------------------------------
* Purpose/Methods:
* - page is opened from the related list of Performance Framework detail page
* - creates Impact and Outcome Grant Indicator record
* - creates Result/Target record for each Grant Indicator records where 
    Type = 'Coverage/Output' and Module =: master module__c.Module
----------------------------------------------------------------------------------
* Unit Test: overrideext_test
----------------------------------------------------------------------------------
* History:
* - VERSION  DATE            DETAIL FEATURES
    1.0      03/11/2014      INITIAL DEVELOPMENT   
*********************************************************************************/
public class overrideindicatordetailextClone_Rahul {
    
    public string Indid{get;set;}
    public List<RecordType> lstrec{get;set;}
    private Grant_Indicator__c objind;
    public string recid;
    public boolean display{get;set;}
    public boolean display1{get;set;}
    public List<Result__c> lstresult{get;set;}
    public String strLanguage {get;set;}
    public boolean editBasedOnStatus {get;set;}
    public boolean blnHistory{get;set;}
    public string pfstatus{get;set;} 
    List<Grant_Indicator__c> GID;
    public Boolean isViewIndiFromCN;
    public Performance_Framework__c pf {get; set;}
    public Date startDate {get; set;}
    public Date endDate {get; set;}
    public Boolean displayFiscal {get; set;}
    public List<Implementation_Period__c> lstimp = new List<Implementation_Period__c>();
    public string year1{get;set;}
    public string year2{get;set;}
    public string year3{get;set;}
    public string year4{get;set;}
    public String sign {get;set;}
    public Boolean readGlobalFundToPRComment{get;set;}
    public String decimalPlaces {get;set;}
    public overrideindicatordetailextClone_Rahul(ApexPages.StandardController controller) {
        decimalPlaces = '0';
        isViewIndiFromCN = false;
        objind = (Grant_Indicator__c)controller.getrecord();
        recid=objind.RecordtypeId;
        Indid= ApexPages.currentpage().getparameters().get('Id');
        GID  = [Select id,Performance_Framework__r.PF_status__c, Grant_Implementation_Period__c, Data_Type__c, Grant_Status__c, Decimal_Places__c
                from grant_indicator__c 
                where id =: Indid ];
        decimalPlaces = GID[0].Decimal_Places__c;
        pfstatus = GID[0].Performance_Framework__r.PF_Status__c;       
        checkProfile();
        lstimp = [select id, name, Component__C, Year_1__c, Year_2__c, Year_3__c, Year_4__c, Length_Years__c, 
                  Number_of_fiscal_cycles__c, Reporting__c, Start_Date__c, End_Date__c
                  from Implementation_Period__c 
                  where id=:gid[0].Grant_Implementation_Period__c];

        year1 = lstimp[0].Year_1__c;
        year2=lstimp[0].Year_2__c;
        year3=lstimp[0].Year_3__c;
        year4=lstimp[0].Year_4__c;
        startDate = lstimp[0].Start_Date__c;
        endDate = lstimp[0].End_Date__c;
        if(lstimp[0].Number_of_fiscal_cycles__c == '3'){
            displayFiscal = false;
        }
        else{
            displayFiscal = true;
        }
        strLanguage = 'ENGLISH';
        if(System.UserInfo.getLanguage() == 'fr'){
            strLanguage = 'FRENCH'; 
        }
        if(System.UserInfo.getLanguage() == 'ru'){
            strLanguage = 'RUSSIAN'; 
        }
        if(System.UserInfo.getLanguage() == 'es'){
            strLanguage = 'SPANISH'; 
        }
        
        /**For signs based on data type **/
        if(GID[0].Data_Type__c == Label.Percent_DT ||GID[0].Data_Type__c == Label.NaP_DT) {
            sign = Label.Percent_symb;
            return;
        }
        if(GID[0].Data_Type__c == Label.Ratio) {
            sign = Label.Ratio_label;
            return;
        }      
    }
    
    public PageReference reDirect() {
        PageReference viewAction;
        String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
        string userTypeStr = UserInfo.getUserType();
        if(userTypeStr == 'Standard') {
            baseUrl = baseUrl+'/';
        }
        else 
            baseUrl = baseUrl+'/GM/';
        
        if(isViewIndiFromCN == true) {
            baseUrl = baseUrl +  Indid + '?nooverride=1';
            viewAction = new PageReference(baseUrl);
            viewAction.setRedirect(true);
            return viewAction;
        }
        else {
           Id profileId=userinfo.getProfileId();
           String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
           if(profileName == 'System Administrator') {
               baseUrl = baseUrl +  Indid + '?nooverride=1';
               viewAction = new PageReference(baseUrl);
               viewAction.setRedirect(true);
               return viewAction;
           }
           else{
              return null;
           }
        }//return null;
            
    }
    
    private void checkProfile() {
       
        if(objind.Performance_Framework__c != null) {
            editBasedOnStatus = false;
            readGlobalFundToPRComment = false;        
            pf = [SELECT Grant_Status__c FROM Performance_Framework__c WHERE id =: objind.Performance_Framework__c];
            Profile profileRec = [select id, name from Profile where id =: UserInfo.getProfileId()];
            String profileNameStr = profileRec.Name;
            List<Profile_Access_Setting__c> checkpage = new List<Profile_Access_Setting__c>();
            List<String> PermissionSets = new List<String>();
            List<PermissionSetAssignment> standalonePermSets = [SELECT PermissionSet.Label 
                                                                FROM PermissionSetAssignment 
                                                                WHERE Assignee.Username =: UserInfo.getUserName()];
            if(standalonePermSets.size()>0) {
                for(PermissionSetAssignment PermSets : standalonePermSets) {
                    PermissionSets.add(PermSets.PermissionSet.Label);
                    system.debug('Name '+PermSets.PermissionSet.Label);
        
                }
            }
            checkpage = [SELECT Salesforce_Item__c,Status__c 
                         FROM Profile_Access_Setting__c 
                         WHERE (Page_Name__c =: 'overrideindicatordetail' OR Page_Name__c =: Label.overrideeditext_page_name) 
                         AND (Profile_Name__c =: profileNameStr OR Permission_Sets__c IN: PermissionSets)];
    
            for (Profile_Access_Setting__c check : checkpage) {
                if(check.Salesforce_Item__c == 'display Edit_Delete button' && (check.Status__c == pf.Grant_Status__c || check.Status__c == null || check.Status__c == ''))
                    editBasedOnStatus = true;
                if((check.Salesforce_Item__c == 'Read_Global fund comment to PR' || check.Salesforce_Item__c == 'Edit_Global fund comment to PR') && 
                   (check.Status__c == pf.Grant_Status__c || check.Status__c == null || check.Status__c == ''))
                    readGlobalFundToPRComment = true;
                
            }
        }
        else 
            isViewIndiFromCN = true;
    }
    

    
    public pageReference edit(){
        pageReference Pr = new pageReference('/apex/overideedit?Id='+indid);
        pr.setredirect(true);
        return pr;  
    }
    public pageReference deleteind(){
        pageReference Pr = new pageReference('/apex/Delete_Indicator?Id='+indid);
        pr.setredirect(true);
        return pr; 
    }
    public pageReference addgoals(){
        pageReference Pr = new pageReference('/apex/GoalSelection?Id='+indid);
        pr.setredirect(true);
        return pr;
       
    }
    
     Public void ShowHistoryPopup(){
         blnHistory = true;
     }
     Public void HidePopupHistory(){
         system.debug('**InsideHidePopup');
         blnHistory = false;
     }
    public pagereference openGuidance(){
        
        Pagereference pr;
        Grant_Indicator__c objGI = [SELECT id, Indicator_Type__c FROM Grant_Indicator__c WHERE id =: indid LIMIT 1];
        Guidance__c gui_impact = [Select Id, Name from Guidance__c WHERE Name =: Label.GM_Goals_Impact_Indicators LIMIT 1];
        Guidance__c gui_outcome = [Select Id, Name from Guidance__c WHERE Name =: Label.GM_Objectives_Outcome_Indicators LIMIT 1];
        Guidance__c gui_coverage = [Select Id, Name from Guidance__c WHERE Name =: Label.GM_Coverage_Output_Indicators LIMIT 1];
        //List<Guidance__c> lstGUI = [Select Id, Name from Guidance__c];
        
        if(objGI.Indicator_Type__c == 'Impact'){
            pr = new Pagereference('/apex/GrantMakingGuidance?Id='+gui_impact.id+'&lang='+strLanguage);
        }
        else if(objGI.Indicator_Type__c == 'Outcome'){
            pr = new Pagereference('/apex/GrantMakingGuidance?Id='+gui_outcome.id+'&lang='+strLanguage);
        }
        else if(objGI.Indicator_Type__c == 'Coverage/Output'){
            pr = new Pagereference('/apex/GrantMakingGuidance?Id='+gui_coverage.id+'&lang='+strLanguage);
        }
        
        pr.setredirect(true);
        return pr; 
    }
    
   public pagereference openPF(){
   Pagereference pr = new Pagereference('/'+objind.Performance_Framework__c);
   pr.setredirect(true);
   return pr;
   }

}