Global Class GMAssumptionDetailedBudget{
    Public String strSelectedYearAssumption {get;set;}
    
    public GMAssumptionDetailedBudget(ApexPages.StandardController controller){
    
        Id BudgetLineID = Apexpages.currentpage().getparameters().get('Id');
        string CatalogGrpType =  Apexpages.currentpage().getparameters().get('GroupType');
         strSelectedYearAssumption = 'Year1';
        List<Assumption__c> AssumptionList = [Select Assumptions_to_Support_Unit_Cost__c,Y1_Duration_Days__c,Budget_Line_Number__r.Cost_Input__r.Cost_Grouping__c, 
                                                    Item_Description__c,Justifications_Comments__c,Unit_of_Measure__c,
                                                    Y1_Quantity__c, Y1_Unit_Cost_Payment_Currency__c,Payment_Currency__c, 
                                                    Y1_Net_Salary_Payment_Currency__c,
                                                    Y1_Total_Salary_Payment_Currency__c,Y1_Level_of_Effort__c,
                                                    Y1_Social_Security_Obligatory_Charges__c,Y1_Number_of_Persons__c,
                                                    Y2_Duration_Days__c,Y2_Quantity__c, Y2_Unit_Cost_Payment_Currency__c,Y2_Net_Salary_Payment_Currency__c,
                                                    Y2_Total_Salary_Payment_Currency__c,Y2_Level_of_Effort__c,
                                                    Y2_Social_Security_Obligatory_Charges__c,Y2_Number_of_Persons__c,
                                                    Y3_Duration_Days__c,Y3_Quantity__c, Y3_Unit_Cost_Payment_Currency__c,Y3_Net_Salary_Payment_Currency__c,
                                                    Y3_Total_Salary_Payment_Currency__c,Y3_Level_of_Effort__c,
                                                    Y3_Social_Security_Obligatory_Charges__c,Y3_Number_of_Persons__c,
                                                    Y4_Duration_Days__c,Y4_Quantity__c, Y4_Unit_Cost_Payment_Currency__c,Y4_Net_Salary_Payment_Currency__c,
                                                    Y4_Total_Salary_Payment_Currency__c,Y4_Level_of_Effort__c,
                                                    Y4_Social_Security_Obligatory_Charges__c,Y4_Number_of_Persons__c
                                                    From Assumption__c where Budget_Line_Number__c =: BudgetLineID];
                                                        
    }
}