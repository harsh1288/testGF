public class ctrlDBreport{
    public List<IP_Detail_Information__c> lstBud{get;set;}
    public string strGIPId{get;set;}
    public Id BudgetId{get;set;}
    Public List<BLModule> lstBLModuleByModule{get;set;}
    public List<Budget_Line__c> lstbudgetline{get;set;}
    public string field{get;set;}
    public string ord{get;set;}
    
    Public Double TotalModuleY1Cost {get;set;}
    Public Double TotalModuleY2Cost {get;set;}
    Public Double TotalModuleY3Cost {get;set;}
    Public Double TotalModuleY4Cost {get;set;}
    Public Double AllYearModuleTotalCost {get;set;}
    
   public ctrlDBreport(ApexPages.StandardController controller) {
      BudgetId= Apexpages.currentpage().getparameters().get('id');      
      
      lstBud = new List<IP_Detail_Information__c>();
      List<IP_Detail_Information__c> lstBud =  [Select id,Name,Implementation_Period__c,Implementation_Period__r.start_date__c,Implementation_Period__r.end_date__c,Implementation_Period__r.Name,Implementation_Period__r.Year_4__c,Implementation_Period__r.Year_1__c,Implementation_Period__r.Year_2__c,Implementation_Period__r.Year_3__c from IP_Detail_Information__c where id =:BudgetId];
      strGIPId = lstBud[0].Implementation_Period__c ;
      lstorderby();
   }
   
   public void lstorderby(){
   	   lstbudgetline = new List<Budget_Line__c>();
   	   field = Apexpages.currentPage().getParameters().get('label');
   	   ord = Apexpages.currentPage().getParameters().get('orderby');
       if(field == Null && ord == Null){
          field = 'Id';
          ord = 'asc';
       }
       
       string querystr = '';
       querystr = 'select id,Grant_Intervention__c,Grant_Intervention_Name__c,Module_Name__c,Reference_Number__c,Activity__c,Cost_Grouping__c,Cost_Input_Name__c,Recipient__c,Unit_of_Measure__c,Currency_Code__c,Unit_Cost_Y1__c,Unit_Cost_Y2__c,Unit_Cost_Y3__c,Unit_Cost_Y4__c,Q1_Quantity__c,Q2_Quantity__c,Q3_Quantity__c,Q4_Quantity__c,Q5_Quantity__c,Q6_Quantity__c,Q7_Quantity__c,Q8_Quantity__c,Q9_Quantity__c,Q10_Quantity__c,Q11_Quantity__c,Q12_Quantity__c,Q13_Quantity__c,Q14_Quantity__c,Q15_Quantity__c,Q16_Quantity__c,Q1_Grant_Amount__c,Q2_Grant_Amount__c,Q3_Grant_Amount__c,Q4_Grant_Amount__c,Q5_Grant_Amount__c,Q6_Grant_Amount__c,Q7_Grant_Amount__c,Q8_Grant_Amount__c,Q9_Grant_Amount__c,Q10_Grant_Amount__c,Q11_Grant_Amount__c,Q12_Grant_Amount__c,Q13_Grant_Amount__c,Q14_Grant_Amount__c,Q15_Grant_Amount__c,Q16_Grant_Amount__c,Annual_Quantity__c,Annual_Quantity_Y2__c,Annual_Quantity_Y3__c,Annual_Quantity_Y4__c,Y1_Grant_Amount__c,Y2_Grant_Amount__c,Y3_Grant_Amount__c,Y4_Grant_Amount__c,Total_Quantity__c,Total_Grant_Amount__c,PR_Comments__c,TGF_Comments__c from Budget_Line__c where Detailed_Budget_Framework__c=:BudgetId order by';
       querystr += ' '+field;
       querystr += ' '+ord;
       lstbudgetline = database.query(querystr);
       listbymodule();
   }
   
   public void listbymodule(){
      
      
      lstBLModuleByModule = new List<BLModule>();
      
      Set<string> setmodname = new Set<string>();
      
      for(Budget_Line__c objbud:lstbudgetline){
      	   if(setmodname.size()!= Null && setmodname.contains(objbud.Module_Name__c)){
      	      continue;  
      	   }
      	   else{
      	     setmodname.add(objbud.Module_Name__c);
      	   }
      }
      List<Budget_Line__c> tempbudlist = new List<Budget_Line__c>();
      lstBLModuleByModule = new List<BLModule>();
      for(string obj:setmodname){
      	  BLModule objblmod = new BLModule();
      	  objblmod.ModuleName = obj;
      	  objblmod.InterventionName = new set<String>();
      	  objblmod.mapBLModule = new Map<string,List<Budget_Line__c>>();
          for(Budget_Line__c objbud:lstbudgetline){
              if(objbud.Module_Name__c == obj){
              	 if(objblmod.InterventionName.size() != Null && objblmod.InterventionName.contains(objbud.Grant_Intervention_Name__c)){
              	    tempbudlist = objblmod.mapBLModule.get(objbud.Grant_Intervention_Name__c);
              	    tempbudlist.add(objbud);
              	 }
              	 else{
              	   objblmod.InterventionName.add(objbud.Grant_Intervention_Name__c);
              	   tempbudlist = new List<Budget_Line__c>();
              	   tempbudlist.add(objbud);
              	 }
              	  objblmod.mapBLModule.put(objbud.Grant_Intervention_Name__c,tempbudlist);
              }
              else{
                 continue;
              }
          }
          lstBLModuleByModule.add(objblmod);   
      }
   }
   
   public Class BLModule{
        Public Double ModuleCostY1 {get;set;}
        Public Double ModuleCostY2 {get;set;}
        Public Double ModuleCostY3 {get;set;}
        Public Double ModuleCostY4 {get;set;}
        Public Double TotalModuleCost {get;set;}
        Public String ModuleName {get;set;}
        Public set<String> InterventionName {get;set;}
        Public Map<string,List<Budget_Line__c>> mapBLModule {get;set;}
        Public Boolean blnbckgrnd {get; set;}
    }
    
    public pageReference vfexcel(){
       pageReference pgr =  new pageReference('/apex/VFDBreportexcel?id='+BudgetId);
       pgr.setredirect(true);
       return pgr;
       
    } 
}