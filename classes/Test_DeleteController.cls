@isTest
private class Test_DeleteController {

   public static testMethod void Delete_Controller() {
        Account objAcc = TestClassHelper.insertAccount();
    
    Country__c country = TestClassHelper.createCountry();
     country.X200DEMA_Exch_Rate_in_EUR__c = 10.0000;
     country.X200DEMA_Exch_Rate_in_USD__c = 10.000;
     insert country;
     
     Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
     objGrant.Country__c = country.Id;
     objGrant.Grant_Status__c = 'Accepted';
     insert objGrant;
     
     Implementation_Period__c ObjImplementationPeriod = TestClassHelper.createIP(objGrant,objAcc);
     ObjImplementationPeriod.Currency_of_Grant_Agreement__c = 'USD';
     ObjImplementationPeriod.Local_Currency__c = 'COP';
     ObjImplementationPeriod.Other_Currency__c = 'EUR';
     ObjImplementationPeriod.Exchange_Rate__c = 50;
     ObjImplementationPeriod.Budget_Status__c = 'Draft';
     insert ObjImplementationPeriod;
     
        Performance_Framework__C objPF = TestClassHelper.createPF(ObjImplementationPeriod);
        insert objPF;
        
        Goals_Objectives__c objGoals = TestClassHelper.createGoalsObjectives();
        objGoals.Type__c = 'Goal';
        objGoals.Performance_Framework__c = objPF.id;
        insert objGoals;
        
        Profile_Access_Setting_CN__c objProsetting = TestClassHelper.createProfileSettingCN();
        objProsetting.Salesforce_Item__c = 'Delete';
        objProsetting.Page_Name__c = 'Goals_Objectives__c';
        objProsetting.Profile_Name__c = 'PR Admin';
        objProsetting.Status__C = 'Accepted';
        insert objProsetting;
        Contact con = new Contact(FirstName = 'Test', LastName = 'PR Admin', AccountId =objAcc.Id );
        insert con;
        Profile profileId=[Select Id,Name from Profile where Name = 'PR Admin'];
        User user1 = new User(Alias = 'standt', Email='standarduserDC1@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='fr', 
        LocaleSidKey='en_US', ProfileId = profileId.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='standarduserDC1@testorg.com', ContactId= con.Id);
        insert user1;
      
        system.runAs(user1){
        ApexPages.currentPage().getParameters().put('id',objGoals.id);
        ApexPages.currentPage().getParameters().put('retUrl',objPF.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(objGoals);
        Delete_Controller deleteController = new Delete_Controller(sc);
        
        deleteController.reDirect();
      }
          system.runAs(user1){
        ApexPages.currentPage().getParameters().put('id',objPF.id);
        ApexPages.currentPage().getParameters().put('retUrl',objPF.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(objPF);
        Delete_Controller deleteController = new Delete_Controller(sc);
        deleteController.reDirect();
          }
    }
    public static testMethod void Delete_Controller1() {
        Account objAcc = TestClassHelper.insertAccount();
    
    Country__c country = TestClassHelper.createCountry();
     country.X200DEMA_Exch_Rate_in_EUR__c = 10.0000;
     country.X200DEMA_Exch_Rate_in_USD__c = 10.000;
     insert country;
     
     Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
     objGrant.Country__c = country.Id;
     objGrant.Grant_Status__c = 'Not yet submitted';
     insert objGrant;
     
     Implementation_Period__c ObjImplementationPeriod = TestClassHelper.createIP(objGrant,objAcc);
     ObjImplementationPeriod.Currency_of_Grant_Agreement__c = 'USD';
     ObjImplementationPeriod.Local_Currency__c = 'COP';
     ObjImplementationPeriod.Other_Currency__c = 'EUR';
     ObjImplementationPeriod.Exchange_Rate__c = 50;
     ObjImplementationPeriod.Budget_Status__c = 'Draft';
     insert ObjImplementationPeriod;
     
        Performance_Framework__C objPF = TestClassHelper.createPF(ObjImplementationPeriod);
        insert objPF;
        
        Goals_Objectives__c objGoals = TestClassHelper.createGoalsObjectives();
        objGoals.Type__c = 'Goal';
        objGoals.Performance_Framework__c = objPF.id;
        insert objGoals;
        
        Grant_Indicator__c objIndicator = TestClassHelper.createGrantIndicator();
        objIndicator.Performance_Framework__c = objPF.id;
        insert objIndicator;
        
        Ind_Goal_Jxn__c objGoalIndJxn = TestClassHelper.insertIndicatorGoalJxn(objGoals,objIndicator);
        
        Profile_Access_Setting_CN__c objProsetting = TestClassHelper.createProfileSettingCN();
        objProsetting.Salesforce_Item__c = 'Delete';
        objProsetting.Status__C = 'Not yet submitted';
        objProsetting.Profile_Name__c = 'System Administrator';
        objProsetting.Page_Name__c = 'Ind_Goal_Jxn__c';
        insert objProsetting;
        
        ApexPages.currentPage().getParameters().put('id',objGoalIndJxn.id);
        ApexPages.currentPage().getParameters().put('retUrl',objPF.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(objGoalIndJxn);
        Delete_Controller deleteController = new Delete_Controller(sc);
        
        deleteController.reDirect();
        
    }
     public static testMethod void Delete_Controller2() {
        Account objAcc = TestClassHelper.insertAccount();
    
    Country__c country = TestClassHelper.createCountry();
     country.X200DEMA_Exch_Rate_in_EUR__c = 10.0000;
     country.X200DEMA_Exch_Rate_in_USD__c = 10.000;
     insert country;
     
     Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
     objGrant.Country__c = country.Id;
     objGrant.Grant_Status__c = 'Not yet submitted';
     insert objGrant;
     
     Implementation_Period__c ObjImplementationPeriod = TestClassHelper.createIP(objGrant,objAcc);
     ObjImplementationPeriod.Currency_of_Grant_Agreement__c = 'USD';
     ObjImplementationPeriod.Local_Currency__c = 'COP';
     ObjImplementationPeriod.Other_Currency__c = 'EUR';
     ObjImplementationPeriod.Exchange_Rate__c = 50;
     ObjImplementationPeriod.Budget_Status__c = 'Draft';
     insert ObjImplementationPeriod;
     
        Performance_Framework__C objPF = TestClassHelper.createPF(ObjImplementationPeriod);
        insert objPF;
        
        Goals_Objectives__c objGoals = TestClassHelper.createGoalsObjectives();
        objGoals.Type__c = 'Goal';
        objGoals.Performance_Framework__c = objPF.id;
        insert objGoals;
        
        FeedItem objFI = new FeedItem();
        objFI.parentId = ObjImplementationPeriod.Id;
        objFI.body = 'test';
        objFI.Type  = 'ContentPost';
        objFI.ContentFileName = 'TestDocument.txt';
        objFI.ContentData =  blob.valueof('TestDocument.txt');
        objFI.ContentDescription = 'Other Document';
        insert objFI;
        
        DocumentUpload_GM__c objDocUpload = new DocumentUpload_GM__c();
        objDocUpload.Implementation_Period__c = ObjImplementationPeriod.id;
        objDocUpload.FeedItem_Id__c = objFI.id;
        objDocUpload.Process_Area__c = 'Concept Note';
        objDocUpload.Description__c = 'test';
        objDocUpload.Language__c = 'English';
        objDocUpload.Type__c = 'Concept note narrative';
        objDocUpload.security__c = 'Visible to all';
        insert objDocUpload;
      
        ApexPages.currentPage().getParameters().put('id',objDocUpload.id);
        ApexPages.currentPage().getParameters().put('retUrl',objPF.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(objDocUpload);
        Delete_Controller deleteController = new Delete_Controller(sc);
        
        deleteController.reDirect();
        
    }
    
    //*To cover objective line*//
       public static testMethod void Delete_Controller10() {
       	    test.startTest();
        Account objAcc = TestClassHelper.insertAccount();
    
    Country__c country = TestClassHelper.createCountry();
     country.X200DEMA_Exch_Rate_in_EUR__c = 10.0000;
     country.X200DEMA_Exch_Rate_in_USD__c = 10.000;
     insert country;
     
     Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
     objGrant.Country__c = country.Id;
     objGrant.Grant_Status__c = 'Not yet submitted';
     insert objGrant;
     
     Implementation_Period__c ObjImplementationPeriod = TestClassHelper.createIP(objGrant,objAcc);
     ObjImplementationPeriod.Currency_of_Grant_Agreement__c = 'USD';
     ObjImplementationPeriod.Local_Currency__c = 'COP';
     ObjImplementationPeriod.Other_Currency__c = 'EUR';
     ObjImplementationPeriod.Exchange_Rate__c = 50;
     ObjImplementationPeriod.Budget_Status__c = 'Draft';
     ObjImplementationPeriod.Principal_Recipient__c =  objAcc.Id;
     ObjImplementationPeriod.Grant__c= objGrant.Id;
     insert ObjImplementationPeriod;
     
        Performance_Framework__C objPF = TestClassHelper.createPF(ObjImplementationPeriod);
        insert objPF;
        
        Goals_Objectives__c objGoals = TestClassHelper.createGoalsObjectives();
        objGoals.Type__c = 'Objective';
        objGoals.Performance_Framework__c = objPF.id;
        insert objGoals;
        
        FeedItem objFI = new FeedItem();
        objFI.parentId = ObjImplementationPeriod.Id;
        objFI.body = 'test';
        objFI.Type  = 'ContentPost';
        objFI.ContentFileName = 'TestDocument.txt';
        objFI.ContentData =  blob.valueof('TestDocument.txt');
        objFI.ContentDescription = 'Other Document';
        insert objFI;
        
        DocumentUpload_GM__c objDocUpload = new DocumentUpload_GM__c();
        objDocUpload.Implementation_Period__c = ObjImplementationPeriod.id;
        objDocUpload.FeedItem_Id__c = objFI.id;
        objDocUpload.Process_Area__c = 'Concept Note';
        objDocUpload.Description__c = 'test';
        objDocUpload.Language__c = 'English';
        objDocUpload.Type__c = 'Concept note narrative';
        objDocUpload.security__c = 'Visible to LFA and TGF internal only';
        insert objDocUpload;
      
        ApexPages.currentPage().getParameters().put('id',objDocUpload.id);
        ApexPages.currentPage().getParameters().put('retUrl',objPF.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(objDocUpload);
        Delete_Controller deleteController = new Delete_Controller(sc);
        
        deleteController.reDirect();
            test.stopTest();
        
    }
  
    /**/
    public static testMethod void Delete_Controller4() {
    
        Profile p1 = [SELECT Id FROM Profile WHERE Name='Management' LIMIT 1]; 
        User user1 = new User(Alias = 'standt', Email='standarduserDC2@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='fr', 
        LocaleSidKey='en_US', ProfileId = p1.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='standarduserDC2@testorg.com');
        
        Profile p2 = [SELECT Id FROM Profile WHERE Name='System Administrator' LIMIT 1]; 
        User user2 = new User(Alias = 'extrt', Email='externaluserDC2@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='fr', 
        LocaleSidKey='en_US', ProfileId = p2.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='externaluserDC2@testorg.com');
    
    Account objAcc = TestClassHelper.insertAccount();
    
    Country__c country = TestClassHelper.createCountry();
     country.X200DEMA_Exch_Rate_in_EUR__c = 10.0000;
     country.X200DEMA_Exch_Rate_in_USD__c = 10.000;
     insert country;
     
     Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
     objGrant.Country__c = country.Id;
     objGrant.Grant_Status__c = 'Not yet submitted';
     insert objGrant;
     
     Implementation_Period__c ObjImplementationPeriod = TestClassHelper.createIP(objGrant,objAcc);
     ObjImplementationPeriod.Currency_of_Grant_Agreement__c = 'USD';
     ObjImplementationPeriod.Local_Currency__c = 'COP';
     ObjImplementationPeriod.Other_Currency__c = 'EUR';
     ObjImplementationPeriod.Exchange_Rate__c = 50;
     ObjImplementationPeriod.Budget_Status__c = 'Draft';
     insert ObjImplementationPeriod;
     
        Performance_Framework__C objPF = TestClassHelper.createPF(ObjImplementationPeriod);
        insert objPF;
        
        Implementer__c objImp = TestClassHelper.createImplementor();
        objImp.Grant_Implementation_Period__c = ObjImplementationPeriod.id;
        objImp.Performance_Framework__c = objPF.id;
        objImp.Implementer_Name__c = 'Test';
        objImp.Implementer_Role__c = 'Test';
        objImp.Add_Implementer__c = 'New';
        insert objImp;
        
        Implementer__c objImp1 = new Implementer__c();
        objImp1.Grant_Implementation_Period__c = ObjImplementationPeriod.id;
        objImp1.Performance_Framework__c = objPF.id;
        objImp1.Implementer_Name__c = 'Test';
        objImp1.Implementer_Role__c = 'Principal Recipient';
        objImp1.Add_Implementer__c = 'New';
        insert objImp1;
     	
        
        
        
        
        System.runAs(user1){
            ApexPages.currentPage().getParameters().put('id',objImp.id);
            ApexPages.currentPage().getParameters().put('retUrl',objPF.Id);
            ApexPages.StandardController sc = new ApexPages.StandardController(objImp);
            Delete_Controller deleteController = new Delete_Controller(sc);
            deleteController.reDirect();
            
        }
        System.runAs(user2){
            ApexPages.currentPage().getParameters().put('id',objImp1.id);
            ApexPages.currentPage().getParameters().put('retUrl',objPF.Id);
            ApexPages.StandardController sc = new ApexPages.StandardController(objImp1);
            Delete_Controller deleteController = new Delete_Controller(sc);
            deleteController.reDirect();
        }
        
    }
    public static testMethod void Delete_Controller5() {
    	Account objAcc = TestClassHelper.insertAccount();
    
        Country__c country = TestClassHelper.createCountry();
         country.X200DEMA_Exch_Rate_in_EUR__c = 10.0000;
         country.X200DEMA_Exch_Rate_in_USD__c = 10.000;
         insert country;
         
        Contact con = new Contact(FirstName = 'TestLFA', LastName = 'LFA Admin', AccountId =objAcc.Id, External_User__c = true,Email = 'testPRAmin@tgf.ex.com' );
        insert con;
        
        Contact con1 = new Contact(FirstName = 'TestLFA', LastName = 'LFA Admin', AccountId =objAcc.Id, External_User__c = true,Email = 'testPRAmin@tgf.ex.com' );
        insert con1;
        
        List<npe5__Affiliation__c> AffAccCon = [select Id,npe5__Status__c from npe5__Affiliation__c where npe5__Organization__c =:objAcc.id AND npe5__Contact__c =:con.id];
        AffAccCon[0].npe5__Status__c = 'former';
        update AffAccCon[0];
        
        List<npe5__Affiliation__c> AffAccCon1 = [select Id,npe5__Status__c from npe5__Affiliation__c where npe5__Organization__c =:objAcc.id AND npe5__Contact__c =:con1.id];
        AffAccCon1[0].npe5__Status__c = 'former';
        update AffAccCon1[0];
        
        npe5__Affiliation__c objAff = new npe5__Affiliation__c();
        objAff.npe5__Contact__c = con.Id;
        objAff.npe5__Organization__c = objAcc.Id;
        objAff.RecordTypeId = label.PR_Affiliation_RT;
        objAff.Access_Level__c = 'Read';
        insert objAff;
        
        Profile p1 = [SELECT Id FROM Profile WHERE Name='LFA Portal User' LIMIT 1]; 
        User user1 = new User(Alias = 'standt', Email='standarduserDC3@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='fr', 
        LocaleSidKey='en_US', ProfileId = p1.Id,ContactId= con.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='standarduserDC3@testorg.com',IsActive= True);
        insert user1;
        
        
        Profile p2 = [SELECT Id FROM Profile WHERE Name='System Administrator' LIMIT 1]; 
        User user2 = new User(Alias = 'extrt', Email='externaluserDC3@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='fr', 
        LocaleSidKey='en_US', ProfileId = p2.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='externaluserDC3@testorg.com');
    
        
     
         Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
         objGrant.Country__c = country.Id;
         objGrant.Grant_Status__c = 'Not yet submitted';
         insert objGrant;
         
         Implementation_Period__c ObjImplementationPeriod = TestClassHelper.createIP(objGrant,objAcc);
         ObjImplementationPeriod.Currency_of_Grant_Agreement__c = 'USD';
         ObjImplementationPeriod.Local_Currency__c = 'COP';
         ObjImplementationPeriod.Other_Currency__c = 'EUR';
         ObjImplementationPeriod.Exchange_Rate__c = 50;
         ObjImplementationPeriod.Budget_Status__c = 'Draft';
         insert ObjImplementationPeriod;
     
        Performance_Framework__C objPF = TestClassHelper.createPF(ObjImplementationPeriod);
        insert objPF;
        
        Implementer__c objImp = TestClassHelper.createImplementor();
        objImp.Grant_Implementation_Period__c = ObjImplementationPeriod.id;
        objImp.Performance_Framework__c = objPF.id;
        objImp.Implementer_Name__c = 'Test';
        objImp.Implementer_Role__c = 'Principal Recipient';
        objImp.Add_Implementer__c = 'New';
        insert objImp;
        
        Implementer__c objImp1 = new Implementer__c();
        objImp1.Grant_Implementation_Period__c = ObjImplementationPeriod.id;
        objImp1.Performance_Framework__c = objPF.id;
        objImp1.Implementer_Name__c = 'Test';
        objImp1.Implementer_Role__c = 'Test';
        objImp1.Add_Implementer__c = 'New';
        insert objImp1;
     	
         FeedItem objFI = new FeedItem();
        objFI.parentId = ObjImplementationPeriod.Id;
        objFI.body = 'test';
        objFI.Type  = 'ContentPost';
        objFI.ContentFileName = 'TestDocument.txt';
        objFI.ContentData =  blob.valueof('TestDocument.txt');
        objFI.ContentDescription = 'Other Document';
        insert objFI;
        System.runAs(user1){
        
        DocumentUpload_GM__c objDocUpload = new DocumentUpload_GM__c();
        objDocUpload.Implementation_Period__c = ObjImplementationPeriod.id;
        objDocUpload.FeedItem_Id__c = objFI.id;
        objDocUpload.Process_Area__c = 'Concept Note';
        objDocUpload.Description__c = 'test';
        objDocUpload.Language__c = 'English';
        objDocUpload.Type__c = 'Concept note narrative';
        objDocUpload.security__c = 'Visible to LFA and TGF internal only';
        insert objDocUpload;
            
            ApexPages.currentPage().getParameters().put('id',objImp.id);
            ApexPages.currentPage().getParameters().put('retUrl',objPF.Id);
            ApexPages.StandardController sc = new ApexPages.StandardController(objImp);
            Delete_Controller deleteController = new Delete_Controller(sc);
            deleteController.reDirect();
            
        ApexPages.currentPage().getParameters().put('id',objDocUpload.id);
            ApexPages.currentPage().getParameters().put('retUrl',objPF.Id);
            ApexPages.StandardController sc1 = new ApexPages.StandardController(objDocUpload);
            Delete_Controller deleteController1 = new Delete_Controller(sc1);
            deleteController1.reDirect();
        }
        System.runAs(user2){
            ApexPages.currentPage().getParameters().put('id',objImp1.id);
            ApexPages.currentPage().getParameters().put('retUrl',objPF.Id);
            ApexPages.StandardController sc = new ApexPages.StandardController(objImp1);
            Delete_Controller deleteController = new Delete_Controller(sc);
            deleteController.reDirect();
        }
        
    }
    public static testMethod void Delete_Controller6() {
    
        Profile p1 = [SELECT Id FROM Profile WHERE Name='Management' LIMIT 1]; 
        User user1 = new User(Alias = 'standt', Email='standarduserDC3@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='fr', 
        LocaleSidKey='en_US', ProfileId = p1.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='standarduserDC3@testorg.com');
        
        Profile p2 = [SELECT Id FROM Profile WHERE Name='System Administrator' LIMIT 1]; 
        User user2 = new User(Alias = 'extrt', Email='externaluserDC3@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='fr', 
        LocaleSidKey='en_US', ProfileId = p2.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='externaluserDC3@testorg.com');
    
        Account objAcc = TestClassHelper.insertAccount();
    
        Country__c country = TestClassHelper.createCountry();
         country.X200DEMA_Exch_Rate_in_EUR__c = 10.0000;
         country.X200DEMA_Exch_Rate_in_USD__c = 10.000;
         insert country;
     
         Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
         objGrant.Country__c = country.Id;
         objGrant.Grant_Status__c = 'Not yet submitted';
         insert objGrant;
         
         Implementation_Period__c ObjImplementationPeriod = TestClassHelper.createIP(objGrant,objAcc);
         ObjImplementationPeriod.Currency_of_Grant_Agreement__c = 'USD';
         ObjImplementationPeriod.Local_Currency__c = 'COP';
         ObjImplementationPeriod.Other_Currency__c = 'EUR';
         ObjImplementationPeriod.Exchange_Rate__c = 50;
         ObjImplementationPeriod.Budget_Status__c = 'Draft';
         insert ObjImplementationPeriod;
     
        Performance_Framework__C objPF = TestClassHelper.createPF(ObjImplementationPeriod);
        insert objPF;
        
        Module__c objModule = TestClassHelper.createModule();
        objModule.Performance_Framework__C = objPF.Id;
        objModule.Implementation_Period__c = ObjImplementationPeriod.Id;
        objModule.PF_is_Null__c = true;
        insert objModule;
        
        Module__c objModule1 = TestClassHelper.createModule();
        objModule1.Performance_Framework__C = objPF.Id;
        objModule1.Implementation_Period__c = ObjImplementationPeriod.Id;
        objModule1.PF_is_Null__c = false;
        insert objModule1;
     
        Module__c objModule2 = TestClassHelper.createModule();
        objModule2.Performance_Framework__C = objPF.Id;
        objModule2.Implementation_Period__c = ObjImplementationPeriod.Id;
        objModule2.PF_is_Null__c = true;
        insert objModule2;
        
        Module__c objModule3 = TestClassHelper.createModule();
        objModule3.Performance_Framework__C = objPF.Id;
        objModule3.Implementation_Period__c = ObjImplementationPeriod.Id;
        objModule3.PF_is_Null__c = false;
        insert objModule3;
        
        Grant_Intervention__c objGI = TestClassHelper.createGrantIntervention(ObjImplementationPeriod);
        objGI.Module__c = objModule1.Id;
        insert objGI;
        
       
        System.runAs(user1){
            ApexPages.currentPage().getParameters().put('id',objModule.id);
            ApexPages.currentPage().getParameters().put('retUrl',objPF.Id);
            ApexPages.StandardController sc = new ApexPages.StandardController(objModule);
            Delete_Controller deleteController = new Delete_Controller(sc);
            deleteController.reDirect();
        }
        System.runAs(user1){
            ApexPages.currentPage().getParameters().put('id',objModule3.id);
            ApexPages.currentPage().getParameters().put('retUrl',objPF.Id);
            ApexPages.StandardController sc = new ApexPages.StandardController(objModule3);
            Delete_Controller deleteController = new Delete_Controller(sc);
            deleteController.reDirect();
        }
        System.runAs(user2){
            ApexPages.currentPage().getParameters().put('id',objGI.id);
            ApexPages.currentPage().getParameters().put('retUrl',objPF.Id);
            ApexPages.StandardController sc2 = new ApexPages.StandardController(objGI);
            Delete_Controller deleteController2 = new Delete_Controller(sc2);
            deleteController2.reDirect();
        }

        System.runAs(user2){
            ApexPages.currentPage().getParameters().put('id',objModule1.id);
            ApexPages.currentPage().getParameters().put('retUrl',objPF.Id);
            ApexPages.StandardController sc = new ApexPages.StandardController(objModule1);
            Delete_Controller deleteController = new Delete_Controller(sc);
            deleteController.reDirect();
        }
        System.runAs(user2){
            ApexPages.currentPage().getParameters().put('id',objModule2.id);
            ApexPages.currentPage().getParameters().put('retUrl',objPF.Id);
            ApexPages.StandardController sc = new ApexPages.StandardController(objModule2);
            Delete_Controller deleteController = new Delete_Controller(sc);
            deleteController.reDirect();
        }
        
    }
    public static testMethod void Delete_Controller7() {
    
         Account objAcc = TestClassHelper.insertAccount();
        
        Contact con = new Contact(FirstName = 'TestLFA', LastName = 'LFA Admin', AccountId =objAcc.Id, External_User__c = true,Email = 'testPRAmin@tgf.ex.com' );
        insert con;
        
        List<npe5__Affiliation__c> AffAccCon = [select Id,npe5__Status__c from npe5__Affiliation__c where npe5__Organization__c =:objAcc.id AND npe5__Contact__c =:con.id];
        AffAccCon[0].npe5__Status__c = 'former';
        update AffAccCon[0];
        
        npe5__Affiliation__c objAff = new npe5__Affiliation__c();
        objAff.npe5__Contact__c = con.Id;
        objAff.npe5__Organization__c = objAcc.Id;
        objAff.RecordTypeId = label.PR_Affiliation_RT;
        objAff.Access_Level__c = 'Read/Write';
        insert objAff;
        
        Profile p1 = [SELECT Id FROM Profile WHERE Name='LFA Portal User' LIMIT 1]; 
        User user1 = new User(Alias = 'standt', Email='standarduserDC3@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='fr', 
        LocaleSidKey='en_US', ProfileId = p1.Id,ContactId= con.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='standarduserDC3@testorg.com',IsActive= True);
        insert user1;
        
        
        Profile p2 = [SELECT Id FROM Profile WHERE Name='System Administrator' LIMIT 1]; 
        User user2 = new User(Alias = 'extrt', Email='externaluserDC3@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='fr', 
        LocaleSidKey='en_US', ProfileId = p2.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='externaluserDC3@testorg.com');
    
        
        Country__c country = TestClassHelper.createCountry();
         country.X200DEMA_Exch_Rate_in_EUR__c = 10.0000;
         country.X200DEMA_Exch_Rate_in_USD__c = 10.000;
         country.LFA__c = objAcc.Id;
         insert country;
     
         Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
         objGrant.Country__c = country.Id;
         objGrant.Grant_Status__c = 'Not yet submitted';
         insert objGrant;
         
         Implementation_Period__c ObjImplementationPeriod = TestClassHelper.createIP(objGrant,objAcc);
         ObjImplementationPeriod.Currency_of_Grant_Agreement__c = 'USD';
         ObjImplementationPeriod.Local_Currency__c = 'COP';
         ObjImplementationPeriod.Other_Currency__c = 'EUR';
         ObjImplementationPeriod.Exchange_Rate__c = 50;
         ObjImplementationPeriod.Budget_Status__c = 'Draft';
         insert ObjImplementationPeriod;
     
        Performance_Framework__C objPF = TestClassHelper.createPF(ObjImplementationPeriod);
        insert objPF;
        
        Grant_Multi_Country__c objGrntMultiCountry = new Grant_Multi_Country__c();
        objGrntMultiCountry.Grant_Implementation_Period__c = ObjImplementationPeriod.Id;
        objGrntMultiCountry.Country__c = country.Id;
        insert objGrntMultiCountry;
        
        Grant_Multi_Country__c objGrntMultiCountry1 = new Grant_Multi_Country__c();
        objGrntMultiCountry1.Grant_Implementation_Period__c = ObjImplementationPeriod.Id;
        objGrntMultiCountry1.Country__c = country.Id;
        insert objGrntMultiCountry1;
        
        FeedItem objFI = new FeedItem();
        objFI.parentId = ObjImplementationPeriod.Id;
        objFI.body = 'test';
        objFI.Type  = 'ContentPost';
        objFI.ContentFileName = 'TestDocument.txt';
        objFI.ContentData =  blob.valueof('TestDocument.txt');
        objFI.ContentDescription = 'Other Document';
        insert objFI;
        
        DocumentUpload_GM__c objDocUpload = new DocumentUpload_GM__c();
        objDocUpload.Implementation_Period__c = ObjImplementationPeriod.id;
        objDocUpload.FeedItem_Id__c = objFI.id;
        objDocUpload.Process_Area__c = 'Concept Note';
        objDocUpload.Description__c = 'test';
        objDocUpload.Language__c = 'English';
        objDocUpload.Type__c = 'Concept note narrative';
        objDocUpload.security__c = 'Visible to LFA and TGF internal only';
        insert objDocUpload;
        
         EditDeleteControllerbyAffiliation__c objEditDeleteAffi = new EditDeleteControllerbyAffiliation__c();
        objEditDeleteAffi.Imp_Period_Lkup_APIName__c = 'Grant_Implementation_Period__c';
        objEditDeleteAffi.Name = 'Grant_Multi_Country__c';
        insert objEditDeleteAffi;
        
        
        System.runAs(user1){
            ApexPages.currentPage().getParameters().put('id',objGrntMultiCountry.id);
            ApexPages.currentPage().getParameters().put('retUrl',objPF.Id);
            ApexPages.StandardController sc = new ApexPages.StandardController(objGrntMultiCountry);
            Delete_Controller deleteController = new Delete_Controller(sc);
            deleteController.reDirect();
        }
        System.runAs(user1){
            ApexPages.currentPage().getParameters().put('id',objDocUpload.id);
            ApexPages.currentPage().getParameters().put('retUrl',objPF.Id);
            ApexPages.StandardController sc1 = new ApexPages.StandardController(objDocUpload);
            Delete_Controller deleteController1= new Delete_Controller(sc1);
            deleteController1.reDirect();
        }
        System.runAs(user2){
            ApexPages.currentPage().getParameters().put('id',objGrntMultiCountry1.id);
            ApexPages.currentPage().getParameters().put('retUrl',objPF.Id);
            ApexPages.StandardController sc = new ApexPages.StandardController(objGrntMultiCountry1);
            Delete_Controller deleteController = new Delete_Controller(sc);
            deleteController.reDirect();
        }
        
    }
}