global Class PostContentAsPDF{
    WebService static String postPDFContentToChatter(String Id,String WpName,String UserFullName) { 
        try{
            PageReference pdf = Page.WP_PDF_neg;
            pdf.getParameters().put('id',Id);
            Blob body;
            if(Test.isRunningTest()){
            }else {
                        
               body = pdf.getContentAsPDF();
            }
            Date d = System.Today();
            
            FeedItem post = new FeedItem();
            post.ParentId = Id; 
            post.Body = UserFullName + ' sent the Work Plan to the LFA.';
            
            if(Test.isRunningTest()){
                post.ContentData = Blob.valueOf('Test');
            }else {    
                post.ContentData = body;
            }
            post.ContentFileName = WpName + ' - ' + d + '.pdf';
            post.Visibility = 'AllUsers';
            insert post;            
        }catch(exception ex){
           return ex.getMessage();
        }
        return '';
    }
    
    WebService static String postPDFContentToChatterSubmitProposal(String Id,String WpName,String UserFirstName,String UserLastName) { 
        system.debug('**************final0000000***********************');
        try{
           /* Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
            User u = [SELECT ID,Alias,Email,EmailEncodingKey,LastName,LanguageLocaleKey,LocaleSidKey,ProfileId,TimeZoneSidKey,UserName FROM User where profileId =: P.ID LIMIT 1];
            System.runAs(u){*/
            
                system.debug('**************final***********************');
               /* Id profileId=userinfo.getProfileId();
                String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
                if(profileName == 'System Administrator'){
                 PageReference pdf = Page.WP_PDF_neg;
                }*/
                PageReference pdf = new PageReference ('/LFA/apex/WP_PDF_neg?id=' +Id);
                system.debug('pdf pagereference'+pdf);
                system.debug('***Id '+Id);
                pdf.getParameters().put('Id',Id);
                Blob body;
                if(Test.isRunningTest()){
                }else { 
                    //body = pdf.getContent();  
                    body = pdf.getContentAsPDF();
                }
                               
                Date d = System.Today();
                FeedItem post = new FeedItem();
                post.ParentId = Id; 
                post.Body = UserFirstName +' '+ UserLastName + ' submitted a proposal to TGF.';
                if(Test.isRunningTest()){
                    post.ContentData = Blob.valueOf('Test');
                }else {    
                    post.ContentData = body;
                }
                post.ContentFileName = WpName + ' - ' + d + '.pdf';
                post.Visibility = 'AllUsers';
                insert post;  
            /*}  */        
        }catch(exception ex){
            system.debug('**************final: '+ ex.getMessage()+'***********************');
           return ex.getMessage();
        }
        return '';
    }
    
    WebService static String UpdateResourceStatus(String WpId) {
        try{
            if(String.IsBlank(WpId) == false){
                //String Query = 'Select Id,LOE__c,Cost__c,Initial_TGF_Proposed_LOE__c,Initial_TGF_Proposed_cost__c from LFA_Resource__c where WP_Status_text__c != \'LFA Proposal\' And LFA_Service__r.LFA_Work_Plan__c = \''+WpId+'\'';
                String Query = 'Select Id,LOE__c,Cost__c,Initial_TGF_Proposed_LOE__c,Initial_TGF_Proposed_cost__c from LFA_Resource__c where LFA_Service__r.LFA_Work_Plan__c = \''+WpId+'\'';
                String Status = 'LFA Proposal';
                
                 /******New Implementation Start***********/
                List<LFA_Resource__c> ResList = new List<LFA_Resource__c >([Select Id,WP_Status_text__c,LOE__c,Cost__c,Initial_TGF_Proposed_LOE__c,Initial_TGF_Proposed_cost__c from LFA_Resource__c where LFA_Service__r.LFA_Work_Plan__c =:WpId]);
                system.debug('reslist ***'+ResList );
                if(ResList.size()>0){
                    for(LFA_Resource__c res: ResList ){
                    res.WasNegotiationOrLFAAgreed__c = false;
                }
                update ResList;
                
                }
                 /******New Implementation End***********/   
                 
                BatchUpdateResourceStatus obj = new BatchUpdateResourceStatus(Query,WpId,Status);
                ID batchprocessid = Database.executeBatch(obj,100);
            }
        }catch(exception ex){
           return ex.getMessage();
        }
        return '';
    }
    
    WebService static String UpdateResourceToCreatingPurchaseOrder(String WpId) {
        try{
            if(String.IsBlank(WpId) == false){
                System.Debug('@@@@@@@@ In Class ');
                String Query = 'Select Id,LOE__c,Cost__c,Initial_TGF_Proposed_LOE__c,Initial_TGF_Proposed_cost__c from LFA_Resource__c where LFA_Service__r.LFA_Work_Plan__c = \''+WpId+'\'';
                String Status = 'Creating Purchase Order';
                BatchUpdateResourceStatus obj = new BatchUpdateResourceStatus(Query,WpId,Status);
                ID batchprocessid = Database.executeBatch(obj,100);
            }
        }catch(exception ex){
           return ex.getMessage();
        }
        return '';
    }
    
    WebService static String UpdateResourceStatusBack(String WpId) {
        try{
            if(String.IsBlank(WpId) == false){
                String Query = 'Select Id,LOE__c,Cost__c,Initial_TGF_Proposed_LOE__c,Initial_TGF_Proposed_cost__c from LFA_Resource__c where LFA_Service__r.LFA_Work_Plan__c = \''+WpId+'\'';
                String Status = 'TGF Counter-Proposal';   
                  
                /******New Implementation Start***********/
                List<LFA_Resource__c> ResList = new List<LFA_Resource__c >([Select Id,WP_Status_text__c,LOE__c,Cost__c,Initial_TGF_Proposed_LOE__c,Initial_TGF_Proposed_cost__c from LFA_Resource__c where LFA_Service__r.LFA_Work_Plan__c =:WpId]);
                system.debug('reslist ***'+ResList );
                if(ResList.size()>0){
                    for(LFA_Resource__c res: ResList ){
                    res.WasNegotiationOrLFAAgreed__c = false;
                }
                update ResList;
                
                }
                 /******New Implementation End***********/                               
                BatchUpdateResourceStatus obj = new BatchUpdateResourceStatus(Query,WpId,Status);
                ID batchprocessid = Database.executeBatch(obj,100);
              
                
                
            } 
        }catch(exception ex){
           return ex.getMessage();
        }
        return '';
    }
    WebService static String UpdateResourceStatusGMSupport(String WpId) {
        try{
            if(String.IsBlank(WpId) == false){
                String Query = 'Select Id,LOE__c,Cost__c,Initial_TGF_Proposed_LOE__c,Initial_TGF_Proposed_cost__c from LFA_Resource__c where WP_Status_text__c != \'Planned by CT\' And LFA_Service__r.LFA_Work_Plan__c = \''+WpId+'\'';
                String Status = 'Planned by CT';
                BatchUpdateResourceStatus obj = new BatchUpdateResourceStatus(Query,WpId,Status);
                ID batchprocessid = Database.executeBatch(obj,100);
            }
        }catch(exception ex){
           return ex.getMessage();
        }
        return '';
    }
    
     WebService static String UpdateResourceGeneratePdf(String WpId,String WpName,String UserFullName) {
        try{
             UpdateResourceStatus(WpId);
             postPDFContentToChatter(WpId,WpName,UserFullName);
           }
        catch(exception ex){
           return ex.getMessage();
        }
        return '';
    }
    WebService static String UpdateResourceBackGeneratePdf(String WpId,String WpName,String UserFullName) {
        try{
             UpdateResourceStatusBack(WpId);
             postPDFContentToChatter(WpId,WpName,UserFullName);
           }
        catch(exception ex){
           return ex.getMessage();
        }
        return '';
    }
    Webservice static String GenerateAgreedXLS(String WpId,String WpName,String UserFirstName,String UserLastName){
    	system.debug('entered');
        /* start */
        PageReference xls = new PageReference ('/LFA/apex/WP_Agreed_XLS?id=' +WpId);
        /* End */
        
        //PageReference xls = Page.WP_Agreed_XLS;
        try{
            xls.getParameters().put('id',WpId);
            Blob body;
            if(Test.isRunningTest()){
            }else {   
                body = xls.getContent();
            }
            Date d = System.Today();
            
            FeedItem post = new FeedItem();
            post.ParentId = WpId; 
            post.Body = UserFirstName + ' ' + UserLastName + ' agreed to the Global Fund offer';
            if(Test.isRunningTest()){
                post.ContentData = Blob.valueOf('Test');
            }else {    
                post.ContentData = body;
                system.debug('****if con****');
            }
            post.ContentFileName = WpName + ' - ' + d + '.xls';
            post.Visibility = 'AllUsers';
            insert post;            
        }catch(exception ex){
           return ex.getMessage();
        }
        return '';
    }
    Webservice static String FollowServices(String WpId){
        try{
            List<LFA_Service__c> lstService = [select Id from LFA_Service__c where LFA_Work_Plan__c=:WpId];
            if(lstService!=null && lstService.size()>0){
            
                List<EntitySubscription> lstEntitySubscriptionExisting = [select Id,ParentId from EntitySubscription 
                    where parentId IN: lstService and subscriberid =: UserInfo.getUserId()];
                Set<ID> setExistingServiceID = new Set<ID>();
                if(lstEntitySubscriptionExisting!=null && lstEntitySubscriptionExisting.size()>0){
                    for(EntitySubscription objEntitySubscriptionExisting : lstEntitySubscriptionExisting){
                        setExistingServiceID.add(objEntitySubscriptionExisting.parentId);
                    }
                }   
                List<EntitySubscription> lstEntitySubscriptionNew = new List<EntitySubscription>();
                EntitySubscription objEntitySubscriptionNew;
                For(LFA_Service__c objService : [select Id from LFA_Service__c where LFA_Work_Plan__c=:WpId]){
                    if(setExistingServiceID.contains(ObjService.ID)==false){
                        objEntitySubscriptionNew = new EntitySubscription (
                            parentId = ObjService.ID,
                            subscriberid = UserInfo.getUserId());
                        lstEntitySubscriptionNew.add(objEntitySubscriptionNew);
                    }
                }
                insert lstEntitySubscriptionNew;
            }
        }catch(exception ex){
           return ex.getMessage();
        }
        return '';
    }
}