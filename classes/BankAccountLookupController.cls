public with sharing class BankAccountLookupController {
    

    //No need to store lst in ViewState as we are doing paramter passing in javascript    
  List<Bank_Account__c> lstAccount = null;  
  String customerRecordName = 'Customer Account';
  string previousSortField = '';
  String selectedCustAcc{get;set;}
  String accountId{get;set;}
  public String role{get;set;}
  private string searchString;
  Integer pageSize = 25;
  public ApexPages.StandardSetController ssController{get;set;}
    
  public BankAccountLookupController () {
    isAscending = true;
    previousSortField = 'Name';
    SortField = 'Name';
    searchString = System.currentPageReference().getParameters().get('lksrch');
    accountId = System.currentPageReference().getParameters().get('accId');
    if(searchString != null && searchString != ''){
      this.searchText = searchString;   
    }
    QueryAccounts();          
  } 
  public string getFormTag() {
    return System.currentPageReference().getParameters().get('frm');
  }
  
  public String getAccountTextBox() {
    return System.currentPageReference().getParameters().get('txt');
  }
  
  public String getAccountId() {
    return System.currentPageReference().getParameters().get('accId');
  }
  
  public List<Bank_Account__c> getAccounts() {
    return ssController.getRecords();
  }
  
  public PageReference search() {
    isAscending = true;
    SortField = 'LastModifiedDate';
    QueryAccounts();
    return null;
  } 
  
  public void QueryAccounts() {    
    if( searchText == null) {
       NormalQuery();
       return ;  
    }
    string searchString = getSearchString();
    SearchQuery(searchString);             
  } 
  
  public Integer getListSize() {
    return ssController.getRecords().size();
  }
  
   // Query to get Account with search criteris on Name, Phone
  private void SearchQuery(string searchString) {
    string sortingOrder = 'ASC';
    string strQuery = '';
    if (isAscending == false){
      sortingOrder = 'DESC';
    }
    system.debug('*************searchString********'+ searchString);
    String textBoxId = getAccountTextBox();
      /*
        Add Field 
        Bank__r.Bank__r.Name__c
        Bank_Account_Number__c
        Bank_Account_Legal_Owner_Name__c
    */
   
    /*if(searchString != '' && searchString != null){
        //  strQuery = 'Select Id, Name FROM Contact where AccountId = \''+getAccountId()+'\' AND Role__c = \'' + role + '\' AND Name LIKE \'' + searchString + '%\' ';
     strQuery = 'Select Id, Name,  Bank__c, Bank__r.Bank__r.Name__c,Bank_Account_Name_Beneficiary__c,Bank_Account_Number__c,Bank_Account_Number_Protected__c,Bank_Account_Legal_Owner_Name__c FROM Bank_Account__c where Account__c = \''+accountId+'\' ';
    }   
    else {
        //   strQuery = 'Select Id, Name FROM Contact where AccountId = \''+getAccountId()+'\' AND Role__c = \'' + role + '\' ';
      strQuery = 'Select Id, Name, Bank__c,Bank__r.Bank__r.Name__c,Bank_Account_Name_Beneficiary__c,Bank_Account_Number__c,Bank_Account_Number_Protected__c,Bank_Account_Legal_Owner_Name__c  FROM Bank_Account__c where Account__c = \''+accountId + '\' ';
    }
    strQuery = strQuery + ' ORDER BY ' + SortField;
    strQuery = strQuery + ' ' + sortingOrder;
    strQuery = strQuery + ' NULLS LAST limit 1000';         
    system.debug(strQuery);
    Database.query( strQuery);
    ssController = new ApexPages.StandardSetController(Database.query( strQuery)); 
    ssController.setPageSize(pageSize); 
    */
     //Query Org Bank Account junction to fetch all associated bank accounts with selected vendor obtained from Account object
     /*List<Org_Bank_Account_Junc__c> juncObj = [Select id, Name , Account__c, Account__r.Vendor_ID__c,
                                                         Bank_Account__c
                                                    FROM  Org_Bank_Account_Junc__c
                                                    WHERE Account__r.Vendor_ID__c =:selectedVendorId ];
     set<Id> idSet = new Set<Id>();
     for( Org_Bank_Account_Junc__c junc: juncObj ){
         idSet.add(junc.Bank_Account__c); 
     }  */
    //First query to Account and fetch vendor Id from Account
     Account acc = [Select ParentId, Id , Name from Account where Id=:accountId];
     String selectedParentId = acc.parentId;
     
    
     List<Account> accList = [Select id, Name from Account where recordType.Name ='PR' and ParentId =:selectedParentId ]; 
     
     List<Bank_Account__c> bankAcc = new List<Bank_Account__c>();
     if(searchString != '' && searchString != null){
         strQuery = 'Select Id, Name,  Country_txt__c, Bank__c,Approval_Status__c , Bank__r.Bank__r.Name__c,Bank_Account_Name_Beneficiary__c,Bank_Account_Number__c,Bank_Account_Number_Protected__c,Bank_Account_Legal_Owner_Name__c FROM Bank_Account__c where Account__c IN : accList AND Bank__r.Bank__r.Name__c LIKE \'' + searchString + '%\'';         
     }else{
        strQuery = 'Select Id, Name, Country_txt__c, Bank__c,Approval_Status__c , Bank__r.Bank__r.Name__c,Bank_Account_Name_Beneficiary__c,Bank_Account_Number__c,Bank_Account_Number_Protected__c,Bank_Account_Legal_Owner_Name__c FROM Bank_Account__c where Account__c IN : accList ';                  
     }
     //bankAcc = [Select Id, Name,  Bank__c, Bank__r.Bank__r.Name__c,Bank_Account_Name_Beneficiary__c,Bank_Account_Number__c,Bank_Account_Number_Protected__c,Bank_Account_Legal_Owner_Name__c FROM Bank_Account__c where Id IN:idSet];                                      
     
     //strQuery = 'Select Id, Name,  Bank__c, Bank__r.Bank__r.Name__c,Bank_Account_Name_Beneficiary__c,Bank_Account_Number__c,Bank_Account_Number_Protected__c,Bank_Account_Legal_Owner_Name__c FROM Bank_Account__c where Id IN:idSet AND (Approval_Status__c =\' Approved \' OR Approval_Status__c = \'Reject\' OR Approval_Status__c = \'Update information\')';

     
     
     strQuery = strQuery + ' ORDER BY ' + SortField;
     strQuery = strQuery + ' ' + sortingOrder;
     strQuery = strQuery + ' NULLS LAST limit 1000';         
     //system.debug(strQuery);
     Database.query( strQuery);
     ssController = new ApexPages.StandardSetController(Database.query( strQuery)); 
     ssController.setPageSize(pageSize);   
    
  }
  
  
  // Query to get recently modified 20 contacts 
  private void NormalQuery() {
    string sortingOrder = 'ASC';
    string strQuery = '';
    if (isAscending == false){
      sortingOrder = 'DESC';
    }
    String textBoxId = getAccountId(); 
    //contactrole
   
     //bankAcc = [Select Id, Name,  Bank__c, Bank__r.Bank__r.Name__c,Bank_Account_Name_Beneficiary__c,Bank_Account_Number__c,Bank_Account_Number_Protected__c,Bank_Account_Legal_Owner_Name__c FROM Bank_Account__c where Id IN:idSet];                                      
     
     //strQuery = 'Select Id, Name, Approval_Status__c , Bank__c, Bank__r.Bank__r.Name__c,Bank_Account_Name_Beneficiary__c,Bank_Account_Number__c,Bank_Account_Number_Protected__c,Bank_Account_Legal_Owner_Name__c FROM Bank_Account__c where Id IN:idSet ';
     
     //strQuery = 'Select Id, Name,  Bank__c, Bank__r.Bank__r.Name__c,Bank_Account_Name_Beneficiary__c,Bank_Account_Number__c,Bank_Account_Number_Protected__c,Bank_Account_Legal_Owner_Name__c FROM Bank_Account__c where Id IN:idSet AND (Approval_Status__c =\' Approved \' OR Approval_Status__c = \'Reject\' OR Approval_Status__c = \'Update information\')';
     
     //strQuery = 'Select Id, Name, Approval_Status__c , Bank__c, Bank__r.Bank__r.Name__c,Bank_Account_Name_Beneficiary__c,Bank_Account_Number__c,Bank_Account_Number_Protected__c,Bank_Account_Legal_Owner_Name__c FROM Bank_Account__c';          
     
     Account acc = [Select ParentId, Id , Name from Account where Id=:accountId];
     String selectedParentId = acc.parentId;
     
    
     List<Account> accList = [Select id, Name from Account where recordType.Name ='PR' and ParentId =:selectedParentId ]; 
     
     List<Bank_Account__c> bankAcc = new List<Bank_Account__c>();
     if(searchString != '' && searchString != null){
         strQuery = 'Select Id, Name,Country_txt__c,  Bank__c,Approval_Status__c , Bank__r.Bank__r.Name__c,Bank_Account_Name_Beneficiary__c,Bank_Account_Number__c,Bank_Account_Number_Protected__c,Bank_Account_Legal_Owner_Name__c FROM Bank_Account__c where Account__c IN : accList AND Bank__r.Bank__r.Name__c LIKE \'' + searchString + '%\'';         
     }else{
        strQuery = 'Select Id, Name, Country_txt__c,  Bank__c,Approval_Status__c , Bank__r.Bank__r.Name__c,Bank_Account_Name_Beneficiary__c,Bank_Account_Number__c,Bank_Account_Number_Protected__c,Bank_Account_Legal_Owner_Name__c FROM Bank_Account__c where Account__c IN : accList ';                  
     }    
     strQuery = strQuery + ' ORDER BY ' + SortField;
     strQuery = strQuery + ' ' + sortingOrder;
     strQuery = strQuery + ' NULLS LAST limit 1000';
     system.debug(strQuery);
     ssController = new ApexPages.StandardSetController(Database.query( strQuery)); 
     ssController.setPageSize(pageSize); 
  }
  
  private string getSearchString() {
    if(searchText == null) {
        return '';
    }
    string input = searchText.replace('*','%');
    if(input.indexOf('%') == -1){
      input = '%' + input + '%';
    }
    return input;
  }
  public string searchText {
    get;
    set; 
  } 

  public boolean isAscending {
    get;
    set;
  }

  public string sortField {
    get;
    set {
      this.previousSortField = sortField;
      this.sortField = value;
      if(previousSortField == sortField) {
        isAscending = !isAscending;
        return;
      }
      this.isAscending = true;  
    }
  }
    
  // SORTING
  public PageReference DoSort(){
    if(searchText == null || searchText == '') {
      NormalQuery();
      return null;  
    }
    string searchString = getSearchString();
    SearchQuery(searchString);    
    return null;
  } 
  }