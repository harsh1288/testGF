/**
    Tests three documentupload_GM__c related classes
*/
@isTest
private class TestDocumentUpload_GM {
    static testMethod void testhpcmail() {
        Test.startTest();
        Account objAcc = TestClassHelper.insertAccount();        
        Implementation_Period__c objIP = new Implementation_Period__c();
        objIP.Principal_Recipient__c = objAcc.Id;        
        insert objIP;  
        Test.stopTest();
        FeedItem objFI1 = new FeedItem(); 
        objFI1.parentId = objIP.Id;
        objFI1.body = 'test';
        objFI1.Type  = 'ContentPost';
        objFI1.ContentFileName = 'TestDocument.txt';
        objFI1.ContentData =  blob.valueOf('TestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txt');
        objFI1.ContentDescription = 'Detailed Budget XLS'; 
        
        DocumentUpload_GM__c objDoc1=new DocumentUpload_GM__c();
        objDoc1.Implementation_Period__c = objIP.Id;
        objDoc1.Process_Area__c = 'Concept Note';
        objDoc1.Description__c = 'test';
        objDoc1.Language__c = 'English';
        objDoc1.GMType__c = 'List of Health Products, Quantities and Costs';
        objDoc1.Final_list_of_health_products_and_costs__c = true;
        objDoc1.Final_detailed_budget__c = false;        
        objDoc1.Security__c = 'Visible to all users';
        ApexPages.Standardcontroller sc2 = new ApexPages.Standardcontroller(objDoc1);
        ApexPages.currentPage().getParameters().put('Id',objDoc1.Implementation_Period__c);
        
        // Assert for UploadDocumentext
        UploadDocumentext objUploadDocext = new UploadDocumentext(sc2);
        objUploadDocext.linkedfilesize = 3;
        objUploadDocext.objDocumentUpload = objDoc1;
        objUploadDocext.objFeedItem = objFI1;
        objUploadDocext.uploadFile();
        objUploadDocext.blnIsExtUser = true;
        objUploadDocext.returntoIP();
        objUploadDocext.blnIsExtUser = false;
        objUploadDocext.returntoIP();       
   }
   static testMethod void testdbmail() {
        Test.startTest();
        Account objAcc = TestClassHelper.insertAccount();        
        Implementation_Period__c objIP = new Implementation_Period__c();
        objIP.Principal_Recipient__c = objAcc.Id;        
        insert objIP;
        Test.stopTest();
        FeedItem objFI1 = new FeedItem(); 
        objFI1.parentId = objIP.Id;
        objFI1.body = 'test';
        objFI1.Type  = 'ContentPost';
        objFI1.ContentFileName = 'TestDocument.txt';
        objFI1.ContentData =  blob.valueOf('TestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txt');
        objFI1.ContentDescription = 'Detailed Budget XLS';
        
         
        Profile_Access_Setting__c access = new Profile_Access_Setting__c();
        access.Name = 'UploadDocument1aad';
        access.Page_Name__c = 'UploadDocument';
        access.Profile_Name__c = 'System Administrator';
        access.Salesforce_Item__c = 'Visibletoexternal';
        insert access;
        
        DocumentUpload_GM__c objDoc1=new DocumentUpload_GM__c();
        objDoc1.Implementation_Period__c = objIP.Id;
        objDoc1.Process_Area__c = 'Concept Note';
        objDoc1.Description__c = 'test';
        objDoc1.Language__c = 'English';
        objDoc1.GMType__c = 'Concept note narrative';
        objDoc1.Final_list_of_health_products_and_costs__c = false;
        objDoc1.Final_detailed_budget__c = false;        
        objDoc1.Security__c = 'Visible to TGF internal only';
        ApexPages.Standardcontroller sc2 = new ApexPages.Standardcontroller(objDoc1);
        ApexPages.currentPage().getParameters().put('Id',objDoc1.Implementation_Period__c);
        
        // Assert for UploadDocumentext
        UploadDocumentext objUploadDocext = new UploadDocumentext(sc2);
        objUploadDocext.linkedfilesize = 3;
        objUploadDocext.strWarning = '';
        objUploadDocext.objDocumentUpload = objDoc1;
        objUploadDocext.objFeedItem = objFI1;
        objUploadDocext.uploadFile();
        objUploadDocext.linkedfilesize=0;
        objUploadDocext.uploadFile();
        objUploadDocext.blnIsExtUser = true;
        objUploadDocext.returntoIP();
        objUploadDocext.blnIsExtUser = false;
        objUploadDocext.returntoIP();
    }
    static testMethod void testdbmailException() {
        Test.startTest();
        Account objAcc = TestClassHelper.insertAccount();        
        Implementation_Period__c objIP = new Implementation_Period__c();
        objIP.Principal_Recipient__c = objAcc.Id;        
        insert objIP;
        Test.stopTest();
        FeedItem objFI1 = new FeedItem(); 
        objFI1.parentId = objIP.Id;
        objFI1.body = '@@@@@@@@';
        objFI1.Type  = '';
        objFI1.ContentFileName = '@@@@@@';
        objFI1.ContentData =  blob.valueOf('');
        objFI1.ContentDescription = 'Detailed Budget XLS';        
        
        DocumentUpload_GM__c objDoc1=new DocumentUpload_GM__c();
        objDoc1.Implementation_Period__c = objIP.Id;
        objDoc1.Process_Area__c = 'Concept Note';
        objDoc1.Description__c = 'test';
        objDoc1.Language__c = 'English';
        objDoc1.GMType__c = 'Detailed Budget';
        objDoc1.Final_list_of_health_products_and_costs__c = false;
        objDoc1.Final_detailed_budget__c = true;    
        objDoc1.Security__c = 'Visible to LFA and TGF internal only';    
        ApexPages.Standardcontroller sc2 = new ApexPages.Standardcontroller(objDoc1);
        ApexPages.currentPage().getParameters().put('Id',objDoc1.Implementation_Period__c);            
        // Assert for UploadDocumentext
        UploadDocumentext objUploadDocext = new UploadDocumentext(sc2);
        objUploadDocext.linkedfilesize = null;
        objUploadDocext.strWarning = '';
        objUploadDocext.objDocumentUpload = objDoc1;
        objUploadDocext.objFeedItem = objFI1;
        objUploadDocext.uploadFile();
        objUploadDocext.linkedfilesize=0;
        objUploadDocext.uploadFile();
        objUploadDocext.blnIsExtUser = true;
        objUploadDocext.returntoIP();
        objUploadDocext.blnIsExtUser = false;
        objUploadDocext.returntoIP();
    }
    static testMethod void forextDocumentUploadandTemplate() {
        Test.startTest();
        Account objAcc = TestClassHelper.insertAccount();        
        Implementation_Period__c objIP = new Implementation_Period__c();
        objIP.Principal_Recipient__c = objAcc.Id;        
        insert objIP;
        Test.stopTest();
        FeedItem objFI1 = new FeedItem(); 
        objFI1.parentId = objIP.Id;
        objFI1.body = '@@@@@@@@';
        objFI1.Type  = '';
        objFI1.ContentFileName = '@@@@@@';
        objFI1.ContentData =  blob.valueOf('');
        objFI1.ContentDescription = 'Detailed Budget XLS';        
        
        DocumentUpload_GM__c objDoc1=new DocumentUpload_GM__c();
        objDoc1.Implementation_Period__c = objIP.Id;
        objDoc1.Process_Area__c = 'Concept Note';
        objDoc1.Description__c = 'test';
        objDoc1.Language__c = 'English';
        objDoc1.GMType__c = 'Detailed Budget';
        objDoc1.Final_list_of_health_products_and_costs__c = false;
        objDoc1.Final_detailed_budget__c = true;    
        objDoc1.Security__c = 'Visible to LFA and TGF internal only';    
        insert objDoc1;
        // Test for downloadTemplate
        ApexPages.Standardcontroller sc2 = new ApexPages.Standardcontroller(objDoc1);
        downloadTemplate objdownloadTemplate = new downloadTemplate(sc2);
        objdownloadTemplate.SelectedTemplate = Label.Detailed_budget_Template_english;
        objdownloadTemplate.downloadTemplate();        
    }
    static testMethod void forDownloadTemplateNull() {
        Test.startTest();
        Account objAcc = TestClassHelper.insertAccount();        
        Implementation_Period__c objIP = new Implementation_Period__c();
        objIP.Principal_Recipient__c = objAcc.Id;        
        insert objIP;
        
        Profile_Access_Setting__c access = new Profile_Access_Setting__c();
        access.Name = 'UploadDocument1aab';
        access.Page_Name__c = 'UploadDocument';
        access.Profile_Name__c = 'System Administrator';
        access.Salesforce_Item__c = 'Visibletoall';
        insert access;
        
        FeedItem objFI1 = new FeedItem(); 
        objFI1.parentId = objIP.Id;
        objFI1.body = 'test';
        objFI1.Type  = 'ContentPost';
        objFI1.ContentFileName = 'TestDocument.txt';
        objFI1.ContentData =  blob.valueOf('TestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txt');
        objFI1.ContentDescription = 'Detailed Budget XLS'; 
        
        DocumentUpload_GM__c objDoc1=new DocumentUpload_GM__c();
        objDoc1.Implementation_Period__c = objIP.Id;
        objDoc1.Process_Area__c = 'Concept Note';
        objDoc1.Description__c = 'test';
        objDoc1.Language__c = 'English';
        objDoc1.GMType__c = 'Concept note narrative';
        objDoc1.Final_list_of_health_products_and_costs__c = false;
        objDoc1.Final_detailed_budget__c = false;
        objDoc1.Security__c = 'Visible to TGF internal only';
        insert objDoc1; 
        Test.stopTest();
        ApexPages.Standardcontroller sc2 = new ApexPages.Standardcontroller(objDoc1);
        ApexPages.currentPage().getParameters().put('Id',objDoc1.Implementation_Period__c);
        
        // Test for downloadTemplate
        downloadTemplate objdownloadTemplate = new downloadTemplate(sc2);
        objdownloadTemplate.SelectedTemplate = null;
        objdownloadTemplate.downloadTemplate();        
    }
    
    static testMethod void forLFA() {
        Test.startTest();
        
        Account objAcc = TestClassHelper.insertAccount();        
        
        Contact Con = new Contact();
        con.FirstName = 'Test Contact';
        con.External_User__c=true;
        con.LastName = 'Testing';
        Con.email = 'standarduser@testorg.com';
        con.accountid= objAcc.id;
        con.recordtypeid = Label.LFA_Contact_RT;
        insert con;
        
        list<npe5__Affiliation__c> AffAccCon = [select Id,npe5__Status__c from npe5__Affiliation__c where npe5__Organization__c =:objAcc.id AND npe5__Contact__c =:con.id];
        AffAccCon[0].npe5__Status__c = 'former';
        update AffAccCon[0];
            
        npe5__Affiliation__c objAff = new npe5__Affiliation__c();
        objAff.Is_Inactive_c__c = False;
        objAff.Access_Level__c = 'Read/Write';
        objAff.Recordtypeid =label.PR_Affiliation_RT;
        objAff.npe5__Organization__c  = objAcc.Id;
        objAff.npe5__Contact__c  = con.Id;
        insert objAff;
        
        Profile_Access_Setting__c access = new Profile_Access_Setting__c();
        access.Name = 'UploadDocument1aaa';
        access.Page_Name__c = 'UploadDocument';
        access.Profile_Name__c = 'LFA Portal User';
        access.Salesforce_Item__c = 'Visibletolfa';
        insert access;
        
        Profile profileId=[Select Id,Name from Profile where Name = 'LFA Portal User'];
        
        User user1 = new User();
        user1.ProfileID = profileId.Id;
        user1.EmailEncodingKey = 'ISO-8859-1';
        user1.LanguageLocaleKey = 'en_US';
        user1.TimeZoneSidKey = 'America/New_York';
        user1.LocaleSidKey = 'en_US';
        user1.FirstName = 'first';
        user1.LastName = 'last';
        user1.Username = 'test@appirio.com';   
        user1.CommunityNickname = 'testUser123';
        user1.Alias = 't1';
        user1.Email = 'no@email.com';
        user1.IsActive = true;
        user1.ContactId = con.Id;

        insert user1;
        
        Implementation_Period__c objIP = new Implementation_Period__c();
        objIP.Principal_Recipient__c = objAcc.Id;        
        insert objIP;
        FeedItem objFI1 = new FeedItem(); 
        objFI1.parentId = objIP.Id;
        objFI1.body = 'test';
        objFI1.Type  = 'ContentPost';
        objFI1.ContentFileName = 'TestDocument.txt';
        objFI1.ContentData =  blob.valueOf('TestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txtTestDocument.txt');
        objFI1.ContentDescription = 'Detailed Budget XLS'; 
        
        System.runAs(user1 ){
         
            DocumentUpload_GM__c objDoc1=new DocumentUpload_GM__c();
            objDoc1.Implementation_Period__c = objIP.Id;
            objDoc1.Process_Area__c = 'Concept Note';
            objDoc1.Description__c = 'test';
            objDoc1.Language__c = 'English';
            objDoc1.GMType__c = 'Concept note narrative';
            objDoc1.Final_list_of_health_products_and_costs__c = false;
            objDoc1.Final_detailed_budget__c = false;
            objDoc1.Security__c = 'Visible to TGF internal only';
            insert objDoc1; 
            Test.stopTest();
            ApexPages.Standardcontroller sc2 = new ApexPages.Standardcontroller(objDoc1);
            ApexPages.currentPage().getParameters().put('Id',objDoc1.Implementation_Period__c);
            
            // Test for downloadTemplate
            downloadTemplate objdownloadTemplate = new downloadTemplate(sc2);
            objdownloadTemplate.SelectedTemplate = null;
            objdownloadTemplate.downloadTemplate();        
        }
    }
}