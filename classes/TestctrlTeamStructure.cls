/*********************************************************************************
* {Test} Class: TestctrlTeamStructure
* Created by {DeveloperName},  {DateCreated 07/26/2013}
----------------------------------------------------------------------------------
* Purpose/Methods:
* - Used for cover all methods of ctrlTeamStructure Controller.
*
----------------------------------------------------------------------------------
* History:
* - VERSION  DEVELOPER NAME    DATE            DETAIL FEATURES
   1.0                        07/26/2013
*********************************************************************************/
@isTest
Public Class TestctrlTeamStructure{
     Public static testMethod Void TestctrlTeamStructure(){
        Account objAcc = new Account();
        objAcc.Name = 'Test Acc';
        insert objAcc;
        
        Country__c objCountry = new Country__c();
        objCountry.Name = 'Test Country';
        objCountry.Region_Name__c ='Test Region';
        insert objCountry;
        
        LFA_Work_Plan__c objWp = new LFA_Work_Plan__c();
        objWp.Name = 'Test Wp';
        objWp.LFA__c = objAcc.Id;
        objWp.Country__c = objCountry.id;
        insert objWp;
        
        Contact objCon = new Contact();
        objCon.AccountId = objAcc.Id;
        objCon.FirstName = 'Test FirstName';
        objCon.LastName = 'Test LastName';        
        objCon.Active_LFA_Member__c = 'yes';
        objCon.Level_of_Experience__c = 'test';
        insert objCon;
        
        Contact objCon1 = new Contact();
        objCon1.AccountId = objAcc.Id;
        objCon1.FirstName = 'Test FirstName';
        objCon1.LastName = 'Test LastName';        
        objCon1.Active_LFA_Member__c = 'yes';
        objCon1.Level_of_Experience__c = 'test';
        insert objCon1;
        
        LFA_Role__c objLFARole = new LFA_Role__c();
        objLFARole.Name = 'test';
        insert objLFARole;
        
        Rate__c objRate = new Rate__c();
        objRate.Contact__c = objCon.Id;
        objRate.LFA_Role__c =objLFARole.Id;
        objRate.Rate__c = 100.00;
        objRate.Next_Year_Daily_Rate__c =200.00;
        objRate.Other_LFA_Role__c = 'test';
        objRate.LFA_Location__c = 'yes';
        objRate.GF_Approved__c = 'yes';
        objRate.Country__c = objCountry.Id;
        insert objRate;
        
        Apexpages.currentpage().getparameters().put('WPId',objWp.id);
        ctrlTeamStructure  objClass = new ctrlTeamStructure();
        objClass.strAccId = objAcc.Id;
        objClass.ContactId=objCon.id;
        objClass.strRateYear = 'Current Year';
        
        objClass.getRateYear();
        objClass.ShowInactive();
        objClass.HideInactive();
        objClass.addContactRateRow();
        objClass.Cancel();
        objClass.saveContact();
        objClass.setSortDirection('Asc');
        objClass.sortExpression = 'Contact__c';
        objClass.ViewData();
        
        Apexpages.currentpage().getparameters().put('AddConId',objCon.id);        
        objClass.addContactRow();
        
        Apexpages.currentpage().getparameters().put('AddConId',objCon1.id);        
        objClass.addContactRow(); 
        
        Apexpages.currentpage().getparameters().put('RateId',objRate.id);
        
        objClass.SaveChangeRequestTS();
        objClass.SaveChangeRequest();
        
        Apexpages.currentpage().getparameters().put('DeleteConRateIndex','0');
        Apexpages.currentpage().getparameters().put('DeleteRateIndex','0');
        
        objClass.DeleteRates();
     }
     
     Public static testMethod Void TestctrlTeamStructure1(){
        Account objAcc = new Account();
        objAcc.Name = 'Test Acc';
        insert objAcc;
        
        Country__c objCountry = new Country__c();
        objCountry.Name = 'Test Country';
        objCountry.Region_Name__c ='Test Region';
        insert objCountry;
        
        LFA_Work_Plan__c objWp = new LFA_Work_Plan__c();
        objWp.Name = 'Test Wp';
        objWp.LFA__c = objAcc.Id;
        objWp.Country__c = objCountry.id;
        insert objWp;
        
        Contact objCon = new Contact();
        objCon.AccountId = objAcc.Id;
        objCon.FirstName = 'Test FirstName';
        objCon.LastName = 'Test LastName';        
        objCon.Active_LFA_Member__c = 'yes';
        objCon.Level_of_Experience__c = 'test';
        insert objCon;
        
        Contact objCon1 = new Contact();
        objCon1.AccountId = objAcc.Id;
        objCon1.FirstName = 'Test FirstName';
        objCon1.LastName = 'Test LastName';        
        objCon1.Active_LFA_Member__c = 'yes';
        objCon1.Level_of_Experience__c = 'test';
        insert objCon1;
        
        LFA_Role__c objLFARole = new LFA_Role__c();
        objLFARole.Name = 'test';
        insert objLFARole;
        
        Rate__c objRate = new Rate__c();
        objRate.Contact__c = objCon.Id;
        objRate.LFA_Role__c =objLFARole.Id;
        objRate.Rate__c = 100.00;
        objRate.Next_Year_Daily_Rate__c =200.00;
        objRate.Other_LFA_Role__c = 'test';
        objRate.LFA_Location__c = 'yes';
        objRate.GF_Approved__c = 'yes';
        objRate.Country__c = objCountry.Id;
        insert objRate;
        
        Apexpages.currentpage().getparameters().put('WPId',objWp.id);
        ctrlTeamStructure  objClass = new ctrlTeamStructure();
        objClass.strAccId = objAcc.Id;
        objClass.ContactId=objCon.id;
        objClass.strRateYear = 'Next Year';
        
        objClass.getRateYear();
        objClass.ShowInactive();
        objClass.HideInactive();
        objClass.addContactRateRow();
        objClass.Cancel();
        objClass.saveContact();
        objClass.setSortDirection('Asc');
        objClass.sortExpression = 'Contact__c';
        objClass.ViewData();
        objClass.FillListOnSelectYear();
        
        Apexpages.currentpage().getparameters().put('AddConId',objCon.id);        
        objClass.addContactRow();
        
        Apexpages.currentpage().getparameters().put('AddConId',objCon1.id);        
        objClass.addContactRow(); 
        
        Apexpages.currentpage().getparameters().put('RateId',objRate.id);
        objClass.SaveChangeRequestTS();
        objClass.SaveChangeRequest();
        
        Apexpages.currentpage().getparameters().put('DeleteConRateIndex','0');
        Apexpages.currentpage().getparameters().put('DeleteRateIndex','0');
        
        objClass.DeleteRates();
     }
}