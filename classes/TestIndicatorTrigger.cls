/**
 * 
 */
@isTest
private class TestIndicatorTrigger {

    static testMethod void myUnitTest() {
    	Account objacc = TestClassHelper.insertAccount();
      
      	Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
        insert objGrant;
      
      	Implementation_period__c objimp = TestClassHelper.createIPWithConceptNote(objGrant,objAcc);
        insert objimp;
      
      	Performance_Framework__c objpf = TestClassHelper.createPF(objimp);
        insert objpf;   
        
        Module__c objmod = TestClassHelper.createModule();
        objmod.Implementation_Period__c = objimp.id;
        objmod.Performance_Framework__c = objpf.id;
        insert objmod;         
     
       	Indicator__c objindcoverage = TestClassHelper.createCatalogIndicator();
        objindcoverage.Indicator_Type__c ='Coverage/Output';
        objindcoverage.Is_Disaggregated__c = true;
        insert objindcoverage;
        
        Catalog_Disaggregated__c objCDAG = new Catalog_Disaggregated__c();
        objCDAG.Catalog_Indicator__c =  objindcoverage.Id;
        objCDAG.Disaggregation_Category__c = 'TestCategory';
        objCDAG.Disaggregation_Value__c = 'TestValue';
       	insert objCDAG;
       	
       	List<RecordType> lstrec = [select id,Name from Recordtype where sObjectType='Grant_Indicator__c'];
       	
       	Grant_Indicator__c objgindcoverage = TestClassHelper.createGrantIndicator();
        objgindcoverage.Grant_Implementation_Period__c = objimp.id;
        objgindcoverage.Performance_Framework__c = objpf.id;
        objgindcoverage.Standard_or_Custom__c = 'Standard';
        objgindcoverage.Indicator__c =  objindcoverage.id;
        objgindcoverage.Data_Type__c =  'Percent' ;
        objgindcoverage.Decimal_Places__c = '2';
        objgindcoverage.Indicator_Type__c = 'Coverage/Output';
        objgindcoverage.Parent_Module__c = objmod.id;
        for(RecordType objrec :lstrec){
        	if(objrec.name == 'Coverage/Output_IP')
            	objgindcoverage.RecordTypeId = objrec.id;
        }
        objgindcoverage.Reporting_Frequency__c = 'Based on Reporting Frequency';
        insert objgindcoverage;
    }
}