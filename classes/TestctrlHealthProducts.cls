@isTest
Public Class TestctrlHealthProducts{
    Public static testMethod void TestctrlHealthProductsmethod(){
    
        Account objAcc = TestClassHelper.insertAccount();
         
        Grant__c objgrant = TestClassHelper.createGrant(objAcc);
        insert objgrant;
        
        Concept_Note__c objcn = TestClassHelper.createCN();
        insert objcn;
        
        Implementation_Period__c objimp = TestClassHelper.createIP(objGrant, objAcc);
        objimp.Principal_Recipient__c = objAcc.Id;
        objimp.Grant__c = objGrant.Id;
        objimp.Concept_Note__c = objcn.Id;
        objimp.Implementation_Period_Status__c = 'Not Yet Submitted';
        objimp.GFS_Task_Name__c = '1.1 - Phase 1';
        objimp.Status__c = 'Grant-Making';
        objimp.Budgeting_Level__c = 'Budget at cost input level';
        objimp.Implementation_Cycle__c = '1';
        objimp.Reporting__c = 'Yearly';
        objimp.PR_Fiscal_Cycle__c = 'January-December';
        insert objimp;
       
        Performance_Framework__c objpf = TestClassHelper.createPF(objimp);
        insert objpf;
        
        IP_Detail_Information__c objipdetail = TestClassHelper.createIPDetail();
        objipdetail.Implementation_Period__c = objimp.Id;
        insert objipdetail;
        
        
        HPC_Framework__c objHpc = TestClassHelper.createHPC();
        objHpc.Grant_Implementation_Period__c = objimp.Id;
        objHpc.HPC_Status__c = 'Not Yet Submitted';
        insert objHpc;
        
        
        Guidance__c objgd = TestClassHelper.createGuidance();
        objgd.Name = 'HPC Framework Guidance';
        insert objgd;
        
        List<Catalog_Cost_Input__c> cciList = new List<Catalog_Cost_Input__c>();
        Catalog_Cost_Input__c objcatcip = TestClassHelper.createCostInput();
        objcatcip.Disease_Impact__c = 'Malaria';
        objcatcip.Product_Category__c = 'Pharmaceutical';
        objcatcip.PSM__c = true;
        objcatcip.Is_Inactive__c = false;
        cciList.add(objcatcip);
        
        Catalog_Cost_Input__c objcatcip1 = new  Catalog_Cost_Input__c();
        objcatcip1.Disease_Impact__c = 'Malaria';
        objcatcip1.Name = 'test PSM';
        objcatcip1.GIS_ID__c = 47;
        objcatcip1.PSM__c = true;
        objcatcip1.Is_Inactive__c = false;
        objcatcip1.Cost_Grouping__c = '7. Procurement and Supply-Chain Management costs (PSM)';
        objcatcip1.Product_Category__c = 'PSM Costs';
        cciList.add(objcatcip1);
        insert cciList;
        
        Catalog_Generic_Product__c catgp = new Catalog_Generic_Product__c();
        catgp.Name = 'Amoxicillin';
        catgp.Product_Sub_Category__c = 'Anti-malaria medicine';
        catgp.Catalog_Cost_Input__c = objcatcip.id;
        catgp.GIS_ID__c = 107006;
        catgp.Is_Inactive__c = false;
        insert catgp;
        
        Catalog_Product__c objcatp = new Catalog_Product__c();
        objcatp.Product_Name__c = 'Amoxicillin 500mg powder for inj Ampoule/vial - 20 * 500mg';
        objcatp.Generic_Product__c = catgp.Id;
        objcatp.Catalog_Cost_Input__c = objcatcip.id;
        objcatp.GIS_ID__c = 3530;
        insert objcatp;
        
        Module__c objmod = TestClassHelper.createModule();
        objmod.Performance_Framework__c = objpf.Id;
        objmod.Implementation_Period__c = objimp.Id;
        insert objmod;
        
        List<Grant_Intervention__c> giList = new List<Grant_Intervention__c>();
        Grant_Intervention__c objgin = TestClassHelper.createGrantIntervention(objimp);
        objgin.Performance_Framework__c = objpf.Id;
        objgin.Implementation_Period__c = objimp.Id;
        objgin.Module__c = objmod.Id;
        giList.add(objgin);
        
        Grant_Intervention__c objgin1 = TestClassHelper.createGrantIntervention(objimp);
        objgin1.Performance_Framework__c = objpf.Id;
        objgin1.Implementation_Period__c = objimp.Id;
        objgin1.Module__c = objmod.Id;
        objgin1.Custom_Intervention_Name__c = 'test intervention';
        giList.add(objgin1);
        insert giList;
        
        List<Product__c> prodList = new List<Product__c>();
        Product__c objproduct = new Product__c();
        objproduct.Catalog_Product__c = objcatp.Id;
        objproduct.Cost_Input__c = objcatcip.Id;
        objproduct.Currency_Used__c = 'Grant Currency';
        objproduct.Module__c = objmod.Id;
        objproduct.Grant_Intervention__c = objgin.Id;
        objproduct.Health_Products_and_Costs__c = objHpc.Id;
        objproduct.Procurement_Channel__c = 'GDF';
        objproduct.Product_Main_Category__c = 'Pharmaceutical';
        objproduct.Unit_Cost_Y1__c = 2;
        objproduct.Quantity_Q1__c = 2;
        prodList.add(objproduct);
        
        Product__c objproduct1 = new Product__c();
        objproduct1.Catalog_Product__c = objcatp.Id;
        objproduct1.Cost_Input__c = objcatcip.Id;
        objproduct1.Currency_Used__c = 'Grant Currency';
        objproduct1.Module__c = objmod.Id;
        objproduct1.Grant_Intervention__c = objgin1.Id;
        objproduct1.Health_Products_and_Costs__c = objHpc.Id;
        objproduct1.Procurement_Channel__c = 'GDF';
        objproduct1.Product_Main_Category__c = 'Pharmaceutical';
        prodList.add(objproduct1);
        insert prodList;
        
        Implementer__c objimpl = TestClassHelper.createImplementor();
        objimpl.Implementer_Role__c = 'Principal Recipient';
        objimpl.Performance_framework__c = objpf.Id;
        insert objimpl;        
        
        Profile_Access_Setting__c objpracsstng = TestClassHelper.createProfileSetting();
        objpracsstng.Page_Name__c = 'HealthProducts';
        objpracsstng.Salesforce_Item__c = 'External Profile';
        objpracsstng.Status__c = objHpc.HPC_Status__c;
        insert objpracsstng;
        
        Test.startTest();
        Apexpages.currentpage().getparameters().put('Id',objHpc.Id);       
        ApexPages.StandardController sc = new ApexPages.StandardController(objHpc);
        ctrlHealthProducts objhpcpage = new ctrlHealthProducts(sc); 
        ctrlHealthProducts.wrapProducts wrapProducts = new ctrlHealthProducts.wrapProducts();
       
        wrapProducts.objProduct = objproduct;
        objhpcpage.lstProducts.add(wrapProducts);
        objhpcpage.fillCatagory();
                
        objhpcpage.strSelectedCatagory =  objcatcip.Product_Category__c;
        objhpcpage.selectedCatagory();
        
        Apexpages.currentpage().getparameters().put('DeleteIndex','1');
        objhpcpage.strPageId = objHpc.Id;
        objhpcpage.DeleteProduct();
        
        objhpcpage.strSelectedProdCatagory = objcatp.Catalog_Cost_Input__c;
        objhpcpage.strSelectedProdName = objcatp.Generic_Product__c;
        objhpcpage.strSelectedSpecification = objcatp.Id;
        
        
        objhpcpage.FillModules();
        
        objhpcpage.objNewProduct.strModuleName = objmod.Id;
        objhpcpage.FillModuleInterventions();
        
        Apexpages.currentpage().getparameters().put('ModId',objmod.Id);
        Apexpages.currentpage().getparameters().put('indexMod','0');
        objhpcpage.FillModuleInterventionsNew();
        
        objhpcpage.strComponent = 'Malaria';
        objhpcpage.FillCostInput();
        objhpcpage.fillProductName();
        //objhpcpage.FillCostInputPSMCost();
        
        objhpcpage.strSelectedProdName = objcatp.Generic_Product__c;
        objhpcpage.fillSpecifications();
        
        Apexpages.currentpage().getparameters().put('EditIndex','0');
        objhpcpage.lstProducts[0].strStrengthAndUnit = objcatp.Specification__c;
        objhpcpage.EditProduct();
        
        Apexpages.currentpage().getparameters().put('CancelIndex','0');
        objhpcpage.lstProducts[0].strStrengthAndUnit = objcatp.Specification__c;
        objhpcpage.CancelProduct();
        
        Apexpages.currentpage().getparameters().put('ChangeIndex','0');
        Apexpages.currentpage().getparameters().put('GenericProductId',catgp.Id);
        objhpcpage.fillStrenthAndUnit();
        
        Apexpages.currentpage().getparameters().put('ChangeIndexGP','0');
        Apexpages.currentpage().getparameters().put('CostInputId',objcatcip.Id);
        objhpcpage.fillGenericProduct();
        
        objhpcpage.objNewProduct.strCostInputName = objcatcip.Id;
        objhpcpage.fillGenericProductNew();
        
        objhpcpage.objNewProduct.strGenericProductName = catgp.Id;
        objhpcpage.fillStrenthAndUnitNew();
        
        
        
        Apexpages.currentpage().getparameters().put('SaveIndex','0');
        Apexpages.currentpage().getparameters().put('CPId',objcatp.Id);
        Apexpages.currentpage().getparameters().put('CostInputId',objcatcip.Id);
        objhpcpage.Cost_Input_ID = objcatcip.Id;
        objhpcpage.lstProducts[0].strModuleName = objmod.Id;
        objhpcpage.lstProducts[0].strInterventionName = objgin.Id;
        objhpcpage.SaveProduct();
        
        Apexpages.currentpage().getparameters().put('OpenBLIndex','0');
        objhpcpage.OpenDialogeBoxRecord();
        
        Apexpages.currentpage().getparameters().put('SaveBLIndex','0');
        objhpcpage.SaveDialogeBox();
        
        Apexpages.currentpage().getparameters().put('CancelBLIndex','0');
        objhpcpage.CancelDialogeBox();
        
        objhpcpage.lstProducts[0].selectedValues = true;
        objhpcpage.EditPage();
        objhpcpage.SaveDetails();
        objhpcpage.lstProducts[0].selectedValues = true;
        objhpcpage.CancelDetails();
        objhpcpage.lstProducts[0].selectedValues = true;
        objhpcpage.DeleteSelected();
                        
        objhpcpage.strSelectedCatagory =  objcatcip.Product_Category__c;
        objhpcpage.selectedCatagory();
        
        Apexpages.currentpage().getparameters().put('SaveIndex','1');
        Apexpages.currentpage().getparameters().put('CPId',objcatp.Id);
        Apexpages.currentpage().getparameters().put('CostInputId',objcatcip.Id);
        objhpcpage.Cost_Input_ID = objcatcip.Id;
        objhpcpage.SaveProduct();		
        
        Test.stopTest();        
    }
}