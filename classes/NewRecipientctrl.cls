public class NewRecipientctrl {
   public boolean display{get;set;}
   public boolean display1{get;set;}
   public string message{get;set;}
   public string pfid{get;set;}
   public boolean displaypb{get;set;}
   public List<wrapimplementer> lstwrapimp{get;set;}
   public List<Implementer__c > lstimplementer = new List<Implementer__c >();
   public List<Implementer__c > newimplementer = new List<Implementer__c >();
   public List<Performance_Framework__c> pflist= new List<Performance_Framework__c >();
   Public List<Account> AccList = new List<Account>();
   Public Implementer__c imp{get;set;}
   Public Implementer__c im ;
   Public Implementer__c c{get;set;}
   Public Implementer__c objImp{get;set;}
   public string baseUrl {get;set;}
   public string checkvalue {get;set;} 
   public static string checkvalue1 {get;set;} 
   public Boolean bool {get;set;}
   Public List<SelectOption> AccountOptions {get;set;}
   Public String strCountry {get;set;}
   Public String strImplementationPeriodId {get;set;}
   Public Id IPPrincipalRecipient {get;set;}
   Public Performance_Framework__c strPageId {get;set;}
   Public  boolean selected{get;set;}
   Public String subRec{get;set;}
   Public Account acc{get;set;}
   Public Id accountRT;
   Public Implementer__c po {get;set;}
   public List<Account> selectedaccountslst{get;set;}
   public boolean errormessage{get;set;}
   public List<Account> newacctlst{get;set;}

   
   public NewRecipientctrl(ApexPages.StandardSetController controller) {
       
        pfid =  ApexPages.currentPage().getParameters().get('id');
        strPageId = [select Implementation_Period__c,Implementation_Period__r.Principal_Recipient__c,Implementation_Period__r.Principal_Recipient__r.Country__c from Performance_Framework__c where id=: pfid limit 1 ];
        strCountry = strPageId.Implementation_Period__r.Principal_Recipient__r.Country__c;
        strImplementationPeriodId = strPageId.Implementation_Period__c;      
        IPPrincipalRecipient = strPageId.Implementation_Period__r.Principal_Recipient__r.Id;
        system.debug('@@prid'+IPPrincipalRecipient );
        AccountOptions = new List<SelectOption>();
        List<Account> lstAccount= [Select Id,Name,Sector__c,Type__c,Short_Name__c,Sub_Type__c 
                                   From Account Where Country__c =: strCountry and (RecordType.DeveloperName =: 'PR' Or RecordType.DeveloperName =: 'Implementer_Non_PR')
                                   and ID Not IN (select  Account__c From Implementer__c Where Performance_framework__c =: pfid )
                                   AND ID !=: IPPrincipalRecipient]; //strImplementationPeriodId
        system.debug('@@Accountlst'+lstAccount);             
        pflist = [select id,name,Implementation_Period__c,Implementation_Period__r.Principal_Recipient__c from Performance_Framework__c where id =:pfid ];
        lstimplementer = [select Id,Name ,Implementer_Name__c ,Principal_Recipient__c,Account__c from Implementer__c where Principal_Recipient__c =: pflist[0].Implementation_Period__r.Principal_Recipient__c];
        RecordType objAccRT = [Select Id From RecordType Where sobjectType = 'Account' And DeveloperName = 'Implementer_Non_PR' limit 1];
        
        if(objAccRT.Id!= null)
        accountRT = objAccRT.Id;
        
         //Adding  to wrapper Class  
            lstwrapimp= new List<wrapimplementer>();
            if(lstimplementer.size()>0){
               for(Account implementer1 : lstAccount){
                   lstwrapimp.add(new wrapimplementer(implementer1));                     
               }
              lstwrapimp.add(new wrapimplementer(null)); 
            }
            
            system.debug('@@Accountlst'+lstwrapimp);  
        Imp = (Implementer__c)controller.getRecord();
      
        string userTypeStr = UserInfo.getUserType();
        baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
        if(userTypeStr == 'Standard') {
            baseUrl = baseUrl+'/';
        }
        else 
            baseUrl = baseUrl+'/GM/';
       selectedaccountslst = new List<Account>();
    }



    Public pageReference Createimplementer(){ 
        pageReference pr;
         newacctlst = new List<Account>();
        for(wrapimplementer objimpl: lstwrapimp){
             if(checkvalue1 == '' && objimpl.selected == True && (objimpl.wrapimplementer.Name == '' ||objimpl.wrapimplementer.Name == null) ) {
                displaypb = false;
                errormessage= true;
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'If other is selected please enter Name');//Same old drama 
                ApexPages.addMessage(myMsg);
             }  
             else if(objimpl.selected == true){
              system.debug('@@@@selectimpl'+objimpl.wrapimplementer);                     
                selectedaccountslst.add(objimpl.wrapimplementer); 
             }
        }
        if(selectedaccountslst.size() == 0){
                errormessage= true;
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please select atleast one PR');//Same old drama 
                ApexPages.addMessage(myMsg);
        }
        else{
            for(Account objimpl :selectedaccountslst){
              system.debug('@@@@selectedpr'+objimpl);
              system.debug('@@@@checkvalue1'+checkvalue1);
                  if(checkvalue1!= '' && objimpl == Null){
                      displaypb = true;
                      errormessage= true;
                      Imp.Implementer_Name__c = checkvalue1;
                       Account objacct = new Account();
                          objacct.Name = checkvalue1;
                          objacct.RecordTypeId = accountRT;
                          objacct.Country__c=  strCountry;
                        newacctlst.add(objacct); 
                  }
                  else{
                      displaypb = false;
                      errormessage= false;
                      Implementer__c newimpl = new implementer__c();
                         newimpl.Implementer_Name__c =  objimpl.Name ;
                         newimpl.Grant_Implementation_Period__c = strImplementationPeriodId ;
                         newimpl.performance_framework__c = pfid;
                         newimpl.PR_Type__c = objimpl.Type__c;
                         newimpl.Implementer_Short_Name__c = objimpl.Short_Name__c; 
                         newimpl.Implementer_Sub_Type__c = objimpl.Sub_Type__c;
                         newimpl.Account__c = objimpl.id;
                       newimplementer.add(newimpl); 
                      pr = new Pagereference(baseUrl+pfid);   
                      pr.setRedirect(true);
                  }
            }
            insert newimplementer;
            insert newacctlst;
         }
         if(errormessage == false){
           return pr;
         }
         else{
           return Null;
         }
              
    }
   
    Public Pagereference Saveimplementer(){
    newimplementer = new List<Implementer__c>();
      for(Account objacct:newacctlst){
        Implementer__c objimp = new Implementer__c();
         objimp.Implementer_Name__c = objacct.name;
         objimp.Grant_Implementation_Period__c = strImplementationPeriodId ;
         objimp.Account__c= objacct.id;
         objimp.performance_framework__c = pfid;
         objimp.PR_Type__c = Imp.PR_Type__c ;
         objimp.Implementer_Short_Name__c = Imp.Implementer_Short_Name__c ; 
         objimp.Implementer_Sub_Type__c = Imp.Implementer_Sub_Type__c ;
         newimplementer.add(objimp);
      }
      insert newimplementer;
      Pagereference pr = new Pagereference(baseUrl+pfid);   
      pr.setRedirect(true);
      return pr;
    }   
    public class wrapimplementer{
            public Account wrapimplementer{get;set;}
            public Account implementer1{get;set;}
            public boolean selected{get;set;}
            public string other {get; set;}
              Public String checkBoxValue{get;set;}
                public wrapimplementer(Account implementer1){
                    if(implementer1 != null){
                       wrapimplementer= implementer1;
                       selected  = false;
                   } else {
                       other = 'other';
                   }
                }
          }

}