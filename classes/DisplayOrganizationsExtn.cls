public class DisplayOrganizationsExtn{

public list<Account> lstAccountToDisplay {get;set;}
Set<Id> accountId = new Set<Id>();
User currentUser;

public DisplayOrganizationsExtn(){
    
    lstAccountToDisplay = new List<Account>();
    List<npe5__Affiliation__c> lstAffiliation = new List<npe5__Affiliation__c>();
    
    lstAffiliation = [Select Id, npe5__Organization__c, npe5__Contact__c from npe5__Affiliation__c where npe5__Contact__c =: currentUser.contactId AND Manage_Contacts__c = true];
    
    for(npe5__Affiliation__c objAff: lstAffiliation){
        accountId.add(objAff.npe5__Organization__c);
    }
    
    
    List<Account> lstAccountToDisplay = [Select id, name from Account where Id in: accountId];
    
    System.debug('lstAccountToDisplay '+lstAccountToDisplay );
    

    }
}