/*********************************************************************************
* Test Class: {TestLFAServiceTrigger }
*  DateCreated : 03-11-2014
----------------------------------------------------------------------------------
* Purpose/Methods:
* - Used for cover all code of TestLFAServiceTrigger .
----------------------------------------------------------------------------------
* History:
* - VERSION  DEVELOPER NAME    DATE            DETAIL FEATURES
     1.0                       03-11-2014     INITIAL DEVELOPMENT
*********************************************************************************/
@isTest
Public Class TestLFAServiceTrigger {
    Public static testMethod Void TestLFAServiceTrigger() {
        Account objAcc = new Account();
        objAcc.Name = 'Test Acc';
        objAcc.Short_Name__c= 'Test Acc';
        insert objAcc;
        
        Account objAcc2 = new Account();
        objAcc2.Name = 'Test Acc';
        objAcc2.Short_Name__c= 'Test Acc';
        objAcc2.ParentId = objAcc.ID;
        insert objAcc2;
        
        Account objAcc3 = new Account();
        objAcc3.Name = 'Test Acc';
        objAcc3.Short_Name__c= 'Test Acc';
        objAcc3.ParentId = objAcc2.ID;
        insert objAcc3;
        
        Account objAcc1 = new Account();
        objAcc1.Name = 'Test Acc';
        objAcc1.Short_Name__c= 'Test Acc';
        objAcc1.ParentId = objAcc.ID;
        insert objAcc1;
        
        Country__c objCountry = new Country__c();
        objCountry.Name ='India';
        objCountry.CT_Public_Group_ID__c = '00Gb0000000tjtJ';
        insert objCountry ;
       
        Country__c objCountry1 = new Country__c();
        objCountry1.Name ='India1';
        //objCountry1.CT_Public_Group_ID__c = '00Gb0000000tjtJ';
        insert objCountry1 ;
        
        LFA_Work_Plan__c objWp = new LFA_Work_Plan__c();
        objWp.Name = 'Test Wp';
        objWp.LFA__c = objAcc.Id;
        objWp.Country__c = objCountry.id;
        objWP.Include_In_Library__c = TRUE;
        insert objWp;
        
        LFA_Service__c objService = new LFA_Service__c();
        objService.Name = 'PUDR';
        objService.LFA_Work_Plan__c = objWp.id;
        objService.Service_Type__c = 'Key Services';
        objService.Service_Sub_Type__c = 'PUDR';
        objService.Exclude_from_Library__c=FALSE; 
        objService.Status__c = 'Cancelled';       
        insert objService;
    
        
        LFA_Service__c objService1 = new LFA_Service__c();
        objService1.Name = 'PUDR';
        objService1.LFA_Work_Plan__c = objWp.id;
        objService1.Service_Type__c = 'Key Services';
        objService1.Service_Sub_Type__c = 'PUDR';
        objService1.Status__c = 'Cancelled';   
        insert objService1;
        
        LFA_Work_Plan__c objWP2 = new LFA_Work_Plan__c(name='Test WO',LFA__c= objAcc1.ID,LFA_Access__c = 'Read Access',Country__c=objCountry1.ID,Country_Team_can_edit__c=FALSE);
        insert objWP2;
        
        
        LFA_Service__c objService2 = new LFA_Service__c();
        objService2.Name = 'PUDR';
        objService2.LFA_Work_Plan__c = objWP2.id;
        objService2.Service_Type__c = 'Other LFA Services';
        objService2.Service_Sub_Type__c = 'PUDR';
        
        insert objService2;
        
        LFA_Resource__c objResource = new LFA_Resource__c();
        objResource.Actual_LOE__c = 123;  
        objResource.LFA_Service__c = objService2.id;
        objResource.Rate__c = 1;
        //objResource.LFA_Role__c = objRole1.Id;
        objResource.LOE__c = 1;
        objResource.CT_Planned_Cost__c = 1;
        objResource.CT_Planned_LOE__c = 1;
        objResource.TGF_Proposed_Cost__c = 1;
        objResource.TGF_Proposed_LOE__c = 1;
        insert objResource;
        objService2.cancellation_approval__c = 'Rejected';
        //objService2.cancellation_approval__c = 'Approved' ;
        objService2.Change_Request_Approval__c  = 'Approved';
        objService2.Completion_Approval__c = 'Approved';
        objService2.Addition_Approval__c = 'Approved';
        objService2.Status__c = 'Cancelled'; 
       
        update objService2;
        
       
    }
}