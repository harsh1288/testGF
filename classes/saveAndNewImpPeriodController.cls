/*
* $Author:      $ TCS - Rahul Kumar
* $Description: $ Class for controlling new Implementation Period create event redirection
* $Date:        $ 13-Nov-2014 
* $Revision:    $
*/
public class saveAndNewImpPeriodController{

    private Implementation_Period__c ip;
    private Map<String, String> keyPrefixMap;
    public String backUrl{get;set;}
    public saveAndNewImpPeriodController(ApexPages.StandardController stdController){
        this.ip = (Implementation_Period__c)stdController.getRecord();
        backUrl = '';         
    }
    
     public PageReference createNewIpurl()
     {
            boolean statusPF = saveandNewHandler.pfTrue('Implementation_Period__c');
            keyPrefixMap =  saveandNewHandler.schemaMap();
            String baseUrl;
            string userTypeStr = UserInfo.getUserType();
            baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
            if(userTypeStr == 'Standard') 
            {
                baseUrl = baseUrl+'/';
            }
            else
            { 
                baseUrl = baseUrl+'/GM/';
            }
           if(ip.Concept_Note__c !=null)
           {
             // Creating url according to user type when Save and New is clicked while editing a record
                List<Concept_Note__c> s = new List<Concept_Note__c>([Select Name from Concept_Note__c where Id  =:ip.Concept_Note__c]);
                baseUrl += String.valueOf(keyPrefixMap.get('Implementation_Period__c'));
                baseUrl += '/e?nooverride=1';
                if(s.size()>0)
                {
                    baseUrl += '&';
                    baseUrl += Label.IP_Concept_Note_Lkup;
                    baseUrl += '=';
                    baseUrl += s[0].Name;            
                    baseUrl += '&retURL=';
                    baseUrl += ip.Concept_Note__c;
                }
                PageReference saveNewUrl = new PageReference(baseUrl);
                saveNewUrl.setRedirect(true);                
                return saveNewUrl;            
           }
           else
           {
            // Creating url according to user type when Save and New is clicked while creating new record
               if(userTypeStr == 'Standard') 
                {                    
                }
                else
                { 
                    if(ApexPages.currentPage().getParameters().get('retUrl').indexOf('GM') > -1)
                    {
                    
                    }
                    else
                    {
                        backUrl += +'/GM';
                    }
                }
               backUrl += ApexPages.currentPage().getParameters().get('retUrl');
               if(backUrl.contains('?'))
               {
                   backUrl += '&nooverride=1';
               }
               else
               {
                   backUrl += '?nooverride=1';
               }
               ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Record has been saved <br/> Can\'t use Save and New feature, Please <a id="goback" href="'+backUrl+'"><b>Go back</b></a>'));                           
               return null;
           }         
     }
}