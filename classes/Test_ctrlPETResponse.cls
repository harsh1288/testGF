@isTest 

Public Class Test_ctrlPETResponse
{

    public static testMethod void myTest()
    {
 
        Account objAcc = new Account();
        objAcc.Name = 'Test Acc';
        objAcc.Short_Name__c= 'Test Acc';
        insert objAcc;
        
        Country__c objCountry = new Country__c();
        objCountry.Name = 'Test Country';
        objCountry.Region_Name__c = 'Test Region';
        objCountry.Used_for_LFA_Services__c = TRUE;
        insert objCountry;
        
        LFA_Work_Plan__c objWp = new LFA_Work_Plan__c();
        objWp.Name = 'Test Wp';
        objWp.LFA__c = objAcc.Id;
        objWp.Country__c = objCountry.id;
        objWP.Include_In_Library__c = TRUE;
        insert objWp;
        
        LFA_Service__c objService = new LFA_Service__c();
        objService.Name = 'PUDR';
        objService.LFA_Work_Plan__c = objWp.id;
        objService.Service_Type__c = 'Key Services';
        objService.Service_Sub_Type__c = 'PUDR';
        objService.Exclude_from_Library__c=FALSE;        
        insert objService;
        
        LFA_Resource__c objResource = new LFA_Resource__c();
        //objResource.Name = 'Test Resource';
        objResource.LFA_Service__c = objService.id;
        objResource.Rate__c = 1;
        //objResource.LFA_Role__c = objRole1.Id;
        objResource.LOE__c = 1;
        objResource.CT_Planned_Cost__c = 1;
        objResource.CT_Planned_LOE__c = 1;
        objResource.TGF_Proposed_Cost__c = 1;
        objResource.TGF_Proposed_LOE__c = 1;
        insert objResource;
        
        LFA_Service__c objService1 = new LFA_Service__c();
        objService1.Name = 'PUDR';
        objService1.LFA_Work_Plan__c = objWp.id;
        objService1.Service_Type__c = 'Key Services';
        objService1.Service_Sub_Type__c = 'PUDR';
        insert objService1;
        
        LFA_Resource__c objResource1 = new LFA_Resource__c();
        //objResource1.Name = 'Test Resource1';
        objResource1.LFA_Service__c = objService1.id;
        objResource1.Rate__c = 2;
        objResource1.LOE__c = 2;
        objResource1.CT_Planned_Cost__c = 2;
        objResource1.CT_Planned_LOE__c = 2;
        objResource1.TGF_Proposed_Cost__c = 2;
        objResource1.TGF_Proposed_LOE__c = 2;
        insert objResource1;
        
        LFA_Service__c objService2 = new LFA_Service__c();
        objService2.Name = 'PUDR';
        objService2.LFA_Work_Plan__c = objWp.id;
        objService2.Service_Type__c = 'Other LFA Services';
        objService2.Service_Sub_Type__c = 'PUDR';
        insert objService2;
        
        LFA_Role__c objRole = new LFA_Role__c();
        objRole.Name = 'Test Role';
        insert objRole;
        
        LFA_Resource__c objResource2 = new LFA_Resource__c();
        //objResource2.Name = 'Test Resource1';
        objResource2.LFA_Service__c = objService2.id;
        objResource2.LFA_Role__c = objRole.id;
        objResource2.Rate__c = 2;
        objResource2.LOE__c = 2;
        objResource2.CT_Planned_Cost__c = 2;
        objResource2.CT_Planned_LOE__c = 2;
        objResource2.TGF_Proposed_Cost__c = 2;
        objResource2.TGF_Proposed_LOE__c = 2;
        insert objResource2;
        
        Contact objCon = new Contact();
        objCon.LastName = 'Test LName';
        objCon.AccountId = objAcc.Id;
        insert objCon;
        
        Rate__c objRate = new Rate__c();
        objRate.LFA_Role__c = objRole.Id;
        objRate.Contact__c = objCon.Id;
        objRate.Country__c = objCountry.Id;
        objRate.Rate__c = 1;
        objRate.Active__c = true;
        insert objRate;
        
        Grant__c objGrant = new Grant__c();
        objGrant.Name = 'Test Grant';
        objGrant.Country__c = objCountry.id;
        objGrant.Principal_Recipient__c = objAcc.id;
        objGrant.Financial_and_Fiduciary_Risks__c = 'Low';
        objGrant.Health_Services_and_Products_Risks__c = 'High';
        objGrant.Programmatic_and_Performance_Risks__c = 'Medium';
        insert objGrant;
        
        LFA_Service__c objService3 = new LFA_Service__c();
        objService3.Name = 'PUDR';
        objService3.LFA_Work_Plan__c = objWp.id;
        objService3.Service_Type__c = 'Key Services';
        objService3.Service_Sub_Type__c = 'PUDR';
        insert objService3;
        
        LFA_Role__c objRole1 = new LFA_Role__c();
        objRole1.Name = 'Test Role1';
        insert objRole1;
        
        LFA_Resource__c objResource3 = new LFA_Resource__c();
        //objResource.Name = 'Test Resource';
        objResource3.LFA_Service__c = objService3.id;
        objResource3.Rate__c = 1;
        objResource3.LFA_Role__c = objRole1.Id;
        objResource3.LOE__c = 1;
        objResource3.CT_Planned_Cost__c = 1;
        objResource3.CT_Planned_LOE__c = 1;
        objResource3.TGF_Proposed_Cost__c = 1;
        objResource3.TGF_Proposed_LOE__c = 1;
        insert objResource3;
        
        Rate__c objRate1 = new Rate__c();
        objRate1.LFA_Role__c = objRole1.Id;
        objRate1.Contact__c = objCon.Id;
        objRate1.Country__c = objCountry.Id;
        objRate1.Rate__c = 1;
        objRate1.Active__c = true;
        insert objRate1;
        
        LFA_Work_Plan__c objWp1 = new LFA_Work_Plan__c();
        objWp1.Name = 'Test Wp';
        objWp1.LFA__c = objAcc.Id;
        objWp1.Country__c = objCountry.id;
        objWp1.End_Date__c = system.today();
        insert objWp1;
        
        LFA_Service__c objService4 = new LFA_Service__c();
        objService4.Name = 'PUDR';
        objService4.LFA_Work_Plan__c = objWp1.id;
        objService4.Service_Type__c = 'Key Services';
        objService4.Service_Sub_Type__c = 'PUDR';
        insert objService4;
        
        LFA_Resource__c objResource4 = new LFA_Resource__c();
        //objResource.Name = 'Test Resource';
        objResource4.LFA_Service__c = objService4.id;
        objResource4.Rate__c = 1;
        objResource4.LFA_Role__c = objRole1.Id;
        objResource4.LOE__c = 1;
        objResource4.CT_Planned_Cost__c = 1;
        objResource4.CT_Planned_LOE__c = 1;
        objResource4.TGF_Proposed_Cost__c = 1;
        objResource4.TGF_Proposed_LOE__c = 1;
        insert objResource4;
        
        LFA_Service__c objService5 = new LFA_Service__c();
        objService5.Name = 'PUDR';
        objService5.LFA_Work_Plan__c = objWp1.id;
        objService5.Service_Type__c = 'Key Services';
        objService5.Service_Sub_Type__c = 'PUDR';
        insert objService5;
        
        LFA_Resource__c objResource5  = new LFA_Resource__c();
        //objResource.Name = 'Test Resource';
        objResource5.LFA_Service__c = objService5.id;
        objResource5.Rate__c = 1;
        objResource5.LFA_Role__c = objRole1.Id;
        objResource5.LOE__c = 1;
        objResource5.CT_Planned_Cost__c = 1;
        objResource5.CT_Planned_LOE__c = 1;
        objResource5.TGF_Proposed_Cost__c = 1;
        objResource5.TGF_Proposed_LOE__c = 1;
        insert objResource5;
        
        Performance_Evaluation__c perf = new Performance_Evaluation__c();
        //perf.Q1_Rating__c='1';
        perf.LFA_Work_Plan__c = objWp.Id;
        insert perf;
        
        
        PET_Response__c petres = new PET_Response__c();
        petres.name='test';
        petres.Performance_Evaluation__c = perf.Id;
        petres.Status__c='Completed';
        petres.Rating__c = '3';
        insert petres; 
        
        Service_PET_Jxn__c petjxn = new Service_PET_Jxn__c();
        petjxn.LFA_Service__c = objService.id;
        petjxn.Performance_Evaluation__c = perf.id;
        insert petjxn;
        
        Apexpages.currentpage().getparameters().put('id',perf.id);
        ApexPages.StandardController sc = new ApexPages.StandardController(perf);
        
        
        ctrlPETResponse objCls = new ctrlPETResponse(sc);
        
        
        //List<Profile> PROFILE = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
        
        
        //Apexpages.currentpage().getparameters().put('RecordType','012g0000000Cp6Y');
        
        objCls.intIndex = 0;
        objCls.intIndexSt = 0;
        
        objCls.closePopup();
        objCls.SaveResponse();
        objCls.SubmitResponse();
        objCls.viewService();
        objCls.HideService();   
        objCls.AnalyseMainGrid();
        objCls.FillServiceType();
        objCls.CalculateTotal();
        objCls.RetriveResourceValues();
        objCls.getFieldsName('Performance_Evaluation__c');
        
        
        
           
        
        //objCls.strRecordTypeId = '012g0000000Cp6Y';
            
        //objCls.ClearFilterMainGrid();
        objCls.ExapndAllServiceType();
        objCls.CollepseAllServiceType();
        
        Apexpages.currentpage().getparameters().put('AddIndex','0');
        //objCls.AddService();
        
        Apexpages.currentpage().getparameters().put('SaveServiceIndex','3');
        Apexpages.currentpage().getparameters().put('SaveServiceTypeIndex','0');
        //objCls.SaveServiceAndAddResources();
        
        Apexpages.currentpage().getparameters().put('newServiceId',objService.id);
        //objCls.AddResource();
        //objCls.CancelResources();
        
        Apexpages.currentpage().getparameters().put('DuplicateIndex','0');
        Apexpages.currentpage().getparameters().put('DuplicateSTIndex','0');
        Apexpages.currentpage().getparameters().put('ServiceId',objService.id);
        //objCls.DuplicateService();
        
        Apexpages.currentpage().getparameters().put('DeleteIndex','1');
        Apexpages.currentpage().getparameters().put('DeleteSTIndex','0');
        //objCls.DeleteService();
        
        Apexpages.currentpage().getparameters().put('EditSTIndex','0');
        Apexpages.currentpage().getparameters().put('EditIndex','3');
        //objCls.EditService();
        Apexpages.currentpage().getparameters().put('ResourceIndex','0');
        //objCls.FillContactsOnChangeRole();
        Apexpages.currentpage().getparameters().put('ResourceIndexForRate','0');
        //objCls.FillRateOnChangeContact();
        Apexpages.currentpage().getparameters().put('intIndexST','0');
        Apexpages.currentpage().getparameters().put('intIndex','3');
       // objCls.SaveResources();
        Apexpages.currentpage().getparameters().put('EditSTIndex','0');
        Apexpages.currentpage().getparameters().put('EditIndex','3');
        //objCls.EditService();
        Apexpages.currentpage().getparameters().put('DeleteResourceIndex','0');
        //objCls.DeleteResource();
        
        //objCls.SelectedCountry = '';
       // objCls.SelectedRegion = '';
        //objCls.AnalyseSearch();
        
       // objCls.sortExpression = 'Country__c';
       // objCls.sortDirection = 'ASC';
       // objCls.SelectedCountry = '';
      //  objCls.SelectedRegion = '';
      //  objCls.getSortDirection();
      //  objCls.setSortDirection('ASC');
      //  objCls.AnalyseSearch();
       // objCls.AddServiceToWorkPlan();
        
        //objCls.WorkPlanChanged();
        //objCls.SavePE();
        
        
          
        Performance_Evaluation__c perf1 = new Performance_Evaluation__c();
        //perf.Q1_Rating__c='1';
        perf.LFA_Work_Plan__c = objWp1.Id;
        insert perf1;
    
        
        Apexpages.currentpage().getparameters().put('id',perf1.id);
        ApexPages.StandardController sc1 = new ApexPages.StandardController(perf1);
        ctrlPerformanceEvaluation objCls1 = new ctrlPerformanceEvaluation(sc1);
        
   /*    objCls.SelectedServiceTypeSearching = 'PUDR';
        objCls.SelectedYear = '2013';
        objCls.SelectedFinancialRisk = 'Low';
        objCls.SelectedHealthRisk = 'High';
        objCls.SelectedProgramaticRisk = 'Medium';
        objCls.SelectedLFA = objAcc.Id;
        objCls.AnalyseSearch();
        objCls.ClearFilterSearchGrid();
        objCls.BackToWorkPlan();
        objCls.getResorcesForPDF();   */
        
        
      
    }


}