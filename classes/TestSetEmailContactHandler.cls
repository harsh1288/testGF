@isTest

public class TestSetEmailContactHandler{
    public static testMethod void testUpdateContact(){
        Account objAcc = TestClassHelper.insertAccount();
        
        Contact Con = new Contact();
        con.LastName = 'Testing';
        Con.email = ''; //standarduser@testorg.com
        con.accountid= objAcc.id;
        con.External_User__c = true;
        con.FirstName = 'Testing';
        insert con;
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Applicant/CM Admin']; 
        
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id,TimeZoneSidKey='America/Los_Angeles', 
        contactid=con.id,UserName='s@testorg.com');
        insert u;   
        List<Id> uList = new List<Id>();
        uList.add(u.id);
        setEmailContactHandler.updateContact(uList);
    }
}