/*
* $Author:      $ TCS- Rahul Kumar
* $Description: $ Handler class for CatalogIndicatorTrigger
* $Date:        $ 31/10/2014
* $Revision:    $ 
*/


public class CatalogIndicatorHandler
{
    
    /*public CatalogIndicatorHandler()
    {
        //Constructor
    }
    
    /*Copies values of Disaggregated_Name fields from Catalog Indicators to child Grant Indicators*/
    /*public static void onDisaggregationUncheck(List<Indicator__c> catalogIndicatorList)
    {
        Set<Id> CIid = new Set<Id>();
        for(Indicator__c ci:catalogIndicatorList)
        {
            if(ci.id !=null)
            {
                CIid.add(ci.Id);
            }
        }
        List<Grant_Indicator__c> GIList = new List<Grant_Indicator__c>([Select Indicator__c,Is_Disaggregated__c,Name_Disaggregated__c,Name_Disaggregated_Russian__c,Name_Disaggregated_Spanish__c,Name_Disaggregated_French__c from Grant_Indicator__c where Indicator__c IN :CIid]);
        Map<Id,List<Grant_Indicator__c>> ciGiMap = new Map<Id,List<Grant_Indicator__c>>();      
        if( GIList.size() > 0 )
        {
            //Start of filling up map of Catalog Indicator pointing to it\'s child Grant indicators
            for(Grant_Indicator__c gi:GIList)
            {
                if(ciGiMap.ContainsKey(gi.Indicator__c))
                {
                    ciGiMap.get(gi.Indicator__c).add(gi);
                }
                else
                {
                    ciGiMap.put(gi.Indicator__c,new List<Grant_Indicator__c>());
                }
            }
            //End of filling up map of Catalog Indicator pointing to it\'s child Grant indicators
            List<Grant_Indicator__c> grantListInScope = new List<Grant_Indicator__c>();
            for(Indicator__c ci:catalogIndicatorList)
            {
                if(ciGiMap.ContainsKey(ci.Id))
                {
                    for(Grant_Indicator__c gi:ciGiMap.get(ci.Id))
                    {
                        //gi.Is_Disaggregated__c = false;
                        gi.Name_Disaggregated__c = ci.Disaggregated_Name__c;
                        gi.Name_Disaggregated_Russian__c = ci.Russian_Name__c;
                        gi.Name_Disaggregated_Spanish__c = ci.Spanish_Name__c;
                        gi.Name_Disaggregated_French__c = ci.French_Name__c;
                        grantListInScope.add(gi);
                    }                   
                }               
            }
            try
            {
                Database.Update(grantListInScope,false);
            }
            Catch(Exception e)
            {
                System.debug('Grant Indicator record could not be updated due to '+e);
            }
        }
    }  */       
}