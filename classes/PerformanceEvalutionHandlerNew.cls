/*********************************************************************************
* Class: PerformanceEvalutionHandler
* DateCreated 11/22/2013
----------------------------------------------------------------------------------
* Purpose: This class is used to insert 4 PET Reponse object based on type for
            each single PE referred from trigger.
----------------------------------------------------------------------------------
*********************************************************************************/
Public class PerformanceEvalutionHandlerNew {

    //THIS IS FIRED ON INSERT OF A PERFORMANCE EVALUATION RECORD//
    Public static void aiCreatePETResponse(List<Performance_Evaluation__c> lstPerformanceEvalution) {
        Set<Id> SetPEIds = new Set<Id>();
        
        for(Performance_Evaluation__c objPE : lstPerformanceEvalution) {
            SetPEIds.add(objPE.Id);        
        }
        
        if(SetPEIds.Size() > 0) {
            List<Performance_Evaluation__c> lstPEInsert = [Select Id from Performance_Evaluation__c where Id IN: SetPEIds];
            List<PET_REsponse__c> lstPETReponse = new List<PET_REsponse__c>();
            if(lstPEInsert.Size() > 0) {
                for(Performance_Evaluation__c objPEvalution : lstPEInsert) {
                    PET_REsponse__c objPETResponse = new PET_REsponse__c();
                    objPETResponse.Performance_Evaluation__c = objPEvalution.Id;
                    objPETResponse.Type__c = 'LFA Finance';
                    lstPETReponse.add(objPETResponse);
                    objPETResponse = new PET_REsponse__c();
                    objPETResponse.Performance_Evaluation__c = objPEvalution.Id;
                    objPETResponse.Type__c = 'LFA Programmatic/M&E';
                    lstPETReponse.add(objPETResponse);
                    objPETResponse = new PET_REsponse__c();
                    objPETResponse.Performance_Evaluation__c = objPEvalution.Id;
                    objPETResponse.Type__c = 'LFA PSM';
                    lstPETReponse.add(objPETResponse);
                    objPETResponse = new PET_REsponse__c();
                    objPETResponse.Performance_Evaluation__c = objPEvalution.Id;
                    objPETResponse.Type__c = 'Legal';
                    lstPETReponse.add(objPETResponse);
                }
                insert lstPETReponse;
            }
        }
    }
    
    //THIS IS FIRED ON UPDATE OF A PERFORMANCE EVALUATION RECORD//
    Public static void auUpdatePETandResponse(List<Performance_Evaluation__c> lstPerformanceEvalutionNew,
                                               Map<Id,Performance_Evaluation__c> PerformanceEvalutionOld ) {
        if(lstPerformanceEvalutionNew.size() > 0){
            Set<Id> setPEToUpdate = new Set<Id>();
            for(Performance_Evaluation__c objPE : lstPerformanceEvalutionNew) {
                if(objPE.Status__c != PerformanceEvalutionOld.get(objPE.id).Status__c && objPE.Status__c == 'Begin Alerts'){
                    setPEToUpdate.add(objPE.id);
                }
        //PET Sharing
        // Get a list of the PET Access records
            List<PET_Access__c> lstPETAccess = [Select Id,User__c,PET_Access__c from PET_Access__c Where LFA_Work_Plan__c =: objPE.LFA_Work_Plan__c];
        
        // Create an empty list of share records
            List<Performance_Evaluation__Share> lstPETShares = new List<Performance_Evaluation__Share>();
        
        //Populate each share record based on the PET Access records
            If(lstPETAccess.size() > 0){
              For(PET_Access__c PETAccess : lstPETAccess){
                Performance_Evaluation__Share Share = new Performance_Evaluation__share();
                    Share.ParentId = objPE.Id;
                    Share.UserOrGroupId = PETAccess.User__c; 
                    if(PETAccess.PET_Access__c == 'Read'){
                        Share.AccessLevel = 'read';}
                    if(PETAccess.PET_Access__c == 'Read/write'){
                        Share.AccessLevel = 'edit';} 
          //Add the filled share record to the list and insert the list
                    lstPETShares.add(Share);                                
              }
            }
              If(objPE.CT_Id__c != null){
                  Performance_Evaluation__Share ctShare = new Performance_Evaluation__Share();
                  ctShare.ParentId = objPE.Id;
                  ctShare.UserOrGroupId = objPE.CT_Id__c;
                  ctShare.AccessLevel = 'edit';
                  lstPETShares.add(ctShare); 
              }      
              if (!Test.IsRunningTest()) {   
                  Insert lstPETShares; 
              }else{
                System.debug('in');
              }    
            }             
            List<Performance_Evaluation__c> lstPE = [Select Id,LFA_Work_Plan__r.Country__r.CT_Public_Group_ID__c,
                                            LFA_Work_Plan__r.LFA__r.ParentId,
                                            LFA_Work_Plan__r.LFA__c,(Select Id,Type__c From PET_Responses__r) 
                                            From Performance_Evaluation__c Where Id IN : setPEToUpdate 
                                            And LFA_Work_Plan__r.Country__c != null 
                                            And LFA_Work_Plan__r.Country__r.CT_Public_Group_ID__c != null];
            
            Map<Id,Set<Id>> mapGroupIdToUsers = new Map<Id,Set<Id>>();
            Map<String,Set<Id>> mapGroupNameToUsers = new Map<String,Set<Id>>();
            Map<Id,List<Contact>> mapLFAToContacts = new Map<Id,List<Contact>>();
            Map<Id,List<Contact>> mapLFAParentToContacts = new Map<Id,List<Contact>>();
            Set<Id> setPEGroupIds = new Set<Id>();
            Set<Id> setLFA = new Set<Id>();
            Set<Id> LFAParentIds = new Set<Id>();
            if(lstPE.size() > 0){ 
                for(Performance_Evaluation__c objPE : lstPE){
                    setPEGroupIds.add(objPE.LFA_Work_Plan__r.Country__r.CT_Public_Group_ID__c);
                    setLFA.add(objPE.LFA_Work_Plan__r.LFA__c);
                    if(objPE.LFA_Work_Plan__r.LFA__r.ParentId != null){
                        LFAParentIds.add(objPE.LFA_Work_Plan__r.LFA__r.ParentId);
                    }
                }
            }
            if(setPEGroupIds.size() > 0){
                List<GroupMember> lstGroupMember = [Select UserOrGroupId,Group.DeveloperName From GroupMember 
                                Where GroupId IN : setPEGroupIds];
                for(GroupMember objGM : lstGroupMember){
                    if(mapGroupIdToUsers.containsKey(objGM.GroupId)){
                        mapGroupIdToUsers.get(objGM.GroupId).add(objGM.UserOrGroupId);
                    }else{
                        mapGroupIdToUsers.put(objGM.GroupId,new Set<Id>{objGM.UserOrGroupId});
                    }
                }
                List<GroupMember> lstGroupMemberExisting = [Select UserOrGroupId,Group.DeveloperName From GroupMember 
                                Where Group.DeveloperName = 'CT_All_PO' 
                                OR Group.DeveloperName = 'CT_All_FPM' 
                                OR Group.DeveloperName = 'CT_All_Program_Finance'
                                OR Group.DeveloperName = 'CT_All_Monitoring' 
                                OR Group.DeveloperName = 'CT_All_Procurement'];
                for(GroupMember objGM : lstGroupMemberExisting){
                    if(mapGroupNameToUsers.containsKey(objGM.Group.DeveloperName)){
                        mapGroupNameToUsers.get(objGM.Group.DeveloperName).add(objGM.UserOrGroupId);
                    }else{
                        mapGroupNameToUsers.put(objGM.Group.DeveloperName,new Set<Id>{objGM.UserOrGroupId});
                    }
                }
            }
            if(setLFA.size() > 0){
                List<Contact> lstContact = [Select Id,AccountId,Account.ParentId From Contact 
                                        Where AccountId IN : setLFA And AccountId != null 
                                        And Receive_PET_Alerts__c = true];
                                        
                                        
                List<Contact> lstContactParent = [Select Id,AccountId,Account.ParentId From Contact 
                                        Where Account.ParentId = null And AccountId IN : LFAParentIds
                                        And Receive_PET_Alerts__c = true];
                
                if(lstContact.size() > 0){
                    for(Contact objCon : lstContact){
                        if(mapLFAToContacts.containsKey(objCon.AccountId)){
                            mapLFAToContacts.get(objCon.AccountId).add(objCon);
                        }else{
                            mapLFAToContacts.put(objCon.AccountId,new List<Contact>{objCon});
                        }
                    }
                }
                if(lstContactParent.size() > 0){
                    for(Contact objCon : lstContactParent){
                        if(mapLFAParentToContacts.containsKey(objCon.Account.ParentId)){
                            mapLFAParentToContacts.get(objCon.AccountId).add(objCon);
                        }else{
                            mapLFAParentToContacts.put(objCon.AccountId,new List<Contact>{objCon});
                        }
                    }
                }
                system.debug('^&&^&^&'+mapLFAToContacts.size());
                system.debug('^&&^&^&'+mapLFAParentToContacts);
            } 
            if(lstPE.size() > 0){
                List<Performance_Evaluation__c> lstPEToUpdate = new List<Performance_Evaluation__c>();
                Performance_Evaluation__c objPENew;
                List<PET_Response__c> lstPETResToUodate = new List<PET_Response__c>();
                PET_Response__c objPETResNew;
                for(Performance_Evaluation__c objPE : lstPE){
                    Id PEGroupId = objPE.LFA_Work_Plan__r.Country__r.CT_Public_Group_ID__c;
                    objPENew = new Performance_Evaluation__c(id = objPE.id,Alerts_User_1__c = null,Alerts_User_2__c = null,Alerts_User_3__c = null,Alerts_User_4__c = null,LFA_Contact_1__c = null,LFA_Contact_2__c = null,LFA_Contact_3__c = null,LFA_Contact_4__c = null,LFA_Contact_5__c = null,LFA_Contact_6__c = null,LFA_Contact_7__c = null,LFA_Contact_8__c = null,LFA_Contact_9__c = null,LFA_Contact_10__c = null,lfa_contact_11__c = null,lfa_contact_12__c = null, lfa_contact_13__c = null, lfa_contact_14__c = null, lfa_contact_15__c = null);
                    
                    List<PET_Access__c> lstPAContacts = [Select Id,User__c,User__r.ContactId, PET_Access__c from PET_Access__c Where LFA_Work_Plan__c =: objPE.LFA_Work_Plan__c And User__r.ContactId != null And Receive_PET_Alerts__c = true];
                    If(lstPAContacts.size() > 0){
                       if(lstPAContacts.size() > 0) objPENew.LFA_Contact_1__c = lstPAContacts[0].User__r.Contactid;
                       if(lstPAContacts.size() > 1) objPENew.LFA_Contact_2__c = lstPAContacts[1].User__r.Contactid;
                       if(lstPAContacts.size() > 2) objPENew.LFA_Contact_3__c = lstPAContacts[2].User__r.Contactid;
                       if(lstPAContacts.size() > 3) objPENew.LFA_Contact_4__c = lstPAContacts[3].User__r.Contactid;
                       if(lstPAContacts.size() > 4) objPENew.LFA_Contact_5__c = lstPAContacts[4].User__r.Contactid;
                       if(lstPAContacts.size() > 5) objPENew.LFA_Contact_6__c = lstPAContacts[5].User__r.Contactid;
                       if(lstPAContacts.size() > 6) objPENew.LFA_Contact_7__c = lstPAContacts[6].User__r.Contactid;
                       if(lstPAContacts.size() > 7) objPENew.LFA_Contact_8__c = lstPAContacts[7].User__r.Contactid;
                       if(lstPAContacts.size() > 8) objPENew.LFA_Contact_9__c = lstPAContacts[8].User__r.Contactid;
                       if(lstPAContacts.size() > 9) objPENew.LFA_Contact_10__c = lstPAContacts[9].User__r.Contactid;
                       if(lstPAContacts.size() > 10) objPENew.LFA_Contact_11__c = lstPAContacts[10].User__r.Contactid;
                       if(lstPAContacts.size() > 11) objPENew.LFA_Contact_12__c = lstPAContacts[11].User__r.Contactid;
                       if(lstPAContacts.size() > 12) objPENew.LFA_Contact_13__c = lstPAContacts[12].User__r.Contactid;
                       if(lstPAContacts.size() > 13) objPENew.LFA_Contact_14__c = lstPAContacts[13].User__r.Contactid;
                       if(lstPAContacts.size() > 14) objPENew.LFA_Contact_15__c = lstPAContacts[14].User__r.Contactid;

                       
                    
                    }           
                    
                    if(mapGroupIdToUsers.get(PEGroupId) != null && mapGroupIdToUsers.get(PEGroupId).size() > 0){
                        if(mapGroupNameToUsers.get('CT_All_FPM') != null && mapGroupNameToUsers.get('CT_All_FPM').size() > 0){
                            if (mapGroupIdToUsers.get(PEGroupId).size() <  mapGroupNameToUsers.get('CT_All_FPM').size()){
                                Integer count = 0;
                                for(Id UserId : mapGroupIdToUsers.get(PEGroupId)){
                                    if(count == 2) break;
                                    if(mapGroupNameToUsers.get('CT_All_FPM').contains(UserId)){
                                        if(count == 0) objPENew.Alerts_User_1__c = UserId;
                                        if(count == 1) objPENew.Alerts_User_2__c = UserId;
                                        count++;
                                    }
                                }
                            }else{
                                Integer count = 0;
                                for(Id UserId : mapGroupNameToUsers.get('CT_All_FPM')){
                                    if(count == 2) break;
                                    if(mapGroupIdToUsers.get(PEGroupId).contains(UserId)){
                                        if(count == 0) objPENew.Alerts_User_1__c = UserId;
                                        if(count == 1) objPENew.Alerts_User_2__c = UserId;
                                        count++;
                                    }
                                }
                            }
                        }
                        if(mapGroupNameToUsers.get('CT_All_PO') != null && mapGroupNameToUsers.get('CT_All_PO').size() > 0){
                            if (mapGroupIdToUsers.get(PEGroupId).size() <  mapGroupNameToUsers.get('CT_All_PO').size()){
                                Integer count = 0;
                                for(Id UserId : mapGroupIdToUsers.get(PEGroupId)){
                                    if(count == 3) break;
                                    if(mapGroupNameToUsers.get('CT_All_PO').contains(UserId)){
                                        if(count == 0) objPENew.Alerts_User_3__c = UserId;
                                        if(count == 1) objPENew.Alerts_User_4__c = UserId;
                                        if(count == 2) objPENew.Alerts_User_5__c = UserId;
                                        count++;
                                    }
                                }
                            }else{
                                Integer count = 0;
                                for(Id UserId : mapGroupNameToUsers.get('CT_All_PO')){
                                    if(count == 3) break;
                                    if(mapGroupIdToUsers.get(PEGroupId).contains(UserId)){
                                        if(count == 0) objPENew.Alerts_User_2__c = UserId;
                                        if(count == 1) objPENew.Alerts_User_3__c = UserId;
                                        if(count == 2) objPENew.Alerts_User_4__c = UserId;
                                        count++;
                                    }
                                }
                            }
                        }     
                        /*if(mapLFAToContacts.ContainsKey(objPE.LFA_Work_Plan__r.LFA__c) 
                            && mapLFAToContacts.get(objPE.LFA_Work_Plan__r.LFA__c) != null 
                            && mapLFAToContacts.get(objPE.LFA_Work_Plan__r.LFA__c).size() > 0){
                            Integer Count = 0;
                            system.debug('$%$%%$%'+mapLFAToContacts.get(objPE.LFA_Work_Plan__r.LFA__c).size());
                            for(Contact objCon : mapLFAToContacts.get(objPE.LFA_Work_Plan__r.LFA__c)){
                                if(Count == 0) objPENew.LFA_Contact_1__c = objCon.id;
                                if(Count == 1) objPENew.LFA_Contact_2__c = objCon.id;
                                if(Count == 2) objPENew.LFA_Contact_3__c = objCon.id;
                                if(Count == 3) objPENew.LFA_Contact_4__c = objCon.id;
                                if(Count == 4) objPENew.LFA_Contact_5__c = objCon.id;
                                if(Count == 5) break;
                                Count ++;
                            }
                        }
                        if(mapLFAParentToContacts.ContainsKey(objPE.LFA_Work_Plan__r.LFA__r.ParentId) 
                            && mapLFAParentToContacts.get(objPE.LFA_Work_Plan__r.LFA__r.ParentId) != null 
                            && mapLFAParentToContacts.get(objPE.LFA_Work_Plan__r.LFA__r.ParentId).size() > 0){
                            Integer Count = 0;
                            system.debug('$%$%%$%'+mapLFAParentToContacts.get(objPE.LFA_Work_Plan__r.LFA__r.ParentId).size());
                            for(Contact objCon : mapLFAParentToContacts.get(objPE.LFA_Work_Plan__r.LFA__r.ParentId)){
                                if(Count == 0) objPENew.LFA_Contact_6__c = objCon.id;
                                system.debug('$%$%%$%'+ objPENew.LFA_Contact_6__c);
                                if(Count == 1) objPENew.LFA_Contact_7__c = objCon.id;
                                if(Count == 2) objPENew.LFA_Contact_8__c = objCon.id;
                                if(Count == 3) objPENew.LFA_Contact_9__c = objCon.id;
                                if(Count == 4) objPENew.LFA_Contact_10__c = objCon.id;
                                if(Count == 5) break;
                                Count ++;
                            }
                        }*/
                        if(objPE.PET_Responses__r.size() > 0){
                            for(PET_Response__c objRes : objPE.PET_Responses__r){
                                objPETResNew = new PET_Response__c(id = objRes.id,Alerts_User_1__c = null,Alerts_User_2__c = null,Alerts_User_3__c = null,Alerts_User_4__c = null);
                                Set<Id> setUserId = new Set<Id>();
                                if(objRes.Type__c == 'LFA Finance' 
                                    && mapGroupNameToUsers.get('CT_All_Program_Finance') != null 
                                    && mapGroupNameToUsers.get('CT_All_Program_Finance').size() > 0 ){
                                    setUserId.addAll(mapGroupNameToUsers.get('CT_All_Program_Finance'));
                                }else if(objRes.Type__c == 'LFA Programmatic/M&E' 
                                    && mapGroupNameToUsers.get('CT_All_Monitoring') != null 
                                    && mapGroupNameToUsers.get('CT_All_Monitoring').size() > 0 ){
                                    setUserId.addAll(mapGroupNameToUsers.get('CT_All_Monitoring'));
                                }else if(objRes.Type__c == 'LFA PSM' 
                                    && mapGroupNameToUsers.get('CT_All_Procurement') != null 
                                    && mapGroupNameToUsers.get('CT_All_Procurement').size() > 0 ){
                                    setUserId.addAll(mapGroupNameToUsers.get('CT_All_Procurement'));
                                }
                                if(setUserId != null && setUserId.size() > 0){
                                    if (mapGroupIdToUsers.get(PEGroupId).size() < setUserId.size()){
                                        Integer count = 0;
                                        for(Id UserId : mapGroupIdToUsers.get(PEGroupId)){
                                            if(count == 4) break;
                                            if(setUserId.contains(UserId)){
                                                if(count == 0) objPETResNew.Alerts_User_1__c = UserId;
                                                if(count == 1) objPETResNew.Alerts_User_2__c = UserId;
                                                if(count == 2) objPETResNew.Alerts_User_3__c = UserId;
                                                if(count == 3) objPETResNew.Alerts_User_4__c = UserId;
                                                count++;
                                            }
                                        }
                                    }else{
                                        Integer count = 0;
                                        for(Id UserId : setUserId){
                                            if(count == 4) break;
                                            if(mapGroupIdToUsers.get(PEGroupId).contains(UserId)){
                                                if(count == 0) objPETResNew.Alerts_User_1__c = UserId;
                                                if(count == 1) objPETResNew.Alerts_User_2__c = UserId;
                                                if(count == 2) objPETResNew.Alerts_User_3__c = UserId;
                                                if(count == 3) objPETResNew.Alerts_User_4__c = UserId;
                                                count++;
                                            }
                                        }
                                    }
                                    lstPETResToUodate.add(objPETResNew);
                                }
                            }
                        }
                    }
                    lstPEToUpdate.add(objPENew);
                }
                if(lstPEToUpdate.size() > 0) update lstPEToUpdate;
                if(lstPETResToUodate.size() > 0) update lstPETResToUodate;
            }
           
        }
    }
    
    //update PE begin_alerts__c checkbox update update Pet Response status.
    Public static void auUpdatePETandResponseStatus(List<Performance_Evaluation__c> lstPerformanceEvalutionNew,
                                               Map<Id,Performance_Evaluation__c> PerformanceEvalutionOld ) {
       
        if(lstPerformanceEvalutionNew.size() > 0){
            Set<Id> setPEToUpdate = new Set<Id>();
            for(Performance_Evaluation__c objPE : lstPerformanceEvalutionNew) {
                if(objPE.begin_alerts__c  != PerformanceEvalutionOld.get(objPE.id).begin_alerts__c ){
                    setPEToUpdate.add(objPE.id);
                }
            }
            system.debug('@@@@@@@ setPEToUpdate' +setPEToUpdate);
            List<PET_Response__c> lstPETRes = new List<PET_Response__c>();
            //get Pet Response to update.
            if(setPEToUpdate.size() >0)
                lstPETRes = [Select Id,pet_status__c,Performance_Evaluation__r.begin_alerts__c From PET_Response__c where Performance_Evaluation__c IN : setPEToUpdate];
            List<PET_Response__c> lstPETResToUpdate = new List<PET_Response__c>();
            if(lstPETRes.size() > 0){
                PET_Response__c objPETResNew;
                for(PET_Response__c objRes : lstPETRes){
                    objPETResNew = new PET_Response__c(id = objRes.id,pet_status__c = objRes.pet_status__c);
                    if(objRes.Performance_Evaluation__r.begin_alerts__c == true) {
                        objPETResNew.pet_status__c ='Begin Alerts';
                    } else {
                        objPETResNew.pet_status__c ='No Alerts';
                    }
                    lstPETResToUpdate.add(objPETResNew);
                }
            }
            system.debug('@@@@@@@ lstPETResToUpdate' +lstPETResToUpdate);
            if(lstPETResToUpdate.size() > 0) update lstPETResToUpdate;
        }
                                            
    }   
    
    //update PE Status status__c="Submitted to LFA" then set 
    //if pet_response.status__c="Incomplete" then update pet_response.status__c to "Skipped."
    Public static void auUpdatePETandResponseStatusSkipped(List<Performance_Evaluation__c> lstPerformanceEvalutionNew,
                                               Map<Id,Performance_Evaluation__c> PerformanceEvalutionOld ) {
       
        if(lstPerformanceEvalutionNew.size() > 0){
            Set<Id> setPEToUpdate = new Set<Id>();
            for(Performance_Evaluation__c objPE : lstPerformanceEvalutionNew) {
                if(objPE.Status__C  != PerformanceEvalutionOld.get(objPE.id).Status__C){
                    if(objPE.Status__C == 'Submitted to LFA') {
                        setPEToUpdate.add(objPE.id);
                    }
                }
            }
            system.debug('@@@@@@@ setPEToUpdate' +setPEToUpdate);
            List<PET_Response__c> lstPETRes = new List<PET_Response__c>();
            //get Pet Response to update.
            lstPETRes = [Select Id,status__c,Performance_Evaluation__r.Status__C From PET_Response__c where Performance_Evaluation__c IN : setPEToUpdate];
            List<PET_Response__c> lstPETResToUpdate = new List<PET_Response__c>();
            if(lstPETRes.size() > 0){
                for(PET_Response__c objRes : lstPETRes){
                    if(objRes.status__c == 'Incomplete' || objRes.status__c == 'In Progress') {
                        objRes.status__c ='Skipped';
                    }
                    lstPETResToUpdate.add(objRes);
                }
            }
            system.debug('@@@@@@@ lstPETResToUpdate' +lstPETResToUpdate);
            if(lstPETResToUpdate.size() > 0) update lstPETResToUpdate;
        }
                                            
    }   
}