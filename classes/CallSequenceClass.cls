public class CallSequenceClass {
    public static boolean firstRun_updateRank1 = false;
    public static boolean firstRun_updateIteration1 = false;
    public static boolean firstRun_updateRelease1 = false;
    public static boolean firstRun_updateParent1 = false;
    
    public static boolean firstRun_updateRank2 = false;
    public static boolean firstRun_updateIteration2 = false;
    public static boolean firstRun_updateRelease2 = false;
    public static boolean firstRun_updateParent2 = false;
    
    public static boolean firstRun_updateRank3 = false;
    public static boolean firstRun_updateIteration3 = false;
    public static boolean firstRun_updateRelease3 = false;
    public static boolean firstRun_updateParent3 = false;
    
    public static boolean firstRun_updateRank4 = false;
    public static boolean firstRun_updateIteration4 = false;
    public static boolean firstRun_updateRelease4 = false;
    public static boolean firstRun_updateParent4 = false;
    /*
        Modified by Peeyush Awadhiya on 11/08/2013 to add instance member to enable the class for test - DO NOT USE
    */
    public boolean instanceVar = false;
}