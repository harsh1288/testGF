@isTest
public class CreateVendorTest{

public static testMethod void CreateVendorTest(){

Set_AccountId__c objSA = new Set_AccountId__c ();
insert objSA;
Country__c objCon = TestClassHelper.insertCountry();
//insert objCon;

Account objAcc = TestClassHelper.createAccount();
objAcc.Country__c = objCon.Id;
objAcc.Full_Legal__c = 'Banks of the World';
objAcc.Type__c = 'External';
objAcc.Sub_Type__c = 'Private';

insert objAcc;

//CreateVendor objCV = new CreateVendor();
CreateVendor.Create(objAcc.Id);
System.assertnotequals(objAcc.Vendor_ID__c,'');
}

}