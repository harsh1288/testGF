Public class ctrlcommunityredirect
{
   public User objUser;
 
public ctrlcommunityredirect(ApexPages.StandardController controller)
{
  String UserID = userinfo.getUserId();
  objUser = [Select ID, Profile.Name, IsPortalEnabled from User where id=:UserID];

}
Public PageReference pageRedirect(){
        system.debug('objUser.Profile.Name ' +objUser.Profile.Name);
        if (objUser.Profile.Name == Label.PR_Read_Only || objUser.Profile.Name == Label.PR_Read_Write_Edit || objUser.Profile.Name == Label.PR_Admin) 
        {        
                        system.debug('objUser.Profile.Name ' +objUser.Profile.Name);
                        PageReference pgrefnew = new PageReference('/GM/apex/GrantMakingHome');
                pgrefnew.setRedirect(true);
                return pgrefnew;
        }
         if (objUser.Profile.Name == Label.Applicant_CM_Admin|| objUser.Profile.Name == Label.Applicant_CM_Read_Only|| objUser.Profile.Name == Label.Applicant_CM_Read_Write) 
        {        
                        system.debug('objUser.Profile.Name ' +objUser.Profile.Name);
                        PageReference pgrefnew = new PageReference('/GM/apex/OpenConceptNotesH');
                pgrefnew.setRedirect(true);
                return pgrefnew;
        }
        return null;
    } 
}