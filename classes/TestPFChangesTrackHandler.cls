@isTest

public class TestPFChangesTrackHandler{
    public static testMethod void testCheckPFFIeld(){
           
           Account objAccount = TestClassHelper.createAccount();
           insert objAccount;
          
           Grant__c objGrant = TestClassHelper.insertGrant(objAccount);
           test.starttest();
           Implementation_Period__c objIP = TestClassHelper.createIPWithConceptNote(objGrant, objAccount);
           insert objIP;
           
           Performance_Framework__c objPF = TestClassHelper.createPF(objIP);
           objPF.PF_status__c = Label.IP_Return_back_to_PR;
           objPF.Is_Changed_Submitted__c = false;
           objPF.PF_Status__c = Label.PF_Status_Submitted_to_MEPH;
           insert objPF;
           
           Catalog_Module__c objCatMod = TestClassHelper.createCatalogModule(); 
           insert objCatMod;
           
           Module__c objModule =  TestClassHelper.createModule();
           objModule.Catalog_Module__c = objCatMod.Id;
           objModule.Implementation_Period__c = objIP.Id;
           objModule.Performance_Framework__c = objPF.Id;
           insert objModule;
           
           Grant_Indicator__c objGIImp = TestClassHelper.createGrantIndicator();
           objGIImp.Performance_Framework__c = objPF.Id; 
           objGIImp.Indicator_Type__c = 'Impact';
           objGIImp.Grant_Implementation_Period__c = objIP.id; 
           insert objGIImp;   
           
           Goals_Objectives__c goals = TestClassHelper.createGoalsAndObjective('Goal', objIP.id);
           insert goals;
           
           Implementer__c imp = TestClassHelper.createImplementor();
           imp.Grant_Implementation_Period__c =  objIP.id; 
           insert imp;
           
           Period__c periodObj  = TestClassHelper.createPeriod();
           periodObj.Implementation_Period__c= objIp.Id;
           insert periodObj;
           
           Grant_Intervention__c Gi =  TestClassHelper.createGrantIntervention(objIp);
           insert gi;
        
    }
    
     public static testMethod void testCheckPFFIeldPositive(){
           
           Account objAccount = TestClassHelper.createAccount();
           insert objAccount;
          
           Grant__c objGrant = TestClassHelper.insertGrant(objAccount);
           test.starttest();
           Implementation_Period__c objIP = TestClassHelper.createIPWithConceptNote(objGrant, objAccount);
           insert objIP;
           
           Performance_Framework__c objPF = TestClassHelper.createPF(objIP);
           objPF.Is_Changed_Submitted__c = true;
           objPF.PF_Status__c = Label.PF_Status_Rejected;
           insert objPF;
           
           /*Performance_Framework__c objPF1 = TestClassHelper.createPF(objIP);
           objPF1.PF_status__c = Label.IP_Return_back_to_PR;
           objPF1.Is_Changed_Submitted__c = false;
           insert objPF1;*/
           
           Catalog_Module__c objCatMod = TestClassHelper.createCatalogModule(); 
           insert objCatMod;
           
           Module__c objModule =  TestClassHelper.createModule();
           objModule.Catalog_Module__c = objCatMod.Id;
           objModule.Implementation_Period__c = objIP.Id;
           objModule.Performance_Framework__c = objPF.Id;
           insert objModule;
           
           
           Indicator__c indi = TestClassHelper.insertCatalogIndicator();
           
           Catalog_Disaggregated__c cd = new Catalog_Disaggregated__c();
           cd.Catalog_Indicator__c = indi.id;
           cd.Disaggregation_Category__c = 'Age';
           cd.Disaggregation_Value__c = '<5';
           insert cd;

           Grant_Indicator__c objGIImp = TestClassHelper.createGrantIndicator();
           objGIImp.Performance_Framework__c = objPF.Id; 
           objGIImp.Indicator_Type__c = 'Impact';
           objGIImp.Grant_Implementation_Period__c = objIP.id; 
           objGIImp.Indicator__c = indi.Id;
           insert objGIImp;   
               
           Goals_Objectives__c goals = TestClassHelper.createGoalsAndObjective('Goal', objIP.id);
           insert goals;
           
           Implementer__c imp = TestClassHelper.createImplementor();
           imp.Grant_Implementation_Period__c =  objIP.id; 
           insert imp;
           
           Period__c periodObj  = TestClassHelper.createPeriod();
           periodObj.Implementation_Period__c= objIp.Id;
           insert periodObj;
           
           Grant_Intervention__c Gi =  TestClassHelper.createGrantIntervention(objIp);
           insert gi;
           
           update objModule;
           update imp;
           update objGIImp;
           update periodObj;
           update gi;
           update goals;
                      
           delete objModule;
           delete imp;
           delete objGIImp;
           delete periodObj;
           delete gi;
           delete goals;

           
    }
}