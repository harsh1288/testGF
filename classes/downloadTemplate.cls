public with sharing class downloadTemplate{
    public String SelectedTemplate {get;set;}
    public String strUserLanguage {get;set;}
        
    
    public downloadTemplate(ApexPages.standardcontroller ctrl) {
        SelectedTemplate = '';
        strUserLanguage = '';
        
        strUserLanguage = 'ENGLISH';
        if(System.UserInfo.getLanguage() == 'fr'){
            strUserLanguage = 'FRENCH'; }
        if(System.UserInfo.getLanguage() == 'ru'){
            strUserLanguage = 'RUSSIAN'; }
        if(System.UserInfo.getLanguage() == 'es'){
            strUserLanguage = 'SPANISH'; } 
    }
    
    public PageReference downloadTemplate() {
        if(SelectedTemplate == Null)
        SelectedTemplate = Label.Grant_Making_narrative_Template_english;
        return new PageReference(Selectedtemplate);
    }
}