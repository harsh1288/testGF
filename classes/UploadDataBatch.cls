global class UploadDataBatch implements Database.Batchable<string>, Database.Stateful
{
   global final blob dataDocuments;
   global String ObjectName;
   global UploadDataBatch ()
   {
   }
   global UploadDataBatch (blob data,String ObjectName)
   {
       this.dataDocuments=data;
       this.ObjectName=ObjectName;
   }

   global Iterable<string>  start(Database.BatchableContext BC)
   {
      string nameFile=this.dataDocuments.toString();     
      return new CSVIterator(this.dataDocuments.toString(), '\n');
   }

   global void execute(Database.BatchableContext BC,List<String> scope)
   {
       //integer i=0;
          /*******************************************************************/
        Map<string, schema.sobjecttype> gd = Schema.getGlobalDescribe();
        Set<string> objectKeys = gd.keySet();
        if(objectKeys.contains(ObjectName.toLowerCase()))
        {
            //Creating a new sObject
               List<sObject> lst= new List<sObject>{Schema.getGlobalDescribe().get(ObjectName).newSObject()};
               for(String row : scope)
               {
                 List<String> fields = row .split(',');
                 IF(fields[0]!='*Date')
                 {
                     sObject li= Schema.getGlobalDescribe().get(ObjectName).newSObject();                                                           
                     lst.add(li);        
                  }
                  System.debug('@@@@@@@@@@@@@@'+row);
                }
            try
            {                               
    //           insert lst;
            }
            Catch(Exception e)
            {
   //             ApexPages.addMessages(e) ;
            }
        }
        else
        {
   //         ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Object API name in file is invalid')) ;
        }
   /*******************************************************************/
   }

   global void finish(Database.BatchableContext BC){
 
     system.debug(' Data upload Complete');
   }

}