/*********************************************************************************
* Controller Class: GrantCoverageOutputIndicator
* DateCreated:  08/26/2013
----------------------------------------------------------------------------------
* Purpose/Methods:
* - page is opened from a button on the GM ModulesInterventions page
* - creates Result/Target record for each Grant_Indicator__c records where 
    Type = 'Coverage/Output' and Module =: master module__c.Module
----------------------------------------------------------------------------------
* Unit Test: TestGrantCoverageOutputIndicator
----------------------------------------------------------------------------------
* History:
* - VERSION  DATE            DETAIL FEATURES
    1.0      08/26/2013      INITIAL DEVELOPMENT   
*********************************************************************************/
Public Class GrantCoverageOutputIndicator{ 
    Public String strModuleId {get;set;}
    Public String strCatalogModuleId {get;set;}
    Public String strImplementationPeriodId {get;set;}    
    Public List<GrantIndicatorResult> lstGrantIndicatorResult{get;set;}
    Public List<Page__c> lstPages {get;set;}
    Public List<Module__c> lstModules {get;set;}
    Public List<Period__c> lstPeriods{get;set;}
    Public GrantIndicatorResult objCustomGrantIndicatorResult{get;set;}
    Public GrantIndicatorResult objStandardGrantIndicatorResult{get;set;}
    Public Boolean blnExpandSection {get;set;}
    Public String strComponent {get;set;}
    
    Public List<SelectOption> CatalogIndicatorOptions {get;set;}
    Public String strSelectedIndicator {get;set;}
    Public Map<ID,Indicator__c> MapCatalogIndicator;
    Public Boolean blnConfirmIndicatorDelete {get;set;}
    Public Map<Id,GrantIndicatorResult> mapGIIdTowrap {get;set;}
    
    Public String strLanguage {get;set;}
    Public String strHome {get;set;}
    Public String strModules {get;set;}
    Public String strHealthProducts {get;set;}
    Public String strDetailedBudget {get;set;}
    Public String strClosepanelLabel {get;set;}
    Public String strOutputIndicators {get;set;}
    Public String strCoverage {get;set;}
    Public String strResponsiblePrincipalRecipient {get;set;}
    Public String strTargetArea{get;set;}
    Public String strTiedTo{get;set;}
    Public String strNum {get;set;}
    Public String strDen {get;set;}
    Public String strTargetAssumptions {get;set;}
    Public String strAddAnIndicator {get;set;}
    Public String strSeeCNTargets {get;set;}
    Public String strConceptNotes {get;set;}
    Public String strSummary {get;set;}
    Public String strGoalsAndImpactIndicators {get;set;}
    Public String strGuidance {get;set;}
    Public String strEdit {get;set;}
    Public String strDelete {get;set;}
    Public String strSelectCatalogIndicator {get;set;}
    Public String strBaseline {get;set;}
    Public String strYear {get;set;}
    Public String strSource {get;set;}
    Public String strComments {get;set;}
    Public String strCountryTeamComments {get;set;}
    Public String strLFAComments {get;set;}
    Public String strTargets {get;set;}
    Public String strDataType {get;set;}
    Public String strSave {get;set;}
    Public String strCancel {get;set;}
    Public String strAreYouSure {get;set;}
    Public String strIndicatorDeleted {get;set;}
    Public String strAddStandardIndicator {get;set;}
    Public String strAddCustomIndicator {get;set;}
    Public String strSelect{get;set;}
    Public String strGuidanceId {get;set;}
    Public String strAnd {get;set;}
    
    /**********************************************************************************************
    Purpose: Initialize the constructor method  
    Parameters: NA
    Returns: NA
    Throws [Exceptions]: NA
    **********************************************************************************************/
    public GrantCoverageOutputIndicator(ApexPages.StandardController controller) {
        System.Debug('Entering GrantCoverageOutputIndicator'); 
        strModuleId = Apexpages.currentpage().getparameters().get('id');
        system.debug(' Module Id** '+strModuleId);
        strAnd = '&';
        if(String.IsBlank(strModuleId) == false){
            List<Module__c> lstModule = [Select ID, Name, Implementation_Period__c,Language__c,Catalog_Module__c,Implementation_Period__r.Component__c 
                                        From Module__c 
                                        Where Id =: strModuleId Limit 1];
            if(lstModule!=null && lstModule.size() > 0) {
                strImplementationPeriodId = lstModule[0].implementation_Period__c;
                strComponent = lstModule[0].Implementation_Period__r.Component__c; 
                if(strComponent == 'Health Systems Strengthening'){
                strComponent = 'HSS';
                }
                strCatalogModuleId = lstModule[0].Catalog_Module__c;
                if(lstModule[0].Language__c != null) strLanguage = lstModule[0].Language__c;
                else strLanguage = 'ENGLISH';
                
                lstPages = new List<Page__c>();   
                lstPages = [Select Id,Name,URL_Prefix__c,Order__c,Modular__c From Page__c Where Implementation_Period__c =: strImplementationPeriodId Order by Order__c];
                lstModules = new List<Module__c>();   
                lstModules = [Select Id,Name,Implementation_Period__c,implementation_Period__r.Name From Module__c Where Implementation_Period__c =: strImplementationPeriodID Order by Name]; 
          
                blnConfirmIndicatorDelete = false;
                getPageText();
                fillPeriods();
                fillGrantIndicatorResult();
                fillCatalogIndicator();
                AddNewCustomGrantIndicatorResult();
            }
        }        
        mapGIIdTowrap = new Map<Id,GrantIndicatorResult>();
        System.Debug('Exiting GrantCoverageOutputIndicator'); 
          List<Guidance__c> lstGuidance = [Select Id from Guidance__c where Name =: label.GM_Coverage_Output_Indicators];
            if(!lstGuidance.isEmpty()) 
            {
              strGuidanceId = lstGuidance[0].Id;
            }
        
    }

    /**********************************************************************************************
    Purpose: Fill Catalog Indicator to add as Standard Indicator
    Parameters: NA
    Returns: NA
    Throws [Exceptions]: NA
    **********************************************************************************************/
    Public void fillCatalogIndicator(){
        System.Debug('Entering fillCatalogIndicator');  
        mapCatalogIndicator = new Map<Id,Indicator__c>
        ([
            Select id,Name,Type_of_Data__c,Full_Name_En__c,Target_Accumulation__c from Indicator__c 
            Where Catalog_Module__c =: strCatalogModuleId
            And Indicator_Type__c = 'Coverage/Output' 
            And Full_Name_En__c != null And Type_of_Data__c != null
                And Id Not IN (Select Indicator__c 
                From Grant_Indicator__c 
                Where Indicator_Type__c =: 'Coverage/Output'
                and Parent_Module__c =: strModuleId)
        ]);
        CatalogIndicatorOptions = new List<SelectOption>();
        CatalogIndicatorOptions.add(new SelectOption('','--None--'));
        if(mapCatalogIndicator!=null && mapCatalogIndicator.size() > 0){
            for(Indicator__c objInd : mapCatalogIndicator.values()){
                CatalogIndicatorOptions.add(new SelectOption(objInd.id,objInd.Full_Name_En__c));
            }
        }
        System.Debug('Exiting fillCatalogIndicator');
    }
    
    /**********************************************************************************************
    Purpose: Fill list of Periods to maintain related results of indicator
    Parameters: NA
    Returns: NA
    Throws [Exceptions]: NA
    **********************************************************************************************/
    Private void fillPeriods(){
        System.Debug('Exiting fillPeriods');
        if(lstPeriods==null) lstPeriods = new List<Period__c>();
        lstPeriods = [select id,name, Start_Date__c, End_Date__c, Start_Date_Short__c 
                    from Period__c 
                    where Implementation_Period__c  =: strImplementationPeriodId
                    Order by Start_Date__c asc];
        System.Debug('Entering fillPeriods');
    }
    
    /**********************************************************************************************
    Purpose: Fill list of Periods to maintain related results of indicator
    Parameters: NA
    Returns: NA
    Throws [Exceptions]: NA
    **********************************************************************************************/
    Private Void fillGrantIndicatorResult(){
        System.Debug('Entering fillPeriods');
        lstGrantIndicatorResult = new List<GrantIndicatorResult>();
        GrantIndicatorResult objGrantIndicatorResult;
        if(lstPeriods!=null && lstPeriods.size()>0){                   
            List<Grant_Indicator__c> lstGrantIndicators = [select id, name,Indicator_Type__c,Comments__c,
                                Target_Area__c, LFA_Comments__c, Country_Team_Comments__c, Tied_To__c, Indicator__c, Baseline_numerator__c ,Baseline_Denominator__c, Baseline_Value__c,
                                Baseline_Year__c ,Baseline_Sources__c, Indicator_Full_Name__c, Data_Type__c,Target_Accumulation__c,Component__c,Decimal_Places__c,
                                (
                                    select id,name,Period__r.Period_Number__c,Period__r.Start_Date__c, Period__r.Start_Date_Short__c, Target_Denominator__c,Target_Numerator__c,Target__c,Comments__c,LFA_Comments__c,Country_Team_Comments__c
                                    from Results__r 
                                    where Period__c IN: lstPeriods
                                    order by Period__r.Start_Date__c asc
                                )
                                from Grant_Indicator__c 
                                where Indicator_Type__c =: 'Coverage/Output'
                                and Parent_Module__c =: strModuleId
                                order by Indicator_Number__c]; 
            system.debug('Query List '+lstGrantIndicators +' Module Id** '+strModuleId);
            List<Grant_Indicator__feed> lstIndicatorfeed= [select id,ParentID from Grant_Indicator__feed where parentId IN: lstGrantIndicators];
            Map<ID,Integer> IndicatorsWithFeedItem = new Map<ID,Integer>();
            for(Grant_Indicator__feed objIndiWithFeed: lstIndicatorfeed){
                if(IndicatorsWithFeedItem.get(objIndiWithFeed.ParentID)==null){
                    IndicatorsWithFeedItem.Put(objIndiWithFeed.ParentID,1);
                }else{
                    IndicatorsWithFeedItem.Put(objIndiWithFeed.ParentID,IndicatorsWithFeedItem.get(objIndiWithFeed.ParentID)+1);
                }
            }
                                 
                                
            for(Grant_Indicator__c objGI : lstGrantIndicators){
                objGrantIndicatorResult = new GrantIndicatorResult();
                objGrantIndicatorResult.blnIndicatorDisplay  = true;
                objGrantIndicatorResult.blndisplaySave= false;                
                objGrantIndicatorResult.objGrantIndicator = objGI;
                objGrantIndicatorResult.lstResults = objGI.Results__r;
                if(objGI.Results__r!=null && objGI.Results__r.size()>0){
                    objGrantIndicatorResult.strAssumptions = objGI.Results__r[0].Comments__c;  
                    objGrantIndicatorResult.strLFAComments=objGI.Results__r[0].LFA_Comments__c; 
                    objGrantIndicatorResult.strCountryTeamComments=objGI.Results__r[0].Country_Team_Comments__c;           
                }
                if(IndicatorsWithFeedItem.get(objGI.ID)!=null){
                        objGrantIndicatorResult.blnHasPost = true;                     
                        objGrantIndicatorResult.intPostCount = IndicatorsWithFeedItem.get(objGI.ID);                     
                }
                lstGrantIndicatorResult.add(objGrantIndicatorResult);
            }
        }
        System.Debug('Exiting fillPeriods List'+lstGrantIndicatorResult);
    }
    
    /**********************************************************************************************
    Purpose: Sets a New Custom Grant Indicator with Result per Period to be filled by user to save
    Parameters: NA
    Returns: NA
    Throws [Exceptions]: NA
    **********************************************************************************************/
    Private void AddNewCustomGrantIndicatorResult(){
        System.Debug('Entering AddNewCustomGrantIndicatorResult');
        if(lstGrantIndicatorResult==null) lstGrantIndicatorResult = new List<GrantIndicatorResult>();
        objCustomGrantIndicatorResult= new GrantIndicatorResult();
        objCustomGrantIndicatorResult.blnIndicatorDisplay  = true;
        objCustomGrantIndicatorResult.blndisplaySave= false; 
        objCustomGrantIndicatorResult.objGrantIndicator = new Grant_Indicator__c(Parent_Module__c = strModuleId,Indicator_Type__c = 'Coverage/Output',Component__c = strComponent);
        objCustomGrantIndicatorResult.strAssumptions ='';
        objCustomGrantIndicatorResult.strCountryTeamComments ='';
        objCustomGrantIndicatorResult.strLFAComments ='';
        objCustomGrantIndicatorResult.lstResults = new List<Result__c>();
        Result__c objResult;
        if(lstPeriods!=null && lstPeriods.size()>0){
            for(Period__c objPeriod : lstPeriods){
                objResult = new Result__c();
                objResult.Period__c = objPeriod.ID ;
                objCustomGrantIndicatorResult.lstResults.add(objResult);
            }
        }
        System.Debug('Exiting AddNewCustomGrantIndicatorResult');
    }
    
    /**********************************************************************************************
    Purpose: Sets a New Standard Grant Indicator with Result per Period to be filled by user to save
    Parameters: NA
    Returns: NA
    Throws [Exceptions]: NA
    **********************************************************************************************/
    Private void AddNewStandardGrantIndicatorResult(){
        System.Debug('Entering AddNewStandardGrantIndicatorResult');
        if(lstGrantIndicatorResult==null) lstGrantIndicatorResult = new List<GrantIndicatorResult>();
        objStandardGrantIndicatorResult= new GrantIndicatorResult();
        objStandardGrantIndicatorResult.blnIndicatorDisplay  = true;
        objStandardGrantIndicatorResult.blndisplaySave= false;        
        objStandardGrantIndicatorResult.objGrantIndicator = new Grant_Indicator__c(Parent_Module__c = strModuleId,Indicator_Type__c = 'Coverage/Output');
        objStandardGrantIndicatorResult.strAssumptions ='';
        objStandardGrantIndicatorResult.strCountryTeamComments ='';
        objStandardGrantIndicatorResult.strLFAComments ='';
        objStandardGrantIndicatorResult.lstResults = new List<Result__c>();
        Result__c objResult;
        if(lstPeriods!=null && lstPeriods.size()>0){
            for(Period__c objPeriod : lstPeriods){
                objResult = new Result__c();
                objResult.Period__c = objPeriod.ID ;
                objStandardGrantIndicatorResult.lstResults.add(objResult);
            }
        } 
        System.Debug('Exiting AddNewStandardGrantIndicatorResult');       
    }
    
    /**********************************************************************************************
    Purpose: Sets boolean flags to edit Grant Indicator and Result per period
    Parameters: NA
    Returns: NA
    Throws [Exceptions]: NA
    **********************************************************************************************/
    Public void EditGrantIndicatorResult(){
        System.Debug('Entering EditGrantIndicatorResult');
        System.Debug('Parameters[0] EditIndiIndex = '+apexpages.currentpage().getparameters().get('EditIndiIndex'));
        Integer intIndexIndicator = integer.valueof(apexpages.currentpage().getparameters().get('EditIndiIndex'));
        
         
        GrantIndicatorResult objGrantIndicatorResultTemp = new GrantIndicatorResult();
        objGrantIndicatorResultTemp.blnIndicatorDisplay = lstGrantIndicatorResult[intIndexIndicator].blnIndicatorDisplay;
        objGrantIndicatorResultTemp.blndisplaySave = lstGrantIndicatorResult[intIndexIndicator].blndisplaySave;
        //objGrantIndicatorResultTemp.blnIndicatorDisplay = true;
        objGrantIndicatorResultTemp.objGrantIndicator = lstGrantIndicatorResult[intIndexIndicator].objGrantIndicator.clone(true);
        objGrantIndicatorResultTemp.strAssumptions = lstGrantIndicatorResult[intIndexIndicator].strAssumptions;
        objGrantIndicatorResultTemp.strCountryTeamComments = lstGrantIndicatorResult[intIndexIndicator].strCountryTeamComments;
        objGrantIndicatorResultTemp.strLFAComments = lstGrantIndicatorResult[intIndexIndicator].strLFAComments;
        objGrantIndicatorResultTemp.lstResults = new List<Result__c>();
        if(lstGrantIndicatorResult[intIndexIndicator].lstResults.size()!=0){
        objGrantIndicatorResultTemp.lstResults.addall(lstGrantIndicatorResult[intIndexIndicator].lstResults);
        }
            if(lstGrantIndicatorResult[intIndexIndicator].lstResults.size()==0){
        Result__c objResult;
        if(lstPeriods!=null && lstPeriods.size()>0){
            for(Period__c objPeriod : lstPeriods){
                objResult = new Result__c();
                objResult.Period__c = objPeriod.ID ;
                objResult.Indicator__c = objGrantIndicatorResultTemp.objGrantIndicator.id;
                insert objResult;
               System.debug('*****objResult'+objResult);
               objGrantIndicatorResultTemp.lstResults.add(objResult);
               lstGrantIndicatorResult[intIndexIndicator].lstResults.add(objResult);
            }
          //lstGrantIndicatorResult.add(objResult);  
        } 
        
        }
        
        //objGrantIndicatorResultTemp.lstResults = new List<Result__c>();
        //if(lstGrantIndicatorResult[intIndexIndicator].lstResults.size()!=0){
        //objGrantIndicatorResultTemp.lstResults.addall(lstGrantIndicatorResult[intIndexIndicator].lstResults);
        //}
        objGrantIndicatorResultTemp.blnHasPost = lstGrantIndicatorResult[intIndexIndicator].blnHasPost;
        objGrantIndicatorResultTemp.intPostCount = lstGrantIndicatorResult[intIndexIndicator].intPostCount;
        mapGIIdTowrap.put(objGrantIndicatorResultTemp.objGrantIndicator.id,objGrantIndicatorResultTemp);
        lstGrantIndicatorResult[intIndexIndicator].blnIndicatorDisplay = false;
        lstGrantIndicatorResult[intIndexIndicator].blndisplaySave = true;
        System.debug('*****Outside If of Edit'+lstGrantIndicatorResult[intIndexIndicator].lstResults.size());
        //fillGrantIndicatorResult();
        
        System.Debug('Exiting EditGrantIndicatorResult');
    }
    /**********************************************************************************************
    Purpose: Resets boolean flags to cancel Grant Indicator and Result per period from edit
    Parameters: NA
    Returns: NA
    Throws [Exceptions]: NA
    **********************************************************************************************/
    Public void CancelGrantIndicatorResult(){
        System.Debug('Entering CancelGrantIndicatorResult');
        System.Debug('Parameters[0] CancelIndiIndex = '+apexpages.currentpage().getparameters().get('CancelIndiIndex'));
        Integer intIndexIndicator = integer.valueof(apexpages.currentpage().getparameters().get('CancelIndiIndex'));
        String GIid = lstGrantIndicatorResult[intIndexIndicator].objGrantIndicator.id;
        lstGrantIndicatorResult[intIndexIndicator] = new GrantIndicatorResult();
        lstGrantIndicatorResult[intIndexIndicator] = mapGIIdTowrap.get(GIid);
        lstGrantIndicatorResult[intIndexIndicator].blnIndicatorDisplay = true;
        lstGrantIndicatorResult[intIndexIndicator].blndisplaySave = false;
        System.Debug('Exiting CancelGrantIndicatorResult');
    }
    
    /**********************************************************************************************
    Purpose: Resets boolean flags to cancel Grant Indicator and Result per period from edit
    Parameters: NA
    Returns: NA
    Throws [Exceptions]: NA
    **********************************************************************************************/
    Public void DeleteGrantIndicatorResult(){
        System.Debug('Entering DeleteGrantIndicatorResult');
        System.Debug('Parameters[0] DeleteIndiIndex = '+apexpages.currentpage().getparameters().get('DeleteIndiIndex'));
        Integer intDeleteIndexIndicator = integer.valueof(apexpages.currentpage().getparameters().get('DeleteIndiIndex'));
        if(intDeleteIndexIndicator != null){
            List<Grant_Indicator__c> lstIndicatorToDelete = new List<Grant_Indicator__c>();
            GrantIndicatorResult objGrantIndicatorResult = lstGrantIndicatorResult[intDeleteIndexIndicator];
            if(objGrantIndicatorResult.objGrantIndicator.Id != null){
                if(objGrantIndicatorResult.lstResults !=null){
                    List<Result__c> lstResultToDelete = new List<Result__c>();
                    for(Result__c objResult : objGrantIndicatorResult.lstResults){
                        if(objResult.id!=null){
                            lstResultToDelete.add(objResult);
                        }
                    }
                    delete lstResultToDelete;
                }
                delete lstGrantIndicatorResult[intDeleteIndexIndicator].objGrantIndicator;
            }
            lstGrantIndicatorResult.remove(intDeleteIndexIndicator);
            
            List<Grant_Indicator__c> lstIndicatorToUpdate = [Select Id,Indicator_number__c From Grant_Indicator__c 
                                                Where Indicator_number__c >: intDeleteIndexIndicator 
                                                And Indicator_Type__c = 'Coverage/Output'
                                                And Parent_Module__c =: strModuleId];
            if(lstIndicatorToUpdate.size() > 0){
                for(Grant_Indicator__c objGi : lstIndicatorToUpdate){
                    objGi.Indicator_number__c = objGi.Indicator_number__c - 1;
                }
                Update lstIndicatorToUpdate;
            }
            
            
            blnConfirmIndicatorDelete = false;
            fillCatalogIndicator();
        }
        System.Debug('Exiting DeleteGrantIndicatorResult');
    }
    
    /**********************************************************************************************
    Purpose: Save Grant Indicator Result object after editing
    Parameters: NA
    Returns: NA
    Throws [Exceptions]: NA
    **********************************************************************************************/
    Public void SaveGrantIndicatorResult(){
        System.Debug('Entering SaveGrantIndicatorResult');
        System.Debug('Parameters[0] SaveIndiIndex = '+apexpages.currentpage().getparameters().get('SaveIndiIndex'));
        Integer intIndexIndicator = integer.valueof(apexpages.currentpage().getparameters().get('SaveIndiIndex'));
        if(intIndexIndicator != null){
            if(mapGIIdTowrap.ContainsKey(lstGrantIndicatorResult[intIndexIndicator].objGrantIndicator.id)){
                mapGIIdTowrap.remove(lstGrantIndicatorResult[intIndexIndicator].objGrantIndicator.id);
            }
            update lstGrantIndicatorResult[intIndexIndicator].objGrantIndicator;
            for(Result__c objResult: lstGrantIndicatorResult[intIndexIndicator].lstResults){
                objResult.Comments__c=lstGrantIndicatorResult[intIndexIndicator].strAssumptions;
                objResult.LFA_Comments__c = lstGrantIndicatorResult[intIndexIndicator].strLFAComments;
                objResult.Country_Team_Comments__c = lstGrantIndicatorResult[intIndexIndicator].strCountryTeamComments;
            }
            update lstGrantIndicatorResult[intIndexIndicator].lstResults;
        }
        lstGrantIndicatorResult[intIndexIndicator].blnIndicatorDisplay = true;
        lstGrantIndicatorResult[intIndexIndicator].blndisplaySave = false;
        System.Debug('Exiting SaveGrantIndicatorResult');
    }
    
    Public void CreateIndicatorOnSelectCatalog(){
        if(String.isBlank(strSelectedIndicator) == false){
            AddNewStandardGrantIndicatorResult();
            Indicator__c objCatalogIndicator = MapCatalogIndicator.get(strSelectedIndicator);
            objStandardGrantIndicatorResult.objGrantIndicator.Standard_or_Custom__c = 'Standard';
            objStandardGrantIndicatorResult.objGrantIndicator.Indicator__c = objCatalogIndicator.Id;
            objStandardGrantIndicatorResult.objGrantIndicator.Indicator_Full_Name__c = objCatalogIndicator.Full_Name_En__c;
            objStandardGrantIndicatorResult.objGrantIndicator.Data_Type__c = objCatalogIndicator .Type_of_Data__c;
            objStandardGrantIndicatorResult.objGrantIndicator.Indicator_Type__c = 'Coverage/Output';
            objStandardGrantIndicatorResult.objGrantIndicator.Component__c = strComponent;
            objStandardGrantIndicatorResult.objGrantIndicator.Target_Accumulation__c = objCatalogIndicator .Target_Accumulation__c;
        }
    }
    
    Public void SaveStandardIndicator(){
        Integer SaveStdIndicator = integer.valueof(apexpages.currentpage().getparameters().get('SaveStdIndicator'));
        objStandardGrantIndicatorResult.objGrantIndicator.Indicator_Number__c = SaveStdIndicator;
        objStandardGrantIndicatorResult.objGrantIndicator.Indicator_Type__c = 'Coverage/Output';
        objStandardGrantIndicatorResult.objGrantIndicator.Component__c = strComponent;
        system.debug('List of Indicator Saved** '+objStandardGrantIndicatorResult.objGrantIndicator);
        insert objStandardGrantIndicatorResult.objGrantIndicator;
        for(Result__c objResult: objStandardGrantIndicatorResult.lstResults){
            objResult.Comments__c=objStandardGrantIndicatorResult.strAssumptions;
             objResult.LFA_Comments__c=objStandardGrantIndicatorResult.strLFAComments;
             objResult.Country_Team_Comments__c =objStandardGrantIndicatorResult.strCountryTeamComments;
            objResult.Indicator__c = objStandardGrantIndicatorResult.objGrantIndicator.ID;
        }        
        insert objStandardGrantIndicatorResult.lstResults;
        lstGrantIndicatorResult.add(objStandardGrantIndicatorResult);
        fillCatalogIndicator();
    }
    Public void SaveCustomIndicator(){
        Integer SaveCusIndicator = integer.valueof(apexpages.currentpage().getparameters().get('SaveCusIndicator'));
        objCustomGrantIndicatorResult.objGrantIndicator.Indicator_Number__c = SaveCusIndicator;
        objCustomGrantIndicatorResult.objGrantIndicator.Indicator_Type__c = 'Coverage/Output';
        objCustomGrantIndicatorResult.objGrantIndicator.Component__c = strComponent;
        insert objCustomGrantIndicatorResult.objGrantIndicator;
        for(Result__c objResult: objCustomGrantIndicatorResult.lstResults){
            objResult.Comments__c=objCustomGrantIndicatorResult.strAssumptions;
             objResult.LFA_Comments__c=objStandardGrantIndicatorResult.strLFAComments;
             objResult.Country_Team_Comments__c =objStandardGrantIndicatorResult.strCountryTeamComments;
            objResult.Indicator__c = objCustomGrantIndicatorResult.objGrantIndicator.ID;
        }
        insert objCustomGrantIndicatorResult.lstResults;
        lstGrantIndicatorResult.add(objCustomGrantIndicatorResult);
        AddNewCustomGrantIndicatorResult();
    }
    public void getPageText(){
        system.debug('#####strLanguage->'+strLanguage);
        if(String.IsBlank(strLanguage) == false){
            Map<String,String> MultiLingualTextMap;
            MultiLingualTextMap = GILanguage.getMultiLingualText(strLanguage,'GrantCoverageOutputIndicator');
            if(MultiLingualTextMap !=null && MultiLingualTextMap.size()>0)
            {
                strHome = MultiLingualTextMap.get('GOHome');
                strModules = MultiLingualTextMap.get('GOmodules');
                strHealthProducts = MultiLingualTextMap.get('GOhealthproducts');
                strDetailedBudget = MultiLingualTextMap.get('GOdetailedbudget');
                strClosepanelLabel = MultiLingualTextMap.get('GOClosePanelLabel');
                strOutputIndicators = MultiLingualTextMap.get('GOOutputIndicators');
                strCoverage = MultiLingualTextMap.get('GOCoverage');
                strResponsiblePrincipalRecipient = MultiLingualTextMap.get('GOResponsibleprincipalrecipient');
                strTargetArea = MultiLingualTextMap.get('GOstrTargetArea'); //remove Tied to
                strNum = MultiLingualTextMap.get('GONum');
                strDen = MultiLingualTextMap.get('GODen');
                strTargetAssumptions = MultiLingualTextMap.get('GOTargetassumptions');
                strAddAnIndicator = MultiLingualTextMap.get('GOAddanindicator');
                strSeeCNTargets = MultiLingualTextMap.get('GOSeeCNtargets');
                strConceptNotes = MultiLingualTextMap.get('GOConceptNotes');
                strSummary = MultiLingualTextMap.get('GOSummary');
                strGoalsAndImpactIndicators = MultiLingualTextMap.get('GOLabel');
                strGuidance = MultiLingualTextMap.get('GOGuidance');
                strEdit = MultiLingualTextMap.get('GIbtnEdit');
                strDelete = MultiLingualTextMap.get('GIbtnDelete');
                strSelectCatalogIndicator = MultiLingualTextMap.get('GObtnSelectCatalogIndicator');
                strBaseline = MultiLingualTextMap.get('GOBaseline');
                strYear = MultiLingualTextMap.get('GOYear');
                strSource = MultiLingualTextMap.get('GOSource');
                strComments = MultiLingualTextMap.get('GOComments');
                strCountryTeamComments = MultiLingualTextMap.get('GOCountryTeamComments');
                strLFAComments = MultiLingualTextMap.get('GOLFAComments');
                strTargets = MultiLingualTextMap.get('GOTargets');
                strTiedTo = MultiLingualTextMap.get('GOTiedTo');
                strDataType = MultiLingualTextMap.get('GODataType');
                strSave = MultiLingualTextMap.get('GIbtnSave');
                strCancel = MultiLingualTextMap.get('GIbtnCancel');
                strAreYouSure = MultiLingualTextMap.get('GOAreyousure');
                strIndicatorDeleted = MultiLingualTextMap.get('GOIndicatorWillDeleted');
                strAddStandardIndicator = MultiLingualTextMap.get('GIbtnAddStandardIndicator');
                strAddCustomIndicator = MultiLingualTextMap.get('GIbtnAddCustomIndicator');
                strSelect = MultiLingualTextMap.get('GIbtnSelect');
            }
        }
    }
    
    Public Class GrantIndicatorResult{
        Public Boolean blnIndicatorDisplay  {get;set;}
        Public Boolean blndisplaySave {get;set;}
        
        Public Grant_Indicator__c objGrantIndicator {get;set;}
        Public String strAssumptions {get;set;}
        Public String strCountryTeamComments {get;set;}
        Public String strLFAComments {get;set;}
        Public List<Result__c> lstResults {get;set;}
        Public Boolean blnHasPost {get;set;}
        Public Integer intPostCount {get;set;}
        
    }
}