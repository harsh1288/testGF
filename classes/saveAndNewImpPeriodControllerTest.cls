/*********************************************************************************
* {Test} Class: {saveAndNewImpPeriodControllerTest}
* Created by {Rahul Kumar},  {DateCreated 18-Nov-2014}
----------------------------------------------------------------------------------
* Purpose/Methods:
* - Used for cover all methods of saveAndNewImpPeriodController Controller.* 
*
----------------------------------------------------------------------------------
* History:
* - VERSION    DEVELOPER NAME     DATE      DETAIL FEATURES
      1.0       Rahul Kumar    18-Nov-2014
*********************************************************************************/

@isTest
Public class saveAndNewImpPeriodControllerTest{
    static saveAndNewImpPeriodController ext;
    static Implementation_Period__c masterObject;
    static PageReference pref;
    static Grant__c grant;
    static User testUser = TestClassHelper.createExtUser();
    static User testUser1 = TestClassHelper.createExtUser();
    Public static testMethod Void TestSaveAndNew(){
    testUser.username = 'savenewimp1hjf41@example.com';
    insert testUser;
            Test.startTest();
            grant = TestClassHelper.insertGrant(TestClassHelper.insertAccount());
            masterObject = TestClassHelper.createIPWithConceptNote(grant,TestClassHelper.insertAccount());
            insert masterObject;
            pref = Page.NewImplementer;
            pref.getParameters().put('id',masterObject.id);
            Test.setCurrentPage(pref);
            ApexPages.StandardController con = new ApexPages.StandardController(masterObject);
            ext = new saveAndNewImpPeriodController(con);
            ext.createNewIpurl();
            masterObject.Concept_Note__c =null;
            update masterObject;
            ext.createNewIpurl();
            Test.stopTest();
    }
    Public static testMethod Void TestSaveAndNewforExt(){
            testUser1.username = 'savenewimp286ff@example.com';
    insert testUser1;
            Test.startTest();
            Account acc = TestClassHelper.insertAccount();
            masterObject = TestClassHelper.createIPWithConceptNote(TestClassHelper.insertGrant(acc),TestClassHelper.insertAccount());
            insert masterObject;
            grant = TestClassHelper.createGrant(acc);
            insert grant;
            pref = Page.NewImplementer;
            pref.getParameters().put('id',masterObject.id);
            Test.setCurrentPage(pref);
            ApexPages.StandardController con = new ApexPages.StandardController(masterObject);
            ext = new saveAndNewImpPeriodController(con);
           System.runAs(testUser1) {                                    
            ext.createNewIpurl();            
        }
            masterObject.Concept_Note__c =null;
            update masterObject;
            pref.getParameters().put('retUrl','GM%2F'+masterObject.id);
            Test.setCurrentPage(pref);
            System.runAs(testUser1) {                                    
            ext.createNewIpurl();            
        }
            pref.getParameters().put('retUrl','?'+masterObject.id);
            Test.setCurrentPage(pref);
            System.runAs(testUser1) {                                    
            ext.createNewIpurl();            
        }
            Test.stopTest();
    }
}