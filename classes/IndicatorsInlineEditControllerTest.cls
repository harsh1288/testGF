@isTest
public class IndicatorsInlineEditControllerTest{

public static testMethod void TestMethod1(){
        
        Account objAcc = TestClassHelper.insertAccount();
        
        Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
        insert objGrant;
       
        Implementation_Period__c objIP = TestClassHelper.createIP(objGrant,objAcc);
        objIP.Reporting__c = 'Yearly';
        objIP.Status__c = 'Concept Note';
        objIP.PR_Fiscal_Cycle__c = 'January-December';
        insert objIP;   
        List<Performance_Framework__c > lstPF = new List<Performance_Framework__c >();
        Performance_Framework__c objPF = TestClassHelper.createPF(objIP);
        insert objPF; 
        lstPF.add(objPF);
        system.debug('**objPF'+objPF); 
        
        Grant_Indicator__c indicator = TestclassHelper.createGrantIndicator();
        indicator.Indicator_Type__c = 'Impact';
        indicator.Performance_Framework__c = objPF.Id;
        indicator.Reporting_Frequency__c = 'Based on Reporting Frequency';
        indicator.Data_Type__c = 'Number and Percent';
        
        insert indicator;
        
        Grant_Indicator__c indicator1 = TestclassHelper.createGrantIndicator();
        indicator1.Indicator_Type__c = 'Impact';
        indicator1.Performance_Framework__c = objPF.Id;
        indicator1.Reporting_Frequency__c = 'Based on Reporting Frequency';
        indicator1.Data_Type__c = 'Ratio';
        insert indicator1;
        
        ApexPages.currentPage().getParameters().put('id',objPF.id);
       
        ApexPages.StandardSetController standardController = new ApexPages.StandardSetController(lstPF);
        IndicatorsInlineEditController extObj = new IndicatorsInlineEditController(standardController);               

        extObj.EditDissagregation();

        extObj.saveList();
        extObj.ReturnToInd();
    }
}