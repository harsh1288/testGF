public class FutureClassInactiveUser{
  
   @future
   public static void updateContact(List<id> ConList) {
       
       List<user> updateUserList = new List<User>();
       List<User> UserList = [Select id,isActive from User where Contactid IN: ConList AND isActive=: TRUE];
    
        for(User u: UserList ){
            u.isActive = False;
            updateUserList.add(u);    
        }
        
        if(updateUserList.size()>0)
            update updateUserList;
        }
}