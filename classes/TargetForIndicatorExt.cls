public with sharing class TargetForIndicatorExt {
    public string indid{get;set;}
    public List<Result__c> lstresult{get;set;}
    public Grant_Indicator__c grantIndicatorRec {get;set;}
    public string sign{get;set;}
    public boolean blnHistory{get;set;}
    public String strLanguage {get;set;}
    public String decimalPlaces{get;set;}
    public List<Period__c> lstperiods {get;set;}
    public List<Result__c> lstResults{get;set;}
    Boolean flowToGrantIndicator;
    public Map<String, result__c>resultPeriodMap{get;set;}
    public TargetForIndicatorExt(ApexPages.StandardController controller) {
        flowToGrantIndicator = false;
        indid = ApexPages.currentpage().getparameters().get('Id');
        grantIndicatorRec = [select id, data_type__c, Decimal_Places__c, Grant_Implementation_Period__c, Reporting_Frequency__c
                             from Grant_Indicator__c 
                             where id =:indid LIMIT 1];
         if( grantIndicatorRec.Decimal_Places__c == null ){
            decimalPlaces = '0';
        }else{                     
            decimalPlaces = grantIndicatorRec.Decimal_Places__c;//'0';
        }
        resultPeriodMap = new Map<string, result__c>();
        strLanguage = 'ENGLISH';
        if(System.UserInfo.getLanguage() == 'fr'){
            strLanguage = 'FRENCH'; 
        }
        if(System.UserInfo.getLanguage() == 'ru'){
            strLanguage = 'RUSSIAN'; 
        }
        if(System.UserInfo.getLanguage() == 'es'){
            strLanguage = 'SPANISH'; 
        }
        if(grantIndicatorRec.Reporting_Frequency__c != 'Based on Reporting Frequency')
            flowToGrantIndicator = true;
        fillPeriods();
        fillResults();
        populateMap();
        
    }
    
    private void populateMap(){
        for( Period__c period: lstPeriods){
            if( !resultPeriodMap.containsKey(period.start_Date_short__c)){
                resultPeriodMap.put(period.start_Date_short__c, new Result__c(Period__c = period.ID,Indicator__c = indid,target__c = null));   
            }   
        }
    }
    Private void fillPeriods() {
        String implementationPeriodStr = grantIndicatorRec.Grant_Implementation_Period__c;
        String yearStr = 'Yearly';
        System.Debug('Entering fillPeriods');               
        if(lstperiods==null) lstperiods = new List<Period__c>();
        String queryStr;
        if(flowToGrantIndicator == true) 
            queryStr = 'select id,name, Start_Date__c, End_Date__c,isChanged__c, Start_Date_Short__c, Flow_to_GrantIndicator__c, Performance_Framework__c, Base_Frequency__c from Period__c where Implementation_Period__c  =: implementationPeriodStr and Base_Frequency__c =: yearStr and Is_Active__c = true Order by Start_Date__c asc';
        else
            queryStr = 'select id,name, Start_Date__c, End_Date__c,isChanged__c, Start_Date_Short__c, Flow_to_GrantIndicator__c, Performance_Framework__c, Base_Frequency__c from Period__c where Implementation_Period__c  =: implementationPeriodStr and Performance_Framework__c !=null Order by Start_Date__c asc';
        System.debug('>>>>>Query String'+queryStr);
        /*lstperiods = [select id,name, Start_Date__c, End_Date__c, Start_Date_Short__c, Flow_to_GrantIndicator__c, Performance_Framework__c
                      from Period__c 
                      where Implementation_Period__c  =: grantIndicatorRec.Grant_Implementation_Period__c
                      and Flow_to_GrantIndicator__c =: flowToGrantIndicator
                      Order by Start_Date__c asc];*/
        lstperiods = Database.query(queryStr);
    }
    private void fillResults() {
        if(lstResults == null) lstResults = new List<Result__c>();
            lstResults = [select id,name,is_Updated__c, Period__r.Period_Number__c,Period__r.Start_Date__c, Period__r.Start_Date_Short__c, 
                          Target_Denominator__c, Target_Numerator__c, Target__c, Period__r.Flow_to_GrantIndicator__c
                          from Result__c 
                          where Period__c IN: lstPeriods and Indicator__c =: grantIndicatorRec.id 
                          order by Period__r.Start_Date__c asc];
            for( Result__c res : lstResults){
                resultPeriodMap.put(res.Period__r.Start_Date_Short__c, res);
            }
    }
    
    Public void ShowHistoryPopup(){
        blnHistory = true;
    }
    Public void HidePopupHistory(){
        system.debug('**InsideHidePopup');
        blnHistory = false;
    }

}