@isTest(SeeAllData= false)
public class TesttrgDeleteIntervention{
   
     Public static testMethod void TesttrgDeleteIntervention(){ 
          Account objAcc1 = TestClassHelper.insertAccount();
     
     Country__c country = TestClassHelper.createCountry();
     country.X200DEMA_Exch_Rate_in_EUR__c = 10.0000;
     country.X200DEMA_Exch_Rate_in_USD__c = 10.000;
     insert country;
     
     Grant__c objGrant  = TestClassHelper.createGrant(objAcc1);
     objGrant.Country__c = country.Id;
     insert objGrant;
     
     test.startTest();
     Implementation_Period__c ObjImplementationPeriod = TestClassHelper.createIP(objGrant,objAcc1);
     ObjImplementationPeriod.Currency_of_Grant_Agreement__c = 'EUR';
     ObjImplementationPeriod.Local_Currency__c = 'COP';
     ObjImplementationPeriod.Other_Currency__c = 'USD';
     ObjImplementationPeriod.Exchange_Rate__c = 50;
     ObjImplementationPeriod.Budget_Status__c = 'Draft';
     
     ObjImplementationPeriod.High_level_budget_GAC_1_EUR__c = 10000000;
     ObjImplementationPeriod.High_level_budget_GAC_1_USD__c = 10000000;
     insert ObjImplementationPeriod;
     
     Performance_Framework__c objPF = TestClassHelper.createPF(ObjImplementationPeriod);
     insert objPF;
      
     IP_Detail_Information__c ipde = new IP_Detail_Information__c();
     ipde.Budget_Status__c = 'Not yet submitted by PR';
     ipde.Implementation_Period__c = ObjImplementationPeriod.Id;
     ipde.High_level_budget_GAC_1_EUR__c = 1000;
     ipde.Total_Budgeted_Amount__c = 10;
     //ipde.Remaining_Budget_Amount__c = 1000;
     insert ipde;
       
     Module__c ObjModule = TestClassHelper.createModule();
     ObjModule.Implementation_Period__c = ObjImplementationPeriod.id;
     ObjModule.Performance_Framework__c  = objPF.id;
     Id objRecModNTId = Schema.SObjectType.Module__c.getRecordTypeInfosByName().get('Grant-Making').getRecordTypeId();
     ObjModule.RecordTypeId = objRecModNTId;
     insert ObjModule; 
     
     Grant_Intervention__c ObjGrantIntervention = TestClassHelper.createGrantIntervention(ObjImplementationPeriod);
     ObjGrantIntervention.Module__c = ObjModule.id;
     ObjGrantIntervention.Performance_Framework__c = objPF.Id;
     insert ObjGrantIntervention;
     
     Id objRecModMEId = Schema.SObjectType.Module__c.getRecordTypeInfosByName().get('Submitted to MEPH - PR').getRecordTypeId(); 
     ObjModule.RecordTypeId = objRecModMEId ;
     update ObjModule;
     
     delete ObjModule;
      test.stopTest();  
      
     }
    
 }