/*********************************************************************************
* Test Class: {TestctrlChangeRequest_Approval}
*  DateCreated : 03-11-2014
----------------------------------------------------------------------------------
* Purpose/Methods:
* - Used for cover all code of TestctrlChangeRequest_Approval.
----------------------------------------------------------------------------------
* History:
* - VERSION  DEVELOPER NAME    DATE            DETAIL FEATURES
     1.0                       03-11-2014     INITIAL DEVELOPMENT
*********************************************************************************/
@isTest
Public Class TestctrlChangeRequestApproval{
    Public static testMethod Void TestctrlChangeRequestApproval() {
        Id strRecordTypeId = [Select Id From RecordType Where SobjectType = 'Account' And RecordType.DeveloperName = 'LFA'].Id;
        
        Account objAcc = new Account();
        objAcc.Name = 'Test Acc';
        objAcc.Short_Name__c= 'Test Acc';
        objAcc.RecordTypeId = strRecordTypeId;
        insert objAcc;
       
        Country__c objCountry = new Country__c();
        objCountry.Name = 'Test Country';
        objCountry.Region_Name__c = 'Test Region';
        objCountry.Used_for_LFA_Services__c = TRUE;
        insert objCountry;
        
        LFA_Work_Plan__c objWp = new LFA_Work_Plan__c();
        objWp.Name = 'Test Wp';
        objWp.LFA__c = objAcc.Id;
        objWp.Country__c = objCountry.id;
        objWP.Include_In_Library__c = TRUE;
        
        insert objWp;
        
        LFA_Service__c objService = new LFA_Service__c();
        objService.Name = 'PUDR';
        objService.LFA_Work_Plan__c = objWp.id;
        objService.Service_Type__c = 'Key Services';
        objService.Service_Sub_Type__c = 'PUDR';
        objService.Exclude_from_Library__c=FALSE;     
        objService.Status__c  ='Cancelled';   
        insert objService;
        
        LFA_Resource__c objResource = new LFA_Resource__c();
        //objResource.Name = 'Test Resource';
        objResource.LFA_Service__c = objService.id;
        objResource.Rate__c = 1;
        //objResource.LFA_Role__c = objRole1.Id;
        objResource.LOE__c = 1;
        objResource.CT_Planned_Cost__c = 1;
        objResource.CT_Planned_LOE__c = 1;
        objResource.TGF_Proposed_Cost__c = 1;
        objResource.TGF_Proposed_LOE__c = 1;
        objResource.Actual_LOE__c = 1;
        insert objResource;
       
        LFA_Role__c objRole = new LFA_Role__c();
        objRole.Name = 'Test Role';
        insert objRole;
        
        LFA_Resource__c objResource2 = new LFA_Resource__c();
        //objResource2.Name = 'Test Resource1';
        objResource2.LFA_Service__c = objService.id;
        objResource2.LFA_Role__c = objRole.id;
        objResource2.Rate__c = 2;
        objResource2.LOE__c = 2;
        objResource2.CT_Planned_Cost__c = 2;
        objResource2.CT_Planned_LOE__c = 2;
        objResource2.TGF_Proposed_Cost__c = 2;
        objResource2.TGF_Proposed_LOE__c = 2;
        objResource2.Actual_LOE__c = 2;
        
        insert objResource2;
        
        Contact objCon = new Contact();
        objCon.LastName = 'Test LName';
        objCon.AccountId = objAcc.Id;
        insert objCon;
        
        Rate__c objRate = new Rate__c();
        objRate.LFA_Role__c = objRole.Id;
        objRate.Contact__c = objCon.Id;
        objRate.Country__c = objCountry.Id;
        objRate.Rate__c = 1;
        objRate.Active__c = true;
        insert objRate;
        
        Grant__c objGrant = new Grant__c();
        objGrant.Name = 'Test Grant';
        objGrant.Country__c = objCountry.id;
        objGrant.Principal_Recipient__c = objAcc.id;
        objGrant.Financial_and_Fiduciary_Risks__c = 'Low';
        objGrant.Health_Services_and_Products_Risks__c = 'High';
        objGrant.Programmatic_and_Performance_Risks__c = 'Medium';
        insert objGrant;
        
        LFA_Service__c objService3 = new LFA_Service__c();
        objService3.Name = 'PUDR';
        objService3.LFA_Work_Plan__c = objWp.id;
        objService3.Service_Type__c = 'Key Services';
        objService3.Service_Sub_Type__c = 'PUDR';
        objService3.Status__c  ='Cancelled';
        
        insert objService3;
        
        LFA_Role__c objRole1 = new LFA_Role__c();
        objRole1.Name = 'Test Role1';
        insert objRole1;
        
        LFA_Resource__c objResource3 = new LFA_Resource__c();
        //objResource.Name = 'Test Resource';
        objResource3.LFA_Service__c = objService3.id;
        objResource3.Rate__c = 1;
        objResource3.LFA_Role__c = objRole1.Id;
        objResource3.LOE__c = 1;
        objResource3.CT_Planned_Cost__c = 1;
        objResource3.CT_Planned_LOE__c = 1;
        objResource3.TGF_Proposed_Cost__c = 1;
        objResource3.TGF_Proposed_LOE__c = 1;
        objResource3.Actual_LOE__c = 1;
        insert objResource3;
        
        Rate__c objRate1 = new Rate__c();
        objRate1.LFA_Role__c = objRole1.Id;
        objRate1.Contact__c = objCon.Id;
        objRate1.Country__c = objCountry.Id;
        objRate1.Rate__c = 1;
        objRate1.Active__c = true;
        insert objRate1;
        
        LFA_Work_Plan__c objWp1 = new LFA_Work_Plan__c();
        objWp1.Name = 'Test Wp';
        objWp1.LFA__c = objAcc.Id;
        objWp1.Country__c = objCountry.id;
        objWp1.End_Date__c = system.today();
        objWp1.Status__c = 'LFA Change Request';
        insert objWp1;
        
        LFA_Service__c objService4 = new LFA_Service__c();
        objService4.Name = 'PUDR';
        objService4.LFA_Work_Plan__c = objWp1.id;
        objService4.Service_Type__c = 'Key Services';
        objService4.Service_Sub_Type__c = 'PUDR';
        objService4.Status__c  ='LFA Change Request';
        insert objService4;
        
        LFA_Resource__c objResource4 = new LFA_Resource__c();
        //objResource.Name = 'Test Resource';
        objResource4.LFA_Service__c = objService4.id;
        objResource4.Rate__c = 1;
        objResource4.LFA_Role__c = objRole1.Id;
        objResource4.LOE__c = 1;
        objResource4.CT_Planned_Cost__c = 1;
        objResource4.CT_Planned_LOE__c = 1;
        objResource4.TGF_Proposed_Cost__c = 1;
        objResource4.TGF_Proposed_LOE__c = 1;
        objResource4.Actual_LOE__c = 1;
        insert objResource4;
        
        LFA_Service__c objService5 = new LFA_Service__c();
        objService5.Name = 'PUDR';
        objService5.LFA_Work_Plan__c = objWp1.id;
        objService5.Service_Type__c = 'Key Services';
        objService5.Service_Sub_Type__c = 'PUDR';
        objService5.Status__c  ='LFA Change Request';
        objService5.Approver_1__c = userInfo.getUserId();
        insert objService5;
        
        LFA_Resource__c objResource5  = new LFA_Resource__c();
        //objResource.Name = 'Test Resource';
        objResource5.LFA_Service__c = objService5.id;
        objResource5.Rate__c = 1;
        objResource5.LFA_Role__c = objRole1.Id;
        objResource5.LOE__c = 120;
        objResource5.CT_Planned_Cost__c = 1;
        objResource5.CT_Planned_LOE__c = 1;
        objResource5.TGF_Proposed_Cost__c = 1;
        objResource5.TGF_Proposed_LOE__c = 1;
        objResource5.Actual_LOE__c = 2;
        objResource5.Proposed_LOE__c = 12;
        objResource5.CT_Proposed_LOE__c = 10;
        insert objResource5;
        
        
        ctrlChangeRequest_Approval objCls = new ctrlChangeRequest_Approval();
        
        objCls.lstDoc = new List<Document>([Select id from Document where Name='TGF-logo']);
        
        objCls.strWorkPlanId = objWp1.id;
        objCls.strServiceId_Approval = objService5.id;
       
        objCls.objServiceForType.Service_Sub_Type__c=objService5.id;
        objCls.objServiceForType.Service_Type__c=objService5.id;
        objCls.SelectedGrantMain = 'Multiple'; 
      
        String strcost = objCls.getCost() ;
        
        objCls.SelectedCountry = 'aaa';
        objCls.SelectedService = 'LFA';
        objCls.SelectedRegion = 'xyzz';
        objCls.SelectedYear = '2001';
        objCls.SelectedServiceTypeSearching='Serach1';
        objCls.SelectedFinancialRisk = 'FRisk';
        objCls.SelectedHealthRisk = 'HRisk';
        objCls.SelectedProgramaticRisk = 'PRisk';
        objCls.SelectedGrantMain = 'Multiple';
        
        objCls.SelectedCountry = '';
        objCls.SelectedRegion = '';
       
        objCls.SelectedCountry = '';
        objCls.SelectedRegion = '';
       
       
        
        ctrlChangeRequest_Approval.wrapResource objwrap3 = new ctrlChangeRequest_Approval.wrapResource();
        objwrap3.objResource = objResource5;
        
        
         
        ctrlChangeRequest_Approval.wrapServices objwrap1 = new ctrlChangeRequest_Approval.wrapServices();
        objwrap1.objService = objService5;
        objwrap1.lstwrapResource = new List<ctrlChangeRequest_Approval.wrapResource>();
        objwrap1.lstwrapResource.add(objwrap3);
       
        ctrlChangeRequest_Approval.wrapServiceType objwrap2 = new ctrlChangeRequest_Approval.wrapServiceType();
        objwrap2.TotalCurrentLOE = 123;
        objwrap2.TotalCurrentCost  = 200;
        
       
        objwrap2.lstwrapServices = new List<ctrlChangeRequest_Approval.wrapServices>();
        objwrap2.lstwrapServices.add(objwrap1);
       
        
        //List<ctrlChangeRequest_Approval.wrapServiceType> lstwrapServicesType = new List<ctrlChangeRequest_Approval.wrapServiceType>();
        objCls.lstwrapServicesType.add(objwrap2);
        
        
        
        objCls.SelectedServiceTypeSearching = 'PUDR';
        objCls.SelectedYear = '2013';
        objCls.SelectedFinancialRisk = 'Low';
        objCls.SelectedHealthRisk = 'High';
        objCls.SelectedProgramaticRisk = 'Medium';
        objCls.SelectedLFA = objAcc.Id;
        objCls.FillRole();
        objCls.FillYear();
        objCls.FillLFA();
        objCls.FillGrant();
        objCls.FillAllRisksOptions();
        //objCls.FillRegion();
        //objCls.FillCountry();
        Id id1 = ctrlChangeRequest_Approval.getWorkItemId(objWp1.id);
        objCls.RetriveResourceValues();
        
        objCls.rerenderaction();
        String str= objCls.getFieldsName('LFA_Resource__c');
        objCls.getParameter();
        
        objCls.CalculateTotal();
    }
}