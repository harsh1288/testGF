/*
* $Author:      $ TCS - Rahul Kumar
* $Description: $ Class for controlling new Document Upload create event redirection
* $Date:        $ 13-Nov-2014 
* $Revision:    $
*/

public class saveAndNewDocuploadController{

    private DocumentUpload_GM__c du;
    private Map<String, String> keyPrefixMap;
    public String backUrl{get;set;}
    public saveAndNewdocuploadController(ApexPages.StandardController stdController){
        this.du = (DocumentUpload_GM__c)stdController.getRecord();
        backUrl = '';         
    }
    
     public PageReference createNewDocurl()
     {
            boolean statusPF = saveandNewHandler.pfTrue('DocumentUpload_GM__c');
            keyPrefixMap =  saveandNewHandler.schemaMap();
            String baseUrl;
            string userTypeStr = UserInfo.getUserType();
            baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
            if(userTypeStr == 'Standard') 
            {
                baseUrl = baseUrl+'/';
            }
            else
            { 
                baseUrl = baseUrl+'/GM/';
            }
           if(du.Implementation_Period__c !=null)
           {
                // Creating url according to user type when Save and New is clicked while editing a record              
                List<Implementation_Period__c> s = new List<Implementation_Period__c>([Select Name from Implementation_Period__c where Id  =:du.Implementation_Period__c]);
                baseUrl += String.valueOf(keyPrefixMap.get('DocumentUpload_GM__c'));
                baseUrl += '/e?nooverride=1';
                if(s.size()>0)
                {
                    baseUrl += '&';
                    baseUrl += Label.Doc_Upload_IP_Lkup;
                    baseUrl += '=';
                    baseUrl += s[0].Name;            
                    baseUrl += '&retURL=';
                    if(userTypeStr != 'Standard') 
                        {   
                            baseUrl +='GM/';                         
                        }
                    baseUrl += du.Implementation_Period__c;
                }
                if(userTypeStr == 'Standard') 
                { 
                    baseUrl += '&RecordType=0121700000009sa&ent=01Ib0000000fyn7';                  
                }
                else
                {
                    baseUrl += '&RecordType=0121700000009sb&ent=01Ib0000000fyn7';
                }                
                            
                PageReference saveNewUrl = new PageReference(baseUrl);
                saveNewUrl.setRedirect(true);                
                return saveNewUrl;            
           }
           else
           {
               // Creating url according to user type when Save and New is clicked while creating new record
               if(userTypeStr == 'Standard') 
                {                    
                }
                else
                { 
                    if(ApexPages.currentPage().getParameters().get('retUrl').indexOf('GM') > -1)
                    {
                    
                    }
                    else
                    {
                        backUrl += +'/GM';
                    }
                }
               backUrl += ApexPages.currentPage().getParameters().get('retUrl');
               if(backUrl.contains('?'))
               {
                   backUrl += '&nooverride=1';
               }
               else
               {
                   backUrl += '?nooverride=1';
               }
               ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Record has been saved <br/> Can\'t use Save and New feature, Please <a id="goback" href="'+backUrl+'"><b>Go back</b></a>'));                           
               return null;
           }         
     }
}