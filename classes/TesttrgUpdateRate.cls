@isTest
Public Class TesttrgUpdateRate{
    Public static testMethod void TesttrgUpdateRate(){
        Country__c objCountry = new Country__c();
        objCountry.Name = 'Test Country';
        insert objCountry;
        
        LFA_Role__c objRole = new LFA_Role__c();
        objRole.Name = 'Test Role';
        insert objRole;
        
        Rate__c objRate = new Rate__c();
        objRate.LFA_Role__c = objRole.Id;
        objRate.Country__c = objCountry.Id;
        objRate.Active__c = true;
        objRate.Next_Year_GF_Approved__c = 'In-Progress';
        objRate.GF_Approved__c = 'In-Progress';
        insert objRate;
        
        Change_Request__c objCR = new Change_Request__c();
        objCR.Status__c = 'Rejected';
        objCR.Rate__c = objRate.Id;
        objCR.Rationale__c = 'test';
        objCR.Proposed_Daily_Rate__c = 200;
        Insert objCR;
        
        Change_Request_Review__c objCRR = new Change_Request_Review__c();
        objCRR.Change_Request__c = objCR.id;
        Insert objCRR;
        
        objCR.Status__c = 'Conditionally Approved';
        Update objCR;
        
        objCR.Status__c = 'Rejected';
        Update objCR;
    }
    Public static testMethod void TesttrgUpdateRate1(){
        Country__c objCountry = new Country__c();
        objCountry.Name = 'Test Country';
        insert objCountry;
        
        LFA_Role__c objRole = new LFA_Role__c();
        objRole.Name = 'Test Role';
        insert objRole;
        
        Rate__c objRate = new Rate__c();
        objRate.LFA_Role__c = objRole.Id;
        objRate.Country__c = objCountry.Id;
        objRate.Active__c = true;
        objRate.Next_Year_GF_Approved__c = 'In-Progress';
        insert objRate;
        
        Change_Request__c objCR = new Change_Request__c();
        objCR.Status__c = 'Rejected';
        objCR.Rate__c = objRate.Id;
        objCR.Rationale__c = 'test';
        objCR.Proposed_Next_Year_Daily_Rate__c = 200;
        Insert objCR;
        
        Change_Request_Review__c objCRR = new Change_Request_Review__c();
        objCRR.Change_Request__c = objCR.id;
        Insert objCRR;
        
        objCR.Status__c = 'Conditionally Approved';
        Update objCR;
        
        objCR.Status__c = 'Rejected';
        Update objCR;
    }
}