@isTest
Public Class Test_ctrlFPMSurveys
{

    static testMethod void Test_Controller()
    {
    
        LFA_Work_Plan__c lfwp = new LFA_Work_Plan__c();
        lfwp.Name = 'Test';
        insert lfwp;
        
        LFA_Service__c lfsv = new LFA_Service__c();
        lfsv.Name='Test1';
        lfsv.LFA_Work_Plan__c = lfwp.Id;
        lfsv.Service_Type__c = 'Key Services';
        insert lfsv;
        
        LFA_Resource__c lfr = new LFA_Resource__c();
        lfr.LFA_Service__c = lfsv.id;
        insert lfr;
        
    
        Performance_Evaluation__c perf = new Performance_Evaluation__c();
        perf.Q12_Rating__c='1';
        perf.Rating__c=0.7;
        perf.initial_adj_rating__c = 0.6;
        perf.Final_Adjusted_Rating__c = 0.6;
        perf.LFA_Work_Plan__c = lfwp.Id;
        perf.Comments__c = '';
        perf.Q1_Rating__c='';
        perf.Q2_Rating__c='';
        perf.Q3_Rating__c='';
        perf.Q4_Rating__c='';
        perf.Q5_Rating__c='';
        perf.Q6_Rating__c='';
        perf.Q7_Rating__c='';
        perf.Q8_Rating__c='';
        insert perf;
        
        
        Service_PET_Jxn__c petjxn = new Service_PET_Jxn__c();
        petjxn.LFA_Service__c = lfsv.id;
        petjxn.Performance_Evaluation__c = perf.id;
        insert petjxn;
        
        PET_Response__c petres1 = new PET_Response__c();
        petres1.name='test';
        petres1.Performance_Evaluation__c = perf.Id;
        petres1.Status__c='Completed';
        petres1.Rating__c = '3';
        insert petres1; 
            
       
        ApexPages.currentPage().getParameters().put('id',perf.id);

                            
        ApexPages.StandardController ctrl = new ApexPages.StandardController(perf);
        ctrlFPMSurveys extController = new ctrlFPMSurveys(ctrl);
        extController.intIndex = 0;
        extController.intIndexSt = 0; 
        
        extController.beingEdited();
        extController.viewService();
        extController.HideService();
        extController.getRating();
        extController.Finalize();
        extController.SendToLFA();
        extController.Save();
        extController.Cancel();
        extController.FinishPET();
        extController.SendToFPM();
        extController.AnalyseMainGrid();
        extController.ExapndAllServiceType();
        extController.CollepseAllServiceType();
        extController.CalculateTotal();
        extController.FillServiceType();
        extController.RetriveResourceValues();
        extController.getFieldsName('Performance_Evaluation__c');
        pageReference pr1 = extController.OnlySave();
        pageReference pr2 = extController.quicksave();
        perf.Q11_Rating__c='1';
        update perf;
        
        extController.LoadPerform();     
        extController.validate();
            
             
        perf.Q10_Rating__c='1';
        update perf;
        
        extController.LoadPerform();     
        extController.validate();
           
        
         
        perf.Q9_Rating__c='1';
        update perf;
        
        extController.LoadPerform();     
        extController.validate();
        extController.FinishPET();
        
         perf.Q8_Rating__c='1';
        update perf;
        
        extController.LoadPerform();     
        extController.validate();
          
             
        perf.Q7_Rating__c='1';
        update perf;
        
        extController.LoadPerform();     
        extController.validate();
       
        
         
        perf.Q8_Rating__c='1';
        update perf;
        
        extController.LoadPerform();     
        extController.validate();
             
        perf.Q7_Rating__c='1';
        update perf;
        
        extController.LoadPerform();     
        extController.validate();
          
             
        perf.Q6_Rating__c='1';
        update perf;
        
        extController.LoadPerform();     
        extController.validate();
            
         
        perf.Q5_Rating__c='1';
        update perf;
        
        extController.LoadPerform();     
        extController.validate();
             
        perf.Q4_Rating__c='1';
        update perf;
        
        extController.LoadPerform();     
        extController.validate();
         
             
        perf.Q3_Rating__c='1';
        update perf;
        
        extController.LoadPerform();     
        extController.validate();
            
         
        perf.Q2_Rating__c='1';
        update perf;
        
        extController.LoadPerform();     
        extController.validate();
             
         perf.Q1_Rating__c='1';
         perf.Display_final_rating_box__c = true;
        update perf;
        
        extController.LoadPerform();     
        extController.validate();
     
            
         perf.Q1_Rating__c='4';
         perf.Q2_Rating__c='4';
         perf.Q3_Rating__c='4';
         perf.Q4_Rating__c='4';
         perf.Q5_Rating__c='4';
         perf.Q6_Rating__c='4';
         perf.Q7_Rating__c='4';
         perf.Q8_Rating__c='4';
         perf.Q9_Rating__c='4';
         perf.Q10_Rating__c='4';
         perf.Q11_Rating__c='4';
         perf.Q12_Rating__c='4';
        
        perf.Display_final_rating_box__c = true;
        update perf;  
       
        extController.SendToLFA();
        extController.FinishPET();
        
    }



}