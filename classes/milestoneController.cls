public class milestoneController{

    /*public string activityId{get;set;}    
    public Key_Activity__c activity{get;set;}
    public List<RPWrapper> wrapperList{get;set;}
    public Milestone_target__c milestoneObj{get;set;}
    public boolean isSelected{get;set;}
    public boolean isEditMode{get;set;}
    public string milestoneId{get;set;}
    List<Milestone_RP_Junction__c> dbjunctionList = new List<Milestone_RP_Junction__c>();*/
    
    
    public milestoneController(ApexPages.StandardController controller) {
      
        /*milestoneId = ApexPages.currentpage().getparameters().get('id');
        string mode = ApexPages.currentpage().getparameters().get('edit');
        System.debug('-milestoneId --'+mode +milestoneId );
        isEditMode  = false;
        
        if(  milestoneId != NULL && mode!= NULL ){
            isEditMode = true;
            mileStoneObj = [Select id, Key_Activity__c,milestoneTitle__c, Name, Criteria__c from Milestone_Target__c where id=:milestoneId];
        }else{
            milestoneObj = (milestone_Target__c)controller.getRecord();
        
        }
        activityId = milestoneObj.Key_Activity__c;
        activity = [Select id, Activity_Title__c, Grant_Intervention__r.implementation_Period__c from Key_Activity__c where id=: milestoneObj.key_activity__c ];
        wrapperList = new List<RPWrapper>();
        isSelected = false;
        if( isEditMode ){
            populateWrapperJunctionList();
        }else{
            populateWrapperList();
        }*/
      
    }

 
    /*public void populateWrapperJunctionList(){
        List<Period__c> periodList = new List<Period__c>();
        List<Milestone_RP_Junction__c> junctionList = new List<Milestone_RP_Junction__c>();
        periodList = [SELECT id, Name, Implementation_Period__c,Start_Date__c, end_date__c from Period__c 
                        WHERE Implementation_Period__c =:activity.Grant_Intervention__r.implementation_Period__c 
                        AND IS_Active__c=true
                        AND Performance_Framework__c != NULL];
       
       dbjunctionList = [Select id, Milestone_Target__c ,Reporting_Period__r.Name, Reporting_Period__c , Reporting_Period__r.start_Date__c, 
                        Reporting_Period__r.End_Date__c from Milestone_RP_Junction__c where Milestone_Target__c =:milestoneId];  
       
       for( period__c periodObj : periodList ){
           Integer flag = 0;
           for( Milestone_RP_Junction__c obj : dbjunctionList ){
               if( periodObj.id == obj.Reporting_Period__c ){
                   flag = 1;
                   RPWrapper RPWrapperObj = new RPWrapper(true, periodObj);
                   wrapperList.add( RPWrapperObj );  
                   break;
               }  
           }
           if( flag == 0){
               RPWrapper RPWrapperObj = new RPWrapper(false, periodObj);
               wrapperList.add( RPWrapperObj );  
           }

        }
    }
    
    public void populateWrapperList(){
        List<Period__c> periodList = new List<Period__c>();
        periodList = [SELECT id, Name, Implementation_Period__c,Start_Date__c, end_date__c from Period__c 
                        WHERE Implementation_Period__c =:activity.Grant_Intervention__r.implementation_Period__c 
                        AND IS_Active__c=true
                        AND Performance_Framework__c != NULL];
       
        for( period__c periodObj : periodList ){
           RPWrapper RPWrapperObj = new RPWrapper(false, periodObj);
           wrapperList.add( RPWrapperObj );  
        }
       
    }
    public pageReference save(){
        
        List<Milestone_RP_Junction__c> junctionList = new List<Milestone_RP_Junction__c>();
         System.debug('milestoneObj--'+milestoneObj);
         for( RPWrapper RPWrapperObj : wrapperList){
            if( RPWrapperObj.status ){
                isSelected = true;
                break;
            } 
        }
        if( !isSelected ){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Please associate the milestone with at least one reporting period'));
            return null;    
        }
        upsert milestoneObj;
        if( isEditMode ){
    
            return saveInEditMode();
        
         }else{

            for( RPWrapper RPWrapperObj : wrapperList){
                if( RPWrapperObj.status ){
                    Milestone_RP_Junction__c obj = new Milestone_RP_Junction__c();
                    obj.Milestone_Target__c = milestoneObj.id;
                    obj.Reporting_Period__c = RPWrapperObj.periodObj.id;
                    junctionList.add(obj);
                }
            }
            
            
            if( !junctionList.isEmpty() ){
                insert junctionList;
            }
            return new PageReference('/apex/milestoneDetailPage?id='+milestoneObj.id);
        }
    }
    
    public pageReference saveInEditMode(){
        
        List<Milestone_RP_Junction__c > deleteList = new List<Milestone_RP_Junction__c >();
        List<Milestone_RP_Junction__c > insertList = new List<Milestone_RP_Junction__c >();
        System.debug('---dbjunctionList =='+dbjunctionList );
        System.debug('---wrapperList=='+wrapperList);
        for( RPWrapper RPWrapperObj : wrapperList){
            Integer isOld = 0;
            for( Milestone_RP_Junction__c obj : dbjunctionList ){    
                if( RPWrapperObj.status ){
                    if( obj.Reporting_Period__c == RPWrapperObj.periodObj.id ){
                        isOld = 1;    
                        break;    
                    }    
                }else{
                     if( obj.Reporting_Period__c == RPWrapperObj.periodObj.id ){
                          deleteList.add( obj );  
                     }    
                }
               
            }
            if( isOld == 0 && RPWrapperObj.status ){
                insertList.add(new  Milestone_RP_Junction__c( Milestone_Target__c = milestoneId,  Reporting_Period__c = RPWrapperObj.periodObj.id )); 
            } 
        }
        if( !deleteList.isEmpty()){
            delete deleteList;
        }
        if( !insertList.isEmpty() ){
            insert insertList;
        }
        return new PageReference('/apex/milestoneDetailPage?id='+milestoneObj.id);
    }
    
    public PageReference cancel(){
        System.debug('---activityId--'+activityId);
        return new Pagereference('/'+activityId);
    }
    
    class RPWrapper{
        public boolean status{get;set;}
        public Period__c periodObj{get;set;}
        public RPWrapper(Boolean statusVal, Period__c periodObjVal){
            status = statusVal;
            periodObj = periodObjVal;            
        }
    }  */  
    
    
}