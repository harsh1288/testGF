public class GoalSelectionCtrl {
  Public List<Goals_Objectives__c> lstGoals{get;set;}
  Public List<Goals_Objectives__c> lstobjectives;
  Public List<Grant_Indicator__c> lstind{get;set;}
  Public id IndicatorId{get;set;}
  public List<wrapgoals> lstwgoalobj{get;set;}
  public boolean display{get;set;}
  public boolean display1{get;set;}
  Public List<Goals_Objectives__c> selectedgoals = new List<Goals_Objectives__c>();
  public List<Ind_Goal_Jxn__c> indGoalList = new List<Ind_Goal_Jxn__c>();
  public string message{get;set;}
  public boolean display2{get;set;}
  public boolean display3{get;set;}
  public string ConNoteId{get;set;}
  public List<Ind_Goal_Jxn__c> lstindgoaljn = new List<Ind_Goal_Jxn__c>();
  Public List<id> lstid = new List<Id>();
  public string linkgoalobject{get;set;}
  
    public GoalSelectionCtrl(ApexPages.StandardController controller) {
        IndicatorId=Apexpages.currentPage().getParameters().get('id');
        
        lstind = [select id,Name,Concept_Note__c,Grant_Implementation_Period__c,Indicator_Type__c,Link_to_Goals__c,performance_framework__c from Grant_Indicator__c where id =: IndicatorId ];
                ConNoteId = lstind[0].Concept_Note__c;
                if(ConNoteId == null){
                  ConNoteId = lstind[0].Grant_Implementation_Period__c;
                }
                
         lstindgoaljn = [select id,Goal_Objective__c,Indicator__c from Ind_Goal_Jxn__c where Indicator__c =:IndicatorId ];  
              for(Ind_Goal_Jxn__c objindgoal :lstindgoaljn ){
                     lstid.add(objindgoal.Goal_Objective__c);
              } 
          lstwgoalobj = new List<wrapgoals>();
         system.debug('Implementation period Id '+ConNoteId+' lstid '+lstid + 'lstind[0].Indicator_Type__c '+lstind[0].Indicator_Type__c );
        if(lstind[0].Indicator_Type__c == 'Impact'){
           
             lstGoals = [select id,Name,Implementation_Period__c,Goal__c,Type__c  from Goals_Objectives__c where Type__c = 'Goal' and (Concept_Note__c =:ConNoteId or Implementation_Period__c =:ConNoteId) and Id not in:lstid ];
               
             if(lstGoals.size()>0){
                display2 = false;
                display3 = true;
                for(Goals_Objectives__c objgoal:lstgoals){
                    lstwgoalobj.add(new wrapgoals(objgoal));
                }
              }
            else {
                 display2= true;
                 display3 = false;
                 message = 'No Goals available to link Indicator Or All  Goals presented are linked to Indicator';
            }
        }
        else if(lstind[0].Indicator_Type__c == 'Outcome'){
             //display = false;
            // display1 = true;
             lstgoals = [select id,Name,Goal__c,Type__c  from Goals_Objectives__c where Type__c = 'Objective' and (Concept_Note__c =:ConNoteId or Implementation_Period__c =:ConNoteId) and Id not in:lstid];
            
             if(lstgoals.size()>0){
               display2 = false;
               display1 = true;
                  for(Goals_Objectives__c objgoal:lstgoals){
                       lstwgoalobj.add(new wrapgoals(objgoal));
                  }
             }
             else{
              display2= true;
              display1 = false;
               message = 'No Objectives availble to link Indicator Or All Objectives presented are linked to Indicator';
             }
         }
         }
        
      
    
    
    public pageReference save(){
         for(wrapgoals objgoal:lstwgoalobj){
            if(objgoal.selected == true){
                 selectedgoals.add(objgoal.wgoalobj);
            }
         }
         if(selectedgoals.size()==0){
             ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please select atleast one Goal/Objective');
             ApexPages.addMessage(myMsg); 
             return null;
         }
       else{
         for(Goals_Objectives__c objgoal:selectedgoals){
              Ind_Goal_Jxn__c IndGoal = new Ind_Goal_Jxn__c();
              IndGoal.Goal_Objective__c = objgoal.id;
              IndGoal.Indicator__c = Indicatorid;
              indGoalList.add(IndGoal);
         }
         insert indGoalList;
         for(Grant_Indicator__c objind:lstind){
               objind.Link_to_Goals__c = true;
         }
         update lstind;
         pageReference pr = new pageReference('/'+lstind[0].performance_framework__c );
         pr.setRedirect(true);
         return pr;
       }
    }
    public pageReference cancel(){
        pageReference pr = new pageReference('/'+lstind[0].performance_framework__c );
        pr.setRedirect(true);
        return pr;   
    }
    
    public class wrapgoals{
       public Goals_Objectives__c wgoalobj{get;set;}
       Public Boolean selected{get;set;}
        public wrapgoals(Goals_Objectives__c wgo){
           wgoalobj = wgo;
           selected = false;
        }
    }

}