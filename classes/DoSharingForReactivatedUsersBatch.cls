global class DoSharingForReactivatedUsersBatch implements Schedulable, Database.Stateful, Database.Batchable<sObject> {
    global String query; 
    private final String BATCH_JOB_Name = 'DoSharingForReactivatedUsersBatch'; 
    global Date sinceDate; 
    global String errorMessage = '';
    //Past i days
    public DoSharingForReactivatedUsersBatch(Integer i){
        sinceDate = Date.today()-i;
        system.debug('sinceDate '+sinceDate);
    }
    //Past 1 day
    public DoSharingForReactivatedUsersBatch(){     
        SYSTEM.debug(' In default constructor');
        sinceDate = Date.today()-1;
        system.debug('sinceDate '+sinceDate);
            }
    global Database.QueryLocator start(Database.BatchableContext BC){      
        
        query = 'Select Id, Do_dummy_update__c from npe5__Affiliation__c where User_reactivated_on__c!=null AND User_reactivated_on__c >=:sinceDate';
        system.debug(query);
        system.debug(' since date '+sinceDate);
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<npe5__Affiliation__c> scope) {        
        List<npe5__Affiliation__c> doDummyUpdateOn = new List<npe5__Affiliation__c>();
        for(npe5__Affiliation__c affWhichReactivatedUser :(List<npe5__Affiliation__c>)scope )
        {
            SYSTEM.debug('affWhichReactivatedUser ID'+affWhichReactivatedUser.ID);
            affWhichReactivatedUser.Do_dummy_update__c = !(affWhichReactivatedUser.Do_dummy_update__c);
            doDummyUpdateOn.add(affWhichReactivatedUser);
        }
        try{
        if(doDummyUpdateOn.size()>0)
            update doDummyUpdateOn;
            }
      catch(DmlException dEx){
                 for (Integer i= 0; i < dEx.getNumDml(); i++){
                    errorMessage += dEx.getDmlMessage(i);
                }
            }
        //errorMessages = InsertMissingSalesTeamServices.insertMissingSalesTeamRecords(scope);
     } 
    global void finish(Database.BatchableContext BC){
        system.debug(' errorMessage '+errorMessage);
        /*AsyncApexJob job = [ SELECT Id, Status, NumberOfErrors, JobItemsProcessed, 
                                TotalJobItems, CreatedBy.Email, LastProcessedOffset
                                FROM AsyncApexJob 
                                WHERE Id =:bc.getJobId()];
            //get accumulative statistics for the last subJob
            List<AsyncApexJob> subJobs = [ SELECT Id, Status, NumberOfErrors, JobItemsProcessed, 
                                          TotalJobItems, CreatedBy.Email, LastProcessedOffset
                                          FROM AsyncApexJob 
                                          WHERE ParentJobId =:bc.getJobId() 
                                          ORDER BY CompletedDate desc, LastProcessedOffset desc LIMIT 1];
            
            String emailBody = BATCH_JOB_Name + ' processed ' + job.TotalJobItems 
                + ' batches with '+ job.NumberOfErrors + ' failures.';
            
            Boolean sendEmailFlag;
            String emailSubject = '';            
            String[] toAddress = new List<String>();
            toAddress.add('nikunj.doshi@theglobalfund.org');
            if(errorMessage != null && errorMessage != ''){
                emailSubject = 'Failure:';
                emailBody += '\n\n Following Errors were Occurred: \n\n' + errorMessage;
                sendEmailFlag = true;    
            }
            else{
                emailSubject = 'Success:';
                sendEmailFlag = true; 
            }
            
            if(sendEmailFlag){
                emailSubject += BATCH_JOB_Name + ' ' + System.Today().year() + '/' + System.Today().month() + '/' + System.Today().day();
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setToAddresses(toAddress);         
                mail.setSubject(emailSubject);     
                mail.setPlainTextBody(emailBody);
                Messaging.sendEmail( new Messaging.SingleEmailMessage[] { mail } );
            }*/
        }                
    
    global void execute( SchedulableContext sc ) {        
        DoSharingForReactivatedUsersBatch job = new DoSharingForReactivatedUsersBatch(); 
        Database.executeBatch(job);
    } 
}