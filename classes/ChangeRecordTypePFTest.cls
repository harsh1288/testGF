@isTest(seealldata=false)
public class ChangeRecordTypePFTest{
          
public static testMethod void testChangeRT1(){
    
        Account objAcc = TestClassHelper.insertAccount();
        
        Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
        insert objGrant;
       
        Implementation_Period__c objIP = TestClassHelper.createIP(objGrant,objAcc);
        objIP.Reporting__c = 'Yearly';
        insert objIP;
        
        Performance_Framework__c objPF = TestClassHelper.createPF(objIP);
        objPF.PF_Status__c = Label.IP_Not_Yet_Submitted;
        insert objPF; 
        
        List<Performance_Framework__c >lstPF = new List<Performance_Framework__c>();
        lstPF.add(objPF);
        
        
        system.debug('**objPF'+objPF); 
        
        Set<Id> PFRecIDRej =  new Set<Id>();
        PFRecIDRej.add(objPF.Id);
        
        Test.StartTest();
        Module__c objModule = TestClassHelper.createModule();
        objModule.Performance_Framework__c = lstPF[0].Id;
        insert objModule;
        system.debug('>>>>module'+objModule);
        Period__c objPer = TestClassHelper.createPeriod();
        objPer.Performance_Framework__c = lstPF[0].Id;
        insert objPer;
        
        Grant_Intervention__c objGI = TestClassHelper.createGrantIntervention(objIP);
        objGI.Performance_Framework__c = lstPF[0].Id;
        insert objGI;
        
        Goals_Objectives__c objGO = TestClassHelper.createGoalsObjectives();
        objGO.Performance_Framework__c = lstPF[0].Id;
        objGO.Type__c = 'Goal';
        insert objGO;
        
        Goals_Objectives__c objGObj = TestClassHelper.createGoalsObjectives();
        objGObj.Performance_Framework__c = lstPF[0].Id;
        objGObj.Type__c = 'Objective';
        insert objGObj;
        
        Indicator__c objCI =  new Indicator__c();
        insert objCI;
        
        Catalog_Disaggregated__c objCD = new Catalog_Disaggregated__c ();
        objCD.Catalog_Indicator__c = objCI.Id;
        insert objCD;
        
        
        Grant_Indicator__c objGIDImp = TestClassHelper.createGrantIndicator();
        objGIDImp.Performance_Framework__c = lstPF[0].Id;
        objGIDImp.Indicator_Type__c = 'Impact';
        insert objGIDImp;
        
        Grant_Indicator__c objGIDOut = TestClassHelper.createGrantIndicator();
        objGIDOut.Performance_Framework__c = lstPF[0].Id;
        objGIDOut.Indicator_Type__c = 'Outcome';
        insert objGIDOut;
        
        Grant_Indicator__c objGIDCov = TestClassHelper.createGrantIndicator();
        objGIDCov.Performance_Framework__c = lstPF[0].Id;
        objGIDCov.Indicator_Type__c = 'Coverage/Output';
        insert objGIDCov;
        
        Implementer__c objIMP = TestClassHelper.createImplementor();
        objIMP.Performance_Framework__c = lstPF[0].Id;
        insert objIMP;
        
        Grant_Disaggregated__c objGD = new Grant_Disaggregated__c();
        objGD.Grant_Indicator__c = objGIDImp.Id;
        objGD.Catalog_Disaggregated__c = objCD.Id;
        insert objGD;
        Test.StopTest();
        
        objPF.PF_Status__c = Label.IP_Return_back_to_PR;
        update objPF;
    }
    
    public static testMethod void testChangeRT2(){
    
        Account objAcc = TestClassHelper.insertAccount();
        
        Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
        insert objGrant;
       
        Implementation_Period__c objIP = TestClassHelper.createIP(objGrant,objAcc);
        objIP.Reporting__c = 'Yearly';
        insert objIP;
        
        Performance_Framework__c objPF = TestClassHelper.createPF(objIP);
        objPF.PF_Status__c = Label.IP_Return_back_to_PR;
        insert objPF; 
        
        List<Performance_Framework__c >lstPF = new List<Performance_Framework__c>();
        lstPF.add(objPF);
        
        
        system.debug('**objPF'+objPF); 
        
        Set<Id> PFRecIDNT =  new Set<Id>();
        PFRecIDNT.add(objPF.Id);
        
        Test.startTest();
        Module__c objModule = TestClassHelper.createModule();
        objModule.Performance_Framework__c = objPF.Id;
        insert objModule;
        
        Period__c objPer = TestClassHelper.createPeriod();
        objPer.Performance_Framework__c = objPF.Id;
        insert objPer;
        
        Grant_Intervention__c objGI = TestClassHelper.createGrantIntervention(objIP);
        objGI.Performance_Framework__c = objPF.Id;
        insert objGI;
        
        Goals_Objectives__c objGO = TestClassHelper.createGoalsObjectives();
        objGO.Performance_Framework__c = objPF.Id;
        objGO.Type__c = 'Goal';
        insert objGO;
        
        Goals_Objectives__c objGObj = TestClassHelper.createGoalsObjectives();
        objGObj.Performance_Framework__c = lstPF[0].Id;
        objGObj.Type__c = 'Objective';
        insert objGObj;
        
        
        Indicator__c objCI =  new Indicator__c();
        insert objCI;
        
        Catalog_Disaggregated__c objCD = new Catalog_Disaggregated__c ();
        objCD.Catalog_Indicator__c = objCI.Id;
        insert objCD;
        
        
        Grant_Indicator__c objGIDImp = TestClassHelper.createGrantIndicator();
        objGIDImp.Performance_Framework__c = lstPF[0].Id;
        objGIDImp.Indicator_Type__c = 'Impact';
        insert objGIDImp;
        
        Grant_Indicator__c objGIDOut = TestClassHelper.createGrantIndicator();
        objGIDOut.Performance_Framework__c = lstPF[0].Id;
        objGIDOut.Indicator_Type__c = 'Outcome';
        insert objGIDOut;
        
        Grant_Indicator__c objGIDCov = TestClassHelper.createGrantIndicator();
        objGIDCov.Performance_Framework__c = lstPF[0].Id;
        objGIDCov.Indicator_Type__c = 'Coverage/Output';
        insert objGIDCov;
        
        Implementer__c objIMP = TestClassHelper.createImplementor();
        objIMP.Performance_Framework__c = objPF.Id;
        insert objIMP;
        
        Grant_Disaggregated__c objGD = new Grant_Disaggregated__c();
        objGD.Grant_Indicator__c = objGIDImp.Id;
        objGD.Catalog_Disaggregated__c = objCD.Id;
        insert objGD;
        Test.StopTest();
        
        objPF.PF_Status__c = Label.IP_Not_Yet_Submitted;
        update objPF;
    }
    
      public static testMethod void testChangeRT3(){
    
        Account objAcc = TestClassHelper.insertAccount();
        
        Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
        insert objGrant;
       
        Implementation_Period__c objIP = TestClassHelper.createIP(objGrant,objAcc);
        objIP.Reporting__c = 'Yearly';
        insert objIP;
        
        Performance_Framework__c objPF = TestClassHelper.createPF(objIP);
        objPF.PF_Status__c = Label.IP_Return_back_to_PR;
        insert objPF; 
        
        List<Performance_Framework__c >lstPF = new List<Performance_Framework__c>();
        lstPF.add(objPF);
        
        
        system.debug('**objPF'+objPF); 
        
        Set<Id> PFRecIDSubToME =  new Set<Id>();
        PFRecIDSubToME.add(objPF.Id);
        Test.StartTest();
        Module__c objModule = TestClassHelper.createModule();
        objModule.Performance_Framework__c = objPF.Id;
        insert objModule;
        
        Period__c objPer = TestClassHelper.createPeriod();
        objPer.Performance_Framework__c = objPF.Id;
        insert objPer;
        
        Grant_Intervention__c objGI = TestClassHelper.createGrantIntervention(objIP);
        objGI.Performance_Framework__c = objPF.Id;
        insert objGI;
        
        Goals_Objectives__c objGO = TestClassHelper.createGoalsObjectives();
        objGO.Performance_Framework__c = objPF.Id;
        objGO.Type__c = 'Goal';
        insert objGO;
        
        Goals_Objectives__c objGObj = TestClassHelper.createGoalsObjectives();
        objGObj.Performance_Framework__c = lstPF[0].Id;
        objGObj.Type__c = 'Objective';
        insert objGObj;
        
        Indicator__c objCI =  new Indicator__c();
        insert objCI;
        
        Catalog_Disaggregated__c objCD = new Catalog_Disaggregated__c ();
        objCD.Catalog_Indicator__c = objCI.Id;
        insert objCD;
        
        
        Grant_Indicator__c objGIDImp = TestClassHelper.createGrantIndicator();
        objGIDImp.Performance_Framework__c = lstPF[0].Id;
        objGIDImp.Indicator_Type__c = 'Impact';
        insert objGIDImp;
        
        Grant_Indicator__c objGIDOut = TestClassHelper.createGrantIndicator();
        objGIDOut.Performance_Framework__c = lstPF[0].Id;
        objGIDOut.Indicator_Type__c = 'Outcome';
        insert objGIDOut;
        
        Grant_Indicator__c objGIDCov = TestClassHelper.createGrantIndicator();
        objGIDCov.Performance_Framework__c = lstPF[0].Id;
        objGIDCov.Indicator_Type__c = 'Coverage/Output';
        insert objGIDCov;
        
        Implementer__c objIMP = TestClassHelper.createImplementor();
        objIMP.Performance_Framework__c = objPF.Id;
        insert objIMP;
        
        Grant_Disaggregated__c objGD = new Grant_Disaggregated__c();
        objGD.Grant_Indicator__c = objGIDImp.Id;
        objGD.Catalog_Disaggregated__c = objCD.Id;
        insert objGD;
        Test.StopTest();
        objPF.PF_Status__c = Label.IP_Sub_to_GF;
        update objPF;
    }
    
       public static testMethod void testChangeRT4(){
    
        Account objAcc = TestClassHelper.insertAccount();
        
        Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
        insert objGrant;
       
        Implementation_Period__c objIP = TestClassHelper.createIP(objGrant,objAcc);
        objIP.Reporting__c = 'Yearly';
        insert objIP;
        
        Performance_Framework__c objPF = TestClassHelper.createPF(objIP);
        objPF.PF_Status__c = Label.IP_Return_back_to_PR;
        insert objPF; 
        
        List<Performance_Framework__c >lstPF = new List<Performance_Framework__c>();
        lstPF.add(objPF);
        
        
        system.debug('**objPF'+objPF); 
        
        Set<Id> PFRecIDs =  new Set<Id>();
        PFRecIDs.add(objPF.Id);
        Test.StartTest();
        Module__c objModule = TestClassHelper.createModule();
        objModule.Performance_Framework__c = objPF.Id;
        insert objModule;
        
        Period__c objPer = TestClassHelper.createPeriod();
        objPer.Performance_Framework__c = objPF.Id;
        insert objPer;
        
        Grant_Intervention__c objGI = TestClassHelper.createGrantIntervention(objIP);
        objGI.Performance_Framework__c = objPF.Id;
        insert objGI;
        
        Goals_Objectives__c objGO = TestClassHelper.createGoalsObjectives();
        objGO.Performance_Framework__c = objPF.Id;
        objGO.Type__c = 'Goal';
        insert objGO;
        
        Goals_Objectives__c objGObj = TestClassHelper.createGoalsObjectives();
        objGObj.Performance_Framework__c = lstPF[0].Id;
        objGObj.Type__c = 'Objective';
        insert objGObj;
        
        Indicator__c objCI =  new Indicator__c();
        insert objCI;
        
        Catalog_Disaggregated__c objCD = new Catalog_Disaggregated__c ();
        objCD.Catalog_Indicator__c = objCI.Id;
        insert objCD;
        
        
        Grant_Indicator__c objGIDImp = TestClassHelper.createGrantIndicator();
        objGIDImp.Performance_Framework__c = lstPF[0].Id;
        objGIDImp.Indicator_Type__c = 'Impact';
        insert objGIDImp;
        
        Grant_Indicator__c objGIDOut = TestClassHelper.createGrantIndicator();
        objGIDOut.Performance_Framework__c = lstPF[0].Id;
        objGIDOut.Indicator_Type__c = 'Outcome';
        insert objGIDOut;
        
        Grant_Indicator__c objGIDCov = TestClassHelper.createGrantIndicator();
        objGIDCov.Performance_Framework__c = lstPF[0].Id;
        objGIDCov.Indicator_Type__c = 'Coverage/Output';
        insert objGIDCov;
        
        Implementer__c objIMP = TestClassHelper.createImplementor();
        objIMP.Performance_Framework__c = objPF.Id;
        insert objIMP;
        
            
        Grant_Disaggregated__c objGD = new Grant_Disaggregated__c();
        objGD.Grant_Indicator__c = objGIDImp.Id;
        objGD.Catalog_Disaggregated__c = objCD.Id;
        insert objGD;
        Test.StopTest();
        objPF.PF_Status__c = 'Accepted';
        update objPF;
    }
    
     public static testMethod void TestPFSendMailToPRs() {
       Account portalAccount = new Account(
            Name = 'TestAccount',
           Boolean_Duplicate__c = true
        );
        insert portalAccount ;
      
        
        Contact objContactPR = new Contact(
            FirstName = 'Test',
            Lastname = 'Amit',
            AccountId = portalAccount.Id,
            Email = 'test@test.com',
            Boolean_Duplicate__c = true
        );
        insert objContactPR; 
        
        Contact objContactTest = new Contact(
            FirstName = 'Test',
            Lastname = 'Amita',
            AccountId = portalAccount.Id,
            Email = 'test@test.com',
            Boolean_Duplicate__c = true
        );
        insert objContactTest; 
        
                    
            Country__c objCon = new Country__c();
            objCon.Name = 'Hakunamatata';
            insert objCon; 
        
        List<Profile> objProfiles = [SELECT id FROM profile WHERE name in ('PR Admin', 'PR Read Write Edit') Limit 3]; 
            
            User objEsUser = new User(alias = 'u1', email='eS@testorg.com',
            emailencodingkey='UTF-8', lastname='TestingEs', languagelocalekey='es',
            localesidkey='en_US', profileid = objProfiles[0].Id, country = objCon.Id, CommunityNickname = 'u1', contactid = objContactPR.Id,
            timezonesidkey='America/Los_Angeles', username='eS@testorg.com');
            insert objEsUser;  
            
            User objRuUser = new User(alias = 'u2', email='rU@testorg.com',
            emailencodingkey='UTF-8', lastname='TestingRu', languagelocalekey='ru',
            localesidkey='en_US', profileid = objProfiles[1].Id, country = objCon.Id, CommunityNickname = 'u2', contactId = objContactTest.Id,
            timezonesidkey='America/Los_Angeles', username='rU@testorg.com');
            insert objRuUser; 
            
            test.startTest();
            objCon.FPM__c = objEsUser.Id;  
            objCon.Finance_Officer__c = objRuUser.Id; 
            objCon.HPM_Specialist__c = objEsUser.Id; 
            objCon.MEPH_Specialist__c = objRuUser.Id;
            objCon.PO_1__c = objEsUser.Id;
            objCon.PO_1__c = objRuUser.Id;
            objCon.PO_1__c = objEsUser.Id;
            update objCon;
            
            Grant__c objGrant = TestClassHelper.createGrant(portalAccount);
            objGrant.Grant_Status__c = 'Not yet submitted';
            insert objGrant;
        
            Implementation_Period__c objIP = TestClassHelper.createIP(objGrant,portalAccount);
        //  objIP.Status__c = 'Grant-Making';
            objIP.Status__c = 'Concept Note';
            objIP.CurrencyIsoCode='USD';
            objIP.Grant_Making_submission_date__c=Date.today().addYears(3);
            objIP.High_level_budget_GAC_1_EUR__c = 1000.00;
            objIP.High_level_budget_GAC_1_USD__c = 10002.99;
            objIP.High_level_budget_TRP_USD__c= 10003.77;
            objIP.High_level_budget_TRP_EUR__c= 10003.67;
            insert objIP;
            
            
            IP_Detail_Information__c ipDetail = new IP_Detail_Information__c();
            ipDetail.Implementation_Period__c = objIP.Id;
            insert ipDetail;
            
            Performance_Framework__c objPF = new Performance_Framework__c();
            objPF.Implementation_Period__c = objIP.Id;
            insert objPF; 
            
            HPC_Framework__c hpcFW = new HPC_Framework__c();
            hpcFW.Grant_Implementation_Period__c = objIP.Id;
            hpcFW.HPC_Status__c = 'Not Yet Submitted by PR';
            hpcFW.Total_HPC_Amount_USD__c = 2000;
            insert hpcFW; 
            
            /*EmailTemplate objEmailTempPR = EmailTemplate();
            objEmailTemp.DeveloperName='Detailed_Budget_Request_input_from_PR';
            insert objEmailTempPR;
            
            EmailTemplate objEmailTempCT = EmailTemplate();
            objEmailTemp.DeveloperName='CT agrees with final budget';
            insert objEmailTempCT;
            */
                
            objPF.PF_Status__c =Label.IP_Return_back_to_PR;    
            update objPF; 
            test.stopTest();
            objPF.PF_Status__c = 'Accepted';
            update objPF;
            
            /*Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setTargetObjectId(objContactPR.Id);
            mail.setWhatId(ipDetail.Id);
            // assuming this Template ID exists in your org
            mail.setTemplateId('00X17000000QEIr'); 
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });*/
    }
    
    public static testMethod void TestPFSendMailToPRs1() {
       Account portalAccount = new Account(
            Name = 'TestAccount',
           Boolean_Duplicate__c = true
        );
        insert portalAccount ;
      
        
        Contact objContactPR = new Contact(
            FirstName = 'Test',
            Lastname = 'Amit',
            AccountId = portalAccount.Id,
            Email = 'test@test.com',
            Boolean_Duplicate__c = true
        );
        insert objContactPR; 
        
        Contact objContactTest = new Contact(
            FirstName = 'Test',
            Lastname = 'Amita',
            AccountId = portalAccount.Id,
            Email = 'test@test.com',
            Boolean_Duplicate__c = true
        );
        insert objContactTest; 
        
                    
            Country__c objCon = new Country__c();
            objCon.Name = 'Hakunamatata';
            insert objCon; 
        
        List<Profile> objProfiles = [SELECT id FROM profile WHERE name in ('PR Admin', 'PR Read Write Edit') Limit 3]; 
            
            User objEsUser = new User(alias = 'u1', email='eS@testorg.com',
            emailencodingkey='UTF-8', lastname='TestingEs', languagelocalekey='es',
            localesidkey='en_US', profileid = objProfiles[0].Id, country = objCon.Id, CommunityNickname = 'u1', contactid = objContactPR.Id,
            timezonesidkey='America/Los_Angeles', username='eS@testorg.com');
            insert objEsUser;  
            
            User objRuUser = new User(alias = 'u2', email='rU@testorg.com',
            emailencodingkey='UTF-8', lastname='TestingRu', languagelocalekey='ru',
            localesidkey='en_US', profileid = objProfiles[1].Id, country = objCon.Id, CommunityNickname = 'u2', contactId = objContactTest.Id,
            timezonesidkey='America/Los_Angeles', username='rU@testorg.com');
            insert objRuUser; 
            
            test.startTest();
            objCon.FPM__c = objEsUser.Id;  
            objCon.Finance_Officer__c = objRuUser.Id; 
            objCon.HPM_Specialist__c = objEsUser.Id; 
            objCon.MEPH_Specialist__c = objRuUser.Id;
            objCon.PO_1__c = objEsUser.Id;
            objCon.PO_1__c = objRuUser.Id;
            objCon.PO_1__c = objEsUser.Id;
            update objCon;
            
            Grant__c objGrant = TestClassHelper.createGrant(portalAccount);
            objGrant.Grant_Status__c = Label.IP_Sub_to_GF;
            insert objGrant;
        
            Implementation_Period__c objIP = TestClassHelper.createIP(objGrant,portalAccount);
        //  objIP.Status__c = 'Grant-Making';
            objIP.Status__c = 'Concept Note';
            objIP.CurrencyIsoCode='USD';
            objIP.Grant_Making_submission_date__c=Date.today().addYears(3);
            objIP.High_level_budget_GAC_1_EUR__c = 1000.00;
            objIP.High_level_budget_GAC_1_USD__c = 10002.99;
            objIP.High_level_budget_TRP_USD__c= 10003.77;
            objIP.High_level_budget_TRP_EUR__c= 10003.67;
            insert objIP;
            
            
            IP_Detail_Information__c ipDetail = new IP_Detail_Information__c();
            ipDetail.Implementation_Period__c = objIP.Id;
            insert ipDetail;
            
            Performance_Framework__c objPF = new Performance_Framework__c();
            objPF.Implementation_Period__c = objIP.Id;
            insert objPF; 
            
            HPC_Framework__c hpcFW = new HPC_Framework__c();
            hpcFW.Grant_Implementation_Period__c = objIP.Id;
            hpcFW.HPC_Status__c = 'Not Yet Submitted by PR';
            hpcFW.Total_HPC_Amount_USD__c = 2000;
            insert hpcFW; 
            
            /*EmailTemplate objEmailTempPR = EmailTemplate();
            objEmailTemp.DeveloperName='Detailed_Budget_Request_input_from_PR';
            insert objEmailTempPR;
            
            EmailTemplate objEmailTempCT = EmailTemplate();
            objEmailTemp.DeveloperName='CT agrees with final budget';
            insert objEmailTempCT;
            */
                
            objPF.PF_Status__c =Label.IP_Return_back_to_PR;    
            update objPF; 
            test.stopTest();
            objPF.PF_Status__c = Label.IP_Sub_to_GF;
            update objPF;
            
            /*Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setTargetObjectId(objContactPR.Id);
            mail.setWhatId(ipDetail.Id);
            // assuming this Template ID exists in your org
            mail.setTemplateId('00X17000000QEIr'); 
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });*/
    }
    
    static testMethod void myUnitTest() {
    
    Account objAcc = TestClassHelper.insertAccount();
    
     Country__c country = TestClassHelper.createCountry();
     country.X200DEMA_Exch_Rate_in_EUR__c = 10.0000;
     country.X200DEMA_Exch_Rate_in_USD__c = 10.000;
     insert country;
     
     Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
     objGrant.Country__c = country.Id;
     objGrant.Grant_Status__c = 'Accepted';
     insert objGrant;
     
     Implementation_Period__c ObjImplementationPeriod = TestClassHelper.createIP(objGrant,objAcc);
     ObjImplementationPeriod.Currency_of_Grant_Agreement__c = 'USD';
     ObjImplementationPeriod.Local_Currency__c = 'COP';
     ObjImplementationPeriod.Other_Currency__c = 'EUR';
     ObjImplementationPeriod.Exchange_Rate__c = 50;
     ObjImplementationPeriod.Budget_Status__c = 'Draft';
     insert ObjImplementationPeriod;
     
       Performance_Framework__C objPF = TestClassHelper.createPF(ObjImplementationPeriod);
       insert objPF;
       
       delete objPF;
    }
}