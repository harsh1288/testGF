/*********************************************************************************
* Controller Class: grantMultiCountriesExt
* DateCreated:  10/03/2015
----------------------------------------------------------------------------------
* Purpose/Methods:
* - page is opened from new Grant MultiCountries Button in Related List of GIP
* - creates Country Record against GIP

----------------------------------------------------------------------------------
* Unit Test: grantMultiCountriesExt_test
----------------------------------------------------------------------------------
* History:
* - VERSION  DATE            DETAIL FEATURES
    1.0      10/03/2015      INITIAL DEVELOPMENT   
*********************************************************************************/

public with sharing class grantMultiCountriesExt {

/********Define Variables*********************/
public Grant_Multi_Country__c mcRecord {get;set;}
public string country {get;set;} 
public Boolean displaypopupmod {get; set;}
public List<Country__c> lstcountry {get; set;}

public Paginate paginater {get;set;}
public List<Country__c> genericList{get;set;}   
public List<List<Country__c>> fullGenericList{get;set;}  
public boolean blnForOnlyMC {get;set;}
public String getBackUrl {get; set;}
list<Id> countryid = new List<Id>();
List<Grant_Multi_Country__c> lstIpMc =new List<Grant_Multi_Country__c>(); 
        
/**********************************************************************************************
    Purpose: Load the details needed to be displayed on the page
    Parameters: NA
    Returns: NA
    Throws [Exceptions]: NA
    **********************************************************************************************/
    
    public grantMultiCountriesExt(ApexPages.StandardController controller) {
        
        mcRecord = (Grant_Multi_Country__c)controller.getRecord();
        mcRecord.Grant_Implementation_Period__c = ApexPages.currentpage().getparameters().get('gid');
        Implementation_Period__c gip = [Select id,GIP_Type__c from Implementation_Period__c where id =: mcRecord.Grant_Implementation_Period__c];
        if(gip.GIP_Type__c == 'Multi-Country' || gip.GIP_Type__c == 'Regional'){
            blnForOnlyMC = true;
        }else{
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Info,'This is not a Multi-Country type of Record.You cannot add Countries for this type of Grant.'));  
            blnForOnlyMC = false;            
        }
        getBackUrl = '/'+mcRecord.Grant_Implementation_Period__c;
        String UserID=userinfo.getUserId();
        User objUser = [Select ID,Profile.Name, country from User where id=:UserID];
        Id profileId=userinfo.getProfileId();
        String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
        if(profileName.Contains('PR') || profileName.Contains('Applicant/CM') )
        //baseUrl = baseUrl+'/GM/';
        //if(objUser.Country!=null){
            getBackUrl = '/GM/'+mcRecord.Grant_Implementation_Period__c;
        //}
        
        lstIpMc =[select Country__c from Grant_Multi_Country__c where Grant_Implementation_Period__c=: mcRecord.Grant_Implementation_Period__c ];
        
        if(lstIpMc.size()>0){
           for(Grant_Multi_Country__c objMC :lstIpMc ){
              countryid.add(objMC.Country__c);
           }
        }
    }
    //Method to Save Record
    public PageReference save(){
        pageReference pr;
        Boolean errorMessage = false;
        displaypopupmod =false;        
       if(country == null || country == ''){
             ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,'Please Select Country');
             ApexPages.addMessage(myMsg); 
             return null;
        }      
        else {
            try{
                lstcountry = [select id,name from Country__c where id NOT IN: countryid AND name=: country Limit 1];
                if(lstcountry.size() == 0){
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,'Country entered is either already added or does not exists. Please select new Country.');
                    ApexPages.addMessage(myMsg); 
                    return null;
                }else{
                    mcRecord.Country__c = lstcountry[0].Id;
                    insert mcRecord;
                }
            }catch(exception e){
                system.debug('Exception '+e); 
            }
            string userTypeStr = UserInfo.getUserType();
            String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
            if(userTypeStr == Label.Standard_type) {
                baseUrl = baseUrl+'/';
            }
            else {
                
                Id profileId=userinfo.getProfileId();
                String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
                if(profileName.Contains('PR') || profileName.Contains('Applicant/CM') )
                baseUrl = baseUrl+'/GM/';
                else
                baseUrl = baseUrl+'/LFA/';                    
                //baseUrl = baseUrl+'/GM/'; 
                
            }
            pr = new pageReference(baseUrl+mcRecord.Grant_Implementation_Period__c);
            pr.setredirect(true);
         }   
        system.debug('error message'+errorMessage+pr);
        if(errorMessage == false)
           return pr;
        else
           return null; 
    } 
    
    /**********************************************************************************************
    Purpose: Cancels the saved changes and returns to the GIP detail page
    Parameters: NA
    Returns: NA
    Throws [Exceptions]: NA
    **********************************************************************************************/
    
    public pageReference cancel(){
        pageReference pr=new pageReference('/'+mcRecord.Grant_Implementation_Period__c);
        pr.setredirect(true);
        return pr;
    }
    
    //Custom lookup window for Module
    public void openlookupwindow() { 
        displaypopupmod =true;
         
        system.debug('country '+country);
          
        
        if(country == null || country == ''){
            lstcountry = [select id,name from Country__c where id not in:countryid];
        }else{
            lstcountry = [select id,name from Country__c where id not in:countryid AND name=: country];
        }
       
        // Pagination Code //
                //These lists hold the data
                this.fullGenericList = new List<List<Country__c>>();
                this.genericList = new List<Country__c>();
           if(lstcountry.size() >0){     
                //Get the data we need to paginate
                List<Country__c> resultsList = lstcountry;
                //Set the page size
                Integer pageSize = 10;
                //Create a new instance of Paginate passing in the overall size of the list of data and the page size you want
                this.paginater = new Paginate(resultsList.size(), pageSize);
               
                //Break out the full list into a list of lists
                if(resultsList.size() > 0){
                    List<Country__c> tempCC = new List<Country__c>();
                    Integer i = 0;
                    for(Country__c cc : resultsList){
                        tempCC.add(cc);
                        i++;
                        if(i == pageSize){
                            this.fullGenericList.add(tempCC);
                            tempCC = new List<Country__c>();
                            i = 0;
                        }
                    }
                    if(!tempCC.isEmpty()){
                        this.fullGenericList.add(tempCC);
                    }
                    //Gets the correct list of data to show on the page
                    this.genericList = this.fullGenericList.get(this.paginater.index);
                } 
                    //SerialNumber = pageIndex * pageSize ;
                    // Pagination Code //
       }
    }
      
    //to close popup window
    public void closepopup(){
       displaypopupmod = false;
    }
    
     /**********************************************************************************************
    Purpose: Give Pagination to the country lookup popup
    Parameters: NA
    Returns: NA
    Throws [Exceptions]: NA
    **********************************************************************************************/
     
    
    // Pagination Methods//
    public PageReference previousPage(){
        this.paginater.decrement();
        return changeData();
    }
    public PageReference nextPage(){
        this.paginater.increment();
        return changeData();
    }
  /*  public PageReference firstPage(){
        this.paginater.firstPage();
        return changeData();
    }
    public PageReference lastPage(){
        this.paginater.lastPage();
        return changeData();
    }

    public PageReference updatePage(){
        this.paginater.updateNumbers();
        return changeData();
    }*/
    public PageReference changeData(){
        this.genericList = this.fullGenericList.get(this.paginater.index);
        return null;
    }
    // Pagination Methods// 
}