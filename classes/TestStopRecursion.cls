/*********************************************************************************
* Test Class: {TestStopRecursion}
*  DateCreated : 01/30/2014
----------------------------------------------------------------------------------
* Purpose/Methods:
* - Used for cover all methods of StopRecursion.
----------------------------------------------------------------------------------
* History:
* - VERSION  DEVELOPER NAME    DATE            DETAIL FEATURES
     1.0                      01/30/2014      INITIAL DEVELOPMENT
*********************************************************************************/

@isTest
Public Class TestStopRecursion{
    Public static testMethod void TestStopRecursion(){
    
    StopRecursion objStopRecursion = new StopRecursion(); 
    StopRecursion.hasAlreadyCreatedFollowUpTasks();
    StopRecursion.setAlreadyCreatedFollowUpTasks();
    StopRecursion.hasAlreadyRunBeforeAcc();
    StopRecursion.setAlreadyRunBeforeAcc();
    StopRecursion.hasAlreadyRunAfterAcc();
    StopRecursion.setAlreadyRunAfterAcc();
    StopRecursion.hasAlreadyRunBeforeInsertAcc();
    StopRecursion.setAlreadyRunBeforeInsertAcc();
    StopRecursion.hasAlreadyRunBeforeUpdateAcc();
    StopRecursion.setAlreadyRunBeforeUpdateAcc();
    StopRecursion.hasAlreadyRunAfterInsertAcc();
    StopRecursion.setAlreadyRunAfterInsertAcc();
    StopRecursion.hasAlreadyRunAfterUpdateAcc();
    StopRecursion.setAlreadyRunAfterUpdateAcc();
    StopRecursion.hasalreadyRunShareGIPExt();
    StopRecursion.setalreadyRunShareGIPExt();
    StopRecursion.hasalreadyRunAfterInsertCN();
    StopRecursion.setalreadyRunAfterInsertCN();
    //StopRecursion.getFollowUpSubject(null);
    }
}