/*********************************************************************************
* {Test} Class: {TestCtrlLookupPage}
* Created by {DeveloperName},  {DateCreated 07/26/2013}
----------------------------------------------------------------------------------
* Purpose/Methods:
* - Used for cover all methods of CtrlLookupPage Controller.* 
*
----------------------------------------------------------------------------------
* History:
* - VERSION  DEVELOPER NAME    DATE            DETAIL FEATURES
   1.0                        07/26/2013
*********************************************************************************/
@isTest
Public class TestCtrlLookupPage{
    Public static testMethod Void TestCtrlLookupPage(){
        Account objAcc = new Account();
        objAcc.Name = 'Test Acc';
        insert objAcc;
        
        Country__c objCountry = new Country__c();
        objCountry.Name = 'Test Country';
        objCountry.Region_Name__c ='Test Region';
        insert objCountry;
        
        LFA_Work_Plan__c objWp = new LFA_Work_Plan__c();
        objWp.Name = 'Test Wp';
        objWp.LFA__c = objAcc.Id;
        objWp.Country__c = objCountry.id;
        insert objWp;
        
        Contact objCon = new Contact();
        objCon.AccountId = objAcc.Id;
        objCon.FirstName = 'Test FirstName';
        objCon.LastName = 'Test LastName';        
        objCon.Active_LFA_Member__c = 'yes';
        objCon.Level_of_Experience__c = 'test';
        insert objCon;
        
        System.currentPageReference().getparameters().put('lktp','Contact');
        System.currentPageReference().getParameters().Put('idfield',objAcc.Id);
        System.currentPageReference().getParameters().Put('Country',objCountry.Id);  
        CtrlLookupPage objLookupPage = new CtrlLookupPage();
        objLookupPage.searchString = 'test';
        objLookupPage.getFormTag();
        objLookupPage.getTextBox();
        objLookupPage.search();
        objLookupPage.ShowPopup();
        objLookupPage.HidePopup();
        objLookupPage.OpenPopupBySelectContact();
        
        Apexpages.currentpage().getparameters().put('contactId',objCon.id);
        objLookupPage.ShowPopupLink();
        
    }
    Public static testMethod Void TestCtrlLookupPage1(){
        Account objAcc = new Account();
        objAcc.Name = 'Test Acc';
        insert objAcc;
        
        Country__c objCountry = new Country__c();
        objCountry.Name = 'Test Country';
        objCountry.Region_Name__c ='Test Region';
        insert objCountry;
        
        Country__c objCountry1 = new Country__c();
        objCountry1.Name = 'Test Country';
        objCountry1.Region_Name__c ='Test Region';
        insert objCountry1;
        
        LFA_Work_Plan__c objWp = new LFA_Work_Plan__c();
        objWp.Name = 'Test Wp';
        objWp.LFA__c = objAcc.Id;
        objWp.Country__c = objCountry.id;
        insert objWp;
        
        Contact objCon = new Contact();
        objCon.AccountId = objAcc.Id;
        objCon.FirstName = 'Test FirstName';
        objCon.LastName = 'Test LastName';        
        objCon.Active_LFA_Member__c = 'yes';
        objCon.Level_of_Experience__c = 'test';
        insert objCon;
        
        LFA_Role__c objLFARole = new LFA_Role__c();
        objLFARole.Name = 'test';
        objLFARole.Default_Rate__c = 100;
        insert objLFARole;
        
        Rate__c objRate = new Rate__c();
        objRate.Contact__c = objCon.Id;       
        objRate.Rate__c = 100.00;
        objRate.Next_Year_Daily_Rate__c =200.00;
        objRate.Other_LFA_Role__c = 'test';
        objRate.LFA_Location__c = 'yes';
        objRate.GF_Approved__c = 'yes';
        objRate.Country__c = objCountry.Id;
        objRate.Active__c = true;
        insert objRate;
        
        Change_Request__c objChangeRequest = new Change_Request__c();
        objChangeRequest.Rate__c = objRate.Id;
        objChangeRequest.Comments__c = 'Test Commment';
        insert objChangeRequest;
        
        System.currentPageReference().getparameters().put('lktp','Contact');
        System.currentPageReference().getParameters().Put('idfield',objAcc.Id);
        System.currentPageReference().getParameters().Put('Country',objCountry.Id);
        Apexpages.currentpage().getparameters().put('WPId',objWp.id);
        CtrlLookupPage objClass = new CtrlLookupPage();
        objClass.strAccId = objAcc.Id;
        objClass.ContactId=objCon.id;
       
        
               
        objClass.ShowPopup();
        objClass.HidePopup();
        objClass.strLastName = 'Test';
        objClass.strFirstName = 'Test';
        objClass.strSelectLFARole = objLFARole.Id;
        objClass.strCountryIdFromPopup = objCountry1.Id;
        objClass.FillCountryIdsSet();
        
        objClass.FillRateOnChange();
        
        objClass.getType();
        objClass.getYear();
        objClass.getRateYear();
        objClass.OpenPopupBySelectContact();
        Apexpages.currentpage().getparameters().put('contactId',objCon.id);
        objClass.ShowPopupLink();
        
        FeedItem objFeed = new FeedItem();
        //objFeed.ParentId = objCon.Id;
        objFeed.Body = 'Test';
        objFeed.ContentFileName = 'TestDocument.txt';
        objFeed.ContentData =  blob.valueof('TestDocument.txt');
        //insert objFeed;
        objClass.objFeedItem =objFeed; 
        objClass.objwrpDocument.strType = 'CV';
        objClass.objwrpDocument.strYear = '2014';
        objClass.objwrpDocument.strFeedItemId = objFeed.id;
        objClass.objwrpDocument.CreatedDate = system.now();
        objClass.objwrpDocument.strCreatedBy = [Select Name From User Where ID =: UserInfo.getUserId()].Name;
        objClass.UploadDocument();
        
        objClass.strRateYear = 'Current Year';
       // objClass.SaveAndSubmit();
        objClass.strDeleteCountryId = objCountry.Id;
        objClass.lstCountry.add(objCountry);
        objClass.lstCountry.add(objCountry1);
        objClass.FillCountryIdsSet();
        objClass.DeleteCountryRate();
        
        
        
        Apexpages.currentpage().getparameters().put('Index','1');
        //objClass.FillDetailsOnChange();
        //Apexpages.currentpage().getparameters().put('DeleteCountryId',objCountry.id);
        objClass.AddDocument();
        objClass.CancelDocument();
        
        Apexpages.currentpage().getparameters().put('Index','1');
        objClass.DeleteDocument();
        
        Apexpages.currentpage().getparameters().put('RateId',objRate.id);
        objCon = null;
        objClass.SaveChangeRequest();
        
        CtrlLookupPage objClass1 = new CtrlLookupPage();
        objClass1.strAccId = objAcc.Id;
        objClass1.ShowPopup();
        objClass1.HidePopup();
        objClass1.strLastName = 'Test1';
        objClass1.strFirstName = 'Test1';
        //objClass1.SaveAndSubmit();
       
     } 
     
     Public static testMethod Void TestCtrlLookupPage2(){
        Account objAcc = new Account();
        objAcc.Name = 'Test Acc';
        insert objAcc;
        
        Country__c objCountry = new Country__c();
        objCountry.Name = 'Test Country';
        objCountry.Region_Name__c ='Test Region';
        insert objCountry;
        
        Country__c objCountry1 = new Country__c();
        objCountry1.Name = 'Test Country';
        objCountry1.Region_Name__c ='Test Region';
        insert objCountry1;
        
        LFA_Work_Plan__c objWp = new LFA_Work_Plan__c();
        objWp.Name = 'Test Wp';
        objWp.LFA__c = objAcc.Id;
        objWp.Country__c = objCountry.id;
        insert objWp;
        
        Contact objCon = new Contact();
        objCon.AccountId = objAcc.Id;
        objCon.FirstName = 'Test FirstName';
        objCon.LastName = 'Test LastName';        
        objCon.Active_LFA_Member__c = 'yes';
        objCon.Level_of_Experience__c = 'test';
        insert objCon;
        
        LFA_Role__c objLFARole = new LFA_Role__c();
        objLFARole.Name = 'test';
        objLFARole.Default_Rate__c = 100;
        insert objLFARole;
        
        Rate__c objRate = new Rate__c();
        objRate.Contact__c = objCon.Id;       
        objRate.Rate__c = 100.00;
        objRate.Next_Year_Daily_Rate__c =200.00;
        objRate.Other_LFA_Role__c = 'test';
        objRate.LFA_Location__c = 'yes';
        objRate.GF_Approved__c = 'yes';
        objRate.Country__c = objCountry.Id;
        objRate.Active__c = true;
        insert objRate;
        
        Change_Request__c objChangeRequest = new Change_Request__c();
        objChangeRequest.Rate__c = objRate.Id;
        objChangeRequest.Comments__c = 'Test Commment';
        insert objChangeRequest;
        
        System.currentPageReference().getparameters().put('lktp','Contact');
        System.currentPageReference().getParameters().Put('idfield',objAcc.Id);
        System.currentPageReference().getParameters().Put('Country',objCountry.Id);
        Apexpages.currentpage().getparameters().put('WPId',objWp.id);
        CtrlLookupPage objClass = new CtrlLookupPage();
        objClass.strAccId = objAcc.Id;
        objClass.ContactId=objCon.id;
       
        
               
        objClass.ShowPopup();
        objClass.HidePopup();
        objClass.strLastName = 'Test';
        objClass.strFirstName = 'Test';
        objClass.strSelectLFARole = objLFARole.Id;
        objClass.strCountryIdFromPopup = objCountry1.Id;
        objClass.FillCountryIdsSet();
        
        objClass.FillRateOnChange();
        
        objClass.getType();
        objClass.getYear();
        objClass.getRateYear();
        objClass.OpenPopupBySelectContact();
        Apexpages.currentpage().getparameters().put('contactId',objCon.id);
        objClass.ShowPopupLink();
        
        FeedItem objFeed = new FeedItem();
        //objFeed.ParentId = objCon.Id;
        objFeed.Body = 'Test';
        objFeed.ContentFileName = 'TestDocument.txt';
        objFeed.ContentData =  blob.valueof('TestDocument.txt');
        //insert objFeed;
        objClass.objFeedItem =objFeed; 
        objClass.objwrpDocument.strType = 'CV';
        objClass.objwrpDocument.strYear = '2014';
        objClass.objwrpDocument.strFeedItemId = objFeed.id;
        objClass.objwrpDocument.CreatedDate = system.now();
        objClass.objwrpDocument.strCreatedBy = [Select Name From User Where ID =: UserInfo.getUserId()].Name;
        objClass.UploadDocument();
        
        objClass.strRateYear = 'Next Year';
        //objClass.SaveAndSubmit();
        objClass.strDeleteCountryId = objCountry.Id;
        objClass.lstCountry.add(objCountry);
        objClass.lstCountry.add(objCountry1);
        objClass.FillCountryIdsSet();
        objClass.DeleteCountryRate();
        
        
        
        Apexpages.currentpage().getparameters().put('Index','1');
        //objClass.FillDetailsOnChange();
        //Apexpages.currentpage().getparameters().put('DeleteCountryId',objCountry.id);
        objClass.AddDocument();
        objClass.CancelDocument();
        
        Apexpages.currentpage().getparameters().put('Index','1');
        objClass.DeleteDocument();
        
        Apexpages.currentpage().getparameters().put('RateId',objRate.id);
        objCon = null;
        objClass.SaveChangeRequest();
        
        CtrlLookupPage objClass1 = new CtrlLookupPage();
        objClass1.strAccId = objAcc.Id;
        objClass1.ShowPopup();
        objClass1.HidePopup();
        objClass1.strLastName = 'Test1';
        objClass1.strFirstName = 'Test1';
        //objClass1.SaveAndSubmit();
       
     }       
}