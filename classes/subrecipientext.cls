public with sharing class subrecipientext {
   public string impid;
   public List<Implementation_Period__c> lstimp = new List<Implementation_Period__c>();
   public List<Account> lstsrs = new List<Account>();
   public List<wrapsubrecipients> wlstsrs {get;set;}
   public List<Implementer__c> lstimpl = new List<Implementer__c>();
   public list<Account> selectedsrs = new list<Account>();
   
    public subrecipientext(ApexPages.StandardController controller) {
      impid = ApexPages.currentpage().getparameters().get('id');
           system.debug('@@@@'+impid);
        lstimp=[select id,name,Principal_Recipient__c,Principal_Recipient__r.Country__c from Implementation_Period__c where id =:impid];
           system.debug('@@@@'+lstimp);
                wlstsrs = new List<wrapsubrecipients>();
        lstsrs=[Select Id,Name,Sector__c  From Account Where Country__c =: lstimp[0].Principal_Recipient__r.Country__c and RecordType.DeveloperName != 'LFA'
                and ID Not IN (select Account__c From Implementer__c  Where Grant_Implementation_Period__c =: impid)
                AND ID !=: lstimp[0].Principal_Recipient__c];
            system.debug('@@@@'+lstsrs); 
             
             for(Account objacc:lstsrs){
                //if(wlstsrs == Null){
                    wlstsrs.add(new wrapsubrecipients(objacc));
               // } 
             }
            system.debug('@@@@'+wlstsrs);  
    }
    
    public pageReference save(){
      for(wrapsubrecipients objwlst : wlstsrs){
          if(objwlst.selected == true){
             selectedsrs.add(objwlst.wrapAcc);
          }
      }
      
      for(Account objacc:selectedsrs){
        system.debug('@@@@'+selectedsrs.size());
         // for(integer i=0;i<selectedsrs.size();i++){
        Implementer__c objimp = new Implementer__c();
          objimp.Account__c  = objacc.Id;
          objimp.Grant_Implementation_Period__c = lstimp[0].id;
          objimp.Sector__c = objacc.Sector__c;
          //objimp.Implementer_Type__c = 'SR';
          lstimpl.add(objimp);
         // }
      }
       insert lstimpl;
      
         system.debug('@@@@'+lstimpl);
      pageReference pr = new pageReference('/'+impid);
      pr.setredirect(true);
        return pr;
    }
    
    public class wrapsubrecipients{
    public Account wrapAcc{get;set;}
    public boolean selected{get;set;}
    public wrapsubrecipients(Account objacc){
       wrapAcc = objacc;
       selected =false;
    }
    }

}