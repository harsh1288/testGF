@isTest(seeAllData = false)
private class Test_Edit_Indicator_Controller {

    public static testMethod void Edit_Indicator_Controller() {
        
    
        Country__c country = TestClassHelper.createCountry();
        country.X200DEMA_Exch_Rate_in_EUR__c = 10.0000;
        country.X200DEMA_Exch_Rate_in_USD__c = 10.000;
        insert country;
        
        Account objAcc = TestClassHelper.insertAccount();
        
        Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
        objGrant.Country__c = country.Id;
        objGrant.Grant_Status__c = 'Accepted';
        objGrant.Principal_Recipient__c = objAcc.Id;
        insert objGrant;
     
        Implementation_Period__c ObjImplementationPeriod = TestClassHelper.createIP(objGrant,objAcc);
        ObjImplementationPeriod.Currency_of_Grant_Agreement__c = 'USD';
        ObjImplementationPeriod.Local_Currency__c = 'COP';
        ObjImplementationPeriod.Other_Currency__c = 'EUR';
        ObjImplementationPeriod.Exchange_Rate__c = 50;
        ObjImplementationPeriod.Budget_Status__c = 'Draft';
        insert ObjImplementationPeriod;
     
        Performance_Framework__C objPF = TestClassHelper.createPF(ObjImplementationPeriod);
        insert objPF;
        
        Goals_Objectives__c objGoals = TestClassHelper.createGoalsObjectives();
        objGoals.Type__c = 'Goal';
        objGoals.Performance_Framework__c = objPF.id;
        insert objGoals;
        
        Grant_Indicator__c objInd = TestClassHelper.createGrantIndicator();
        objInd.Performance_Framework__c = objPF.Id;
        objInd.Indicator_Type__c = 'Impact';
        objInd.Goal_Objective__c = objGoals.Id;
        insert objInd;
        //objInd.Grant_Status__c = 'Accepted';
        
        Contact con = new Contact(FirstName = 'TestPR', LastName = 'PR Admin', AccountId =objAcc.Id, External_User__c = true,Email = 'testPRAmin@tgf.ex.com' );
        insert con;
        
        List<npe5__Affiliation__c> AffAccCon = [select Id,npe5__Status__c from npe5__Affiliation__c where npe5__Organization__c =:objAcc.id AND npe5__Contact__c =:con.id];
        AffAccCon[0].npe5__Status__c = 'former';
        update AffAccCon[0];
        
        npe5__Affiliation__c objAff = new npe5__Affiliation__c();
        objAff.npe5__Contact__c = con.Id;
        objAff.npe5__Organization__c = objAcc.Id;
        objAff.RecordTypeId = label.PR_Affiliation_RT;
        insert objAff;
        
        Profile profileId=[Select Id,Name from Profile where Name = 'PR Admin'];
        
        User user1 = new User(Alias = 'standt', Email='standarduserEIC@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='fr', 
        LocaleSidKey='en_US', ProfileId = profileId.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='standarduserEIC@testorg.com', ContactId= con.Id, Country = country.Name);
        insert user1;
      
        system.runAs(user1){
        ApexPages.currentPage().getParameters().put('id',objInd.id);
        ApexPages.currentPage().getParameters().put('retUrl',objPF.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(objInd);
        Edit_Indicator_Controller EditIndController = new Edit_Indicator_Controller(sc);
        
        EditIndController.reDirect();
      }
      
    }
     public static testMethod void Edit_Indicator_Controller1() {
        
    
        Country__c country = TestClassHelper.createCountry();
        country.X200DEMA_Exch_Rate_in_EUR__c = 10.0000;
        country.X200DEMA_Exch_Rate_in_USD__c = 10.000;
        insert country;
        
        Account objAcc = TestClassHelper.insertAccount();
        
        Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
        objGrant.Country__c = country.Id;
        objGrant.Grant_Status__c = 'Accepted';
        objGrant.Principal_Recipient__c = objAcc.Id;
        insert objGrant;
     
        Implementation_Period__c ObjImplementationPeriod = TestClassHelper.createIP(objGrant,objAcc);
        ObjImplementationPeriod.Currency_of_Grant_Agreement__c = 'USD';
        ObjImplementationPeriod.Local_Currency__c = 'COP';
        ObjImplementationPeriod.Other_Currency__c = 'EUR';
        ObjImplementationPeriod.Exchange_Rate__c = 50;
        ObjImplementationPeriod.Budget_Status__c = 'Draft';
        insert ObjImplementationPeriod;
     
        Performance_Framework__C objPF = TestClassHelper.createPF(ObjImplementationPeriod);
        insert objPF;
        
        Goals_Objectives__c objGoals = TestClassHelper.createGoalsObjectives();
        objGoals.Type__c = 'Goal';
        objGoals.Performance_Framework__c = objPF.id;
        insert objGoals;
        
        Grant_Indicator__c objInd = TestClassHelper.createGrantIndicator();
        objInd.Performance_Framework__c = objPF.Id;
        objInd.Indicator_Type__c = 'Impact';
        objInd.Goal_Objective__c = objGoals.Id;
        insert objInd;
        
        ApexPages.currentPage().getParameters().put('id',objInd.id);
        ApexPages.currentPage().getParameters().put('retUrl',objPF.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(objInd);
        Edit_Indicator_Controller EditIndController = new Edit_Indicator_Controller(sc);
        
        EditIndController.reDirect();
     }
}