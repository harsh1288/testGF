@isTest
Public Class Test_removeEdit{
public static testMethod void removeEdit(){
	Account objAcc = TestClassHelper.insertAccount();
	
	Country__c country = TestClassHelper.createCountry();
     country.X200DEMA_Exch_Rate_in_EUR__c = 10.0000;
     country.X200DEMA_Exch_Rate_in_USD__c = 10.000;
     insert country;
     
     Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
     objGrant.Country__c = country.Id;
     insert objGrant;
     
     Implementation_Period__c ObjImplementationPeriod = TestClassHelper.createIP(objGrant,objAcc);
     ObjImplementationPeriod.Currency_of_Grant_Agreement__c = 'USD';
     ObjImplementationPeriod.Local_Currency__c = 'COP';
     ObjImplementationPeriod.Other_Currency__c = 'EUR';
     ObjImplementationPeriod.Exchange_Rate__c = 50;
     ObjImplementationPeriod.Budget_Status__c = 'Draft';
     insert ObjImplementationPeriod;
	
	IP_Detail_Information__c objIP = TestClassHelper.createIPDetail();
	objIP.Implementation_Period__c = ObjImplementationPeriod.Id;
	insert objIP;
	
	Apexpages.currentpage().getparameters().put('id',objIP.Id);
	ApexPages.StandardController sc = new ApexPages.StandardController(objIP);
	removeEdit IPDetailed = new removeEdit(sc);
	
	IPDetailed.checkprof();
	IPDetailed.returnDetailPage();
}
}