public with sharing class AddingPRext {

/*Public string intrvntnid{get;set;}
public string cnid;
Public List<Intervention__c> lstintrvntn;
public List<Implementation_Period__c> lstgip;
public List<wrapimp> lstwgip{get;set;}
public List<Implementation_Period__c> selectedpr = new List<Implementation_Period__c>();
public List<Implementation_Period__c> imp;
public List<Id> prid = new List<Id>();
public list<Grant_Intervention__c > lstgin = new List<Grant_Intervention__c >();
public string message{get;set;}
public boolean display{get;set;}  
public boolean display1{get;set;}  
public List<RecordType> lstrec = new List<RecordType>();
 List<RecordType> lstrecmod = new List<RecordType>();
public List<RecordType> lstreccn = new List<RecordType>();
public List<Module__c> newlstmod = new List<Module__c>();
public list<Grant_Intervention__c> newlstgincn = new List<Grant_Intervention__c>();
public List<Grant_Intervention__c> lstgins = new List<Grant_Intervention__c>();
list<string> prname = new List<string>();
public String modid{get;set;}

    public AddingPRext(ApexPages.StandardController controller) {
      intrvntnid = ApexPages.currentpage().getparameters().get('id');
      system.debug('@@@@'+intrvntnid );
      lstintrvntn = [select id,Name,Concept_Note__c,Concept_Note__r.Component__c,Module_rel__c,Module_rel__r.Name,Module_rel__r.Catalog_Module__c,Module__c,Intervention__c from Intervention__c where id =: intrvntnid limit 1 ];
        if(lstintrvntn.size()>0){
           cnid = lstintrvntn[0].Concept_Note__c ;
        }
        lstgins =[select id,name,Pr1__c,Module__c from Grant_Intervention__c where Concept_Note__c=:cnid and Module__c=:lstintrvntn[0].Module_rel__c];
        for(Grant_Intervention__c objgin:lstgins){
            prname.add(objgin.Pr1__c);
        }
      lstgip = [select id,Principal_Recipient__c,Principal_Recipient__r.Name from Implementation_Period__c where Concept_Note__c =: cnid and Principal_Recipient__r.Name Not In:prname];
      
      if(lstgip.size()==0){
        display= true;
        display1=false;
         message='No PRs available for this Concept Note';
      }
      else{
        display= false;
        display1=true;
      if(lstwgip == Null){
        lstwgip = new List<wrapimp>();
         for(Implementation_period__c imp : lstgip){
              lstwgip.add(new wrapimp(imp));
               System.Debug('######@@@@@@######LSTwrp-->'+lstwgip);
         }
      }
      }
      modid=lstintrvntn[0].Module_rel__c;
      lstrec = [Select Id,SobjectType,Name From RecordType where Name = 'Grant-Making' and SobjectType = 'Grant_Intervention__c'  limit 1];
      lstreccn = [Select Id,SobjectType,Name From RecordType where Name = 'Concept Note' and SobjectType = 'Grant_Intervention__c'  limit 1];
      lstrecmod = [Select Id,SobjectType,Name From RecordType where Name = 'Grant-Making' and SobjectType = 'Module__c'  limit 1];
    }
    public pageReference save(){
      for(wrapimp wimp : lstwgip ){
         if(wimp.selected == true){
             selectedpr.add(wimp.gip);
         } 
      }
      if(selectedpr.size()==0){
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please select at least one PR');
        ApexPages.addMessage(myMsg); 
        return null;
      }
      else{
      for(Implementation_Period__c i : selectedpr ){
           prid.add(i.Principal_Recipient__c);
      }
      
      imp = new List<Implementation_Period__c>([select id,Principal_Recipient__c,Principal_Recipient__r.Name from Implementation_Period__c where Concept_Note__c =: cnid And Principal_Recipient__c in:prid ]);
           system.debug('###'+imp);
       for(Implementation_Period__c intr :selectedpr ){
           Module__c objmod = new Module__c();
           objmod.Name = lstintrvntn[0].Module_rel__r.Name;
           objmod.Implementation_Period__c = imp[0].id;
           objmod.Component__c = lstintrvntn[0].Concept_Note__r.Component__c;
           objmod.Catalog_Module__c = lstintrvntn[0].Module_rel__r.Catalog_Module__c ;
           //objmod.RecordTypeId = lstrec[0].id;
           newlstmod.add(objmod);
       }  
       insert newlstmod;
       List<Module__c> lstmod = [select id,Name from module__c where Implementation_Period__c =:imp[0].id  and Catalog_Module__c =: lstintrvntn[0].Module_rel__r.Catalog_Module__c];
      for(Implementation_Period__c intr :selectedpr ){
         Grant_Intervention__c objgin = new Grant_Intervention__c();
           objgin.Name = lstintrvntn[0].Name;
           objgin.Implementation_Period__c=imp[0].id;
           objgin.Module__c = lstmod[0].id;
           objgin.Catalog_Intervention__c = lstintrvntn[0].Intervention__c;
           objgin.Recordtypeid=lstrec[0].id;
           lstgin.add(objgin);
      }
      insert lstgin;
      
      for(Implementation_Period__c intr :selectedpr ){
         Grant_Intervention__c objgin = new Grant_Intervention__c();
           objgin.Name = lstintrvntn[0].Name;
           objgin.Concept_Note__c=lstintrvntn[0].Concept_Note__c;
           objgin.Module__c = lstintrvntn[0].Module_rel__c;
           objgin.Catalog_Intervention__c = lstintrvntn[0].Intervention__c;
           objgin.Recordtypeid=lstreccn[0].id;
           objgin.Pr1__c=intr.Principal_Recipient__r.Name;
           newlstgincn .add(objgin);
      }
      
      insert newlstgincn;
      pageReference pr= new pageReference('/'+lstintrvntn[0].Module_rel__c ); 
      pr.setRedirect(true);
      return pr;
      }
    
    }
    
    public class wrapimp{
      public Implementation_Period__c gip{get;set;}
      public boolean selected{get;set;}
      public wrapimp(Implementation_Period__c imp){
         gip = imp;
         selected = false;
      }
    }*/
   
}