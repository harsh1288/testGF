@isTest
public class CovOutputInlineEditControllerTest{

public static testMethod void TestMethod1(){
        
        Account objAcc = TestClassHelper.insertAccount();
        
        Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
        insert objGrant;
       
        Implementation_Period__c objIP = TestClassHelper.createIP(objGrant,objAcc);
        objIP.Reporting__c = 'Yearly';
        objIP.Status__c = 'Concept Note';
        objIP.PR_Fiscal_Cycle__c = 'January-December';
        insert objIP;   
        List<Performance_Framework__c > lstPF = new List<Performance_Framework__c >();
        Performance_Framework__c objPF = TestClassHelper.createPF(objIP);
        insert objPF; 
        lstPF.add(objPF);
        system.debug('**objPF'+objPF); 
        
        Period__c ObjRP = TestClassHelper.createPeriod();
        ObjRP.Implementation_Period__c = objIP.Id;
        ObjRP.Base_Frequency__c = 'Yearly';
        ObjRP.is_Active__c = true;
        insert ObjRP;
        
        Period__c ObjRP1 = TestClassHelper.createPeriod();
        ObjRP1.Implementation_Period__c = objIP.Id;
        ObjRP1.Base_Frequency__c = objIP.Reporting__c;
        ObjRP1.Performance_Framework__c = objPF.Id;
        insert ObjRP1;
        
        Grant_Indicator__c indicator = TestclassHelper.createGrantIndicator();
        indicator.Indicator_Type__c = 'Coverage/Output';
        indicator.Performance_Framework__c = objPF.Id;
        indicator.Reporting_Frequency__c = 'Based on Reporting Frequency';
        insert indicator;
        
        Grant_Indicator__c indicator1 = TestclassHelper.createGrantIndicator();
        indicator1.Indicator_Type__c = 'Coverage/Output';
        indicator1.Performance_Framework__c = objPF.Id;
        indicator1.Reporting_Frequency__c = '12 Months';
        insert indicator1;
        
        ApexPages.currentPage().getParameters().put('id',objPF.id);
       
        ApexPages.StandardSetController standardController = new ApexPages.StandardSetController(lstPF);
        CovOutputInlineEditController extObj = new CovOutputInlineEditController(standardController);               
        extObj.init();
        extObj.EditDissagregation();
        //extObj.updateParam();
        extObj.saveList();
        extObj.ReturnToInd();
    }
}