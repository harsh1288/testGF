public with sharing class Help_Guidance_Document_Upload{
    public String strLanguage {get; set;}
    public Guidance__c guidance {get; set;}
    
    public Help_Guidance_Document_Upload(){
        strLanguage = 'ENGLISH';
        if(System.UserInfo.getLanguage() == 'fr'){
            strLanguage = 'FRENCH'; 
        }
        if(System.UserInfo.getLanguage() == 'ru'){
            strLanguage = 'RUSSIAN'; 
        }
        if(System.UserInfo.getLanguage() == 'es'){
            strLanguage = 'SPANISH'; 
        }
        Document_Upload();
    }
    
    public void Document_Upload(){
        guidance = [SELECT Name, English_Guidance__c, French_Guidance__c,GM_Guidance_Name__c, Russian_Guidance__c, Spanish_Guidance__c, 
                    English_Guidance_Overflow__c, French_Guidance_Overflow__c, Russian_Guidance_Overflow__c, Spanish_Guidance_Overflow__c
                    FROM Guidance__c WHERE Name =: Label.Guidance_for_documents_related_list];
        
    }

}