@IsTest
public with sharing class WPTMMilestoneRPTriggerHandler_Test
{
    static testmethod void testmethod_A(){ 
    
        Account tAcc=TestClassHelper.createAccount();
        insert tAcc;
        
        Grant__c tGrant=TestClasshelper.createGrant(tAcc);
        insert tGrant;
        
        Implementation_Period__c tImpPeriod=TestClassHelper.createIP(tGrant,tAcc);
        insert tImpPeriod;
        
        Grant_Intervention__c tGi=TestClassHelper.createGrantIntervention(tImpPeriod);
        insert tGi;
        
        Key_Activity__c tKact = new Key_Activity__c();
        // tKact.Grant_Implementation_Period__c=tImpPeriod.id;
        tKact.Activity_Description__c = 'Test';
        tKact.Grant_Intervention__c=tGi.id;
        //tKact.Performance_Framework__c=;            
        insert tKact;
        
        //SELECT Criteria__c,Id,Key_Activity__c,Linked_To__c,MilestoneTitle__c,Name,Reporting_Period__c FROM Milestone_Target__c
        Milestone_Target__c tMtc=new Milestone_Target__c();
        tMtc.Key_Activity__c=tKact.id;
        tMtc.MilestoneTitle__c='TestTitle';
        insert tMtc;       
        
        Test.startTest(); 
        Map<Id,Milestone_Target__c> mileStoneRecord=new Map<Id,Milestone_Target__c>([SELECT Criteria__c,Id,Key_Activity__c,Linked_To__c,MilestoneTitle__c,Name,Reporting_Period__c FROM Milestone_Target__c]);
        //WPTMMilestoneRPTriggerHandler WPTMTriggerHandler = new WPTMMilestoneRPTriggerHandler();
        WPTMMilestoneRPTriggerHandler.deleteMileStoneRPRecords(mileStoneRecord);
        Test.stopTest();
        
    }
}