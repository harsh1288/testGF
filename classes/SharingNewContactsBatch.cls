global class SharingNewContactsBatch implements Schedulable, Database.Stateful, Database.Batchable<sObject> {
    global String query; 
    
     global Database.QueryLocator start(Database.BatchableContext BC){      
          query = 'Select Id, Share_To_Manage_Contact__c,npe5__organization__c,npe5__contact__c,npe5__status__c,Manage_Contacts__c from npe5__Affiliation__c where Share_To_Manage_Contact__c= True';
          system.debug(query);
          return Database.getQueryLocator(query);
     } 
     
     global void execute(Database.BatchableContext BC, List<npe5__Affiliation__c> scope) {        
        List<npe5__Affiliation__c> NewContactAffiliation = new List<npe5__Affiliation__c>();
        List<npe5__Affiliation__c> UncheckConShareAff = new List<npe5__Affiliation__c>();
        for(npe5__Affiliation__c shareContactAffiliation :(List<npe5__Affiliation__c>)scope ){
            NewContactAffiliation.add(shareContactAffiliation);
        }
        try{
            if(NewContactAffiliation.size()>0){
                AffiliationServices.shareNewAffiliatedContact(NewContactAffiliation);
                for(npe5__Affiliation__c ConShareAff:NewContactAffiliation){
                    ConShareAff.Share_To_Manage_Contact__c = false;
                    UncheckConShareAff.add(ConShareAff);    
                }
                system.debug('UncheckConShareAff::'+UncheckConShareAff);
                if(UncheckConShareAff.size()>0)
                    update UncheckConShareAff;
            }        
        }
        catch(DmlException dEx){
        }
        //errorMessages = InsertMissingSalesTeamServices.insertMissingSalesTeamRecords(scope);
     } 
     global void finish(Database.BatchableContext BC){}
      
     global void execute( SchedulableContext sc ) {        
         SharingNewContactsBatch job = new SharingNewContactsBatch(); 
         Database.executeBatch(job);
     }  
    
}