public with sharing class Update_Burn_Balance {

    public static Boolean flag = true; 
    
    public static void Update_Balance() {

        // Map to hold the Ids and balances of records to be updated
        // This is defined outside of the for loop, so that it is maintained
        // across querymore sets
        Map<Id, Decimal> mBals = new Map<Id, Decimal>();
        Decimal dBal; 
        
        // loop through all burn report items, starting at those with no 'previous' report
        // there is no way to categorically identify through SOQL the items that need to 
        // be updated, so fetch all and work out in memory
        // never expect > 10000 burn items, so shouldn't hit limit 
        //
        // calculations are run in date order, working on the assumption that items are 
        // linked to previous items with an earlier date (by week_ending).  
        for (List<Burn_Report__c> brs :
            [select Id, Balance_Budget_Hrs__c, Budget_Hrs_Wk__c, Actual_Hrs_Wk__c, 
                    Previous_Burn_Report__c, Week_Ending__c 
            FROM Burn_Report__c 
            ORDER BY Week_Ending__c ASC]){
            
            for (Burn_Report__c br : brs) {
                // calculate new balance
                
                system.debug ('Burn History item: ' + br );

                // retrieve newly calculated balance
                dBal = mBals.get(br.Previous_Burn_Report__c); 
                if (dBal == null) dBal = 0.0;

                // for past reports & reports where Actual hours has been set, 
                // increment the balance by the actual hours worked
                if (br.Week_Ending__c <= date.today() || 
                    (br.Actual_Hrs_Wk__c != 0.0 && br.Actual_Hrs_Wk__c != null)) {
                    dBal += br.Budget_Hrs_Wk__c == null ? 0.0 : br.Budget_Hrs_Wk__c;
                    dBal -= br.Actual_Hrs_Wk__c == null ? 0.0 : br.Actual_Hrs_Wk__c;
                } 
                
                // set new balance
                br.Balance_Budget_Hrs__c = dBal;
                
                system.debug ('New balance: ' + dBal);
                 
                // store new balance in Map
                mBals.put(br.Id, dBal); 
            }
            
            system.debug('Balances' + mBals); 
            update brs;
        }
    }
}