@isTest
Public Class ctrlObjectivesOutcomeTest{
    Public static testMethod void ctrlObjectivesOutcomeTest(){
        Test.startTest();     
        Account objAcc = TestClassHelper.insertAccount();
        Concept_Note__c objCN = TestClassHelper.createCN();
        objCN.CCM_new__c = objAcc.Id;
        objCN.Language__c = 'ENGLISH';
        insert objCN;
        
        Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
        insert objGrant;
        
        Implementation_Period__c objIP = TestClassHelper.createIP(objGrant,objAcc);
        //objIP.Concept_Note__c = objCN.id;
        insert objIP;
        
        Page__c ObjPage = TestClassHelper.createPage();
        ObjPage.Concept_note__c = objCN.Id;   
        insert Objpage;
        
        Goals_Objectives__c objGoal = TestClassHelper.createGoalsObjectives();
        
        objGoal.Concept_Note__c =objCN.Id;
        objGoal.Type__c = 'Objective';
        
        insert objGoal;
        
        
        Grant_Indicator__c objIndicator = TestClassHelper.createGrantIndicator();
        objIndicator.Concept_Note__c =objCN.Id;
        objIndicator.Goal_Objective__c = objGoal.id;
        insert objIndicator;
        
        Ind_Goal_Jxn__c objJun = TestClassHelper.insertIndicatorGoalJxn(objGoal,objIndicator);
        
        Indicator__c objCatIndicator = TestClassHelper.createCatalogIndicator();
        insert objCatIndicator;
        
        Project_multi_lingual__c objPs = TestClassHelper.createProjMultiLanguage();
        insert objPs;
        
        Guidance__c objGuidance = new Guidance__c();
        objGuidance.Name = 'Objective Indicator';
        insert objGuidance;
        
        Apexpages.currentpage().getparameters().put('id',objPage.Id);        
        ApexPages.StandardController sc = new ApexPages.StandardController(objPage);
        ctrlObjectivesOutcome objCO = new ctrlObjectivesOutcome(sc);
        
        Apexpages.currentpage().getparameters().put('SaveIndex','0');
        objCO.SaveGoal();
        objCO.AddNewGoal();
        objCO.getPageText();
        objCO.fillMapGoal();
        objCO.SetCatalogIndicator();
        objCO.FillAllIndicatorMap();
        //objCO.SaveNewSTdCustomIndicator(objIndicator,0);
        //Apexpages.currentpage().getparameters().put('EditIndex','1');
        //objCO.EditGoal();
        
        Apexpages.currentpage().getparameters().put('SaveIndex','3');
        objCO.SaveGoal();
        
        Apexpages.currentpage().getparameters().put('EditIndex','0');
        objCO.EditGoal();
        
        Apexpages.currentpage().getparameters().put('CancelIndex','0');
        objCO.CancelGoal();
         
        Apexpages.currentpage().getparameters().put('EditIndiIndex','0');
        objCO.EditIndicator();
        
        Apexpages.currentpage().getparameters().put('CancelIndiIndex','0');
        objCO.CancelIndicator();
        
        
        Apexpages.currentpage().getparameters().put('SaveIndiIndex','0');
        objCO.lstIndicators[0].setGoalIdAdd = objGoal.id;
        objCO.SaveIndicator();
        
        objCO.objNewIndicator = new Grant_indicator__c(Indicator_Full_Name__c = 'Test',Data_Type__c='Percent');
        objCO.setAddGoalCustom = objGoal.id;
        objCO.SaveNewIndicator();
        
        objCO.strSelectedIndicator = objCatIndicator.id;
        objCO.CreateIndicatorOnSelectCatalog();
        objCO.setAddGoalStandard = objGoal.id;
        //Apexpages.currentpage().getparameters().put('SaveIndex','0');
        //objCO.SaveStdIndicator();
        Apexpages.currentpage().getparameters().put('HistoryIndiIndex','0');
        objCO.ShowHistoryPopup();
        objCO.HidePopupHistory();
       
        Apexpages.currentpage().getparameters().put('DeleteIndiIndex','0');
        objCO.DeleteIndicator();
        
        Apexpages.currentpage().getparameters().put('DeleteIndex','0');
        objCO.DeleteGoal();
        //======//
        objCN.Language__c = 'FRENCH';
        update objCN;
        
        Apexpages.currentpage().getparameters().put('id',objPage.Id);        
        ApexPages.StandardController sc1 = new ApexPages.StandardController(objPage);
        // ctrlObjectivesOutcome objCOFr = new ctrlObjectivesOutcome(sc1);                             
        Test.stopTest();
    }
    Public static testMethod void CheckProfileTest2(){
         
        Test.startTest();
        Account objAcc =TestClassHelper.insertAccount();

        Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
        insert objGrant;
        

        Concept_Note__c objCN = TestClassHelper.createCN();
        objCN.Number_of_Years__c = '4';
        objCN.Status__c = 'Not yet submitted';
        objCN.CCM_new__c = objAcc.Id;
        objCN.Start_Date__c= system.today();
        insert objCN;
               
        
        Implementation_Period__c objIP = TestClassHelper.createIP(objGrant,objAcc);
        //objIP.Concept_Note__c = objCN.id;
        insert objIP;
        
        Page__c objPage = TestClassHelper.createPage();
        objPage.Concept_note__c = objCN.Id;
        ObjPage.Implementation_Period__c   =objIP.Id;
        insert objPage;
        
        Goals_Objectives__c objGoal = TestClassHelper.createGoalsObjectives();  
        objGoal.Concept_Note__c =objCN.Id;
        objGoal.Type__c = 'Objective';
        insert objGoal;
        
        Apexpages.currentpage().getparameters().put('id',objPage.Id);        
        ApexPages.StandardController sc = new ApexPages.StandardController(objPage);
        ctrlObjectivesOutcome objPg = new ctrlObjectivesOutcome(sc);
        
        Profile_Access_Setting_CN__c checkProfile = TestClassHelper.createProfileSettingCN();
        checkProfile.Page_Name__c ='CN_Objectives';
        checkProfile.Salesforce_Item__c = 'Edit';
        checkProfile.Status__c = 'Not yet submitted'; 
        insert checkProfile;
        objpg.checkProfile(); 
        Test.stopTest();               
    }
       
}