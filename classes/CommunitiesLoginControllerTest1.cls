/**
 * An apex page controller that exposes the site login functionality
 */
@IsTest global with sharing class CommunitiesLoginControllerTest1 {
    @IsTest(SeeAllData=true) 
    global static void testCommunitiesLoginController () {
        CommunitiesLoginController controller = new CommunitiesLoginController ();
        // 25-Jun-2013 Manu Erwin (Salesforce.com)  Fixing insufficient code coverage for default Communities Apex Tests
        PageReference pageRef = controller.forwardToAuthPage();
    }    
}