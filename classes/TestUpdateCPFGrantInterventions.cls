@isTest
Public Class TestUpdateCPFGrantInterventions{
    Public static testMethod void TestUpdateCPFGrantInterventions(){
                
        Country__c objCountry = new Country__c();
        objCountry.Name = 'ind';
        objCountry.Country_Fiscal_Cycle_End_Date__c = system.today();
        insert objCountry;
    
        Account objAccount = new Account();
        objAccount.name = 'test';
        objAccount.country__c = objCountry.id;
        insert objAccount;
        
        Concept_Note__c objConcept_Note = new Concept_Note__c();
        objConcept_Note.Name = 'test';
        objConcept_Note.Component__c = 'Tuberculosis';
        objConcept_Note.CCM_new__c = objAccount.id;
        insert objConcept_Note;
        
        CPF_Report__c objCPF_Report = new CPF_Report__c();
        objCPF_Report.Concept_Note__c = objConcept_Note.id;
        insert objCPF_Report;
        
        Catalog_Module__c objCatalogModule = new Catalog_Module__c();
        objCatalogModule.Name = 'Test Catalog Module';
        objCatalogModule.Component_Multi__c = 'Tuberculosis';
        insert objCatalogModule;
        
        Module__c objModule = new Module__c();
        objModule.Name = 'Test Module';
        objModule.CurrencyIsoCode = 'USD';
        objModule.Concept_Note__c = objConcept_Note.id;
        objModule.Catalog_Module__c = objCatalogModule.Id;
        insert objModule;
        
        Module__c objModule1 = new Module__c();
        objModule1.Name = 'Test Module';
        objModule1.CurrencyIsoCode = 'USD';
        objModule1.Catalog_Module__c = objCatalogModule.Id;
        objModule1.CN_Module__c = objModule.Id;
        insert objModule1;
        
        Implementation_Period__c objImplementation_Period = new Implementation_Period__c();
        objImplementation_Period.Concept_Note__c = objConcept_Note.id;
        objImplementation_Period.Principal_Recipient__c = objAccount.id;
        insert objImplementation_Period;
        
        Grant_Intervention__c objGrant_Intervention = new Grant_Intervention__c();
        objGrant_Intervention.Implementation_Period__c = objImplementation_Period.id;
        insert objGrant_Intervention;
        
        
        
        set<Id> setCNID = new set<Id>();
        setCNID.add(objConcept_Note.id);
        
        updateCPFGrantInterventions objupdateCPFGrantInterventions = new updateCPFGrantInterventions();
        updateCPFGrantInterventions.updateCPF(setCNID);
    }
    
    Public static testMethod void TestUpdateCPFGrantInterventionsHIV(){
                
        Country__c objCountry = new Country__c();
        objCountry.Name = 'ind';
        objCountry.Country_Fiscal_Cycle_End_Date__c = system.today();
        insert objCountry;
    
        Account objAccount = new Account();
        objAccount.name = 'test';
        objAccount.country__c = objCountry.id;
        insert objAccount;
        
        Concept_Note__c objConcept_Note = new Concept_Note__c();
        objConcept_Note.Name = 'test';
        objConcept_Note.Component__c = 'HIV/TB';
        objConcept_Note.CCM_new__c = objAccount.id;
        insert objConcept_Note;
        
        CPF_Report__c objCPF_Report = new CPF_Report__c();
        objCPF_Report.Concept_Note__c = objConcept_Note.id;
        insert objCPF_Report;
        
        Catalog_Module__c objCatalogModule = new Catalog_Module__c();
        objCatalogModule.Name = 'Test Catalog Module';
        objCatalogModule.Component_Multi__c = 'HIV/TB';
        insert objCatalogModule;
        
        Module__c objModule = new Module__c();
        objModule.Name = 'Test Module';
        objModule.CurrencyIsoCode = 'USD';
        objModule.Concept_Note__c = objConcept_Note.id;
        objModule.Catalog_Module__c = objCatalogModule.Id;
        insert objModule;
        
        Module__c objModule1 = new Module__c();
        objModule1.Name = 'Test Module';
        objModule1.CurrencyIsoCode = 'USD';
        objModule1.Catalog_Module__c = objCatalogModule.Id;
        objModule1.CN_Module__c = objModule.Id;
        insert objModule1;
        
        Implementation_Period__c objImplementation_Period = new Implementation_Period__c();
        objImplementation_Period.Concept_Note__c = objConcept_Note.id;
        objImplementation_Period.Principal_Recipient__c = objAccount.id;
        insert objImplementation_Period;
        
        Grant_Intervention__c objGrant_Intervention = new Grant_Intervention__c();
        objGrant_Intervention.Implementation_Period__c = objImplementation_Period.id;
        insert objGrant_Intervention;
        
        
        
        set<Id> setCNID = new set<Id>();
        setCNID.add(objConcept_Note.id);
        
        updateCPFGrantInterventions objupdateCPFGrantInterventions = new updateCPFGrantInterventions();
        updateCPFGrantInterventions.updateCPF(setCNID);
    }
}