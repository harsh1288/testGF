/*********************************************************************************
* Test class:   TestNewModuleExt
  Class: NewModuleExt
*  DateCreated : 26/11/2014
----------------------------------------------------------------------------------
* Purpose/Methods:
* - To test NewModuleExt class 
----------------------------------------------------------------------------------
* History:
* - VERSION  DEVELOPER NAME    DATE            DETAIL FEATURES
     1.0        TCS           26/11/2014      INITIAL DEVELOPMENT
*********************************************************************************/ 

@isTest
public class TestNewModuleExt {

    public static testMethod void TestModwCN() {
      
      Concept_Note__c objCN = TestClassHelper.createCN();
      insert objCN;
      
      Catalog_Module__c objCatMod = TestClassHelper.createCatalogModule();
      objCatMod.component_multi__c = objCN.Component__c;
      insert objCatMod;
      
      Catalog_Module__c objCMSelect = TestClassHelper.createCatalogModule();
      objCMSelect.Name = 'Module for Malaria';
      objCMSelect.component_multi__c = objCN.Component__c;
      insert objCMSelect;
      
      Module__c objModule = TestClassHelper.createModule();
      objModule.Catalog_Module__c = objCatMod.Id;
      objModule.Concept_Note__c = objCN.Id;
      insert objModule;
      
      List<Module__c> lstModules = new List<Module__c>();
      lstModules.add(objModule);
      
      Apexpages.currentpage().getparameters().put('id',ObjCN.Id);
      ApexPages.StandardSetController scwCN = new ApexPages.StandardSetController (lstModules);
      scwCN.setSelected(lstModules);
      NewModuleExt objmod = new NewModuleExt(scwCN);
      objmod.lstwrapcatmod[0].selected = true;
      System.debug('All Modules'+objmod.lstwrapcatmod);
      objmod.save();
      //System.assert(objMod.success); 
       
        
    }

public static testMethod void TestModwoCN()
	{

	   
       Account objAccount = TestClassHelper.createAccount();
	   insert objAccount;
	   	
	   Grant__c objGrant = TestClassHelper.insertGrant(objAccount); 
	   objGrant.Disease_Component__c = 'Malaria';
	   update objGrant; 
	   		   
	   Implementation_Period__c objIP = TestClassHelper.createIP(objGrant, objAccount);
       insert objIP;
       
       Catalog_Module__c objCatMod = TestClassHelper.createCatalogModule();
       objCatMod.component_multi__c = objIP.Component__c;
       insert objCatMod;
	   
       Catalog_Module__c objCMSelect = TestClassHelper.createCatalogModule();
       objCMSelect.Name = 'Module for Malaria';
       objCMSelect.component_multi__c = objIP.Component__c;
       insert objCMSelect;
	   
       Performance_Framework__c objPF = TestClassHelper.createPF(objIP);
       insert objPF;
       
       Module__c objModule =  TestClassHelper.createModule();
       objModule.Catalog_Module__c = objCatMod.Id;
       objModule.Implementation_Period__c = objIP.Id;
       objModule.Performance_Framework__c = objPF.Id;
       insert objModule;
      
       List<Module__c> lstModules = new List<Module__c>();
       lstModules.add(objModule);
       
       Apexpages.currentpage().getparameters().put('id',ObjPF.Id);       
       ApexPages.StandardSetController scwCN = new ApexPages.StandardSetController (lstModules);
       scwCN.setSelected(lstModules);
       System.debug('Object Id'+objIP.Id + objPF);
       NewModuleExt objmod = new NewModuleExt(scwCN);
       System.debug('All Modules'+objmod.lstwrapcatmod);
       objmod.save();
      
	}

}