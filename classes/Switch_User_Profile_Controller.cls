Public without sharing class Switch_User_Profile_Controller{

    public Switch_User_Profile_Controller()
    {
    }
      public static final string baseURL= Label.Site_url_for_select_profile;
    public User  currentUser {get;private set;}
    public List<profile> profileList {get;private set;}
    
    public pageReference initialize()
    {
        String profileId = ApexPages.currentPage().getParameters().get('profileId');
        return profileId == null ? initializeView() : initializeUpdate();
    }
    public pageReference initializeView()
    {
        currentUser = [Select Profile.UserLicenseId,
                       Profile.Name,Username from user 
                       where Id=:UserInfo.getUserId()limit 1];
                       
        profileList = [Select Name from profile where 
                       UserLicenseId =:currentUser.Profile.UserLicenseId and Id !=:currentUser.Profile.Id
                       order by Name asc limit 1000];
                       return null;
        
    }
    public pageReference initializeUpdate()
    {
        try
        {
            String profileId = ApexPages.currentPage().getParameters().get('profileId');
            String userId = ApexPages.currentPage().getParameters().get('userId');
            update new User(Id=userId,profileId=profileId);                
        }
        catch(exception ex)
        {
            ApexPages.addMessages(ex); 
            System.debug('Profile couldn\'t be changed due to: '+ex);
        }
        return null;
    }
    public pageReference selectProfile()
    {
        String profileId = ApexPages.currentPage().getParameters().get('profileId');
        String remoteUrl = baseURL+'/SelectProfile?profileId='+profileId+'&userId='+UserInfo.getUserId();
        if(!Test.isRunningtest())
        {
            HttpRequest httpRequest = new HttpRequest();
            httpRequest.setMethod('GET');
            httpRequest.setEndPoint(remoteUrl);        
            HttpResponse httpResponse = new Http().send(httpRequest);
        }
        return initializeView();
    }    
}