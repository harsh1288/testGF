/*********************************************************************************
* Class: ConceptNoteTriggerHandler
* Created by {Nikhil Kamath}, {DateCreated Apr-28-2015}
----------------------------------------------------------------------------------
* Purpose: This class is is used to Contain all methods used in ConceptNote trigger *    
----------------------------------------------------------------------------------
*********************************************************************************/

public class ConceptNoteTriggerHandler {
	
	public static void Share_with_CCM_CT (List<Concept_Note__c> lstcn) {
		
		List<Concept_Note__Share> cnShares  = new List<Concept_Note__Share>();
 		map<String,String> mapAccountCTId = new map<String,String>();

  		for(Concept_Note__c note : [Select Id, CCM_new__r.CT_ID__c from Concept_Note__c WHERE Id in: lstcn])
  		{
    		mapAccountCTId.put(note.Id,note.CCM_new__r.CT_ID__c);
  		}

 		for(Concept_Note__c cn : lstcn){
        
        	Concept_Note__Share ctShare = new Concept_Note__Share();       
        	ctShare.ParentId = cn.Id;       
        	ctShare.UserOrGroupId = mapAccountCTId.get(cn.Id); 
        	System.debug(mapAccountCTId.get(cn.Id));
        	                    
         	ctShare.AccessLevel = 'edit';            
        	// Specify that the reason
        	ctShare.RowCause = Schema.Concept_Note__Share.RowCause.Share_with_Country_Team__c;	            
    	    // Add the new Share record to the list of new Share records.
        	cnShares.add(ctShare);
    	}        
     	// Insert all of the newly created Share records and capture save result 
    	Database.SaveResult[] cnShareInsertResult = Database.insert(cnShares,false);
	}
	
	public static void rollupFieldsToBand (List<Concept_Note__c> oldlstcn, List<Concept_Note__c> newlstcn) {
		
		List<Concept_Note__c> cnList = (Trigger.isDelete) ? oldlstcn : newlstcn;
		Set<Id> bandIds = new Set<Id>();

		for(Concept_Note__c cn : cnList)
 		{
    		bandIds.add(cn.Band__c);  
 		}
    
		List<Band__c> bandList = [Select Id, Sum_of_Risk_Adjustments__c, Sum_of_Final_Allocations__c, Sum_of_Ceilings__c, Sum_of_Eligible_Risk_Adjustments__c, Sum_of_NFAs_not_equal_to_EF__c, Sum_of_Eligible_NFAs__c, Sum_of_Ineligible_NFAs__c, Sum_of_P_I_IR_Adjusted_Notional_Funding__c, Sum_of_NFAs__c, Sum_of_Hard_Floors__c, Sum_of_Soft_Floors__c, Sum_of_NAAs__c, (Select Id, Risk_Adjustment__c, P_I_IR_Adjusted_Notional_Funding__c, NFA_EF__c, NFA__c, Hard_Floor__c, Soft_Floor__c, NAA__c, Eligible_for_Rescaling__c, Final_Allocation__c, Existing_Funding__c, Funding_Ceiling__c from Concept_Notes__r) from Band__c where Id in :bandIds];
 
	    for(Band__c band : bandList)
    	{
        Decimal countRA = 0;
        for(integer i = 0; i < band.Concept_Notes__r.size(); i++)
           { if(band.Concept_Notes__r[i].Risk_Adjustment__c <> null)
            { countRA = countRA + band.Concept_Notes__r[i].Risk_Adjustment__c; }
        } band.Sum_of_Risk_Adjustments__c = countRA;
        
        Decimal countERA = 0;
        for(integer i = 0; i < band.Concept_Notes__r.size(); i++)
           { if(band.Concept_Notes__r[i].Risk_Adjustment__c <> null && band.Concept_Notes__r[i].Eligible_for_Rescaling__c == 'Yes')
            { countERA = countERA + band.Concept_Notes__r[i].Risk_Adjustment__c; }
        } band.Sum_of_Eligible_Risk_Adjustments__c = countERA;
        
        Decimal countNFAEF = 0;
        for(integer i = 0; i < band.Concept_Notes__r.size(); i++)
           { if(band.Concept_Notes__r[i].NFA__c <> null && band.Concept_Notes__r[i].Eligible_for_Rescaling__c == 'Yes' && band.Concept_Notes__r[i].NFA_EF__c == False)
            { countNFAEF = countNFAEF + band.Concept_Notes__r[i].NFA__c; }
        } band.Sum_of_NFAs_not_equal_to_EF__c = countNFAEF;
        
        Decimal countFC = 0;
        for(integer i = 0; i < band.Concept_Notes__r.size(); i++)
           { if(band.Concept_Notes__r[i].Funding_Ceiling__c <> null && band.Concept_Notes__r[i].Eligible_for_Rescaling__c == 'Yes')
            { countFC = countFC + band.Concept_Notes__r[i].Funding_Ceiling__c; }
        } band.Sum_of_Ceilings__c = countFC;
        
        Decimal countNE = 0;
        for(integer i = 0; i < band.Concept_Notes__r.size(); i++)
           { if(band.Concept_Notes__r[i].NFA__c <> null && band.Concept_Notes__r[i].Eligible_for_Rescaling__c == 'No')
            { countNE = countNE + band.Concept_Notes__r[i].NFA__c; }
        } band.Sum_of_Ineligible_NFAs__c = countNE;
        
        Decimal countE = 0;
        for(integer i = 0; i < band.Concept_Notes__r.size(); i++)
           { if(band.Concept_Notes__r[i].NFA__c <> null && band.Concept_Notes__r[i].Eligible_for_Rescaling__c == 'Yes')
            { countE = countE + band.Concept_Notes__r[i].NFA__c; }
        } band.Sum_of_Eligible_NFAs__c = countE;
        
        Decimal countANF = 0;
        for(integer i = 0; i < band.Concept_Notes__r.size(); i++)
           { if(band.Concept_Notes__r[i].P_I_IR_Adjusted_Notional_Funding__c <> null) 
            { countANF = countANF + band.Concept_Notes__r[i].P_I_IR_Adjusted_Notional_Funding__c; }
        } band.Sum_of_P_I_IR_Adjusted_Notional_Funding__c = countANF;
        
        Decimal countFA = 0;
        for(integer i = 0; i < band.Concept_Notes__r.size(); i++)
           { if(band.Concept_Notes__r[i].Final_Allocation__c <> null) 
            { countFA = countFA + band.Concept_Notes__r[i].Final_Allocation__c; }
        } band.Sum_of_Final_Allocations__c = countFA;
        
        Decimal countNFA = 0;
        for(integer i = 0; i < band.Concept_Notes__r.size(); i++)
           { if(band.Concept_Notes__r[i].NFA__c <> null)
            { countNFA = countNFA + band.Concept_Notes__r[i].NFA__c; }
        } band.Sum_of_NFAs__c = countNFA;
        
         Decimal countHF = 0;
        for(integer i = 0; i < band.Concept_Notes__r.size(); i++)
           { if(band.Concept_Notes__r[i].Hard_Floor__c <> null && band.Concept_Notes__r[i].Eligible_for_Rescaling__c == 'Yes')
            { countHF = countHF + band.Concept_Notes__r[i].Hard_Floor__c; }
        } band.Sum_of_Hard_Floors__c = countHF;
        
         Decimal countSF = 0;
        for(integer i = 0; i < band.Concept_Notes__r.size(); i++)
           { if(band.Concept_Notes__r[i].Soft_Floor__c <> null && band.Concept_Notes__r[i].Eligible_for_Rescaling__c == 'Yes')
            { countSF = countSF + band.Concept_Notes__r[i].Soft_Floor__c; }
        } band.Sum_of_Soft_Floors__c = countSF;
        
        Decimal countNAA = 0;
        for(integer i = 0; i < band.Concept_Notes__r.size(); i++)
           { if(band.Concept_Notes__r[i].NAA__c <> null)
            { countNAA = countNAA + band.Concept_Notes__r[i].NAA__c; }
        } band.Sum_of_NAAs__c = countNAA;        
    	}

		update bandList;	
	}
	
	public static void trgBeforeInsertCN (List<Concept_Note__c> lstcn) {
		
		List<Funding_Opportunity__c> lstFundOpps = [Select Id, Name, isCurrent__c from Funding_Opportunity__c where isCurrent__c = true];
 
 		if(!lstFundOpps.isEmpty()){
   			for(Concept_Note__c objCN: lstcn){
    			if(objCN.Funding_Opportunity__c == null){
    				objCN.Funding_Opportunity__c = lstFundOpps[0].Id; 
    			}
    		}
  		}
	}
	
	public static void trgAfterInsertCN (List<Concept_Note__c> lstcn) {
		
		List<Page__c> lstCNPages = new List<Page__c>();
    
    	Page__c objCNPage;
    	CPF_Report__c objCPF_HIV;
    	CPF_Report__c objCPF_TB;
    	CPF_Report__c objCPF;
    	Map<ID,set<ID>> mapCNTemplateID = new Map<ID,set<ID>>();
    	Map<String,set<ID>> mapCNComponentID = new Map<String,set<ID>>();  
    	List<CPF_Report__c> lstCPFReports = new List<CPF_Report__c>();
   
    	for(Concept_Note__c objCN:lstcn){
        	if(objCN.Component__c!=null){
            	if(mapCNComponentID.get(objCN.Component__c)==null){
                	mapCNComponentID.put(objCN.Component__c,new set<ID>{objCN.ID});
            	}else{
                	mapCNComponentID.get(objCN.Component__c).add(objCN.ID);
            	}
            
        	}
        	if(objCN.Page_Template__c!=null){
            	if(mapCNTemplateID.get(objCN.Page_Template__c)==null){
                	mapCNTemplateID.put(objCN.Page_Template__c,new set<ID>{objCN.ID});
            	}else{
                	mapCNTemplateID.get(objCN.Page_Template__c).add(objCN.ID);
            	}
        	}
        
        if(objCN.Component__c != 'HIV/TB'){
            objCPF = New CPF_Report__c();
            objCPF.Concept_Note__c = objCN.id;
            objCPF.Component__c = objCN.Component__c;
            objCPF.Country__c = objCN.CountryId__c;
            objCPF.Currency__c = objCN.CurrencyISOCode;
            objCPF.CurrencyIsoCode = objCN.CurrencyISOCode;
            objCPF.Reporting_Cycle__c = 'Calendar Year';
            objCPF.Implementation_Cycle__c = 'January - December';
            lstCPFReports.add(objCPF);
        } else {
            objCPF_HIV = New CPF_Report__c();
            objCPF_HIV.Concept_Note__c = objCN.id;
            objCPF_HIV.Component__c = 'HIV/AIDS';
            objCPF_HIV.Country__c = objCN.CountryId__c;
            objCPF_HIV.Currency__c = objCN.CurrencyISOCode;
            objCPF_HIV.CurrencyIsoCode = objCN.CurrencyISOCode;
            objCPF_HIV.Reporting_Cycle__c = 'Calendar Year';
            objCPF_HIV.Implementation_Cycle__c = 'January - December';
            lstCPFReports.add(objCPF_HIV);
            
            objCPF_TB = New CPF_Report__c();
            objCPF_TB.Concept_Note__c = objCN.id;
            objCPF_TB.Component__c = 'Tuberculosis';
            objCPF_TB.Country__c = objCN.CountryId__c;
            objCPF_TB.Currency__c = objCN.CurrencyISOCode;
            objCPF_TB.CurrencyIsoCode = objCN.CurrencyISOCode;
            objCPF_TB.Reporting_Cycle__c = 'Calendar Year';
            objCPF_TB.Implementation_Cycle__c = 'January - December';
            lstCPFReports.add(objCPF_TB);
        }
    	}   
    	insert lstCPFReports;
    	//Query the pages from the selected template and add these pages to the list to be inserted.
     
    	for(Page__c objPage: [select Id,Name,URL_Prefix__c,Template__c,Order__c,modular__c, Guidance__c, Standard_Controller__c,French_Name__c,Spanish_Name__c,Russian_Name__c
                            from Page__c 
                            where Template__c IN:mapCNTemplateID.keyset()]){
        if(mapCNTemplateID.get(objPage.Template__c)!=null){                    
            for(ID CNid : mapCNTemplateID.get(objPage.Template__c)){
                objCNPage = New Page__c();
                objCNPage.Name = objPage.Name;
                objCNPage.Concept_Note__c = CNid;
                objCNPage.URL_Prefix__c = objPage.URL_Prefix__c;
                objCNPage.Order__c = objPage.Order__c;
                objCNPage.Status__c = 'Not yet ready for review';
                objCNPage.modular__c = objpage.modular__c;
                objCNPage.Guidance__c = objpage.Guidance__c;
                objCNPage.Standard_Controller__c = objpage.Standard_Controller__c;
                objCNPage.French_Name__c = objpage.French_Name__c;
                objCNPage.Spanish_Name__c = objpage.Spanish_Name__c;
                objCNPage.Russian_Name__c = objpage.Russian_Name__c;
                lstCNPages.add(objCNPage);
            }
        }
    	}
    	insert lstCNPages;
    /*Query the Catalog Impact/Outcome Indicators for this Component and create Grant Indicators for this
    that link them to this CN*/
    /*List<Grant_Indicator__c> lstGrantIndicator = new List<Grant_Indicator__c>();
    Grant_Indicator__c objGrantIndicator;
    For(Indicator__c objIndicator: [Select ID, name,Type_of_Data__c,Indicator_Type__c,Programme_Area__c
                                        from Indicator__c
                                        where Programme_Area__c IN: mapCNComponentID.keyset()
                                        and Default_Indicator__c=true
                                        and (Indicator_Type__c='Outcome' or Indicator_Type__c='Impact')])
    {
        if(mapCNComponentID.get(objIndicator.Programme_Area__c)!=null){
            for(ID CNid : mapCNComponentID.get(objIndicator.Programme_Area__c)){
                objGrantIndicator= new Grant_Indicator__c();
                objGrantIndicator .Indicator__c = objIndicator.ID;
                objGrantIndicator .Type__c = objIndicator.Type_of_data__c;
                objGrantIndicator .Data_Type__c = objIndicator.Type_of_Data__c;
                objGrantIndicator .Concept_Note__c = CNid;
                lstGrantIndicator.add(objGrantIndicator);
            }
        }
    }*/
    //insert lstGrantIndicator;
	}
	
	public static void trgAfterUpdateCN (List<Concept_Note__c> lstcn, Map<Id,Concept_Note__c> oldMap) {
		
		Set<Id> setCNIds = new Set<Id>();

    	for(Concept_Note__c CN : lstcn){
        if(CN.CurrencyIsoCode != oldMap.get(CN.Id).CurrencyIsoCode){
          setCNIds.add(CN.Id);
        }
    	}
    
    	List<CPF_Report__c> lstCPF = [Select Id, CurrencyIsoCode, Concept_Note__r.CurrencyIsoCode,
                               (Select Id, CurrencyIsoCode from Funding_Sources__r)
                               from CPF_Report__c where Concept_Note__c in : setCNIds];
    
    	List<Funding_Source__c> lstFundingSourceToUpdate = new List<Funding_Source__c>();
    
    	for(CPF_Report__c CPF : lstCPF){
        CPF.Currency__c = CPF.Concept_Note__r.CurrencyIsoCode;
        CPF.CurrencyIsoCode = CPF.Concept_Note__r.CurrencyIsoCode;
        
        for(Funding_Source__c FS : CPF.Funding_Sources__r){
            FS.CurrencyIsoCode = CPF.Concept_Note__r.CurrencyIsoCode;
            lstFundingSourceToUpdate.add(FS);
        }   
    	}
    
    	update lstCPF;
    	update lstFundingSourceToUpdate;
	}
	
	public static void ShareCNAffliation (List<Concept_Note__c> lstcnote) {
		
		Set<Id> setCNId = new Set<Id>();
    	Set<Id> setAccId = new Set<Id>();
    	List<Concept_Note__c> cnListShare = new List<Concept_Note__c>();
    	AccountShare externalAFFRecord = new AccountShare();
    	Set<Id> CNIds = new Set<Id>();
    	Map<Id,Id> userConMap = new Map<Id,Id>(); 
    	List<Concept_Note__Share> lstCNShare = new List<Concept_Note__Share>();
    	List<AccountShare> externalAccShareCCM = new List<AccountShare>();
    	List<AccountShare> lstAffShare  = new List<AccountShare>();
        
    	for(Concept_Note__c objCN : lstcnote){
    	setCNId.add(objCN.Id);
    	setAccId.add(objCN.CCM_New__c);
    	}
     	List<User> lstUser = [Select ID,Profile.Name,ContactId from User where User.Profile.UserLicense.Name=:'Partner Community Login'];
     	/*List<npe5__Affiliation__c> lstAff = [Select Id,Name,Is_Inactive_c__c,Access_Level__c,npe5__Organization__c,npe5__Contact__c from npe5__Affiliation__c  where npe5__Organization__c in : setAccId];*/
     	List<npe5__Affiliation__c> lstAff = [Select Id,Name,Is_Inactive_c__c,Access_Level__c,npe5__Organization__c,npe5__Contact__c, npe5__Status__c, RecordTypeId from npe5__Affiliation__c  where npe5__Organization__c in : setAccId
                                                                                                                                                              AND (RecordTypeId =: label.CM_Affiliation_RT OR RecordTypeId =: label.PR_Affiliation_RT)
                                                                                                                                                            AND npe5__Status__c = 'Current'];  /*27-02-2015: Added condition for CRM change in Access Level values*/
    	System.debug('**lstUser '+lstUser);
    	System.debug('**lstAff '+lstAff );
    	for(User objUser :lstUser ){
        userConMap.put(objUser.ContactId,objUser.id);
    	}
    	
    	List<Concept_Note__c> lstCN = [Select Id,Name,CCM_New__c from Concept_Note__c Where Id in : setCNId ];
    	System.debug('**lstCN '+lstCN );
    	Map<id, List<Concept_Note__c>> conceptMap = new Map<id,List<Concept_Note__c>>();
     
    	/*Get the Account and its related Concept Note */
    	for(Concept_Note__c c : lstCN){
        if(conceptMap.get(c.CCM_New__c) != null){
            List<Concept_Note__c> DocList =conceptMap.get(c.CCM_New__c);
            DocList.add(c);
            system.debug('**DocList'+DocList);
            conceptMap.put(c.CCM_New__c,DocList);
        }
        else{
            List<Concept_Note__c> DocList = new List<Concept_Note__c>();
            DocList.add(c);
            conceptMap.put(c.CCM_New__c,DocList);
        }
        system.debug('**conceptMap'+conceptMap);
    	}
    
      	for(npe5__Affiliation__c objAff : lstAff){
        //if(objAff.Is_Inactive_c__c == false && (objAff.Access_Level__c=='CCM Admin' ||  objAff.Access_Level__c=='CCM Read Write' )){  
          if(objAff.npe5__Status__c == 'Current' && (objAff.Access_Level__c=='Admin' ||  objAff.Access_Level__c=='Read/Write' )  && objAff.RecordTypeId == label.CM_Affiliation_RT){ /*27-02-2015: Added condition for CRM change in Access Level values*/            
            /*externalAFFRecord.AccountID = objAff.npe5__Organization__c;
            
            externalAFFRecord.UserOrGroupId = userConMap.get(objAff.npe5__Contact__c);
            
            System.debug('**UserOrGroupId '+externalAFFRecord.UserOrGroupId);
            System.debug('**externalAFFRecord.AccountID'+externalAFFRecord.AccountID);
            externalAFFRecord.AccountAccessLevel= 'Edit';
            externalAFFRecord.OpportunityAccessLevel = 'Edit';*/
            
           /***********************Share concept Notes records related to CCM Organisation******************************************************/
              
            if(lstCN.size()>0){
                cnListShare = conceptMap.get(objAff.npe5__Organization__c);
                List<Implementation_Period__c> lstIP = [Select id,Principal_Recipient__c from Implementation_Period__c where Concept_Note__c in :cnListShare ];
                System.debug('****lstIP'+lstIP);
                for(Concept_Note__c cn :cnListShare){
                    Concept_Note__Share cns = new Concept_Note__Share();
                    cns.ParentId = cn.ID;
                    cns.AccessLevel = 'Edit';
                    cns.UserOrGroupId = userConMap.get(objAff.npe5__Contact__c);
                    System.debug('**UserOrGroupId of Concept NOte'+cns.UserOrGroupId);
                    System.debug('**Parent Id of the record'+cns.ParentId);
                    lstCNShare.add(cns);
                    
                }
                
                /******************* Share Implementation Period of Concept note which where shared for CCM organistaion**************/
                
                /*for(Implementation_Period__c ip : lstIP){
                    AccountShare accshare = new AccountShare();
                    accshare.AccountID = ip.Principal_Recipient__c;
                    accshare.UserOrGroupId = userConMap.get(objAff.npe5__Contact__c);
                    accshare.AccountAccessLevel= 'Edit';
                    accshare.OpportunityAccessLevel = 'Edit';
                    externalAccShareCCM.add(accshare);
                    
                }*/
            }
            
            
            //externalAFFRecord.RowCause = ;
            //lstAffShare.add(externalAFFRecord);
           
        }
        
         /************************* Share Implementation period of PR Organisation **********************/
             
        //if(objAff.Is_Inactive_c__c == false && (objAff.Access_Level__c == 'PR Admin' ||  objAff.Access_Level__c == 'PR Read Write')){
          if(objAff.npe5__Status__c == 'Current' && (objAff.Access_Level__c=='Admin' ||  objAff.Access_Level__c=='Read/Write' )  && objAff.RecordTypeId == label.PR_Affiliation_RT){ /*27-02-2015: Added condition for CRM change in Access Level values*/  
            externalAFFRecord = new AccountShare();
            externalAFFRecord.AccountID = objAff.npe5__Organization__c;
            externalAFFRecord.UserOrGroupId = userConMap.get(objAff.npe5__Contact__c);
            externalAFFRecord.AccountAccessLevel= 'Edit';
            externalAFFRecord.OpportunityAccessLevel = 'Edit';
            lstAffShare.add(externalAFFRecord);
            
            List<Implementation_Period__c> lstIP = [Select id,Principal_Recipient__c,Concept_Note__c from Implementation_Period__c where Principal_Recipient__c =:objAff.npe5__Organization__c];
            System.debug('**lstIP'+lstIP);
            
            for(Implementation_Period__c ip : lstIP){
                CNIds.add(ip.Concept_Note__c);
            }
            List<Concept_Note__c> lstCNPR = [Select Id,Name,CCM_New__c from Concept_Note__c Where id in : CNIds];
            System.debug('**lstCNPR'+lstCNPR);
            
            /************* Share Concept Note for which IP is created **************/
            
            for(Concept_Note__c cpr :lstCNPR){
                Concept_Note__Share cnps = new Concept_Note__Share();
                cnps.ParentId = cpr.ID;
                cnps.AccessLevel = 'Read';
                cnps.UserOrGroupId = userConMap.get(objAff.npe5__Contact__c);
                System.debug('**UserOrGroupId of Concept NOte'+cnps.UserOrGroupId);
                System.debug('**Parent Id of the record'+cnps.ParentId);
                lstCNShare.add(cnps);
                
            }
        }
        
        
        //if( objAff.Is_Inactive_c__c == false && (objAff.Access_Level__c=='CCM Read')){    
          if(objAff.npe5__Status__c == 'Current' && objAff.Access_Level__c=='Read'  && objAff.RecordTypeId == label.CM_Affiliation_RT){ /*27-02-2015: Added condition for CRM change in Access Level values*/ 
            if(lstCN.size()>0){
                cnListShare = conceptMap.get(objAff.npe5__Organization__c);
                List<Implementation_Period__c> lstIP = [Select id,Principal_Recipient__c from Implementation_Period__c where Concept_Note__c in :cnListShare ];
                System.debug('****lstIP'+lstIP);
                for(Concept_Note__c cn :cnListShare){
                    Concept_Note__Share cns = new Concept_Note__Share();
                    cns.ParentId = cn.ID;
                    cns.AccessLevel = 'Read';
                    cns.UserOrGroupId = userConMap.get(objAff.npe5__Contact__c);
                    System.debug('**UserOrGroupId of Concept NOte'+cns.UserOrGroupId);
                    System.debug('**Parent Id of the record'+cns.ParentId);
                    lstCNShare.add(cns);
                    //CNtoDeleteCCM.add(cn.ID);
                }
                /*for(Implementation_Period__c ip : lstIP){
                    AccountShare accshare = new AccountShare();
                    accshare.AccountID = ip.Principal_Recipient__c;
                    accshare.UserOrGroupId = userConMap.get(objAff.npe5__Contact__c);
                    accshare.AccountAccessLevel= 'Read';
                    accshare.OpportunityAccessLevel = 'Read';
                    externalAccShareCCM.add(accshare);
                    
                }*/
          }    
     }
     
     	//if( objAff.Is_Inactive_c__c == false && (objAff.Access_Level__c == 'PR Read')){
       	if(objAff.npe5__Status__c == 'Current' && objAff.Access_Level__c=='Read'  && objAff.RecordTypeId == label.PR_Affiliation_RT){ /*27-02-2015: Added condition for CRM change in Access Level values*/
            externalAFFRecord.AccountID = objAff.npe5__Organization__c;
            externalAFFRecord.UserOrGroupId = userConMap.get(objAff.npe5__Contact__c);
            externalAFFRecord.AccountAccessLevel= 'Read';
            externalAFFRecord.OpportunityAccessLevel = 'Read';
            lstAffShare.add(externalAFFRecord);
            //AcctoDeletePR.add(objAff.npe5__Organization__c);
            List<Implementation_Period__c> lstIP = [Select id,Principal_Recipient__c,Concept_Note__c from Implementation_Period__c where Principal_Recipient__c =:objAff.npe5__Organization__c];
            System.debug('**lstIP'+lstIP);
            for(Implementation_Period__c ip : lstIP){
                CNIds.add(ip.Concept_Note__c);
            }
            List<Concept_Note__c> lstCNPR = [Select Id,Name,CCM_New__c from Concept_Note__c Where id in : CNIds];
            System.debug('**lstCNPR'+lstCNPR);
            for(Concept_Note__c cpr :lstCNPR){
                Concept_Note__Share cnps = new Concept_Note__Share();
                cnps.ParentId = cpr.ID;
                cnps.AccessLevel = 'Read';
                cnps.UserOrGroupId = userConMap.get(objAff.npe5__Contact__c);
                System.debug('**UserOrGroupId of Concept NOte'+cnps.UserOrGroupId);
                System.debug('**Parent Id of the record'+cnps.ParentId);
                lstCNShare.add(cnps);
                //CNtoDeletePR.add(cpr.ID);
            }
        }
    
    	}// lstAff loops ends here
        System.debug('lstAffShare'+lstAffShare);
     	Database.SaveResult[] accShareInsertResult = Database.insert(externalAccShareCCM,false);
     	Database.SaveResult[] accShareInsertResult2 = Database.insert(lstAffShare,false);
     	Database.SaveResult[] cnShareInsertResult = Database.insert(lstCNShare,false);

	    System.debug(accShareInsertResult);
    	System.debug(cnShareInsertResult);
	}
	
	public static void PCNSubmittedAfterUpdate (List<Concept_Note__c> lstcn) {
		
		List<PCN_Criteria__c> lstToUpdate = new List<PCN_Criteria__c>();
		for(Concept_Note__c c : lstcn){
    		lstToUpdate = [Select Id,Name,Status__c,Concept_Note__c from PCN_Criteria__c where Concept_Note__c =:c.Id and Is_Perf_Imp_Inc_Risk__c = true];
    		if(c.P_I_IR_Risk_Submitted_Final__c == true){
    			for(PCN_Criteria__c obj :lstToUpdate){
        			If(obj.Status__c == 'Under CT Review'){
            			obj.Status__c = 'Accepted';
        			}    
					update lstToUpdate;
    			}
    		}
    	}
	}
}