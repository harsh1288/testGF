//****************************************************************************************
// Purpose     :  This Class is used to create Key Activity from Performance framework page
// Date        :  3 April 2015
// Created By  :  TCS 
//**************************************************************************************** 
public class WPTMKeyActivityExtensionA{

    public Key_Activity__c keyAcc{get; set;}
    public List<selectOption> options {get; set;}
    public List<selectOption> moduleOptions{get; set;}
    public String performanceId{get; set;}
    public Performance_Framework__c  performanceFrameworkRec {get;set;} 
    public WPTMSecurityObject  securityMatrixObj{get;set;}
    public String performanceFrameworkstatus{get;set;}
    public User userRec ;
    Public Id loggedInUserId;
    public String contactId; 
    public List<npe5__Affiliation__c> affiliationList;
    public String performanceFrameworkaccountId;
    public String profileName{get;set;}
    public boolean blfaprofile{get;set;}
    public Id moduleId{get;set;}
    public Id ModuleNewId{get;set;}
    public WPTMKeyActivityExtensionA(ApexPages.StandardController controller){
        //options= new List<selectOption>(); 
        moduleOptions = new List<selectOption>(); 
        keyAcc = new Key_Activity__c();
        performanceId= ApexPages.currentPage().getParameters().get('Performance');      
        if(performanceId == null){
            keyAcc = (Key_Activity__c)controller.getRecord();
            keyAcc = [select id, Performance_Framework__c,Activity_Description__c,Global_Fund_Comments_to_LFA__c,
                    Global_Fund_Comments_to_PR__c, Grant_Implementation_Period__c, Grant_Intervention__r.name,Grant_Intervention__r.Module__c,
                    LFA_Comments__c, Number_of_Milestones__c, PF_Status__c, PR_Comments__c,Grant_Intervention__r.Module__r.name
                    from Key_Activity__c where id=: keyAcc.id];
            system.debug('@@keyAcc '+keyAcc );
            performanceId = keyAcc.Performance_Framework__c;
            
        }
        moduleId= keyAcc.Grant_Intervention__r.Module__c; 
        system.debug('@@performanceId '+performanceId ); 
        performanceFrameworkRec =[SELECT Implementation_Period__r.GIP_Type__c,PF_Status__c FROM Performance_Framework__c 
                        WHERE id=:performanceId] ;
        performanceFrameworkstatus =performanceFrameworkRec.PF_Status__c;

        loggedInUserId= userinfo.getUserId() ;                            
        Id profileId=userinfo.getProfileId();
        profileName=[Select Id,Name from Profile where Id=:profileId].Name;
        if(profileName=='LFA Portal User'){
            blfaprofile = true;
        }else{
            blfaprofile = false;    
        }
        showModules();
        //showGrantInterventions();
        securityMatrixObj = WPTMSecurityObject.XMLDomDocument(profileName,performanceFrameworkstatus);
        system.debug('@@@@modulenewId'+modulenewId);
    }
    //Fill Modules Pick List
    public void showModules(){
        for(Module__c moduleRec : Database.Query('select Id,Name  from Module__c where Performance_Framework__c =: performanceId')){
            moduleOptions.add(new selectOption(moduleRec.id, moduleRec.Name));
        }    system.debug('blfaprofile@@@@'+blfaprofile);
    
    }
    //Fill Grant Intervention List
    public void showGrantInterventions(){
        if(performanceId!=NULL && modulenewId!=NULL){
            for(Grant_Intervention__c giObj : Database.Query('SELECT Id,Name,Custom_Intervention_Name__c,Catalog_Intervention__r.Is_Other_Intervrntion__c from Grant_Intervention__c where Performance_Framework__c =: performanceId AND Module__c=:modulenewId') ){
                if(giObj.Catalog_Intervention__r.Is_Other_Intervrntion__c == false){
                    options.add(new selectOption(giObj.id, giObj.Name));
                }else{
                    if(giObj.Custom_Intervention_Name__c!= null){
                        options.add(new selectOption(giObj.id, giObj.Custom_Intervention_Name__c));
                    }
                }
            } 
        }
        else{
        system.debug('In Else@@@');
            for(Grant_Intervention__c giObj : Database.Query('SELECT Id,Name,Custom_Intervention_Name__c,Catalog_Intervention__r.Is_Other_Intervrntion__c from Grant_Intervention__c where Performance_Framework__c =: performanceId' ) ){
                if(giObj.Catalog_Intervention__r.Is_Other_Intervrntion__c == false){
                    options.add(new selectOption(giObj.id, giObj.Name));
                }else{
                    if(giObj.Custom_Intervention_Name__c!= null){
                        options.add(new selectOption(giObj.id, giObj.Custom_Intervention_Name__c));
                    }
                }
            
            }
        }
    }
    public void refreshGrantInterventionList(){
        //options.clear();   
        options= new List<selectOption>();
        system.debug('@@ModuleNewId '+ModuleNewId);
        if(ModuleNewId!=null){
        for(Grant_Intervention__c gin:Database.query('SELECT Id,Name, Custom_Intervention_Name__c, Catalog_Intervention__r.Is_Other_Intervrntion__c from Grant_Intervention__c where Module__c=:modulenewId LIMIT 50000')){
            if(gin.Catalog_Intervention__r.Is_Other_Intervrntion__c == false){
                options.add(new selectOption(gin.id, gin.Name));
                }
            else if(gin.Custom_Intervention_Name__c!= null){
                options.add(new selectOption(gin.id, gin.Custom_Intervention_Name__c));
            }
        }
        }
        System.debug('In Refresh@@@');
    }
    
    public PageReference saveRecord(){     
     try{
        Performance_Framework__c prfObj = [select id, PF_status__c from Performance_Framework__c where id=:performanceId];
        Map<string,id> rtypeMap = new Map<string,id>();
        for(RecordType rtObj : [Select Id,Name FROM RecordType WHERE SobjectType='Key_Activity__c']){
            rtypeMap.put(rtObj.Name,rtObj.Id);
        }
        keyAcc.RecordTypeId = rtypeMap.get(prfObj.PF_Status__c);
        upsert keyAcc;       
        PageReference recDetail = new PageReference('/' + keyAcc.id);
        return recDetail;  
     }     
     catch (Exception ex) {
         ApexPages.addMessages(ex);
         return null;
     }
    }
    
    public PageReference cancelRecord(){
        PageReference prefDetail;
        if(keyAcc.id!= null){
            prefDetail = new PageReference('/'+keyAcc.id);
        }else{
            prefDetail = new PageReference('/apex/WPTMKeyActivityReport?id='+performanceId);
        }    
        return prefDetail ;
    }

}