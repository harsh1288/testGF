public with sharing class GMDetailedBudgetFlow_controller {

/*-----------START---------Variable Initialisation------------------------------*/
	Public String strSelectedInterventions {get;set;}
	Public String strSelectedCostInput {get;set;}
	Public String strSelectedCostGroup {get;set;}
	Public String strIntervention {get;set;}
	Public String strSelectedPayee {get;set;}
	Public boolean blnCostInput {get;set;}
    Public boolean blnCostGrouping  {get;set;}
	Public boolean blnmoduleIntervention {get;set;}
    Public boolean blnselectRecipients {get;set;}
    Public boolean blnshowAllReadOnly {get;set;}
    Public boolean blnshowAllEditable {get;set;}
    Public String strModuleId;
    Public String baseURL;
    Public String userTypeStr;
    Public String strImplementationPeriodId;
    Public  List<Module__c> lstModuleTemp {get;set;}
    Public IP_Detail_Information__c ipde {get;set;}
    public Date startDate {get; set;}
    public Date endDate {get; set;}
    Public  List<SelectOption> GrantInterventionOptions{get;set;}
    public Performance_Framework__c PF {get;set;}
    public List<SelectOption> CostInputOptions {get;set;}
    public Map<Id,String> MapCostInput {get;set;}
    public List<SelectOption> AllCostGrpOptions {get;set;}
    public List<SelectOption> ImplementersOptions {get;set;}
    Public String strComponent {get;set;}
    Public Grant_Intervention__c objGI;
    Public list<Grant_Intervention__c> lstGI;
    Public list<wrapperSelectCostInput> lstwrapSelectCostInput {get;set;}
    Public wrapperSelectCostInput objwrapSelectCostInput;
    Public list<wrapperSelectRecipient> lstwrapSelectPayee {get;set;}
    Public wrapperSelectRecipient objwrapSelectPayee;
   public list<Catalog_Cost_Input__c> lstccifinal ;
    public List<Catalog_Cost_Input__c> CatalogIC ; 
    public List<wrapperClassControllerAll> lstwrapAll;
    public Budget_line__c objBL;
    public wrapperClassControllerAll objwrapAll;
    
 /*-----------START---------Constructor Defined------------------------------*/
    public GMDetailedBudgetFlow_controller(ApexPages.StandardController controller) {
        blnCostInput = false;
        blnCostGrouping = false;
        blnmoduleIntervention = false;
        blnSelectRecipients = false;
        blnshowAllReadOnly = false;
        blnshowAllEditable = false;
        strModuleId = ApexPages.currentPage().getParameters().get('id');   //id='a1km0000000odGg'
        baseURL = URL.getSalesforceBaseUrl().toExternalForm();
        userTypeStr = UserInfo.getUserType();
        if(userTypeStr != 'Standard') {
            baseUrl = baseUrl+'/GM/';
        }
        else
            baseUrl = baseUrl+'/';
        //objModule = [SELECT id, Name, Implementation_Period__c FROM Module__c WHERE id =: pageId LIMIT 1];
        //objHPC = [SELECT id, Grant_Implementation_Period__c FROM HPC_Framework__c WHERE Grant_Implementation_Period__c =: objModule.Implementation_Period__c LIMIT 1];
        lstModuleTemp = new List<Module__c>();
        lstModuleTemp = [Select Implementation_Period__c,Implementation_Period__r.Start_Date__c,Implementation_Period__r.End_Date__c,Implementation_Period__r.Component__c,Implementation_Period__r.Budgeting_level__c
        				 From Module__c 
	                    Where id =: strModuleId And Implementation_Period__c != null];

            strImplementationPeriodId = lstModuleTemp[0].Implementation_Period__c;
            ipde = [Select Id,name,Budget_Status__c,Grant_Currency_Exchange_Rate__c,Exchange_rate__c,HPC_Amount__c,High_level_budget_GAC_1_EUR__c,High_level_budget_GAC_1_USD__c,Implementation_Period__c , Remaining_Budget_Amount__c ,Total_Approved_Budget__c ,Total_Budgeted_Amount__c from IP_Detail_Information__c where Implementation_Period__c =:strImplementationPeriodId ]; 
           // BudStatus = ipde.id;
           PF = [Select Id from Performance_Framework__c where Implementation_Period__c=: strImplementationPeriodId]; 
        //FillCostInput();
        SelectIntervention();
        strSelectedInterventions = '';
        strSelectedCostInput = '';
        startDate = lstModuleTemp[0].Implementation_Period__r.Start_Date__c;
        endDate = lstModuleTemp[0].Implementation_Period__r.End_Date__c;
        strComponent = lstModuleTemp[0].Implementation_Period__r.Component__c;
        
    }
    /*-----------FINISH---------Constructor Defined------------------------------*/
    /*-----------START---------Method to Cancel the job and go back to origin------------------------------*/
    Public Pagereference CancelPage(){
        Pagereference pr = new Pagereference(baseURL+'/apex/GMDetailedBudget?id='+strModuleId);
        pr.setRedirect(true);
        return pr;
    }
    /*-----------FINISH---------Method to Cancel the job and go back to origin------------------------------*/
    /*-----------START---------Method to Select Intervention------------------------------*/
    public void SelectIntervention(){   
    	blnCostInput = false;
        blnCostGrouping = false;
        blnmoduleIntervention = true;
        blnSelectRecipients = false;
        blnshowAllReadOnly = false;
        blnshowAllEditable = false;
    GrantInterventionOptions = new List<SelectOption>();
                GrantInterventionOptions.add(new SelectOption('','--None--'));      
                lstGI = [Select Id,Name,Custom_Intervention_Name__c From Grant_Intervention__c Where Module__c =: strModuleId AND Performance_Framework__c =:PF.id Order BY Name];
                if(lstGI.size() > 0){
                    for(Grant_Intervention__c objBL : lstGI){
                        if(objBL.Custom_Intervention_Name__c != null && objBL.Custom_Intervention_Name__c != ''){
                            GrantInterventionOptions.add(new SelectOption(objBL.Id,objBL.Custom_Intervention_Name__c));
                        }else{
                        GrantInterventionOptions.add(new SelectOption(objBL.Id,objBL.Name));
                        }
                    }
                }
    }
    /*-----------FINISH---------Method to Select Intervention------------------------------*/
    /******************************************* Selection Option for Cost Grouping ************/
                                                /************* By TCS ***********/
    Public void FillCostInput(){
    	if(strIntervention != '' && strIntervention != null){
            objGI = [SELECT id, Name, Module__r.Name FROM Grant_Intervention__c WHERE id =: strIntervention LIMIT 1];
            blnCostInput = true;
            blnCostGrouping = false;
            blnmoduleIntervention = false;
            blnSelectRecipients = false;
            blnshowAllReadOnly = false;
            blnshowAllEditable = false;
            strSelectedCostInput = '';
            strSelectedInterventions = objGI.Module__r.Name+' - '+objGI.Name;
            
        CostInputOptions = new List<SelectOption>();
        MapCostInput = new Map<Id,String>();
        List<Catalog_Cost_Input__c> lstCostInput = [Select Id,Name, Cost_Grouping__c, Unit_Cost_Definition__c 
                                            From Catalog_Cost_Input__c 
                                            Where PSM__c = false AND NAME!=: 'All' AND Is_Inactive__c =: False
                                            AND Disease_Impact__c INCLUDES(:strComponent) 
                                            Order by Cost_Input_Number__c];
        CostInputOptions.add(new SelectOption('','--None--'));
        if(lstCostInput.size() > 0){
            for(Catalog_Cost_Input__c objCI : lstCostInput){
                CostInputOptions.add(new SelectOption(objCI.Id,objCI.Name));
                MapCostInput.put(objCI.Id,objCI.Unit_Cost_Definition__c);
            }
            lstwrapSelectCostInput = new list<wrapperSelectCostInput>();
            if(lstGI.size()>0){  
                for(Catalog_Cost_Input__c objCI : lstCostInput){
                    objwrapSelectCostInput = new wrapperSelectCostInput(objCI);
                    lstwrapSelectCostInput.add(objwrapSelectCostInput);
                }
            }
        }
    	}else{
    	 	blnCostInput = false;
            blnCostGrouping = false;
            blnmoduleIntervention = true;
            blnSelectRecipients = false;
            blnshowAllReadOnly = false;
            blnshowAllEditable = false;
            if(lstGI.size() > 0){
                ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.Fatal,'Please Select an Intervention');
                ApexPages.addMessage(errorMsg);
            }
            else{
                ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.Warning,'No Intervention Available');
                ApexPages.addMessage(errorMsg);
            }
    	}
    }
     /*-----------FINISH---------Method to Fill Cost Input------------------------------*/
  
    Public void FillImplementers(){
        ImplementersOptions = new List<SelectOption>();
        ImplementersOptions.add(new SelectOption('','--None--'));
        List<Implementer__c> lstImplementer = [Select Id,Name, Implementer_Name__c From Implementer__c where Grant_Implementation_Period__c =: strImplementationPeriodId AND Performance_Framework__c =:PF.id];
        if(lstImplementer.size() > 0){
            for(Implementer__c objImp : lstImplementer){
                ImplementersOptions.add(new SelectOption(objImp.Id,objImp.Implementer_Name__c));
            }
        }
    }
     /*-----------FINISH---------Method to Fill Implementers------------------------------*/
      /*-----------START---------Method to load the fourth pageblock section for Intervention Selection------------------------------*/
    Public void InterventionSection(){
    	blnCostInput = false;
    	blnCostGrouping = false;
        blnmoduleIntervention = true;
        blnselectRecipients = false;
        blnshowAllReadOnly = false;
        blnshowAllEditable = false;
        strSelectedInterventions = '';
        strSelectedCostInput = '';
        
    }
    /*-----------FINISH---------Method to load the fourth pageblock section for Intervention Selection------------------------------*/
    /*-----------START---------Method to select the product categories------------------------------*/
    Public void selectedCostInput(){
    	
        Set<String> strSetUniquecgp = new Set<String>();
        String strUniquecgp;
        lstccifinal = new list<Catalog_Cost_Input__c>();
        for(wrapperSelectCostInput cci: lstwrapSelectCostInput){
            if(cci.selectedCstInp == true){
                blnCostInput = false;
                 blnCostGrouping = false;
                blnmoduleIntervention = false;
                blnSelectRecipients = true;
                blnshowAllReadOnly = false;
                blnshowAllEditable = false;
                strUniquecgp = '';
                strUniquecgp = cci.objCatCostWrap.Name + cci.objCatCostWrap.Cost_Grouping__c + cci.objCatCostWrap.Unit_Cost_Definition__c;
                strSetUniquecgp.add(strUniquecgp);
                System.debug('****strSetUniquecgp'+strSetUniquecgp);
                lstccifinal.add(cci.objCatCostWrap);
                lstwrapSelectPayee = new list<wrapperSelectRecipient>();
           	 	List<Implementer__c> lstImplementer = [Select Id,Name, Implementer_Name__c From Implementer__c where Grant_Implementation_Period__c =: strImplementationPeriodId AND Performance_Framework__c =:PF.id];
        		if(lstImplementer.size() > 0){
            		for(Implementer__c objImp : lstImplementer){
               			objwrapSelectPayee = new wrapperSelectRecipient(objImp.Implementer_Name__c);
               			objwrapSelectPayee.wrpSelectedCstInp = cci;
                    	lstwrapSelectPayee.add(objwrapSelectPayee);
                    	
                	}
        		}
        		 if(strSelectedCostInput == '')
                    strSelectedCostInput = cci.objCatCostWrap.Name;     
                else             
                    strSelectedCostInput += ('; ' + cci.objCatCostWrap.Name);
        		
           }
               
            
        }
        if(blnSelectRecipients == false ){
        	blnCostInput = true;
            blnCostGrouping = false;
            blnmoduleIntervention = false;
            blnSelectRecipients = false;
            blnshowAllReadOnly = false;
            blnshowAllEditable = false;
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.Fatal,'Please Select Atleast One Category Value');
            ApexPages.addMessage(errorMsg);
        }
    }
    /*-----------FINISH---------Method to select the product categories------------------------------*/
    /*-----------START--------------Method to select the all the selections from different objects------------------------------*/
    public void ShowAllSelection(){
        //lstrecfinal = new list<Implementer__c>();
        //lstgifinal = new list<Grant_Intervention__c>();
        //objGI = [SELECT id, Name, Module__r.Name FROM Grant_Intervention__c WHERE id =: strIntervention LIMIT 1];
       // lstcgpfinal = new list<Catalog_Cost_Input__c>();
        for(wrapperSelectRecipient lwp: lstwrapSelectPayee){      
            if(lwp.selectedPayee == true){
                blnCostInput = false;
                blnCostGrouping = false;
                blnmoduleIntervention = false; 
                blnSelectRecipients = false;
                blnshowAllReadOnly = true;
                blnshowAllEditable = false;
                if(strSelectedPayee == '')
                    strSelectedPayee = lwp.strSelectReciWrap;    
                else             
                    strSelectedPayee += ('; ' + lwp.strSelectReciWrap);
                }  
            }
            //lstgifinal.add(wc.objIntvWrap);   
            
        if(blnshowAllReadOnly == true){
            lstwrapAll = new list<wrapperClassControllerAll>(); 
            objBL = new Budget_line__c();
            
           /* for(String sc: strSelectedCostInput){
                for(wrapperSelectRecipient cci: lstwrapSelectPayee){ 
                        if(sc == cci.wrpSelectedCstInp.objCatCostWrap.Name){
                       objwrapAll = new wrapperClassControllerAll(sc,cci.wrpSelectedCstInp.objCatCostWrap,cci.wrpSelectedCstInp.objCatCostWrap.Name,objGI, cci.strSelectReciWrap, objBL);
                                        System.Debug('****objwrapAll:'+objwrapAll);
                                       lstwrapAll.add(objwrapAll);
                                }
                            }
            }
            System.Debug('****lstwrapAll'+lstwrapAll);
            System.Debug('****lstwrapAll size'+lstwrapAll.size());
            Integer intIndex = 1;
            for(Integer i=0; i<lstwrapAll.size(); i++){
                lstwrapAll[i].intTabIndexwrap = intIndex;
                
                intIndex++;
            }*/
        }
        //Check if any of the product name has been selected or not
        else{
        	blnCostInput = false;
            blnCostGrouping = false;
            blnmoduleIntervention = false;
            blnSelectRecipients = true;
            blnshowAllReadOnly = false;
            blnshowAllEditable = false;
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.Fatal,'Please Select Atleast One Product');
            ApexPages.addMessage(errorMsg);            
        }
    }
    /*-----------FINISH-------------Method to select the all the selections from different objects------------------------------*/
     /*-----------START---------Wrapper Class to bind Select Cost Input and Checkbox------------------------------*/
    public class wrapperSelectCostInput{
         public Catalog_Cost_Input__c objCatCostWrap {get; set;}
        public Boolean selectedCstInp {get; set;}
        public wrapperSelectCostInput(Catalog_Cost_Input__c p){
                this.objCatCostWrap = p;
                this.selectedCstInp = false;
        }
    }
    /*-----------FINISH---------Wrapper Class to bind Select Cost Input and Checkbox------------------------------*/
     /*-----------START---------Wrapper Class to bind Select Cost Input and Checkbox------------------------------*/
    public class wrapperSelectRecipient{
         public String strSelectReciWrap {get; set;}
        public Boolean selectedPayee {get; set;}
        public wrapperSelectCostInput wrpSelectedCstInp {Get; set;}
        public wrapperSelectRecipient(String p){
                this.strSelectReciWrap = p;
                this.selectedPayee = false;
        }
    }
    /*-----------FINISH---------Wrapper Class to bind Select Cost Input and Checkbox------------------------------*/
    /*********************START*********************Wrapper Class to bind all the selected values from all objects****************************/
    public class wrapperClassControllerAll{
        public Integer intTabIndexwrap {get;set;}
        public String strMainCategoryWrap {get; set;}
        public Catalog_Cost_Input__c objCatalogInpWrap {get; set;}
        Public String strCatalogCostInputWrap {get;set;}
        public Grant_Intervention__c objGrantInterventionvWrap {get; set;}
        public Implementer__c objRecipientWrap {get; set;}
        public Budget_Line__c objBLWrap {get;set;}
        public Boolean selectedValues {get; set;}
        public Boolean editableValues {get;set;}
        public Boolean deleteValues {get;set;}
        public Boolean copyValues {get;set;}
        //public Integer quantityQ1
        
        public wrapperClassControllerAll(String sm, Catalog_Cost_Input__c ci, String cciop, Grant_Intervention__c gi, Implementer__c imp, Budget_Line__c bl){
            this.strMainCategoryWrap = sm;
            this.objCatalogInpWrap = ci;
            this.strCatalogCostInputWrap = cciop;
            this.objGrantInterventionvWrap = gi;
            this.objRecipientWrap = imp;
            this.objBLWrap = bl;
            this.selectedValues = false;
            this.editableValues = false;
            this.deleteValues = false;
            this.copyValues = false;
        }
    }
    /*********************FINISH*********************Wrapper Class to bind all the selected values from all objects****************************/
}