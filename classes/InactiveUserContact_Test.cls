@isTest
public class InactiveUserContact_Test{

public static testMethod void updatingStatusOnAffiliation(){

Test.startTest();
Account objAccount = TestClassHelper.createAccount();
objAccount.Name = 'InactiveUserContact'; 
insert objAccount;

Contact objContact = TestClassHelper.createContact();
objContact.FirstName = 'Inactive';
objContact.LastName = 'UserContact';
objContact.AccountId = objAccount.Id;
objContact.External_User__c = true;
objContact.Email = 'InactiveUserContact@tgf.ex.com';
insert objContact;

list<npe5__Affiliation__c> AffAccCon = [select Id,npe5__Status__c from npe5__Affiliation__c where npe5__Organization__c =:objAccount.id AND npe5__Contact__c =:objContact.id];
AffAccCon[0].npe5__Status__c = 'former';
update AffAccCon[0];

npe5__Affiliation__c objAff = new npe5__Affiliation__c();
objAff.npe5__Organization__c = objAccount.Id;
objAff.npe5__Contact__c = objContact.Id;
objAff.npe5__Status__c = 'Current';
objAff.Access_Level__c = 'Admin';
objAff.RecordTypeId = label.CM_Affiliation_RT;
insert objAff;

Test.stopTest();


//delete objAff;
objAff.npe5__Status__c = 'Former';
update objAff;

//System.assertEquals(false, [Select id, isActive from User where contactId =: objContact.Id limit 1].isActive);

    }
    
public static testMethod void updatingExternalUserFlgOnContact(){
Test.startTest();
Account objAccount = TestClassHelper.createAccount();
objAccount.Name = 'InactiveUserContact'; 
insert objAccount;

Contact objContact = TestClassHelper.createContact();
objContact.FirstName = 'Inactive';
objContact.LastName = 'UserContact';
objContact.AccountId = objAccount.Id;
objContact.External_User__c = true;
objContact.Email = 'InactiveUserContact@tgf.ex.com';
insert objContact;

list<npe5__Affiliation__c> AffAccCon = [select Id,npe5__Status__c from npe5__Affiliation__c where npe5__Organization__c =:objAccount.id AND npe5__Contact__c =:objContact.id];
AffAccCon[0].npe5__Status__c = 'former';
update AffAccCon[0];

npe5__Affiliation__c objAff = new npe5__Affiliation__c();
objAff.npe5__Organization__c = objAccount.Id;
objAff.npe5__Contact__c = objContact.Id;
objAff.npe5__Status__c = 'Current';
objAff.Access_Level__c = 'Admin';
objAff.RecordTypeId = label.CM_Affiliation_RT;
insert objAff;

Test.stopTest();

objContact.External_User__c = false;
update objContact;

//System.assertEquals(false, [Select id, isActive from User where contactId =: objContact.Id limit 1].isActive);

}    
}