public with sharing class UploadDocumenttext_664074{
    Public DocumentUpload_GM__c objDocumentUpload {get;set;}   
    Public FeedItem objFeedItem {get;set;}
    Public boolean fileoverload {get;set;}
    public Integer fileSize {get;set;}
    public Id strImplementationPeriodId {get;set;}
    public String strWarning {get;set;}
    public boolean blnIsExtUser {get;set;}
    public RecordType internalRT {get;set;}
    public RecordType externalRT {get;set;}    
    public Profile profilename;   
    
    Public boolean blnvisibleall{get;set;}  
    Public boolean blnvisiblelfa{get;set;}  
    Public boolean blnvisibleexntrnl{get;set;} 
    
    public List<selectoption> securityoptions{get;set;} 
    
    public string  strsecurityoptn{get;set;}    
    public List<RecordType> lstrt{get;set;}
    
    public UploadDocumenttext_664074(ApexPages.StandardController controller) {                
        strImplementationPeriodId = ApexPages.currentpage().getparameters().get('id');
        System.debug('===Id==>'+strImplementationPeriodId);
        objDocumentUpload = new DocumentUpload_GM__c (Implementation_Period__c = strImplementationPeriodId,Process_Area__c = 'Grant-making');
        objFeedItem = new FeedItem();
        
        id s = Userinfo.getUserId();
        User objUser = [Select Id,Name,ContactId,ProfileId From User where Id =:s];
        profilename = [select Name from Profile where id =:objUser.ProfileId];
        if(objUser.ContactId!=null){
            blnIsExtUser = true;
            //externalRT = [SELECT Id from RecordType where name = 'Grant Making Public'  AND SobjectType ='DocumentUpload_GM__c ' limit 1];
        }
        else{
            blnIsExtUser = false;
            //internalRT = [SELECT Id from RecordType where name = 'Grant Making'  AND SobjectType ='DocumentUpload_GM__c ' limit 1];
        }
        checkprofile(); 
        fillsecurity();   
        /*objDocumentUpload = [Select Description__c,Implementation_Period__c,FeedItem_Id__c,Language__c,Language_Code__c,Process_Area__c,GMType__c,Type__c,Name
                             from DocumentUpload_GM__c  
                             where Id =: objDocumentUpload.Id];*/       
    }
    
    public void checkprofile(){
    	blnvisibleall = false;
    	blnvisiblelfa = false;
    	blnvisibleexntrnl = false;
    	
        List<Profile_Access_Setting__c> checkpage = [Select Salesforce_Item__c,Status__c from Profile_Access_Setting__c where Page_Name__c ='UploadDocument_664074' and (Profile_Name__c =: profilename.Name)];
        for (Profile_Access_Setting__c check : checkpage){
             if(check.Salesforce_Item__c == 'Visibletoall')blnvisibleall = true;
             if(check.Salesforce_Item__c == 'Visibletolfa')blnvisiblelfa = true;
             if(check.Salesforce_Item__c == 'Visibletoexternal')blnvisibleexntrnl = true;
        }
    }
    
    public void fillsecurity(){
        securityoptions = new List<Selectoption>(); 
        Schema.DescribeFieldResult fieldResult = DocumentUpload_GM__c.Security__c.getDescribe();
        system.debug('@@'+fieldResult);
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
       
        if(blnvisibleall){
           for(Schema.PicklistEntry f : ple){
               securityoptions.add(new SelectOption(f.getLabel(), f.getValue()));
           }
           objDocumentUpload.recordTypeId = Schema.SObjectType.DocumentUpload_GM__c.getRecordTypeInfosByName().get('Grant Making Public').getRecordTypeId();
        }
        
        if(blnvisiblelfa){
           securityoptions = new List<Selectoption>();
           integer i =0;
           
           for(Schema.PicklistEntry f : ple){
              if( f.getLabel().contains('Visible to all users') ){
              	securityoptions.add(new SelectOption(f.getLabel(), f.getValue()));
              }	
       		  if( f.getLabel().contains('Visible to LFA and TGF internal only')){
       		  	strsecurityoptn = f.getValue();
       		  	securityoptions.add(new SelectOption(f.getLabel(), f.getValue()));
       		  }	
           }
           objDocumentUpload.recordTypeId = Schema.SObjectType.DocumentUpload_GM__c.getRecordTypeInfosByName().get('Grant Making').getRecordTypeId();
                
        }
        if(blnvisibleexntrnl){
           securityoptions = new List<Selectoption>();
           for(Schema.PicklistEntry f : ple){
           	  if( f.getLabel().contains('Visible to all users') ){
              	securityoptions.add(new SelectOption(f.getLabel(), f.getValue()));
              }	
           }
           objDocumentUpload.recordTypeId = Schema.SObjectType.DocumentUpload_GM__c.getRecordTypeInfosByName().get('Grant Making').getRecordTypeId();     
        }
        
    }
    
    
    
    public PageReference uploadFile () {
        System.debug('In Upload  objDocumentUpload ' +objDocumentUpload +'Feed = '+ objFeedItem  );
        
        if( strsecurityoptn.contains('Visible to all users') ){
        	objDocumentUpload.recordTypeId = Schema.SObjectType.DocumentUpload_GM__c.getRecordTypeInfosByName().get('Grant Making Public').getRecordTypeId();	
        }else if(strsecurityoptn.contains('Visible to LFA and TGF internal only') ){
        	objDocumentUpload.recordTypeId = Schema.SObjectType.DocumentUpload_GM__c.getRecordTypeInfosByName().get('Grant Making').getRecordTypeId();		
        }else{
        	objDocumentUpload.recordTypeId = Schema.SObjectType.DocumentUpload_GM__c.getRecordTypeInfosByName().get('Grant Making').getRecordTypeId();	
        }
        
        if(objFeedItem != null){           
            system.debug('@#@#@#@#'+objFeedItem);
                       
            if(objFeedItem.ContentSize == Null || objFeedItem.ContentSize > 10485760){                              
                strWarning = 'This file exceeds the maximum size limit of 10MB.';
                return null; //pgrefnew;  
            }
            if(objFeedItem.ContentSize == 0)
            {   
                strWarning = 'File to be uploaded cannot be empty.';
                return null;                 
            }   
            
            try {
                if(objDocumentUpload != null)
                {
                    objDocumentUpload.File_Name__c = objFeedItem.ContentFileName;
                    objDocumentUpload.Description__c = objFeedItem.body;
                    objDocumentUpload.Security__c = strsecurityoptn;
                    /*for(RecordType obj:lstrt){
                    	system.debug('@@obj'+obj.Name);
                    	system.debug('@@obj'+blnvisibleall);
                        if(obj.Name == 'VisibletoALL' && blnvisibleall)
                           objDocumentUpload.RecordTypeId = obj.Id;
                        if(obj.Name == 'VIsibletoLFA' && blnvisiblelfa)
                           objDocumentUpload.RecordTypeId = obj.Id;
                        if(obj.Name == 'VisibletoExternal' && blnvisibleexntrnl)
                           objDocumentUpload.RecordTypeId = obj.Id;
                        system.debug('@@obj'+objDocumentUpload.RecordTypeId);
                    }*/
                    insert objDocumentUpload;
                    objFeedItem.ParentId = objDocumentUpload.Id;
                }
                
                objFeedItem.Type = 'ContentPost';
                objFeedItem.Visibility = 'AllUsers';
                insert objFeedItem;
                
                FeedItem lstFeedItem = [Select RelatedRecordId from FeedItem where id=:objFeedItem.Id];                
                system.debug(lstFeedItem);
                objDocumentUpload.FeedItem_Id__c = objFeedItem.ID;
                objDocumentUpload.DownloadId__c = lstFeedItem.RelatedRecordId;                                                                                 
                system.debug(objDocumentUpload.DownloadId__c);
                
                /*if(blnIsExtUser)
                    objDocumentUpload.RecordTypeId = externalRT.Id;
                else
                    objDocumentUpload.RecordTypeId = internalRT.Id;*/
                    
                update objDocumentUpload;
                    
                system.debug('@@objDocumentUpload@@'+objDocumentUpload);                    
                system.debug('objFeedItem '+objFeedItem);
            }                
            
            catch (Exception ex) {
                /*delete objDocumentUpload;
                delete objFeedItem;*/                
                System.debug(ex); 
            }                      
        }
        PageReference pgrefnew = new PageReference('/'+objDocumentUpload.Id);
        pgrefnew.setRedirect(true);
        return pgrefnew;
    }
    
    public PageReference returntoIP () {
        PageReference pgrefnew; 
        if(blnIsExtUser)
            pgrefnew = new PageReference('/GM/'+strImplementationPeriodId);
        else
            pgrefnew = new PageReference('/'+strImplementationPeriodId);
        pgrefnew.setRedirect(true);
        return pgrefnew;
    }
}