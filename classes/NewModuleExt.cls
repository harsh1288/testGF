public with sharing class NewModuleExt {

    public List<Module__c> lstmod = new List<Module__c>();
    public List<Catalog_Module__c> lstcatmod = new List<Catalog_Module__c>();
    public id cnid;
    public List<Concept_Note__c> lstcn = new List<Concept_Note__c>();
    public List<id> lstmodid = new List<id>();
    public List<wrapcatmod> lstwrapcatmod{get;set;}
    public string message{get;set;}
    public List<Catalog_Module__c> selectedcatmod = new List<Catalog_Module__c>();
    public List<Module__c> newlstmod = new List<Module__c>();    
    public List<RecordType> lstrec = new List<RecordType>();
    public List<RecordType> lstrecip = new List<RecordType>();
    public List<Implementation_Period__c> lstimp = new List<Implementation_Period__c>();
    public boolean display{get;set;}
    public boolean display1{get;set;}
    List<performance_framework__c> lstpf{get;set;}
    public string pfid{get;set;}
    public Boolean readOnlyUser{get;set;}
    public NewModuleExt(ApexPages.StandardSetController controller) {
           cnid=Apexpages.currentpage().getparameters().get('id');
            
            //For Concept Note
            lstcn  = [Select id,Component__c,Name from Concept_Note__c where id =:cnid];
            
            if(lstcn.size()>0){
                //getting list of Modules already presenteed for Concept Note
            lstmod = [select id,Name,Catalog_Module__c from MOdule__c where Concept_Note__c =:cnid ];
                  for(Module__c objmod:lstmod){
                       lstmodid.add(objmod.Catalog_Module__c);
                  }
                  //Catlog Modules of same components as Concept Note
            lstcatmod = [select Id,Name,Component__c, component_multi__c,French_Name__c,Spanish_Name__c,Russian_Name__c from Catalog_Module__c where component_multi__c INCLUDES (:lstcn[0].Component__c)  and 
                                        Id not in :lstmodid ];
            } 
            else if(lstcn.size() == 0){
              //To get Implementation period id from Performence framework
              lstpf = new List<Performance_framework__C>();
              lstpf = [select id,implementation_period__c from performance_framework__c where id =:cnid];
              pfid = lstpf[0].Id;
              //For Implementation Period  
               lstimp = [Select id,Component__c,Name,Principal_Recipient__c  from Implementation_Period__c where id =:lstpf[0].implementation_period__c ];
               
               readOnlyUser = false;
                User obj = [Select id, Name, ContactId from User where Id=:UserInfo.getUserId()];
                if( obj.ContactId != NULL ){
                  List<npe5__Affiliation__c> lstAff = [Select Access_Level__c, id,npe5__Contact__c,npe5__Organization__c FROM npe5__Affiliation__c where npe5__Contact__c=: obj.ContactId and npe5__Organization__c=:lstimp[0].Principal_Recipient__c and npe5__Status__c='Current' order by LastModifiedDate desc]; 
                  if( lstAff[0].Access_Level__c== 'Read'){
                       ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'You are not Allowed to Add Modules.');
                       ApexPages.addMessage(myMsg); 
                       readOnlyUser = true;
                       //return null;                      
                  }               
               }
               
               if(!readOnlyUser){
               //getting list of Modules already presenteed for Implementation Period             
               lstmod = [select id,Name,Catalog_Module__c from MOdule__c where Implementation_Period__c =:lstpf[0].implementation_period__c ];
               if(lstmod.size()>0){
                  for(Module__c objmod:lstmod){
                       lstmodid.add(objmod.Catalog_Module__c);
                  }
               }
             //Catlog Modules of same components as Grant making  
               lstcatmod = [select Id,Name,Component__c, component_multi__c,French_Name__c,Spanish_Name__c,Russian_Name__c from Catalog_Module__c where component_multi__c INCLUDES (:lstimp[0].Component__c)  and 
                                        Id not in :lstmodid ];
                   
            }
           }                           
          //Adding Catmodules to wrapper Class  
            lstwrapcatmod = new List<wrapcatmod>();
            if(lstcatmod.size()>0){
                display=false;
                display1 = true;
               for(Catalog_Module__c objcatmod:lstcatmod){
                   lstwrapcatmod.add(new wrapcatmod(objcatmod));  
               }
            }
            else{
                display= true;
                display1 = false;
              message ='No Modules available ';
            }
            
           //getting Recordtypeid of Module for Concept Note
            lstrec = [Select Id,SobjectType,Name From RecordType where Name = 'Concept Note' and SobjectType = 'Module__c'  limit 1];
           
           
            //getting Recordtypeid of Module for Implementation Period
            lstrecip=[Select Id,SobjectType,Name From RecordType where Name = 'Grant-Making' and SobjectType = 'Module__c'  limit 1];
    
            
        
    
     }
          /*public NewModuleExt(ApexPages.StandardController controller) {
            cnid=Apexpages.currentpage().getparameters().get('id');
            lstcn  = [Select id,Component__c,Name from Concept_Note__c where id =:cnid];
            
            lstmod = [select id,Name,Catalog_Module__c from MOdule__c where Concept_Note__c =:cnid ];
                  for(Module__c objmod:lstmod){
                       lstmodid.add(objmod.Catalog_Module__c);
                  }
            
            lstcatmod = [select Id,Name,Component__c, component_multi__c,French_Name__c,Spanish_Name__c,Russian_Name__c from Catalog_Module__c where component_multi__c INCLUDES (:lstcn[0].Component__c) and 
                                        Id not in :lstmodid ];
            
            lstwrapcatmod = new List<wrapcatmod>();
            if(lstcatmod.size()>0){
               for(Catalog_Module__c objcatmod:lstcatmod){
                   lstwrapcatmod.add(new wrapcatmod(objcatmod));  
               }
            }
            else{
              message ='No Modules Presented';
            }
            
          }*/
          
          public pageReference save(){
            // Getting selected Modules
              for(wrapcatmod objwrap : lstwrapcatmod){
                if(objwrap.selected == true){
                    selectedcatmod.add(objwrap.wrapcatmod);
                }
              }
               if(selectedcatmod.size()==0){
                 ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please select atleast one Module');
                 ApexPages.addMessage(myMsg); 
                 return null;
               }
         else{
            //For Concept Note
              if(lstcn.size() > 0){
                //creating Modules
              for(Catalog_Module__c objcatmod:selectedcatmod){
                 Module__c objmod= new Module__c();
                    objmod.Catalog_Module__c = objcatmod.id;
                    objmod.Component__c = lstcn[0].Component__c;
                    objmod.Name = objcatmod.name;
                    objmod.Russian_Name__c = objcatmod.Russian_Name__c;
                    objmod.French_Name__c = objcatmod.French_Name__c;
                    objmod.Spanish_Name__c =objcatmod.Spanish_Name__c;
                    objmod.Concept_Note__c = cnid;
                    objmod.RecordTypeId = lstrec[0].Id;
                    newlstmod.add(objmod);
              }
              insert newlstmod;
              }
              //For Implementation Period
              else if(lstimp.size()>0){
                    //creating Modules
                   for(Catalog_Module__c objcatmod:selectedcatmod){
                    Module__c objmod= new Module__c();
                    objmod.Catalog_Module__c = objcatmod.id;
                    objmod.Component__c = lstimp[0].Component__c;
                    objmod.Name = objcatmod.name;
                    objmod.Russian_Name__c = objcatmod.Russian_Name__c;
                    objmod.French_Name__c = objcatmod.French_Name__c;
                    objmod.Spanish_Name__c =objcatmod.Spanish_Name__c;
                    objmod.Implementation_Period__c = lstimp[0].id;
                    objmod.RecordTypeId = lstrecip[0].Id;
                    objmod.Performance_Framework__c = lstpf[0].id; 
                    newlstmod.add(objmod);
              }
              insert newlstmod;
              }
              pageReference pr = new pageReference('/'+newlstmod[0].Performance_Framework__c );
              pr.setRedirect(true);
              return pr;
           }
              
              
             
          }
          
          public class wrapcatmod{
            public Catalog_Module__c wrapcatmod{get;set;}
            public boolean selected{get;set;}
                public wrapcatmod(Catalog_Module__c objcatmod){
                       wrapcatmod = objcatmod;
                       selected  = false;
                }
            
          }
}