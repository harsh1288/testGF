public with sharing class TranslateGoalClass {

    public string lang{get;set;}
    public string language{get;set;}
 // public List<Goals_Objectives__c> goalRec{get;set;}
    public Goals_Objectives__c goalRec{get;set;}
    Id recordId{get;set;}
    
    public TranslateGoalClass(ApexPages.StandardController controller) {
    
       goalRec = (Goals_Objectives__c)controller.getRecord();
     //recordId = ApexPages.currentPage().getParameters().get('Id');
     system.debug('@@recordId '+recordId ); 
     goalRec = [Select id,Goal__c, Goal_French__c,Goal_Russian__c,Goal_Spanish__c,Implementer_Comments__c, PR_Comments_Spanish__c,PR_Comments_French__c,PR_Comments_Russian__c,Global_Fund_Comments_to_Implementer__c,Global_Fund_Comments_to_PR_Russian__c,Global_Fund_Comments_to_PR_Spanish__c,Global_Fund_Comments_to_PR_French__c from Goals_Objectives__c where id=:goalRec.Id limit 1]; 
    
     if(UserInfo.getLanguage()=='fr')
         {
           lang='fr';
           language='French';
         }
        else if(UserInfo.getLanguage() =='es'){
            lang='es';
            language='Spanish'; 
           }
        else if(UserInfo.getLanguage() =='ru'){
           lang='ru';
           language='Russian';
          }
        else{
           language='English';
           lang='en';
         }
  
     }
  
    public pagereference closeWindow()
      {
         Pagereference page = new Pagereference('/'+goalRec.Id);
         page.setRedirect(true);
         return page;
      }

  }