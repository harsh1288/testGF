public class ImpTrgRecursionController{

private static boolean alreadyRunBeforeUpdateImp = false;
private static boolean alreadyRunAfterImp = false;
private static boolean alreadyRunAfterUpdateImp = false; 

  /*****************************************************
     To Stop recursion of Implementation Trigger
 *****************************************************/
    public static boolean hasAlreadyRunBeforeUpdateImp() 
    {
      return alreadyRunBeforeUpdateImp ;
    }
    public static void setAlreadyRunBeforeUpdateImp() 
    {
        alreadyRunBeforeUpdateImp = true;
    }
    // below method used in test class
    public static void resetAlreadyRunBeforeUpdateImp() 
    {
        alreadyRunBeforeUpdateImp = false;
    }
    public static boolean hasAlreadyRunAfterImp() 
    {
      return alreadyRunAfterImp ;
    }
    public static void setAlreadyRunAfterImp() 
    {
        alreadyRunAfterImp = true;
    }
    // added for test class
    public static void resetAlreadyRunAfterImp() 
    {
        alreadyRunAfterImp = false;
    }
    public static boolean hasAlreadyRunAfterUpdateImp() 
    {
      return alreadyRunAfterUpdateImp ;
    }
    public static void setAlreadyRunAfterUpdateImp() 
    {
        alreadyRunAfterUpdateImp = true;
    }    
}