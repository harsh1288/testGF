public class StopRecursionCRM{

private static boolean alreadyRunBeforeCon = false;
private static boolean alreadyRunAfterCon = false; 
private static boolean alreadyRunBeforeInsertCon = false;
private static boolean alreadyRunBeforeUpdateCon = false;
private static boolean alreadyRunAfterInsertCon = false;
private static boolean alreadyRunAfterUpdateCon = false; 

/*****************************************************
 * adding booleans for trigger recursion control
 * ***************************************************/
  /*****************************************************
     To Stop recursion of Contact Trigger
 *****************************************************/
    public static boolean hasAlreadyRunBeforeCon() 
    {
      return alreadyRunBeforeCon ;
    }
    public static void setAlreadyRunBeforeCon() 
    {
        alreadyRunBeforeCon = true;
    }
    public static boolean hasAlreadyRunAfterCon() 
    {
      return alreadyRunAfterCon ;
    }
    public static void setAlreadyRunAfterCon() 
    {
        alreadyRunAfterCon = true;
    }
    public static boolean hasAlreadyRunBeforeInsertCon() 
    {
      return alreadyRunBeforeInsertCon ;
    }
    public static void setAlreadyRunBeforeInsertCon() 
    {
        alreadyRunBeforeInsertCon = true;
    }
    public static boolean hasAlreadyRunBeforeUpdateCon() 
    {
      return alreadyRunBeforeUpdateCon ;
    }
    public static void setAlreadyRunBeforeUpdateCon() 
    {
        alreadyRunBeforeUpdateCon = true;
    }
    public static boolean hasAlreadyRunAfterInsertCon() 
    {
      return alreadyRunAfterInsertCon ;
    }
    public static void setAlreadyRunAfterInsertCon() 
    {
        alreadyRunAfterInsertCon = true;
    }
    public static boolean hasAlreadyRunAfterUpdateCon() 
    {
      return alreadyRunAfterUpdateCon ;
    }
    public static void setAlreadyRunAfterUpdateCon() 
    {
        alreadyRunAfterUpdateCon = true;
    }
 
 

 //-------------------------------------------------------------------------
}