/*
* $Author:      $ TCS - Rahul Kumar
* $Description: $ Class for controlling new Grant Disaggregated create event redirection
* $Date:        $ 13-Nov-2014 
* $Revision:    $
*/

public class saveAndNewGrantDisagController{

    private Grant_Disaggregated__c gd;
    private Map<String, String> keyPrefixMap;
    public String backUrl{get;set;}
    public saveAndNewGrantDisagController(ApexPages.StandardController stdController){
        this.gd = (Grant_Disaggregated__c)stdController.getRecord();
        backUrl = '';         
    }
    
     public PageReference createNewGDurl()
     {
            boolean statusPF = saveandNewHandler.pfTrue('Grant_Disaggregated__c');
            keyPrefixMap =  saveandNewHandler.schemaMap();
            String baseUrl;
            string userTypeStr = UserInfo.getUserType();
            baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
            if(userTypeStr == 'Standard') 
            {
                baseUrl = baseUrl+'/';
            }
            else
            { 
                baseUrl = baseUrl+'/GM/';
            }
           if(gd.Grant_Indicator__c !=null)
           {
                // Creating url according to user type when Save and New is clicked while editing a record
            //    List<Performance_Framework__c> s = new List<Performance_Framework__c>([Select Name from Performance_Framework__c where Id  =:gd.Performance_Framework__c]);
                List<Grant_Indicator__c> s = new List<Grant_Indicator__c>([Select Performance_Framework__r.Name from Grant_Indicator__c where Id  =:gd.Grant_Indicator__c]);
                baseUrl += String.valueOf(keyPrefixMap.get('Grant_Disaggregated__c'));
                baseUrl += '/e?nooverride=1';
                if(s.size()>0)
                {
                    baseUrl += '&';
                    baseUrl += Label.Grant_Dis_PF_Lkup;
                    baseUrl += '=';
                    baseUrl += s[0].Performance_Framework__r.Name;            
                    baseUrl += '&retURL=';
                    baseUrl += gd.Grant_Indicator__c;
                }
                PageReference saveNewUrl = new PageReference(baseUrl);
                saveNewUrl.setRedirect(true);                
                return saveNewUrl;            
           }
           else
           {
               // Creating url according to user type when Save and New is clicked while creating new record
               if(userTypeStr == 'Standard') 
                {                    
                }
                else
                { 
                    if(ApexPages.currentPage().getParameters().get('retUrl').indexOf('GM') > -1)
                    {
                    
                    }
                    else
                    {
                        backUrl += +'/GM';
                    }
                }            
               backUrl += ApexPages.currentPage().getParameters().get('retUrl');
               if(backUrl.contains('?'))
               {
                   backUrl += '&nooverride=1';
               }
               else
               {
                   backUrl += '?nooverride=1';
               }               
               ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Record has been saved <br/> Can\'t use Save and New feature, Please <a id="goback" href="'+backUrl+'"><b>Go back</b></a>'));                           
               return null;
           }         
     }
}