@isTest
Public class Test_Help_Guidance{
    public static TestMethod void Help_Guidance1(){
    
      Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
      User user1 = new User(Alias = 'standt', Email='standarduser122@testorg.com', 
      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='fr', 
      LocaleSidKey='en_US', ProfileId = p.Id, 
      TimeZoneSidKey='America/Los_Angeles', UserName='standarduser122@testorg.com');
        
        Guidance__c GIPguidance = TestClassHelper.createGuidance();
        GIPguidance.Name = 'Grant-Making Overview';
        insert GIPguidance;
        
        Guidance__c GMCPguidance = TestClassHelper.createGuidance();
        GMCPguidance.Name = 'GrantMultiCountry';
        insert GMCPguidance;
        
        Guidance__c DBguidance = TestClassHelper.createGuidance();
        DBguidance.Name = 'Guidance for DB RL';
        insert DBguidance;
      
        Guidance__c GDguidance = TestClassHelper.createGuidance();
        GDguidance.Name = 'Grant Disaggregation Guidance';
        insert GDguidance;

        Guidance__c DocUploadGuidance= TestClassHelper.createGuidance();
        DocUploadGuidance.Name = Label.Guidance_for_documents_related_list;
        insert DocUploadGuidance;
        
        Guidance__c Goal_Guidance= TestClassHelper.createGuidance();
        Goal_Guidance.Name = 'GM Goals and Objectives';
        insert Goal_Guidance;
        
        Guidance__c objective_Guidance= TestClassHelper.createGuidance();
        objective_Guidance.Name = 'GM indicator related list';
        insert objective_Guidance;
        
        Guidance__c guidance_coverage= TestClassHelper.createGuidance();
        guidance_coverage.Name = 'GM Coverage & Output Indicators';
        insert guidance_coverage;
     
        Guidance__c guidance_Module= TestClassHelper.createGuidance();
        guidance_Module.Name = 'GM Modules related list';
        insert guidance_Module;
        
        Guidance__c guidance_GrntIntervention= TestClassHelper.createGuidance();
        guidance_GrntIntervention.Name = 'GM Interventions related list';
        insert guidance_GrntIntervention;
        
        Guidance__c guidance_PerformanceFramewrk= TestClassHelper.createGuidance();
        guidance_PerformanceFramewrk.Name = 'Performance Framework Detail';
        insert guidance_PerformanceFramewrk;
        
        Guidance__c guidance_RP= TestClassHelper.createGuidance();
        guidance_RP.Name = 'GM Reporting Periods';
        insert guidance_RP;
        
        Guidance__c guidance_outcome= TestClassHelper.createGuidance();
        guidance_outcome.Name = 'Grant-Making Home';
        insert guidance_outcome;
        
        Guidance__c guidance_Recipient= TestClassHelper.createGuidance();
        guidance_Recipient.Name = 'Add Recipient';
        insert guidance_Recipient;
        
        Guidance__c guidance_HPC= TestClassHelper.createGuidance();
        guidance_HPC.Name = Label.Guidance_for_HPC_RL;
        insert guidance_HPC;
        
        Guidance__c guidance_GIJ= TestClassHelper.createGuidance();
        guidance_GIJ.Name = 'Indicator Goal Junction Guidance';
        insert guidance_GIJ;
        
        System.runAs(user1) {
        Help_Detailed_Budget Help_GMD = new Help_Detailed_Budget();
        Help_GMD.Detailed_Budget();
        
        Help_Grant_Disaggregated Help_GD = new Help_Grant_Disaggregated();
        Help_GM_MultiCountry  Help_GrntMultiCountry = new Help_GM_MultiCountry();
        Help_Guidance_Document_Upload Help_DocUpload = new Help_Guidance_Document_Upload();
        Help_Guidance_Goals_Objectives Help_GoalObjective = new Help_Guidance_Goals_Objectives();
        Help_Guidance_Grant_Indicator Help_GrantIndicator = new Help_Guidance_Grant_Indicator();
        Help_Guidance_Grant_Intervention Help_GrantIntervention = new Help_Guidance_Grant_Intervention();
        Help_Guidance_Period Help_Reportingperiod = new Help_Guidance_Period();
        Help_Guidance_Implementation_Period Help_IP = new Help_Guidance_Implementation_Period();
        Help_Guidance_Module Help_Module = new Help_Guidance_Module();
        Help_Guidance_Recipient Help_Recipient = new Help_Guidance_Recipient();
        Help_HPC_Framework Help_HPC = new Help_HPC_Framework();
        Help_Indicator_Goal_Junction Help_IndGoalJnx = new Help_Indicator_Goal_Junction();
        Help_Guidance_Performance Help_performance = new Help_Guidance_Performance();
        }
    }
      public static TestMethod void Help_Guidance2(){
    
      Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
      User user1 = new User(Alias = 'standt', Email='standarduser2132@testorg.com', 
      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='ru', 
      LocaleSidKey='en_US', ProfileId = p.Id, 
      TimeZoneSidKey='America/Los_Angeles', UserName='standarduser2132@testorg.com');
      
        Guidance__c GIPguidance = TestClassHelper.createGuidance();
        GIPguidance.Name = 'Grant-Making Overview';
        insert GIPguidance;
        
        Guidance__c GMCPguidance = TestClassHelper.createGuidance();
        GMCPguidance.Name = 'GrantMultiCountry';
        insert GMCPguidance;
        
        Guidance__c DBguidance = TestClassHelper.createGuidance();
        DBguidance.Name = 'Guidance for DB RL';
        insert DBguidance;
      
        Guidance__c GDguidance = TestClassHelper.createGuidance();
        GDguidance.Name = 'Grant Disaggregation Guidance';
        insert GDguidance;

        Guidance__c DocUploadGuidance= TestClassHelper.createGuidance();
        DocUploadGuidance.Name = Label.Guidance_for_documents_related_list;
        insert DocUploadGuidance;
        
        Guidance__c Goal_Guidance= TestClassHelper.createGuidance();
        Goal_Guidance.Name = 'GM Goals and Objectives';
        insert Goal_Guidance;
        
        Guidance__c objective_Guidance= TestClassHelper.createGuidance();
        objective_Guidance.Name = 'GM indicator related list';
        insert objective_Guidance;
        
        Guidance__c guidance_coverage= TestClassHelper.createGuidance();
        guidance_coverage.Name = 'GM Coverage & Output Indicators';
        insert guidance_coverage;
      
        Guidance__c guidance_Module= TestClassHelper.createGuidance();
        guidance_Module.Name = 'GM Modules related list';
        insert guidance_Module;
        
        Guidance__c guidance_GrntIntervention= TestClassHelper.createGuidance();
        guidance_GrntIntervention.Name = 'GM Interventions related list';
        insert guidance_GrntIntervention;
        
        Guidance__c guidance_PerformanceFramewrk= TestClassHelper.createGuidance();
        guidance_PerformanceFramewrk.Name = 'Performance Framework Detail';
        insert guidance_PerformanceFramewrk;
        
        Guidance__c guidance_RP= TestClassHelper.createGuidance();
        guidance_RP.Name = 'GM Reporting Periods';
        insert guidance_RP;
        
        Guidance__c guidance_outcome= TestClassHelper.createGuidance();
        guidance_outcome.Name = 'Grant-Making Home';
        insert guidance_outcome;
        
        Guidance__c guidance_Recipient= TestClassHelper.createGuidance();
        guidance_Recipient.Name = 'Add Recipient';
        insert guidance_Recipient;
        
        Guidance__c guidance_HPC= TestClassHelper.createGuidance();
        guidance_HPC.Name =  Label.Guidance_for_HPC_RL;
        insert guidance_HPC;
        
        Guidance__c guidance_GIJ= TestClassHelper.createGuidance();
        guidance_GIJ.Name = 'Indicator Goal Junction Guidance';
        insert guidance_GIJ;
        
        System.runAs(user1) {
        Help_Detailed_Budget Help_GMD = new Help_Detailed_Budget();
        Help_GMD.Detailed_Budget();
        
        Help_Grant_Disaggregated Help_GD = new Help_Grant_Disaggregated();
        Help_GM_MultiCountry  Help_GrntMultiCountry = new Help_GM_MultiCountry();
        Help_Guidance_Document_Upload Help_DocUpload = new Help_Guidance_Document_Upload();
        Help_Guidance_Goals_Objectives Help_GoalObjective = new Help_Guidance_Goals_Objectives();
        Help_Guidance_Grant_Indicator Help_GrantIndicator = new Help_Guidance_Grant_Indicator();
        Help_Guidance_Grant_Intervention Help_GrantIntervention = new Help_Guidance_Grant_Intervention();
        Help_Guidance_Period Help_Reportingperiod = new Help_Guidance_Period();
        Help_Guidance_Implementation_Period Help_IP = new Help_Guidance_Implementation_Period();
        Help_Guidance_Module Help_Module = new Help_Guidance_Module();
        Help_Guidance_Recipient Help_Recipient = new Help_Guidance_Recipient();
        Help_HPC_Framework Help_HPC = new Help_HPC_Framework();
        Help_Indicator_Goal_Junction Help_IndGoalJnx = new Help_Indicator_Goal_Junction();
        Help_Guidance_Performance Help_performance = new Help_Guidance_Performance();
        }
    }
    public static TestMethod void Help_Guidance3(){
    
      Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
      User user1 = new User(Alias = 'standt', Email='standard1456user4@testorg.com', 
      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='es', 
      LocaleSidKey='en_US', ProfileId = p.Id, 
      TimeZoneSidKey='America/Los_Angeles', UserName='standard1456user4@testorg.com');
      
        Guidance__c GIPguidance = TestClassHelper.createGuidance();
        GIPguidance.Name = 'Grant-Making Overview';
        insert GIPguidance;
        
        Guidance__c GMCPguidance = TestClassHelper.createGuidance();
        GMCPguidance.Name = 'GrantMultiCountry';
        insert GMCPguidance;
        
        Guidance__c DBguidance = TestClassHelper.createGuidance();
        DBguidance.Name = 'Guidance for DB RL';
        insert DBguidance;
      
        Guidance__c GDguidance = TestClassHelper.createGuidance();
        GDguidance.Name = 'Grant Disaggregation Guidance';
        insert GDguidance;

        Guidance__c DocUploadGuidance= TestClassHelper.createGuidance();
        DocUploadGuidance.Name = Label.Guidance_for_documents_related_list;
        insert DocUploadGuidance;
        
        Guidance__c Goal_Guidance= TestClassHelper.createGuidance();
        Goal_Guidance.Name = 'GM Goals and Objectives';
        insert Goal_Guidance;
        
        Guidance__c objective_Guidance= TestClassHelper.createGuidance();
        objective_Guidance.Name = 'GM indicator related list';
        insert objective_Guidance;
        
        Guidance__c guidance_coverage= TestClassHelper.createGuidance();
        guidance_coverage.Name = 'GM Coverage & Output Indicators';
        insert guidance_coverage;
      
        Guidance__c guidance_Module= TestClassHelper.createGuidance();
        guidance_Module.Name = 'GM Modules related list';
        insert guidance_Module;
        
        Guidance__c guidance_GrntIntervention= TestClassHelper.createGuidance();
        guidance_GrntIntervention.Name = 'GM Interventions related list';
        insert guidance_GrntIntervention;
        
        Guidance__c guidance_PerformanceFramewrk= TestClassHelper.createGuidance();
        guidance_PerformanceFramewrk.Name = 'Performance Framework Detail';
        insert guidance_PerformanceFramewrk;
        
        Guidance__c guidance_RP= TestClassHelper.createGuidance();
        guidance_RP.Name = 'GM Reporting Periods';
        insert guidance_RP;
        
        Guidance__c guidance_outcome= TestClassHelper.createGuidance();
        guidance_outcome.Name = 'Grant-Making Home';
        insert guidance_outcome;
        
        Guidance__c guidance_Recipient= TestClassHelper.createGuidance();
        guidance_Recipient.Name = 'Add Recipient';
        insert guidance_Recipient;
        
        Guidance__c guidance_HPC= TestClassHelper.createGuidance();
        guidance_HPC.Name =  Label.Guidance_for_HPC_RL;
        insert guidance_HPC;
        
        Guidance__c guidance_GIJ= TestClassHelper.createGuidance();
        guidance_GIJ.Name = 'Indicator Goal Junction Guidance';
        insert guidance_GIJ;
        
        System.runAs(user1) {
        Help_Detailed_Budget Help_GMD = new Help_Detailed_Budget();
        Help_GMD.Detailed_Budget();
        
        Help_Grant_Disaggregated Help_GD = new Help_Grant_Disaggregated();
        Help_GM_MultiCountry  Help_GrntMultiCountry = new Help_GM_MultiCountry();
        Help_Guidance_Document_Upload Help_DocUpload = new Help_Guidance_Document_Upload();
        Help_Guidance_Goals_Objectives Help_GoalObjective = new Help_Guidance_Goals_Objectives();
        Help_Guidance_Grant_Indicator Help_GrantIndicator = new Help_Guidance_Grant_Indicator();
        Help_Guidance_Grant_Intervention Help_GrantIntervention = new Help_Guidance_Grant_Intervention();
        Help_Guidance_Period Help_Reportingperiod = new Help_Guidance_Period();
        Help_Guidance_Implementation_Period Help_IP = new Help_Guidance_Implementation_Period();
        Help_Guidance_Module Help_Module = new Help_Guidance_Module();
        Help_Guidance_Recipient Help_Recipient = new Help_Guidance_Recipient();
        Help_HPC_Framework Help_HPC = new Help_HPC_Framework();
        Help_Indicator_Goal_Junction Help_IndGoalJnx = new Help_Indicator_Goal_Junction();
        Help_Guidance_Performance Help_performance = new Help_Guidance_Performance();
        }
    }
}