Public Class extensionDocumentUpload
{
    
    /*Public FeedItem objFeedItem {get;set;}
    Public String FeedBody {get;set;}
    public integer linkedfilesize {get;set;}
    Public DocumentUpload_GM__c  objDocumentUpload {get; set;}
    public Boolean refreshPage {get;set;}
    public Boolean showempty {get;set;}
    public Boolean showsize {get;set;}
    public Boolean success {get;set;}
    Public Boolean blnIsExtUser{get;set;}
    public Boolean finalVer{get;Set;}        
    public boolean showUploadSection{get;set;}
    public extensionDocumentUpload(ApexPages.StandardController controller) 
    {
        
        this.objDocumentUpload = (DocumentUpload_GM__c )controller.getRecord(); 
        if(objDocumentUpload!= NULL) 
            objDocumentUpload = [select CreatedBy.Id, Hide_Section__c,Final_detailed_budget__c,Final_list_of_health_products_and_costs__c, Implementation_Period__r.Id from DocumentUpload_GM__c  where id=:objDocumentUpload.Id ];        
        if(String.isBlank(objDocumentUpload.Id) == false)
        {
            objFeedItem = new FeedItem(ParentId =objDocumentUpload.Id , Type='ContentPost');
        }
        FeedBody = null;
        refreshPage = false;
        showsize = false;
        showempty= false;
        System.debug('-----'+UserInfo.getUserId()+'==>'+objDocumentUpload.createdBy.Id);
        showUploadSection = false;
        Id profileId=userinfo.getProfileId();
        String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
        if( UserInfo.getUserId() == objDocumentUpload.createdBy.Id || profileName == 'Super User' || profileName == 'System Administrator')
        {
            showUploadSection = true;
        }
        if(( objDocumentUpload.Final_detailed_budget__c || objDocumentUpload.Final_list_of_health_products_and_costs__c) )
        {
            finalVer = true;
            
        }
        else
        {
            finalVer = false;
        }  
        System.debug('--finalVer ---'+finalVer);
        id s = Userinfo.getUserId();
        User objUser = [Select Id,Name,ContactId From User where Id =:s];
        if(objUser.ContactId!=null){
        blnIsExtUser = true;            
        }
        else{
         blnIsExtUser = false;
        }
    }

    Public void uploadFile()
    {
        if(objFeedItem != null)
        {
            if(linkedfilesize > 10485760 || linkedfilesize == null)
            {   
                showempty=false;
                showsize =true;                            
            }
            else if(linkedfilesize == 0)
            {   
                showempty=true;
                showsize = false;                 
            }
            else
            {
            System.debug('sizeelse@@@@@@'+linkedfilesize);
                //objFeedItem.ContentDescription = 'Other Document'; 
                List<FeedItem> feedItemToDel = [Select id from FeedItem where ParentId=: objDocumentUpload.id];
                if(feedItemToDel.size()>0)
                {
                    delete feedItemToDel;
                }
                
                  try 
                {
                  objFeedItem.ContentDescription = 'Other Document';
                  objFeedItem.Type='ContentPost';
                  objFeedItem.ParentId = objDocumentUpload.Implementation_Period__r.id;
                  objFeedItem.Visibility = 'AllUsers';
                  insert objFeedItem;
                } catch (DMLException e) {
                  ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading file'));
                }
                success = true;                                 
                FeedItem lstFeedItem = [Select RelatedRecordId from FeedItem where id=:objFeedItem.Id limit 1];
                if(objDocumentUpload != null)
                {
                    objDocumentUpload.FeedItem_Id__c = objFeedItem.ID;
                    objDocumentUpload.DownloadId__c=lstFeedItem.RelatedRecordId;
                    objDocumentUpload.Hide_Section__c= true;
                    objDocumentUpload.File_Name__c = objFeedItem.ContentFileName;
                    update objDocumentUpload;
                    system.debug('@@objDocumentUpload@@'+objDocumentUpload);
                    objFeedItem = new FeedItem(ParentId = objDocumentUpload.Id, Type='ContentPost', Visibility='AllUsers');
                    system.debug('objFeedItem '+objFeedItem);
                }
                FeedBody = null;                
            }
            refreshPage = true;
        }               
    }*/
    
}