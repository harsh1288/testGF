@isTest(seealldata=false)
public class ctrlReportingPeriodsTest{

        
public static testMethod void myctrlReportingPeriodsTest(){


 User objUser = [Select ID, country from User where id=:userinfo.getUserId()];
     
        /*Country__c objCountry = new Country__c();
        objCountry.Name = 'test';
        objCountry.FPM__c = objUser.id;
        objCountry.Country_Fiscal_Cycle_Start_Date__c = Date.today();
        objCountry.Country_Fiscal_Cycle_End_Date__c = Date.today()+12;
        insert objCountry;
        //static Country__c objCountry;
        //objCountry=TestClassHelper.insertCountry();
        //Account objAcc = TestClassHelper.insertAcc();
        //Grant__c objGrant = TestClassHelper.insertGrant();
        
        Account objAcc = new Account();
        objAcc.Name = 'Test Acc';
        objAcc.Short_Name__c = 'MOH';
        objAcc.Country__c = objCountry.Id;
        insert objAcc;
        
        Grant__c objGrant = new Grant__c();
        objGrant.Disease_Component__c =  'Malaria';
        objGrant.Name = 'Test';
        objGrant.Principal_Recipient__c = objAcc.Id;
        insert objGrant;
        
        Implementation_Period__c objIP = new Implementation_Period__c();
        objIP.Principal_Recipient__c = objAcc.Id;
        objIP.Grant__c = objGrant.Id;
        objIP.Start_Date__c =Date.today();
        objIP.End_Date__c =Date.today().addYears(3);
        objIP.Length_Years__c ='3';
        //objIP.Principal_Recipient__r.Country__r.Country_Fiscal_Cycle_Start_Date__c = Date.today();
        //objIP.Principal_Recipient__r.Country__r.Country_Fiscal_Cycle_End_Date__c = Date.today()+12;     
        insert objIP;
        List<Implementation_Period__c>lstIP = new List<Implementation_Period__c>();
        lstIP.add(objIP);
        system.debug('@@list IP@@'+lstIP);
        
        Page__c objPage =  TestClassHelper.insertPage();
            
        Page__c objPage = new Page__c();
        objPage.Implementation_Period__c = objIP.Id;
        insert objPage;
        
        Period__c per = new Period__c();
        per.Implementation_Period__c = objIP.Id;
        per.Start_Date__c = Date.today();
        insert per;*/
        
        Account objAcc = TestClassHelper.insertAccount();
        
        Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
        insert objGrant;
       
        Implementation_Period__c objIP = TestClassHelper.createIP(objGrant,objAcc);
        objIP.Reporting__c = 'Yearly';
        objIP.PR_Fiscal_Cycle__c = 'January-December';
        insert objIP;
        
        Performance_Framework__c objPF = TestClassHelper.createPF(objIP);
        insert objPF; 
        system.debug('**objPF'+objPF); 
        
        List<Implementation_Period__c>lstIP = new List<Implementation_Period__c>();
        lstIP.add(objIP);
        
        Page__c ObjPage = TestClassHelper.createPage();
        ObjPage.Implementation_Period__c = objIP.Id;
        insert ObjPage;
        
        Period__c ObjRP = TestClassHelper.createPeriod();
        ObjRP.Implementation_Period__c = objIP.Id;
        ObjRP.Start_Date__c = Date.today();
        ObjRP.EFR__c=true;
        ObjRP.EFR_Due_Date__c = system.today();
        ObjRP.PU__c=true;
        ObjRP.PU_Due_Date__c =  system.today();
        ObjRP.Audit_Report__c=true;
        ObjRP.AR_Due_Date__c=system.today();
        ObjRP.DR__c = true;
        ObjRP.Type__c = 'Reporting';
        ObjRP.Due_Date__c=system.today();
        insert ObjRP;
        
        Guidance__c objGuidance = new Guidance__c();
        objGuidance.Name = 'GM Reporting Periods';
        insert objGuidance; 
        
        Reporting_Period_Detail__c detailObj = TestClassHelper.createReportingDetailPeriod(objip.id, true); 
        detailObj.PR_Reporting_Cycle__c = objIP.PR_Fiscal_Cycle__c;
        detailObj.Reporting_Frequency__c= objIP.Reporting__c;
        detailObj.Is_Active__c = true;
        detailObj.Implementation_Period_Start_Date__c = objIp.start_Date__c.addDays(3);
        detailObj.Implementation_Period_End_Date__c = objIp.End_Date__c;
        insert detailObj;
        
        ApexPages.StandardController stdctrl = new ApexPages.StandardController(objIP);
        ApexPages.currentPage().getParameters().put('sid',objIP.id);
        changeReportingPeriodExt extObj = new changeReportingPeriodExt(stdctrl);
        extObj.GeneratePeriod();
        extObj.CreateNewRP();

}
public static testMethod void myctrlReportingPeriodsTest2(){
Account objAcc = TestClassHelper.insertAccount();
        
        Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
        insert objGrant;
       
        Implementation_Period__c objIP = TestClassHelper.createIP(objGrant,objAcc);
        objIP.Reporting__c = 'Half-yearly';
        objIP.PR_Fiscal_Cycle__c = 'January-December';
        insert objIP;
        
        Performance_Framework__c objPF = TestClassHelper.createPF(objIP);
        insert objPF; 
        system.debug('**objPF'+objPF); 
        
        List<Implementation_Period__c>lstIP = new List<Implementation_Period__c>();
        lstIP.add(objIP);
        
        Page__c ObjPage = TestClassHelper.createPage();
        ObjPage.Implementation_Period__c = objIP.Id;
        insert ObjPage;
        
        Period__c ObjRP = TestClassHelper.createPeriod();
        ObjRP.Implementation_Period__c = objIP.Id;
        ObjRP.Start_Date__c = Date.today();
        ObjRP.EFR__c=true;
        ObjRP.EFR_Due_Date__c = system.today().addDays(10);
        ObjRP.PU__c=true;
        ObjRP.PU_Due_Date__c =  system.today().addDays(10);
        ObjRP.Audit_Report__c=false;
        //ObjRP.AR_Due_Date__c=system.today();
        ObjRP.DR__c=True;
        ObjRP.Type__c = 'Reporting';
        ObjRP.Due_Date__c=system.today();
        insert ObjRP;
     
        Reporting_Period_Detail__c detailObj = TestClassHelper.createReportingDetailPeriod(objip.id, true); 
        detailObj.PR_Reporting_Cycle__c = objIP.PR_Fiscal_Cycle__c;
        detailObj.Reporting_Frequency__c= objIP.Reporting__c;
        detailObj.Is_Active__c = true;
        detailObj.Implementation_Period_Start_Date__c = objIp.start_Date__c.addDays(3);
        detailObj.Implementation_Period_End_Date__c = objIp.End_Date__c;
        insert detailObj;
        
        ApexPages.StandardController stdctrl = new ApexPages.StandardController(objIP);
        ApexPages.currentPage().getParameters().put('sid',objIP.id);
        changeReportingPeriodExt extObj = new changeReportingPeriodExt(stdctrl);
        extObj.GeneratePeriod();
        extObj.CreateNewRP();



}
public static testMethod void myctrlReportingPeriodsTest3(){
    Account objAcc = TestClassHelper.insertAccount();
        
        Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
        insert objGrant;
       
        Implementation_Period__c objIP = TestClassHelper.createIP(objGrant,objAcc);
        objIP.Reporting__c = 'Quarterly';
        objIP.PR_Fiscal_Cycle__c = 'January-December';
        insert objIP;
        
        Performance_Framework__c objPF = TestClassHelper.createPF(objIP);
        insert objPF; 
        system.debug('**objPF'+objPF); 
        
        List<Implementation_Period__c>lstIP = new List<Implementation_Period__c>();
        lstIP.add(objIP);
        
        Page__c ObjPage = TestClassHelper.createPage();
        ObjPage.Implementation_Period__c = objIP.Id;
        insert ObjPage;
        
         
        
        Period__c ObjRP = TestClassHelper.createPeriod();
        ObjRP.Implementation_Period__c = objIP.Id;
        ObjRP.Start_Date__c = Date.today();
        ObjRP.EFR__c=true;
        ObjRP.EFR_Due_Date__c = system.today();
        system.debug('**ObjRP.EFR_Due_Date__c'+ObjRP.EFR_Due_Date__c);
        ObjRP.PU__c=true;
        ObjRP.PU_Due_Date__c =  system.today();
        //ObjRP.Audit_Report__c=true;
        //ObjRP.AR_Due_Date__c=system.today();
        ObjRP.DR__c=true;
        ObjRP.Type__c = 'Reporting';
        ObjRP.Due_Date__c=system.today().addDays(2);
        insert ObjRP;
     
        Reporting_Period_Detail__c detailObj = TestClassHelper.createReportingDetailPeriod(objip.id, true); 
        detailObj.PR_Reporting_Cycle__c = objIP.PR_Fiscal_Cycle__c;
        detailObj.Reporting_Frequency__c= objIP.Reporting__c;
        detailObj.Is_Active__c = true;
        detailObj.Implementation_Period_Start_Date__c = objIp.start_Date__c.addDays(3);
        detailObj.Implementation_Period_End_Date__c = objIp.End_Date__c;
        insert detailObj;
        
        ApexPages.StandardController stdctrl = new ApexPages.StandardController(objIP);
        ApexPages.currentPage().getParameters().put('sid',objIP.id);
        changeReportingPeriodExt extObj = new changeReportingPeriodExt(stdctrl);
        extObj.GeneratePeriod();
        extObj.CreateNewRP();
}
public static testMethod void myctrlReportingPeriodsTest4(){
        
        
        Account objAcc = TestClassHelper.createAccount();
        
        insert objAcc;
        
        Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
        insert objGrant;
       
        Implementation_Period__c objIP = TestClassHelper.createIP(objGrant,objAcc);
        objIP.Start_Date__c =null;
        objIP.End_Date__c =null;
        objIP.Reporting__c = 'Quarterly';
        objIP.PR_Fiscal_Cycle__c = 'January-December';
        insert objIP;
        
        Performance_Framework__c objPF = TestClassHelper.createPF(objIP);
        insert objPF; 
        system.debug('**objPF'+objPF); 
        
        List<Implementation_Period__c>lstIP = new List<Implementation_Period__c>();
        lstIP.add(objIP);
        
        Page__c ObjPage = TestClassHelper.createPage();
        ObjPage.Implementation_Period__c = objIP.Id;
        insert ObjPage;
        
         
        
        Period__c ObjRP = TestClassHelper.createPeriod();
        ObjRP.Implementation_Period__c = objIP.Id;
        ObjRP.Start_Date__c = Date.today();
        ObjRP.EFR__c=true;
        ObjRP.EFR_Due_Date__c = system.today();
        system.debug('**ObjRP.EFR_Due_Date__c'+ObjRP.EFR_Due_Date__c);
        ObjRP.PU__c=true;
        ObjRP.PU_Due_Date__c =  system.today();
        ObjRP.Audit_Report__c=true;
        ObjRP.AR_Due_Date__c=system.today();
        ObjRP.DR__c=true;
        ObjRP.Type__c = 'Reporting';
        ObjRP.Due_Date__c=system.today().addDays(2);
        insert ObjRP;
     
         Reporting_Period_Detail__c detailObj = TestClassHelper.createReportingDetailPeriod(objip.id, true); 
        detailObj.PR_Reporting_Cycle__c = objIP.PR_Fiscal_Cycle__c;
        detailObj.Reporting_Frequency__c= objIP.Reporting__c;
        detailObj.Is_Active__c = true;
        detailObj.Implementation_Period_Start_Date__c = system.today();
        detailObj.Implementation_Period_End_Date__c = system.today().addDays(3);
        insert detailObj;
        
        ApexPages.StandardController stdctrl = new ApexPages.StandardController(objIP);
        ApexPages.currentPage().getParameters().put('sid',objIP.id);
        changeReportingPeriodExt extObj = new changeReportingPeriodExt(stdctrl);
        extObj.GeneratePeriod();
        extObj.CreateNewRP();
        
}
public static testMethod void myctrlReportingPeriodsTest5(){
        
        Country__c objCountry = TestClassHelper.createCountry();
        objCountry.Country_Fiscal_Cycle_Start_Date__c = null;
        objCountry.Country_Fiscal_Cycle_End_Date__c = null;
        insert objCountry;
        
        Account objAcc = TestClassHelper.createAccount();
        objAcc.Country__c = objCountry.Id ;
        insert objAcc;
        
        Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
        insert objGrant;
       
        Implementation_Period__c objIP = TestClassHelper.createIP(objGrant,objAcc);
        objIP.Reporting__c = 'Quarterly';
        objIP.PR_Fiscal_Cycle__c = 'April-March';
        insert objIP;
        
        Performance_Framework__c objPF = TestClassHelper.createPF(objIP);
        insert objPF; 
        system.debug('**objPF'+objPF); 
        
        List<Implementation_Period__c>lstIP = new List<Implementation_Period__c>();
        lstIP.add(objIP);
        
        Page__c ObjPage = TestClassHelper.createPage();
        ObjPage.Implementation_Period__c = objIP.Id;
        insert ObjPage;
        
         
        
        Period__c ObjRP = TestClassHelper.createPeriod();
        ObjRP.Implementation_Period__c = objIP.Id;
        ObjRP.Start_Date__c = Date.today();
        ObjRP.EFR__c=true;
        ObjRP.EFR_Due_Date__c = system.today();
        system.debug('**ObjRP.EFR_Due_Date__c'+ObjRP.EFR_Due_Date__c);
        ObjRP.PU__c=true;
        ObjRP.PU_Due_Date__c =  system.today();
        ObjRP.Audit_Report__c=true;
        ObjRP.AR_Due_Date__c=system.today().addDays(10);
        ObjRP.DR__c=true;
        ObjRP.Type__c = 'Reporting';
        ObjRP.Due_Date__c=system.today().addDays(2);
        insert ObjRP;
     
        Reporting_Period_Detail__c detailObj = TestClassHelper.createReportingDetailPeriod(objip.id, true); 
        detailObj.PR_Reporting_Cycle__c = objIP.PR_Fiscal_Cycle__c;
        detailObj.Reporting_Frequency__c= objIP.Reporting__c;
        detailObj.Is_Active__c = true;
        detailObj.Implementation_Period_Start_Date__c = objIp.start_Date__c.addDays(3);
        detailObj.Implementation_Period_End_Date__c = objIp.End_Date__c;
        insert detailObj;
        
        ApexPages.StandardController stdctrl = new ApexPages.StandardController(objIP);
        ApexPages.currentPage().getParameters().put('sid',objIP.id);
        changeReportingPeriodExt extObj = new changeReportingPeriodExt(stdctrl);
        extObj.GeneratePeriod();
        extObj.CreateNewRP();
        
         
        
        

       
}
}