/*********************************************************************************
* {Extension} Class: {controlPET_create}
*  DateCreated : 11/11/2013
----------------------------------------------------------------------------------
* used with {Controller} Class: {LFA_Work_Plan__c}
* (has fields)
* LFA_Service__c.LFA_Service_Name__c
* LFA_Service__c.Grant__c
* LFA_Service__c.LFA_Resource__c.LFA_role__c
* LFA_Service__c.LFA_Resource__c.Contact_Name__c
* LFA_Service__c.LFA_Resource__c.Actual_LOE__c
* LFA_Service__c.Service_Type__c
----------------------------------------------------------------------------------
* Purpose/Methods:
* - allows for selection of multiple LFA_Service__c [js]
* - validates that at least one service is checked [js]
* - saves new PET, returns to PET tab [pagereference]
* - passes all checked LFA_Service__c to new Service_PET_jxn__c [save; SOQL query; insert/update]
* - for specific LFA_Service__c.Service_Sub_Type__c, creates PET_Response__c
* 
* Unit Test: n/a
----------------------------------------------------------------------------------
* History:
* - VERSION  DATE            DETAIL FEATURES
     1.0   11/11/2013      INITIAL DEVELOPMENT
*********************************************************************************/

Public Class ctrlPETCreate{

    Public List<SelectOption> getServiceTypes()
    {
      List<SelectOption> options = new List<SelectOption>();
        
       Schema.DescribeFieldResult fieldResult =
        LFA_Service__c.Service_Type__c.getDescribe();
       List<Schema.PicklistEntry> ServiceTypes = fieldResult.getPicklistValues();
        
       for( Schema.PicklistEntry f : ServiceTypes)
       {
          options.add(new SelectOption(f.getLabel(), f.getValue()));
       }       
       return options;
    }
    
    Public Boolean serviceSelected {get; set;}    

    /********************************************************************
        Constructor
    ********************************************************************/
    
    Public ctrlPETCreate(ApexPages.StandardController controller) {
    
    }
}



// passes all checked LFA_Service__c to new Service_PET_jxn__c [save; SOQL query; insert/update]

// for each checked LFA_Service__c:

/*
if (LFA_Service__c.Service_Sub_Type__c = "PUDR") {
    new PET_Response__c set Type__c = "LFA Finance" [initiate]
} else {
    if (LFA_Service__c.Service_Sub_Type__c = "Grant/Renewal") { 
        new PET_Response__c set Type__c = "LFA Finance";
        new PET_Response__c set Type__c = "LFA Programmatic/M&E";
        new PET_Response__c set Type__c = "LFA PSM";
    } else {
        if (LFA_Service__c.Service_Sub_Type__c = "OSDV/M&E") {
            new PET_Response__c set Type__c = "LFA Programmatic/M&E";
         } else {
            if (LFA_Service__c.Service_Sub_Type__c = "PSM Country Profile/Procurement Review" {
                new PET_Response__c set Type__c = "LFA PSM";
            } else {
            return null;
            }
        }
    }
}
*/

// Service_PET_jxn__c:
//includes list of all PET_Response__c
// for each PET_Response__c sends email to ??????? (with link to PET_Response__c page)
// schedules reminder email (after n days)

// when PET_Response__c is completed, checks:
// if (all PET_Response__c are complete) {
// stops reminder email || sends email to creator of PET (with link to FPM Survey) || removes modal from FPM survey; } else { waits patiently; }