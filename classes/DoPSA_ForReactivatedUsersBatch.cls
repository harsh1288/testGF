global class DoPSA_ForReactivatedUsersBatch implements Schedulable, Database.Stateful, Database.Batchable<sObject> {
    global String query; 
    private final String BATCH_JOB_Name = 'DoSharingForReactivatedUsersBatch'; 
    global Date sinceDate; 
    global String errorMessage = '';
    //Past i days
    public DoPSA_ForReactivatedUsersBatch (Integer i){
        sinceDate = Date.today()-i;
        system.debug('sinceDate '+sinceDate);
    }
    //Past 1 day
    public DoPSA_ForReactivatedUsersBatch (){     
        sinceDate = Date.today()-1;
        system.debug('sinceDate '+sinceDate);
    }
    global Database.QueryLocator start(Database.BatchableContext BC){      
        
        query = 'Select Id, Do_dummy_update__c, recordtypeid, npe5__Organization__c, Manage_Contacts__c, recordtype.name, npe5__Status__c, npe5__Contact__c, npe5__Contact__r.RecordtypeId,Access_Level__c, name , LFA_Work_Plans__c, PET_Access__c  from npe5__Affiliation__c where User_reactivated_on__c!=null AND User_reactivated_on__c >=:sinceDate';
        system.debug(query);
        system.debug(' since date '+sinceDate);
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<npe5__Affiliation__c> scope) {        
        
        Map<Id, npe5__Affiliation__c > emptyMap= new Map<Id, npe5__Affiliation__c >();
        AffiliationServices.doPSA(scope , emptyMap); 
        Set<Id> LFAusersForGrantMakingPSassignment = AffiliationServices.filterUsersForGmPSassignment(scope, emptyMap);
        if(LFAusersForGrantMakingPSassignment.size()>0) AffiliationServices.assignGrantMakingPermissionSet(LFAusersForGrantMakingPSassignment);
        
        
    } 
    global void finish(Database.BatchableContext BC){
        system.debug(' errorMessage '+errorMessage);        
    }                
    
    global void execute( SchedulableContext sc ) {        
        DoPSA_ForReactivatedUsersBatch job = new DoPSA_ForReactivatedUsersBatch(); 
        Database.executeBatch(job);
    } 
}