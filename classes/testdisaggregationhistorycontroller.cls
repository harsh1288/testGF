@istest(SeeAllData=true)
public with sharing class testdisaggregationhistorycontroller {
   Public static testMethod void testdisaggregationhistorycontroller(){
         Indicator__c objcat = TestClassHelper.createCatalogIndicator();
         objcat.Is_Disaggregated__c =true;
         insert objcat;
         
         Catalog_Disaggregated__c objcatdis = new Catalog_Disaggregated__c();
         objcatdis.Catalog_Indicator__c = objcat.id;
         objcatdis.Disaggregation_Category__c = 'Type of testing';
         objcatdis.Disaggregation_Value__c = '12 months after initiation';
         insert objcatdis; 
          
         Grant_Indicator__c objIndicator = TestClassHelper.createGrantIndicator();
         objIndicator.Indicator__c = objcat.id;
         objIndicator.Indicator_Type__c = 'Impact';
         insert objIndicator;
         
         
         Grant_Disaggregated__c gd = TestClassHelper.createGrantDisaggregatedWithoutGI();
         gd.Grant_Indicator__c = objIndicator.Id;
         gd.Catalog_Disaggregated__c = objcatdis.id;
         gd.Disaggregated_Baseline_Value__c = 100000;
         gd.Disaggregated_Baseline_Sources__c = 'Reports (specify)';
         insert gd;
         
         gd.Disaggregated_Baseline_Value__c = 200000;
         gd.Disaggregated_Baseline_Sources__c = 'Reports, Surveys, Questionnaires, etc. (specify)';
         update gd;
         
         gd.Baseline_Denominator__c =3;
         update gd;
         
         //Grant_Disaggregated__History objGDHistory = new Grant_Disaggregated__History(ParentId = gd.id, Field = 'Baseline_Numerator__c');
         //insert objGDHistory;
        Grant_Disaggregated__c gd1 = [select Grant_Indicator__c from Grant_Disaggregated__c where  Grant_Indicator__c != Null and Catalog_Disaggregated__c != Null  limit 1];
        
        disaggregationHistoryController obj = new disaggregationHistoryController();
        obj.strRecordId = gd1.Grant_Indicator__c ;
        obj.strObjectName = 'Grant_Disaggregated__c';
        system.debug('@@'+gd);
        //system.debug('@@'+obj.lstHistoryRecords.size());
        //system.assert(obj.lstHistoryRecords.size() == 0);
        //system.assert([select ParentId from Grant_Disaggregated__History where isdeleted=false limit 1].size() == 1);
        obj.lstHistoryRecords = [select ParentId from Grant_Disaggregated__History where Parent.Grant_Indicator__c =: gd1.Grant_Indicator__c ];
        system.debug('@@'+obj.lstHistoryRecords);
        //system.assert(obj.lstHistoryRecords.size() >0); 
        obj.getGrntDisaggregationFiedHistry();
        disaggregationHistoryController obj1 = new disaggregationHistoryController();
        obj1.strObjectName = 'Account';
        obj1.getGrntDisaggregationFiedHistry();
   }
   
}