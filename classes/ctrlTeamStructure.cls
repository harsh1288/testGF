/*********************************************************************************
* {Controller} Class: {ctrlTeamStructure}
* DateCreated  : 07/22/2013
----------------------------------------------------------------------------------
* Purpose/Methods:
* - This controller used for Team Structure page which opens through a button on work plan 
    and will be used for country teams to manage their teams.
* 
* Unit Test: TestctrlTeamStructure
----------------------------------------------------------------------------------
* History:
* - VERSION  DEVELOPER NAME    DATE            DETAIL FEATURES
    1.0                      07/22/2013      INITIAL DEVELOPMENT
*********************************************************************************/

public class ctrlTeamStructure {

    // Global Variables
    
    Public  Id ContactId{get;set;}
    Public  String strAccId {get;set;}
    Public  String strAccName{get;set;}
    Public  String strParentAccId{get;set;}
    Public  String strConId{get;set;}
    
    Public  String strCountryName{get;set;}
    Public  String strCountryRegion{get;set;}
    Public  String strRoleId{get;set;}
    Public  String strRoleName{get;set;}
    Public  String strErrorMsg{get;set;}
    Public  String strCountryId{get;set;}
    Public  List<SelectOption> LocationOptions{get;private set;}
    Public  List<SelectOption> GfApprovedOptions{get;private set;}
    Public  List<Contact> lstContact{get;set;} 
    Public  List<SelectOption> RoleOptions {get; private set;}
    Public  List<WrapperContactRate> lstWrapContactRate{get;set;}
    Public String strWorkPlanId{get;set;} 
    Private Boolean blnLFARole;
    Public Contact objContact {get; set;}
    Public Change_Request__c objChangeRequest {get; set;}
    Public String strSelectLFARole {get; set;}
    Public Boolean blnHideInactive {get; set;}
    Public Boolean blnShowInactive {get; set;}
    Public String strErrorMessage {get; set;}
    Public String strChangeRequestID {get; set;}
    Public String strCurrentUserName {get; set;}
    Public String strCurrentDateTime {get; set;}
    Public RecordType objRecordType {get;set;}
    Public List<Change_Request__c> lstChangeRequest {get;set;}
    private String sortDirection = 'ASC';
    private String sortExp = 'name';
    Public String strRateYear {get; set;}
    
   /********************************************************************
       Constructor
       Purpose: Initialize Objects and Lists used by this page
   
   ********************************************************************/
       
    Public ctrlTeamStructure(){
        strWorkPlanId = Apexpages.currentpage().getparameters().get('WPId');
        lstWrapContactRate = new List<WrapperContactRate>();
        blnLFARole =false;
        blnHideInactive = false;
        blnShowInactive = true;
        strErrorMessage = '';
        strRateYear = 'Current Year';
        objRecordType = [Select Id From RecordType Where SobjectType = 'Contact' And RecordType.DeveloperName = 'LFA' Limit 1];
        objContact = new Contact(RecordTypeID = objRecordType.Id);
        strChangeRequestID = null;
        objChangeRequest = new Change_Request__c();
        fillList();
        FillRole();
        FillLocationAndGFApproved();
        strCurrentUserName = [Select Name From User Where ID =: UserInfo.getUserId()].Name;
    }

   /********************************************************************
       Name: addContactRow
       Purpose: Used for add Contact and new rate row in list.   
   ********************************************************************/
    Public PageReference addContactRow(){
        String AddConId = Apexpages.currentpage().getparameters().get('AddConId');      
        if(String.IsBlank(AddConId) == false){
            List<Contact> lstContactTemp = [Select id,Name,FirstName,LastName,Active_LFA_Member__c,Experience__c from Contact where id = :AddConId Limit 1];
            for(WrapperContactRate objWrapConRate :lstWrapContactRate){
                if(lstContactTemp[0].id == objWrapConRate.ObjContact.Id){
                    strErrorMsg = 'Contact already exits';                    
                    return null;
                }                
            }
            strErrorMsg = null;
            WrapperContactRate objWrapCRate = new WrapperContactRate(new Contact(AccountId = strAccId));
            objWrapCRate.ObjContact = lstContactTemp[0];           
            
            Rate__c objRate = new Rate__c(Contact__c = lstContactTemp[0].Id,Country__c = strCountryId);
            wrpRate objwrpRate = new wrpRate(objRate);
            objwrpRate.strRoleName = objwrpRate.objRate.LFA_Role__r.Name;           
            objWrapCRate.lstWrpRate = new List<wrpRate>();           
            objWrapCRate.lstWrpRate.add(objwrpRate);
            lstWrapContactRate.add(objWrapCRate);            
        }
        return null;
    }
    
   /********************************************************************
       Name: SaveContact
       Purpose: Used for Save Contact and releted rates in workplans 
                and redirect to releted workplan detail page. 
   
   ********************************************************************/
    
    Public PageReference SaveContact(){        
        QuickSaveContact();
        if(blnLFARole == true){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please select LFA Role .');
            ApexPages.addMessage(myMsg);
            return null;
        }else{
            PageReference p;
            if(String.IsBlank(strWorkPlanId) == false){
                p = new PageReference('/'+strWorkPlanId);
            }
            return p;
        }
    }
    
    /********************************************************************
       Name: Cancel
       Purpose: Used for Cancel save contact and rates.
   
   ********************************************************************/
    
    Public PageReference Cancel(){
        PageReference p;
        if(String.IsBlank(strWorkPlanId) == false){
            p = new PageReference('/'+strWorkPlanId);
        }
        return p;
    }
    
    /***********************************************************************************
       Name: FillRole
       Purpose: Used for Fill LFA Role Name Field and set values from lookup to picklist.
                Called on 'Quick Save' button and also in SaveContact method.
   
    ************************************************************************************/
    
    Public Void FillRole(){
        RoleOptions = new List<SelectOption>();
        List<LFA_Role__c> lstRoles = [Select Id,Name From LFA_Role__c];
        RoleOptions.add(new Selectoption('',''));
        if(lstRoles != null && lstRoles.size() > 0){
            for(LFA_Role__c objRole: lstRoles){
                RoleOptions.add(new Selectoption(objRole.id,objRole.Name));                
            }
        }
    }
    
    public List<SelectOption> getRateYear(){
        List<SelectOption> RateYearOptions = new List<SelectOption>();
        RateYearOptions.add(new SelectOption('','--None--'));
        RateYearOptions.add(new SelectOption('Last Year','Last Year'));
        RateYearOptions.add(new SelectOption('Current Year','Current Year'));
        RateYearOptions.add(new SelectOption('Next Year','Next Year'));
        return RateYearOptions;
    }
    
   /************************************************************************
       Name: fillList
       Purpose: Used for retrive contact and their releted rates data in List. 
                Called in Construstor of this class.
   
   *************************************************************************/
    
    Public Void fillList(){        
        if(String.IsBlank(strWorkPlanId) == false){
            List<LFA_Work_Plan__c> lstWorkplan = [Select id,LFA__c,LFA__r.Name,LFA__r.ParentId,Country__c,Country__r.Name,
                                                  Country__r.Region_name__c from LFA_Work_Plan__c where Id = :strWorkPlanId Limit 1];
            system.debug('@@@@@lstWorkplan: '+lstWorkplan);  
            if(lstWorkplan.size() >0){
                strAccId = lstWorkplan[0].LFA__c;
                strAccName = lstWorkplan[0].LFA__r.Name;
                strParentAccId = lstWorkplan[0].LFA__r.ParentId;
                strCountryId = lstWorkplan[0].Country__c;
                strCountryName = lstWorkplan[0].Country__r.Name;
                strCountryRegion = lstWorkplan[0].Country__r.Region_name__c;
                //strAppendCountryID = strCountryId;
                //Rate__c objRate = new Rate__c(Country__c = strCountryId);
                //lstRate.add(objRate);
            }            
        }
        if(String.IsBlank(strCountryId)==false && (String.IsBlank(strAccId)==false || String.IsBlank(strParentAccId)==false)){
            String strQuery = 'Select Id,Name,FirstName,LastName,Active_LFA_Member__c,Experience__c,Account.Country__r.Name,Account.Country__r.Region_name__c,';
            strQuery += '(Select id,Name,LFA_Role__c,Other_LFA_Role__c,LFA_Role__r.Name,Active__c,LFA_Location__c,Rate__c,Next_Year_Daily_Rate__c,Difference__c,';
            strQuery += 'GF_Approved__c,Last_Year_Active__c,Last_Year_Daily_Rate__c,Last_Year_GF_Approved__c,';
            strQuery += 'Last_Year_LFA_Location__c,Next_Year_Active__c,Next_Year_GF_Approved__c,';
            strQuery += 'Next_Year_LFA_Location__c from Rates__r Where ';
            if(strRateYear == 'Last Year'){
                strQuery += 'Last_Year_Daily_Rate__c > 0 And ';
            }
            if(strRateYear == 'Next Year'){
                strQuery += 'Next_Year_Daily_Rate__c > 0 And ';
            }
            if(strRateYear == 'Current Year'){
                strQuery += 'Rate__c > 0 And ';
            }
            strQuery += 'Contact__c != null And Country__c =: strCountryId)';
            strQuery += 'from Contact where (AccountId =: strAccId OR AccountId =:strParentAccId OR (Contact.Account.ParentId != null and Contact.Account.ParentId =:strParentAccId)) Order By LastName ASC';
            
            
            System.debug('###@@@strQuery: '+strQuery);
            lstContact = Database.Query(strQuery);
            /*lstContact = [Select Id,Name,FirstName,LastName,Active_LFA_Member__c,Experience__c,
                            Account.Country__r.Name,Account.Country__r.Region_name__c,
                            (Select id,Name,LFA_Role__c,Other_LFA_Role__c,LFA_Role__r.Name,Active__c,
                                LFA_Location__c,Rate__c,Next_Year_Daily_Rate__c,Difference__c,
                                GF_Approved__c,Last_Year_Active__c,Last_Year_Daily_Rate__c,Last_Year_GF_Approved__c,
                                Last_Year_LFA_Location__c,Next_Year_Active__c,Next_Year_GF_Approved__c,
                                Next_Year_LFA_Location__c from Rates__r Where Contact__c != null And Country__c =: strCountryId) 
                                from Contact where (AccountId =: strAccId OR AccountId =:strParentAccId
                                OR (Contact.Account.ParentId != null and Contact.Account.ParentId =:strParentAccId)) Order By LastName ASC];*/
        }     
        system.debug('@@@@@lstContact: '+lstContact); 
        List<Rate__c> listRate = new List<Rate__c>();
        if(lstContact != null && lstContact.size() > 0){                      
            for(Contact objCon : lstContact){
                if(objCon.Rates__r!=null && objCon.Rates__r.size() >0){
                     listRate.addAll(objCon.Rates__r);
                }
            }
        }
        system.debug('###@@@listRate: '+ listRate);
        if(listRate.size() > 0){
            lstChangeRequest = [Select Id, Name, LastModifiedDate, Contact__c, Comments__c, LFA_Role__c, Other_LFA_Role__c, Rate__c,
                            Proposed_Daily_Rate__c, type__c,Proposed_LFA_Location__c, Proposed_Status__c, Status__c,
                            Proposed_Next_Year_Daily_Rate__c,Proposed_Next_Year_LFA_Location__c,Proposed_Next_Year_Status__c
                            From Change_Request__c Where Rate__c IN :listRate Order By LastModifiedDate];
        }
        Map<ID,Change_Request__c> MapChangeRequest;
        System.debug('###@@@listRate.size(): '+listRate.size());
        
        if(listRate.size() > 0){
            MapChangeRequest = new Map<ID,Change_Request__c>
                ([
                    Select Id, Name, LastModifiedDate, Contact__c, Comments__c, LFA_Role__c, Other_LFA_Role__c, Rate__c,
                    Proposed_Daily_Rate__c, type__c, Proposed_LFA_Location__c, Proposed_Status__c, Status__c,
                    Proposed_Next_Year_Daily_Rate__c,Proposed_Next_Year_LFA_Location__c,Proposed_Next_Year_Status__c
                    From Change_Request__c Where Rate__c IN :listRate Order By LastModifiedDate
                ]);
        }
        
        Map<ID,ID> MapPendingCR = new Map<ID,ID>(); // RateID => ChageRequestID   
        if(MapChangeRequest != null && MapChangeRequest.size() > 0){       
            List<ProcessInstance> lstPICR = [Select ID, Status, TargetObjectID From ProcessInstance Where TargetObjectID IN :MapChangeRequest.keySet() AND Status = 'Pending'];                
            if(lstPICR != null && lstPICR.size() > 0){
                for(ProcessInstance objPI :lstPICR)
                {
                    //MapPendingCR = new MapPendingCR<ID,ID>((MapChangeRequest.get(objPI.TargetObjectID.Rate__c),objPI.TargetObjectID));
                    MapPendingCR.put(MapChangeRequest.get(objPI.TargetObjectID).Rate__c,objPI.TargetObjectID);
                }
            }
        }
        
        lstWrapContactRate = New List<WrapperContactRate>();  
        if(lstContact != null && lstContact.size() > 0){                      
            for(Contact objCon : lstContact){
                if(objCon.Rates__r!=null && objCon.Rates__r.size() >0){
                    WrapperContactRate objWrapConRate = new WrapperContactRate(objCon);                   
                    List<wrpRate> lstwrapRateTemp = new List<wrpRate>();                    
                    for(Rate__c objRate:objCon.Rates__r){
                        wrpRate objwrpRate = new wrpRate(objRate);
                        if(MapPendingCR.containsKey(objRate.id)==true){
                            objwrpRate.strPendingCRID=MapPendingCR.get(objRate.id);
                        } 
                        objwrpRate.objRate = objRate;
                        objwrpRate.strRoleName = objwrpRate.objRate.LFA_Role__r.Name;
                        objwrpRate.decdifference = objwrpRate.objRate.Difference__c;
                        lstwrapRateTemp.add(objwrpRate);
                    }                  
                    if(lstwrapRateTemp.size() >0){
                        objWrapConRate.lstWrpRate = lstwrapRateTemp;
                    }                   
                    lstWrapContactRate.add(objWrapConRate);       
                }
            }
        }
    }
    
    Public void FillListOnSelectYear(){
        fillList();
    }
    
    Public void FillLocationAndGFApproved(){
        LocationOptions = new List<SelectOption>();
        GfApprovedOptions = new List<SelectOption>();
        Schema.sObjectType objType = Rate__c.getSObjectType();
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe(); 
        map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap(); 
        List<Schema.PicklistEntry> Locations = fieldMap.get('LFA_Location__c').getDescribe().getPickListValues();
        List<Schema.PicklistEntry> GfApproved = fieldMap.get('GF_Approved__c').getDescribe().getPickListValues();
        LocationOptions.add(new SelectOption('', '--None--'));
        GfApprovedOptions.add(new SelectOption('', '--None--'));
        for (Schema.PicklistEntry Entry : Locations){ 
            LocationOptions.add(new SelectOption(Entry.getLabel(), Entry.getValue()));
        }
        for (Schema.PicklistEntry Entry : GfApproved){ 
            GfApprovedOptions.add(new SelectOption(Entry.getLabel(), Entry.getValue()));
        }
    }
    
    /******************** Sorting *************************/
    
    public String sortExpression
    {
        get
        {
            return sortExp;
        }
        set
        {
            //if the column is clicked on then switch between Ascending and Descending modes
            if (value == sortExp)
                sortDirection = (sortDirection == 'ASC')? 'DESC' : 'ASC';
            else
                sortDirection = 'ASC';
                sortExp = value;
        }
    }
    
    public String getSortDirection()
    {
        //if not column is selected 
        if (sortExpression == null || sortExpression == '')
            return 'ASC';
        else
            return sortDirection;
    }
    
    public void setSortDirection(String value)
    {  
        sortDirection = value;
    }
    
    public PageReference ViewData() {
        //build the full sort expression
        string sortFullExp = sortExpression  + ' ' + sortDirection;
        
        List<Rate__c> listRate = new List<Rate__c>();
        if(lstContact != null && lstContact.size() > 0){                      
            for(Contact objCon : lstContact){
                if(objCon.Rates__r!=null && objCon.Rates__r.size() >0){
                     listRate.addAll(objCon.Rates__r);
                }
            }
        }
        //query the database based on the sort expression
        if(listRate.size() > 0){
            lstChangeRequest = Database.query('Select Id, Name, LastModifiedDate, Contact__c, Comments__c, LFA_Role__c, Other_LFA_Role__c, Rate__c, Proposed_Daily_Rate__c, Proposed_LFA_Location__c, type__c, Proposed_Status__c, Status__c From Change_Request__c Where Rate__c IN :listRate order by ' + sortFullExp + ' NULLS LAST');
        }
        return null;
    }
    
   /************************************************************************
       Name: addContactRateRow
       Purpose: Used for add rate object row in List of wrapper class list.                 
   
   ************************************************************************/    
    
    Public Void addContactRateRow(){                
        if(String.IsBlank(ContactId) == false){
            for(WrapperContactRate objWrapConRate : lstWrapContactRate){
                if(objWrapConRate.objContact.Id == ContactId){                    
                    Rate__c objRate = new Rate__c(Contact__c = ContactId,Country__c = strCountryId);
                    wrpRate objwrpRate = new wrpRate(objRate);
                    objwrpRate.strRoleName = objwrpRate.objRate.LFA_Role__r.Name;                  
                    if(objWrapConRate.lstWrpRate == null){
                        objWrapConRate.lstWrpRate = New List<wrpRate>();
                    }                 
                    objWrapConRate.lstWrpRate.add(objwrpRate);
                    break;
                }
            } 
            ContactId = null; 
        }
    }
    
   /************************************************************************
       Name: DeleteRates
       Purpose: Used for delete rate releted to contact .                 
   
   ************************************************************************/
    Public Void DeleteRates(){        
        Integer DeleteConRateIndex = integer.valueof(Apexpages.currentpage().getparameters().get('DeleteConRateIndex'));
        Integer DeleteRateIndex = integer.valueof(Apexpages.currentpage().getparameters().get('DeleteRateIndex'));
        
        List<Rate__c> lstRateToDelete = new List<Rate__c>();
        if(DeleteConRateIndex !=null && DeleteRateIndex != null){
            if(lstWrapContactRate[DeleteConRateIndex].lstWrpRate[DeleteRateIndex].objRate.Id != null){
                lstRateToDelete.add(lstWrapContactRate[DeleteConRateIndex].lstWrpRate[DeleteRateIndex].objRate);
            }            
            lstWrapContactRate[DeleteConRateIndex].lstWrpRate.remove(DeleteRateIndex);
            if(lstWrapContactRate[DeleteConRateIndex].lstWrpRate.size() == 0){
                lstWrapContactRate.remove(DeleteConRateIndex);
                system.debug('###lstWrapContactRate->'+lstWrapContactRate);
            }
            DeleteConRateIndex = null;
            DeleteRateIndex = null;            
        }
        if(lstRateToDelete.size() > 0) Delete lstRateToDelete;
    }
    
    /************************************************************************
       Name: QuickSaveContact
       Purpose: Used for save Contact with releted rates rows.
               Called on 'Quick Save' button and also in SaveContact method.
   
   ************************************************************************/
    Public void QuickSaveContact(){
        blnLFARole = false;
        List<Rate__c> lstRateToupsert = new List<Rate__c>();        
        Map<Id,Contact> mapOldContactIdToNewContactObj = new Map<Id,Contact>();        
        Map<Id,List<Rate__c>> mapOldContactIdToRates = new Map<Id,List<Rate__c>>();    
        if(lstWrapContactRate != null && lstWrapContactRate.size() > 0){
            Contact objContactTemp;
            List<Rate__c> lstRatesInsert;
            Rate__c objRateTemp;             
            for(WrapperContactRate objWrapConRate : lstWrapContactRate){
                objContactTemp = new Contact();
                objContactTemp = objWrapConRate.objContact;
                system.debug('###objContactTemp.Id->'+objContactTemp.Id);
                mapOldContactIdToNewContactObj.put(objContactTemp.Id,objContactTemp);
               
                lstRatesInsert = new List<Rate__c>();
                if(objWrapConRate.lstWrpRate.size() > 0){
                    for(WrpRate objwrpRate : objWrapConRate.lstWrpRate){                    
                        objRateTemp = new rate__c();
                        objRateTemp = objwrpRate.objRate;
                        if(objwrpRate.strRoleName != 'Other Professional'){
                            objRateTemp.Other_LFA_Role__c = null;
                        }
                        lstRatesInsert.add(objRateTemp);
                    }
                }
                mapOldContactIdToRates.put(objContactTemp.Id,lstRatesInsert);
            }
        }
        if(mapOldContactIdToNewContactObj.size() > 0){            
            update mapOldContactIdToNewContactObj.values();
        }
       
        for(ID ContactId : mapOldContactIdToRates.keyset()){
            if(mapOldContactIdToRates.get(ContactId) != null){            
                for(Rate__c objRate : mapOldContactIdToRates.get(ContactId)){
                    if(objRate.LFA_Location__c != null || objRate.GF_Approved__c != null ||
                       objRate.Next_Year_Daily_Rate__c != null || objRate.LFA_Location__c != null ||
                       objRate.LFA_Role__c != null){
                        if(objRate.LFA_Role__c != null){
                            objRate.Contact__c = mapOldContactIdToNewContactObj.get(ContactId).id;
                            lstRateToupsert.add(objRate);
                        }else{
                            blnLFARole =true;
                            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please select LFA Role.');
                            ApexPages.addMessage(myMsg);
                            return;
                        }
                    }
                }
            }
        }
        
        if(lstRateToupsert.size() > 0) upsert lstRateToupsert;
        lstWrapContactRate = new List<WrapperContactRate>();
        fillList();
    }
    
    
    /********************************************************************
       Name: DeleteChangeRequest
       Purpose: Used for delete Change Request, Call When click on Delete link from the Change Request 
                Related List of Rates. 
                
    ********************************************************************/
    /*Public Void DeleteChangeRequest(){
        Integer CRIndex = integer.valueof(Apexpages.currentpage().getparameters().get('CRIndex'));
        System.debug('###$$$##CRDeleteIndex :' + CRIndex);
        if(CRIndex != null){
            List<Change_Request__c> lstChangeRequestToDelete = [Select Id From Change_Request__c Where Id =: lstChangeRequest[CRIndex].Id Limit 1];
            if(lstChangeRequestToDelete.size() > 0){
                delete lstChangeRequestToDelete;
            }
            lstChangeRequest.remove(CRIndex);
        }
    }*/
    
    /********************************************************************
       Name: SaveChangeRequest
       Purpose: Used for create new Change Request, Call When click on Change Request Icon 
                and redirect to new created Change Request detail page. 
   
   ********************************************************************/
    
    Public PageReference SaveChangeRequest(){  
            System.debug('@@@@@RateID:');      
            String RateID = Apexpages.currentpage().getparameters().get('RateId');
            System.debug('@@@@@RateID:'+RateID);
            if(RateID != null && RateID != ''){
                List<Rate__c> lstRateForChangeRequest = [Select Id,Name,Active__c,Rate__c,LFA_Location__c,
                                                         Next_Year_Daily_Rate__c,Next_Year_Active__c,
                                                         Next_Year_GF_Approved__c,Next_Year_LFA_Location__c
                                                         From Rate__c Where Id =: RateID];
                System.debug('@@@@@lstRateForChangeRequest:'+lstRateForChangeRequest);
                if(lstRateForChangeRequest.size() > 0){
                    objChangeRequest = new Change_Request__c();
                    objChangeRequest.Rate__c =  lstRateForChangeRequest[0].Id;
                    if(strRateYear == 'Current Year'){
                        objChangeRequest.Proposed_Daily_Rate__c = lstRateForChangeRequest[0].Rate__c;
                        objChangeRequest.Proposed_Next_Year_Daily_Rate__c = lstRateForChangeRequest[0].Rate__c;
                        objChangeRequest.Proposed_LFA_Location__c = lstRateForChangeRequest[0].LFA_Location__c;
                        objChangeRequest.Proposed_Next_Year_LFA_Location__c = lstRateForChangeRequest[0].LFA_Location__c;
                        String strProposedStatus = '';
                        if(lstRateForChangeRequest[0].Active__c == true){
                            strProposedStatus = 'Active';
                        }else{
                            strProposedStatus = 'Inactive';
                        }
                        objChangeRequest.Proposed_Status__c = strProposedStatus;
                        objChangeRequest.Proposed_Next_Year_Status__c = 'Active';
                    }
                    if(strRateYear == 'Next Year'){
                        objChangeRequest.Proposed_Next_Year_Daily_Rate__c = lstRateForChangeRequest[0].Next_Year_Daily_Rate__c;
                        objChangeRequest.Proposed_Next_Year_LFA_Location__c = lstRateForChangeRequest[0].Next_Year_LFA_Location__c;
                        /*String strProposedNextYearStatus = '';
                        if(lstRateForChangeRequest[0].Next_Year_Active__c == true){
                            strProposedNextYearStatus = 'Active';
                        }else{
                            strProposedNextYearStatus = 'Inactive';
                        }*/
                        objChangeRequest.Proposed_Next_Year_Status__c = 'Active';
                    }
                    objChangeRequest.WP_ID__c = strWorkPlanId;
                    insert objChangeRequest;
                    System.debug('@@@ChageRequestID: ' + objChangeRequest.Id);
                }
            }
            System.debug('@@@ChageRequestID: ' + objChangeRequest.Id);
            PageReference p;
            List<User> lstUser = [Select Profile.Name From User Where id =: UserInfo.getUserId()];
            if(lstUser.size() > 0){
                if(lstUser[0].Profile.Name == 'LFA Portal User'){
                    p = new PageReference('/LFA/'+objChangeRequest.Id);
                    
                }else{
                    p = new PageReference('/'+objChangeRequest.Id);
                    
                }
            }
            //PageReference p = new PageReference('/'+objChangeRequest.Id);
            return p;
            //strChangeRequestID = objChangeRequest.Id;
            //return p;
    }
    
    /********************************************************************
       Name: SaveChangeRequestTS
       Purpose: Used for create new Change Request, Call When click on Change Request Icon 
                and redirect to new created Change Request detail page in new tab. 
   
   ********************************************************************/
    
    Public Void SaveChangeRequestTS(){  
            String RateID = Apexpages.currentpage().getparameters().get('RateId');
            System.debug('@@@@@RateID:'+RateID);
            if(RateID != null && RateID != ''){
                List<Rate__c> lstRateForChangeRequest = [Select Id,Name,Active__c,Rate__c,LFA_Location__c,
                                                         Next_Year_Daily_Rate__c,Next_Year_Active__c,
                                                         Next_Year_GF_Approved__c,Next_Year_LFA_Location__c
                                                         From Rate__c Where Id =: RateID];
                System.debug('@@@@@lstRateForChangeRequest:'+lstRateForChangeRequest);
                if(lstRateForChangeRequest.size() > 0){
                    objChangeRequest = new Change_Request__c();
                    objChangeRequest.Rate__c =  lstRateForChangeRequest[0].Id;
                    System.debug('@@@@@strRateYear: ' + strRateYear);
                    if(strRateYear == 'Current Year'){
                        objChangeRequest.Proposed_Daily_Rate__c = lstRateForChangeRequest[0].Rate__c;
                        objChangeRequest.Proposed_Next_Year_Daily_Rate__c = lstRateForChangeRequest[0].Rate__c;
                        objChangeRequest.Proposed_LFA_Location__c = lstRateForChangeRequest[0].LFA_Location__c;
                        objChangeRequest.Proposed_Next_Year_LFA_Location__c = lstRateForChangeRequest[0].LFA_Location__c;
                        String strProposedStatus = '';
                        if(lstRateForChangeRequest[0].Active__c == true){
                            strProposedStatus = 'Active';
                        }else{
                            strProposedStatus = 'Inactive';
                        }
                        objChangeRequest.Proposed_Status__c = strProposedStatus;
                        objChangeRequest.Proposed_Next_Year_Status__c = 'Active';
                        System.debug('@@@@@SucessCurrentNextYear');
                    }
                    if(strRateYear == 'Next Year'){
                        objChangeRequest.Proposed_Next_Year_Daily_Rate__c = lstRateForChangeRequest[0].Next_Year_Daily_Rate__c;
                        objChangeRequest.Proposed_Next_Year_LFA_Location__c = lstRateForChangeRequest[0].Next_Year_LFA_Location__c;
                        /*String strProposedNextYearStatus = '';
                        if(lstRateForChangeRequest[0].Next_Year_Active__c == true){
                            strProposedNextYearStatus = 'Active';
                        }else{
                            strProposedNextYearStatus = 'Inactive';
                        }*/
                        objChangeRequest.Proposed_Next_Year_Status__c = 'Active';
                        System.debug('@@@@@SucessNextYear');
                    }
                    objChangeRequest.WP_ID__c = strWorkPlanId;
                    insert objChangeRequest;
                    System.debug('@@@ChageRequestID: ' + objChangeRequest.Id);
                }
            }
            System.debug('@@@ChageRequestID: ' + objChangeRequest.Id);
            //PageReference p = new PageReference('/'+objChangeRequest.Id);
            strChangeRequestID = objChangeRequest.Id;
            //return null;
    }
    
    /***********************************************************************************
       Name: ShowInactive
       Purpose: Used for Show Inactive Rate Records on Team Structure Page.
   
    ************************************************************************************/
    
    Public Void ShowInactive(){
        lstWrapContactRate = new List<WrapperContactRate>();
        fillList();
        blnHideInactive = false;
        blnShowInactive = true;
    }
    
    /***********************************************************************************
       Name: HideInactive
       Purpose: Used for Hide Inactive Rate Records on Team Structure Page.
   
    ************************************************************************************/
    
    Public Void HideInactive(){
        for(WrapperContactRate objWrapCR : lstWrapContactRate){
            List<wrpRate> lstRateTemp = new List<wrpRate>();
            if(objWrapCR.lstWrpRate.size() > 0){
                for(wrpRate objR : objWrapCR.lstWrpRate){
                    if(objR.objRate.Active__c == true){
                        lstRateTemp.add(objR);
                    }
                }
                objWrapCR.lstWrpRate = new List<wrpRate>();
                objWrapCR.lstWrpRate.addAll(lstRateTemp);
            }
        }
        blnHideInactive = true;
        blnShowInactive = false;
    }
   
    
    /***********************************************************************************
      WrapperClass :- WrapperContactRate
    ************************************************************************************/
    
    Public Class WrapperContactRate{
        Public Contact objContact{get;set;}    
        Public List<wrpRate> lstWrpRate{get;set;}
        
        Public WrapperContactRate(Contact objCon){                   
            this.objContact = objCon;
        }  
    }
    /***********************************************************************************
      WrapperClass :- wrpRate       
    ************************************************************************************/
    Public Class wrpRate{
        Public Rate__c objRate{get;set;}
        Public Decimal decdifference{get;set;}        
        Public String strRoleName{get;set;}
        Public Boolean blnShowRate{get;set;}  
        Public String strPendingCRID{get;set;} 
        Public wrpRate(Rate__c objRate){
            this.objRate = objRate;           
        }
    }
}