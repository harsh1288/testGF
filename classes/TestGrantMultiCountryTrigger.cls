@istest
public class TestGrantMultiCountryTrigger{

public static testMethod void TestgrntMultiCountrytrg(){
            
        Account objAcc = TestClassHelper.insertAccount();
         
        Country__c objCountry = TestClassHelper.insertCountry();
        
        Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
        insert objGrant;
        
        Implementation_Period__c objIP = TestClassHelper.createIP(objGrant, objAcc);
        insert objIP;
        
        Grant_Multi_Country__c objGrntMC = TestClassHelper.createGrntMultiCountry(objCountry.Id,objIP.Id );
        objGrntMC.Country__c = objCountry.Id;
        insert objGrntMC;
        
        Indicator__c objCatInd2 = TestClassHelper.createCatalogIndicator();
        objCatInd2.Indicator_Type__c = 'Impact';
        insert objCatInd2;
        
        
        
        Grant_Indicator__c objInd = TestClassHelper.createGrantIndicator();
        objInd.Grant_Implementation_Period__c= objIP.Id;
        objInd.Indicator_Type__c = 'Impact';
        objInd.Standard_or_Custom__c = 'Standard';
        objInd.Indicator__c = objCatInd2.Id;
        //objInd.Goal_Objective__c = objGoal.Id;
        objInd.Data_Type__c = 'Number';
        objInd.Reporting_Frequency__c = '12 Months';
        objInd.Country__c = objCountry.Id;
        objInd.Is_IP_Coverage_Indicator__c = true;
        insert objInd;
        
        Grant_Indicator__c objInd2 = TestClassHelper.createGrantIndicator();
        objInd.Grant_Implementation_Period__c= objIP.Id;
        objInd2.Indicator_Type__c = 'Coverage/Output';
        objInd2.Standard_or_Custom__c = 'Standard';
        objInd2.Is_IP_Coverage_Indicator__c = true;
        //objInd2.Indicator__c = objCatInd2.Id;
        //objInd2.Goal_Objective__c = objGoal.Id;
        objInd2.Data_Type__c = 'Number';
        objInd2.Reporting_Frequency__c = '12 Months';
        objInd2.Country__c = objCountry.Id;
        insert objInd2;
        
        delete objGrntMC;
        
    }
}