/**
* Test class:   TestGMbudgetTableext
*  DateCreated : 14/11/2014
----------------------------------------------------------------------------------
* Purpose/Methods:
* - Test class for GMbudgetTableext
----------------------------------------------------------------------------------
* History:
* - VERSION  DEVELOPER NAME    DATE            DETAIL FEATURES
     1.0        TCS           14/11/2014      INITIAL DEVELOPMENT
 */
@isTest
public class TestGMbudgetTableext {

  public static testMethod void GMbudgetTableext() {
        
    test.startTest();
        Account objAcc = TestClassHelper.insertAccount();
        //Period__c ObjRP = TestClassHelper.insertPeriod();
        
        Module__c ObjModule = TestClassHelper.createModule();
        objModule.Name = 'Test Module';
        insert ObjModule;
        
        Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
        insert objGrant;
        
        Implementer__c ObjImplementer = TestClassHelper.insertImplementor();
        
        Implementation_Period__c objIP = TestClassHelper.createIP(objGrant,objAcc);
        insert objIP;
        
        Performance_Framework__c pfObj = TestClassHelper.createPF(objIP);
        insert pfObj;
        
        IP_Detail_Information__c objIPDet = TestClassHelper.createIPDetail();
        objIPDet.Implementation_Period__c = objIP.Id;
        insert objIPDet;
        
        Grant_Intervention__c ObjGrantIntervention = TestClassHelper.createGrantIntervention(objIP);
        ObjGrantIntervention.Custom_Intervention_Name__c = 'Test Intervention';
        ObjGrantIntervention.Module__c = ObjModule.Id;
        insert ObjGrantIntervention;
        
        
        Grant_Intervention__c ObjGrantIntervention1 = TestClassHelper.createGrantIntervention(objIP);
        ObjGrantIntervention1.Module__c = ObjModule.Id;
        insert ObjGrantIntervention1;
        
        Catalog_Cost_Input__c ObjCatalogCostInput = TestClassHelper.insertCostInput();
        
        Profile_Access_Setting__c objPAS = TestClassHelper.createProfileSetting();
        objPAS.Salesforce_Item__c = 'External Profile';
        objPAS.Page_name__c = 'GMbudgetTable';
        objPAS.Profile_Name__c = 'PR Admin';
        insert objPAS;
        
        Budget_Line__c ObjBudgetLine = TestClassHelper.createBudgetLine();
        ObjBudgetLine.Detailed_Budget_Framework__c = objIPDet.Id;
        ObjBudgetLine.Grant_Intervention__c = ObjGrantIntervention.id; 
        ObjBudgetLine.Payee__c = ObjImplementer.id;  
        ObjBudgetLine.Cost_Input__c = ObjCatalogCostInput.Id;
        ObjBudgetLine.Unit_Cost_Y1__c = 2;
        ObjBudgetLine.Unit_Cost_Y2__c = 2;
        ObjBudgetLine.Unit_Cost_Y3__c = 2;
        ObjBudgetLine.Unit_Cost_Y4__c = 2;
        ObjBudgetLine.Q1_Amount__c = 1;
        ObjBudgetLine.Q2_Amount__c = 2;
        ObjBudgetLine.Q3_Amount__c = 3;
        ObjBudgetLine.Q4_Amount__c = 4;
        ObjBudgetLine.Q5_Amount__c = 2;
        ObjBudgetLine.Q6_Amount__c = 3;
        ObjBudgetLine.Q7_Amount__c = 2;
        ObjBudgetLine.Q8_Amount__c = 4;
        ObjBudgetLine.Q9_Amount__c = 2;
        ObjBudgetLine.Q10_Amount__c =5;
        ObjBudgetLine.Q11_Amount__c = 1;
        ObjBudgetLine.Q12_Amount__c = 1;
        ObjBudgetLine.Q13_Amount__c = 7;
        ObjBudgetLine.Q14_Amount__c = 1;
        ObjBudgetLine.Q15_Amount__c = 2;
        ObjBudgetLine.Q16_Amount__c = 2;
        insert ObjBudgetLine;
        
        Budget_Line__c ObjBudgetLine1 = TestClassHelper.createBudgetLine();
        ObjBudgetLine1.Detailed_Budget_Framework__c = objIPDet.Id;
        ObjBudgetLine1.Grant_Intervention__c = ObjGrantIntervention1.id; 
        ObjBudgetLine1.Payee__c = ObjImplementer.id;  
        ObjBudgetLine1.Cost_Input__c = ObjCatalogCostInput.Id;
        ObjBudgetLine1.Unit_Cost_Y1__c = 2;
        ObjBudgetLine1.Unit_Cost_Y2__c = 2;
        ObjBudgetLine1.Unit_Cost_Y3__c = 2;
        ObjBudgetLine1.Unit_Cost_Y4__c = 2;
        ObjBudgetLine1.Q1_Amount__c = 1;
        ObjBudgetLine1.Q2_Amount__c = 2;
        ObjBudgetLine1.Q3_Amount__c = 3;
        ObjBudgetLine1.Q4_Amount__c = 4;
        ObjBudgetLine1.Q5_Amount__c = 2;
        ObjBudgetLine1.Q6_Amount__c = 3;
        ObjBudgetLine1.Q7_Amount__c = 2;
        ObjBudgetLine1.Q8_Amount__c = 4;
        ObjBudgetLine1.Q9_Amount__c = 2;
        ObjBudgetLine1.Q10_Amount__c =5;
        ObjBudgetLine1.Q11_Amount__c = 1;
        ObjBudgetLine1.Q12_Amount__c = 1;
        ObjBudgetLine1.Q13_Amount__c = 7;
        ObjBudgetLine1.Q14_Amount__c = 1;
        ObjBudgetLine1.Q15_Amount__c = 2;
        ObjBudgetLine1.Q16_Amount__c = 2;
        insert ObjBudgetLine1;
        
        Budget_Line__c ObjBudgetLine2 = TestClassHelper.createBudgetLine();
        ObjBudgetLine2.Detailed_Budget_Framework__c = objIPDet.Id;
        ObjBudgetLine2.Grant_Intervention__c = ObjGrantIntervention1.id; 
        ObjBudgetLine2.Payee__c = ObjImplementer.id;  
        ObjBudgetLine2.Cost_Input__c = ObjCatalogCostInput.Id;
        insert ObjBudgetLine2;
        
        Apexpages.currentpage().getparameters().put('id',objIP.Id);       
        ApexPages.StandardController sc = new ApexPages.StandardController(objIP);
        GMbudgetTableext objGMO = new GMbudgetTableext(sc);
        
        GMbudgetTableext.BLCost wraperblcost =  new GMbudgetTableext.BLCost();
        GMbudgetTableext.BLModule wrapperblmod = new GMbudgetTableext.BLModule();
        GMbudgetTableext.BLPayee wrapperblpay = new GMbudgetTableext.BLPayee();
      
        objGMO.checkProfile();    
        test.stopTest();
           
    }
   public static testMethod void NativebudgetTableext()
        
        {
        Account objAcc = TestClassHelper.insertAccount();
        //Period__c ObjRP = TestClassHelper.insertPeriod();
        
        Module__c ObjModule = TestClassHelper.createModule();
        objModule.Name = 'Test Module1';
        insert ObjModule;
        
        Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
        insert objGrant;
        
        Implementer__c ObjImplementer = TestClassHelper.insertImplementor();
        
        Implementation_Period__c objIP = TestClassHelper.createIP(objGrant,objAcc);
        insert objIP;
        
        Grant_Intervention__c ObjGrantIntervention = TestClassHelper.createGrantIntervention(objIP);
        ObjGrantIntervention.Module__c = ObjModule.Id;
        insert ObjGrantIntervention;
        
        Grant_Intervention__c ObjGrantIntervention2 = TestClassHelper.createGrantIntervention(objIP);
        ObjGrantIntervention2.Custom_Intervention_Name__c = 'Test';
        ObjGrantIntervention2.Module__c = ObjModule.Id;
        insert ObjGrantIntervention2;
        
        
        Catalog_Cost_Input__c ObjCatalogCostInput = TestClassHelper.insertCostInput();
        
        Profile_Access_Setting__c objPAS = TestClassHelper.createProfileSetting();
        objPAS.Salesforce_Item__c = 'External Profile';
        objPAS.Page_name__c = 'GMbudgetTable';
        objPAS.Profile_Name__c = 'PR Admin';
        insert objPAS;
        
        IP_Detail_Information__c objIPDet = TestClassHelper.createIPDetail();
        objIPDet.Implementation_Period__c = objIP.Id;
        insert objIPDet;
        
        HPC_Framework__c  objHPC = TestClassHelper.createHPC();
        objHPC.Grant_Implementation_Period__c = objIP.Id;
        Insert objHPC;
        
        Product__c objPR = new Product__c();
        objPR.Health_Products_and_Costs__c = objHPC.Id;
        objPR.Grant_Intervention__c = ObjGrantIntervention.Id;
        insert objPR;
        
        Product__c objPR2 = new Product__c();
        objPR2.Health_Products_and_Costs__c = objHPC.Id;
        objPR2.Grant_Intervention__c = ObjGrantIntervention2.Id;
        insert objPR2;
                
        Budget_Line__c ObjBudgetLine = TestClassHelper.createBudgetLine();
        ObjBudgetLine.Detailed_Budget_Framework__c = objIPDet.Id;
        ObjBudgetLine.Grant_Intervention__c = ObjGrantIntervention.id; 
        ObjBudgetLine.Payee__c = ObjImplementer.id;  
        ObjBudgetLine.Cost_Input__c = ObjCatalogCostInput.Id;
        ObjBudgetLine.Unit_Cost_Y1__c = 2;
        ObjBudgetLine.Unit_Cost_Y2__c = 2;
        ObjBudgetLine.Unit_Cost_Y3__c = 2;
        ObjBudgetLine.Unit_Cost_Y4__c = 2;
        ObjBudgetLine.Q1_Amount__c = 1;
        ObjBudgetLine.Q2_Amount__c = 2;
        ObjBudgetLine.Q3_Amount__c = 3;
        ObjBudgetLine.Q4_Amount__c = 4;
        ObjBudgetLine.Q5_Amount__c = 2;
        ObjBudgetLine.Q6_Amount__c = 3;
        ObjBudgetLine.Q7_Amount__c = 2;
        ObjBudgetLine.Q8_Amount__c = 4;
        ObjBudgetLine.Q9_Amount__c = 2;
        ObjBudgetLine.Q10_Amount__c =5;
        ObjBudgetLine.Q11_Amount__c = 1;
        ObjBudgetLine.Q12_Amount__c = 1;
        ObjBudgetLine.Q13_Amount__c = 7;
        ObjBudgetLine.Q14_Amount__c = 1;
        ObjBudgetLine.Q15_Amount__c = 2;
        ObjBudgetLine.Q16_Amount__c = 2;
        insert ObjBudgetLine;
        
        Budget_Line__c ObjBudgetLine2 = TestClassHelper.createBudgetLine();
        ObjBudgetLine2.Detailed_Budget_Framework__c = objIPDet.Id;
        ObjBudgetLine2.Grant_Intervention__c = ObjGrantIntervention2.id; 
        ObjBudgetLine2.Payee__c = ObjImplementer.id;  
        ObjBudgetLine2.Cost_Input__c = ObjCatalogCostInput.Id;
        ObjBudgetLine2.Unit_Cost_Y1__c = 2;
        ObjBudgetLine2.Unit_Cost_Y2__c = 2;
        ObjBudgetLine2.Unit_Cost_Y3__c = 2;
        ObjBudgetLine2.Unit_Cost_Y4__c = 2;
        ObjBudgetLine2.Q1_Amount__c = 1;
        ObjBudgetLine2.Q2_Amount__c = 2;
        ObjBudgetLine2.Q3_Amount__c = 3;
        ObjBudgetLine2.Q4_Amount__c = 4;
        ObjBudgetLine2.Q5_Amount__c = 2;
        ObjBudgetLine2.Q6_Amount__c = 3;
        ObjBudgetLine2.Q7_Amount__c = 2;
        ObjBudgetLine2.Q8_Amount__c = 4;
        ObjBudgetLine2.Q9_Amount__c = 2;
        ObjBudgetLine2.Q10_Amount__c =5;
        ObjBudgetLine2.Q11_Amount__c = 1;
        ObjBudgetLine2.Q12_Amount__c = 1;
        ObjBudgetLine2.Q13_Amount__c = 7;
        ObjBudgetLine2.Q14_Amount__c = 1;
        ObjBudgetLine2.Q15_Amount__c = 2;
        ObjBudgetLine2.Q16_Amount__c = 2;
        insert ObjBudgetLine2;
        
        Apexpages.currentpage().getparameters().put('id',objIPDet.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(objIPDet);
        NativebudgetTableext objBL = new NativebudgetTableext(sc);
        //NativebudgetTableext.BLModule objblmod = new NativebudgetTableext.BLModule();
        objBL.checkProfile();
        } 
       
}