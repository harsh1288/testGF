public with sharing class prindicatorext {

  /* private ApexPages.StandardSetController standardController;
   public string indid{get;set;}
   public list<Grant_Intervention__c > lstgin;
   public list<wgintrvntn> wraplstgin{get;set;}
   public List<Id> prid;
   //public List<id> indid = new List<id>();
   public List<string> prname = new List<string>();
   public List<Implementation_Period__c> lstimp = new List<Implementation_period__c>();
   public List<Module__c> lstmod = new List<Module__c>();
   public string cnid;
   public List<Grant_Indicator__c> lstind{get;set;}
   public boolean display{get;set;}
   public boolean display1{get;set;}
   public boolean display2{get;set;}
   public string message{get;set;}
   public List<RecordType> lstrec = new List<RecordType>();
   public List<Grant_Indicator__c> newlstind = new List<Grant_Indicator__c>();
   public boolean displayname{get;set;}
   //public list<Grant_Indicator__c> lstgind= new List<Grant_Indicator__c>();
   
      public prindicatorext(ApexPages.StandardController controller) {
          indid = ApexPages.currentpage().getparameters().get('id');
         // system.debug('@@@@'+indid);
          
          lstind =[select Id,Name,Indicator_Full_Name__c,Indicator__c,Standard_or_Custom__c,Component__c,Indicator_Type__c,Concept_Note__c,Parent_Module__c,principal_Recipient__c from Grant_Indicator__c where id = :indid];
         // system.debug('@@@@'+lstind);
                     cnid=lstind[0].Concept_Note__c;
           system.debug('@@@@'+cnid);          
          lstgin= [select id,Name,Module__c,CN_Intervention__c,CN_Intervention__r.Module_rel__c,PR__c,PR1__c,PR_Short_Name__c from Grant_Intervention__c where Module__c =: lstind[0].Parent_Module__c and Module__c != Null ];
           //system.debug('@@@@'+lstgin);
           if(lstgin.size()>0){
          if(lstgin[0].PR__c == Null){
            displayname=true;
          }
          else{
            displayname=false;
          }
          }
          if(lstgin.size()==0){
               display = true;
               display1=false;
               message = 'No Interventions are availale for this Module.Need to add Intervention and PR for Module';
            }
          
          // system.debug('@@@@'+lstind );
          // system.debug('@@@@'+lstgin); 
          else{
            display = false;
            display1= true;
            if(wraplstgin == Null){
              wraplstgin = new List<wgintrvntn>();
              for(Grant_Intervention__c  gin:lstgin){
                 wraplstgin.add(new wgintrvntn(gin));
              }
            }
          }
          // system.debug('@@@@'+wraplstgin);  
       lstrec = [Select Id,SobjectType,Name From RecordType where Name = 'Coverage/Output_IP' and SobjectType = 'Grant_Indicator__c'  limit 1];
          
    }
    /*public prindicatorext(ApexPages.StandardSetController standardController) {
        modid = ApexPages.currentpage().getparameters().get('id');
        system.debug('@@@@'+modid);
          lstmod =[select id,name,Concept_Note__c from Module__c where id =:modid];
            cnid=lstmod[0].Concept_Note__c;
            system.debug('@@@@'+cnid);
             for(Grant_Indicator__c ind: (List<Grant_Indicator__c>) standardController.getSelected()){
                  indid.add(ind.id);
             }
             system.debug('@@@@'+indid);
             if(indid.size() > 1){
               display = false;
               display1=true;
               message = 'select only one indicator';
             }
             else{
               display = true;
               display1=false;
             }
          lstind =[select Id,Name,Indicator__c,Standard_or_Custom__c,Component__c,Indicator_Type__c from Grant_Indicator__c where id in :indid];
          lstgin= [select id,Name,Module__c,CN_Intervention__c,CN_Intervention__r.Module_rel__c,PR__c,PR_Short_Name__c from Grant_Intervention__c where CN_Intervention__r.Module_rel__c =:modid  ];
           system.debug('@@@@'+lstind );
           system.debug('@@@@'+lstgin); 
            if(wraplstgin == Null){
              wraplstgin = new List<wgintrvntn>();
              for(Grant_Intervention__c  gin:lstgin){
                 wraplstgin.add(new wgintrvntn(gin));
              }
            }
           system.debug('@@@@'+wraplstgin);  
    }*/

    /*public prindicatorext(ApexPages.StandardController controller) {
    
          modid = ApexPages.currentpage().getparameters().get('id');
          lstgin= [select id,Name,Module__c,PR__c,PR_Short_Name__c from Grant_Intervention__c where Module__c =:modid  ];
            
            if(wraplstgin == Null){
              wraplstgin = new List<wgintrvntn>();
              for(Grant_Intervention__c  gin:lstgin){
                 wraplstgin.add(new wgintrvntn(gin));
              }
            }
          }*/
      /*    public pageReference save(){
              for(wgintrvntn gin: wraplstgin){
                   if(gin.selected == true){
                   prname.add(gin.grantintrvntn.PR1__c);
                   } 
              }
             // system.debug('@@@@'+prname);
              if(prname.Size() > 1){
               ApexPages.Message msg = new Apexpages.Message(ApexPages.Severity.Warning,'Select Only One PR' );
               ApexPages.addmessage(msg);
               return null; 
              }
              else if(prname.Size()==0){
                ApexPages.Message msg = new Apexpages.Message(ApexPages.Severity.Warning,'Select One PR' );
                ApexPages.addmessage(msg);
                return null;
              }
              else{
               //system.debug('@@@@'+prname);
              lstimp =[select id,Principal_Recipient__c,Principal_Recipient__r.Name from Implementation_Period__c where Concept_Note__c =: cnid And Principal_Recipient__r.Name In :prname];
               // system.debug('@@@@'+lstimp.size());
                for(Implementation_Period__c imp:lstimp){
                  Grant_Indicator__c objind = new Grant_Indicator__c();
                    objind.Indicator_Full_Name__c = lstind[0].Indicator_Full_Name__c;
                    objind.Indicator__c = lstind[0].Indicator__c;
                    objind.Component__c = lstind[0].Component__c;
                    objind.Indicator_Type__c = lstind[0].Indicator_Type__c;
                    objind.Grant_Implementation_Period__c =imp.id; 
                    objind.Standard_or_Custom__c =lstind[0].Standard_or_Custom__c;
                    objind.RecordTypeId = lstrec[0].id;
                    newlstind.add(objind);
                }
                //system.debug(''+newlstind.size());
                insert newlstind;
                
                for(Implementation_Period__c imp:lstimp){
                    lstind[0].Principal_recipient__c = imp.Principal_Recipient__c;
                }
                update lstind;
                
                
               pageReference pr = new pageReference('/'+indid );
               pr.setRedirect(true);  
               return pr;
               }
                
              
          }
          
         
          
          public class wgintrvntn{
           public Grant_Intervention__c grantintrvntn{get;set;}
           public boolean selected{get;set;}
             public wgintrvntn(Grant_Intervention__c gin){
                grantintrvntn = gin;
                selected = false;
            }
          }*/


    }