/*********************************************************************************
*  Class: ResetCurrentPreviousRateBatch
* Created by {DeveloperName}, {DateCreated 07/10/2014}
----------------------------------------------------------------------------------
* Purpose/Methods:
* - This class is use for reset current year rate and last year rate in all
    rate records of the rate object using some critearea.
----------------------------------------------------------------------------------
* Unit Test: {TestResetCurrentPreviousRateBatch}
----------------------------------------------------------------------------------
* History:
* - VERSION  DEVELOPER NAME    DATE            DETAIL FEATURES
    1.0      Name             07/10/2014     INITIAL DEVELOPMENT  
*********************************************************************************/
global class ResetCurrentPreviousRateBatch implements Database.Batchable<sObject> {
    global final String Query;

    global ResetCurrentPreviousRateBatch(String strQuery) {
        Query = strQuery;
    }
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(Query);
    }
    global void execute(Database.BatchableContext BC, List<Sobject> scope) {
        ResetCurrentPreviousRateBatch((List<Rate__c>)scope);
    }

    global void finish(Database.BatchableContext BC){

    }
    
    Public Void ResetCurrentPreviousRateBatch(List<Rate__c> lstRates){
        if(lstRates != null && lstRates.size() > 0){
            for(Rate__c objRate: lstRates){
                // Assign current year values to last year
                objRate.Last_Year_Active__c = objRate.Active__c;
                objRate.Last_Year_Daily_Rate__c = objRate.Rate__c;
                objRate.Last_Year_GF_Approved__c = objRate.GF_Approved__c;
                objRate.Last_Year_LFA_Location__c = objRate.LFA_Location__c;
                
                // Assign next year values to current year
                objRate.Active__c = objRate.Next_Year_Active__c;
                objRate.Rate__c = objRate.Next_Year_Daily_Rate__c;
                objRate.GF_Approved__c = objRate.Next_Year_GF_Approved__c;
                objRate.LFA_Location__c = objRate.Next_Year_LFA_Location__c;
            }
            
            update lstRates;
        }
    }
}