@istest
public class testIterationTasksControllerExtension {
    public static testmethod void testIterationTasksControllerExtension(){
        Iteration__c objitr = new Iteration__c();
        objitr.Name = 'sprint';
        insert objitr;
       
        Project_Overview__c objbo = new Project_Overview__c();
        objbo.Name = 'test';
        insert objbo;
        
        SS_Release__c objrelease = new SS_Release__c();
        insert objrelease;
        
        Work_Product__c objwp = new Work_Product__c();
         objwp.Iteration__c = objitr.Id;
         objwp.Project_Overview__c = objbo.Id;
         objwp.SS_Release__c = objrelease.id;
        objwp.Plan_Estimate__c = '2';
         insert objwp; 
        
        Work_Product_Task__c objwpt = new Work_Product_Task__c();
        objwpt.Work_Product__c = objwp.Id;
        insert objwpt;
        
        ApexPages.currentPage().getParameters().put('id',objitr.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(objwp);
        IterationTasksControllerExtension obj = new IterationTasksControllerExtension(sc);
        obj.saveEdits();
        
        delete objwp;
    }
  
}