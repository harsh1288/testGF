// Purpose     :  This Page is meant for Page: WPTMKeyActivityReport                     
// Date        :  12 March 2015
// Created By  :  TCS 
// Author      :  Gaurav Guleria
// Description :  This Page shows Workplan Tracking measure report embeded in Performance Framework
//                Standard Page.Displays Interventions with respective Activities/Milestone Targets/Reporting Periods
// Revision:   :  

public with sharing class WPTMKeyActivityReportExtensionA {
    public Performance_Framework__c performanceFrameworkObj;
    public Performance_Framework__c performanceFrameworkRec;
    public List<grantInterventionWrapper> displayList{get;set;}
    public String interventionName {get; set;}
    public String activityName {get; set;}
    public String moduleName {get; set;}
    public List<Milestone_Target__c> milestoneTargetList ;    
    public Id keyActivityId {get;set;}
    public String perdiodNumbers {get; set;}
    public List <Milestone_RP_Junction__c> milestoneRPJunctionList;
    public Id activityId{get; set;}
    public String profileName{get;set;}
    public boolean showWPTMVFComponent{get;set;}
    public Boolean showLinkedTo{get;set;}
    public WPTMSecurityObject  securityMatrixObj{get;set;}
    public String performanceFrameworkstatus{get;set;}
    public String contactId;
    public String performanceFrameworkaccountId;
    public Id profileId;
    Public Id loggedInUserId;
    public User userRec ;    
    public List<npe5__Affiliation__c> affiliationList;
    public WPTMKeyActivityReportExtensionA(ApexPages.StandardController controller){
        showLinkedTo=FALSE;
        showWPTMVFComponent=false;
        performanceFrameworkObj = (Performance_Framework__c) controller.getRecord();
        performanceFrameworkRec =[SELECT Implementation_Period__r.Principal_Recipient__c,Implementation_Period__r.GIP_Type__c,PF_Status__c FROM Performance_Framework__c 
                                WHERE id=:performanceFrameworkObj.ID] ;
        performanceFrameworkaccountId = performanceFrameworkRec.Implementation_Period__r.Principal_Recipient__c;
        performanceFrameworkstatus =  performanceFrameworkRec.PF_Status__c;
        if(performanceFrameworkRec.Implementation_Period__r.GIP_Type__c=='Multi-Country'){
            showLinkedTo=TRUE;            
        }
        loggedInUserId= userinfo.getUserId() ;
        profileId=userinfo.getProfileId();
        profileName = [Select Id,Name from Profile where Id=:profileId].Name;
        /***
        System.debug(performanceFrameworkstatus);
        if(profileName==system.Label.LFA_Portal_User){
           userRec = [SELECT contactId,AccountId FROM USER WHERE ID=:loggedInUserId ];
           contactId=userRec.contactId ; 
           affiliationList = new List<npe5__Affiliation__c>();
           //Fetch all Affiliations which belong to portal User
           affiliationList = [SELECT npe5__Organization__c,npe5__Contact__c,Access_Level__c FROM npe5__Affiliation__c 
                               WHERE npe5__Contact__c=:contactId];
           for(npe5__Affiliation__c aff:affiliationList){
               if(aff.npe5__Organization__c==performanceFrameworkaccountId){
                    profileName =  aff.Access_Level__c;  
               }
           }
        }
        **/
        securityMatrixObj = WPTMSecurityObject.XMLDomDocument(profileName,performanceFrameworkstatus); 
    }
    //Method to create new Key Activity record
    public PageReference addKeyActivity(){                
         PageReference pageRef = new PageReference('/apex/WPTMKeyActivity?Performance='+performanceFrameworkObj.Id);
        return pageRef;
    }
    //Method to redirect back to  Performance Framework record
    public PageReference backToPerformanceFramework(){ 
        if(profileName==System.Label.LFA_Portal_User){
            PageReference pageRef = new PageReference('/LFA/'+performanceFrameworkObj.Id);  
            return pageRef;  
        }
        else if(profileName==System.Label.PR_Admin || profileName==System.Label.Community_PR_Admin || 
            profileName==System.Label.PR_Read_Write_Edit || profileName==System.Label.Community_PR_Read_Write_Edit || 
            profileName==System.Label.PR_Read_Only || profileName==System.Label.Community_PR_Read_Only ){
            
            PageReference pageRef = new PageReference('/GM/'+performanceFrameworkObj.Id); 
            return pageRef;
        }
        else{       
            PageReference pageRef = new PageReference('/'+performanceFrameworkObj.Id); 
            return pageRef;
        }
        
    }
    //Method to redirect to respective Key Activity Standard Page on click of Key Activity hyperlink
    public PageReference openKeyActivity(){
        PageReference openKeyActivityPage = new PageReference('/'+keyActivityId);           
        return openKeyActivityPage;    
    }
    //Method to open Help Page
    public PageReference openHelp(){
        PageReference openHelpPage = new PageReference('/apex/WPTMSeeHelpPage');           
        return openHelpPage;    
    }
    //method to show grantInterventionWrapper Records
    public List<grantInterventionWrapper> getRecordsToDisplay() {
        displayList  = new List<grantInterventionWrapper>(); //set the displayList object to a new grantInterventionWrapper List
        Set<Id> keyActivityIdSet = new Set <Id>();          
        List<Key_Activity__c>  keyActivityList = new List<Key_Activity__c>([SELECT ID,Grant_Intervention__r.Catalog_Intervention__r.Is_Other_Intervrntion__c,Grant_Intervention__r.Module__r.Name, Activity_Description__c,
                                                Grant_Intervention__r.Custom_Intervention_Name__c,Grant_Intervention__c,Grant_Intervention__r.Name,(SELECT Id,MilestoneTitle__c 
                                                FROM Milestones_Targets__r ) FROM Key_Activity__c WHERE 
                                                Grant_Intervention__r.Performance_Framework__c =:performanceFrameworkObj.ID 
                                                ORDER BY Grant_Intervention__r.Name]);
        for(Key_Activity__c keyact :keyActivityList){
            keyActivityIdSet.add(keyact.Id) ;           
        }                  
        Map<ID,Milestone_Target__c> milestoneTargetId_milestoneTargetRecMap = new Map<ID,Milestone_Target__c>(
                                    [SELECT ID,MilestoneTitle__c,CreatedDate,Key_Activity__c,Key_Activity__r.Grant_Intervention__r.Name,Criteria__c,Key_Activity__r.Grant_Intervention__r.Module__r.Name,Key_Activity__r.Grant_Intervention__r.Implementation_Period__r.GIP_Type__c,Linked_To__c,                                    
                                    (SELECT Period_Number__c FROM Milestone_RP_Junction__r) FROM Milestone_Target__c WHERE Key_Activity__c IN:
                                    keyActivityIdSet ]
                                    );
        for(Key_Activity__c keyact: keyActivityList){
            milestoneTargetList = new List<Milestone_Target__c>();
            List<WPTMmilestoneWrapper> lstMileStoneList = new List<WPTMmilestoneWrapper>();
            interventionName = '';
            activityName = '';
            moduleName  = '';
            activityId = keyact.Id; 
            
            if(keyact.Grant_Intervention__r.Catalog_Intervention__r.Is_Other_Intervrntion__c==TRUE){
                if(keyact.Grant_Intervention__r.Custom_Intervention_Name__c!=NULL)
                    interventionName = interventionName + keyact.Grant_Intervention__r.Custom_Intervention_Name__c;
            }
            else{
                interventionName = interventionName + keyact.Grant_Intervention__r.Name;  
            }                             
            
            if(keyact.Activity_Description__c!=null){
                activityName = activityName+ keyact.Activity_Description__c ;                
            }
            if(keyact.Grant_Intervention__r.Module__r.Name!=null){
                moduleName = moduleName + keyact.Grant_Intervention__r.Module__r.Name;                
            }
            for(Milestone_Target__c milt:milestoneTargetId_milestoneTargetRecMap.Values()){
                if(milt.Key_Activity__c== keyact.Id){
                     lstMileStoneList.add(new WPTMmilestoneWrapper(milT));
                }
            }
            lstMileStoneList.sort();
            for(WPTMmilestoneWrapper miltwrp:lstMileStoneList){
                milestoneTargetList.add(miltwrp.milestone);            
            }
            displayList.add(new grantInterventionWrapper(interventionName,activityName,moduleName,activityId,milestoneTargetList));
        } 
        return displayList;//return the list of full records 
    }
    //Wrapper class to hold Intervention Name,Activity Name,MilestoneWrapper and Key Activity record Id
    public class grantInterventionWrapper{        
        public String interventionName{get; set;}//Intervention-
        public String activityName{get; set;}//activityName       
        public String moduleName{get; set;}//moduleName           
        public List<MilestoneWrapper> MilestoneTargetWrapperLst{get; set;}   
        public Id keyActivityId {get; set;}       
        public grantInterventionWrapper(String interventions, String activity,String module,Id activityId,List<Milestone_Target__c> milestoneTarget){  
            MilestoneTargetWrapperLst = new List<MilestoneWrapper>();
            this.interventionName = interventions;  
            this.keyActivityId = activityId;
            this.activityName = activity;
            this.moduleName = module;
            for(Milestone_Target__c milT: milestoneTarget){
                MilestoneWrapper obj = new MilestoneWrapper(milT);
                MilestoneTargetWrapperLst.add(obj);
            }
        }        
    }
    //Wrapper class to hold Milestone Targets and respective Reporting Period Numbers
    public class MilestoneWrapper{   
        public String MileStoneID{get; set;}
        public String MileStoneName{get; set;}
        public String CriteriaForCompletion{get; set;}
        public String linkedTo{get; set;}
        public String ReportingPeriods{get;set;}        
        public MilestoneWrapper(Milestone_Target__c milestoneTarget){              
            this.linkedTo = '';        
            this.MileStoneName = milestoneTarget.MilestoneTitle__c;         
            this.MileStoneID = milestoneTarget.Id;
            this.CriteriaForCompletion=milestoneTarget.Criteria__c;
            this.linkedTo=milestoneTarget.Linked_To__c;            
            String temp='';
            List<Double> reportingNumbers = new List<Double>();
            //create a String of reporting numbers separated by Commas          
            for(Milestone_RP_Junction__c obj:milestoneTarget.Milestone_RP_Junction__r){
                reportingNumbers.add(obj.Period_Number__c);
            }
            //sort reporting periods
            reportingNumbers.sort();
            for(Double val:reportingNumbers){
                temp+=val.Round()+',';
            }
            //Remove , from end of Reporting Periods
            this.ReportingPeriods=temp.removeEnd(',');            
        }        
    }
    
}