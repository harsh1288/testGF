@IsTest
public with sharing class WPTMMilestoneDetailsExtension_Test
{
    public static String profilename = 'LFA Portal User';
    
    static testmethod void testmethod_A(){ 
        
        Account tAcc=TestClassHelper.createAccount();
        insert tAcc;
        
        Contact tempContact = new Contact(); 
        tempContact.lastName = 'testContact 1';        
        tempContact.AccountId = tAcc.id;
        tempContact.Email = 'testAccount@test.com';
        insert tempContact;
        
        /*Profile p = [select Id,name from Profile where name=:profilename  limit 1];
        
        User newUser = new User(
        profileId = p.id,
        username = System.now().millisecond() + 'PortalUser@yahoo.com',
        email = 'portal@ff.com',
        emailencodingkey = 'UTF-8',
        localesidkey = 'en_US',
        languagelocalekey = 'en_US',
        timezonesidkey = 'America/Los_Angeles',
        alias ='nuser',
        lastname ='user',
        country = 'TestCountry',
        contactid = tempContact.id
        );
        insert newUser;
        */
        
        GroupMember grpMember = new GroupMember();
        
        Country__c cont = new Country__c();
        cont.Name = 'Test Country';
        cont.X200DEMA_Exch_Rate_in_EUR__c = 80;
        cont.X200DEMA_Exch_Rate_in_USD__c = 60;
        cont.X200DEMA_Exch_Rate_Last_Updated_Date__c = Date.Today()+50;
        insert cont;
        
        Grant__c tGrant=TestClasshelper.createGrant(tAcc);        
        insert tGrant;
        
        Implementation_Period__c tImpPeriod=TestClassHelper.createIP(tGrant,tAcc);      
        insert tImpPeriod;
        
        Performance_Framework__c tPF=TestClassHelper.createPF(tImpPeriod);
        tPF.PF_status__c='Not yet submitted';
        insert tPF;
        
        Period__c tPeriod= new Period__c();
        tPeriod.Country__c = cont.Name ;
        tPeriod.Performance_Framework__c=tPF.id;
        insert tPeriod;         
        
        Module__c tMod=TestClassHelper.createModule();
        tMod.Performance_Framework__c=tPF.id;
        insert tMod;
        
        Grant_Intervention__c tGi=TestClassHelper.createGrantIntervention(tImpPeriod);
        tGi.Module__c=tMod.id;
        insert tGi;
        
        Key_Activity__c tKact = new Key_Activity__c();
        // tKact.Grant_Implementation_Period__c=tImpPeriod.id;
        tKact.Activity_Description__c = 'Test';
        tKact.Grant_Intervention__c=tGi.id;
        //tKact.Performance_Framework__c=;        
        insert tKact;
        
        Key_Activity__c tKact2 = new Key_Activity__c();
        // tKact.Grant_Implementation_Period__c=tImpPeriod.id;
        tKact2.Activity_Description__c = 'Test2';
        tKact2.Grant_Intervention__c=tGi.id;
        //tKact.Performance_Framework__c=;        
        insert tKact2;
        
        //SELECT Criteria__c,Id,Key_Activity__c,Linked_To__c,MilestoneTitle__c,Name,Reporting_Period__c FROM Milestone_Target__c
        
        Milestone_Target__c tMtc=new Milestone_Target__c();
        tMtc.Key_Activity__c=tKact.id;
        tMtc.MilestoneTitle__c='TestTitle';
        tMtc.Criteria__c ='testCriteria';
        tMtc.Reporting_Period__c ='testreportingperiod';
        tMtc.Linked_To__c ='testlink';
        insert tMtc;
        
        Milestone_Target__c tMtc1=new Milestone_Target__c();
        tMtc1.Key_Activity__c=tKact.id;
        tMtc1.Criteria__c ='testCriteria';
        tMtc1.MilestoneTitle__c='TestTitle2';
        tMtc1.Reporting_Period__c ='testreportingperiod';
        tMtc1.Linked_To__c ='testlink2';
        insert tMtc1;
        
        Milestone_RP_Junction__c tMileJunc=new Milestone_RP_Junction__c(); 
        tMileJunc.Reporting_Period__c=tPeriod.id;
        tMileJunc.Milestone_Target__c=tMtc.id;
        insert tMileJunc;
        
        
        /*User thisUser = [ select Id from User where Id = :newUser.id ];
        System.runAs (thisUser) { */
        
        /* Group oGrp = new Group();
        oGrp.Name='Test';
        oGrp.Type='Regular';
        insert oGrp;
        
        grpMember.UserOrGroupId= thisuser.id;
        grpMember.GroupId = oGrp.Id;
        insert grpMember;*/
        
        //countryRecord.CT_Public_Group_ID__c = grpMember.GroupId;
        //insert countryRecord;
        
        
        Test.startTest(); 
        //List<Key_Activity__c> lstKeyActivity=[SELECT Global_Fund_Comments_to_LFA__c,Global_Fund_Comments_to_PR__c,Grant_Implementation_Period__c,Grant_Intervention__c,Id,LFA_Comments__c,Performance_Framework__c,PF_Status__c,PR_Comments__c,RecordTypeId FROM Key_Activity__c];
        //WPTM_MilestoneRPTriggerHandler WPTMTriggerHandler = new WPTM_MilestoneRPTriggerHandler();
        ApexPages.StandardController sc = new ApexPages.standardController(tKact);
        apexpages.currentpage().getparameters().put('selectedMilestone' , tMtc.id);
        WPTMMilestoneDetailsExtension.WrapperMilestoneRPCls wrapObject= new WPTMMilestoneDetailsExtension.WrapperMilestoneRPCls(true,tPeriod,tMtc);
        
        WPTMMilestoneDetailsExtension oWPTMMileStoneDetailExt=new WPTMMilestoneDetailsExtension(sc);
        oWPTMMileStoneDetailExt.cancelPopUp();
        oWPTMMileStoneDetailExt.fetchReportingPeriod(); 
        oWPTMMileStoneDetailExt.saveReportingPeriod();
        oWPTMMileStoneDetailExt.savingCustomRecord();
        oWPTMMileStoneDetailExt.AddRow();
        //apexpages.currentpage().getparameters().put('contextItem',null);
        oWPTMMileStoneDetailExt.confirmDeleteMilestone();
        apexpages.currentpage().getparameters().put('contextItem', tMtc.id);
        oWPTMMileStoneDetailExt.confirmDeleteMilestone();
        
        apexpages.currentpage().getparameters().put('selectedMilestone' , tMtc1.id);
        WPTMMilestoneDetailsExtension.WrapperMilestoneRPCls wrapObject2= new WPTMMilestoneDetailsExtension.WrapperMilestoneRPCls(false,tPeriod,tMtc1);
        oWPTMMileStoneDetailExt.fetchReportingPeriod();
        oWPTMMileStoneDetailExt.saveReportingPeriod(); 
        
        Test.stopTest();
         
    }
}