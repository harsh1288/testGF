public class setEmailContactHandler {
  
   
 
   @future
   public static void updateContact(List<id> Userlist) {
       list<Contact> ConUpdateList= new List<Contact>();
       Map<id,string> EmailMap = new Map<id,string>();
       
       list<user> UserinfoList = [Select id,contactid,email from User where id IN: Userlist ];
       
       for(user us: UserinfoList){
           EmailMap.put(us.Contactid,us.email);
       }
    
       List<Contact> ContactinfolIst = [Select id,LastName,Email from Contact where id IN: EmailMap.keyset() AND Email =: ''];
    
       for(Contact c: ContactinfolIst){
           string e = EmailMap.get(c.id);
           c.email = e;
           ConUpdateList.add(c);
       }
    
       if(ConUpdateList.size()>0)
           update ConUpdateList;
    }
}