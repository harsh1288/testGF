/*
* $Author:      $ TCS - Rahul Kumar
* $Description: $ Handler class for controlling save and new event redirection
* $Date:        $ 13-Nov-2014 
* $Revision:    $
*/
public class saveandNewHandler {

    public saveandNewHandler()
    {
        //Constructor
    }
   // Below method returns SObjectType when it's api name is passed
    public static sObject createObject(String typeName) 
    {
            Schema.SObjectType targetType = Schema.getGlobalDescribe().get(typeName);
            if (targetType == null) {
                // throw an exception
            }
            return targetType.newSObject(); 
    }
    // Below method returns whether Performance framework lookup is present for the object
    public static boolean pfTrue(String objLabel)
    {       
            boolean childOfPF = false;
            SObject sdu = createObject(objLabel);
            Map<String, SobjectField> fieldMap = sdu.getsObjectType().getDescribe().Fields.getMap(); 
             for(String f :  fieldMap.keySet())
             {
                if(fieldMap.get(f).getDescribe().getName() == 'Performance_Framework__c')
                {
                    childOfPF = true;
                }
             }
             return childOfPF;
     }
     // Below method returns Schema map of org which helps in finding keyprefix
     public static Map<String, String> schemaMap()
     {
        Map<String,Schema.SObjectType> gd;
        Map<String, String> keyPrefixMap;
        Set<String> keyPrefixSet;
        //Creating Map of KeyPrefix to get the sObjectName.
        gd = Schema.getGlobalDescribe();  
          
        // to store objects and their prefixes  
        keyPrefixMap = new Map<String, String>{};  
          
        //get the object prefix in IDs  
        keyPrefixSet = gd.keySet();  
          
        // fill up the prefixes map  
        for(String sObj : keyPrefixSet)  
        {  
            Schema.DescribeSObjectResult r =  gd.get(sObj).getDescribe();  
            String tempName = r.getName();  
            String tempPrefix = r.getKeyPrefix();             
            keyPrefixMap.put(tempName,tempPrefix);  
        }
        return keyPrefixMap;
     }
}