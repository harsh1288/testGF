public with sharing class PFTableExtClone{

    public List<TableWrapper> tbldata{get;set;}
    Public Map<String,List<MilestoneWrapper>> actMilestoneWrapperMap;
    Public List<MilestoneWrapper> wrapdata{get;set;}
    Public boolean showChildtbl{get;set;}
    Public List<Period__c> allRP{get;set;}
    public string actName{get;set;}
    public string actId{get;set;}
    public string strPFId{get;set;} 
        
    public PFTableExtClone(ApexPages.StandardController controller) {   
    //getting Implementation Period Id
    strPFId = Apexpages.currentpage().getparameters().get('id');   
    Map<Id,List<Key_Activity__c>> InterventionActivityMap = new Map<Id,List<Key_Activity__c>>();
    Map<Id,List<Milestone_Target__c>> activityMilestoneMap = new Map<Id,List<Milestone_Target__c>>();
    Map<Id,List<Period__c>> milestoneRPMap = new Map<Id,List<Period__c>>();   
    Set<Id> kaIds = new Set<Id>();
    Set<Id> mtIds = new Set<Id>();
    tbldata = new List<TableWrapper>();
    actMilestoneWrapperMap = new Map<String,List<MilestoneWrapper>>();
    wrapdata = new List<MilestoneWrapper>();
    showChildtbl = false;
    
//Start of filling all map and list variable for this performance framework context
     
    allRP = new List<Period__c>([Select Id,Start_Date__c,End_Date__c from Period__c where Performance_Framework__c =:strPFId and Is_Active__c = true order by Start_Date__c ]);    
    for(Grant_Intervention__c gi:[Select Id,Name,(Select Id,Name/*Activity_Title__c*/,Global_Fund_Comments_to_LFA__c,Global_Fund_Comments_to_PR__c,LFA_Comments__c,PR_Comments__c from Grant_Intervention__c.Key_Activities__r) from Grant_Intervention__c where Performance_Framework__c =:strPFId])
    {
        InterventionActivityMap.put(gi.id,gi.Key_Activities__r);
    }    
    for(Key_Activity__c ka:[Select Id from Key_Activity__c where Grant_Intervention__c IN:InterventionActivityMap.keyset()])
    {
        kaIds.add(ka.Id);
    }
    if(kaIds.size()>0)
    {
        for(Key_Activity__c  ka:[Select Id,Name/*Activity_Title__c*/,(Select Id,MilestoneTitle__c,Criteria__c from Key_Activity__c.Milestones_Targets__r) from Key_Activity__c where Id IN:kaIds])
        {
            activityMilestoneMap.put(ka.id,ka.Milestones_Targets__r);
        }
    }
    for(Milestone_Target__c mt:[Select Id from Milestone_Target__c where Key_Activity__c IN:activityMilestoneMap.keyset()])
    {
        mtIds.add(mt.Id);
    }
    if(mtIds.size()>0)
    {
       List<Milestone_RP_Junction__c> mrpJunctionRec = new List<Milestone_RP_Junction__c>
                                                     ([Select Id,Milestone_Target__c,Reporting_Period__c 
                                                       from Milestone_RP_Junction__c where Milestone_Target__c IN:mtIds]);                       
       if(mrpJunctionRec.size()>0)
       {   
           Set<Id> rpIds = new Set<Id>();
           for(Milestone_RP_Junction__c mrpJnc:mrpJunctionRec)
           {
               rpIds.add(mrpJnc.Reporting_Period__c);
           }
           if(rpIds.size()>0)
           {
               Map<Id,Period__c> rpMap = new Map<Id,Period__c>([Select Id,Start_Date__c,End_Date__c from Period__c where Id IN:rpIds order by Start_Date__c]);                      
               for(Milestone_RP_Junction__c mrpJnc:mrpJunctionRec)
               {
                   if(milestoneRPMap.containsKey(mrpJnc.Milestone_Target__c))
                   {
                       milestoneRPMap.get(mrpJnc.Milestone_Target__c).add(rpMap.get(mrpJnc.Reporting_Period__c));
                   }
                   else
                   {
                       milestoneRPMap.put(mrpJnc.Milestone_Target__c,new List<Period__c>{rpMap.get(mrpJnc.Reporting_Period__c)});
                   }
               }
           }
       }
    }
//Finish of filling all map and list variable for this performance framework context

// Start of Creating wrapper list to be shown in table

      List<Grant_Intervention__c>  AllInterventionsList = new List<Grant_Intervention__c>([Select Id,Name from Grant_Intervention__c where Performance_Framework__c =:strPFId order by Name]);            
      for(Grant_Intervention__c gi:AllInterventionsList)
        {  
            TableWrapper tempwrap  = new TableWrapper();                         
            List<Key_Activity__c> Intka  = new List<Key_Activity__c>(); 
            if(InterventionActivityMap.get(gi.Id).size()>0)
            {                   
                Intka = new List<Key_Activity__c>(InterventionActivityMap.get(gi.Id));
            }
            else
            {
                Intka.add(new Key_Activity__c(Activity_Description__c/*NameActivity_Title__c*/='-',Global_Fund_Comments_to_LFA__c='-',Global_Fund_Comments_to_PR__c='-',LFA_Comments__c='-',PR_Comments__c='-'));
            }
        tempwrap.Intervention = new Grant_Intervention__c(id=gi.id,Name=gi.Name);
        tempwrap.ActivitiesList = new List<Key_Activity__c>(Intka);        
        tbldata.add(tempwrap);
        }
// Finish of Creating wrapper list to be shown in table
        //check if active reporting periods exist
        if(allRP.size()>0)
        {
// Start of filling map of milestone and boolean for linked reporting periods
        
            Map<Id,List<boolean>> milestoneRPLinkMap = new Map<Id,List<boolean>>();
            for(Id i:milestoneRPMap.keyset())
            {
                List<boolean> lnkstat = new List<boolean>();
                List<Period__c> mtRps = new List<Period__c>(milestoneRPMap.get(i));
                Set<Id> resultIds = (new Map<Id,Period__c>(mtRps)).keySet();
                for(Period__c p:allRP)
                {
                   if(resultIds.contains(p.Id))
                   {
                       lnkstat.add(true);
                   }
                   else
                   {
                       lnkstat.add(false);
                   } 
                }
                milestoneRPLinkMap.put(i,lnkstat);
            }
        
// Finish of filling map of milestone and boolean for linked reporting periods
            for(Id inter:InterventionActivityMap.keyset())
            {
                if(InterventionActivityMap.get(inter).size()>0)
                {
                    for (Id i:(new Map<Id,Key_Activity__c>(InterventionActivityMap.get(inter))).keySet())
                    {                        
                        if(activityMilestoneMap.get(i).size()>0)
                        {    
                            List<MilestoneWrapper> mtwrapList = new List<MilestoneWrapper>();                    
                            for(Milestone_Target__c mil:activityMilestoneMap.get(i))
                            {
                                MilestoneWrapper mtwrap = new MilestoneWrapper(mil,milestoneRPLinkMap.get(mil.id));
                                mtwrapList.add(mtwrap);                    
                            }
                            actMilestoneWrapperMap.put(i,mtwrapList);
                        }
                        else
                        {
                            List<MilestoneWrapper> mtwrapList = new List<MilestoneWrapper>();
                            Milestone_Target__c mil = new  Milestone_Target__c(MilestoneTitle__c='-',Criteria__c='-');
                            List<boolean> dummylist = new List<boolean>();
                            for(Period__c p:allRP)
                            {
                                dummylist.add(false);
                            }
                            MilestoneWrapper mtwrap = new MilestoneWrapper(mil,dummylist);
                            mtwrapList.add(mtwrap);
                            actMilestoneWrapperMap.put(i,mtwrapList);
                        }
                    }
                }
                else
                {
                    Milestone_Target__c mil = new  Milestone_Target__c(MilestoneTitle__c='-',Criteria__c='-');
                    List<boolean> dummylist = new List<boolean>();
                    for(Period__c p:allRP)
                    {
                        dummylist.add(false);
                    }
                    List<MilestoneWrapper> mtwrapList = new List<MilestoneWrapper>();
                    MilestoneWrapper mtwrap = new MilestoneWrapper(mil,dummylist);
                    mtwrapList.add(mtwrap);
                    actMilestoneWrapperMap.put('dummy',mtwrapList);
                }
            }
        }                      
    }
    public void createWrapperForAct()
    {                  
            if(actId != '')
            {
                showChildtbl = true;
                wrapdata = new List<MilestoneWrapper>(actMilestoneWrapperMap.get(actId));
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO,'Table For: '+actName);
                ApexPages.addMessage(msg);
            }
            else
            {
                showChildtbl = false;
                ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.INFO,'You have Clicked on a blank row');
                ApexPages.addMessage(errorMsg);
            }
    }         
    public Class TableWrapper{
        Public Grant_Intervention__c Intervention {get;set;}
        Public List<Key_Activity__c> ActivitiesList {get;set;}        
    }
    public Class MilestoneWrapper{
        Public Milestone_Target__c mt{get;set;}
        Public List<boolean> linkedRp{get;set;}
        public MilestoneWrapper()
        {
        }
        public MilestoneWrapper(Milestone_Target__c mt,List<boolean> linkedRp)
        {
            this.mt = mt;
            this.linkedRp = linkedRp;
        }
    }
}