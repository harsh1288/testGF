// Purpose     :  This class is used to sort Milestone Target records used in WPTMKeyActivityReport Page                    
// Date        :  7 April 2015
// Created By  :  TCS 
// Author      :  Gaurav Guleria
// Description :  This class is used to sort Milestone Target records used in WPTMKeyActivityReport Page
// Revision:   :  

global class WPTMmilestoneWrapper implements Comparable {

    public Milestone_Target__c milestone;
    // Constructor
    public WPTMmilestoneWrapper (Milestone_Target__c milt) {
        milestone= milt;
    }
    global Integer compareTo(Object compareTo) {
        WPTMmilestoneWrapper compareToMilt = (WPTMmilestoneWrapper)compareTo;
        Integer returnValue = 0;
        //ORDER BY ASC
        if (milestone.CreatedDate  > compareToMilt.milestone.CreatedDate ) {
            
            returnValue = 1;
        } else if (milestone.CreatedDate < compareToMilt.milestone.CreatedDate) {
           
            returnValue = -1;
        }
        return returnValue;       
    }
}