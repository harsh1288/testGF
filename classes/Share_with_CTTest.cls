@isTest
Public Class Share_with_CTTest{
    Public static testMethod void Share_with_CTTest(){
        
        Account objAcc = new Account();
        objAcc.Name = 'Test Acc';
        insert objAcc;
        
        Account objAcc1 = new Account();
        objAcc1.Name = 'Test Acc1';
        objAcc1.ParentId = objAcc.Id;
        insert objAcc1;
        
        Contact objCon = new Contact();
        objCon.AccountId = objAcc.id;
        objCon.LastName = 'Test Lname';
        insert objCon;
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User newUser = new User(
        profileId = p.id,
        username = 'newUser@yahoo.com',
        email = 'pb@ff.com',
        emailencodingkey = 'UTF-8',
        localesidkey = 'en_US',
        languagelocalekey = 'en_US',
        timezonesidkey = 'America/Los_Angeles',
        alias='nuser',
        lastname='lastname',
        contactId = objCon.id
        );
        
        LFA_Work_Plan__c objWP = new LFA_Work_Plan__c();
        objWP.Name = 'Test WP';
        objWP.LFA__c = objAcc.Id;
        objWP.LFA_Access__c = 'Read Access';
        insert objWP;
        
        objWP.LFA__c = objAcc1.Id;
        update objWP;
        
        LFA_Work_Plan__c objWP1 = new LFA_Work_Plan__c();
        objWP1.Name = 'Test WP';
        objWP1.LFA__c = objAcc.Id;
        objWP1.LFA_Access__c = 'Read/Write Access';
        objWP1.Country_Team_can_edit__c = true;
        insert objWP1;
        
    }    
}