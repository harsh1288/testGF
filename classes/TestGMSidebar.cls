@isTest
Public Class TestGMSidebar{
    Public static testMethod void TestSidebar(){
        Account objAcc = new Account();
        objAcc.Name = 'Test Acc';
        insert objAcc;
        
        Template__c objTemplate = new Template__c(Name = 'Test Template');
        insert objTemplate;
        
        Concept_Note__c objCN = new Concept_Note__c();
        objCN.Name = 'Test CN';
        objCN.Component__c = 'Malaria';
        objCN.Language__c = 'ENGLISH';
        objCN.CCM_new__c = objAcc.Id;
        objCN.Number_of_Years__c = '3';
        objCN.Page_Template__c = objTemplate.Id;
        objCN.Indicative_Amount__c = 10;
        insert objCN;
        
        Page__c objPage = new Page__c();
        objPage.Concept_note__c = objCN.Id;
        insert objPage;
        
        Catalog_Module__c objCM = new Catalog_Module__c();
        objCM.Name = 'Test CM';
        insert objCM;
        
        Module__c objModule = new Module__c();
        objmodule.Name = 'Test';
        objModule.Component__c = 'Malaria';
        objModule.Concept_note__c = objCN.Id;
        objModule.Catalog_Module__c = objCM.id;
        insert objModule;
        
        
        GMSidebar objS = new GMSidebar();
        objS.strId = objPage.Id;
        objS.getParameters();
        
        GMSidebar objS1 = new GMSidebar();
        objS1.strId = objModule.Id;
        objS1.getParameters();
        
        GMSidebar objS2 = new GMSidebar();
        objS2.strId = objCN.Id;
        objS2.getParameters();
    }
    
    Public static testMethod void TestSidebar1(){
        Account objAcc = new Account();
        objAcc.Name = 'Test Acc';
        insert objAcc;
        
        Template__c objTemplate = new Template__c(Name = 'Test Template');
        insert objTemplate;
        
        Concept_Note__c objCN = new Concept_Note__c();
        objCN.Name = 'Test CN';
        objCN.Component__c = 'Malaria';
        objCN.Language__c = 'ENGLISH';
        objCN.CCM_new__c = objAcc.Id;
        objCN.Number_of_Years__c = '3';
        objCN.Page_Template__c = objTemplate.Id;
        objCN.Indicative_Amount__c = 10;
        insert objCN;
        
        Implementation_Period__c objIP = new Implementation_Period__c();
        objIP.Principal_Recipient__c = objAcc.id;        
        objIP.Length_Years__c = '4';
        insert objIP;
        
        Page__c objPage = new Page__c();
        objPage.Concept_note__c = objCN.Id;
        objPage.Implementation_Period__c = objIP.Id;
        insert objPage;
        
        Catalog_Module__c objCM = new Catalog_Module__c();
        objCM.Name = 'Test CM';
        insert objCM;
        
        Module__c objModule = new Module__c();
        objmodule.Name = 'Test';
        objModule.Component__c = 'Malaria';
        objModule.Concept_note__c = objCN.Id;
        objModule.Catalog_Module__c = objCM.id;
        insert objModule;
        
        
        GMSidebar objS = new GMSidebar();
        objS.strId = objPage.Id;
        objS.getParameters();
        
        Sidebar objS1 = new Sidebar();
        objS1.strId = objModule.Id;
        objS1.getParameters();
        
        Sidebar objS2 = new Sidebar();
        objS2.strId = objCN.Id;
        objS2.getParameters();
    }
}