@istest
public with sharing class testSaveAndNewGrantIntController {
   Public static testMethod void testSaveAndNewGrantIntController(){
      Account objacct = TestClassHelper.insertAccount();
      
      Grant__c objGrant =  TestClassHelper.createGrant(objacct);
      objGrant.Principal_Recipient__c = objacct.Id;
      
      Implementation_Period__c objimp = TestClassHelper.createIP(objGrant, objacct);
      objimp.Principal_Recipient__c = objacct.Id;
      objimp.Grant__c = objGrant.Id;
      insert objimp;
      
      Performance_Framework__c objpf = TestClassHelper.createPF(objimp);
      objpf.Implementation_Period__c = objimp.Id;
      insert objpf;
      
      Grant_Intervention__c objint = new Grant_Intervention__c();
      objint.Performance_Framework__c = objpf.id;
      objint.Implementation_Period__c = objimp.Id;
      insert objint;
      
      User objext = TestClassHelper.createExtUser();
      objext.username = 'test'+UserInfo.getOrganizationId() + System.now().millisecond()+'@yahoo.com'; 
      insert objext;
      
      string baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
      
      ApexPages.StandardController stdctrlr = new ApexPages.StandardController(objint);
      SaveAndNewGrantIntController obj = new SaveAndNewGrantIntController(stdctrlr);
      obj.createNewGrantIntUrl();
      
      system.runAs(objext){
        obj.createNewGrantIntUrl();
      }
      
      Grant_Intervention__c objint1 = new Grant_Intervention__c();
      objint1.Performance_Framework__c = objpf.id;
      objint1.Implementation_Period__c = objimp.Id;
      insert objint1;
      
      ApexPages.StandardController stdctrlr1 = new ApexPages.StandardController(objint1);
      SaveAndNewGrantIntController obj1 = new SaveAndNewGrantIntController(stdctrlr1);
      obj1.createNewGrantIntUrl();
      
      system.runAs(objext){
        ApexPages.currentPage().getParameters().put('retUrl',baseUrl);
        obj1.createNewGrantIntUrl();
      }
   }
}