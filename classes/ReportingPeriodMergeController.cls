/***********************************************************
* Created:27-Nov-2014
* Purpose: Controller for MergeReportingPeriods vf
* CreatedBy: Rahul Kumar
* Revision: 29-Dec-2014 added more due date fields
************************************************************/
public with sharing class ReportingPeriodMergeController
{
    private ApexPages.StandardSetController standardController;
    public List<wrapper> periodwrap{get;set;}
    public boolean shwmsg{get;set;}
    public boolean merged{get;set;}
    public List<Period__c> allRepPeriods;
    public integer perLength;
    Map<decimal,Period__c> srcTrgPeriodMap = new Map<decimal,Period__c>();

    public ReportingPeriodMergeController(ApexPages.StandardSetController standardController)
    {
        //Constructor        
        Id profileId=userinfo.getProfileId();
        String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
        List<String> prflist = new List<String>();
        prflist = Label.Profiles_With_Access_to_Merge_Periods.split(',');
        boolean accessible = false;
        for(String s:prflist)
        {
            if(profileName.equals(s))
            {
              accessible = true;  
            }
        }
        if(accessible == true)
        {
            this.standardController = standardController;
            periodwrap = new List<wrapper>(); 
            shwmsg = false; // used for rendering messages on page
            merged = false; // used for rendering blocks when merge is successful.     
            String pfId = ApexPages.currentPage().getParameters().get('id');
            allRepPeriods = new List<Period__c>([Select Reporting_Period_Detail__c ,Base_Frequency__c, Period_Number__c,Performance_Framework__c,Implementation_Period__c,ReportingFrequencyInMonths__c,Period_Length__c,Start_Date__c,End_Date__c,Audit_Report__c,AR_Due_Date__c,EFR__c,EFR_Due_Date__c,DR__c,Due_Date__c,PU_Due_Date__c from Period__c where Performance_Framework__c =:pfid order by Period_Number__c ]);
            if(allRepPeriods.size() > 0)
            {
                checkSize(allRepPeriods); // checks whether page has any records for merge if yes then calls respective methods
                                          // to create wrapper classes otherwise adds error messages
            }
            else
            {
                shwmsg = true;
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'No Reporting Periods are available to merge.Please choose Cancel to close window.')); 
            }
        }
        else
        {
            merged = true;
            shwmsg = true;
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'You don\'t have permission to use this functionality.Please choose Cancel to close window.')); 
        }
    }
    // method to check validity of selected records to merge and follow appropriate logic 
    public void mergeRepPeriods()
    { 
        List<wrapper> tempwrap = new List<wrapper>();          
        for(wrapper pw:periodwrap)
        {
            if(pw.selected == true)
            {
               tempwrap.add(pw); 
            }
        }
        if(!(tempwrap.size() > 0)) 
        {
            shwmsg = true;
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Please choose a record')); 
        }
        else if(tempwrap.size()==2)
        {
            if(String.valueOf(tempwrap[0].targetPrd.id).equals(String.valueOf(tempwrap[1].targetPrd.id)))
            {
                shwmsg = true;
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Please choose only one record, as there are only three reporting periods for this performance framework'));
            }
            else
            {
                mergeRecords(tempwrap);
            }
        }
        else
        {
            mergeRecords(tempwrap);
        }           
    }
    // method responsible for merging pairs of selected reporting periods
    public void mergeRecords(List<wrapper> tempwrap)
    {
        List<Period__c> insertRP = new List<Period__c>();
        List<Period__c> deleteRP = new List<Period__c>();
        for(wrapper tw:tempwrap)
        {
            deleteRP.add(tw.shortPrd);
            deleteRP.add(tw.targetPrd);
           if(tw.shortPrd.Period_Number__c ==1)
           {
               insertRP.add(new Period__c(Implementation_Period__c = tw.shortPrd.Implementation_Period__c,Type__c = 'Reporting',
                                          Performance_Framework__c = tw.shortPrd.Performance_Framework__c,
                                          Start_Date__c = tw.shortPrd.Start_Date__c,End_Date__c = tw.targetPrd.End_Date__c ,
                                          Period_Number__c = 1,PU__c = true,PU_Due_Date__c=tw.targetPrd.PU_Due_Date__c,
                                          Audit_Report__c = tw.targetPrd.Audit_Report__c,
                                          AR_Due_Date__c = tw.targetPrd.AR_Due_Date__c,
                                          EFR__c = tw.targetPrd.EFR__c,
                                          EFR_Due_Date__c= tw.targetPrd.EFR_Due_Date__c,
                                          DR__c = tw.targetPrd.DR__c,
                                          Due_Date__c= tw.targetPrd.Due_Date__c,
                                          Is_Active__c = true, 
                                          Base_Frequency__c = tw.targetPrd.Base_Frequency__c,
                                          Reporting_Period_Detail__c = tw.targetPrd.Reporting_Period_Detail__c));                                          
               for(Period__c p:allRepPeriods)
               {
                   if(p.Period_Number__c > 2)
                   {
                       p.Period_Number__c -=1;
                   }
               }
           }
           else
           {
               insertRP.add(new Period__c(Implementation_Period__c = tw.shortPrd.Implementation_Period__c,Type__c = 'Reporting',
                                          Performance_Framework__c = tw.shortPrd.Performance_Framework__c ,Start_Date__c = tw.targetPrd.Start_Date__c,
                                          End_Date__c =tw.shortPrd.End_Date__c ,Period_Number__c =tw.targetPrd.Period_Number__c ,PU__c =true,
                                          PU_Due_Date__c=tw.shortPrd.PU_Due_Date__c,
                                          Audit_Report__c = tw.shortPrd.Audit_Report__c,
                                          AR_Due_Date__c = tw.shortPrd.AR_Due_Date__c,
                                          EFR__c = tw.shortPrd.EFR__c,
                                          EFR_Due_Date__c= tw.shortPrd.EFR_Due_Date__c,
                                          DR__c = tw.shortPrd.DR__c,
                                          Due_Date__c= tw.shortPrd.Due_Date__c,
                                          Is_Active__c = true,
                                          Base_Frequency__c = tw.shortPrd.Base_Frequency__c,
                                          Reporting_Period_Detail__c = tw.shortPrd.Reporting_Period_Detail__c));
           }
        }   
        Savepoint sp = Database.setSavepoint();
        try
        {
            update allRepPeriods;
            insert insertRP;
            delete deleteRP;
            merged = true;                   
        }
        catch(Exception e)
        {
            System.debug('DML couldn\'t be performed due to ' +e);
            Database.rollback(sp);
            merged = false;
            shwmsg = true;
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.info,'There was some error in merging records.'));
        }
    }
    // wrapper class to construct object, instances of which can be shown on user interface
    public class wrapper    
    {       
        public boolean selected{get;set;}
        public Period__c shortPrd{get;set;}
        public Period__c targetPrd{get;set;}
        
        public wrapper(boolean selected,Period__c shortPrd,Period__c targetPrd)
        {
            this.selected = selected;
            this.shortPrd = shortPrd;
            this.targetPrd = targetPrd;            
        }   
    }
    // methods to check eligiblity of a record to have at least one pair of reporting periods to be merged otherwise adds error message.  
    public void checkSize(List<Period__c> prds)
    {
            // check whether short periods exist 
            List<Period__c> shrtprds = new List<Period__c>();            
            for(Period__c p:prds)
            {	
            	
                if(Integer.valueOf(p.Period_Length__c.replace('<','')) < p.ReportingFrequencyInMonths__c)
                {
                    shrtprds.add(p);
                }
                srcTrgPeriodMap.put(p.Period_Number__c,p);            
            }
            if(!(shrtprds.size() > 0))
            {
                shwmsg = true;
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'There are no short periods.Please choose Cancel to close window.')); 
            }
            else
            {
                // Check whether total periods are less than three
                if(prds.size() < 3)
                {
                    shwmsg = true;
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Total reporting periods should be more than three in numbers.Please choose Cancel to close window.')); 
                }
                else
                {                                   
                   periodwrap=createPeriodWrap(shrtprds);
                   if(!(periodwrap.size() > 0))
                   {
                      shwmsg = true;
                      ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'No periods can be merged.Please choose Cancel to close window.'));  
                   }                
                }
            }
    }
   // method to construct wrapper list to be shown on user interface
    public List<wrapper> createPeriodWrap(List<Period__c> shrtprds)
    {
        List<wrapper> temp = new List<wrapper>();
        for(Period__c sp:shrtprds)
        {
            if(sp.Period_Number__c ==1)
            {
                // A period can\'t be of more than 18 months in length
                if(!((Integer.valueOf(sp.Period_Length__c.replace('<','')) + Integer.valueOf(srcTrgPeriodMap.get(2).Period_Length__c.replace('<','')))> 18))
                {
                    temp.add(new wrapper(false,sp,srcTrgPeriodMap.get(2)));
                }
            }
            else
            {
                if(!((Integer.valueOf(sp.Period_Length__c.replace('<','')) + Integer.valueOf(srcTrgPeriodMap.get(srcTrgPeriodMap.size()-1).Period_Length__c.replace('<','')))> 18))
                {
                    temp.add(new wrapper(false,sp,srcTrgPeriodMap.get(srcTrgPeriodMap.size()-1)));
                }
            }
        }        
        return temp;
    }
}