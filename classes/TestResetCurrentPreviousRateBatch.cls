/*********************************************************************************
* Test Class: {TestResetCurrentPreviousRateBatch}
*  DateCreated : 07/10/2014
----------------------------------------------------------------------------------
* Purpose/Methods:
* - Used for cover all methods of ResetCurrentPreviousRateBatch.
----------------------------------------------------------------------------------
* History:
* - VERSION  DEVELOPER NAME    DATE            DETAIL FEATURES
     1.0                      07/10/2014      INITIAL DEVELOPMENT
*********************************************************************************/
@IsTest
global class TestResetCurrentPreviousRateBatch { 
    global static testMethod void TestResetCurrentPreviousRateBatch() {
        Account objAcc = new Account();
        objAcc.Name = 'TestAcc';
        Insert objAcc;
        
        Country__c objCountry = new Country__c();
        objCountry.Name = 'TestCountry';
        objCountry.LFA__c = objAcc.Id;
        Insert objCountry;
        
        Contact objContact = new Contact();
        objContact.FirstName = 'TestFirst';
        objContact.LastName = 'TestLast';
        objContact.Email = 'test@test.com';
        objContact.AccountId = objAcc.Id;
        objContact.Country__c = objCountry.Id;
        Insert objContact;
        
        Rate__c objRate = new Rate__c();
        objRate.Rate__c = 100;
        objRate.Active__c = true;
        objRate.LFA_Location__c = 'In-Country';
        objRate.GF_Approved__c = 'Approved';
        objRate.Contact__c = objContact.Id;
        objRate.Country__c = objCountry.Id;
        Insert objRate;
        
        String s = 'Select Id,Name,Difference__c,Approval_Process_Type__c,Contact__c,Country__c,Active__c,Rate__c,GF_Approved__c,LFA_Location__c,Last_Year_Active__c,Last_Year_Daily_Rate__c,Last_Year_GF_Approved__c,Last_Year_LFA_Location__c,LFA_of_contact__c,LFA_Role__c,Next_Year_Active__c,Next_Year_Daily_Rate__c,Next_Year_GF_Approved__c,Next_Year_LFA_Location__c,Other_LFA_Role__c From Rate__c';
        List<Sobject> scope = [Select Id,Name,Difference__c,Approval_Process_Type__c,Contact__c,Country__c,Active__c,Rate__c,GF_Approved__c,LFA_Location__c,Last_Year_Active__c,Last_Year_Daily_Rate__c,Last_Year_GF_Approved__c,Last_Year_LFA_Location__c,LFA_of_contact__c,LFA_Role__c,Next_Year_Active__c,Next_Year_Daily_Rate__c,Next_Year_GF_Approved__c,Next_Year_LFA_Location__c,Other_LFA_Role__c From Rate__c];
        ResetCurrentPreviousRateBatch objResetCurrentPreviousRateBatch = new ResetCurrentPreviousRateBatch(s);
        objResetCurrentPreviousRateBatch.start(null);
        objResetCurrentPreviousRateBatch.execute(null,(List<Rate__c>) scope);
        objResetCurrentPreviousRateBatch.finish(null);
    }
}