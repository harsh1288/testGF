public class NewCNtoModule_Mobile_Ext {

public id cnpageid;

    public NewCNtoModule_Mobile_Ext(ApexPages.StandardController controller) {
        cnpageid = Apexpages.currentpage().getparameters().get('id');
    }
    
    public Pagereference NewModulePage(){
        Pagereference pr = new Pagereference('/apex/NewModule?id='+cnpageid);
        pr.setRedirect(true);
        return pr;
    }
}