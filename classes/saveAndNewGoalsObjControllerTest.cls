/*********************************************************************************
* {Test} Class: {saveAndNewGoalsObjControllerTest}
* Created by {Rahul Kumar},  {DateCreated 19-Nov-2014}
----------------------------------------------------------------------------------
* Purpose/Methods:
* - Used for cover all methods of saveAndNewGoalsObjController Controller.* 
*
----------------------------------------------------------------------------------
* History:
* - VERSION    DEVELOPER NAME     DATE      DETAIL FEATURES
      1.0       Rahul Kumar    19-Nov-2014
*********************************************************************************/
@isTest
public class saveAndNewGoalsObjControllerTest {
    static saveAndNewGoalsObjController ext;
    static Goals_Objectives__c masterObject;
    static PageReference pref;
    static PageReference prefTypeNull;
    static User testUser = TestClassHelper.createExtUser();
    Public static testMethod Void TestSaveAndNew(){
    testUser.username = 'savegoals1@example.com';
     insert testUser;
            Test.startTest();
            masterObject = TestClassHelper.createGoalsObjectives();
            Account acc = TestClassHelper.insertAccount();
            Grant__c grnt = testClassHelper.insertGrant(acc);
            Implementation_Period__c ip = TestClassHelper.createIP(grnt,acc);
            insert ip;
            Performance_Framework__c pf = TestClassHelper.createPF(ip);
            insert pf;
            pref = Page.newGoalsObjRediection;
            pref.getParameters().put('type','Goal');
            pref.getParameters().put('ipId',ip.id);
            pref.getParameters().put('pfId',pf.id);
            Test.setCurrentPage(pref);
            ApexPages.StandardController con = new ApexPages.StandardController(masterObject);
            ext = new saveAndNewGoalsObjController(con);
            ext.createNewGobjurl();
            prefTypeNull = Page.newGoalsObjRediection;
            prefTypeNull.getParameters().put('type','Objective');
            prefTypeNull.getParameters().put('ipId',ip.id);
            prefTypeNull.getParameters().put('pfId',pf.id);
            Test.setCurrentPage(prefTypeNull);
            System.runAs(testUser) {                                    
            ext.createNewGobjurl();            
        }
            prefTypeNull = Page.newGoalsObjRediection;          
            Test.setCurrentPage(prefTypeNull);
            prefTypeNull.getParameters().put('retUrl','GM%2F');
            ext.createNewGobjurl();
            System.runAs(testUser) {                                    
            ext.createNewGobjurl();            
        }
            prefTypeNull.getParameters().put('retUrl','GM%2F');
            System.runAs(testUser) {                                    
            ext.createNewGobjurl();            
        }  
            Test.setCurrentPage(prefTypeNull);
            prefTypeNull.getParameters().put('retUrl','?');         
            System.runAs(testUser) {                                    
            ext.createNewGobjurl();            
        }
            Test.stopTest();
    }
}