/*********************************************************************************
* {Controller} Class: {CreateInvoice}
* DateCreated  : 02/13/2014
----------------------------------------------------------------------------------
* Purpose/Methods:
* - This controller used for Create Invoice page which opens through a button on work plan 
    and will be used for create service invoice records using selected services.
* 
* Unit Test: TestCreateInvoice
----------------------------------------------------------------------------------
* History:
* - VERSION  DEVELOPER NAME    DATE            DETAIL FEATURES
    1.0                      02/13/2014      INITIAL DEVELOPMENT
*********************************************************************************/

Public class CreateInvoice{

    

    Public LFA_Invoice__c objInvoice {get; set;}
    Public Boolean blnShowService {get; set;}
    Public List<LFA_Invoice__c> lstInvoice {get; set;}
    Public List<Service_Invoice__c> lstServiceInvoice {get; set;}
    Public Map<Id,Id> mapServiceInvoiceIds {get; set;} //Key(ID)= service id, value(ID)=invoice id // ServiceID => Invoice ID
    Public List<LFA_Service__c> lstService {get; set;}
    Public String WPID {get; set;}
    Public String InvoiceId {get; set;}
    Public Decimal decTotalValue {get; set;}
    Public Decimal decPDFTotalValue {get; set;}
    Public String strTotalValue {get; set;}
    Public Boolean blnShowPageBlock {get; set;}
    Public Boolean blnShowPDF {get; set;}
    
    
    Public String strWorkPlanId {get; set;}
    Public String strInvoiceId {get; set;}
    Public Decimal intBudgeted_Labor {get; set;}
    Public Decimal intTotal_Budget {get; set;}
    Public Decimal intLFA_Proposed_Total {get; set;}
    Public List<wrapServiceType> lstwrapServicesType {get; set;}
    Public Decimal CurrentLOE {get; set;}
    Public Decimal CurrentCost {get; set;}
    Public Decimal TGFProposedLOE {get; set;}
    Public Decimal TGFProposedCost {get; set;}
    Public Decimal LFAProposedLOE {get; set;}
    Public Decimal LFAProposedCost {get; set;}
    Public Decimal POLOE {get; set;}
    Public Decimal POCost {get; set;}
    Public List<SelectOption> ServiceTypeOptions {get; private set;}
    Public List<SelectOption> ServiceSubTypeOptions {get; private set;}
    Public List<SelectOption> ServiceSubTypeFilterOptions {get; private set;}
    Public List<SelectOption> ServiceSubTypeSearchingOptions {get; private set;}
    Public List<String> lstServiceType {get; set;}
    Public Boolean blnShowCTPlanned {get; set;}
    Public Boolean blnShowTGF {get; set;}
    Public Boolean blnShowLFAprop{get; set;}
    Public Boolean blnShowPOLFAprop {get; set;}
    Public Integer intTemp {get;set;}
    Public String strMessage {get;set;}
    Public List<LFA_Work_Plan__c> lstWorkPlanToUpdate {get;set;}
    Public List<LFA_Work_Plan__c> lstWorkPlan;
    Public LFA_Service__c objServiceForType{get;set;}
    Public List<UserRecordAccess> lstshare {get;set;}
    Public String ProfileName{get;set;}
    Public String ServiceIDApproval{get;set;}
   
    /********************************************************************
        Constructor
    ********************************************************************/
    public CreateInvoice(ApexPages.StandardController controller) {
        strInvoiceId = Apexpages.currentpage().getparameters().get('InvoiceId');
        strWorkPlanId = Apexpages.currentpage().getparameters().get('id');
        String strViewPDF = Apexpages.currentpage().getparameters().get('ViewPdf');
        blnShowService = false;
        blnShowPageBlock = true;
        
        if(String.IsBlank(strViewPDF) == false){
            blnShowPDF = true;
        }else{
            blnShowPDF = false;
        }
        system.debug('@@##@@blnShowPDF: '+blnShowPDF);
        //lstInvoice = [Select Id,name,Type__c,Comments__c,Isupplier_Date__c,LFA_Work_Plan__c,Status__c,Total_Value__c,Status_Portal__c From LFA_Invoice__c Where LFA_Work_Plan__c =: strWorkPlanId];
        lstInvoice = [Select Id From LFA_Invoice__c Where LFA_Work_Plan__c =: strWorkPlanId];
        system.debug('@@@@@lstInvoice: '+lstInvoice);
        lstServiceInvoice = [Select LFA_Service__c, LFA_Invoice__c From Service_Invoice__c Where LFA_Invoice__c IN : lstInvoice];
        system.debug('@@@@@lstServiceInvoice: '+ lstServiceInvoice);
        mapServiceInvoiceIds= new Map<Id,Id>();
        if(lstServiceInvoice.size() > 0){
            for(Service_Invoice__c objSI : lstServiceInvoice){
                mapServiceInvoiceIds.put(objSI.LFA_Service__c,objSI.LFA_Invoice__c);
            }
        }
        System.debug('@@@setServiceIds: '+mapServiceInvoiceIds);
        
        if(strInvoiceId != null && strInvoiceId != ''){
            List<LFA_Invoice__c> lstgetInvoices;
            if(blnShowPDF == true){
                lstgetInvoices = [Select Id,Name,Comments__c,Isupplier_Date__c,iSupplier_Value__c,Status__c,Total_Value__c,Type__c,Status_Portal__c,LFA_Work_Plan__c,LFA_Work_Plan__r.name,LFA_Work_Plan__r.Labor_PO__c,LFA_Work_Plan__r.LFA__r.Parent_Account_Formula__c From LFA_Invoice__c Where Id =: strInvoiceId];
            }else{
                lstgetInvoices = [Select Id,Name,Comments__c,Isupplier_Date__c,iSupplier_Value__c,Status__c,Total_Value__c,Type__c,Status_Portal__c,LFA_Work_Plan__c,LFA_Work_Plan__r.name,LFA_Work_Plan__r.Labor_PO__c,LFA_Work_Plan__r.LFA__r.Parent_Account_Formula__c From LFA_Invoice__c Where Id =: strInvoiceId AND Status__c = 'Draft'];
            }
            system.debug('@@##@@lstgetInvoices: '+lstgetInvoices);
            if(lstgetInvoices != null && lstgetInvoices.size() > 0){
                objInvoice = lstgetInvoices[0];
                system.debug('@@##@@objInvoice: '+objInvoice);
                if(blnShowPDF == true){
                    if(objInvoice.Type__c == 'Services'){
                        blnShowService = true;
                        decPDFTotalValue = objInvoice.Total_Value__c;
                        
                    }else{
                        decPDFTotalValue = objInvoice.Total_Value__c;
                    }
                }else{
                    if(objInvoice.Type__c == 'Services'){
                        blnShowService = true;
                        decTotalValue = objInvoice.Total_Value__c;
                    }
                }
            }else{
                if(blnShowPDF == false){
                    blnShowPageBlock = false;
                    apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.Fatal,'This invoice cannot be viewed in draft mode as it has already been submitted. Please close this window and return to the work plan.');
                    apexpages.addmessage(msg);
                }
            }        
        }else{
            blnShowPageBlock = true;
            objInvoice = new LFA_Invoice__c();
            objInvoice.Isupplier_Date__c = System.NOW().date();
        }
        
        objServiceForType = new LFA_Service__c();
        lstwrapServicesType = new List<wrapServiceType>();
        blnShowLFAprop = true;
        blnShowTGF = true;
        blnShowPOLFAprop = true;
        intTemp = 1;
       
        FillServiceType();
        
        if(String.IsBlank(strWorkPlanId) == false){
            FillServices();
        }
    }
    
    /********************************************************************
        Name : FillServices
        Purpose : Used for fill services in service grid.
    ********************************************************************/
        
    Public Void FillServices(){
        Decimal decTotalActualCost = 0;
        lstService = new List<LFA_Service__c>();
        lstwrapServicesType = new List<wrapServiceType>();
        List<LFA_Service__c> lstServicesFromInvoice = new List<LFA_Service__c>();
        String strLFAStatus = 'TGF Agreed';
        String strStatus = 'Ready to Invoice';
        
        if(strInvoiceId != null && strInvoiceId != ''){
            if(blnShowPDF == true){
                lstServicesFromInvoice = [Select actual_LOE__c,actual_Cost__c,PO_LOE__c,PO_Cost__c,CT_Proposed_LOE__c,CT_Proposed_Cost__c,Id, Name,Alert_check_grant__c,Old_TGF_proposed_LOE__c,Old_TGF_proposed_Cost__c,is_LFA_LOE_Different__c,Is_LOE_different_LFA_perspective__c,Other_Sub_Category__c,LastmodifiedDate,LastmodifiedBy.Name,Has_Resorces__c,LOE_Difference__c,num_resources__c,has_TBD_contact__c,TBD_LOE_Zero__c,Grant__c, Grant__r.Start_Date__c, Grant__r.End_Date__c, Grant__r.Name,LOE__c,Cost__c,CT_Planned_Cost__c, CT_Planned_LOE__c,LFA_Work_Plan__r.Country__r.Name,Service_Type__c,Service_Sub_Type__c,LFA_Work_Plan__r.Country__c,Initial_TGF_Proposed_LOE__c,Initial_TGF_Proposed_Cost__c,TGF_Proposed_LOE__c,TGF_Proposed_Cost__c ,LFA_Proposed_Cost__c,TGF_Proposed_LOE_2__c,TGF_Proposed_Cost_2__c ,LFA_Proposed_LOE__c,Status__c,programmatic_start__c,programmatic_end__c,Period_Start__c, Period_End__c, Period_End_Year__c,Period_Start_Year__c,Due_Date__c,Forecasted_Start_Date__c,Forecasted_End_Date__c,Change_Request_Rationale__c, Comments__c,Grant__r.Grant_Status__c, Grant__r.Principal_Recipient__r.Name, Grant__r.Financial_and_Fiduciary_Risks__c,Grant__r.Health_Services_and_Products_Risks__c,Grant__r.Programmatic_and_Performance_Risks__c From LFA_Service__c Where ID IN(Select LFA_Service__c From Service_Invoice__c Where LFA_Invoice__c =: strInvoiceId)];
            }
        }
        
        String Query = 'Select actual_LOE__c,actual_Cost__c,PO_LOE__c,PO_Cost__c,CT_Proposed_LOE__c,CT_Proposed_Cost__c,Id, Name,Alert_check_grant__c,Old_TGF_proposed_LOE__c,Old_TGF_proposed_Cost__c,is_LFA_LOE_Different__c,Is_LOE_different_LFA_perspective__c,Other_Sub_Category__c,LastmodifiedDate,LastmodifiedBy.Name,Has_Resorces__c,LOE_Difference__c,num_resources__c,has_TBD_contact__c,TBD_LOE_Zero__c,Grant__c, Grant__r.Start_Date__c, Grant__r.End_Date__c, Grant__r.Name,LOE__c,Cost__c,CT_Planned_Cost__c, CT_Planned_LOE__c,LFA_Work_Plan__r.Country__r.Name,Service_Type__c,Service_Sub_Type__c,LFA_Work_Plan__r.Country__c,Initial_TGF_Proposed_LOE__c,Initial_TGF_Proposed_Cost__c,TGF_Proposed_LOE__c,TGF_Proposed_Cost__c ,LFA_Proposed_Cost__c,TGF_Proposed_LOE_2__c,TGF_Proposed_Cost_2__c ,LFA_Proposed_LOE__c,Status__c,programmatic_start__c,programmatic_end__c,Period_Start__c, Period_End__c, Period_End_Year__c,Period_Start_Year__c,Due_Date__c,Forecasted_Start_Date__c,Forecasted_End_Date__c,Change_Request_Rationale__c, Comments__c,Grant__r.Grant_Status__c, Grant__r.Principal_Recipient__r.Name, Grant__r.Financial_and_Fiduciary_Risks__c,Grant__r.Health_Services_and_Products_Risks__c,Grant__r.Programmatic_and_Performance_Risks__c From LFA_Service__c Where LFA_Work_Plan__c = :strWorkPlanId And Status__c = :strLFAStatus And Status__c != :strStatus Order by Period_Start__c';
        lstService = Database.Query(Query);
        
        if(blnShowPDF == true && lstServicesFromInvoice.size() > 0){
            lstService = new List<LFA_Service__c>();
            lstService.addAll(lstServicesFromInvoice);
        }
        for(String strServiceType:lstServiceType){
            wrapServiceType objwrapST = new wrapServiceType();
            objwrapST.lstwrapServices = new List<wrapServices>();
            objwrapST.ServiceType = strServiceType;
            if(lstService != null && lstService.size() > 0){
                for(LFA_Service__c objS : lstService){
                    if(objS.Service_Type__c == strServiceType){
                        objwrapST.blnExpandSection = true;
                        wrapServices objWrap = new wrapServices();
                        objWrap.objService = new LFA_Service__c();
                        objWrap.objService = objS;
                        if(mapServiceInvoiceIds.containsKey(objS.id)){
                            objWrap.blnIsChecked = true;
                            objWrap.strInvoiceID= mapServiceInvoiceIds.get(objS.Id);
                            decTotalActualCost += objS.actual_Cost__c;
                        }
                        else objWrap.blnIsChecked = false;
                        objwrapST.lstwrapServices.add(objWrap);
                    }
                }
            }
            lstwrapServicesType.add(objwrapST);
            decTotalValue = decTotalActualCost;
        }
        
        
            
        //CalculateTotal();
    }
    
    
    /********************************************************************
        Name : CalculateTotal
        Purpose : Used for calculate total of all services.
    ********************************************************************/
    
    /*Public void CalculateTotal(){
        if(lstwrapServicesType.size() > 0){
            CurrentLOE = null;
            CurrentCost = null;
            TGFProposedLOE = null;
            TGFProposedCost = null;
            LFAProposedLOE = null;
            LFAProposedCost = null;
            POLOE = null;
            POCost = null;
            List<Profile> PROFILE = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
             if(PROFILE.size() > 0){
                 ProfileName = PROFILE[0].Name;
             }
            for(wrapServiceType objWrapST : lstwrapServicesType){
                objWrapST.TotalCurrentLOE = null;
                objWrapST.TotalCurrentCost = null;
                objWrapST.TotalTGFProposedLOE = null;
                objWrapST.TotalTGFProposedCost = null;
                objWrapST.TotalLFAProposedLOE = null;
                objWrapST.TotalLFAProposedCost = null;
                objWrapST.TotalPOLOE = null;
                objWrapST.TotalPOCost = null;
                for(wrapServices objS : objWrapST.lstwrapServices){
                    if(ProfileName == 'LFA Portal User'){
                        if(objS.objService.TGF_proposed_LOE_2__c != null){                               
                            if(objWrapST.TotalCurrentLOE == null) objWrapST.TotalCurrentLOE = objS.objService.TGF_proposed_LOE_2__c;
                            else objWrapST.TotalCurrentLOE += objS.objService.TGF_proposed_LOE_2__c;
                        }
                    }else{
                        if(objS.objService.actual_LOE__c != null){
                            if(objWrapST.TotalCurrentLOE == null) objWrapST.TotalCurrentLOE = objS.objService.actual_LOE__c;
                            else objWrapST.TotalCurrentLOE += objS.objService.actual_LOE__c;
                        }
                    }                    
                    if(ProfileName == 'LFA Portal User'){
                        if(objS.objService.TGF_proposed_Cost_2__c != null){
                            if(objWrapST.TotalCurrentCost == null) objWrapST.TotalCurrentCost = objS.objService.TGF_proposed_Cost_2__c;
                            else objWrapST.TotalCurrentCost += objS.objService.TGF_proposed_Cost_2__c;
                        }
                    }else{
                        if(objS.objService.actual_Cost__c != null){
                            if(objWrapST.TotalCurrentCost == null) objWrapST.TotalCurrentCost = objS.objService.actual_Cost__c;
                            else objWrapST.TotalCurrentCost += objS.objService.actual_Cost__c;
                        }
                    }
                    if(objS.objService.Initial_TGF_Proposed_Cost__c != null){
                        if(objWrapST.TotalTGFProposedCost == null) objWrapST.TotalTGFProposedCost = objS.objService.Initial_TGF_Proposed_Cost__c;
                        else objWrapST.TotalTGFProposedCost += objS.objService.Initial_TGF_Proposed_Cost__c;
                    }
                    if(objS.objService.Initial_TGF_Proposed_LOE__c!= null){
                        if(objWrapST.TotalTGFProposedLOE == null) objWrapST.TotalTGFProposedLOE = objS.objService.Initial_TGF_Proposed_LOE__c;
                        else objWrapST.TotalTGFProposedLOE += objS.objService.Initial_TGF_Proposed_LOE__c;
                    }
                    if(objS.objService.LFA_Proposed_Cost__c != null){
                        if(objWrapST.TotalLFAProposedCost == null) objWrapST.TotalLFAProposedCost = objS.objService.LFA_Proposed_Cost__c;
                        else objWrapST.TotalLFAProposedCost += objS.objService.LFA_Proposed_Cost__c;
                    }
                    if(objS.objService.LFA_Proposed_LOE__c != null){
                        if(objWrapST.TotalLFAProposedLOE == null) objWrapST.TotalLFAProposedLOE = objS.objService.LFA_Proposed_LOE__c;
                        else objWrapST.TotalLFAProposedLOE += objS.objService.LFA_Proposed_LOE__c;
                    }
                    //PO
                    if(objS.objService.PO_Cost__c != null){
                        if(objWrapST.TotalPOCost == null) objWrapST.TotalPOCost = objS.objService.PO_Cost__c;
                        else objWrapST.TotalPOCost += objS.objService.PO_Cost__c;
                    }
                    if(objS.objService.PO_LOE__c != null){
                        if(objWrapST.TotalPOLOE == null) objWrapST.TotalPOLOE = objS.objService.PO_LOE__c;
                        else objWrapST.TotalPOLOE += objS.objService.PO_LOE__c;
                    }
                }
                if(objWrapST.TotalCurrentLOE != null){
                    if(CurrentLOE == null) CurrentLOE = objWrapST.TotalCurrentLOE;
                    else CurrentLOE += objWrapST.TotalCurrentLOE;
                }
                if(objWrapST.TotalCurrentCost != null){
                    if(CurrentCost == null) CurrentCost = objWrapST.TotalCurrentCost;
                    else CurrentCost += objWrapST.TotalCurrentCost;
                }
                if(objWrapST.TotalTGFProposedLOE != null){
                    if(TGFProposedLOE == null) TGFProposedLOE = objWrapST.TotalTGFProposedLOE;
                    else TGFProposedLOE += objWrapST.TotalTGFProposedLOE;
                }
                if(objWrapST.TotalTGFProposedCost != null){
                    if(TGFProposedCost == null) TGFProposedCost = objWrapST.TotalTGFProposedCost;
                    else TGFProposedCost += objWrapST.TotalTGFProposedCost;
                }
                if(objWrapST.TotalLFAProposedLOE != null){
                    if(LFAProposedLOE == null) LFAProposedLOE = objWrapST.TotalLFAProposedLOE;
                    else LFAProposedLOE += objWrapST.TotalLFAProposedLOE;
                }
                if(objWrapST.TotalLFAProposedCost != null){
                    if(LFAProposedCost == null) LFAProposedCost = objWrapST.TotalLFAProposedCost;
                    else LFAProposedCost += objWrapST.TotalLFAProposedCost;
                }
                //TOTAL PO
                if(objWrapST.TotalPOLOE != null){
                    if(POLOE == null) POLOE = objWrapST.TotalPOLOE;
                    else POLOE += objWrapST.TotalPOLOE;
                }
                if(objWrapST.TotalPOCost != null){
                    if(POCost == null) POCost = objWrapST.TotalPOCost;
                    else POCost += objWrapST.TotalPOCost;
                }
            }
            if(lstWorkPlanToUpdate!=null && lstWorkPlanToUpdate.size() > 0 ){
                intBudgeted_Labor = CurrentCost;
                if(ProfileName == 'LFA Portal User'){
                    intTotal_Budget = lstWorkPlanToUpdate[0].tgf_proposed_total__c;
                }else if(ProfileName != 'LFA Portal User' && CurrentCost!=null){
                    intTotal_Budget = CurrentCost + lstWorkPlanToUpdate[0].Budgeted_ODC__C;
                }
                if(LFAProposedCost != null){
                    intLFA_Proposed_Total = LFAProposedCost + lstWorkPlanToUpdate[0].Budgeted_ODC__C;
                }
                
            }
        }   
    }*/
    
    /********************************************************************
        Name : ShowServices
        Purpose : Used for show/hide services.
    ********************************************************************/
    
    Public void ShowServices(){
        if(objInvoice.Type__c == 'Services'){
            blnShowService = true;
        }else{
            blnShowService = false;
        }
    }
    
    /********************************************************************
        Name : SubmitInvoice
        Purpose : Used for create invoice record with status set to "Submitted".
                  Status of all selected services to "Ready to Invoice".
                  Service Invoice records are created for each of the selected services.
                  If unselect services, the service invoice records deleted,
                  and return to the workplan record.
    ********************************************************************/
    
    Public PageReference SubmitInvoice(){
        Set<Id> setUpdateServicesStatusIDs = new Set<Id>();
        Set<Id> setServiceIDsToDelete = new Set<Id>();
        Set<Id> setInvoiceIDsToDelete = new Set<Id>();
        List<LFA_Invoice__c> lstInsertInvoice = new List<LFA_Invoice__c>();
        if(objInvoice.Type__c == 'Services'){
            System.debug('$$$Type: '+objInvoice.Type__c);
            System.debug('$$$strTotalValue: '+decTotalValue);
            objInvoice.Total_Value__c = decTotalValue;
        }
        if(objInvoice.LFA_Work_Plan__c == null){
            objInvoice.LFA_Work_Plan__c = strWorkPlanId;
        }
        objInvoice.Status__c = 'Submitted';
        lstInsertInvoice.add(objInvoice);
        Upsert lstInsertInvoice;
        
        if(objInvoice.Type__c == 'Services'){
            List<Service_Invoice__c> lstInsertServiceInvoice = new List<Service_Invoice__c>();
            List<Service_Invoice__c> lstDeleteServiceInvoice = new List<Service_Invoice__c>();
            for(wrapServiceType strServiceType:lstwrapServicesType){
               for(wrapServices objS : strServiceType.lstwrapServices){    
                   if(objS.blnIsChecked == true){
                        if(mapServiceInvoiceIds.containsKey(objS.objService.Id) == false){
                            Service_Invoice__c objServiceInvoice = new Service_Invoice__c();
                            objServiceInvoice.LFA_Invoice__c = objInvoice.Id;
                            objServiceInvoice.LFA_Service__c = objS.objService.Id;
                            //setUpdateServicesStatusIDs.add(objS.objService.Id);
                            lstInsertServiceInvoice.add(objServiceInvoice);
                        }
                        setUpdateServicesStatusIDs.add(objS.objService.Id);
                    }
                    if(objS.blnIsChecked == false && mapServiceInvoiceIds.containsKey(objS.objService.Id)){
                        setServiceIDsToDelete.add(objS.objService.Id);
                        setInvoiceIDsToDelete.add(objS.strInvoiceID);
                    }
                }
            }
            Insert lstInsertServiceInvoice;
            lstDeleteServiceInvoice = [Select LFA_Service__c, LFA_Invoice__c From Service_Invoice__c Where LFA_Service__c IN : setServiceIDsToDelete AND LFA_Invoice__c IN : setInvoiceIDsToDelete];
            delete lstDeleteServiceInvoice;
        }
        System.debug('@@@setServiceIds: '+setUpdateServicesStatusIDs);
        List<LFA_Service__c> lstUpdateServicesStatus = [Select ID,Status__c From LFA_Service__c Where ID IN : setUpdateServicesStatusIDs];
        System.debug('@@@lstUpdateServicesStatus: '+lstUpdateServicesStatus);
        if(lstUpdateServicesStatus.size() > 0){
            for(LFA_Service__c objS : lstUpdateServicesStatus){
                objS.Status__c = 'Ready to Invoice';
            }
            update lstUpdateServicesStatus;
        }
        PageReference p = new PageReference('/'+strWorkPlanId);
        return p;
    }
    
    /********************************************************************
        Name : SaveDraft
        Purpose : Used for create invoice record with status set to "Draft".
                  Service Invoice records are created for each of the selected services.
                  If unselect services, the service invoice records deleted,
                  and return to the workplan record.
    ********************************************************************/
    
    Public PageReference SaveDraft(){
        Set<Id> setUpdateServicesStatusIDs = new Set<Id>();
        Set<Id> setServiceIDsToDelete = new Set<Id>();
        Set<Id> setInvoiceIDsToDelete = new Set<Id>();
        List<LFA_Invoice__c> lstInsertInvoice = new List<LFA_Invoice__c>();
        if(objInvoice.Type__c == 'Services'){
            System.debug('$$$Type: '+objInvoice.Type__c);
            System.debug('$$$strTotalValue: '+decTotalValue);
            objInvoice.Total_Value__c = decTotalValue;
        }
        if(objInvoice.LFA_Work_Plan__c == null){
            objInvoice.LFA_Work_Plan__c = strWorkPlanId;
        }
        objInvoice.Status__c = 'Draft';
        lstInsertInvoice.add(objInvoice);
        Upsert lstInsertInvoice;
        
        if(objInvoice.Type__c == 'Services'){
            List<Service_Invoice__c> lstInsertServiceInvoice = new List<Service_Invoice__c>();
            List<Service_Invoice__c> lstDeleteServiceInvoice = new List<Service_Invoice__c>();
            for(wrapServiceType strServiceType:lstwrapServicesType){
               for(wrapServices objS : strServiceType.lstwrapServices){    
                   if(objS.blnIsChecked == true){
                        if(mapServiceInvoiceIds.containsKey(objS.objService.Id) == false){
                            Service_Invoice__c objServiceInvoice = new Service_Invoice__c();
                            objServiceInvoice.LFA_Invoice__c = objInvoice.Id;
                            objServiceInvoice.LFA_Service__c = objS.objService.Id;
                            setUpdateServicesStatusIDs.add(objS.objService.Id);
                            lstInsertServiceInvoice.add(objServiceInvoice);
                        }
                    }
                    if(objS.blnIsChecked == false && mapServiceInvoiceIds.containsKey(objS.objService.Id)){
                        setServiceIDsToDelete.add(objS.objService.Id);
                        setInvoiceIDsToDelete.add(objS.strInvoiceID);
                    }
                }
            }
            Insert lstInsertServiceInvoice;
            lstDeleteServiceInvoice = [Select LFA_Service__c, LFA_Invoice__c From Service_Invoice__c Where LFA_Service__c IN : setServiceIDsToDelete AND LFA_Invoice__c IN : setInvoiceIDsToDelete];
            delete lstDeleteServiceInvoice;
        }
        PageReference p = new PageReference('/'+strWorkPlanId);
        return p;
    }
    
    /********************************************************************
        Name : CancelToWorkPlan
        Purpose : Used for return to workplan record.
    ********************************************************************/
    
    Public PageReference CancelToWorkPlan(){
        PageReference p = new PageReference('/'+strWorkPlanId);
        return p;
    }
    
    /********************************************************************
        Name : FillServiceType
        Purpose : Used for fill service type picklist.
    ********************************************************************/
    
    Public void FillServiceType(){
        ServiceTypeOptions = new List<SelectOption>();
        ServiceSubTypeOptions = new List<SelectOption>();
        ServiceSubTypeFilterOptions = new List<SelectOption>();
        ServiceSubTypeSearchingOptions  = new List<SelectOption>();
        lstServiceType = new List<String>();
        Schema.sObjectType objType = LFA_Service__c.getSObjectType();
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe(); 
        map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap(); 
        List<Schema.PicklistEntry> ServiceTypes = fieldMap.get('Service_Type__c').getDescribe().getPickListValues();
        List<Schema.PicklistEntry> ServiceSubTypes = fieldMap.get('Service_Sub_Type__c').getDescribe().getPickListValues();
        ServiceTypeOptions.add(new SelectOption('','All Service Types'));
        ServiceSubTypeSearchingOptions.add(new SelectOption('','All Service Sub Types'));
        ServiceSubTypeFilterOptions.add(new SelectOption('','All Service Sub Types'));
        for (Schema.PicklistEntry Entry : ServiceTypes){ 
            ServiceTypeOptions.add(new SelectOption(Entry.getLabel(), Entry.getValue()));
            lstServiceType.add(Entry.getValue());
        }
        for (Schema.PicklistEntry Entry : ServiceSubTypes){ 
            ServiceSubTypeOptions.add(new SelectOption(Entry.getLabel(), Entry.getValue()));
            ServiceSubTypeFilterOptions.add(new SelectOption(Entry.getLabel(), Entry.getValue()));
            ServiceSubTypeSearchingOptions.add(new SelectOption(Entry.getLabel(), Entry.getValue()));
        }
    }
    
    
    /********************************************************************
        Wrapper Class : wrapServices
    ********************************************************************/
    
    Public Class wrapServices{
        Public LFA_Service__c objService {get; set;}
        public Boolean blnIsChecked{get;set;}
        Public String strInvoiceID{get;set;}
        
        Public wrapServices(){
            this.blnIsChecked = false;
        }
    }
    
    /********************************************************************
        Wrapper Class : wrapServiceType
    ********************************************************************/
    
    Public Class wrapServiceType{
        Public String ServiceType {get;set;}
        Public List<wrapServices> lstwrapServices {get; set;}
        Public Boolean blnExpandSection {get; set;}
        Public Decimal TotalCurrentLOE {get; set;}
        Public Decimal TotalCurrentCost {get; set;}
        Public Decimal TotalTGFProposedLOE {get; set;}
        Public Decimal TotalTGFProposedCost {get; set;}
        Public Decimal TotalLFAProposedLOE {get; set;}
        Public Decimal TotalLFAProposedCost {get; set;}
        Public Decimal TotalPOLOE {get; set;}
        Public Decimal TotalPOCost {get; set;}
    }
}