public without sharing class RSVPextension {

    public Campaign campaign {get;set;}
    public CampaignMember campaignMember {get;set;}
    public Contact contact {get;set;}
    public Campaign camp {get;set;}
    List<CampaignMember> lstCampaignMember = new List<CampaignMember>();
    public Boolean blnDisplay {get;set;}
    public String strConId;
    public String strCampaignId;
    public List<Campaign> lstCampaign {get;set;}
    public String strAnyGuests {get;set;}
    public String strResponse {get;set;}
    public Campaign objCampaign {get;set;}
    Public boolean showMember {get;set;}
    Public boolean anyMembers {get;set;}
    public List<Guest__c> lstMembers{get;set;}
    public integer members {get;set;}
    public list<Guest__c> insertGuestList = new List<Guest__c>();
    

     public RSVPextension() {
         strCampaignId = ApexPages.currentPage().getParameters().get('Id');
         camp = [select Name,id,Guests_Permitted__c,maximum_Number_of_Guests_Permitted__c,Description from Campaign where ID=:strCampaignId ];
         if(ApexPages.currentPage().getParameters().get('ContactId') != NULL){
            strConId = ApexPages.currentPage().getParameters().get('ContactId'); }
         if(strConId != null){
            contact = [Select Name,lastName, Id from Contact where Id = :strConId]; }
            
         getCampaignMember();
         if(strCampaignId !=null)
            lstCampaign = [Select Id, maximum_Number_of_Guests_Permitted__c,Guests_Permitted__c  from campaign where Id=:strCampaignId];
         if(!lstCampaign.isEmpty()){
             objCampaign=lstCampaign[0];
         }
         anyMembers = false;
         
         
    }
 
    public void getCampaignMember(){
        if(contact != null && strCampaignId != null){
           blnDisplay = true;
        
           lstCampaignMember = [Select Id, ContactId, HasResponded,Contact.Name, Comments__c, Status, CampaignId, Number_of_Guests__c from CampaignMember where ContactId = :contact.Id AND CampaignId = :strCampaignId ];       
           if(!lstCampaignMember.isEmpty()){
               campaignMember = lstCampaignMember[0]; }
           else {
               campaignMember = new CampaignMember (ContactId = contact.Id, CampaignId = strCampaignId ); }
           }
           else {
               blnDisplay = false; }    
    }
    
    
    public List<Selectoption> getResponses(){
        List<Selectoption> lstResponses = new List<Selectoption>();
        lstResponses.add(new Selectoption('','--None--'));
        lstResponses.add(new Selectoption('Accepted','Yes'));
        lstResponses.add(new Selectoption('Declined','No'));     
        return lstResponses;
    }

    public List<Selectoption> getAnyGuests(){
        List<Selectoption> lstAnyGuests = new List<Selectoption>();
        lstAnyGuests.add(new Selectoption('','--None--'));
        lstAnyGuests.add(new Selectoption('Yes','Yes'));
        lstAnyGuests.add(new Selectoption('No','No'));     
        return lstAnyGuests;
    }

    public List<Selectoption> getMaxNumberOfGuests(){
        List<Selectoption> lstMaxGuests = new List<Selectoption>();
        lstMaxGuests.add(new Selectoption('','--None--'));
        if(objCampaign.Id!=null)
            for(integer  i = 1; i<=objCampaign.maximum_Number_of_Guests_Permitted__c; i++){
                lstMaxGuests.add(new Selectoption(String.ValueOf(i),String.ValueOf(i)));
            }
        return lstMaxGuests;
    }
       
    public void showMember(){
        if(strAnyGuests == 'Yes'){
            showMember = True;
        }
        else
            showMember = False;
    }   
    
    public void totalmembers(){
        members = integer.valueof(campaignMember.Number_of_Guests__c);
        lstmembers = new List<Guest__c>();
        for(integer p =0;p<members ; p++){
            Guest__c g = new Guest__c();
            lstMembers.add(g);  
        }
        system.debug('lstMembers**'+lstMembers);
        anyMembers = true;
        
    } 
    
    public pageReference SaveNew(){ 

        insertGuestList = new List<Guest__c>();
        Guest__c memberGuest = new Guest__c ();
        
        if(strResponse == NULL){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please indicate whether or not you will attend this event');
            ApexPages.addMessage(myMsg);
            return Null;
        }
        
         
         if(strAnyGuests=='Yes' && camp.Guests_Permitted__c ==true && campaignMember.Number_of_Guests__c ==null){
             ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please add the number of guests');
             ApexPages.addMessage(myMsg);
             system.debug('myMsg**'+myMsg); 
             return null;
         }
         else{
            if(strResponse == 'Accepted' && (strAnyGuests=='No' || strAnyGuests == NULL)){
                campaignMember.status = 'Accepted';
                
                
            }
            if(strResponse == 'Accepted' && strAnyGuests=='Yes') {
                campaignMember.status = 'Accepted – requested guests';
                
            }
            if(strResponse == 'Declined'){
                campaignMember.status = 'Declined';
            } 
            try{
                system.debug('Status'+campaignMember.status);
                 
                 
                 
                 
                 
                if(lstMembers != Null){
                    for(Guest__c Guest: lstMembers){
                        if(Guest.name!=Null){
                            system.debug('test');
                            Guest.Campaign__c = camp.id;
                            Guest.Contact__c = contact.id;
                            insertGuestList.add(Guest);
                        }
                        else{
                            ApexPages.Message myMsg1 = new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter name of the guest');
                            ApexPages.addMessage(myMsg1);
                            return null;
                            break;
                        }
                    }
                }
                upsert campaignMember;
                
                if (strResponse.contains('Accepted'))
                 {
                  memberGuest.Name = contact.Name; 
                  memberGuest.Campaign__c = camp.id;
                  memberGuest.Contact__c = contact.id;
                  insert memberGuest;
                  }
                
                if(insertGuestList != NULL)
                    insert insertGuestList;
    
                pagereference p = page.RSVP_AfterSubmit;
                 return p;
            }
            catch(Exception ex) {
                ApexPages.addMessages(ex); 
                system.debug('ex**'+ex);
                return Null;
            }
            

            //return new pageReference('https://theglobalfund1.force.com/RSVP/RSVP_AfterSubmit');
          }
     }
}