@isTest
Public class TestNotifyLFA{
    Public static testMethod Void TestNotifyLFA(){
        List<RecordType> lstRecordTypeAccount = [Select Id,DeveloperName from RecordType Where SObjectType = 'Account' AND DeveloperName= 'LFA' limit 1];
        Account objAccount = new Account();
        objAccount.RecordTypeId = lstRecordTypeAccount[0].Id;
        objAccount.Name = 'Test1';
        insert objAccount;
        Account objAccount2= new Account();
        objAccount2.RecordTypeId = lstRecordTypeAccount[0].Id;
        objAccount2.Name = 'Test2';
        objAccount2.ParentID = objAccount.ID;
        insert objAccount2;
        
        Contact objContact = new Contact();
        objContact.LastName = 'TestLast';
        objContact.AccountID = objAccount2.ID;
        objContact.Receive_PET_Alerts__c = true;
        insert objContact;
        
        
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com',ContactID = objContact.ID);
        
        
        
        Country__c objCountry = new Country__c();
        objCountry.Name ='India';
        objCountry.CT_Public_Group_ID__c = '00Gb0000000tjtJ';
        insert objCountry ;
    
        LFA_Role__c objLFARole = new LFA_Role__c(Name ='testlfarole',Default_Rate__c=5.6);
        insert objLFARole;
        
        Rate__c objRate = new Rate__c(LFA_Role__c =objLFARole.id,Rate__c = 4.5,Country__c= objCountry.id,Contact__c=objContact.id);
        insert objRate;
        
        Change_Request__c objChng = new Change_Request__c(Rationale__c = 'test',Rate__c = objRate.id,Proposed_Status__c='Active',Status__c = 'Closed by LFA Coordination');
        insert objChng;
        Change_Request_Review__c objccr = new Change_Request_Review__c(Change_Request__c = objChng.id,LFA_Coordination_Comments__c='sdjfhs');
        insert objccr;
        objChng.status__C = 'Approved';
        objChng.Proposed_Status__c='InActive';
        objChng.Rationale__c = 'test1';
        update objChng;
        Change_Request__c objChng1 = new Change_Request__c(Rationale__c = 'test',Rate__c = objRate.id,Status__c = 'Conditionally Approved',Proposed_Status__c='InActive');
        insert objChng1;
        objccr.Approval_Status__c = 'Approved';
        objccr.LFA_Coordination_Comments__c='dsmhfkjsadhfkhs';
        objccr.Comments__c='asdh';
        update objccr;
        objChng1.status__C = 'Approved';
        objChng1.Proposed_Status__c='Active';
        update objChng1;
        
        string strRes=NotifyLFA.setNotification(objChng.id);
        string strRes1=NotifyLFA.setNotification('testStringIncrease');
        
    }
}