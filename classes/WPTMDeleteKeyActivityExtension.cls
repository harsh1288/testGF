public without sharing class WPTMDeleteKeyActivityExtension {
    public Key_Activity__c kacc;
    public Id keyActivityId;
    public Id performanceFrameworkId;
    public WPTMDeleteKeyActivityExtension() {        
        keyActivityId = ApexPages.currentPage().getParameters().get('Id');
        if(keyActivityId!=NULL){
            kacc = DataBase.Query('Select id,Grant_Intervention__r.Performance_Framework__c FROM Key_Activity__c WHERE id=:keyActivityId');
            performanceFrameworkId = kacc.Grant_Intervention__r.Performance_Framework__c;
        }
    }
    public PageReference deleteKeyActivity(){        
        try{
            delete kacc;
        }
        catch(Exception ex){
            ApexPages.addMessages(ex);            
        }
        PageReference pageRef = new PageReference('/apex/WPTMKeyActivityReport?id='+performanceFrameworkId);
        return pageRef;
    }
}