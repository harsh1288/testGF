@IsTest
public class testupdateGIPwithUserDetail {
  public static testMethod void testupdateGIPwithUserDetail(){
    Id prID = [Select ID from RecordType where SObjectType = 'Account' and Name = 'PR'].Id;
    
    Country__c objCountry = TestClassHelper.createCountry();
    insert  objCountry;
    
    //Create sample users with Standard User
    Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
    
    User u2 = new User(Alias = 'newUser', Email='newuser2@testorg.com', 
         EmailEncodingKey='UTF-8', LastName='Kapoor', FirstName='Ranbir', LanguageLocaleKey='en_US', 
         LocaleSidKey='en_US', ProfileId = p.Id, 
         TimeZoneSidKey='America/Los_Angeles', UserName='newuserX2@testorg.com');
         insert u2;
    
    Account objAcc = TestClassHelper.createAccount();
    objAcc.RecordTypeId = prID;
    objAcc.Country__c = objCountry.Id;
    objAcc.Locked__c = false;
    insert objAcc;
    
     objCountry.PO_1__c =  u2.id;
     
     Grant__c objGrant = TestClassHelper.createGrant(objAcc);
     objGrant.Country__c = objCountry.Id;
     objGrant.Principal_Recipient__c = objAcc.Id;
     insert objGrant;
     
     Implementation_Period__c objIP = TestClassHelper.createIPWithConceptNote(objGrant,objAcc);
     objIP.Status__c = 'Grant-Making';
     objIP.Principal_Recipient__c = objAcc.Id;
     objIP.Grant__c = objGrant.Id;
     objIP.GFS_Task_Name__c = '1.1 - Phase 1';
     insert objIP;
     
     IP_Detail_Information__c objIPd = TestClassHelper.createIPDetail();
     objIPd.Implementation_Period__c = objIP.Id;
     insert objIPd;
     
     Performance_Framework__c objPF = TestClassHelper.createPF(objIP);
     objPF.Implementation_Period__c = objIP.Id;
     insert objPF;
     
     HPC_Framework__c objHPC = TestClassHelper.createHPC();
     objHPC.Grant_Implementation_Period__c = objIP.Id;
     insert objHPC;
     
     update objCountry;
  }
}