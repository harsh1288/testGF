public class overrideindicatordetailext {
    
    public string Indid{get;set;}
    public List<RecordType> lstrec{get;set;}
    private Grant_Indicator__c objind;
    public string recid;
    public boolean display{get;set;}
    public boolean display1{get;set;}
    public List<Result__c> lstresult{get;set;}
    public String strLanguage {get;set;}
    public boolean editBasedOnStatus {get;set;}
    public boolean blnHistory{get;set;}
    public string pfstatus{get;set;} 
    List<Grant_Indicator__c> GID;
    public Boolean isViewIndiFromCN;
    public Performance_Framework__c pf {get; set;}
    public Date startDate {get; set;}
    public Date endDate {get; set;}
    public Boolean displayFiscal {get; set;}
    public List<Implementation_Period__c> lstimp = new List<Implementation_Period__c>();
    public string year1{get;set;}
    public string year2{get;set;}
    public string year3{get;set;}
    public string year4{get;set;}
    public String sign {get;set;}
    public Boolean readGlobalFundToPRComment{get;set;}
    public String decimalPlaces {get;set;}
    /*Changes for name change*/
    public string freqName {get;set;}
    public string freqNameTemp;
    public Boolean blnIPDateErr {get;set;}
    Public Boolean blnMultiCountryRegional {get;set;}
    public boolean showGLFAComments {get;set;} 
    public boolean showLFAComments {get;set;}
    private List<npe5__Affiliation__c> lstAffLFA;
    public overrideindicatordetailext(ApexPages.StandardController controller) {
        showLFAComments = false;
        showGLFAComments = false;
        decimalPlaces = '0';
        isViewIndiFromCN = false;
        objind = (Grant_Indicator__c)controller.getrecord();
        recid=objind.RecordtypeId;
        Indid= ApexPages.currentpage().getparameters().get('Id');
        GID  = [Select id,Performance_Framework__r.PF_status__c,Reporting_Frequency__c, Grant_Implementation_Period__c, Data_Type__c, Grant_Status__c, Decimal_Places__c,Performance_Framework__c
                from grant_indicator__c 
                where id =: Indid ];
        decimalPlaces = GID[0].Decimal_Places__c;
        pfstatus = GID[0].Performance_Framework__r.PF_Status__c;       
        checkProfile();
        lstimp = [select id, name, Component__C, Year_1__c, Year_2__c, Year_3__c, Year_4__c, Length_Years__c, 
                  Number_of_fiscal_cycles__c, Reporting__c, Start_Date__c, End_Date__c,Concept_note__r.Concept_Note_Type__c,GIP_Type__c,Principal_Recipient__c
                  from Implementation_Period__c 
                  where id=:gid[0].Grant_Implementation_Period__c];
        if(lstimp.size() >0){
        year1 = lstimp[0].Year_1__c;
        year2=lstimp[0].Year_2__c;
        year3=lstimp[0].Year_3__c;
        year4=lstimp[0].Year_4__c;
        startDate = lstimp[0].Start_Date__c;
        freqNameTemp = lstimp[0].Reporting__c; //for changing picklist naming convention
        endDate = lstimp[0].End_Date__c;
        blnIPDateErr = false;
            if(startDate == null || endDate == null){
                 blnIPDateErr = true;
                 ApexPages.Message msg = new ApexPages.Message(Apexpages.Severity.ERROR, label.IP_date_Error );
                 ApexPages.addMessage(msg);
            }
        
          if(lstimp[0].GIP_Type__c == 'Regional' || lstimp[0].GIP_Type__c == 'Multi-Country'){
            
                        blnMultiCountryRegional = true; }else { blnMultiCountryRegional = false;}     
            
        if(GID[0].Reporting_Frequency__c == 'Based on Reporting Frequency'){
            freqName = GID[0].Reporting_Frequency__c;
        }else{
            freqName = 'Annually';
        }
            
        if(lstimp[0].Number_of_fiscal_cycles__c == '3'){
            displayFiscal = false;
        }
        else{
            displayFiscal = true;
        }
        }
        strLanguage = 'ENGLISH';
        if(System.UserInfo.getLanguage() == 'fr'){
            strLanguage = 'FRENCH'; 
        }
        if(System.UserInfo.getLanguage() == 'ru'){
            strLanguage = 'RUSSIAN'; 
        }
        if(System.UserInfo.getLanguage() == 'es'){
            strLanguage = 'SPANISH'; 
        }
        
        /**For signs based on data type **/
        if(GID[0].Data_Type__c == Label.Percent_DT ||GID[0].Data_Type__c == Label.NaP_DT) {
            sign = Label.Percent_symb;
            return;
        }
        if(GID[0].Data_Type__c == Label.Ratio) {
            sign = Label.Ratio_label;
            return;
        }      
    }
    
    public PageReference reDirect() {
        PageReference viewAction;
        String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
        string userTypeStr = UserInfo.getUserType();
        if(userTypeStr == 'Standard') {
            baseUrl = baseUrl+'/';
        }
        else 
        {
            Id profileId=userinfo.getProfileId();
            String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
            if(profileName.Contains('PR') || profileName.Contains('Applicant/CM') )
            baseUrl = baseUrl+'/GM/';
            else
            baseUrl = baseUrl+'/LFA/';
        }
        if(isViewIndiFromCN == true) {
            baseUrl = baseUrl +  Indid + '?nooverride=1';
            viewAction = new PageReference(baseUrl);
            viewAction.setRedirect(true);
            return viewAction;
        }
        else {
           Id profileId=userinfo.getProfileId();
           String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
           if(profileName == 'System Administrator') {
               baseUrl = baseUrl +  Indid + '?nooverride=1';
               viewAction = new PageReference(baseUrl);
               viewAction.setRedirect(true);
               return viewAction;
           }
           else{
              return null;
           }
        }//return null;
            
    }
    
    private void checkProfile() {
       
        if(objind.Performance_Framework__c != null) {
            editBasedOnStatus = false;
            readGlobalFundToPRComment = false;        
            pf = [SELECT Grant_Status__c,Implementation_Period__r.Grant__c FROM Performance_Framework__c WHERE id =: objind.Performance_Framework__c];
            Profile profileRec = [select id, name from Profile where id =: UserInfo.getProfileId()];
            String profileNameStr = profileRec.Name;
            List<Profile_Access_Setting__c> checkpage = new List<Profile_Access_Setting__c>();
            List<String> PermissionSets = new List<String>();
            List<PermissionSetAssignment> standalonePermSets = [SELECT PermissionSet.Label 
                                                                FROM PermissionSetAssignment 
                                                                WHERE Assignee.Username =: UserInfo.getUserName()];
            if(standalonePermSets.size()>0) {
                for(PermissionSetAssignment PermSets : standalonePermSets) {
                    PermissionSets.add(PermSets.PermissionSet.Label);
                    system.debug('Name '+PermSets.PermissionSet.Label);
        
                }
            }
            checkpage = [SELECT Salesforce_Item__c,Status__c 
                         FROM Profile_Access_Setting__c 
                         WHERE (Page_Name__c =: 'overrideindicatordetail' OR Page_Name__c =: Label.overrideeditext_page_name) 
                         AND (Profile_Name__c =: profileNameStr OR Permission_Sets__c IN: PermissionSets)];
    
            for (Profile_Access_Setting__c check : checkpage) {
                if(check.Salesforce_Item__c == 'display Edit_Delete button' && (check.Status__c == pf.Grant_Status__c || check.Status__c == null || check.Status__c == ''))
                    editBasedOnStatus = true;
                if((check.Salesforce_Item__c == 'Read_Global fund comment to PR' || check.Salesforce_Item__c == 'Edit_Global fund comment to PR') && 
                   (check.Status__c == pf.Grant_Status__c || check.Status__c == null || check.Status__c == ''))
                    readGlobalFundToPRComment = true;
                if(check.Salesforce_Item__c == 'View LFA Comments' && (check.Status__c == pf.Grant_Status__c || check.Status__c == null || check.Status__c == '')){
                    showLFAComments = true;  
                } 
                if(check.Salesforce_Item__c == 'View_Global Fund Comments to LFA' && (check.Status__c == pf.Grant_Status__c || check.Status__c == null || check.Status__c == '')){
                    showGLFAComments = true;  
                }  
            }
        }
        else 
            isViewIndiFromCN = true;
    }
    

    
    public pageReference edit(){
        User obj = [Select id, Name, ContactId from User where Id=:UserInfo.getUserId()];
        String profileNameStr = [select id, name from Profile where id =: UserInfo.getProfileId()].Name;
                     
        if( obj.ContactId != NULL ){
        if(profileNameStr.contains('LFA')){
            Set<Id> setAccID = new Set<Id>();
            Grant__c objgrant = [Select id,Country__r.LFA__c, Country__r.LFA__r.ParentID from Grant__c where id=: pf.Implementation_Period__r.Grant__c];
            ID objLFAAccId = objgrant.Country__r.LFA__c;
            Id objLFAParentId = objgrant.Country__r.LFA__r.ParentID;
            lstAffLFA = new List<npe5__Affiliation__c>();
            lstAffLFA = [Select Access_Level__c, id,npe5__Contact__c,npe5__Organization__c FROM npe5__Affiliation__c where npe5__Contact__c=: obj.ContactId and (npe5__Organization__c =:objLFAAccId OR npe5__Organization__c =:objLFAParentId) and npe5__Status__c='Current' order by LastModifiedDate desc ];
            if( lstAffLFA[0].Access_Level__c== 'Read'){
                system.debug(label.GM_Edit_Message2);
                system.debug('inside edit pf id --->'+ GID[0].Performance_Framework__c);
                ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.ERROR, label.GM_Edit_Message2);
                ApexPages.addMessage(errorMsg);
                return null;   
               
             }else{
                pageReference Pr = new pageReference('/apex/overideedit?id='+indid);
                pr.setredirect(true);
                return pr;
            }                              
          }else{
          List<npe5__Affiliation__c> lstAff = [Select Access_Level__c, id,npe5__Contact__c,npe5__Organization__c FROM npe5__Affiliation__c where npe5__Contact__c=: obj.ContactId and npe5__Organization__c=:lstimp[0].Principal_Recipient__c and npe5__Status__c='Current' order by LastModifiedDate desc]; 
          system.debug('Inside Edit Overrideindicatordetailext');
          if( lstAff[0].Access_Level__c== 'Read'){
                system.debug(label.GM_Edit_Message2);
                system.debug('inside edit pf id --->'+ GID[0].Performance_Framework__c);
                ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.ERROR, label.GM_Edit_Message2);
                ApexPages.addMessage(errorMsg);
                return null;   
               
             }else{
                pageReference Pr = new pageReference('/apex/overideedit?id='+indid);
                pr.setredirect(true);
                return pr;
            } 
            } 
        }else
        {
                pageReference Pr = new pageReference('/apex/overideedit?id='+indid);
                pr.setredirect(true);
                return pr;
        }
    }
    public pageReference deleteind(){
        pageReference Pr = new pageReference('/apex/Delete_Indicator?Id='+indid);
        pr.setredirect(true);
        return pr; 
    }
    public pageReference addgoals(){
        pageReference Pr = new pageReference('/apex/GoalSelection?Id='+indid);
        pr.setredirect(true);
        return pr;
       
    }
    
     Public void ShowHistoryPopup(){
         blnHistory = true;
     }
     Public void HidePopupHistory(){
         system.debug('**InsideHidePopup');
         blnHistory = false;
     }
    public pagereference openGuidance(){
        
        Pagereference pr;
        Grant_Indicator__c objGI = [SELECT id, Indicator_Type__c FROM Grant_Indicator__c WHERE id =: indid LIMIT 1];
        Guidance__c gui_impact = [Select Id, Name from Guidance__c WHERE Name =: Label.GM_Goals_Impact_Indicators LIMIT 1];
        Guidance__c gui_outcome = [Select Id, Name from Guidance__c WHERE Name =: Label.GM_Objectives_Outcome_Indicators LIMIT 1];
        Guidance__c gui_coverage = [Select Id, Name from Guidance__c WHERE Name =: Label.GM_Coverage_Output_Indicators LIMIT 1];
        //List<Guidance__c> lstGUI = [Select Id, Name from Guidance__c];
        
        if(objGI.Indicator_Type__c == 'Impact' || objGI.Indicator_Type__c == 'Outcome'){
            pr = new Pagereference('/apex/GrantMakingGuidance?Id='+gui_impact.id+'&lang='+strLanguage);
        }
        /*else if(objGI.Indicator_Type__c == 'Outcome'){
            pr = new Pagereference('/apex/GrantMakingGuidance?Id='+gui_outcome.id+'&lang='+strLanguage);
        }*/
        else if(objGI.Indicator_Type__c == 'Coverage/Output'){
            pr = new Pagereference('/apex/GrantMakingGuidance?Id='+gui_coverage.id+'&lang='+strLanguage);
        }
        
        pr.setredirect(true);
        return pr; 
    }
    
   public pagereference openPF(){
   Pagereference pr = new Pagereference('/'+objind.Performance_Framework__c);
   pr.setredirect(true);
   return pr;
   }

}