public with sharing class hpcMassInsert_controller {
    
    /*-----------START---------Variable Initialisation------------------------------*/
    Public String strSelectedCatagory {get;set;}
    Public List<String> lstSelectedCatagory {get;set;}
    Public String strProductCatagory {get;set;}
    Public List<String> lstProductCatagory {get;set;}
    Public String strProductName {get;set;}
    Public String strIntervention {get;set;}
    Public String strSpecification {get;set;}
    Public List<SelectOption> CatagoryOptions {get;set;}
    Public List<SelectOption> CostInputOptions {get;set;}
    Public List<SelectOption> ProductNameOptions {get;set;}
    Public List<SelectOption> InterventionOptions {get;set;}
    Public List<SelectOption> SpecificationOptions {get;set;}
    Public String pageId;
    Public list<Implementation_Period__c> lstIP;
    Public list<HPC_Framework__c> lstHPC;
    Public List<Catalog_Cost_Input__c> lstCostInput;
    Public List<Catalog_Cost_Input__c> lstCostInputOption;
    Public list<Catalog_Generic_Product__c> lstCatGenPro;
    Public list<Product__c> lstProd;
    Public list<Module__c> lstMod;
    Public list<Grant_Intervention__c> lstGI;
    Public list<Implementer__c> lstRecpnt;
    Public list<Catalog_Product__c> lstCatProd;
    Public list<wrapperClassControllercgp> lstwrapProd {get;set;}
    Public list<wrapperClassControllerintv> lstwrapintv {get;set;}
    Public list<wrapperClassControllerRcpnt> lstwrapRecpnt {get;set;}
    Public list<wrapperClassControllerAll> lstwrapAll {get;set;}
    Public list<wrapperClassControllercci> lstwrapCatCost {get;set;}
    Public wrapperClassControllercci objwrapCatCost;
    Public wrapperClassControllerAll objwrapAll;
    Public wrapperClassControllerRcpnt objwrapRecpnt;
    Public wrapperClassControllercgp objwrapProd;
    Public wrapperClassControllerintv objwrapintv;
    Public Module__c objModule;
    Public Catalog_Product__c objCatProd;
    Public Implementer__c objRecpnt;
    Public Grant_Intervention__c objGI;
    Public Product__c objProd;
    Public Catalog_Generic_Product__c objCatGenPro;
    Public Implementation_Period__c objIP;
    Public HPC_Framework__c objHPC;
    Public Catalog_Cost_Input__c objCostInput {get;set;}
    Public String strComponent;
    Public String strSelectedCategory {get;set;}
    Public String strSelectedProducts {get;set;}
    Public String strSelectedInterventions {get;set;}
    Public String baseURL;
    Public String userTypeStr;
    Public list<Catalog_Cost_Input__c> lstccifinal;
    Public list<Catalog_Generic_Product__c> lstcgpfinal;
    Public list<Grant_Intervention__c> lstgifinal;
    Public list<Implementer__c> lstrecfinal;
    Public boolean blnselectcategory {get;set;}
    Public boolean blnproductcategory {get;set;}
    Public boolean blnproductname {get;set;}
    Public boolean blnmoduleIntervention {get;set;}
    Public boolean blnselectRecipients {get;set;}
    Public boolean blnshowAllReadOnly {get;set;}
    Public boolean blnshowAllEditable {get;set;}
    /*-----------FINISH---------Variable Initialisation------------------------------*/
    
    /*-----------START---------Constructor Defined------------------------------*/
    public hpcMassInsert_controller(ApexPages.StandardController controller) {
        blnselectcategory = false;
        blnproductcategory = false;
        blnproductname = false;
        blnmoduleIntervention = false;
        blnSelectRecipients = false;
        blnshowAllReadOnly = false;
        blnshowAllEditable = false;
        pageId = ApexPages.currentPage().getParameters().get('id');   //id='a1km0000000odGg'
        baseURL = URL.getSalesforceBaseUrl().toExternalForm();
        userTypeStr = UserInfo.getUserType();
        if(userTypeStr != 'Standard') {
            baseUrl = baseUrl+'/GM/';
        }
        else
            baseUrl = baseUrl+'/';
        //objModule = [SELECT id, Name, Implementation_Period__c FROM Module__c WHERE id =: pageId LIMIT 1];
        //objHPC = [SELECT id, Grant_Implementation_Period__c FROM HPC_Framework__c WHERE Grant_Implementation_Period__c =: objModule.Implementation_Period__c LIMIT 1];
        objHPC = [SELECT id, Grant_Implementation_Period__c FROM HPC_Framework__c WHERE id =: pageId LIMIT 1];
        objIP = [SELECT id, Component__c FROM Implementation_Period__c WHERE id =: objHPC.Grant_Implementation_Period__c LIMIT 1];
        fillCatagory();
        strSelectedcategory = '';
        strSelectedProducts = '';
        strSelectedInterventions = '';
    }
    /*-----------FINISH---------Constructor Defined------------------------------*/
    
    /*-----------START---------Method to load the first pageblock section for selecting initial categories------------------------------*/
    Public void SelectCategorySection(){
        blnselectcategory = true;
        blnproductcategory = false;
        blnproductname = false;
        blnmoduleIntervention = false;
        blnselectRecipients = false;
        blnshowAllReadOnly = false;
        blnshowAllEditable = false;
    }
    /*-----------FINISH---------Method to load the first pageblock section for selecting initial categories------------------------------*/
    
    /*-----------START---------Method to load the second pageblock section for selecting product categories------------------------------*/
    Public void ProductCategorySection(){
        blnselectcategory = false;
        blnproductcategory = true;
        blnproductname = false;
        blnmoduleIntervention = false;
        blnselectRecipients = false;
        blnshowAllReadOnly = false;
        blnshowAllEditable = false;
        strSelectedCategory = '';
    }
    /*-----------FINISH---------Method to load the second pageblock section for selecting product categories------------------------------*/
    
    /*-----------START---------Method to load the third pageblock section for Product Name Selection------------------------------*/
    Public void ProductNameSection(){
        blnselectcategory = false;
        blnproductcategory = false;
        blnproductname = true; 
        blnmoduleIntervention = false;
        blnselectRecipients = false;
        blnshowAllReadOnly = false;
        blnshowAllEditable = false;
        strSelectedProducts = '';
    }
    /*-----------FINISH---------Method to load the third pageblock section for Product Name Selection------------------------------*/
    
    /*-----------START---------Method to load the fourth pageblock section for Intervention Selection------------------------------*/
    Public void InterventionSection(){
        blnselectcategory = false;
        blnproductcategory = false;
        blnproductname = false; 
        blnmoduleIntervention = true;
        blnselectRecipients = false;
        blnshowAllReadOnly = false;
        blnshowAllEditable = false;
        strSelectedInterventions = '';
    }
    /*-----------FINISH---------Method to load the fourth pageblock section for Intervention Selection------------------------------*/
    
    /*-----------START---------Method to load the fifth pageblock section for Recipient Selection------------------------------*/
    Public void RecipientSection(){
        blnselectcategory = false;
        blnproductcategory = false;
        blnproductname = false;     
        blnmoduleIntervention = false; 
        blnSelectRecipients = true;
        blnshowAllReadOnly = false;
        blnshowAllEditable = false;
    }
    /*-----------FINISH---------Method to load the fifth pageblock section for Recipient Selection------------------------------*/
    
    /*-----------START---------Method to load the sixth pageblock section for Show All Values Read-Only------------------------------*/
    Public void ShowAllEditSection(){
        blnselectcategory = false;
        blnproductcategory = false;
        blnproductname = false;     
        blnmoduleIntervention = false; 
        blnSelectRecipients = false;
        blnshowAllReadOnly = true;
        blnshowAllEditable = false;
    }
     /*-----------FINISH---------Method to load the sixth pageblock section for Show All Values Read-Only------------------------------*/
    
    /*-----------START---------Method to Cancel the job and go back to origin------------------------------*/
    Public Pagereference CancelPage(){
        Pagereference pr = new Pagereference(baseURL+pageId);
        pr.setRedirect(true);
        return pr;
    }
    /*-----------FINISH---------Method to Cancel the job and go back to origin------------------------------*/
    
    /*-----------START---------Method to load the initial categories------------------------------*/
    Public void fillCatagory(){
        blnselectcategory = true;
        blnproductcategory = false;
        blnproductname = false;
        blnmoduleIntervention = false;
        blnSelectRecipients = false;
        blnshowAllReadOnly = false;
        blnshowAllEditable = false;
        strSelectedCatagory = '';
        CatagoryOptions = new List<SelectOption>();
        Schema.sObjectType objType = Catalog_Cost_Input__c.getSObjectType();
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe(); 
        map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap(); 
        List<Schema.PicklistEntry> lstCatagory = fieldMap.get('Product_Category__c').getDescribe().getPickListValues();
        CatagoryOptions.add(new SelectOption('','--None--')); 
        for (Schema.PicklistEntry Entry : lstCatagory){ 
            CatagoryOptions.add(new SelectOption(Entry.getLabel(), Entry.getValue())); 
        }
    }
    /*-----------FINISH---------Method to load the initial categories------------------------------*/
    
    /*-----------START---------Method to select the product categories------------------------------*/
    Public void selectedCatagory(){
            if(strSelectedCatagory == null || strSelectedCatagory == ''){
                blnselectcategory = true;
                blnproductcategory = false;
                blnproductname = false;
                blnmoduleIntervention = false;
                blnSelectRecipients = false;
                blnshowAllReadOnly = false;
                blnshowAllEditable = false;
                ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.Fatal,'Please Select Category Value');
                ApexPages.addMessage(errorMsg);
            }
            else{
                blnselectcategory = false;
                blnproductcategory = true;
                blnproductname = false;
                blnmoduleIntervention = false;
                blnSelectRecipients = false;
                blnshowAllReadOnly = false;
                blnshowAllEditable = false;
                system.debug('#$#$strSelectedCatagory #$#$'+strSelectedCatagory);
                if(strSelectedCatagory != 'PSM Cost'){
                //String strQuery = 'Select Product_Name__c,Cost_Input__r.Name,Cost_Input__r.Is_Inactive__c, Product_Category__c,Strength_Shape__c,Catalog_Product__c,(Select Id from PSM_Intervention_Splits__r),Catalog_Product__r.Name,Catalog_Product__r.Product_Name__c,Catalog_Product__r.Is_Inactive__c,Catalog_Product__r.Generic_Product__c,Catalog_Product__r.Generic_Product__r.Is_Inactive__c,Catalog_Product__r.Generic_Product__r.name,Catalog_Product__r.Generic_Product__r.Catalog_Cost_Input__c,Catalog_Product__r.Specification__c,Implementer__c,Implementer__r.name,Implementer__r.Implementer_Name__c,Catalog_Product__r.Generic_Product__r.Catalog_Cost_Input__r.Name,Quantity_Q1__c,Quantity_Q10__c,Quantity_Q11__c,Quantity_Q12__c,Quantity_Q13__c,Quantity_Q14__c,Quantity_Q15__c,Quantity_Q16__c,Quantity_Q2__c,Quantity_Q3__c,Quantity_Q4__c,Quantity_Q5__c,Quantity_Q6__c,Quantity_Q7__c,Quantity_Q8__c,Quantity_Q9__c,Unit_Cost_Y1__c,Unit_Cost_Y2__c,Unit_Cost_Y3__c,Unit_Cost_Y4__c,Cost_Y1__c,Cost_Y2__c,Cost_Y3__c,Cost_Y4__c,Total_Cost__c,Product_Main_Category__c,Latest_LFA_Comment__c,Latest_PR_Comment__c,Latest_TGF_Comment__c,LFA_Comments__c,PR_Comments__c,TGF_Comments__c From Product__c Where Implementation_Period__c =: objIP.id And Product_Main_Category__c =: strSelectedCatagory Order By Product_Number__c';
                strComponent = objIP.Component__c;
                
                lstCostInput = [SELECT Id, Name, Cost_Grouping__c, Unit_Cost_Definition__c 
                                FROM Catalog_Cost_Input__c 
                                WHERE PSM__c = true 
                                AND Disease_Impact__c INCLUDES(:strComponent) 
                                AND Product_Category__c =: strSelectedCatagory 
                                ORDER BY Cost_Input_Number__c];
                
                if(lstCostInput.size() > 0){                
                    lstwrapCatCost = new list<wrapperClassControllercci>();
                    for(Catalog_Cost_Input__c ci: lstCostInput){
                        objwrapCatCost = new wrapperClassControllercci(ci);
                        lstwrapCatCost.add(objwrapCatCost);
                    }
                }               
                System.debug('****lstwrapCatCost: '+lstwrapCatCost);
                CostInputOptions = new List<SelectOption>();
                if(lstCostInput.size() > 0){
                    for(Catalog_Cost_Input__c objCI : lstCostInput){
                        CostInputOptions.add(new SelectOption(objCI.Id,objCI.Name));
                    }
                }
                //List<Product__c> lstProductTemp = new List<Product__c>();
                //lstProductTemp = Database.Query(strQuery);
            }
        }
    }
    /*-----------FINISH---------Method to select the product categories------------------------------*/
    
    /*-----------START---------Method to select only one product category at the final table------------------------------*/
    
    Public void SelectProductCategory(){
        //CostInputOptions.add(new SelectOption('','--None--'));
        System.Debug('****Selected categories: '+lstSelectedCatagory);
        Set<String> strSetSelectedcategories = new Set<String>();
        for(wrapperClassControllerAll wca: lstwrapAll){
            strSetSelectedcategories.add(wca.strMainCategoryWrap);
        }
        
        lstCostInputOption = [SELECT Id, Name, Cost_Grouping__c, Unit_Cost_Definition__c 
                              FROM Catalog_Cost_Input__c 
                              WHERE PSM__c = true 
                              AND Disease_Impact__c INCLUDES(:strComponent) 
                              AND Product_Category__c IN: strSetSelectedcategories 
                              ORDER BY Cost_Input_Number__c];
        CostInputOptions = new List<SelectOption>();
        if(lstCostInputOption.size() > 0){
            for(Catalog_Cost_Input__c objCI : lstCostInputOption){
                CostInputOptions.add(new SelectOption(objCI.Id,objCI.Name));
            }
        }
        System.debug('######'+CostInputOptions);
        System.debug('****Selected Product Categories: '+strProductCatagory);
        
        /*for(String s: lstProductCatagory){
            strProductCatagory = s;     
        }*/
    }
    /*-----------FINISH---------Method to select only one product category at the final table------------------------------*/
    
    /*-----------START---------Method to select the Product Names------------------------------*/
    Public void SelectProductName(){
        
        lstccifinal = new list<Catalog_Cost_Input__c>();
        for(wrapperClassControllercci cci: lstwrapCatCost){
            if(cci.selectedcci == true){
                blnselectcategory = false;
                blnproductcategory = false;
                blnproductname = true;
                blnmoduleIntervention = false;
                blnSelectRecipients = false;
                blnshowAllReadOnly = false;
                blnshowAllEditable = false;
                lstccifinal.add(cci.objCatCostWrap);
                if(strSelectedCategory == '')
                    strSelectedCategory = cci.objCatCostWrap.Name;    
                else             
                    strSelectedCategory += ('; ' + cci.objCatCostWrap.Name); 
            }                    
        }
        if(blnproductname == true){  
            Set<Id> costipId = new Set<Id>();
            for(Catalog_Cost_Input__c cci: lstccifinal){
                costipId.add(cci.id);
            }
            //objCostInput = [SELECT Id, Name, Product_Category__c FROM Catalog_Cost_Input__c  WHERE id =: strProductCatagory LIMIT 1];
            //System.Debug('****Catalog Cost Input: '+objCostInput);
            if(costipId.size() > 0){
                lstCatGenPro = [SELECT id, Name, Product_Category__c, Product_Sub_Category__c
                                FROM Catalog_Generic_Product__c 
                                WHERE Catalog_Cost_Input__c IN: costipId];
            }
            System.Debug('****Catalog Generic Product: '+lstCatGenPro);
                        
            if(lstCatGenPro.size()>0){
                lstwrapProd = new list<wrapperClassControllercgp>();
                for(Catalog_Generic_Product__c cgp: lstCatGenPro){                
                    objwrapProd = new wrapperClassControllercgp(cgp);
                    lstwrapProd.add(objwrapProd);
                }
            }
            System.Debug('****WrapperList: '+lstwrapProd);   
            
            //ProductNameOptions.add(new SelectOption('','--None--'));
            ProductNameOptions = new List<SelectOption>();
            if(lstCatGenPro.size() > 0){
                for(Catalog_Generic_Product__c objCI : lstCatGenPro){
                    ProductNameOptions.add(new SelectOption(objCI.Id,objCI.Name));
                }
            }            
        }
        else{
            blnselectcategory = false;
            blnproductcategory = true;
            blnproductname = false;     
            blnmoduleIntervention = false;
            blnSelectRecipients = false;
            blnshowAllReadOnly = false;
            blnshowAllEditable = false;
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.Fatal,'Please Select Atleast One Product Category');
            ApexPages.addMessage(errorMsg);
        }
    }
    /*-----------FINISH---------Method to select the Product Names------------------------------*/
    
    /*-----------START---------Method to select the Interventions of particular modules------------------------------*/
    public void SelectIntervention(){
        //boolean notselectedproduct = false;
        lstcgpfinal = new list<Catalog_Generic_Product__c>();
        for(wrapperClassControllercgp lwp: lstwrapProd){            
            if(lwp.selectedcgp == true){  
                blnselectcategory = false;
                blnproductcategory = false;
                blnproductname = false;     
                blnmoduleIntervention = true; 
                blnSelectRecipients = false;
                blnshowAllReadOnly = false;
                blnshowAllEditable = false;
                lstcgpfinal.add(lwp.objProdWrap);
                if(strSelectedProducts == '')
                    strSelectedProducts = lwp.objProdWrap.Name;    
                else             
                    strSelectedProducts += ('; ' + lwp.objProdWrap.Name);                
            }            
        }
        if(blnmoduleIntervention == true){
            lstMod = [SELECT id, Name, Implementation_Period__c FROM Module__c 
                      WHERE Implementation_Period__c =: objHPC.Grant_Implementation_Period__c];
            Set<id> ModId = new Set<id>();
            if(lstMod.size()>0){
                for(Module__c c: lstMod){
                    ModId.add(c.id);            
                }
            }
            System.Debug('****Module ID in SET: '+lstMod);
            if(ModId.size()>0){
                lstGI = [SELECT Id, Name, Implementation_Period__c, Module__r.Name, Module__c
                         FROM Grant_Intervention__c
                         WHERE Module__c IN: ModId];
            }
            System.Debug('****Grant Intervention: '+lstGI);
            /*if(lstGI.size()>0){
                lstwrapintv = new list<wrapperClassControllerintv>();  
                for(Grant_Intervention__c gi: lstGI){
                    objwrapintv = new wrapperClassControllerintv(gi);
                    lstwrapintv.add(objwrapintv);
                }
            }
            System.Debug('****Intervention Wrapper List : '+lstwrapintv);     */ 
            
            InterventionOptions = new list<SelectOption>();
            InterventionOptions.add(new SelectOption('','--None--'));
            InterventionOptions = new List<SelectOption>();
            if(lstGI.size() > 0){
                for(Grant_Intervention__c objCI : lstGI){
                    InterventionOptions.add(new SelectOption(objCI.Id, objCI.Module__r.Name+' - '+objCI.Name));
                }
            }            
        //Method called to add Principal Recipient    
        SelectRecipient();
        }

        //Check if any of the product name has been selected or not
        else{
            blnselectcategory = false;
            blnproductcategory = false;
            blnproductname = true;     
            blnmoduleIntervention = false;
            blnSelectRecipients = false;
            blnshowAllReadOnly = false;
            blnshowAllEditable = false;
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.Fatal,'Please Select Atleast One Product');
            ApexPages.addMessage(errorMsg);    
        }
    }
    /*-----------FINISH---------Method to select the Interventions of particular modules------------------------------*/
    
    /*-----------START---------Method to select the Principal Recipient of particular Implementation Period------------------------------*/
    public Implementer__c SelectRecipient(){

        objRecpnt = [SELECT Id, Name, Implementer_Name__c, Grant_Implementation_Period__c
                     FROM Implementer__c
                     WHERE Grant_Implementation_Period__c =: objHPC.Grant_Implementation_Period__c AND Implementer_Role__c =: 'Principal Recipient' LIMIT 1];
        return objRecpnt;
        }
    
    /*-----------FINISH--------Method to select the Principal Recipient of particular Implementation Period------------------------------*/
    
    /*-----------START--------------Method to select the all the selections from different objects------------------------------*/
    public void ShowAllSelection(){
        //lstrecfinal = new list<Implementer__c>();
        List<integer> indexlist = new List<integer>();
        lstgifinal = new list<Grant_Intervention__c>();
        objGI = [SELECT id, Name, Module__r.Name FROM Grant_Intervention__c WHERE id =: strIntervention LIMIT 1];
        if(objGI != null){  
            blnselectcategory = false;
            blnproductcategory = false;
            blnproductname = false;     
            blnmoduleIntervention = false; 
            blnSelectRecipients = false;
            blnshowAllReadOnly = true;
            blnshowAllEditable = false;
            //lstgifinal.add(wc.objIntvWrap);  
            lstwrapAll = new list<wrapperClassControllerAll>(); 
            objProd = new Product__c();
            Set<Id> cgpId = new Set<Id>();
            for(Catalog_Generic_Product__c cgp: lstcgpfinal){
                cgpId.add(cgp.id);
            }
            lstCatProd = [SELECT id, Specification__c, Generic_Product__c FROM Catalog_Product__c WHERE Generic_Product__c IN: cgpId];
            /*SpecificationOptions = new list<SelectOption>();
            if(lstCatProd.size()>0){                
                for(Catalog_Product__c cp: lstCatProd){
                    SpecificationOptions.add(new SelectOption(cp.id, cp.Specification__c));
                }
            }*/
            for(Catalog_Cost_Input__c cci: lstccifinal){   
                for(Catalog_Generic_Product__c cgp: lstcgpfinal){
                    objwrapAll = new wrapperClassControllerAll(strSelectedCatagory, cci, strProductCatagory, cgp,  
                                                               strProductName, objGI, objRecpnt, objProd, '');
                    lstwrapAll.add(objwrapAll);
                }
            }
            for(Integer i=0; i<lstwrapAll.size(); i++){
                indexlist.add(i);
            //}
            //for(integer EditIndex :indexlist){
                lstwrapAll[i].StrengthAndUnitOptionsWrap = new List<SelectOption>();
                //lstCatProd = [SELECT id, Specification__c, Generic_Product__c FROM Catalog_Product__c WHERE Generic_Product__c =: lstwrapAll[EditIndex].objCatalogGenericProductWrap.id];
                if(lstCatProd.size()>0){                
                    for(Catalog_Product__c cp: lstCatProd){
                        //lstwrapAll[i].StrengthAndUnitOptionsWrap.add(new SelectOption('', '--None--'));
                        if(lstwrapAll[i].objCatalogGenericProductWrap.id == cp.Generic_Product__c){
                            lstwrapAll[i].StrengthAndUnitOptionsWrap.add(new SelectOption(cp.id, cp.Specification__c));
                        }
                    }
                }
            }
        }
        else{
            blnselectcategory = false;
            blnproductcategory = false;
            blnproductname = false;     
            blnmoduleIntervention = true; 
            blnSelectRecipients = false;
            blnshowAllReadOnly = false;
            blnshowAllEditable = false;
            if(lstGI.size() > 0){
                ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.Fatal,'Please Select an Intervention');
                ApexPages.addMessage(errorMsg);
            }
            else{
                ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.Warning,'No Intervention Available');
                ApexPages.addMessage(errorMsg);
            }
        }
    }
    /*-----------FINISH-------------Method to select the all the selections from different objects------------------------------*/
    
    /*-----------START--------------Method to select particular rows to edit from all the selections from different objects------------------------------*/
    public void EditPage(){
        for(wrapperClassControllerAll wc: lstwrapAll){
            if(wc.selectedValues == true){
                wc.editableValues = true;
            }
        }
    }
    /*-----------FINISH-------------Method to select particular rows to edit from all the selections from different objects------------------------------*/
    
    /*-----------START--------------Method to select particular rows to delete from all the selections from different objects------------------------------*/
    public void DeleteRows(){
        boolean blndelete = false;
        for(wrapperClassControllerAll wc: lstwrapAll){
            if(wc.selectedValues == true){
                wc.deleteValues = true;
                blndelete = true;
            }
        }
        for(Integer k = 0; k<lstwrapAll.size(); k++){
            //if(lstwrapAll[k].deleteValues == true){
            if(lstwrapAll[k].selectedValues == true){
                System.Debug('****Wrapper row Index: '+lstwrapAll[k]+'--->'+lstwrapAll[k].deleteValues);
                lstwrapAll.remove(k);
            }
        }
    }
    /*-----------FINISH-------------Method to select particular rows to delete from all the selections from different objects------------------------------*/
    
    /*-----------START--------------Method to select particular rows to copy from all the selections from different objects------------------------------*/
    Public void CopyRows(){
        boolean blncopy = true;
        for(wrapperClassControllerAll wc: lstwrapAll){
            if(wc.selectedValues == true){
                wc.copyValues = true;
                blncopy = true;
            }
        }
        for(Integer k = 0; k<lstwrapAll.size(); k++){
            if(blncopy == true){
                //lstwrapAll.add(k);
                //lstwrapAll.clone(k);
            }
        }
    }
    /*-----------FINISH-------------Method to select particular rows to copy from all the selections from different objects------------------------------*/
    
    
/*******************************************************************************************************************************************************************
                                                                    START OF ALL WRAPPER CLASSES
*******************************************************************************************************************************************************************/
    
    /*-----------START---------Wrapper Class to bind Product Category and Checkbox------------------------------*/
    public class wrapperClassControllercci{
        public Catalog_Cost_Input__c objCatCostWrap {get; set;}
        public Boolean selectedcci {get; set;}
        public wrapperClassControllercci(Catalog_Cost_Input__c p){
                this.objCatCostWrap = p;
                this.selectedcci = false;
        }
    }
    /*-----------FINISH---------Wrapper Class to bind Product Category and Checkbox------------------------------*/
    
    /*-----------START---------Wrapper Class to bind Catalog generic Product and Checkbox------------------------------*/
    public class wrapperClassControllercgp{
        public Catalog_Generic_Product__c objProdWrap {get; set;}
        public Boolean selectedcgp {get; set;}
        public wrapperClassControllercgp(Catalog_Generic_Product__c p){
                this.objProdWrap = p;
                this.selectedcgp = false;
        }
    }
    /*-----------FINISH---------Wrapper Class to bind Catalog generic Product and Checkbox------------------------------*/
    
    /*-----------START---------Wrapper Class to bind Module/Interventions and Checkbox------------------------------*/
    public class wrapperClassControllerintv{
        public Grant_Intervention__c objIntvWrap {get; set;}
        public Boolean selectedintv {get; set;}
        public wrapperClassControllerintv(Grant_Intervention__c g){
                this.objIntvWrap = g;
                this.selectedintv = false;
        }
    }
    /*-----------FINISH---------Wrapper Class to bind Module/Interventions and Checkbox------------------------------*/
    
    /*-----------START---------Wrapper Class to bind Recipients and Checkbox------------------------------*/
    public class wrapperClassControllerRcpnt{
        public Implementer__c objRecpWrap {get; set;}
        public Boolean selectedRecpnt {get; set;}
        public wrapperClassControllerRcpnt(Implementer__c i){
                this.objRecpWrap = i;
                this.selectedRecpnt = false;
        }
    }
    /*-----------FINISH---------Wrapper Class to bind Recipients and Checkbox------------------------------*/
    
    /*-----------START---------Wrapper Class to bind Selectlist values of Category------------------------------*/
    /*public class recWrapper{ 
        //public sObject theRec = YOUR OBJECT {get;set;}
        Public SelectOption[] theOptions {get;set;}
        Public String SelectedValue {get;set;}
         
                 public recWrapper(sObject rec){
         
                    theRec = rec;
                    //Populate the Select options list or pass it in
                }
         
        }*/
    /*-----------FINISH---------Wrapper Class to bind Selectlist values of Category------------------------------*/
    
    /*********************START*********************Wrapper Class to bind all the selected values from all objects****************************/
    public class wrapperClassControllerAll{
        public String strMainCategoryWrap {get; set;}
        public Catalog_Cost_Input__c objProductCategoryWrap {get; set;}
        Public String strCatalogCostInputWrap {get;set;}
        public Catalog_Generic_Product__c objCatalogGenericProductWrap {get; set;}
        public String strCatalogGenericProductOptionswrap {get;set;}
        public Grant_Intervention__c objGrantInterventionvWrap {get; set;}
        //public String strGrantInterventionOptionswrap {get;set;}
        public Implementer__c objRecipientWrap {get; set;}
        public Product__c objProductWrap {get;set;}
        public String SpecificationWrap {get;set;}
        public List<SelectOption> StrengthAndUnitOptionsWrap {get;set;}
        public Boolean selectedValues {get; set;}
        public Boolean editableValues {get;set;}
        public Boolean deleteValues {get;set;}
        public Boolean copyValues {get;set;}
        
        public wrapperClassControllerAll(String sm, Catalog_Cost_Input__c ci, String cciop, Catalog_Generic_Product__c cgp, String cgpop, Grant_Intervention__c gi, Implementer__c imp, Product__c p, String spec){
            this.strMainCategoryWrap = sm;
            this.objProductCategoryWrap = ci;
            this.strCatalogCostInputWrap = cciop;
            this.objCatalogGenericProductWrap = cgp;
            this.strCatalogGenericProductOptionswrap = cgpop;
            this.objGrantInterventionvWrap = gi;
            //this.strGrantInterventionOptionswrap = giop;
            this.objRecipientWrap = imp;
            this.objProductWrap = p;
            this.SpecificationWrap = spec;
            //this.StrengthAndUnitOptionsWrap = opSpec;
            this.selectedValues = false;
            this.editableValues = false;
            this.deleteValues = false;
            this.copyValues = false;
        }
    }
    /*********************FINISH*********************Wrapper Class to bind all the selected values from all objects****************************/
}