Public class ctrlLFADefaultTab{
   public User objUser;
   public boolean WorplanAccess;
   public boolean PETAccess;
   public boolean GMAccess;
 
    /*public ctrlLFADefaultTab(ApexPages.StandardController controller){
      String UserID = userinfo.getUserId();
      objUser = [Select ID, Profile.Name, IsPortalEnabled from User where id=:UserID];
      WorplanAccess =false;
      PETAccess = false;
      GMAccess = false;
    }*/

     public ctrlLFADefaultTab(){
      String UserID = userinfo.getUserId();
      objUser = [Select ID, Profile.Name, IsPortalEnabled from User where id=:UserID];
      WorplanAccess =false;
      PETAccess = false;
      GMAccess = false;
    }

    Public PageReference pageRedirect(){
        system.debug('objUser.Profile.Name ' +objUser.Profile.Name);
        try{
            if(objUser.Profile.Name == label.LFA_Portal_User){        
                //List<PermissionSetAssignment> psList = [SELECT Id,name FROM PermissionSet WHERE Name =: 'LFA_Portal_User_Work_Plans' OR Name =: 'LFA_Portal_User_PET' OR Name =: 'Make_GM_app_visible_to_LFA_user_PermissionSet'];
                List<PermissionSetAssignment> psList = [Select id, permissionset.id, permissionset.name from permissionsetAssignment WHERE AssigneeId=:objUser.ID AND (permissionset.name =: 'LFA_Portal_User_Work_Plans' OR permissionset.name =: 'LFA_Portal_User_PET' OR permissionset.name =: 'Make_GM_app_visible_to_LFA_user_PermissionSet')];
                
                
                for(PermissionSetAssignment ps: psList){
                    if(ps.permissionset.name == 'LFA_Portal_User_Work_Plans')
                        WorplanAccess = True;
                    if(ps.permissionset.name == 'LFA_Portal_User_PET')
                        PETAccess = True;
                    if(ps.permissionset.name == 'Make_GM_app_visible_to_LFA_user_PermissionSet')
                        GMAccess = True;        
                }
                
                if(WorplanAccess == true && GMAccess == False){
                    PageReference pgrefnew = new PageReference('/LFA/a0Q/o');
                    pgrefnew.setRedirect(true);
                    return pgrefnew;
                }
                
                if(WorplanAccess == false && GMAccess == true  && PETAccess == false){
                    PageReference pgrefnew = new PageReference('/LFA/apex/GrantMakingHome');
                    pgrefnew.setRedirect(true);
                    return pgrefnew;
                }
                
                if(WorplanAccess == true && GMAccess == True){
                    PageReference pgrefnew = new PageReference('/LFA/a0Q/o');
                    pgrefnew.setRedirect(true);
                    return pgrefnew;
                } 
                
                if(PETAccess == true && GMAccess == false && WorplanAccess == False){
                    PageReference pgrefnew = new PageReference('/LFA/a0r/o');
                    pgrefnew.setRedirect(true);
                    return pgrefnew ;
                } 
                
                if(PETAccess == true && GMAccess == True && WorplanAccess == False){
                    PageReference pgrefnew = new PageReference('/LFA/a0r/o');
                    pgrefnew.setRedirect(true);
                    return pgrefnew ;
                }  
                    
            }
        }catch(Exception e){
            return NULL;
        }           
            return null;
    }     
}