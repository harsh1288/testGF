/*********************************************************************************
* Test class:   TestctrlDetailPageLink
  Class: ctrlDetailPageLink 
*  DateCreated : 21/11/2014
----------------------------------------------------------------------------------
* Purpose/Methods:
* - To test ctrlDetailPageLink class 
----------------------------------------------------------------------------------
* History:
* - VERSION  DEVELOPER NAME    DATE            DETAIL FEATURES
     1.0        TCS           21/11/2014      INITIAL DEVELOPMENT
*********************************************************************************/ 

@isTest
private class TestctrlDetailPageLink {

    static testMethod void TestctrlDetailPageLink() {
       
       Catalog_Module__c objCatMod = TestClassHelper.createCatalogModule();  
       insert objCatMod;
       
       Catalog_Intervention__c objCatInt = TestClassHelper.createCatalogIntervention(); 
       objCatInt.Catalog_Module__c = objCatMod.Id;
       insert objCatInt;
       
       Catalog_Intervention__c objCI = TestClassHelper.createCatalogIntervention(); 
       objCI.Name = 'Please specify';
       objCI.Catalog_Module__c = objCatMod.Id;
       insert objCI;
       
       Catalog_Intervention__c objCITest = TestClassHelper.createCatalogIntervention(); 
       objCITest.Name = 'Test Records';
       objCITest.Catalog_Module__c = objCatMod.Id;
       insert objCITest;
       
       test.startTest();
       Account objAccount = TestClassHelper.createAccount();
       insert objAccount;
       
       Grant__c objGrant = TestClassHelper.insertGrant(objAccount);
      
       Implementation_Period__c objIP = TestClassHelper.createIPWithConceptNote(objGrant, objAccount);
       insert objIP;
       
       Performance_Framework__c objPF = TestClassHelper.createPF(objIP);
       insert objPF;
       
       IP_Detail_Information__c objIPDetail = TestClassHelper.createIPDetail();
       objIPDetail.Implementation_Period__c = objIP.Id;
       insert objIPDetail;
       
       Module__c objModule =  TestClassHelper.createModule();
       objModule.Catalog_Module__c = objCatMod.Id;
       objModule.Implementation_Period__c = objIP.Id;
       objModule.Performance_Framework__c = objPF.Id;
       insert objModule;
       
       Grant_Intervention__c objGI = TestClassHelper.createGrantIntervention(objIP);
       objGI.Module__c = objModule.Id;
       objGI.Catalog_Intervention__c = objCatInt.Id;
       objGI.Performance_Framework__c = objPF.Id;
       insert objGI; 
       test.stopTest();
       
       Apexpages.currentpage().getparameters().put('id',objIPDetail.Id);       
       ApexPages.StandardController sc = new ApexPages.StandardController(objIPDetail);
       ctrlDetailPageLink objctrlDetailPageLink = new ctrlDetailPageLink(sc);
    }
}