public class editController {
	
    private final Sobject sObj;
    public Id recordId;
    public String returnUrlId;
    public String sObjName;
    public boolean page_redirect;
    public boolean goal_objective;
    public boolean new_implementer;
    public String query;
    public String moduleUrl;
    public Goals_Objectives__c GoalObj;
    public Implementer__c ImplementerObj;
    public Id profileId;
    public String profileName;
    public string status;
    public String getBackUrl {get; set;}
    public String error {get; set;}
    Public List<npe5__Affiliation__c> lstAffLFA;
    public string test123;
    public editController(ApexPages.StandardController stdController) {        
        getBackUrl = null;                
        page_redirect = false;
        moduleUrl = null;
        profileId=userinfo.getProfileId();
        profileName=[Select Id,Name from Profile where Id=:profileId].Name;
        getBackUrl = null;
        error = '';
        returnUrlId = ApexPages.currentPage().getParameters().get('retUrl');
        recordId = ApexPages.currentPage().getParameters().get('id');
        sObjName = recordId.getsObjectType().getDescribe().getName();
        query = 'SELECT Grant_Status__c,CreatedById FROM ' + sObjName + ' WHERE id =: recordId';
        system.debug('query:'+query);
        sObj = Database.query(query);
        system.debug('Sobject Record:'+sObj);
        status = String.valueOf(sObj.get('Grant_Status__c'));
        system.debug('Sobject Grant Status:'+status);
        system.debug('Sobject Name:'+sObjName);
        }
        
    public PageReference reDirect() 
    {
        List<npe5__Affiliation__c> conAffltn = new List<npe5__Affiliation__c>();
        List<npe5__Affiliation__c> conAffltnLFA = new List<npe5__Affiliation__c>();
        PageReference editAction;
        String baseUrl;
        string userTypeStr = UserInfo.getUserType();
        baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
        if(userTypeStr == 'Standard') 
        {
            baseUrl = baseUrl+'/';
        }
        else
        {
            Id profileId=userinfo.getProfileId();
            Id usrId=UserInfo.getUserId();
            Id conId =[SELECT ContactId FROM User where Id=:usrId limit 1].ContactId;
            
            
            EditDeleteControllerbyAffiliation__c csdata = EditDeleteControllerbyAffiliation__c.getInstance(sObjName);
            if(csdata != null)
            {
                String fldName = csdata.Imp_Period_Lkup_APIName__c;
                System.debug(' ######## '+fldName);
                String fetchimpId = 'Select '+fldName+' from ' +sObjName+ ' where Id = \''+recordId+'\'';
                System.debug('%%%%% '+fetchimpId);
                LIST<SObject> impList = Database.query(fetchimpId);
                System.debug('#####  '+impList);
                String impId='';
                if(impList.size()>0)
                {
                    impId += ( impList[0].get(fldName));
                    System.debug('#####$  '+impId);                    
                }
                System.debug('#### '+impid);
                List<Implementation_Period__c> prName = [Select Principal_Recipient__r.Name,Grant__c from Implementation_Period__c where Id =:impId limit 1];
                System.debug('##### '+prName);
                Grant__c objgrant = [Select id,Country__r.LFA__c, Country__r.LFA__r.ParentID from Grant__c where id=: prName[0].Grant__c]; 
                 System.debug('**objgrant '+objgrant );
                 ID objLFAAccId = objgrant.Country__r.LFA__c;
                 Id objLFAParentId = objgrant.Country__r.LFA__r.ParentID;
                 //lstAffLFA = new List<npe5__Affiliation__c>();
                 //lstAffLFA = [Select Access_Level__c, id,npe5__Contact__c,npe5__Organization__c FROM npe5__Affiliation__c where npe5__Contact__c=: obj.ContactId and (npe5__Organization__c =:objLFAAccId OR npe5__Organization__c =:objLFAParentId) and npe5__Status__c='Current' order by LastModifiedDate desc ];
                                        
                conAffltn =[Select Id from npe5__Affiliation__c where npe5__Status__c='Current' and npe5__Contact__c=:conId and Access_Level__c='Read' and npe5__Organization__r.Name=:prName[0].Principal_Recipient__r.Name];
                conAffltnLFA =[Select Id from npe5__Affiliation__c where npe5__Status__c='Current' and npe5__Contact__c=:conId and Access_Level__c='Read' and (npe5__Organization__c =:objLFAAccId OR npe5__Organization__c =:objLFAParentId) ];    
            }
            String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
            if(profileName.Contains('PR') || profileName.Contains('Applicant/CM') )
            baseUrl = baseUrl+'/GM/';
            else
            baseUrl = baseUrl+'/LFA/';           
        } 
        System.debug('@@@@@@@@@@@@@@' +!(profileName.Contains('PR'))+' $$$$$$ '+!(conAffltn.size()>0));
        List<Profile_Access_Setting__c> lstProAcc = new List<Profile_Access_Setting__c>();
        System.debug('######### '+sObjName+' $$ '+status);
        lstProAcc = [SELECT id, Name, Salesforce_Item__c, Status__c, Page_Name__c, Profile_Id__c, Profile_Name__c 
                     FROM Profile_Access_Setting__c 
                     WHERE Profile_Name__c =: profileName AND Page_Name__c =: sObjName AND Status__c =: status AND Salesforce_Item__c = 'Edit'];
        system.debug('Custom Setting data:'+lstProAcc+'');
        System.debug(';;PR;' +profileName.Contains('PR')+' ;;;LFA;;; '+profileName.Contains('LFA')+' ;;;affl;; '+(conAffltn.size()>0)+' ;;;;; '+((profileName.Contains('PR') || profileName.Contains('LFA')) && (conAffltn.size()>0)));
        if(((profileName.Contains('PR') || profileName.Contains('LFA')) && (conAffltn.size()>0) || conAffltnLFA.size()>0)<> true)
        {
            System.debug('**** in IF *** '+sObjName+' *** '+lstProAcc);
            if(lstProAcc.size() > 0 && sObjName != 'Performance_Framework__c' && sObjName != 'Grant_Intervention__c' && sObjName != 'HPC_Framework__c')
            {
                 boolean childOfPF = false;
                 getBackUrl ='';                 
                 Map<String, SobjectField> fieldMap = recordId.getsObjectType().getDescribe().Fields.getMap(); 
                 for(String f :  fieldMap.keySet())
                 {
                    if(fieldMap.get(f).getDescribe().getName() == 'Performance_Framework__c')
                    {
                        childOfPF = true;
                    }    
                 }
                 if(childOfPF)
                 {
                     system.debug('Is Child of PF is true');
                     String fetchpfId = 'Select Performance_Framework__c from ' +sObjName+ ' where Id = \''+recordId+'\'';
                     LIST<SObject> sObjectList = Database.query(fetchpfId);
                     if(sObjectList.size()>0)
                     {
                         getBackUrl += baseUrl+String.valueOf( sObjectList[0].get('Performance_Framework__c') );                    
                     }
                     else
                     {
                          getBackUrl += baseUrl+'apex/GrantMakingHome?sfdc.tabName=01rb0000000b9g6';
                     }
                 }
                 else
                 {              
                     if(sObjName == 'Ind_Goal_Jxn__c'){
                         String fetchpfId1 = 'Select Goal_Objective__c from ' +sObjName+ ' where Id = \''+recordId+'\'';
                         LIST<SObject> sObjectList1 = Database.query(fetchpfId1);
                         if(sObjectList1.size()>0)
                         {
                             getBackUrl += baseUrl+String.valueOf( sObjectList1[0].get('Goal_Objective__c') );                    
                         }
                        }else if(sObjName == 'Grant_Multi_Country__c'){
                        /***Added For Restricting Edit Controller**/
                         String fetchpfId = 'Select Grant_Implementation_Period__c from ' +sObjName+ ' where Id = \''+recordId+'\'';
                         LIST<SObject> sObjectList = Database.query(fetchpfId);
                         system.debug('Sobject List is greateer than one '+sObjectList );
                         if(sObjectList.size()>0)
                         {
                             getBackUrl += baseUrl+String.valueOf( sObjectList[0].get('Grant_Implementation_Period__c') );                    
                         }
                     }else{
                             getBackUrl += baseUrl+'apex/GrantMakingHome?sfdc.tabName=01rb0000000b9g6';
                     }                 
                 }
                system.debug('list size is greater than zero');
                System.debug('Base URL: ' + getBackUrl);       
                error = 'You are not allowed to edit the record';            
                ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.Warning, label.GM_Edit_Message1);
                ApexPages.addMessage(errorMsg);
                page_redirect = false;
            }
            else if(sObjName == 'Grant_Intervention__c')
            {
             Grant_Intervention__c objGI = [Select id,name,Custom_Intervention_Name__c,Module__c from Grant_Intervention__c where id=:recordId];                        
               if(lstProAcc.size() > 0)
               {
                   system.debug('I am in 1nd if');
                   getBackUrl = baseUrl+objGI.Module__c;
                   System.debug('Base URL: ' + getBackUrl);        
                   ApexPages.Message errorMsg1 = new ApexPages.Message(ApexPages.Severity.Warning, label.GM_Edit_Message2);
                   ApexPages.addMessage(errorMsg1 );
                   page_redirect = false;
               }
               else
               {
                   system.debug('I am in 1nd else ');
                   if(objGI.Custom_Intervention_Name__c !=null )
                   {
                       system.debug('I am in 1nd else if ');
                       moduleUrl = baseUrl+'apex/overideGIedit?id='+recordId+'&retURL=%2F'+recordId+'&sfdc.override=1';
                       editAction = new PageReference(moduleUrl);
                       editAction.setRedirect(true);
                       page_redirect = true;
                   }
                   else
                   {
                       system.debug('I am in 2nd Else');
                       getBackUrl = baseUrl+objGI.Module__c;
                       System.debug('Base URL: ' + getBackUrl);        
                       ApexPages.Message errorMsg1 = new ApexPages.Message(ApexPages.Severity.Warning, label.GM_Edit_Message2);
                       ApexPages.addMessage(errorMsg1 );
                       page_redirect = false;
                   } 
               }                         
            }        
            else if(lstProAcc.size() > 0 && sObjName == 'Performance_Framework__c')
            {
                getBackUrl ='';
                system.debug('list size is greater than zero and object is Performance Framework');
                if(returnUrlId.indexOf('/') != -1)
                    {
                       returnUrlId = returnUrlId.substring(returnUrlId.lastIndexOf('/')+1,returnUrlId.length()); 
                    }
                    if(returnUrlId.length()>=15)
                    {
                       getBackUrl += baseUrl+returnUrlId; 
                    }
                    else
                    {
                        getBackUrl += baseUrl+recordId;
                    }
                ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.Warning, label.GM_Edit_Message2);
                ApexPages.addMessage(errorMsg);
                page_redirect = false;
            }
            else if(lstProAcc.size() > 0 && sObjName == 'HPC_Framework__c')
            {
                getBackUrl ='';
                system.debug('list size is greater than zero and object is Performance Framework');
                if(returnUrlId.indexOf('/') != -1)
                    {
                       returnUrlId = returnUrlId.substring(returnUrlId.lastIndexOf('/')+1,returnUrlId.length()); 
                    }
                    if(returnUrlId.length()>=15)
                    {
                       getBackUrl += baseUrl+returnUrlId; 
                    }
                    else
                    {
                        getBackUrl += baseUrl+recordId;
                    }
                ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.Warning, label.GM_Edit_Message3);
                ApexPages.addMessage(errorMsg);
                page_redirect = false;
            }
            else if(sObjName =='DocumentUpload_GM__c' && profileName != 'Super User' && profileName != 'System Administrator')
            {            
                System.debug('----'+String.valueOf(sObj.get('CreatedById'))+'===='+String.valueOf(UserInfo.getUserId()));
                if(String.valueOf(sObj.get('CreatedById')) != String.valueOf(UserInfo.getUserId()))
                {
                    getBackUrl ='';
                    system.debug('list size is zero and object is Document Upload');
                    if(returnUrlId.indexOf('/') != -1)
                    {
                       returnUrlId = returnUrlId.substring(returnUrlId.lastIndexOf('/')+1,returnUrlId.length()); 
                    }
                    if(returnUrlId.length()>=15)
                    {
                       getBackUrl += baseUrl+returnUrlId; 
                    }
                    else
                    {
                        getBackUrl += baseUrl+recordId;
                    }
                    ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.Warning,'You are not allowed to edit this record.');
                    ApexPages.addMessage(errorMsg);
                    page_redirect = false;
                }
                else
                {
                 string docUrl;
                 string domainstr  = '';
                 if(baseUrl.contains('GM'))
                 {
                     domainstr='/GM/';
                 }
                 if(baseUrl.contains('LFA'))
                 {
                     domainstr='/LFA/';
                 }
                 docUrl = baseUrl+'apex/EditDocumentUpload?id='+recordId;
                 system.debug('Redirect URL '+docUrl );
                 editAction = new PageReference(docUrl);
                 editAction.setRedirect(true);
                 page_redirect = true;
                }
            }
            else
            {
                 system.debug('I am in inner IF');
                 String moduleUrl;
                 if(sObjName == 'Grant_Indicator__c')
                 {
                     Grant_Indicator__c gi = [Select id, Performance_Framework__r.Implementation_Period__r.Principal_Recipient__c  from Grant_Indicator__c where id=:recordId];                   
                     User obj = [Select id, Name, ContactId from User where Id=:UserInfo.getUserId()];
                     String profileNameStr = [select id, name from Profile where id =: UserInfo.getProfileId()].Name;
                     
                     if( obj.ContactId != NULL )
                     {
                         if(profileNameStr.contains('LFA')){
                             Set<Id> setAccID = new Set<Id>();
                             Grant_Indicator__c giLFA = [Select id, Performance_Framework__r.Implementation_Period__r.Principal_Recipient__c,Performance_Framework__r.Implementation_Period__r.Grant__c from Grant_Indicator__c where id=:recordId]; 
                             Grant__c objgrant = [Select id,Country__r.LFA__c, Country__r.LFA__r.ParentID from Grant__c where id=: giLFA.Performance_Framework__r.Implementation_Period__r.Grant__c]; 
                             System.debug('**objgrant '+objgrant );
                             ID objLFAAccId = objgrant.Country__r.LFA__c;
                             Id objLFAParentId = objgrant.Country__r.LFA__r.ParentID;
                             
                             lstAffLFA = new List<npe5__Affiliation__c>();
                             lstAffLFA = [Select Access_Level__c, id,npe5__Contact__c,npe5__Organization__c FROM npe5__Affiliation__c where npe5__Contact__c=: obj.ContactId and (npe5__Organization__c =:objLFAAccId OR npe5__Organization__c =:objLFAParentId) and npe5__Status__c='Current' order by LastModifiedDate desc ];
                                        
                         }
                         List<npe5__Affiliation__c> lstAff = [Select Access_Level__c, id,npe5__Contact__c,npe5__Organization__c FROM npe5__Affiliation__c where npe5__Contact__c=: obj.ContactId and npe5__Organization__c=:gi.Performance_Framework__r.Implementation_Period__r.Principal_Recipient__c  and npe5__Status__c='Current' order by LastModifiedDate desc ];
                         System.debug('@@@@@@@@@@@@ '+lstAff);
                         if(!profileNameStr.contains('LFA') && lstAff[0].Access_Level__c == 'Read')
                         {
                             getBackUrl ='';
                             system.debug('list size is greater than zero and object is Performance Framework');
                             getBackUrl += baseUrl+recordId;
                             ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.Warning, label.GM_Edit_Message2);
                             ApexPages.addMessage(errorMsg);
                             page_redirect = false;   
                             return null;    
                         }
                         else if(profileNameStr.contains('LFA') && lstAffLFA[0].Access_Level__c == 'Read'){
                             getBackUrl ='';
                             system.debug('list size is greater than zero and object is Performance Framework');
                             getBackUrl += baseUrl+recordId;
                             ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.Warning, label.GM_Edit_Message2);
                             ApexPages.addMessage(errorMsg);
                             page_redirect = false;   
                             return null;    
                         }else{
                             moduleUrl = baseUrl+'apex/overideedit?id='+recordId+'&retURL=%2F'+recordId+'&sfdc.override=1';      
                         }                         
                     }
                     else
                     {          
                         moduleUrl = baseUrl+'apex/overideedit?id='+recordId+'&retURL=%2F'+recordId+'&sfdc.override=1';
                     }
                 }
                 else if(sObjName == 'Grant_Intervention__c')
                 {
                      moduleUrl = baseUrl+'apex/overideGIedit?id='+recordId+'&retURL=%2F'+recordId+'&sfdc.override=1';
                 }
                 else
                 {                    
                      moduleUrl = baseUrl+recordId+'/e?nooverride=1&retURL=%2F'+recordId+'&saveURL=%2F'+recordId;
                 }    
                 editAction = new PageReference(moduleUrl);
                 editAction.setRedirect(true);
                 page_redirect = true;
            }      
        }
        else
        {
            System.debug('%%%%%% in else %%%%%%');
            if(lstProAcc.size()>0 && sObjName != 'Performance_Framework__c' && sObjName != 'Grant_Intervention__c' && sObjName != 'HPC_Framework__c')
            {
                 boolean childOfPF = false;
                 getBackUrl ='';                 
                 Map<String, SobjectField> fieldMap = recordId.getsObjectType().getDescribe().Fields.getMap(); 
                 for(String f :  fieldMap.keySet())
                 {
                    if(fieldMap.get(f).getDescribe().getName() == 'Performance_Framework__c')
                    {
                        childOfPF = true;
                    }    
                 }
                 if(childOfPF)
                 {
                     system.debug('Is Child of PF is true');
                     String fetchpfId = 'Select Performance_Framework__c from ' +sObjName+ ' where Id = \''+recordId+'\'';
                     LIST<SObject> sObjectList = Database.query(fetchpfId);
                     if(sObjectList.size()>0)
                     {
                         getBackUrl += baseUrl+String.valueOf( sObjectList[0].get('Performance_Framework__c') );                    
                     }
                     else
                     {
                          getBackUrl += baseUrl+'apex/GrantMakingHome?sfdc.tabName=01rb0000000b9g6';
                     }
                 }
                 else
                 {              
                     if(sObjName == 'Ind_Goal_Jxn__c'){
                         String fetchpfId1 = 'Select Goal_Objective__c from ' +sObjName+ ' where Id = \''+recordId+'\'';
                         LIST<SObject> sObjectList1 = Database.query(fetchpfId1);
                         if(sObjectList1.size()>0)
                         {
                             getBackUrl += baseUrl+String.valueOf( sObjectList1[0].get('Goal_Objective__c') );                    
                         }
                        }else if(sObjName == 'Grant_Multi_Country__c'){
                        /***Added For Restricting Edit Controller**/
                         String fetchpfId = 'Select Grant_Implementation_Period__c from ' +sObjName+ ' where Id = \''+recordId+'\'';
                         LIST<SObject> sObjectList = Database.query(fetchpfId);
                         system.debug('Sobject List is greateer than one '+sObjectList );
                         if(sObjectList.size()>0)
                         {
                             getBackUrl += baseUrl+String.valueOf( sObjectList[0].get('Grant_Implementation_Period__c') );                    
                         }
                     }else{
                             getBackUrl += baseUrl+'apex/GrantMakingHome?sfdc.tabName=01rb0000000b9g6';
                     }                 
                 }
                system.debug('list size is greater than zero');
                System.debug('Base URL: ' + getBackUrl);       
                error = 'You are not allowed to edit the record';            
                ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.Warning, label.GM_Edit_Message1);
                ApexPages.addMessage(errorMsg);
                page_redirect = false;
            }        
            else if(sObjName == 'Grant_Intervention__c')
            {
             Grant_Intervention__c objGI = [Select id,name,Custom_Intervention_Name__c,Module__c from Grant_Intervention__c where id=:recordId];
               getBackUrl ='';
               getBackUrl = baseUrl+objGI.Module__c;       
               ApexPages.Message errorMsg1 = new ApexPages.Message(ApexPages.Severity.Warning, label.GM_Edit_Message2);
               ApexPages.addMessage(errorMsg1 );
               page_redirect = false;                       
            }        
            else if(sObjName == 'Performance_Framework__c')
            {
                getBackUrl ='';
                if(returnUrlId.indexOf('/') != -1)
                {
                   returnUrlId = returnUrlId.substring(returnUrlId.lastIndexOf('/')+1,returnUrlId.length()); 
                }
                if(returnUrlId.length()>=15)
                {
                   getBackUrl += baseUrl+returnUrlId; 
                }
                else
                {
                    getBackUrl += baseUrl+recordId;
                }
                ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.Warning, label.GM_Edit_Message2);
                ApexPages.addMessage(errorMsg);
                page_redirect = false;
            }
            else if(sObjName == 'HPC_Framework__c')
            {
                getBackUrl ='';
                if(returnUrlId.indexOf('/') != -1)
                {
                   returnUrlId = returnUrlId.substring(returnUrlId.lastIndexOf('/')+1,returnUrlId.length()); 
                }
                if(returnUrlId.length()>=15)
                {
                   getBackUrl += baseUrl+returnUrlId; 
                }
                else
                {
                    getBackUrl += baseUrl+recordId;
                }
                ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.Warning, label.GM_Edit_Message3);
                ApexPages.addMessage(errorMsg);
                page_redirect = false;
            }
            else if(sObjName =='DocumentUpload_GM__c')
            {
                getBackUrl ='';
                if(returnUrlId.indexOf('/') != -1)
                {
                   returnUrlId = returnUrlId.substring(returnUrlId.lastIndexOf('/')+1,returnUrlId.length()); 
                }
                if(returnUrlId.length()>=15)
                {
                   getBackUrl += baseUrl+returnUrlId; 
                }
                else
                {
                    getBackUrl += baseUrl+recordId;
                }
                ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.Warning,'You are not allowed to edit this record.');
                ApexPages.addMessage(errorMsg);
                page_redirect = false;
            }
            else
            {
                    getBackUrl ='';
                    if(sObjName == 'Ind_Goal_Jxn__c')
                    {
                         String fetchpfId1 = 'Select Goal_Objective__c from ' +sObjName+ ' where Id = \''+recordId+'\'';
                         LIST<SObject> sObjectList1 = Database.query(fetchpfId1);
                         if(sObjectList1.size()>0)
                         {
                             getBackUrl += baseUrl+String.valueOf( sObjectList1[0].get('Goal_Objective__c') );                    
                         }
                    }
                    else if(sObjName == 'Grant_Multi_Country__c')
                    {
                        /***Added For Restricting Edit Controller**/
                         String fetchpfId = 'Select Grant_Implementation_Period__c from ' +sObjName+ ' where Id = \''+recordId+'\'';
                         LIST<SObject> sObjectList = Database.query(fetchpfId);
                         system.debug('Sobject List is greateer than one '+sObjectList );
                         if(sObjectList.size()>0)
                         {
                             getBackUrl += baseUrl+String.valueOf( sObjectList[0].get('Grant_Implementation_Period__c') );                    
                         }
                     }
                    else
                    {
                        if(returnUrlId.indexOf('/') != -1)
                            {
                               returnUrlId = returnUrlId.substring(returnUrlId.lastIndexOf('/')+1,returnUrlId.length()); 
                            }
                            if(returnUrlId.length()>=15)
                            {
                               getBackUrl += baseUrl+returnUrlId; 
                            }
                            else
                            {
                                getBackUrl += baseUrl+recordId;
                            }
                        ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.Warning, label.GM_Edit_Message2);
                        ApexPages.addMessage(errorMsg);
                        page_redirect = false;   
                        return null;
                    }                                
            }
        }
        if(page_redirect)
        {
            try 
            {
                return editAction;
            }
            catch (exception e) 
            {                    
                ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.Warning, label.GM_Edit_Message2);
                ApexPages.addMessage(errorMsg);
                page_redirect = false;
                return NULL;
            }
        }
        else
        {
            return null;
        }
    }
}