public with sharing class Help_Guidance_Goals_Objectives{
    public String strLanguage {get; set;}
    public Guidance__c guidance_goal {get; set;}
    public Guidance__c guidance_objective {get; set;}
    
    public Help_Guidance_Goals_Objectives(){
        strLanguage = 'ENGLISH';
        if(System.UserInfo.getLanguage() == 'fr'){
            strLanguage = 'FRENCH'; 
        }
        if(System.UserInfo.getLanguage() == 'ru'){
            strLanguage = 'RUSSIAN'; 
        }
        if(System.UserInfo.getLanguage() == 'es'){
            strLanguage = 'SPANISH'; 
        }
        Goals_Objectives();
    }
    
    public void Goals_Objectives(){
        guidance_goal = [SELECT Name, English_Guidance__c, French_Guidance__c,GM_Guidance_Name__c, Russian_Guidance__c, Spanish_Guidance__c, 
                    English_Guidance_Overflow__c, French_Guidance_Overflow__c, Russian_Guidance_Overflow__c, Spanish_Guidance_Overflow__c
                    FROM Guidance__c WHERE Name = 'GM Goals and Objectives'];
                    
        guidance_objective = [SELECT Name, English_Guidance__c, French_Guidance__c,GM_Guidance_Name__c, Russian_Guidance__c, Spanish_Guidance__c, 
                    English_Guidance_Overflow__c, French_Guidance_Overflow__c, Russian_Guidance_Overflow__c, Spanish_Guidance_Overflow__c
                    FROM Guidance__c WHERE Name = 'GM Goals and Objectives'];
    }
}