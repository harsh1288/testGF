public with sharing class CovOutputInlineEditController {
    
    public List<Grant_Indicator__c> shwIndicatorsList {get;set;}
    public boolean shwmsg{get;set;}
    public boolean saved{get;set;}
    public Date startDate {get; set;}
    public Date endDate {get; set;}
    public Boolean displayFiscal {get; set;}
    public Result__c objResult;
    public List<Period__c> lstPeriodYearly {get;set;}
    public List<Period__c> lstPeriodPF{get;set;}
    public List<Result__c> lstResultFreq{get;set;}
    public List<Result__c> lstResultYear{get;set;}
    public List<Result__c> lstResultInsert{get;set;}    
    public List<Grant_Indicator__c> lstGrantOnlyYearly {get;set;}
    public List<Grant_Indicator__c> lstGrantBasedonFreq {get;set;}
    public List<GrantIndicatorResult> lstGrantIndicatorResult {get;set;}
    public List<GrantIndicatorResult> lstGrantIndicatorResultYearly {get;set;}
    public Boolean myFlag{get; set;}
   
    public Boolean indEdit{get; set;}
    public String freqName{get;set;}
    // for component attribute
    public string targetRecord{get;set;}
    public string pfStatus{get;set;}
    public string dataType{get;set;}
    public string decimalPlaces{get;set;}
    public String indName{get;set;}
    public String pfId;
    public String indicatortype;    
    
    public CovOutputInlineEditController(ApexPages.StandardSetController standardController){
        indEdit = true;
        shwmsg = false;
        saved = false;
        myFlag=false;
        indicatortype = 'Coverage/Output';
        pfId = ApexPages.currentPage().getParameters().get('id');   
        lstGrantIndicatorResult = new List<GrantIndicatorResult>();
        lstGrantIndicatorResultYearly = new List<GrantIndicatorResult>(); 
        lstResultFreq = new List<Result__c> ();
        lstResultYear = new List<Result__c> ();
        lstResultInsert = new List<Result__c>();
        Performance_Framework__c objPF = [Select Id,Implementation_Period__c,Implementation_Period__r.Reporting__c from Performance_Framework__c where Id=:pfId];
        lstPeriodYearly = new List<Period__c> ([Select id,Start_Date_Short__c from Period__c where Base_Frequency__c = 'Yearly' and is_Active__c = true and Implementation_Period__c =: objPF.Implementation_Period__c  order by Start_Date__c asc]);
        lstPeriodPF = new List<Period__c> ([Select id,Start_Date_Short__c from Period__c where Performance_Framework__c =: pfId  order by Start_Date__c asc]);
        lstGrantBasedonFreq = new List<Grant_Indicator__c>([ Select Id,Name,Indicator_Full_Name__c,Standard_or_Custom__c,Indicator_Type__c,
                                                          Data_Type__c,Baseline_numerator__c,Baseline_Denominator__c,Baseline_Value1__c,Baseline_Year__c,
                                                          Baseline_Sources__c,Component__c,Decimal_Places__c,Reporting_Frequency__c,Is_Disaggregated1__c,Performance_Framework__r.Grant_Status__c,
                                                          (Select id,Period__r.Start_Date_Short__c,Target__c,Target_Numerator__c,Target_Denominator__c 
                                                          from Results__r where Period__r.is_active__c = true AND Period__r.Base_Frequency__c =: objPF.Implementation_Period__r.Reporting__c 
                                                           order by Period__r.Start_Date__c asc) ,  Parent_Module__r.Name 
                                                          from Grant_Indicator__c 
                                                          where Performance_Framework__c =: pfId  AND Reporting_Frequency__c = 'Based on Reporting Frequency' limit 10000 ]);    
        
        lstGrantOnlyYearly = new List<Grant_Indicator__c>([ Select Id,Name,Indicator_Full_Name__c,Standard_or_Custom__c,Indicator_Type__c,
                                                          Data_Type__c,Baseline_numerator__c,Baseline_Denominator__c,Baseline_Value1__c,Baseline_Year__c,
                                                          Baseline_Sources__c,Component__c,Decimal_Places__c,Reporting_Frequency__c,Is_Disaggregated1__c,Performance_Framework__r.Grant_Status__c,
                                                          (Select id,Period__r.Start_Date_Short__c,Target__c,Target_Numerator__c,Target_Denominator__c 
                                                          from Results__r where Period__r.is_active__c = true AND Period__r.Base_Frequency__c = 'Yearly'
                                                          order by Period__r.Start_Date__c asc)  , Parent_Module__r.Name 
                                                          from Grant_Indicator__c  
                                                          where Performance_Framework__c =: pfId AND Reporting_Frequency__c = '12 Months' limit 10000 ]); 
                                                          
         system.debug('**lstGrantOnlyYearly'+lstGrantOnlyYearly);   
        
         
         //freqName = objPF.Implementation_Period__r.Reporting__c + ' Reporting';
         freqName = 'Based on Reporting Frequency ';
        /*GrantIndicatorResult objGrantIndicatorResult;    
        lstGrantIndicatorResult = new List<GrantIndicatorResult>();                                            
          for(Grant_Indicator__c objGI : lstGrantBasedonFreq){
                objGrantIndicatorResult = new GrantIndicatorResult();
                                
                objGrantIndicatorResult.objGrantIndicator = objGI;
                if(objGI.Results__r !=null )
                {
                objGrantIndicatorResult.lstResults = objGI.Results__r;
                }else{
                    if(objGI.Reporting_Frequency__c == '12 Months')
                    {
                        for(Period__c objPer : lstPeriodYearly)
                        {
                            objResult = new Result__c();
                            objResult.Period__c =  objPer.ID ;
                            objResult.Indicator__c = objGI.id;
                            insert objResult;
                        }
                    }else{
                        for(Period__c objPer : lstPeriodPF)
                        {
                            objResult = new Result__c();
                            objResult.Period__c =  objPer.ID ;
                            objResult.Indicator__c = objGI.id;
                            insert objResult;
                        }
                    }
                
                
               }
                lstGrantIndicatorResult.add(objGrantIndicatorResult);   
          }        */                                      
        /*shwIndicatorsList = new List<Grant_Indicator__c>([Select Id,Name,Indicator_Full_Name__c,Standard_or_Custom__c,Indicator_Type__c,
                                                          Data_Type__c,Baseline_numerator__c,Baseline_Denominator__c,Baseline_Value1__c,Baseline_Year__c,
                                                          Baseline_Sources__c,Component__c, Target_Value_Y12__c, Target_Value_Y22__c, Target_Value_Y32__c, 
                                                          Target_Value_Y42__c, Decimal_Places__c,Target_Numerator_Y1__c,Target_Numerator_Y2__c,Target_Numerator_Y3__c,
                                                          Target_Numerator_Y4__c,Target_Denominator_Y1__c,Target_Denominator_Y2__c,Target_Denominator_Y3__c,
                                                          Target_Denominator_Y4__c,Y1_Report_Due__c,Y2_Report_Due__c,Y3_Report_Due__c,Y4_Report_Due__c,
                                                          Grant_Implementation_Period__r.Start_Date__c, Grant_Implementation_Period__r.End_Date__c, 
                                                          Grant_Implementation_Period__r.Number_of_fiscal_cycles__c 
                                                          from Grant_Indicator__c 
                                                          where Performance_Framework__c =: pfId limit 10000 ]);
        startDate = shwIndicatorsList [0].Grant_Implementation_Period__r.Start_Date__c;
        endDate = shwIndicatorsList [0].Grant_Implementation_Period__r.End_Date__c;
        if(shwIndicatorsList [0].Grant_Implementation_Period__r.Number_of_fiscal_cycles__c== '3'){
            displayFiscal = false;
        }
        else{
            displayFiscal = true;
        }
        if(!(shwIndicatorsList.size() >0))
        {
            shwmsg= true;
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'No Indicators are available to show.')); 
        }*/
    }
    
       public void init()
     {  
         
          if(!myFlag){
             GrantIndicatorResult objGrantIndicatorResult;    
             shwmsg= false;
             //lstGrantIndicatorResult = new List<GrantIndicatorResult>();
             //lstGrantIndicatorResultYearly = new List<GrantIndicatorResult>();  
             System.debug('**lstGrantIndicatorResult'+lstGrantIndicatorResult.size());                                          
          for(Grant_Indicator__c objGI : lstGrantBasedonFreq){     
                List<Result__c> lstToADDResult  = new List<Result__c>();
                objGrantIndicatorResult = new GrantIndicatorResult();
                                                
                objGrantIndicatorResult.objGrantIndicator = objGI;
                if( objGI.Baseline_Value1__c == '' || objGI.Baseline_Value1__c == NULL){
                    objGI.Baseline_Value1__c = '0';
                }
                system.debug('**Before IF'+objGI.Results__r.size());
                /*if(objGI.Results__r.size()==0){
                    for(Period__c objPer : lstPeriodPF){
                        objResult = new Result__c();
                        objResult.Period__c =  objPer.ID ;
                        objResult.Indicator__c = objGI.id;
                        //insert objResult;
                        lstToADDResult.add(objResult);
                        myFlag=true;
                    }
                    objGrantIndicatorResult.lstResults = lstToADDResult;
                    
                }else{
                    system.debug('**Inside first if');
                    objGrantIndicatorResult.lstResults = objGI.Results__r;
                    myFlag=true;
               }*/
               boolean resultsFound = false;
               Map<Id, Result__c> periodResultMap = new Map<Id, Result__c>();
               for( Result__c res : objGI.Results__r){
                   periodResultMap.put(res.Period__r.Id, res );    
               }
               for( Period__c objPer : lstPeriodPF){
                   if( periodResultMap.containsKey(objPer.Id)){
                       resultsFound = true;
                       lstToADDResult.add( periodResultMap.get(objPer.Id));
                   }else{
                        resultsFound = true;
                        objResult = new Result__c();
                        objResult.Period__c =  objPer.ID ;
                        objResult.Indicator__c = objGI.id;
                        lstToADDResult.add(objResult);
                        myFlag=true;        
                   } 
               }
               /*if( !resultsFound ){
                   for(Period__c objPer : lstPeriodPF){
                       objResult = new Result__c();
                       objResult.Period__c =  objPer.ID ;
                       objResult.Indicator__c = objGI.id;
                       lstToADDResult.add(objResult);
                       myFlag=true;        
                   }
               }*/
                objGrantIndicatorResult.lstResults = lstToADDResult;
               lstGrantIndicatorResult.add(objGrantIndicatorResult); 
               System.debug('**lstGrantIndicatorResult'+lstGrantIndicatorResult.size());
          }
          List<Result__c> resultLst = new List<Result__c>();
          for( GrantIndicatorResult resultObj : lstGrantIndicatorResult){
              resultLst.addAll(resultObj.lstResults);
          }
          upsert resultLst;
          for(Grant_Indicator__c objGI : lstGrantOnlyYearly)
          {      
                  
                objGrantIndicatorResult = new GrantIndicatorResult();
                List<Result__c> lstToADDResult1  = new List<Result__c>();               
                objGrantIndicatorResult.objGrantIndicator = objGI;
                system.debug('**Before IF'+objGI.Results__r.size());
                system.debug('**Inside sencond if');
                boolean resultsPeresent = false;
                if(objGI.Reporting_Frequency__c == '12 Months'){
                    Map<Id, Result__c> periodTargetMap = new Map<Id, Result__c>();
                    for(Result__c res : objGI.Results__r){    
                        periodTargetMap.put(res.Period__r.Id, res);    
                    }
                    for(Period__c period : lstPeriodYearly ){
                        if( periodTargetMap.containsKey( period.Id) ){
                            resultsPeresent = true;
                            lstToADDResult1.add(periodTargetMap.get( period.Id) );    
                        }else{
                            resultsPeresent = true;
                            objResult = new Result__c();
                            objResult.Period__c =  period.ID ;
                            objResult.Indicator__c = objGI.id;
                            lstToADDResult1.add(objResult);    
                            myFlag=true;
                        }
                      
                    }
                    /*if( !resultsPeresent ){
                        for(Period__c objPer : lstPeriodYearly){
                            objResult = new Result__c();
                            objResult.Period__c =  objPer.ID ;
                            objResult.Indicator__c = objGI.id;
                            lstToADDResult1.add(objResult);
                            
                            myFlag=true;
                        }
                    }*/
                    objGrantIndicatorResult.lstResults = lstToADDResult1;
                }
               lstGrantIndicatorResultYearly.add(objGrantIndicatorResult); 
            }   
            List<Result__c> resultLst1 = new List<Result__c>();
          for( GrantIndicatorResult resultObj : lstGrantIndicatorResultYearly){
              resultLst1.addAll(resultObj.lstResults);
          }
          upsert resultLst1;
        }
        
         lstResultFreq = [select id from result__c where Period__c IN:lstPeriodPF AND Indicator__c IN : lstGrantBasedonFreq];
         lstResultYear = [select id from result__c where Period__c IN:lstPeriodYearly AND Indicator__c IN : lstGrantOnlyYearly];
         system.debug('**lstResultFreq'+lstResultFreq);
         system.debug('**lstResultYear'+lstResultYear);
         if(  lstPeriodYearly.isEmpty() || lstPeriodPF.isEmpty() ){
            shwmsg = true;
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,Label.RPMissing_Error));     
         }else{   
             if((lstResultFreq.size() == 0 && lstResultYear.size()==0)){
                shwmsg= true;
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,Label.EditCov_Err)); 
             }
         }
     }
    public void saveList()
    {
        List<Grant_Indicator__c> updatedIndicatorsList=new List<Grant_Indicator__c>();
        List<Result__c> updatedResult=new List<Result__c>();
        Integer flag = 0;
        Boolean stopSave = false;
        Decimal num;
        Decimal den;
        Integer idx=0;
        //for(GrantIndicatorResult objIndicatorResult : lstGrantIndicatorResult)
        //{
        for (GrantIndicatorResult validateNumDenFieldsOnIndicatorRecords : lstGrantIndicatorResult)
        {
            idx++;
            num = validateNumDenFieldsOnIndicatorRecords.objGrantIndicator.Baseline_numerator__c;
            den = validateNumDenFieldsOnIndicatorRecords.objGrantIndicator.Baseline_Denominator__c;
            //Check if numerator has proper value, then check corresponding denominator also has proper value, if not set flag and break out of for loop
           /* if(validateNumDenFieldsOnIndicatorRecords.objGrantIndicator.Data_Type__c == 'Number and Percent')
           {
                if(num!=null)
                {    
                    if (den ==0 || den == null)
                    {
                        flag = 1;
                        stopSave = true;
                        saved = false;
                        shwmsg= true;
                        //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Line ' + idx + ' - Please fill a non zero value in Baseline Denominator')); 
                        validateNumDenFieldsOnIndicatorRecords.objGrantIndicator.Baseline_Denominator__c.addError('Please fill a non zero value in Baseline Denominator field');
                        //  break;
                    }
                }
                //Check if den has proper value, then check corresponding num also has proper value, if not set flag and break out of for loop
                if(!(den == 0 || den == null))
                {
                    if( num == null )
                    {                 
                        flag = 1;
                        stopSave = true;
                        saved = false;
                        shwmsg= true;
                        //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Line ' + idx + ' - Please fill a value in Baseline Numerator field')); 
                        validateNumDenFieldsOnIndicatorRecords.objGrantIndicator.Baseline_Numerator__c.addError('Please fill a value in Baseline Numerator field');    
                        // break;
                    }
                }
            }*/
        }
        
        // Code for saving the value for indicator of Frequency type Yearly
        for (GrantIndicatorResult validateNumDenFieldsOnIndicatorRecords : lstGrantIndicatorResultYearly)
        {
            idx++;
            num = validateNumDenFieldsOnIndicatorRecords.objGrantIndicator.Baseline_numerator__c;
            den = validateNumDenFieldsOnIndicatorRecords.objGrantIndicator.Baseline_Denominator__c;
            //Check if numerator has proper value, then check corresponding denominator also has proper value, if not set flag and break out of for loop
           /* if(validateNumDenFieldsOnIndicatorRecords.objGrantIndicator.Data_Type__c == 'Number and Percent')
            {
                if(num!=null)
                {    
                    if (den ==0 || den == null)
                    {
                        flag = 1;
                        stopSave = true;
                        saved = false;
                        shwmsg= true;
                        //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Line ' + idx + ' - Please fill a non zero value in Baseline Denominator')); 
                        validateNumDenFieldsOnIndicatorRecords.objGrantIndicator.Baseline_Denominator__c.addError('Please fill a non zero value in Baseline Denominator field');
                        //  break;
                    }
                }
                //Check if den has proper value, then check corresponding num also has proper value, if not set flag and break out of for loop
                if(!(den == 0 || den == null))
                {
                    if( num == null )
                    {                 
                        flag = 1;
                        stopSave = true;
                        saved = false;
                        shwmsg= true;
                        //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Line ' + idx + ' - Please fill a value in Baseline Numerator field')); 
                        validateNumDenFieldsOnIndicatorRecords.objGrantIndicator.Baseline_Numerator__c.addError('Please fill a value in Baseline Numerator field');    
                        // break;
                    }
                }
            }*/
        }
        
        //}
       if(!stopSave)
        {
            for (GrantIndicatorResult addPercentSymbolToIndFields : lstGrantIndicatorResult )
            {
                if(addPercentSymbolToIndFields.objGrantIndicator.Data_Type__c == Label.Percent_DT || addPercentSymbolToIndFields.objGrantIndicator.Data_Type__c == Label.NaP_DT) {
                    if(addPercentSymbolToIndFields.objGrantIndicator.Baseline_Value1__c != null && addPercentSymbolToIndFields.objGrantIndicator.Baseline_Value1__c != '' && (!addPercentSymbolToIndFields.objGrantIndicator.Baseline_Value1__c.contains(Label.Percent_symb)))
                        addPercentSymbolToIndFields.objGrantIndicator.Baseline_Value1__c += Label.Percent_symb; 
              
                }
                if(addPercentSymbolToIndFields.objGrantIndicator.Data_Type__c == 'Ratio') {
                    if(addPercentSymbolToIndFields.objGrantIndicator.Baseline_Value1__c != null && addPercentSymbolToIndFields.objGrantIndicator.Baseline_Value1__c != '' && (!addPercentSymbolToIndFields.objGrantIndicator.Baseline_Value1__c.contains(Label.Ratio_label)))
                        addPercentSymbolToIndFields.objGrantIndicator.Baseline_Value1__c =  addPercentSymbolToIndFields.objGrantIndicator.Baseline_Value1__c+' ' +Label.Ratio_label; 
                
                }
                updatedIndicatorsList.add(addPercentSymbolToIndFields.objGrantIndicator);
            }
            
            for (GrantIndicatorResult addPercentSymbolToIndFields : lstGrantIndicatorResultYearly )
            {
                if(addPercentSymbolToIndFields.objGrantIndicator.Data_Type__c == Label.Percent_DT || addPercentSymbolToIndFields.objGrantIndicator.Data_Type__c == Label.NaP_DT) {
                    if(addPercentSymbolToIndFields.objGrantIndicator.Baseline_Value1__c != null && addPercentSymbolToIndFields.objGrantIndicator.Baseline_Value1__c != '' && (!addPercentSymbolToIndFields.objGrantIndicator.Baseline_Value1__c.contains(Label.Percent_symb)))
                        addPercentSymbolToIndFields.objGrantIndicator.Baseline_Value1__c += Label.Percent_symb; 
              
                }
                if(addPercentSymbolToIndFields.objGrantIndicator.Data_Type__c == 'Ratio') {
                    if(addPercentSymbolToIndFields.objGrantIndicator.Baseline_Value1__c != null && addPercentSymbolToIndFields.objGrantIndicator.Baseline_Value1__c != '' && (!addPercentSymbolToIndFields.objGrantIndicator.Baseline_Value1__c.contains(Label.Ratio_label)))
                        addPercentSymbolToIndFields.objGrantIndicator.Baseline_Value1__c =  addPercentSymbolToIndFields.objGrantIndicator.Baseline_Value1__c+' ' +Label.Ratio_label; 
                
                }
                updatedIndicatorsList.add(addPercentSymbolToIndFields.objGrantIndicator);
            }
            
            for(GrantIndicatorResult objRes : lstGrantIndicatorResult)
            {
                
                updatedResult.addALL(objRes.lstResults);
            }
            
            for(GrantIndicatorResult objRes : lstGrantIndicatorResultYearly)
            {
                
                updatedResult.addALL(objRes.lstResults);
            }
            
            //updatedIndicatorsList.add(lstGrantIndicatorResult.lstResults);
            try
            {
                update updatedIndicatorsList;
                update updatedResult;
                //update shwIndicatorsList;
                saved = true;    
            }
            catch(exception ex)
            {
                saved = false;
                shwmsg = true;
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'There was some error in saving records.')); 
            }
        } 
    }
    
     public class GrantIndicatorResult{
        public Grant_Indicator__c objGrantIndicator {get;set;}
        public List<Result__c> lstResults {get;set;}
        
        public GrantIndicatorResult(){
            lstResults = new List<Result__c>();
        }
     } 
     
     public PageReference EditDissagregation(){
         saveList();
         if(saved == true)
         {
            indEdit = false;
            PageReference pageRef = new PageReference('/apex/DisaggregationTableEdit?par1='+targetRecord+'&par2='+pfStatus+'&par3='+dataType+'&par4='+decimalPlaces+'&par5='+pfId+'&par6='+indicatortype+'&par7='+indName+'&sourceParam=InlineInd');
            pageRef.setRedirect(true);
            return pageRef;
         }
         else
         {
            return null;
         }
     }
     public PageReference  ReturnToInd(){
        indEdit = true;
        PageReference pageRef = new PageReference('/apex/IndicatorInlineCovOutput?id='+pfId);
        pageRef.setRedirect(true);
        return pageRef;
    }   
     
}