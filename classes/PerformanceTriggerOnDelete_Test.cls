@isTest
Private Class PerformanceTriggerOnDelete_Test{
    static testMethod void myUnitTest() {
    
    Account objAcc = TestClassHelper.insertAccount();
    
     Country__c country = TestClassHelper.createCountry();
     country.X200DEMA_Exch_Rate_in_EUR__c = 10.0000;
     country.X200DEMA_Exch_Rate_in_USD__c = 10.000;
     insert country;
     
     Grant__c objGrant  = TestClassHelper.createGrant(objAcc);
     objGrant.Country__c = country.Id;
     objGrant.Grant_Status__c = 'Accepted';
     insert objGrant;
     
     Implementation_Period__c ObjImplementationPeriod = TestClassHelper.createIP(objGrant,objAcc);
     ObjImplementationPeriod.Currency_of_Grant_Agreement__c = 'USD';
     ObjImplementationPeriod.Local_Currency__c = 'COP';
     ObjImplementationPeriod.Other_Currency__c = 'EUR';
     ObjImplementationPeriod.Exchange_Rate__c = 50;
     ObjImplementationPeriod.Budget_Status__c = 'Draft';
     insert ObjImplementationPeriod;
     
       Performance_Framework__C objPF = TestClassHelper.createPF(ObjImplementationPeriod);
       insert objPF;
       
       delete objPF;
    }
}