/*********************************************************************************
* {Controller} Class: {ctrlChangeRequestReview}
* DateCreated  : 01/31/2014
----------------------------------------------------------------------------------
* Purpose/Methods:
* - This controller used for Change Request Review page which opens through a button on change request 
    and will be used for manage change request reviews.
* 
* Unit Test: TestctrlChangeRequestReview
----------------------------------------------------------------------------------
* History:
* - VERSION  DEVELOPER NAME    DATE            DETAIL FEATURES
    1.0                      01/31/2014      INITIAL DEVELOPMENT
*********************************************************************************/

Public Class ctrlChangeRequestReview{
    Public Boolean blnHasChild {get; set;}
    Public String strCRRID {get; set;}
    Public String strPopupName {get; set;}
    Public String strSendRemName {get;set;}
    Public String strCountryTeamUserNames {get; set;}
    Public String strPSMUserNames {get; set;}
    Public String strFinanceUserNames {get; set;}
    Public String strMandEUserNames {get; set;}
    Public String strPurchasingUserNames {get; set;}
    Public String strLFACoordinationComments {get; set;}
    Public List<Change_Request_Review__c> lstCRRUsers {get; set;}
    Public Change_Request_Review__c objCRR {get; set;}
    Public Set<ID> setApprovalProcessIDs {get; set;}
    Public String dateofLastRemCT {get;set;}
    Public String dateofLastRemPSM {get;set;}
    Public String dateofLastRemFNC {get;set;}
    Public String dateofLastRemME {get;set;}
    Public String dateofLastRemPUR {get;set;}
    Public Map<ID,String> mapUserType;
    
    Public ctrlChangeRequestReview(ApexPages.StandardController controller){
        strCRRID = System.currentPageReference().getParameters().get('id');
        System.Debug('@@@strCRRID: ' + strCRRID);
        List<Change_Request_Review__c> lstCRR = [Select Id,Name,LFA_Coordination_Comments__c From Change_Request_Review__c Where Change_Request__c =: strCRRID];
        if(lstCRR != null && lstCRR.size() > 0){
            strLFACoordinationComments = lstCRR[0].LFA_Coordination_Comments__c;
        }
        blnHasChild = false;
        setApprovalProcessIDs = new Set<ID>();
        mapUserType = new Map<ID,String>();
        objCRR = new Change_Request_Review__c();
        FillCountryTeamUsers();
        FillPSMUsers();
        FillFinanceUsers();
        FillMandEUsers();
        FillPurchasingUsers();
        LastRemDate();
    }
    
    Public void FillCountryTeamUsers(){
        strCountryTeamUserNames = '';
        List<Change_Request_Review__c> lstCountryTeamUsers = new List<Change_Request_Review__c>();
        lstCountryTeamUsers = [Select Id,User__c,User__r.Name, Type__c From Change_Request_Review__c Where Change_Request__c =: strCRRID and Type__c = 'Country Team' Order By User__r.Name];
        System.Debug('@@@lstCountryTeamUsers: ' + lstCountryTeamUsers);
        String strUserId = '';
        if(lstCountryTeamUsers.size() > 0){
            for(Change_Request_Review__c objCrr : lstCountryTeamUsers){
                if(objCrr.User__c != null){
                    setApprovalProcessIDs.add(objCrr.Id);
                    mapUserType.put(objCrr.Id, objCrr.Type__c);
                }
                
                if(strCountryTeamUserNames == null || strCountryTeamUserNames == ''){
                    strCountryTeamUserNames = objCrr.User__r.Name;
                }else{
                    strCountryTeamUserNames += ', ' + objCrr.User__r.Name;
                }
            }
            System.Debug('@@@strCountryTeamUserNames: ' + strCountryTeamUserNames);
        }
    }
    
    Public void FillPSMUsers(){
        strPSMUserNames = '';
        List<Change_Request_Review__c> lstPSMUsers = new List<Change_Request_Review__c>();
        lstPSMUsers = [Select Id,User__c,User__r.Name,Type__c From Change_Request_Review__c Where Change_Request__c =: strCRRID and Type__c = 'PSM' Order By User__r.Name];
        if(lstPSMUsers.size() > 0){
            for(Change_Request_Review__c objCrr : lstPSMUsers){
                if(objCrr.User__c != null){
                    setApprovalProcessIDs.add(objCrr.Id);
                    mapUserType.put(objCrr.Id, objCrr.Type__c);
                }
                
                if(strPSMUserNames == null || strPSMUserNames == ''){
                    strPSMUserNames = objCrr.User__r.Name;
                }else{
                    strPSMUserNames += ', ' + objCrr.User__r.Name;
                }
            }
            System.Debug('@@@strPSMUserNames: ' + strPSMUserNames);
        }
    }
    
    Public void FillFinanceUsers(){
        strFinanceUserNames = '';
        List<Change_Request_Review__c> lstFinanceUsers = new List<Change_Request_Review__c>();
        lstFinanceUsers = [Select Id,User__c,User__r.Name,Type__c From Change_Request_Review__c Where Change_Request__c =: strCRRID and Type__c = 'Finance' Order By User__r.Name];
        if(lstFinanceUsers.size() > 0){
            for(Change_Request_Review__c objCrr : lstFinanceUsers){
                if(objCrr.User__c != null){
                    setApprovalProcessIDs.add(objCrr.Id);
                    mapUserType.put(objCrr.Id, objCrr.Type__c);
                }
                
                if(strFinanceUserNames == null || strFinanceUserNames == ''){
                    strFinanceUserNames = objCrr.User__r.Name;
                }else{
                    strFinanceUserNames += ', ' + objCrr.User__r.Name;
                }
            }
            System.Debug('@@@strFinanceUserNames: ' + strFinanceUserNames);
        }
    }
    
    Public void FillMandEUsers(){
        strMandEUserNames = '';
        List<Change_Request_Review__c> lstMandEUsers = new List<Change_Request_Review__c>();
        lstMandEUsers  = [Select Id,User__c,User__r.Name,Type__c From Change_Request_Review__c Where Change_Request__c =: strCRRID and Type__c = 'M&E' Order By User__r.Name];
        if(lstMandEUsers.size() > 0){
            for(Change_Request_Review__c objCrr : lstMandEUsers){
                if(objCrr.User__c != null){
                    setApprovalProcessIDs.add(objCrr.Id);
                    mapUserType.put(objCrr.Id, objCrr.Type__c);
                }
                
                if(strMandEUserNames == null || strMandEUserNames == ''){
                    strMandEUserNames = objCrr.User__r.Name;
                }else{
                    strMandEUserNames += ', ' + objCrr.User__r.Name;
                }
            }
            System.Debug('@@@strMandEUserNames: ' + strMandEUserNames);
        }
    }
    Public void FillPurchasingUsers(){
        strPurchasingUserNames = '';
        List<Change_Request_Review__c> lstPurchasingUsers = new List<Change_Request_Review__c>();
        lstPurchasingUsers = [Select Id,User__c,User__r.Name,Type__c From Change_Request_Review__c Where Change_Request__c =: strCRRID and Type__c = 'Purchasing' Order By User__r.Name];
        if(lstPurchasingUsers.size() > 0){
            for(Change_Request_Review__c objCrr : lstPurchasingUsers){
                if(objCrr.User__c != null){
                    setApprovalProcessIDs.add(objCrr.Id);
                    mapUserType.put(objCrr.Id, objCrr.Type__c);
                    System.debug('$$$##$$$mapUserType: '+mapUserType);
                }
                
                if(strPurchasingUserNames == null || strPurchasingUserNames == ''){
                    strPurchasingUserNames = objCrr.User__r.Name;
                }else{
                    strPurchasingUserNames += ', ' + objCrr.User__r.Name;
                }
            }
            System.Debug('@@@strPurchasingUserNames: ' + strPurchasingUserNames);
        }
    }
    
    /********************************************************************
       Name: ShowPopup
       Purpose: Used for open Add Contact Popup.
   
    ********************************************************************/
    
    public void ShowPopup()
    {
        strPopupName = Apexpages.currentpage().getparameters().get('Name');
        system.debug('@@@strPopupName: '+strPopupName);
        
        if(strPopupName == 'Country Team'){
            lstCRRUsers = new List<Change_Request_Review__c>();
            lstCRRUsers = [Select Id,User__c,User__r.Name From Change_Request_Review__c Where Change_Request__c =: strCRRID and Type__c = 'Country Team' Order By User__r.Name NULLS LAST LIMIT 6];
           
        }
        if(strPopupName == 'PSM'){
            lstCRRUsers = new List<Change_Request_Review__c>();
            lstCRRUsers = [Select Id,User__c,User__r.Name From Change_Request_Review__c Where Change_Request__c =: strCRRID and Type__c = 'PSM' Order By User__r.Name NULLS LAST LIMIT 6];
            
        }
        if(strPopupName == 'Finance'){
            lstCRRUsers = new List<Change_Request_Review__c>();
            lstCRRUsers = [Select Id,User__c,User__r.Name From Change_Request_Review__c Where Change_Request__c =: strCRRID and Type__c = 'Finance' Order By User__r.Name NULLS LAST LIMIT 6];
            
        }
        if(strPopupName == 'M&E'){
            lstCRRUsers = new List<Change_Request_Review__c>();
            lstCRRUsers = [Select Id,User__c,User__r.Name From Change_Request_Review__c Where Change_Request__c =: strCRRID and Type__c = 'M&E' Order By User__r.Name NULLS LAST LIMIT 6];
            
        }
        if(strPopupName == 'Purchasing'){
            lstCRRUsers = new List<Change_Request_Review__c>();
            lstCRRUsers = [Select Id,User__c,User__r.Name From Change_Request_Review__c Where Change_Request__c =: strCRRID and Type__c = 'Purchasing' Order By User__r.Name NULLS LAST LIMIT 6];
            
        }
        while(lstCRRUsers.size() < 6){
            objCRR = new Change_Request_Review__c();
            objCRR.Change_Request__c = strCRRID;
            objCRR.Type__c = strPopupName;
            objCRR.LFA_Coordination_Comments__c = strLFACoordinationComments;
            lstCRRUsers.add(objCRR);
        }
        blnHasChild = true;
    }
    
    /********************************************************************
       Name: HidePopup
       Purpose: Used for close Add Contact Popup.
                Called on 'Cancel' & 'Save & Submit' buttons in Add Contact Popup.
   
    ********************************************************************/
    
    public void HidePopup()
    {
        lstCRRUsers = new List<Change_Request_Review__c>();
        blnHasChild = false;
    }
    
    Public PageReference CancelCRR(){
        strLFACoordinationComments = '';
        PageReference pRef = new PageReference('/'+strCRRID);
        return pRef;
    }
    
    Public PageReference SaveCRR(){
        UpdateCRRCoordinationComments();
        strLFACoordinationComments = '';
        PageReference pRef = new PageReference('/'+strCRRID);
        return pRef;
    }
    
    Public PageReference SaveAndSend(){
        UpdateCRRCoordinationComments();
        system.debug('@@@setApprovalProcessIDs: '+setApprovalProcessIDs);
        //SubmitApprovalProcess(setApprovalProcessIDs);
        List<ProcessInstance> lstPIChangeRequestReview = [Select ID, Status, TargetObjectID From ProcessInstance Where TargetObjectID IN :setApprovalProcessIDs];
        system.debug('$$##$$lstPIChangeRequestReview: ' + lstPIChangeRequestReview);
        if(lstPIChangeRequestReview != null && lstPIChangeRequestReview.size() > 0){
            for(ProcessInstance objPI :lstPIChangeRequestReview){
                if(setApprovalProcessIDs.contains(objPI.TargetObjectID)){
                    setApprovalProcessIDs.remove(objPI.TargetObjectID);
                }
            }
        }
        system.debug('$$##$$setApprovalProcessIDs: ' + setApprovalProcessIDs);
        if(setApprovalProcessIDs != null && setApprovalProcessIDs.Size() > 0) {
            List<Approval.ProcessSubmitRequest> lstCRRApprovalProcess = new List<Approval.ProcessSubmitRequest>();
    
            for(Id idCRR : setApprovalProcessIDs) {
                Approval.ProcessSubmitRequest UserApprovalRequest = New Approval.ProcessSubmitRequest();
                UserApprovalRequest.setObjectId(idCRR);
                lstCRRApprovalProcess.Add(UserApprovalRequest);
            }
            System.Debug('@@@lstCRRApprovalProcess: '+lstCRRApprovalProcess);
            List<Approval.ProcessResult> lstApprovalResult = new List<Approval.ProcessResult>();
            
            lstApprovalResult = Approval.Process(lstCRRApprovalProcess);
            System.Debug('@@@lstApprovalResult: '+lstApprovalResult);
        }
        if(setApprovalProcessIDs != null && setApprovalProcessIDs.size() > 0){
            List<CR_Reminder__c> lstCRReminder = new List<CR_Reminder__c>();
            set<String> setType = new set<String>();
            for(Id obj :setApprovalProcessIDs){
                if(setType.contains(mapUserType.get(obj)) == false){
                    CR_Reminder__c objCRReminder = new CR_Reminder__c();
                    objCRReminder.Change_Request__c = strCRRID;
                    objCRReminder.Date__c = Date.Today();
                    System.debug('$$$###$$$mapUserType.get(obj): '+mapUserType.get(obj));
                    objCRReminder.Type__c = mapUserType.get(obj);
                    setType.add(objCRReminder.Type__c);
                    lstCRReminder.add(objCRReminder);
                }
            }
            insert lstCRReminder;
        }
        PageReference pRef = new PageReference('/'+strCRRID);
        return pRef;
    }
    
    Public void SaveUsers(){
        try{
            List<Change_Request_Review__c> lstInsertCRR = new List<Change_Request_Review__c>();
            List<Change_Request_Review__c> lstDeleteCRR = new List<Change_Request_Review__c>();
            List<Change_Request_Review__c> lstUpdateCRR = new List<Change_Request_Review__c>();
            if(lstCRRUsers.size() > 0){
                for(Change_Request_Review__c obj : lstCRRUsers){
                    if(obj.Id == null && String.isblank(obj.User__c) == false){
                        //obj.LFA_Coordination_Comments__c = strLFACoordinationComments;
                        lstInsertCRR.add(obj);
                    }
                    
                    if(obj.Id != null && String.isblank(obj.User__c)){
                        lstDeleteCRR.add(obj);
                    }
                    
                    if(obj.Id != null && String.isblank(obj.User__c) == false){
                        //obj.LFA_Coordination_Comments__c = strLFACoordinationComments;
                        lstUpdateCRR.add(obj);
                    }
                }
                system.debug('@@@lstInsertCRR: '+lstInsertCRR);
                if(lstInsertCRR.size() > 0) insert lstInsertCRR;
                if(lstDeleteCRR.size() > 0) delete lstDeleteCRR;
                if(lstUpdateCRR.size() > 0) update lstUpdateCRR;
            }
            
            if(strPopupName == 'Country Team'){
                FillCountryTeamUsers();
            }else if(strPopupName == 'PSM'){
                FillPSMUsers();
            }else if(strPopupName  == 'Finance'){
                FillFinanceUsers();
            }else if(strPopupName  == 'M&E'){
                FillMandEUsers();
            }else if(strPopupName  == 'Purchasing'){
                FillPurchasingUsers();
            }
            
            HidePopup();
        }catch (Exception ex) {
           ApexPages.AddMessage(new ApexPages.message(ApexPages.Severity.Error, String.ValueOf(ex)));
        }
    }
    
    Public Void UpdateCRRCoordinationComments(){
        List<Change_Request_Review__c> lstToUpdateCRR = [Select Id,Name,LFA_Coordination_Comments__c From Change_Request_Review__c Where Change_Request__c =: strCRRID];
        System.debug('###$$$###lstToUpdateCRR' + lstToUpdateCRR);
        if(lstToUpdateCRR != null && lstToUpdateCRR.size() > 0){
            List<Change_Request_Review__c> lstToUpdate = new List<Change_Request_Review__c>();
            for(Change_Request_Review__c objCRR :lstToUpdateCRR){
                objCRR.LFA_Coordination_Comments__c = strLFACoordinationComments;
                lstToUpdate.add(objCRR);
            }
            update lstToUpdate;
        }
    }
    
    Public void SendReminder(){
        strSendRemName = Apexpages.currentpage().getparameters().get('PName');
        Map<ID,Change_Request_Review__c> mapChngReqReview;
        mapChngReqReview = new Map<ID,Change_Request_Review__c>([ select ID, User__c,country__c,lfa_coordination_comments__c from Change_Request_Review__c Where Type__c =: strSendRemName and Change_Request__c =: strCRRID and Status__c = null ]);
        
        Set<Id> setUser = new Set<Id>();
        /*System.debug('$$##$$lstChngReqReview: ' + mapChngReqReview);
        for(Change_Request_Review__c objCRR : mapChngReqReview.values()){
            setUser.add(objCRR.User__c);
        }*/
        List<Messaging.SingleEmailMessage> lstMsgs = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage msg;
        List<EmailTemplate> lstEmailTemplate = [select id from EmailTemplate where DeveloperName='Change_Request_Review_Approval_Reminders'];
        List<ProcessInstance> lstPICRRIDs = [Select ID, Status, TargetObjectID  From ProcessInstance Where TargetObjectID IN :mapChngReqReview.keyset() AND Status = 'Pending'];
        if(lstPICRRIDs != null && lstPICRRIDs.size() > 0){
            for(ProcessInstance objPI :lstPICRRIDs){
                if(mapChngReqReview.containskey(objPI.TargetObjectID)){
                    //setUser.add(mapChngReqReview.get(objPI.TargetObjectID).User__c);
                    if(mapChngReqReview.get(objPI.TargetObjectID) != null && mapChngReqReview.get(objPI.TargetObjectID).User__c != null){
                        msg = new Messaging.SingleEmailMessage();
                        msg.setTemplateId(lstEmailTemplate[0].id );
                        msg.setWhatId(objPI.TargetObjectID);
                        system.debug('set:'+msg);
                        msg.setTargetObjectId(mapChngReqReview.get(objPI.TargetObjectID).User__c);                       
                        msg.setSaveAsActivity(false);
                        lstMsgs.add(msg);
                        System.debug('@@@lstMsgs: '+lstMsgs);
                    }
                }
            }
        }
        if(lstMsgs.size()>0) Messaging.sendEmail(lstMsgs);
        
        CR_Reminder__c objCRReminder = new CR_Reminder__c();
        objCRReminder.Change_Request__c = strCRRID;
        objCRReminder.Date__c = Date.Today();
        objCRReminder.Type__c = strSendRemName;
        insert objCRReminder;
         
        /*List<Messaging.SingleEmailMessage> lstMsgs = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage msg;
        for(ID objset : setUser){
            msg = new Messaging.SingleEmailMessage();
            msg.setTemplateId( [select id from EmailTemplate where DeveloperName='Change_Request_Review_Approval_Reminders'].id );
           //Change_Request_Review_Approval_Reminders, Change_Request_Review_Approval_Reminder
            msg.setWhatId(strCRRID);
            system.debug('set:'+msg);
            msg.setTargetObjectId(objset);
            lstMsgs.add(msg);
            msg.setSaveAsActivity(false);
        }
        
        system.debug('lstmsg :'+lstMsgs);
        //msg.setTargetObjectId(UserInfo.getUserId());
        Messaging.sendEmail(lstMsgs);*/
        
    }
    Public void LastRemDate(){
        List<CR_Reminder__c> lstCRem = [ select Date__c, Type__c from CR_Reminder__c where Change_Request__c =: strCRRID ];
        
        for(CR_Reminder__c objCRem : lstCRem ){
            
                if(objCRem.Type__c == 'Country Team' ){
                    if(dateofLastRemCT == null) dateofLastRemCT = string.valueof(objCRem.Date__c);
                    else dateofLastRemCT += ', ' + string.valueof(objCRem.Date__c);
                    
                }else if(objCRem.Type__c == 'PSM'){
                    if(dateofLastRemPSM  == null) dateofLastRemPSM  = string.valueof(objCRem.Date__c);
                    else dateofLastRemPSM  += ', ' + string.valueof(objCRem.Date__c);
                   
                }else if(objCRem.Type__c == 'Finance'){
                    if(dateofLastRemFNC  == null) dateofLastRemFNC  = string.valueof(objCRem.Date__c);
                    else dateofLastRemFNC  += ', ' + string.valueof(objCRem.Date__c);
                 
                }else if(objCRem.Type__c == 'M&E'){
                    if(dateofLastRemME  == null) dateofLastRemME  = string.valueof(objCRem.Date__c);
                    else dateofLastRemME  += ', ' + string.valueof(objCRem.Date__c);
                    
                }else if(objCRem.Type__c == 'Purchasing'){
                    if(dateofLastRemPUR == null) dateofLastRemPUR = string.valueof(objCRem.Date__c);
                    else dateofLastRemPUR += ', ' + string.valueof(objCRem.Date__c);
                   
                }
          
        }
    }
   
}