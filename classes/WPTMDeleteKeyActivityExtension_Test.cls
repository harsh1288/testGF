@IsTest
public class WPTMDeleteKeyActivityExtension_Test{
     static testmethod void testmethod_A(){ 
        Account tAcc=TestClassHelper.createAccount();
        insert tAcc;

        Grant__c tGrant=TestClasshelper.createGrant(tAcc);
        insert tGrant;

        Implementation_Period__c tImpPeriod=TestClassHelper.createIP(tGrant,tAcc);
        insert tImpPeriod;

        Performance_Framework__c tPF = new Performance_Framework__c ();
        tPF.Implementation_Period__c = tImpPeriod.id; 
        insert tPF;

        Grant_Intervention__c tGi=TestClassHelper.createGrantIntervention(tImpPeriod);
        tGi.Performance_Framework__c = tPF.id;
        insert tGi;     

        Key_Activity__c tKact = new Key_Activity__c();
        tKact.Activity_Description__c = 'Test';
        tKact.Grant_Intervention__c= tGi.id;        
        insert tKact;
        
        test.startTest();
        Test.setCurrentPage(Page.WPTMDeleteKeyActivity);
        //cover catch block
        apexpages.currentpage().getparameters().put('id',NULL); 
        WPTMDeleteKeyActivityExtension obj = new WPTMDeleteKeyActivityExtension();
        obj.deleteKeyActivity(); 
        //cover try block
        apexpages.currentpage().getparameters().put('id',tKact.id); 
        obj = new WPTMDeleteKeyActivityExtension();
        obj.deleteKeyActivity();        
     }
}