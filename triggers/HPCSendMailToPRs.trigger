trigger HPCSendMailToPRs on HPC_Framework__c (after update) {

/*********************************************************************************
* Trigger: HPCSendMailToPRs
  Test class:   
*  DateCreated : 10/11/2014
----------------------------------------------------------------------------------
* Purpose/Methods:
* - To add PR Admin and PR Read Write Edit Contacts to send Email
----------------------------------------------------------------------------------
* History:
* - VERSION  DEVELOPER NAME    DATE            DETAIL FEATURES
     1.0        TCS           28/10/2014      INITIAL DEVELOPMENT
*********************************************************************************/
    Set<Id> accIds = new Set<Id>();
    Set<Id> contactIds = new Set<Id>();
    Set<Id> ipIds = new Set<Id>();
    Set<Id> ipDetailIds = new Set<Id>();
    
    List<HPC_Framework__c> lstIPDetail = new List<HPC_Framework__c>(); 
    List<Account> lstAccount = new List<Account>();
    List<Implementation_Period__c> lstIP = new List<Implementation_Period__c>();
    List<String> lstEMailAdd = new List<String>(); 
    List<Id> lstConIds = new List<Id>(); 
    List<User> lstPRUsers = new List<User>();

    for(HPC_Framework__c ipDetail: Trigger.new){
      if(Trigger.isInsert){
          ipDetailIds.add(ipDetail.Id);
      }
      if(Trigger.isUpdate && (ipDetail.HPC_Status__c != System.Trigger.oldMap.get(ipDetail.Id).HPC_Status__c && (ipDetail.HPC_Status__c == Label.IP_Return_back_to_PR))){
          ipDetailIds.add(ipDetail.Id);
      }
      if(Trigger.isUpdate && (ipDetail.HPC_Status__c != System.Trigger.oldMap.get(ipDetail.Id).HPC_Status__c && (ipDetail.HPC_Status__c == Label.IP_Sub_to_GF) && System.Trigger.oldMap.get(ipDetail.Id).HPC_Status__c != Label.HPC_Accepted)){
          ipDetailIds.add(ipDetail.Id);
      }
      if(Trigger.isUpdate && (ipDetail.HPC_Status__c != System.Trigger.oldMap.get(ipDetail.Id).HPC_Status__c && (ipDetail.HPC_Status__c == Label.HPC_Accepted))){
          ipDetailIds.add(ipDetail.Id);
      }
    }

    /*Query for Implementation Period and email fields of the HPC Framework*/
    lstIPDetail = [SELECT Id,HPC_Status__c,Country_Team_Email__c, HPC_Specialist_Email__c,FO_Email__c,Grant_Implementation_Period__c, Grant_Implementation_Period__r.Principal_Recipient__c FROM HPC_Framework__c WHERE Id=:ipDetailIds];
    
    
    for(HPC_Framework__c ipDInfo: lstIPDetail){
        if(ipDInfo.Grant_Implementation_Period__c!=null)  
            ipIds.add(ipDInfo.Grant_Implementation_Period__c);
        if(ipDInfo.Grant_Implementation_Period__r.Principal_Recipient__c!=null)
            accIds.add(ipDInfo.Grant_Implementation_Period__r.Principal_Recipient__c);
    }

if(accIds!=null){

/*Query for all Contacts associated with the Principal Recipients*/
    List<Account> lstAccounts = [SELECT Id, (SELECT Id from Contacts) FROM Account WHERE Id=:accIds];
    List<npe5__Affiliation__c> lstAff = [Select id,npe5__Contact__c,npe5__Organization__c FROM npe5__Affiliation__c where npe5__Organization__c IN:lstAccounts  and Access_Level__c !='Read' and npe5__Status__c='Current'];
    for(npe5__Affiliation__c aff : lstAff ){
            //contactIds.add();
            lstConIds.add(aff.npe5__Contact__c);
    }
    //System.debug('lstAccounts **'+lstAccounts );
    System.debug('lstConIds**'+lstConIds);
}

/*Query for Email of Users where Profile name is PR Admin or PR Read Write Edit*/
 
 
  if(lstConIds.size()>0){
       lstPRUsers = [SELECT ID, Email, ContactId, Contact.Firstname, Contact.Lastname, ProfileId, Profile.Name FROM User WHERE ContactID in: lstConIds and Profile.Name in ('PR Read Write Edit', 'PR Admin') and isActive=: True];
        // Remove PR Read Only
 system.debug('lstPRUsers '+lstPRUsers);
   
   system.debug('lstPRUsers '+lstPRUsers );
    for(User objUser: lstPRUsers){
        if(objUser.Email!=null)
            lstEMailAdd.add(objUser.Email);
            System.debug('lstEMailAdd'+lstEMailAdd);
    }

    String[] toAddresses = new String[] {};
    string[] ccAddress = new String[] {};
    string[] SBToAddress = new string[] {};
    string[] HPMToAddress = new string[] {};
    
    
    for(String email: lstEMailAdd){ 
        toAddresses.add(email);
    }
 
      
  if(lstPRUsers.size()>0){
    for(HPC_Framework__c ipDet: lstIPDetail){
   /** Commenting for Sprint 12 US **/
   /* if(ipDet.HPC_Status__c == Label.IP_Return_back_to_PR) {  // Email 1 sent mail to PR when status is sent back to PR
          Messaging.SingleEmailMessage mailToPRs = new Messaging.SingleEmailMessage();
          mailToPRs.setTargetObjectId(lstPRUsers[0].ContactId);
          mailToPRs.setToAddresses(toAddresses);
          //mailToPRs.setccAddresses(ccAddress);
          mailToPRs.setUseSignature(false);
          mailToPRs.setBccSender(false);
          mailToPRs.setSaveAsActivity(false);
          mailToPRs.setOrgWideEmailAddressId(Label.OrgWideAddrss_GMP);
          mailToPRs.setWhatid(ipDet.id);
          EmailTemplate et= [Select id from EmailTemplate where DeveloperName=:'HPM_Specialist_Sent_Back_HPC_to_PR'];
          mailToPRs.setTemplateId(et.id);
          system.debug('To Addess1'+toAddresses);
         Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mailToPRs});   
    }
    if(ipDet.HPC_Status__c == Label.IP_Sub_to_GF) {// Mail to PR
          Messaging.SingleEmailMessage mailToPRs = new Messaging.SingleEmailMessage();
          mailToPRs.setTargetObjectId(lstPRUsers[0].ContactId);
          mailToPRs.setWhatId(ipDet.Id);
          mailToPRs.setToAddresses(toAddresses);
          mailToPRs.setUseSignature(false);
          mailToPRs.setBccSender(false);
          mailToPRs.setSaveAsActivity(false);
          mailToPRs.setWhatid(ipDet.id);
          mailToPRs.setOrgWideEmailAddressId(Label.OrgWideAddrss_GMP);

          EmailTemplate et= [Select id from EmailTemplate where DeveloperName=:'Template_for_Alert_to_PR_on_HPC_Submit'];
          mailToPRs.setTemplateId(et.id);
          mailToPRs.setWhatId(ipDet.Id);
          system.debug('To Addess3'+toAddresses);
          Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mailToPRs});   
    } */
    if(ipDet.HPC_Status__c == Label.HPC_Accepted) {// Mail to PR
          Messaging.SingleEmailMessage mailToPRs = new Messaging.SingleEmailMessage();
          mailToPRs.setTargetObjectId(lstPRUsers[0].ContactId);
          mailToPRs.setWhatId(ipDet.Id);
          mailToPRs.setToAddresses(toAddresses);
          mailToPRs.setUseSignature(false);
          mailToPRs.setBccSender(false);
          mailToPRs.setSaveAsActivity(false);
          mailToPRs.setWhatid(ipDet.id);
          mailToPRs.setOrgWideEmailAddressId(Label.OrgWideAddrss_GMP);

         // EmailTemplate et= [Select id from EmailTemplate where DeveloperName=:'Email_To_PR_HPM_Specialist_Accepted_HPC'];
          EmailTemplate et= [Select id from EmailTemplate where DeveloperName=:'Email_to_PR_user_on_Acceptance_of_HPC'];
          mailToPRs.setTemplateId(et.id);
          mailToPRs.setWhatId(ipDet.Id);
          system.debug('To Addess3'+toAddresses);
          Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mailToPRs});   
    }
   }
   }
  }
  
}