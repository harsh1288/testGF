/*********************************************************************************
* Trigger: UpdateAccountFinanceOfficer
* Created by {DeveloperName}, {DateCreated 12/10/2013}
----------------------------------------------------------------------------------
* Purpose:
* - This Trigger is used to set Finance officer of Bank Account.
----------------------------------------------------------------------------------
*********************************************************************************/

trigger UpdateBankAccountFinanceOfficer on Bank_Account__c (After Insert,After Update) {    
    UpdateBankAccountFinanceOfficerHandler.aiuSetBankAccountFinanceOfficer(Trigger.New,Trigger.OldMap,Trigger.IsInsert,Trigger.IsUpdate);
}