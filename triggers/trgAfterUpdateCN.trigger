/************************************
The primary purpose of this trigger is to update the currency 
of CPF Reports and Funding Sources if the CurrencyIsoCode of 
the concept note is changed.

Added 2014-6-19 by Matthew Miller
************************************/

trigger trgAfterUpdateCN on Concept_Note__c (after update) {
    
    Set<Id> setCNIds = new Set<Id>();

    for(Concept_Note__c CN : Trigger.new){
        if(CN.CurrencyIsoCode != Trigger.oldMap.get(CN.Id).CurrencyIsoCode){
          setCNIds.add(CN.Id);
        }
    }
    
    List<CPF_Report__c> lstCPF = [Select Id, CurrencyIsoCode, Concept_Note__r.CurrencyIsoCode,
                               (Select Id, CurrencyIsoCode from Funding_Sources__r)
                               from CPF_Report__c where Concept_Note__c in : setCNIds];
    
    List<Funding_Source__c> lstFundingSourceToUpdate = new List<Funding_Source__c>();
    
    for(CPF_Report__c CPF : lstCPF){
        CPF.Currency__c = CPF.Concept_Note__r.CurrencyIsoCode;
        CPF.CurrencyIsoCode = CPF.Concept_Note__r.CurrencyIsoCode;
        
        for(Funding_Source__c FS : CPF.Funding_Sources__r){
            FS.CurrencyIsoCode = CPF.Concept_Note__r.CurrencyIsoCode;
            lstFundingSourceToUpdate.add(FS);
        }   
    }
    
    update lstCPF;
    update lstFundingSourceToUpdate;   
}