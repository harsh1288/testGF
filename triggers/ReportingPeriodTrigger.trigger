trigger ReportingPeriodTrigger on Period__c (before update, before delete, after insert, after update) {
        
        if(Trigger.isBefore){
            if (Trigger.isDelete){
                List<sObject> sObjectList = new List<sObject>();
                for( Period__c per : Trigger.old)
                    sObjectList.add((sObject)per );
                PFChangesTrackerHandler.CheckPFField(sObjectList);
            }
         
            
             if ( Trigger.isInsert )
            {  
                List<sObject> sObjectList = new List<sObject>();
                for( Period__c per : Trigger.new ){
                 if(Trigger.oldMap.get(per.Id).RecordTypeId == per.RecordTypeId)
                    sObjectList.add((sObject)per );
                }    
                PFChangesTrackerHandler.CheckPFField(sObjectList);
           
            }
            
            if(trigger.isUpdate) 
            {    
                //Method Call up to update Milestone/Target concatinated fields on delete of Reporting Period Object.
                System.debug('^^1.TriggerIn');
                ReportingPeriodTriggerHandler.UpdateMileStoneReportingPeriod(Trigger.new);
                List<sObject> sObjectList = new List<sObject>();
                for( Period__c per : Trigger.new ){
                 if(Trigger.oldMap.get(per.Id).RecordTypeId == per.RecordTypeId)
                    sObjectList.add((sObject)per );
                }    
                PFChangesTrackerHandler.CheckPFField(sObjectList);
            }
        }
}