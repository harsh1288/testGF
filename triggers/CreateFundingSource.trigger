trigger CreateFundingSource on CPF_Report__c (after insert) {
    Id RecordTypeNSF = [Select Id From RecordType Where sobjectType = 'Funding_Source__c' And RecordType.Developername = 'National_Strategic_Funding'].Id;
    Id RecordTypeB = [Select Id From RecordType Where sobjectType = 'Funding_Source__c' And RecordType.Developername = 'B'].Id;
    Id RecordTypeC = [Select Id From RecordType Where sobjectType = 'Funding_Source__c' And RecordType.Developername = 'C'].Id;
    Id RecordTypeD = [Select Id From RecordType Where sobjectType = 'Funding_Source__c' And RecordType.Developername = 'D'].Id;
    Id RecordTypeI = [Select Id From RecordType Where sobjectType = 'Funding_Source__c' And RecordType.Developername = 'I'].Id;
    Id RecordTypeJ = [Select Id From RecordType Where sobjectType = 'Funding_Source__c' And RecordType.Developername = 'J'].Id;
    
    Set<Id> setCountry = new Set<Id>();
    Set<Id> setCPFReportId = new Set<Id>();
    Set<Id> setCountryForGIP = new Set<Id>();
    String strCNId;
    Set<String> setComponent = new Set<String>();
    if(Trigger.New.size() > 0){
        for(CPF_Report__c objNew : Trigger.New){
            if(objNew.Country__c != null){
                setCountryForGIP.add(objNew.Country__c);
                setCPFReportId.add(objNew.id);
                strCNId = objNew.Concept_note__c;
            }
            if(objNew.Country__c != null && objNew.Component__c != null){
                setCountry.add(objNew.Country__c);
                setComponent.add(objNew.Component__c);
                
            }
        }
    }
    
    Map<String,List<Implementation_Period__c>> mapFilterToIpList = new Map<String,List<Implementation_Period__c>>();
    List<Implementation_Period__c> lstIP = [Select Id,Grant_Number_s__c,Principal_Recipient__r.Country__c,Component__c,Grant__c,Grant__r.Name From Implementation_Period__c Where  Is_Active__c = true And Principal_Recipient__r.Country__c IN : setCountry And Component__c IN : setComponent];
    system.debug('@#@#@#@'+lstIP);
    for(Implementation_Period__c obj : lstIP){
        String strFilter = obj.Principal_Recipient__r.Country__c + '|' + obj.Component__c;
        if(mapFilterToIpList.ContainsKey(strFilter)){
            mapFilterToIpList.get(strFilter).add(obj);
        }else{
            mapFilterToIpList.put(strFilter,New List<Implementation_Period__c>{obj});
        }
    }
    system.debug('@#@#@#@'+mapFilterToIpList);
    
    List<Funding_Source__c> lstFundingSourceToInsert = new List<Funding_Source__c>();
    Funding_Source__c objFundingSourceNew;
    if(Trigger.New.size() > 0){
        for(CPF_Report__c objNew : Trigger.New){
            objFundingSourceNew = new Funding_Source__c(RecordTypeId = RecordTypeNSF,CPF_Report__c=objNew.Id, CurrencyIsoCode = objNew.CurrencyIsoCode);
            lstFundingSourceToInsert.add(objFundingSourceNew);
            
            objFundingSourceNew = new Funding_Source__c(Type__c = '1',RecordTypeId = RecordTypeB,CPF_Report__c=objNew.Id, CurrencyIsoCode = objNew.CurrencyIsoCode);
            lstFundingSourceToInsert.add(objFundingSourceNew);
            objFundingSourceNew = new Funding_Source__c(Type__c = '2',RecordTypeId = RecordTypeB,CPF_Report__c=objNew.Id, CurrencyIsoCode = objNew.CurrencyIsoCode);
            lstFundingSourceToInsert.add(objFundingSourceNew);
            objFundingSourceNew = new Funding_Source__c(Type__c = '3',RecordTypeId = RecordTypeB,CPF_Report__c=objNew.Id, CurrencyIsoCode = objNew.CurrencyIsoCode);
            lstFundingSourceToInsert.add(objFundingSourceNew);
            objFundingSourceNew = new Funding_Source__c(Type__c = '4',RecordTypeId = RecordTypeB,CPF_Report__c=objNew.Id, CurrencyIsoCode = objNew.CurrencyIsoCode);
            lstFundingSourceToInsert.add(objFundingSourceNew);
            objFundingSourceNew = new Funding_Source__c(Type__c = '5',RecordTypeId = RecordTypeB,CPF_Report__c=objNew.Id, CurrencyIsoCode = objNew.CurrencyIsoCode);
            lstFundingSourceToInsert.add(objFundingSourceNew);
            
            objFundingSourceNew = new Funding_Source__c(Type__c = '1',RecordTypeId = RecordTypeC,CPF_Report__c=objNew.Id, CurrencyIsoCode = objNew.CurrencyIsoCode);
            lstFundingSourceToInsert.add(objFundingSourceNew);
            objFundingSourceNew = new Funding_Source__c(Type__c = '2',RecordTypeId = RecordTypeC,CPF_Report__c=objNew.Id, CurrencyIsoCode = objNew.CurrencyIsoCode);
            lstFundingSourceToInsert.add(objFundingSourceNew);
            objFundingSourceNew = new Funding_Source__c(Type__c = '3',RecordTypeId = RecordTypeC,CPF_Report__c=objNew.Id, CurrencyIsoCode = objNew.CurrencyIsoCode);
            lstFundingSourceToInsert.add(objFundingSourceNew);
            
             String strFilterNew;
            if(objNew.Country__c != null && objNew.Component__c != null) strFilterNew = objNew.Country__c + '|' + objNew.Component__c;
            if(mapFilterToIpList.get(strFilterNew) != null && mapFilterToIpList.get(strFilterNew).size() > 0){
                for(Integer i=0;i<mapFilterToIpList.get(strFilterNew).size();i++){
                    objFundingSourceNew = new Funding_Source__c(Type__c = String.valueof(i+1),RecordTypeId = RecordTypeD,CPF_Report__c=objNew.Id, CurrencyIsoCode = objNew.CurrencyIsoCode);
                    if(mapFilterToIpList.get(strFilterNew)[i].Grant__c!=null){
                        objFundingSourceNew.Grant_Number__c = mapFilterToIpList.get(strFilterNew)[i].Grant__r.Name;
                    }
                    lstFundingSourceToInsert.add(objFundingSourceNew);
                }
            }
            objFundingSourceNew = new Funding_Source__c(Type__c = '1',RecordTypeId = RecordTypeI,CPF_Report__c=objNew.Id, CurrencyIsoCode = objNew.CurrencyIsoCode);
            lstFundingSourceToInsert.add(objFundingSourceNew);
            objFundingSourceNew = new Funding_Source__c(Type__c = '2',RecordTypeId = RecordTypeI,CPF_Report__c=objNew.Id, CurrencyIsoCode = objNew.CurrencyIsoCode);
            lstFundingSourceToInsert.add(objFundingSourceNew);
            objFundingSourceNew = new Funding_Source__c(Type__c = '1',RecordTypeId = RecordTypeJ,CPF_Report__c=objNew.Id, CurrencyIsoCode = objNew.CurrencyIsoCode);
            lstFundingSourceToInsert.add(objFundingSourceNew);
            objFundingSourceNew = new Funding_Source__c(Type__c = '2',RecordTypeId = RecordTypeJ,CPF_Report__c=objNew.Id, CurrencyIsoCode = objNew.CurrencyIsoCode);
            lstFundingSourceToInsert.add(objFundingSourceNew);
            objFundingSourceNew = new Funding_Source__c(Type__c = '3',RecordTypeId = RecordTypeJ,CPF_Report__c=objNew.Id, CurrencyIsoCode = objNew.CurrencyIsoCode);
            
            lstFundingSourceToInsert.add(objFundingSourceNew);
        }
        if(lstFundingSourceToInsert.size() > 0) Insert lstFundingSourceToInsert;
        
        if(setCountryForGIP.size() > 0){
             GIPFundingSource.CreateGIPFundingSourceRecords(setCountryForGIP,strCNId,setCPFReportId);
        }
    }
}