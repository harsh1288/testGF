trigger trgUpdateRate on Change_Request__c (after Insert, after update) {

    set<Id> rateId = new set<Id>();
    set<Id> reqId = new set<Id>();
    Set<Id> setChangeRequestIDs = new Set<Id>();
    
    for(Change_Request__c req : Trigger.new)
    {
        if(trigger.isUpdate){
            if((req.Status__c == 'Approved' || req.Status__c == 'Conditionally Approved' || req.Status__c == 'Rejected') && req.Status__c != System.Trigger.oldMap.get(req.Id).Status__c){
                reqId.add(req.Id);
                rateId.add(req.Rate__c);
            }
            if((req.Status__c == 'Approved' || req.Status__c == 'Conditionally Approved' || req.Status__c == 'Rejected' || req.Status__c == 'Cancelled') && System.Trigger.oldMap.get(req.Id).Status__c != 'Closed by LFA Coordination'){
                setChangeRequestIDs.add(req.Id);
            }
        }else{
            if(req.Status__c == 'Approved' || req.Status__c == 'Conditionally Approved' || req.Status__c == 'Rejected'){   
                reqId.add(req.Id);
                rateId.add(req.Rate__c);
            }
        }
    }
    if(reqId.size() > 0 && rateId.size() > 0){
        List<Rate__c> lstRate = [Select Id,Name,Active__c,GF_Approved__c,Rate__c,LFA_Location__c,Next_Year_Active__c,
                                    Next_Year_Daily_Rate__c,Next_Year_LFA_Location__c,Next_Year_GF_Approved__c,
                                    (Select Id,Name,Status__c,Active__c,Proposed_Status__c,Proposed_LFA_Location__c,
                                    Proposed_Daily_Rate__c,Proposed_Next_Year_LFA_Location__c,Proposed_Next_Year_Status__c,
                                    Proposed_Next_Year_Daily_Rate__c  from Change_Requests__r 
                                    where Id in :reqId order by createdDate)
                                from Rate__c where Id in :rateId];
        
        for(Rate__c rate : lstRate){
            for(Change_Request__c req : rate.Change_Requests__r){
                
                /*if(req.Status__c == 'Approved') rate.GF_Approved__c = 'Approved';   
                if(req.Status__c == 'Conditionally Approved') rate.GF_Approved__c = 'Conditionally Approved';
                if(req.Status__c == 'Rejected') rate.GF_Approved__c = 'Rejected';
                if(req.Status__c == 'Approved' || req.Status__c == 'Conditionally Approved'){
                    rate.Rate__c = req.Proposed_Daily_Rate__c;
                    rate.LFA_Location__c = req.Proposed_LFA_Location__c; 
                    if(req.Proposed_Status__c == 'Active') rate.Active__c = true;
                    if(req.Proposed_Status__c == 'Inactive') rate.Active__c = false;
                }*/
                
                if(req.Proposed_Daily_Rate__c > 0){
                    if(req.Status__c == 'Approved' || req.Status__c == 'Conditionally Approved'){
                        rate.Active__c = true;
                        rate.Next_Year_Active__c = true;
                        rate.LFA_Location__c = req.Proposed_LFA_Location__c;
                        rate.Next_Year_LFA_Location__c = req.Proposed_LFA_Location__c;
                        rate.GF_Approved__c = req.Status__c;
                        rate.Next_Year_GF_Approved__c = req.Status__c;
                        rate.Rate__c = req.Proposed_Daily_Rate__c;
                        rate.Next_Year_Daily_Rate__c = req.Proposed_Daily_Rate__c;
                    }
                    if(req.Status__c == 'Rejected'){
                        if(rate.GF_Approved__c == 'In-Progress' || rate.Next_Year_GF_Approved__c == 'In-Progress'){
                            rate.GF_Approved__c = req.Status__c;
                            rate.Next_Year_GF_Approved__c = req.Status__c;
                        }
                    }
                }
                
                if(req.Proposed_Next_Year_Daily_Rate__c > 0){
                    if(req.Status__c == 'Approved' || req.Status__c == 'Conditionally Approved'){
                        rate.Next_Year_Active__c = true;
                        rate.Next_Year_LFA_Location__c = req.Proposed_Next_Year_LFA_Location__c;
                        rate.Next_Year_GF_Approved__c = req.Status__c;
                        rate.Next_Year_Daily_Rate__c = req.Proposed_Next_Year_Daily_Rate__c;
                    }
                    if(req.Status__c == 'Rejected'){
                        if(rate.Next_Year_GF_Approved__c == 'In-Progress'){
                            rate.Next_Year_GF_Approved__c = req.Status__c;
                        }
                    }
                }
            }
        }
        if(!lstRate.isEmpty()) upsert lstRate;                  
    }
    
    // Update Change Request Object Status to 'Closed by LFA Coordination' then All it's Child ChangeRequestReview's Stauts update to 'LFA Coordination Closed'.
    
    
    List<Change_Request_Review__c> lstChangeReqestReview = new List<Change_Request_Review__c>();
    
    if(setChangeRequestIDs.size() > 0){
        lstChangeReqestReview = [Select ID,Name,Status__c From Change_Request_Review__c
        Where Change_Request__c IN: setChangeRequestIDs];    
    }              
    
    if(lstChangeReqestReview != null && lstChangeReqestReview.size() > 0){
        for(Change_Request_Review__c objCRR :lstChangeReqestReview){
            objCRR.Status__c = 'LFA Coordination Closed';
        }
        update lstChangeReqestReview;
    }
}