trigger UpdateFedIdOnUser on Contact (after Update) {
  
    List<User> UpdateUserList = new List<User>();
    List<id> ConIDList = new List<id>();
    List<User> Us = new List<User>();

    for(Contact con: trigger.new){
     
      If(Trigger.isUpdate){
        if(Con.Federation_ID__c != trigger.oldmap.get(con.id).Federation_ID__c){
            ConIDList.add(Con.id);
            }
        }
    }
    
    Us = [Select id,isActive,FederationIdentifier, ContactID, Contact.Federation_ID__c FROM User WHERE ContactID IN: ConIDList AND isActive = True AND User.Profile.UserLicense.Name='Partner Community Login' ];
   
    if(Us.size()>0){
        for(User U: Us){
           if(U.FederationIdentifier != U.Contact.Federation_ID__c){ 
           U.FederationIdentifier = U.Contact.Federation_ID__c;
           UpdateUserList.add(u);
           }
        }
       
        if(UpdateUserList.size()>0)
           update UpdateUserList;
     }       
}