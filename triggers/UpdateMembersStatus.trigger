trigger UpdateMembersStatus on iContactforSF__iContact_Sent_Message__c (after insert) {

    list<id> CampList = new List<id>();
    list<campaignMember> CMUpdateList = new List<campaignMember>();
    
    for(iContactforSF__iContact_Sent_Message__c iCon : trigger.new){
        camplist.add(iCon.iContactforSF__Campaign__c);
    }

    list<campaignMember> CampMember = [Select id,status,campaignid from campaignMember where campaignid IN: camplist];
    
    for(campaignMember CM: CampMember){
        CM.Status = 'Sent';
        CMUpdateList.add(CM);
    }
    
    if(CMUpdateList!=Null)
        update CMUpdateList;
}