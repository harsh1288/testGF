trigger updateOrgBAJuncStatus on Bank_Account__c (After Update) {
    set<Id> bankIdSet = new set<Id>();
    for( Bank_Account__c bank: Trigger.new ){
        if( ( bank.approval_Status__c == 'Approved' && bank.approval_Status__c != Trigger.oldMap.get(bank.id).approval_Status__c )
                 || (bank.approval_Status__c == 'Reject' && bank.approval_Status__c != Trigger.oldMap.get(bank.id).approval_Status__c )
                 || (bank.approval_Status__c == 'Send to finance system for approval' && Trigger.oldMap.get(bank.id).approval_Status__c == 'Finance Officer verification') ){
             
             bankIdSet.add( bank.Id);    
        }  
    }
    if( !bankIdSet.isEmpty() ){
        List<Org_Bank_Account_Junc__c> juncList = new List<Org_Bank_Account_Junc__c>();
        juncList = [SELECT id, Current_Status__c, Name,Approval_Status__c, Bank_Account__r.Approval_Status__c 
                        FROM Org_Bank_Account_Junc__c 
                        WHERE Bank_Account__c IN: bankIdSet 
                        AND Current_Status__c= true ];
                        
        List<Org_Bank_Account_Junc__c> upsertList = new List<Org_Bank_Account_Junc__c>();
        for( Org_Bank_Account_Junc__c junObj : juncList ){
        if(junObj.Bank_Account__r.Approval_Status__c == 'Approved' || junObj.Bank_Account__r.Approval_Status__c == 'Reject' ){
            junObj.Current_Status__c = false;
            }
            junObj.Approval_Status__c = junObj.Bank_Account__r.Approval_Status__c;    
            upsertList.add(junObj);
        }
        if( !upsertList.isEmpty() ){
            upsert upsertList;
        }
    }
    
}