trigger AffiliationForExtContact on Contact (after insert, after update) {
    
    list<npe5__Affiliation__c> AffList =  new List<npe5__Affiliation__c>();
    RecordType ExternalRT = [Select id,name from RecordType where Name=: 'External Contact' AND SobjectType=: 'Contact'];
    
    for(Contact Con: Trigger.new){
        if(trigger.isUpdate){
            if(Con.recordtypeid == ExternalRT.id && Con.Accountid!=Null && (con.recordtypeid != trigger.oldmap.get(con.id).recordtypeid || con.Accountid != trigger.oldmap.get(con.id).Accountid)){
                npe5__Affiliation__c Aff = new npe5__Affiliation__c();
                Aff.npe5__Contact__c = con.id;
                Aff.npe5__Organization__c = Con.Accountid;  
                Aff.npe5__StartDate__c = system.today();
                Afflist.add(Aff); 
            }    
        }
        if(trigger.isInsert){    
            if(Con.recordtypeid == ExternalRT.id && Con.Accountid!=Null){
                npe5__Affiliation__c Aff = new npe5__Affiliation__c();
                Aff.npe5__Contact__c = con.id;
                Aff.npe5__Organization__c = Con.Accountid; 
                Aff.npe5__StartDate__c = system.today(); 
                Afflist.add(Aff);  
            }
        }
    }
    
    if(Afflist != Null)
        upsert Afflist ;

}