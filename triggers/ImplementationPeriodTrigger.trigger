trigger ImplementationPeriodTrigger on Implementation_Period__c (before insert, before update,after insert,after update) 
    {
        if(trigger.isBefore)
        {
            if(trigger.isInsert) 
            {
                    
            }
            if(trigger.isUpdate) 
            {
                if(!ImpTrgRecursionController.hasAlreadyRunBeforeUpdateImp())
                {   
                    
                    ImpTrgRecursionController.setAlreadyRunBeforeUpdateImp();
                    ImplementationPeriodTriggerHandler.UpdateGMIP(trigger.new,trigger.oldMap);    
                    ImplementationPeriodTriggerHandler.reopenFramework(trigger.new,trigger.oldMap);
                    
                                    
                }
            }
        }
        if(trigger.isAfter)
        {            
            if(!ImpTrgRecursionController.hasAlreadyRunAfterImp())
            {
                ImpTrgRecursionController.setAlreadyRunAfterImp();
                ImplementationPeriodTriggerHandler.insertOnImpUpdate(trigger.new,trigger.oldMap);
                
                ImplementationPeriodTriggerHandler.GIPApprovalUpdateBankAccount(trigger.new,trigger.oldMap,trigger.isInsert,trigger.isUpdate);
                ImplementationPeriodTriggerHandler.UpdateOnApprovalStatusChangeExp_Imp(trigger.new,trigger.oldMap,trigger.isInsert,trigger.isUpdate);                               
            }             
            if(trigger.isInsert) 
            {    
                AffiliationServices.shareIPRecord(trigger.new);       
                ImplementationPeriodTriggerHandler.changeNamingConvention(trigger.new,trigger.oldMap); 
                /**02-July-2015: Methods to share newly created Implementation Period with LFA Portal Users**/  
                AffiliationServices.shareIPwithLFA(trigger.new);
                //ImplementationPeriodTriggerHandler.shareIPRecord(trigger.new);
                
            }
            if(trigger.isUpdate) 
            {
                if(!ImpTrgRecursionController.hasAlreadyRunAfterUpdateImp())
                {    
                    system.debug('**Triger Old Map After'+trigger.oldMap); 
                    
                    ImpTrgRecursionController.setAlreadyRunAfterUpdateImp();
                    
                    //ImplementationPeriodTriggerHandler.generateReportingPeriods(trigger.new,trigger.oldMap); 
                    //ImplementationPeriodTriggerHandler.insertOnImpUpdate(trigger.new,trigger.oldMap);                                                           
                }
            }
        }
    }
    
/*
trigger ImplementationPeriodTrigger on Implementation_Period__c (after delete, after insert, after undelete, 
                                                                 after update, before delete, before insert, before update) {
    Set<Id> ipIDSet = new Set<Id>();
    Set<Id> conceptNoteIdSet = new Set<Id>();
    String reportingStr;
    List<Implementation_Period__c> triggernewIPList = new List<Implementation_Period__c>();
    // INSERT   
    if(trigger.isInsert) {
        //is Before Insert trigger
        if(trigger.isBefore) {
            
            
        }
        //is After Insert trigger
        if(trigger.isAfter) {
            for(Implementation_Period__c objIP : trigger.new) {
                conceptNoteIdSet.add(objIP.Concept_Note__c);
            }
            if(conceptNoteIdSet != null && !conceptNoteIdSet.isEmpty())
                ImplementationPeriodTriggerHandler.shareIPRecord(conceptNoteIdSet, trigger.new); 
        }
    }
    
    //  Update   
    if(trigger.isUpdate) {
        //is Before Update trigger
        if(trigger.isBefore) {
            
        }
        //is After Update trigger
        if(trigger.isAfter) {
            
                for(Implementation_Period__c objIP : trigger.new) {
                
                    if(objIP.Concept_Note__c != null)
                        conceptNoteIdSet.add(objIP.Concept_Note__c);
                    //
                    if(Trigger.oldMap.get(objIP.id).Status__c != objIP.Status__c && objIP.Status__c == 'Grant-Making') {
                        ipIDSet.add(objIP.Id);
                        reportingStr = Trigger.newMap.get(objIP.Id).Reporting__c;
                        triggernewIPList.add(objIP);
                        
                    }
                }
                if(conceptNoteIdSet != null && !conceptNoteIdSet.isEmpty())
                    ImplementationPeriodTriggerHandler.shareIPRecord(conceptNoteIdSet, trigger.new); 
                if(triggernewIPList != null && !triggernewIPList.isEmpty())
                   ImplementationPeriodTriggerHandler.generateReportingPeriods(triggernewIPList);
        }
    }
    
    //  DELETE
    if(trigger.isDelete) {
        if(trigger.isBefore) {
            
        }
        if(trigger.isAfter) {
            
        }
    }
    
}
*/