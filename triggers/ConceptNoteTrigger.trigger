/*********************************************************************************
* Trigger: ConceptNoteTrigger
* Created by {Nikhil Kamath}, {DateCreated Apr-28-2015}
----------------------------------------------------------------------------------
* Purpose: This trigger on ConceptNote consists of previosly active CN triggers
1.Share with CCM CT
2.trgAfterUpdateCN
3.trgAfterInsertCN
4.ShareCNAffliation
5.PCNSubmittedAfterUpdate 
6.trgBeforeInsertCN
7.rollupFieldsToBand    
----------------------------------------------------------------------------------
*********************************************************************************/

trigger ConceptNoteTrigger on Concept_Note__c (before insert, after insert, after update, after delete, after undelete) {
 /*	If(Trigger.isBefore)
   {
        if(!StopRecursion.CN_hasalreadyrunbeforeCN())
        {
        	//StopRecursion.set_CN_hasalreadyrunbeforeCN();
        }
        If(Trigger.isInsert)
        {
        	if(!StopRecursion.CN_hasalreadyrunbeforeinsertCN()) {
        		ConceptNoteTriggerHandler.trgBeforeInsertCN(trigger.new);
        		//StopRecursion.set_CN_hasalreadyrunbeforeinsertCN();	
        	}
        	
        }
        If(Trigger.isUpdate)
        {
             if(!StopRecursion.CN_hasalreadyrunbeforeupdateCN()) {
             	StopRecursion.set_CN_hasalreadyrunbeforeupdateCN();
             }     
        }
   }
   If(Trigger.isAfter)
   {    
        if(!StopRecursion.CN_hasalreadyrunafterCN())
        {
            StopRecursion.set_CN_hasalreadyrunafterCN(); 
        }
        If(Trigger.isInsert)
        {   
            if(!StopRecursion.CN_hasalreadyrunafterinsertCN()) {
            	ConceptNoteTriggerHandler.trgAfterInsertCN(trigger.new);
            	ConceptNoteTriggerHandler.Share_with_CCM_CT(trigger.new);
            	ConceptNoteTriggerHandler.ShareCNAffliation(trigger.new);
            	ConceptNoteTriggerHandler.rollupFieldsToBand(trigger.old,trigger.new);
            	StopRecursion.set_CN_hasalreadyrunafterinsertCN();
            }
        }
        If(Trigger.isUpdate)
        {
            if(!StopRecursion.CN_hasalreadyrunafterupdateCN()) {
            	ConceptNoteTriggerHandler.trgAfterUpdateCN(trigger.new, trigger.oldMap);
            	ConceptNoteTriggerHandler.Share_with_CCM_CT(trigger.new);
            	//ConceptNoteTriggerHandler.PCNSubmittedAfterUpdate(trigger.new); // trigger is inactive
            	ConceptNoteTriggerHandler.rollupFieldsToBand(trigger.old,trigger.new);
            	StopRecursion.set_CN_hasalreadyrunafterupdateCN();	
            }
                        
        }
        If(Trigger.isDelete)
        {
            if(!StopRecursion.CN_hasalreadyrunafterdeleteCN()) {
            	ConceptNoteTriggerHandler.rollupFieldsToBand(trigger.old,trigger.new);
            	StopRecursion.set_CN_hasalreadyrunafterdeleteCN();
            }            
        }
        If(Trigger.isUnDelete)
        {
        	if(!StopRecursion.CN_hasalreadyrunafterundeleteCN()) {
            	ConceptNoteTriggerHandler.rollupFieldsToBand(trigger.old,trigger.new);
            	StopRecursion.set_CN_hasalreadyrunafterundeleteCN();
            }        	
        }
   }*/
}