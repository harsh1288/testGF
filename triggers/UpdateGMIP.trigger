trigger UpdateGMIP on Implementation_Period__c (before Update) {

   /* List<Implementation_Period__c> IPList = new List<Implementation_Period__c>();
    List<Id> CPList = new List<Id>();
    List<Id> IPIdList = new List<Id>();
    List<Grant_Indicator__c> lstGrntInd = new List<Grant_Indicator__c>();
    List<Grant_Indicator__c> lstGrntCovInd = new List<Grant_Indicator__c>();
    List<Goals_Objectives__c> listClneGoals = new List<Goals_Objectives__c>();
    List<Ind_Goal_Jxn__c> jnctGoalIndicator = new List<Ind_Goal_Jxn__c>();
    List<Module__c> lstModIP = new List<Module__c>();
    List<Grant_Intervention__c> lstGIntervention = new List<Grant_Intervention__c>();
    List<Implementer__c> lstIMP = new List<Implementer__c>();
    Map<Id,Id> mapIp = new Map<Id,Id>();
    Map<Id,Id> OldNewGoalMap = new Map<Id,Id>();
    Map<Id,Id> goalMap = new Map<Id,Id>();
    Map<Id,Id> OldNewIndiMap = new Map<Id,Id>();
    Map<Id,Set<Id>> mapGIPTemplateID = new Map<Id,Set<Id>>();
    Map<Id,Id> mapIMIP = new Map<Id,Id>();
    Set<Id> oldGoalId = new Set<Id>();
    Set<Id> oldIndiId = new Set<Id>();
    Set<Id> grantIdList = new Set<Id>();
    String strConCode;
    String strCompCode;
    String strShortNM;
    String strImpCycle; 

    
    Page__c objGIPPage;
    List<Page__c> lstGIPPages = new List<Page__c>();
    Id tempId;
    Set<Id> lstCNPageId = new Set<Id>() ;
    Set<Id> lstIPPageId = new Set<Id>();
    Set<Id> lstAccId = new Set<Id>();
    Id PFId;
    List<Page__feed> IPPagefeed = new List<Page__feed>();
    
    for(Implementation_Period__c IP: trigger.new){
        //system.debug('Old IP Status ** '+Trigger.oldMap.get(IP.id).Status__c);
        system.debug('**Trigger.oldMap.get(IP.id).Status__c **'+Trigger.oldMap.get(IP.id).Status__c);
        system.debug('**Trigger.oldMap.get(IP.id).Name && Trigger.oldMap.get(IP.id).Status__c!= IP.Status__c **'+Trigger.oldMap.get(IP.id).Name);
        if(IP.Status__c == 'Grant-Making' && Trigger.oldMap.get(IP.id).Status__c!= IP.Status__c && Trigger.oldMap.get(IP.id).Name== IP.Name )
        {
        IPList.add(IP);
        lstAccId.add(IP.Principal_Recipient__c);
        CPlist.add(IP.Concept_Note__c);
        system.debug('**CPlist **'+CPlist);
        IPIdList.add(IP.Id);
        system.debug('**IPIdList **'+IPIdList);
        mapIp.put(IP.Concept_Note__c,IP.id);
        mapIMIP.put(IP.Principal_Recipient__c,IP.id);
        strImpCycle = IP.Implementation_Cycle__c;
        
          if(IP.Grant__c!=null)
            {
                grantIdList.add(IP.Grant__c);
            }
        system.debug('**mapIp **'+mapIp);
        system.debug('IP List '+IPList+' CP LIst '+CPList+' IP Id '+IPIDList);
        system.debug('**lstAccId'+lstAccId);
    }  
    }
    
    List<Account> lstAccForNM = [Select Id,Short_Name__c,Country__r.Country_Code__c From Account where Id in: lstAccId];
    for(Account acc : lstAccForNM ){
        strConCode = acc.Country__r.Country_Code__c;
        strShortNM = acc.Short_Name__c;
    }
    
    
    if(IPIdList.size()>0){
    
    /*********************************************************************************************************************************************
    Purpose : Creating a MAP on Grant to Query email Ids from various fields on Country Object. Also to take the Component for Naming FrameWorks
    ***********************************************************************************************************************************************/    
    
    /*  Map<Id,Id> cntryMephMap = new Map<Id,Id>();
      Map<Id,Id> countryHPMMap = new Map<Id,Id>();
        if(grantIdList.size()>0)
        {   
            List<Grant__c> grantList = new List<Grant__c>([Select Id,Country__r.MEPH_Specialist__c,Country__r.HPM_Specialist__c,Disease_Component__c from Grant__c where id in:grantIdList and Country__r.MEPH_Specialist__c != null]);
           List<Grant__c> grantListNM = new List<Grant__c>([Select Id,Country__r.MEPH_Specialist__c,Disease_Component__c from Grant__c where id in:grantIdList ]);
           for(Grant__c objGrant: grantListNM ){
                if(objGrant.Disease_Component__c == 'Malaria'){
                        strCompCode = 'M';
                    }else if(objGrant.Disease_Component__c == 'Tuberculosis'){
                        strCompCode = 'T';
                    }else if(objGrant.Disease_Component__c == 'HIV/AIDS'){
                        strCompCode = 'H';
                    }else if(objGrant.Disease_Component__c == 'HIV/TB'){
                        strCompCode = 'C';
                    }else{
                        strCompCode = 'S';
                    }

           }
           for(Grant__c c:grantList)
            {
                    cntryMephMap.put(c.Id,c.Country__r.MEPH_Specialist__c);
                    countryHPMMap.put(c.Id,c.Country__r.HPM_Specialist__c);
                    
            }            
        }
        
    /*********************************************************************************************************************************************
    Purpose : To create Different Frameworks for Performance, Budget, HPC
    ***********************************************************************************************************************************************/    
       
   /*  List<Performance_Framework__c> newPf = new List<Performance_Framework__c>();        
     List<IP_Detail_Information__c> newDb = new List<IP_Detail_Information__c>();
     List<HPC_Framework__c> newHPC = new List<HPC_Framework__c>();
     
        for (Implementation_Period__c ip: trigger.new)
        {
            if(cntryMephMap.containskey(String.valueOf(ip.Grant__c)))
            {
                System.debug(cntryMephMap.get(ip.Grant__c));
                Performance_Framework__c pf = new Performance_Framework__c(Name = 'PF-'+strConCode+'-'+strCompCode+'-'+strShortNM+'-'+strImpCycle ,Implementation_Period__c =ip.id,PF_Status__c='Not yet submitted',MEPH_Specility__c=cntryMephMap.get(ip.Grant__c));
                newPf.add(pf);
            }
            else
            {
                Performance_Framework__c pf = new Performance_Framework__c(Name = 'PF-'+strConCode+'-'+strCompCode+'-'+strShortNM+'-'+strImpCycle ,Implementation_Period__c =ip.id,PF_Status__c='Not yet submitted');
                newPf.add(pf);
            }
            
            Decimal GrantCurrency;
            if(ip.Currency_of_Grant_Agreement__c == 'USD'){
                GrantCurrency = ip.High_level_budget_GAC_1_USD__c;
            }else{
                GrantCurrency = ip.High_level_budget_GAC_1_EUR__c;
            }
            
            IP_Detail_Information__c detailBudget = new IP_Detail_Information__c(Name = 'Budget-'+strConCode+'-'+strCompCode+'-'+strShortNM+'-'+strImpCycle ,Implementation_Period__c=ip.id, High_level_budget_GAC_1_EUR__c = ip.High_level_budget_GAC_1_EUR__c  ,High_level_budget_GAC_1_USD__c = ip.High_level_budget_GAC_1_USD__c,Remaining_Budget_Amount__c = GrantCurrency );            
            newDb.add(detailBudget);
            
             if(countryHPMMap.containskey(String.valueOf(ip.Grant__c)))
            {
                System.debug(countryHPMMap.get(ip.Grant__c));
                HPC_Framework__c hp = new HPC_Framework__c (Name = 'HPC-'+strConCode+'-'+strCompCode+'-'+strShortNM+'-'+strImpCycle ,Grant_Implementation_Period__c = ip.Id,HPC_Specialist__c=countryHPMMap.get(ip.Grant__c),HPC_Status__c = 'Not Yet Submitted by PR');
                newHPC.add(hp);
            }
            else
            {
                HPC_Framework__c hp = new HPC_Framework__c (Name = 'HPC-'+strConCode+'-'+strCompCode+'-'+strShortNM+'-'+strImpCycle ,Grant_Implementation_Period__c = ip.Id,HPC_Status__c = 'Not Yet Submitted by PR');
                newHPC.add(hp);
            }
            
        }
        try
        {
            insert newPf;
        }
        Catch(Exception e)
        {
            System.debug('*******Insertion of performance framework record failed due to: '+e);
        }
        try
        {
            insert newDb;
        }
        catch(Exception e)
        {
            System.debug('*******Insertion of detail budget record failed due to: '+e);
        }
        try
        {
            insert newHPC;
        }
        catch(Exception e)
        {
            System.debug('*******Insertion of detail budget record failed due to: '+e);
        }
    
    /*********************************************************************************************************************************************
    Purpose : Querying the Only Performance FrameWork which may be present in the Implementation Period.
    ***********************************************************************************************************************************************/    
        
   /* List<Performance_Framework__c> lstPFwork = [Select Id from Performance_Framework__c where Implementation_Period__c IN:IPIdList ];
    if(lstPFwork.size() > 1 ){
        for(Implementation_Period__c ipRec : Trigger.new)
          ipRec.addError('FrameWork Record Already Exist.Delete the Existing and try recreating the record.');
    }
    if(lstPFwork.size()>0){
    for(Template__c listTemplate : [Select Id From Template__c Where Name =: Label.GM_Template_Name Limit 1])
    {
        tempId = listTemplate.Id;
    }
    
    /*********************************************************************************************************************************************
    Purpose : To create one Default Implementer that is the Principal Recipient of that Implementation Period.
    ***********************************************************************************************************************************************/    
        
   /* List<Account> lstAcc = [Select Id,Name,Short_Name__c,Country__c,Type__c,Sub_Type__c From Account where Id in:lstAccId];
    
    for(Account objAcc : lstAcc){
        Implementer__c objIMP = new Implementer__c();
        Id IPId  = mapIMIP.get(objAcc.Id);
        objIMP.Implementer_Short_Name__c = objAcc.Short_Name__c;
        objIMP.Implementer_Sub_Type__c = objAcc.Sub_Type__c;
        objIMP.Add_Implementer__c = 'New';
        objIMP.Implementer_Name__c = objAcc.Name;
        objIMP.Implementer_Role__c = 'Principal Recipient';
        objIMP.Principal_Recipient__c = objAcc.Id;
        objIMP.Grant_Implementation_Period__c = IPId;
        objIMP.Country__c = objAcc.Country__c;
        ObjIMP.Performance_Framework__c = lstPFwork[0].Id;
        lstIMP.add(objIMP);
    }
    system.debug('**lstIMP'+lstIMP);
    if(lstIMP.size()>0){upsert lstIMP;}
    
    /*********************************************************************************************************************************************
    Purpose : To create Goals and Objectives which are Present in Concept Note of that Implementation Period.
    ***********************************************************************************************************************************************/  
     //List of Goals
    /* List<Goals_Objectives__c> goal = [SELECT Concept_Note__c,Goal__c,Performance_Framework__c,Goal_French__c,Goal_Russian__c,Goal_Spanish__c,Number__c,Type__c,Old_GoalID__c FROM Goals_Objectives__c WHERE Concept_Note__c IN: CPlist];
     for(Goals_Objectives__c objGoal : goal){
     oldGoalId.add(objGoal.ID);     
     }
     
     RecordType objRecGoalNT = [SELECT name,Id from RecordType where name = 'Not Yet Submitted - PR' AND SobjectType ='Goals_Objectives__c'];
     RecordType objRecObjNT = [SELECT name,Id from RecordType where name = 'Objective Not Yet Submitted -PR' AND SobjectType ='Goals_Objectives__c']; 
     RecordType objRecGINT = [SELECT name,Id from RecordType where name = 'Grant-Making' AND SobjectType ='Grant_Intervention__c'];
     /*********************************************************************************************************************************************
    Purpose : To pass Performance FrameWork ID into Modules which are linked to that Implementation Period through PR.
    ***********************************************************************************************************************************************/  
    
     /* List<Module__c> IPModule =[SELECT Id,Name,Performance_Framework__c  FROM Module__c WHERE Implementation_Period__c IN: IPIdList];
         for(Module__c objMod : IPModule ){
             objMod.Performance_Framework__c = lstPFwork[0].Id;
             lstModIP.add(objMod);
         }
         upsert lstModIP;
    /*********************************************************************************************************************************************
    Purpose : To create Grant Indicators of Type Impact and Outcome for this Implementation Period which are present in corresponding Concept Note 
    ***********************************************************************************************************************************************/  
   /* RecordType objGrantIndiOut = [SELECT name,Id from RecordType where name = 'Coverage/Output_IP'  AND SobjectType ='Grant_Indicator__c'];
    RecordType objGrantIndiImp = [SELECT name,Id from RecordType where name = 'Impact_IP'  AND  SobjectType ='Grant_Indicator__c'];
    RecordType objGrantIndiOutCome = [SELECT name,Id from RecordType where name = 'Outcome_IP'  AND SobjectType ='Grant_Indicator__c'];
    
    //List of Indicator 
    List<Grant_Indicator__c> grntIndicator = [SELECT Name,Grant_Intervention__c,Implementer__c,Performance_Framework__c,Indicator_Type__c,Indicator__c,Indicator_Full_Name__c,Data_Type__c,
                                              Baseline_Denominator__c,Baseline_numerator__c,Baseline_Percent__c,Baseline_Sources__c,Baseline_Value__c,Baseline_Year__c,
                                              Comments__c,Comments_French__c,Comments_Russian__c,Comments_Spanish__c,Component__c,Country__c,Coverage_Text__c,
                                              Target_Accumulation__c,Target_Area__c,Target_Denominator_Y1__c,Target_Denominator_Y2__c,Target_Denominator_Y3__c,
                                              Target_Denominator_Y4__c,Target_Numerator_Y1__c,Target_Numerator_Y2__c,Target_Numerator_Y3__c,Target_Numerator_Y4__c,
                                              Target_Value_Y1__c,Target_Value_Y2__c,Target_Value_Y3__c,Target_Value_Y4__c,Type__c,Grant_Implementation_Period__c,
                                              Concept_Note__c,Goal_Objective__c,Standard_or_Custom__c,Parent_Module__c, 
                                              Indicator__r.Is_Disaggregated__c, Indicator__r.Disaggregated_Name__c,Old_IndicatorId__c,Y4_Report_Due__c,Y3_Report_Due__c,Y2_Report_Due__c,Y1_Report_Due__c,Target_Value_Y12__c,
                                              Target_Value_Y22__c,Target_Value_Y32__c,Target_Value_Y42__c,Baseline_Value1__c 
                                              From Grant_Indicator__c 
                                              where Concept_Note__c IN: CPlist and (Indicator_Type__c =: 'Impact' OR Indicator_Type__c =: 'Outcome' ) ];  //and Standard_or_Custom__c =: 'Standard' and Indicator__c != null
        system.debug('List of Indicators '+grntIndicator);                    
        
   /*********************************************************************************************************************************************
    Purpose : To create Grant Indicators of Type Output for this Implementation Period with Is IP Coverage checkbox equal to true. 
              This ensures that we have different set of Output indicators for CN and IP.
    ***********************************************************************************************************************************************/  
    
    /* List<Grant_Indicator__c> grntCovOutIndicator = [SELECT Name,Grant_Intervention__c, Performance_Framework__c,Indicative_Denominator1__c,Indicative_Denominator2__c,Indicative_Denominator3__c,Indicative_Denominator4__c,
                                              IndicativeNumerator4__c,Indicative_Percent__c,Indicative_Percent1__c,Indicative_Percent2__c,Indicative_Percent3__c,Indicative_Percent4__c,
                                              IndicativeTarget1__c,IndicativeTarget2__c,IndicativeTarget3__c,
                                              Above_Indicative_Denominator1__c,Above_Indicative_Denominator2__c,Above_Indicative_Denominator3__c,IndicativeNumerator1__c,IndicativeNumerator2__c,
                                              Above_Indicative_Percent2__c,Above_Indicative_Percent3__c,Above_Indicative_Percent4__c,Above_Indicative_Denominator4__c,IndicativeNumerator3__c,
                                              Above_Indicative_Numerator1__c,Above_Indicative_Numerator2__c,Above_Indicative_Numerator3__c,Above_Indicative_Numerator4__c,Above_Indicative_Percent1__c,
                                              Implementer__c,Indicator_Type__c,Indicator__c,Indicator_Full_Name__c,Data_Type__c,
                                              Baseline_Denominator__c,Baseline_numerator__c,Baseline_Percent__c,Baseline_Sources__c,Baseline_Value__c,Baseline_Year__c,
                                              Comments__c,Comments_French__c,Comments_Russian__c,Comments_Spanish__c,Component__c,Country__c,Coverage_Text__c,
                                              Target_Accumulation__c,Target_Area__c,Target_Denominator_Y1__c,Target_Denominator_Y2__c,Target_Denominator_Y3__c,
                                              Target_Denominator_Y4__c,Target_Numerator_Y1__c,Target_Numerator_Y2__c,Target_Numerator_Y3__c,Target_Numerator_Y4__c,
                                              Target_Value_Y1__c,Target_Value_Y2__c,Target_Value_Y3__c,Target_Value_Y4__c,Type__c,Grant_Implementation_Period__c,
                                              Concept_Note__c,Goal_Objective__c,Standard_or_Custom__c,Parent_Module__c, Tied_To__c,Decimal_Places__c,Reporting_Frequency__c,
                                              Indicator__r.Is_Disaggregated__c,Indicator__r.Reporting_Frequency__c, Indicator__r.Disaggregated_Name__c,Old_IndicatorId__c,Is_IP_Coverage_Indicator__c,Y4_Report_Due__c,Y3_Report_Due__c,Y2_Report_Due__c,Y1_Report_Due__c
                                              From Grant_Indicator__c 
                                              where Grant_Implementation_Period__c IN: IPIdList and Indicator_Type__c =: 'Coverage/Output' AND Is_IP_Coverage_Indicator__c = false ];
     
     system.debug('***List of grntCovOutIndicator '+grntCovOutIndicator);
     for(Grant_Indicator__c objIndi : grntIndicator){
     oldIndiId.add(objIndi.ID);     
     }
     
     List<Ind_Goal_Jxn__c> lstJuncObj = [Select Indicator__c,Goal_Objective__c from Ind_Goal_Jxn__c where Goal_Objective__c in:goal AND Indicator__c in:grntIndicator ];
     system.debug('**lstJuncObj'+lstJuncObj);
     
        //Insert Goals Record
        for (Goals_Objectives__c CNgoals : goal)
        {
            Id IPId1  = mapIp.get(CNgoals.Concept_Note__c);
            Goals_Objectives__c clonegoal = CNgoals.clone(false,true);
            clonegoal.Concept_Note__c = null;
            clonegoal.Implementation_Period__c = IPId1;
            clonegoal.Old_GoalID__c = CNgoals.Id;
            clonegoal.Performance_Framework__c = lstPFwork[0].Id;
            if(clonegoal.Type__c == 'Goal'){
                clonegoal.RecordTypeId = objRecGoalNT.Id;
            }else{
                 clonegoal.RecordTypeId = objRecObjNT.Id;
            }
            goalMap.put(IPId1,CNgoals.Id);
            
            
            listClneGoals.add(clonegoal);
        }
        
        if (listClneGoals.size() >0)upsert listClneGoals;
        
         //Insert Grant indicator Record
        for (Grant_Indicator__c grntInd : grntIndicator)
            {
                
                Id IPId  = mapIp.get(grntInd.Concept_Note__c);
                Id GoalId = goalMap.get(IPId);
                system.debug('Grant Id '+IPId);
                Grant_Indicator__c clnGrntInd = new Grant_Indicator__c();
                system.debug('Grant Details before Clone**' +grntInd);
                clnGrntInd = grntInd.clone(false,true);
                String perSym = '';
                if(grntInd.Data_Type__c == Label.Percent_DT || grntInd.Data_Type__c == Label.NaP_DT){
                    perSym =  Label.Percent_symb;
                }
                if(grntInd.Data_Type__c == Label.Ratio){
                    perSym =   Label.Ratio_label;
                }
                
                //clnGrntInd.Is_Disaggregated__c = grntInd.Indicator__r.Is_Disaggregated__c;
                clnGrntInd.Disaggregated_Name__c =  grntInd.Indicator__r.Disaggregated_Name__c;
                if(grntInd.Target_Value_Y1__c !=null)
                clnGrntInd.Target_Value_Y12__c = String.valueof(grntInd.Target_Value_Y1__c)+perSym ;
                if(grntInd.Target_Value_Y2__c !=null)
                clnGrntInd.Target_Value_Y22__c = String.valueof(grntInd.Target_Value_Y2__c)+perSym;
                if(grntInd.Target_Value_Y3__c !=null)
                clnGrntInd.Target_Value_Y32__c = String.valueof(grntInd.Target_Value_Y3__c)+perSym;
                if(grntInd.Target_Value_Y4__c !=null)
                clnGrntInd.Target_Value_Y42__c = String.valueof(grntInd.Target_Value_Y4__c)+perSym;
                if(grntInd.Baseline_Value__c !=null)
                clnGrntInd.Baseline_Value1__c = String.valueof(grntInd.Baseline_Value__c)+perSym;
                if(clnGrntInd.Indicator_Type__c == 'Impact'){
                    clnGrntInd.RecordTypeId = objGrantIndiImp.Id;
                }else{
                    clnGrntInd.RecordTypeId = objGrantIndiOutCome.Id;
                }
                system.debug('Grant Details after Clone**' +clnGrntInd);
                clnGrntInd.Concept_Note__c = null;
                clnGrntInd.Grant_Implementation_Period__c = IPId;
                clnGrntInd.Old_IndicatorId__c = grntInd.Id;
                //clnGrntInd.Goal_Objective__c = objgoal.Id;
                //Commented as this functionality is already working
                clnGrntInd.Performance_Framework__c = lstPFwork[0].Id;
                system.debug('Grant Details after adding Grant**' +clnGrntInd);
                lstGrntInd.add(clnGrntInd);
                
            }
            if(lstGrntInd.size() >0)insert lstGrntInd;
            system.debug('Grant Inserted '+lstGrntInd);
            
         for (Grant_Indicator__c grntCov : grntCovOutIndicator)
            {
                
                
                Grant_Indicator__c clnGrntInd = new Grant_Indicator__c();
                system.debug('Grant Details before Clone**' +grntCov);
                clnGrntInd = grntCov.clone(false,true);
                //clnGrntInd.Is_Disaggregated__c = grntCov.Indicator__r.Is_Disaggregated__c;
                clnGrntInd.Disaggregated_Name__c =  grntCov.Indicator__r.Disaggregated_Name__c;
                clnGrntInd.Is_IP_Coverage_Indicator__c = true;
                //Decimal baseVal = grntCov.Baseline_Value__c;
                if(grntCov.Baseline_Value__c !=null){
                String d = grntCov.Decimal_Places__c;
                if(d==null){
                    d='0';
                }
                Integer deci = Integer.valueof(d); 
                
                String perSym = '';
                if(grntCov.Data_Type__c == Label.Percent_DT || grntCov.Data_Type__c == Label.NaP_DT){
                    perSym =  Label.Percent_symb;
                }
                if(grntCov.Data_Type__c == Label.Ratio){
                    perSym =   Label.Ratio_label;
                }
                
                clnGrntInd.Baseline_Value1__c = String.valueof((grntCov.Baseline_Value__c).setScale(deci)) + perSym;
                }//clnGrntInd.Baseline_Value1__c = String.valueof((grntCov.Baseline_Value__c));
                system.debug('Grant Details after Clone**' +clnGrntInd);
                //clnGrntInd.Concept_Note__c = null;
                //clnGrntInd.Grant_Implementation_Period__c = IPId;
                if(grntCov.Indicator__r.Reporting_Frequency__c!=null){
                        if(grntCov.Indicator__r.Reporting_Frequency__c.contains('12 Months') && !grntCov.Indicator__r.Reporting_Frequency__c.contains('Based on Reporting Frequency')){
                             clnGrntInd.Reporting_Frequency__c = '12 Months';                       
                        }else{
                             clnGrntInd.Reporting_Frequency__c = 'Based on Reporting Frequency';
                        }
                }else{
                clnGrntInd.Reporting_Frequency__c = 'Based on Reporting Frequency';
                }
                clnGrntInd.Old_IndicatorId__c = grntCov.Id;
                 clnGrntInd.Performance_Framework__c = lstPFwork[0].Id;
                 clnGrntInd.RecordTypeId = objGrantIndiOut.Id;
                //clnGrntInd.Goal_Objective__c = objgoal.Id;
                //Commented as this functionality is already working
                if(clnGrntInd.Indicator_Type__c == 'Coverage/Output' )
                {
                    //clnGrntInd.Target_Accumulation__c = '';
                    clnGrntInd.Target_Area__c = '';
                    clnGrntInd.Target_Denominator_Y1__c = 0;
                    clnGrntInd.Target_Denominator_Y2__c = 0;
                    clnGrntInd.Target_Denominator_Y2__c = 0;
                    clnGrntInd.Target_Denominator_Y3__c = 0;
                    clnGrntInd.Target_Denominator_Y4__c = 0;
                    clnGrntInd.Target_Numerator_Y1__c = 0;
                    clnGrntInd.Target_Numerator_Y2__c = 0;
                    clnGrntInd.Target_Numerator_Y3__c = 0;
                    clnGrntInd.Target_Numerator_Y4__c = 0;
                    clnGrntInd.Target_Value_Y1__c = 0;
                    clnGrntInd.Target_Value_Y2__c = 0;
                    clnGrntInd.Target_Value_Y3__c = 0;
                    clnGrntInd.Target_Value_Y3__c = 0;
                  
                   
                } 
                system.debug('Grant Details after adding Grant**' +clnGrntInd);
                lstGrntCovInd.add(clnGrntInd);
                
            }
            if(lstGrntCovInd.size() >0)upsert lstGrntCovInd;
            
            //Insert Junction Object Record
            List<Goals_Objectives__c> lstNewGoal = [SELECT Id,Implementation_Period__c,Old_GoalID__c FROM Goals_Objectives__c where Implementation_Period__c IN: IPList AND Old_GoalID__c IN: oldGoalId];
            List<Grant_Indicator__c> lstNewIPIndicator = [SELECT Id,Goal_Objective__c,Grant_Implementation_Period__c,Old_IndicatorId__c FROM Grant_Indicator__c WHERE Grant_Implementation_Period__c IN: IPIdList and (Indicator_Type__c = 'Impact' OR Indicator_Type__c = 'Outcome') AND Old_IndicatorId__c IN:oldIndiId];
            
                for(Goals_Objectives__c objNewG : lstNewGoal){
                OldNewGoalMap.put(objNewG.Old_GoalID__c,objNewG.Id);
                }
                for(Grant_Indicator__c objNewI : lstNewIPIndicator){
                OldNewIndiMap.put(objNewI.Old_IndicatorId__c,objNewI.Id);
                }
            
            for(Ind_Goal_Jxn__c objJxn : lstJuncObj){
                            Ind_Goal_Jxn__c jnctObj = new Ind_Goal_Jxn__c();
                            jnctObj.Goal_Objective__c = OldNewGoalMap.get(objJxn.Goal_Objective__c);
                            jnctObj.Indicator__c = OldNewIndiMap.get(objJxn.Indicator__c);
                            jnctGoalIndicator.add(jnctObj);
            }
            if(jnctGoalIndicator.size() >0) upsert jnctGoalIndicator;
          
             
    /*********************************************************************************************************************************************
    Purpose : Passing the Performance FrameWork Id in the Intervention which are related to modules as queried above.
    ***********************************************************************************************************************************************/  
    
     /*    List<Grant_Intervention__c> grntIntervntionIPPF = [SELECT Id, Name,Implementation_Period__c,Performance_Framework__c  FROM Grant_Intervention__c WHERE Module__c IN: IPModule and Implementation_Period__c IN: IPIdList];
         for(Grant_Intervention__c objGI : grntIntervntionIPPF ){
             objGI.Performance_Framework__c = lstPFwork[0].Id;
             objGI.RecordTypeId = objRecGINT.Id;
             lstGIntervention.add(objGI);
         }
         upsert lstGIntervention;
         
         //Remove Grant indicator Record if Module is reomove
         List<Grant_Indicator__c> grntIndicatorIP = [SELECT Id,Name
                                              From Grant_Indicator__c 
                                              where Grant_Implementation_Period__c IN: IPIdList and Indicator_Type__c =: 'Coverage/Output' and Standard_or_Custom__c =: 'Standard' and Indicator__c != null and Parent_Module__c NOT IN: IPModule];
        system.debug('List of Indicators '+grntIndicatorIP);
          if(grntIndicatorIP.size()>0) delete grntIndicatorIP;
      //Remove Grant intervention Record if Module is reomove
    /*List<Grant_Intervention__c> grntIntervntionIP = [SELECT Id, Name,Implementation_Period__c FROM Grant_Intervention__c WHERE Module__c NOT IN: IPModule and Implementation_Period__c IN: IPIdList];
    system.debug('List of Intervention** '+grntIntervntionIP);
   if(grntIntervntionIP.size()>0) delete grntIntervntionIP;
 
            }
       }*/
  }