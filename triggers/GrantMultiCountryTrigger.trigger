trigger GrantMultiCountryTrigger on Grant_Multi_Country__c (before delete) {
Set<Id> setGMCID = new Set<Id>();
Set<Id> setCountryID = new Set<Id>();
Set<Id> IPiD= new Set<Id>();
List<Grant_Indicator__c> lstGI = new List<Grant_Indicator__c>();
List<Grant_Indicator__c> lstGICov = new List<Grant_Indicator__c>();
List<Grant_Indicator__c> lstToUpdateGI = new List<Grant_Indicator__c>();
List<Grant_Indicator__c> lstToUpdateGICov = new List<Grant_Indicator__c>();

List<Grant_Multi_Country__c> lstGMC = new List<Grant_Multi_Country__c> ();

    for(Grant_Multi_Country__c objGMC : trigger.old){
        setGMCID.add(objGMC.Id);
        setCountryID.add(objGMC.Country__c);
        IPiD.add(objGMC.Grant_Implementation_Period__c);
    }
    
   lstGI = [Select Id,Name from Grant_Indicator__c where Country__c IN:setCountryID AND Grant_Implementation_Period__c IN : IPiD AND Indicator_type__c != 'Coverage/Output'];
   lstGICov = [Select Id,Name from Grant_Indicator__c where Country__c IN:setCountryID AND Grant_Implementation_Period__c IN : IPiD AND Is_IP_Coverage_Indicator__c = true];
   
   if(lstGI.size() > 0 || lstGICov.size() >0 ){
       for(Grant_Indicator__c objGI : lstGI ){
           objGI.Country__c = null;
           lstToUpdateGI.add(objGI);
       } 
       
       for(Grant_Indicator__c objCov : lstGICov){
           objCov.Country__c = null;
           lstToUpdateGICov.add(objCov);
       }
        
       if(lstToUpdateGI.size() > 0){
           update lstToUpdateGI;
       }
       
       if(lstToUpdateGICov.size() > 0){
           update lstToUpdateGICov;
       }
       
   }
}