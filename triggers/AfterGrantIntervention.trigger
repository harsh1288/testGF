trigger AfterGrantIntervention on Grant_Intervention__c (after insert, after update, after delete, after undelete) {

List<Grant_Intervention__c> giList = (Trigger.isDelete) ? Trigger.old : Trigger.new;
Set<Id> gipIds = new Set<Id>();
Set<Id> cnIds = new Set<Id>();


for(Grant_Intervention__c gi : giList) {
    gipIds.add(gi.Implementation_Period__c);
 }

for(Implementation_Period__c gip : [Select Concept_Note__c from Implementation_Period__c where Id in :gipIds
                                     And Concept_Note__c != null]){
                                     cnIds.add(gip.Concept_Note__c);
                                  }
                                    
updateCPFGrantInterventions.updateCPF(cnIds); 
/* 

List<CPF_Report__c> lstCPFReport = [Select Id, Concept_Note__c, Above_Indicative_Y1__c, Above_Indicative_Y2__c, 
                                    Above_Indicative_Y3__c,Above_Indicative_Y4__c,
                                    Indicative_Y1__c,Indicative_Y2__c,Indicative_Y3__c, Indicative_Y4__c
                                     from CPF_Report__c where Concept_Note__c in :cnIds];
                                     System.Debug('%%% lstCPFReport: ' + lstCPFReport);

List<AggregateResult> lstIndicativeValues = [Select 
                                Sum(Above_Indicative_Y1__c) AY1, Sum(Above_Indicative_Y2__c) AY2,
                                Sum(Above_Indicative_Y3__c) AY3, Sum(Above_Indicative_Y4__c) AY4,                                
                                Sum(Indicative_Y1__c) Y1, Sum(Indicative_Y2__c) Y2,
                                Sum(Indicative_Y3__c) Y3, Sum(Indicative_Y4__c) Y4,
                                Implementation_Period__r.Concept_Note__c CN
                                    From Grant_Intervention__c 
                                    Where Implementation_Period__r.Concept_Note__c in : cnIds
                                    GROUP BY Implementation_Period__r.Concept_Note__c];

for(CPF_Report__c cpf : lstCPFReport) {

    for(AggregateResult objAgg : lstIndicativeValues) {
            if(objAgg.get('CN') == cpf.Concept_Note__c) {
                    
                    System.Debug('%%% CN: ' + objAgg.get('CN'));
                    System.Debug('%%% objAgg: ' + objAgg);                     
                    cpf.Above_Indicative_Y1__c = (Decimal)objAgg.get('AY1');
                    cpf.Above_Indicative_Y2__c= (Decimal)objAgg.get('AY2');
                    cpf.Above_Indicative_Y3__c= (Decimal)objAgg.get('AY3');
                    cpf.Above_Indicative_Y4__c= (Decimal)objAgg.get('AY4');
                    cpf.Indicative_Y1__c = (Decimal)objAgg.get('Y1');
                    cpf.Indicative_Y2__c = (Decimal)objAgg.get('Y2');
                    cpf.Indicative_Y3__c = (Decimal)objAgg.get('Y3');
                    cpf.Indicative_Y4__c = (Decimal)objAgg.get('Y4');
                    
                    if(cpf.Indicative_Y1__c == null) cpf.Indicative_Y1__c = 0;
                    if(cpf.Indicative_Y2__c == null) cpf.Indicative_Y2__c = 0;
                    if(cpf.Indicative_Y3__c == null) cpf.Indicative_Y3__c = 0;
                    if(cpf.Indicative_Y4__c == null) cpf.Indicative_Y4__c  = 0;
                    if(cpf.Above_Indicative_Y1__c == null) cpf.Above_Indicative_Y1__c  = 0;
                    if(cpf.Above_Indicative_Y2__c  == null) cpf.Above_Indicative_Y2__c = 0;
                    if(cpf.Above_Indicative_Y3__c == null) cpf.Above_Indicative_Y3__c = 0;
                    if(cpf.Above_Indicative_Y4__c  == null) cpf.Above_Indicative_Y4__c = 0;
                    
                    break;
                }
          }          
     }
     update lstCPFReport; */
}