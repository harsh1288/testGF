/* Purpose: Trigger for calculation of amount fields in Budget framework
* Created Date: 04th-Apr-2015
* Created By: RK
* Updated On:
*/
trigger BudgetLineTrigger on Budget_Line__c (after insert,after update) 
{
    List<Budget_Line__c> extIdnotUpdatedList = new List<Budget_Line__c>();
    if( Trigger.isAfter && Trigger.isUpdate ){
        for(Budget_Line__c b:trigger.new)
        {
            if( !(b.trackField__c != Trigger.oldMap.get(b.Id).trackField__c) ){
                if(b.Budget_Line_ETL_ID__c !=null)
                {
                    extIdnotUpdatedList.add(b);
                }
            }
        }
    }
    
    if( Trigger.isAfter && Trigger.isInsert ){
        for(Budget_Line__c b:trigger.new)
        {
            System.debug('--b.Budget_Line_ETL_ID__c---'+b.Budget_Line_ETL_ID__c);
            if(b.Budget_Line_ETL_ID__c !=null)
            {
                extIdnotUpdatedList.add(b);
            }
            
        }
    }
    if(extIdnotUpdatedList.size()>0)
    {   
        System.debug('=====>'+StopRecursion.hasalreadyRunAfterUpdateBL());
        if(!StopRecursion.hasalreadyRunAfterUpdateBL())
        {
            StopRecursion.setalreadyRunAfterUpdateBL();
            BudgetLineHandler.CalculateBudgetTrackerAfterInsert(extIdnotUpdatedList);            
        }
    }
}