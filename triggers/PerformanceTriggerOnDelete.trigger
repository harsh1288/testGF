trigger PerformanceTriggerOnDelete on Performance_Framework__c (before delete) {
    
    Set<Id> pfid = new Set<Id>();
    List<Module__c> lstModule = new List<Module__c>();
    List<Grant_Indicator__c> lstGI = new List<Grant_Indicator__c>();
    List<Grant_Intervention__c> lstGInt = new List<Grant_Intervention__c>();
    
    
    For(Performance_Framework__c pf: trigger.old){
        pfid.add(pf.id);
    }
    
    lstModule = [SELECT id, PF_is_Null__c 
                FROM Module__c 
                WHERE Performance_Framework__c IN: pfid AND PF_is_Null__c = true];
                
    lstGI = [SELECT id, Is_IP_Coverage_Indicator__c 
            FROM Grant_Indicator__c 
            WHERE Performance_Framework__c IN: pfid AND Is_IP_Coverage_Indicator__c = true];
            
    lstGInt = [SELECT id 
              FROM Grant_Intervention__c 
              WHERE Performance_Framework__c IN: pfid AND Module__r.PF_is_Null__c = true];
    
    
    try{
        if(lstModule.size() > 0 && lstModule != null){
            delete lstModule;
        }
        
        if(lstGI.size() > 0 && lstGI != null){
            delete lstGI;
        }
        
        if(lstGI.size() > 0 && lstGI != null){
            delete lstGInt;
        }
    }
    catch(Exception e){
        System.Debug(e.getMessage());
    }
    
}