trigger trgPerformanceFramework on Performance_Framework__c (before insert, before update,after insert,after update,before delete) {
    
     if(trigger.isAfter)
        {
         if(trigger.isUpdate) 
            {  
                 if(checkRecursive.runOnce()  || Test.isRunningTest()){
                     PerformanceFrameworkTriggerHandler.changeRecordTypePF(trigger.new,trigger.oldMap); 
                     PerformanceFrameworkTriggerHandler.sendMailtoPR(trigger.new,trigger.oldMap);
                    
                     PerformanceFrameworkTriggerHandler.changeKeyActivityRecordType(trigger.newmap,trigger.oldmap); 
                 }
                                                            
            }
         }
     if(trigger.isBefore)
         {
            if(trigger.isDelete)
            {
                PerformanceFrameworkTriggerHandler.PerformanceTriggerOnDelete(trigger.new,trigger.old);
            }     
     
         }
}