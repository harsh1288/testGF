/*
* $Author:      $ TCS Developer
* $Description: $ Trigger for Implementer object
* $Date:        $ 07-Nov-2014
* $Revision:    $ 
*/

trigger ImplementerTrigger on Implementer__c (before insert, before update, before delete, after insert, after update, after delete)
{    
        if(Trigger.isBefore){
            if (Trigger.isDelete){
                List<sObject> sObjectList = new List<sObject>();
                for( Implementer__c  rec : Trigger.old)
                    sObjectList.add((sObject)rec );
                PFChangesTrackerHandler.CheckPFField(sObjectList);
            }
        
            if ( Trigger.isInsert )
            {  
                
                List<sObject> sObjectList = new List<sObject>();
                for( Implementer__c  rec : Trigger.new ){
                
                    sObjectList.add((sObject)rec );
                }
                PFChangesTrackerHandler.CheckPFField(sObjectList);
           
            }
            else if ( Trigger.isUpdate )
            {
                List<sObject> sObjectList = new List<sObject>();
                for( Implementer__c  rec : Trigger.new ){
                 if(Trigger.oldMap.get(rec.Id).RecordTypeId == rec.RecordTypeId)
                    sObjectList.add((sObject)rec );
                }    
                PFChangesTrackerHandler.CheckPFField(sObjectList);
       
            }
            
        }
}