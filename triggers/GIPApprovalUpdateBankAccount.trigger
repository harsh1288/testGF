/*************************************
A trigger to update the validity period start and end dates on the Bank Account
when GIPs are created for the first time, or when they are approved.

Created by Matthew Miller
*************************************/

trigger GIPApprovalUpdateBankAccount on Implementation_Period__c (after insert, after update) {

/*Set<Id>BankAccountIds = new Set<Id>();
Set<Id>GrantIds = new Set<Id>();

for(Implementation_Period__c gip : trigger.New){
if(trigger.isInsert){
if((gip.Approval_Status__c == 'Approved')||  
      (gip.Bank_Account__c != null )){
           BankAccountIds.add(gip.Bank_Account__c); }
}
if(trigger.isUpdate){
 if((gip.Approval_Status__c == 'Approved' && Trigger.oldMap.get(gip.Id).Approval_Status__c != 'Approved') || 
      (gip.Bank_Account__c != null && gip.Bank_Account__c != Trigger.oldMap.get(gip.Id).Bank_Account__c)){
           BankAccountIds.add(gip.Bank_Account__c); }
}
}
 
 /*  if((gip.Approval_Status__c == 'Approved' && Trigger.oldMap.get(gip.Id).Approval_Status__c != 'Approved') || 
      (gip.Bank_Account__c != null && gip.Bank_Account__c != Trigger.oldMap.get(gip.Id).Bank_Account__c)){
           BankAccountIds.add(gip.Bank_Account__c); }*/

    /*List<Bank_Account__c> lstBankAccounts = [Select Id, Bank_Account_Validity_Period_End_Date__c,
                         (Select Id, Start_Date__c, End_Date__c, Approval_Status__c from Implementation_Periods__r                          
                         WHERE Id in: trigger.New) 
                         from Bank_Account__c where Id in :BankAccountIds AND Locked__c = false];
                         
                         
       for(Bank_Account__c ba : lstBankAccounts){
         for(Implementation_Period__c baGIP: ba.Implementation_Periods__r){
             if(ba.Implementation_Periods__r.size()== 1){
                 ba.Bank_Account_Validity_Period_Start_Date__c = baGIP.Start_Date__c;
                 ba.Bank_Account_Validity_Period_End_Date__c = baGIP.End_Date__c; }
             if(baGIP.Approval_Status__c == 'Approved' &&(baGIP.End_Date__c > ba.Bank_Account_Validity_Period_End_Date__c  || ba.Bank_Account_Validity_Period_End_Date__c  == null)){
                 ba.Bank_Account_Validity_Period_End_Date__c = baGIP.End_Date__c; }
           }
       }
       
      update lstBankAccounts;*/
       
}