trigger UpdateExchRate on Country__c (After Update) {
    
   
    Decimal USD;
    Decimal EURO;
    boolean flag = false;
    list<Country__c> updateList = new List<Country__c>();
    List<Country__c> ExchrateCon = [Select id,Name,X200DEMA_Exch_Rate_in_USD__c,X200DEMA_Exch_Rate_in_EUR__c From Country__c];
    /*for(Country__c c: ExchrateCon){
        if(c.Name == Label.Country_EURO && trigger.newMap.get(c.id).X200DEMA_Exch_Rate_in_EUR__c != trigger.oldMap.get(c.id).X200DEMA_Exch_Rate_in_EUR__c ){
            EURO = trigger.newMap.get(c.id).X200DEMA_Exch_Rate_in_EUR__c ; 
            system.debug('Debug1'+Euro+' id '+c.id);
        }    
        if(c.Name == Label.Country_USD && trigger.newMap.get(c.id).X200DEMA_Exch_Rate_in_USD__c != trigger.oldMap.get(c.id).X200DEMA_Exch_Rate_in_USD__c ){ 
            USD = trigger.newMap.get(c.id).X200DEMA_Exch_Rate_in_USD__c ; 
            system.debug('Debug2'+USD+' id= '+c.id);   
        }        
    }*/
   
   
    
    for(Country__c c: Trigger.new){
        if(c.Name == Label.Country_EURO && c.X200DEMA_Exch_Rate_in_USD__c != trigger.oldMap.get(c.id).X200DEMA_Exch_Rate_in_USD__c ){
            EURO = c.X200DEMA_Exch_Rate_in_USD__c ; 
        }    
        if(c.Name == Label.Country_USD && c.X200DEMA_Exch_Rate_in_EUR__c != trigger.oldMap.get(c.id).X200DEMA_Exch_Rate_in_EUR__c){ 
            USD = c.X200DEMA_Exch_Rate_in_EUR__c ;   
        }
             
    }
    
    
    if(EURO != NULL || USD != NULL){ 
        for(Country__c con: ExchrateCon){
            if(EURO != NULL)
                con.USD_to_Euro_Exchange_rate__c = EURO;
            if(USD != NULL)    
            con.Euro_to_USD_Exchange_rate__c = USD;  
            
            updateList.add(con);        
        }
        
        if(updateList!= NULL)
            update updateList;
    }        
}