/* Purpose: Trigger for calculation of Cost fields in Budget Line
* Created Date: 04th-Apr-2015
* Created By: RK
* Updated On:
*/
trigger AssumptionTrigger on Assumption__c (after insert,after update) 
{
    Map<Id,Assumption__c> extIdnotnullMap = new Map<Id,Assumption__c>();
    for(Assumption__c a:trigger.new)
    {
        if(a.Assumption_ETL_ID__c !=null)
        {
           // extIdnotnullMap.put(a.Id,a);
        }
    }
    if(extIdnotnullMap.size()>0)
    {
        if(!StopRecursion.hasalreadyRunAfterUpdateAssump())
        {
            //StopRecursion.setalreadyRunAfterUpdateAssump();
            //AssumptionHandler.CalculateCostAfterInsert(extIdnotnullMap);            
        }
    }
}