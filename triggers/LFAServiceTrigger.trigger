/**********************************************************************************
* Trigger: {LFAServiceTrigger}
* Created by {},{DateCreated 02/25/2014}
----------------------------------------------------------------------------------
*/

trigger LFAServiceTrigger on LFA_Service__c (after insert,after update) {
    if(Trigger.isUpdate){
        //On Update Call The Handler Method.
        System.debug('In Trigger' +Trigger.New );
        LFAServiceHandler.UpdateServiceResourceLOE(Trigger.New,Trigger.Oldmap);
        if(LFAServiceHandler.blnStopRecursion == false)
            LFAServiceHandler.aiauUpdateLFAService(Trigger.New,Trigger.Oldmap);
        
        LFAServiceHandler.UpdateServiceResourceStatus(Trigger.New,Trigger.Oldmap);
       
        System.debug('Trigger Completed');
    }
    if(Trigger.isInsert) {
        System.debug('In Trigger' +Trigger.New );
        
        LFAServiceHandler.aiauUpdateLFAService(Trigger.New,null);
        System.debug('Trigger Completed');
    }
}