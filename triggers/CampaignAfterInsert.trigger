trigger CampaignAfterInsert on Campaign (after insert) {

    Set<Id> setCampIds = new Set<Id>();
    RecordType EventRT = [Select id,name from RecordType where Name=: 'Event' AND SobjectType=: 'Campaign'];
    RecordType EmailRT = [Select id,name from RecordType where Name=: 'Email' AND SobjectType=: 'Campaign'];
  
    for(Campaign camp : Trigger.new){   
        setCampIds.add(camp.Id);  
    }
       
    List<CampaignMemberStatus> lstCMStatusToDelete = [Select Id from CampaignMemberStatus where CampaignId in: setCampIds AND Label != 'Sent'];
    delete lstCMStatusToDelete;
      
    
    for(Campaign camp : Trigger.new){ 
        List<CampaignMemberStatus> lstCMStatus = new List<CampaignMemberStatus>(); 
        if(camp.recordtypeid == EventRT.id){
            lstCMStatus.add(new CampaignMemberStatus(CampaignId = camp.Id, Label = 'Invitation Not Sent', HasResponded = False, isDefault=True, sortOrder=3));
            lstCMStatus.add(new CampaignMemberStatus(CampaignId = camp.Id, Label = 'Response Pending', HasResponded = False, isDefault=false, sortOrder=4));
            lstCMStatus.add(new CampaignMemberStatus(CampaignId = camp.Id, Label = 'Accepted', HasResponded = true, isDefault=false, sortOrder=5));
            lstCMStatus.add(new CampaignMemberStatus(CampaignId = camp.Id, Label = 'Accepted – Requested Guests', HasResponded = true, isDefault=false, sortOrder=6));
            lstCMStatus.add(new CampaignMemberStatus(CampaignId = camp.Id, Label = 'Declined', HasResponded = true, isDefault=false, sortOrder=7));
            lstCMStatus.add(new CampaignMemberStatus(CampaignId = camp.Id, Label = 'No-Show', HasResponded = true, isDefault=false, sortOrder=8));
        } 
        
        if(camp.recordtypeid == EmailRT.id){
            lstCMStatus.add(new CampaignMemberStatus(CampaignId = camp.Id, Label = 'Not Sent', HasResponded = False, isDefault=false, sortOrder=3));
            lstCMStatus.add(new CampaignMemberStatus(CampaignId = camp.Id, Label = 'Sent', HasResponded = False, isDefault=false, sortOrder=4));
            lstCMStatus.add(new CampaignMemberStatus(CampaignId = camp.Id, Label = 'Received', HasResponded = True, isDefault=false, sortOrder=5));
        }   
    
    for(CampaignMemberStatus cmStat : lstCMStatus) {
        try{    
        insert lstCMStatus; }
        catch(Exception ex) {
            System.debug('%%%' + ex);  }
      }  
   }
}