trigger Create_Branch_and_Bank on Bank_Account__c (before update) {
/*
    Written by Matthew Miller, February 1 2014
    This trigger is used for inserting bank and branch records when a bank account is approved by Oracle.
    1. If the bank account already has a bank branch in the Bank__c field, nothing will happen.
    2. If the bank account has no bank branch:   
        a. If an existing bank has not been chosen, the trigger will create a new bank (Bank__c), link it
           to the existing country (Id stored by selectList in Selected_Country_Id__c), then insert that bank
        b. The trigger will create a new branch (also a Bank__c) and link it either to an existing bank
           (Id stored by selectList in Selected_Bank_Id__c) or to the newly inserted bank.
        c. The Id of the new branch will be stored in the Bank__c lookup on the Bank Account
*/

for(Bank_Account__c ba : Trigger.new){

String newBankId;
String newBranchId;

if(ba.Approval_Status__c == 'Approved' && System.Trigger.oldMap.get(ba.Id).Approval_Status__c != 'Approved'){

    //1. Existing branch
    if(ba.Bank__c != null){
    /*do nothing*/}
    
    //2. No existing branch
    else if(ba.Bank__c == null){
    
        //a. New bank
         if(ba.Selected_Bank_Id__c == 'New' && ba.Selected_Country_Id__c != 'New' && ba.Selected_Country_Id__c != 'none'){
             Bank__c newBank = new Bank__c();
               newBank.Name__c = ba.New_Bank_Name__c;
               newBank.Country__c = ba.Selected_Country_Id__c;
               newBank.GFS_Bank_ID__c = ba.GFS_Bank_ID__c;
               insert newBank;
               newBankId = newBank.Id;  }
             
        //b. New branch
        if(ba.Selected_Branch_Id__c == 'New'){
            Bank__c newBranch = new Bank__c();
              if(ba.Selected_Bank_Id__c != 'New' && ba.Selected_Bank_Id__c != 'none'){           
                  newBranch.Bank__c = ba.Selected_Bank_Id__c;}
                  else{ newBranch.Bank__c = newBankId; }                 
              newBranch.SWIFT_BIC_Code__c = ba.SWIFT_BIC_Code__c;
              newBranch.ABA_Code__c = ba.ABA_Code__c ;
              newBranch.GFS_Branch_ID__c = ba.GFS_Branch_ID__c;
            insert newBranch; 
            newBranchId = newBranch.Id;  }
            
        //c. Store branch in Bank Account Bank__c lookup
            ba.Bank__c = newBranchId;
            ba.Selected_Bank_Id__c = newBankId;
            ba.Selected_Branch_Id__c = newBranchId; 
            }

        }

    }
    
}