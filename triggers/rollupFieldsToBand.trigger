trigger rollupFieldsToBand on Concept_Note__c (after insert, after update, after delete, after undelete) {

List<Concept_Note__c> cnList = (Trigger.isDelete) ? Trigger.old : Trigger.new;

Set<Id> bandIds = new Set<Id>();

for(Concept_Note__c cn : cnList)
 {
    bandIds.add(cn.Band__c);  
 }
    
List<Band__c> bandList = [Select Id, Sum_of_Risk_Adjustments__c, Sum_of_Final_Allocations__c, Sum_of_Ceilings__c, Sum_of_Eligible_Risk_Adjustments__c, Sum_of_NFAs_not_equal_to_EF__c, Sum_of_Eligible_NFAs__c, Sum_of_Ineligible_NFAs__c, Sum_of_P_I_IR_Adjusted_Notional_Funding__c, Sum_of_NFAs__c, Sum_of_Hard_Floors__c, Sum_of_Soft_Floors__c, Sum_of_NAAs__c, (Select Id, Risk_Adjustment__c, P_I_IR_Adjusted_Notional_Funding__c, NFA_EF__c, NFA__c, Hard_Floor__c, Soft_Floor__c, NAA__c, Eligible_for_Rescaling__c, Final_Allocation__c, Existing_Funding__c, Funding_Ceiling__c from Concept_Notes__r) from Band__c where Id in :bandIds];
 
    for(Band__c band : bandList)
    {
        Decimal countRA = 0;
        for(integer i = 0; i < band.Concept_Notes__r.size(); i++)
           { if(band.Concept_Notes__r[i].Risk_Adjustment__c <> null)
            { countRA = countRA + band.Concept_Notes__r[i].Risk_Adjustment__c; }
        } band.Sum_of_Risk_Adjustments__c = countRA;
        
        Decimal countERA = 0;
        for(integer i = 0; i < band.Concept_Notes__r.size(); i++)
           { if(band.Concept_Notes__r[i].Risk_Adjustment__c <> null && band.Concept_Notes__r[i].Eligible_for_Rescaling__c == 'Yes')
            { countERA = countERA + band.Concept_Notes__r[i].Risk_Adjustment__c; }
        } band.Sum_of_Eligible_Risk_Adjustments__c = countERA;
        
        Decimal countNFAEF = 0;
        for(integer i = 0; i < band.Concept_Notes__r.size(); i++)
           { if(band.Concept_Notes__r[i].NFA__c <> null && band.Concept_Notes__r[i].Eligible_for_Rescaling__c == 'Yes' && band.Concept_Notes__r[i].NFA_EF__c == False)
            { countNFAEF = countNFAEF + band.Concept_Notes__r[i].NFA__c; }
        } band.Sum_of_NFAs_not_equal_to_EF__c = countNFAEF;
        
        Decimal countFC = 0;
        for(integer i = 0; i < band.Concept_Notes__r.size(); i++)
           { if(band.Concept_Notes__r[i].Funding_Ceiling__c <> null && band.Concept_Notes__r[i].Eligible_for_Rescaling__c == 'Yes')
            { countFC = countFC + band.Concept_Notes__r[i].Funding_Ceiling__c; }
        } band.Sum_of_Ceilings__c = countFC;
        
        Decimal countNE = 0;
        for(integer i = 0; i < band.Concept_Notes__r.size(); i++)
           { if(band.Concept_Notes__r[i].NFA__c <> null && band.Concept_Notes__r[i].Eligible_for_Rescaling__c == 'No')
            { countNE = countNE + band.Concept_Notes__r[i].NFA__c; }
        } band.Sum_of_Ineligible_NFAs__c = countNE;
        
        Decimal countE = 0;
        for(integer i = 0; i < band.Concept_Notes__r.size(); i++)
           { if(band.Concept_Notes__r[i].NFA__c <> null && band.Concept_Notes__r[i].Eligible_for_Rescaling__c == 'Yes')
            { countE = countE + band.Concept_Notes__r[i].NFA__c; }
        } band.Sum_of_Eligible_NFAs__c = countE;
        
        Decimal countANF = 0;
        for(integer i = 0; i < band.Concept_Notes__r.size(); i++)
           { if(band.Concept_Notes__r[i].P_I_IR_Adjusted_Notional_Funding__c <> null) 
            { countANF = countANF + band.Concept_Notes__r[i].P_I_IR_Adjusted_Notional_Funding__c; }
        } band.Sum_of_P_I_IR_Adjusted_Notional_Funding__c = countANF;
        
        Decimal countFA = 0;
        for(integer i = 0; i < band.Concept_Notes__r.size(); i++)
           { if(band.Concept_Notes__r[i].Final_Allocation__c <> null) 
            { countFA = countFA + band.Concept_Notes__r[i].Final_Allocation__c; }
        } band.Sum_of_Final_Allocations__c = countFA;
        
        Decimal countNFA = 0;
        for(integer i = 0; i < band.Concept_Notes__r.size(); i++)
           { if(band.Concept_Notes__r[i].NFA__c <> null)
            { countNFA = countNFA + band.Concept_Notes__r[i].NFA__c; }
        } band.Sum_of_NFAs__c = countNFA;
        
         Decimal countHF = 0;
        for(integer i = 0; i < band.Concept_Notes__r.size(); i++)
           { if(band.Concept_Notes__r[i].Hard_Floor__c <> null && band.Concept_Notes__r[i].Eligible_for_Rescaling__c == 'Yes')
            { countHF = countHF + band.Concept_Notes__r[i].Hard_Floor__c; }
        } band.Sum_of_Hard_Floors__c = countHF;
        
         Decimal countSF = 0;
        for(integer i = 0; i < band.Concept_Notes__r.size(); i++)
           { if(band.Concept_Notes__r[i].Soft_Floor__c <> null && band.Concept_Notes__r[i].Eligible_for_Rescaling__c == 'Yes')
            { countSF = countSF + band.Concept_Notes__r[i].Soft_Floor__c; }
        } band.Sum_of_Soft_Floors__c = countSF;
        
        Decimal countNAA = 0;
        for(integer i = 0; i < band.Concept_Notes__r.size(); i++)
           { if(band.Concept_Notes__r[i].NAA__c <> null)
            { countNAA = countNAA + band.Concept_Notes__r[i].NAA__c; }
        } band.Sum_of_NAAs__c = countNAA;
        
    }

update bandList;

}