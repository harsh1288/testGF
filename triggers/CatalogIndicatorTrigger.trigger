/*
* $Author:      $ TCS - Rahul Kumar
* $Description: $ Trigger for Catalog Indicator
* $Date:        $ 31/10/2014
* $Revision:    $
*/

trigger CatalogIndicatorTrigger on Indicator__c (before insert, before update, before delete, after insert, after update, after delete)
{
   /* if ( Trigger.isBefore )
    {
        if ( Trigger.isInsert )
        {
        }
        else if ( Trigger.isUpdate )
        {                  
        }
        else if ( Trigger.isDelete )
        {        
        }
    }
    else if ( Trigger.isAfter )
    {
        if ( Trigger.isInsert )
        {            
        }
        else if ( Trigger.isUpdate )
        {
            List<Indicator__c> uncheckedCIList = new List<Indicator__c>();
            for(Indicator__c ci:Trigger.new)
            {
                //filter to only those records for which Disaggregated has been unchecked 
               if(Trigger.OldMap.get(ci.id).Is_Disaggregated__c == true && Trigger.NewMap.get(ci.id).Is_Disaggregated__c == false)
                {
                    uncheckedCIList.add(ci);
                }
                //call method here
                CatalogIndicatorHandler.onDisaggregationUncheck(uncheckedCIList);
            }   
        }
        else if ( Trigger.isDelete )
        {
        }
    }*/
}