trigger updateRelatedAccountOwners on Country__c (after update) {

Set<Id> countryIds = new Set<Id>();
Id prID = [Select ID from RecordType where SObjectType = 'Account' and Name = 'PR'].Id;
List<Account> lstAccts = new List<Account>();

for(Country__c co: Trigger.new){

    if(co.FPM__c != System.Trigger.oldMap.get(co.Id).FPM__c){
        countryIds.add(co.Id); }
    }
 if(countryIds.size()>0){   
    List<Country__c> lstCountries = [Select Id, FPM__c, (Select Id, RecordTypeId, OwnerId from Accounts__r WHERE Locked__c = false) from Country__c WHERE Id in :countryIds AND FPM__c!=null];
    for(Country__c co: lstCountries){
        for(Account acct : co.Accounts__r){
        if(acct.RecordTypeId == prID){
         acct.OwnerId = co.FPM__c; }
        }
        //update co.Accounts__r;
        lstAccts.addAll(co.Accounts__r);
    }
    update lstAccts;
}
}