trigger PCNSubmittedAfterUpdate on Concept_Note__c (after Update) {
/*Map<Id,List<PCN_Criteria__c>> mapContactIdAndPartner = new Map<Id,List<PCN_Criteria__c>>();
    for(PCN_Criteria__c obj :[select Id,Name,Status__c,Concept_Note__c from PCN_Criteria__c where Concept_Note__c in : trigger.newmap.keyset()]){
if(mapContactIdAndPartner.get(obj.Concept_Note__c) == null){
mapContactIdAndPartner.put(obj.Concept_Note__c,new List<PCN_Criteria__c>());
}
mapContactIdAndPartner.get(obj.Concept_Note__c).add(obj);
}*/
List<PCN_Criteria__c> lstToUpdate = new List<PCN_Criteria__c>();
for(Concept_Note__c c : trigger.new){
    lstToUpdate = [Select Id,Name,Status__c,Concept_Note__c from PCN_Criteria__c where Concept_Note__c =:c.Id and Is_Perf_Imp_Inc_Risk__c = true];
    if(c.P_I_IR_Risk_Submitted_Final__c == true){
    for(PCN_Criteria__c obj :lstToUpdate){
        If(obj.Status__c == 'Under CT Review'){
            obj.Status__c = 'Accepted';
        }    
update lstToUpdate;
    }
    }}
}