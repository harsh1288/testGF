trigger Share_with_CT on LFA_Work_Plan__c (after insert, after update) {

    // We execute the trigger after a WP record has been inserted or updated
    // because we need the Id of the WP record to already exist.
     
    // LFA_Work_Plan__Share is the "Share" table that was created when the
    // Organization Wide Default sharing setting is "Private".
    // Allocate storage for a list of LFA_Work_Plan__Share records.
    List<LFA_Work_Plan__Share> wpShares  = new List<LFA_Work_Plan__Share>();
    
    // For each of the WP records being inserted, do the following:
    //*********** Start: LFA Mannual sharing********************************//
    Set<ID> setAccountId = new Set<Id>();
    Set<ID> setOldAccountId = new Set<Id>();
    Set<ID> setWPId = new Set<ID>();
    Set<ID> setOldWPId = new Set<ID>();
    //*********** End: LFA Mannual sharing********************************//
    for(LFA_Work_Plan__c WP : trigger.new){

        // Create a new LFA_Work_Plan__Share record to be inserted in to the LFA_Work_Plan_Share table.
        LFA_Work_Plan__Share ctShare = new LFA_Work_Plan__Share();
            
        // Populate the LFA_Work_Plan__Share record with the ID of the record to be shared.
        ctShare.ParentId = WP.Id;
            
        // Then, set the ID of user or group being granted access. In this case,
        // we’re setting the Id of the Hiring Manager that was specified by 
        // the CT ID formula field, which brings in the value of the CT ID on Country
        ctShare.UserOrGroupId = wp.ct_id__c; 
            
        // CT should have R/W if the box is ticked
        if(wp.Country_Team_can_edit__c==TRUE){
            ctShare.AccessLevel = 'edit';}
        // CT should have read-only if the box is not ticked
        if(wp.Country_Team_can_edit__c==FALSE){
            ctShare.AccessLevel = 'read';}
            
        // Specify that the reason
        ctShare.RowCause = Schema.LFA_Work_Plan__Share.RowCause.Share_with_Country_Team__c;
            
        // Add the new Share record to the list of new Share records.
        wpShares.add(ctShare);
        
        //*********** Start: LFA Mannual sharing********************************//
        if(Trigger.isInsert && WP.LFA__c != null && WP.LFA_Access__c != null && WP.LFA_Access__c != 'No Access'){
            setAccountId.add(WP.LFA__c);
            setWPId.add(WP.ID);
        }else if(Trigger.isUpdate && 
            (WP.LFA__c != Trigger.oldmap.get(WP.Id).LFA__c || WP.LFA_Access__c != Trigger.oldmap.get(WP.Id).LFA_Access__c)){
            if(WP.LFA__c != null && WP.LFA_Access__c != null && WP.LFA_Access__c != 'No Access'){
                setAccountId.add(WP.LFA__c);
                setWPId.add(WP.ID);
            }
            //to pull away access rights from old LFA and its parent
            setOldAccountId.add(Trigger.oldmap.get(WP.Id).LFA__c);
            setOldWPId.add(WP.ID);
        }
        //*********** End: LFA Mannual sharing********************************//
        
    }
        
    // Insert all of the newly created Share records and capture save result 
    Database.SaveResult[] wpShareInsertResult = Database.insert(wpShares,false);
    // Error handling code omitted for readability.
    //*********** Start: LFA Mannual sharing********************************//
    if(setOldAccountId.size()>0){
        List<Account> lstOldAccount = [Select ParentId From Account Where Id In : setOldAccountId];
        Set<Id> SetOldParentAccId = new Set<id>();
        Set<Id> SetAccountIdOldTemp = new Set<id>();
        for(Account objOldAcc : lstOldAccount ){
            SetOldParentAccId.add(objOldAcc.ParentId);
            SetAccountIdOldTemp.add(objOldAcc.ParentId);
        }
        
        lstOldAccount = [Select ParentId From Account Where Id In : SetAccountIdOldTemp];
        SetAccountIdOldTemp = new Set<id>();
        for(Account objOldAcc : lstOldAccount ){
            SetOldParentAccId.add(objOldAcc.ParentId);
            SetAccountIdOldTemp.add(objOldAcc.ParentId);
        }
        
        lstOldAccount = [Select ParentId From Account Where Id In : SetAccountIdOldTemp];
        SetAccountIdOldTemp = new Set<id>();
        for(Account objOldAcc : lstOldAccount ){
            SetOldParentAccId.add(objOldAcc.ParentId);
            SetAccountIdOldTemp.add(objOldAcc.ParentId);
        }
        
        lstOldAccount = [Select ParentId From Account Where Id In : SetAccountIdOldTemp];
        SetAccountIdOldTemp = new Set<id>();
        for(Account objOldAcc : lstOldAccount){
            SetOldParentAccId.add(objOldAcc.ParentId);
            SetAccountIdOldTemp.add(objOldAcc.ParentId);
        }
        
        system.debug('$$$$'+SetOldParentAccId);
        List<User> lstOldUser = [Select Id,Contact.AccountId From user Where Contact.AccountId IN : setOldAccountId OR Contact.AccountId IN : SetOldParentAccId];
        List<LFA_Work_Plan__c> lstOldWP = [select id,LFA__c,LFA__r.ParentID from LFA_Work_Plan__c where id IN: setOldWPId];
        List<LFA_Work_Plan__Share> lstOldWPSharesLFA = [select id,ParentId, UserOrGroupId
                                                        from LFA_Work_Plan__Share 
                                                        where ParentId IN: setOldWPId and 
                                                        UserOrGroupId IN: lstOldUser];
        IF(lstOldWPSharesLFA.size()>0){
            Database.DeleteResult[] wpOldShareDeleteResult = Database.delete(lstOldWPSharesLFA,false);   
        }
    }
    if(setAccountId.size() > 0){
        
        List<LFA_Work_Plan__Share> lstWPSharesLFA = new List<LFA_Work_Plan__Share>();
        List<Account> lstAccount = [Select ParentId From Account Where Id In : setAccountId];
        Set<Id> SetParentAccId = new Set<id>();
        Set<Id> SetAccountIdTemp = new Set<id>();
        for(Account objAcc : lstAccount ){
            if(objAcc.ParentId != null){
                SetParentAccId.add(objAcc.ParentId);
                SetAccountIdTemp.add(objAcc.ParentId);
            }
        }
        lstAccount = [Select ParentId From Account Where Id In : SetAccountIdTemp];
        SetAccountIdTemp = new Set<id>();
        for(Account objAcc : lstAccount ){
            if(objAcc.ParentId != null){
                SetParentAccId.add(objAcc.ParentId);
                SetAccountIdTemp.add(objAcc.ParentId);
            }
        }
        
        lstAccount = [Select ParentId From Account Where Id In : SetAccountIdTemp];
        SetAccountIdTemp = new Set<id>();
        for(Account objAcc : lstAccount ){
            if(objAcc.ParentId != null){
                SetParentAccId.add(objAcc.ParentId);
                SetAccountIdTemp.add(objAcc.ParentId);
            }
        }
        
        lstAccount = [Select ParentId From Account Where Id In : SetAccountIdTemp];
        SetAccountIdTemp = new Set<id>();
        for(Account objAcc : lstAccount ){
            if(objAcc.ParentId != null){
                SetParentAccId.add(objAcc.ParentId);
                SetAccountIdTemp.add(objAcc.ParentId);
            }
        }
        
        system.debug('$%SetParentAccId$%$%'+SetParentAccId);
        system.debug('$%setAccountId$%$%'+setAccountId);
        List<User> lstUser = [Select Id,Contact.AccountId From user Where Contact.AccountId IN : setAccountId OR Contact.AccountId IN : SetParentAccId];
        List<LFA_Work_Plan__c> lstWP = [select id,LFA_Access__c,LFA__c,LFA__r.ParentID,LFA__r.Parent.ParentID,LFA__r.Parent.Parent.ParentID from LFA_Work_Plan__c where id IN: setWPId];
        system.debug('$%lstUser$%$%'+lstUser);
        system.debug('$%lstWP$%$%'+lstWP);
        if(lstUser.size() > 0){
            LFA_Work_Plan__Share ctShareLFA;
            for(user User : lstUser){
                for(LFA_Work_Plan__c objWP : lstWP){
                    if(User.Contact.AccountId == objWp.LFA__c || User.Contact.AccountId == objWp.LFA__r.ParentID 
                        || User.Contact.AccountId == objWp.LFA__r.Parent.ParentID || User.Contact.AccountId == objWp.LFA__r.Parent.Parent.ParentID){
                        ctShareLFA = new LFA_Work_Plan__Share();
                        ctShareLFA.ParentId = objWp.Id;        
                        ctShareLFA.UserOrGroupId = User.ID;         
                        if(objWp.LFA_Access__c == 'Read Access'){
                            ctShareLFA.AccessLevel = 'read';
                        }else if(objWp.LFA_Access__c == 'Read/Write Access'){
                            ctShareLFA.AccessLevel = 'edit';
                        }
                        if(User.Contact.AccountId == objWp.LFA__c){
                            ctShareLFA.RowCause = Schema.LFA_Work_Plan__Share.RowCause.LFA_Account_Sharing__c;
                        }else{
                            ctShareLFA.RowCause = Schema.LFA_Work_Plan__Share.RowCause.Parent_LFA_Account_Sharing__c;
                        }
                        lstWPSharesLFA.add(ctShareLFA);
                        
                    }
                }
            } 
            if(lstWPSharesLFA.size() > 0){
                system.debug('$%lstWPSharesLFA$%$%'+lstWPSharesLFA);
                Database.SaveResult[] wpLFAShareInsertResult = Database.insert(lstWPSharesLFA,false);   
            }
        }
    }
    //*********** End: LFA Mannual sharing********************************//
    
}