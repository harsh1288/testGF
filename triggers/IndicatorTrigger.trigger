trigger IndicatorTrigger on Grant_Indicator__c (after insert, after Update, Before Delete,before insert,before update) {
     
    if(trigger.isInsert && trigger.isAfter) {
        Set<Id> grantIndicatorIdSet = new Set<Id>();
        List<Grant_Disaggregated__c> grantDisaggregatedList = new List<Grant_Disaggregated__c>();
        Grant_Disaggregated__c grantDisaggregatedRec;
        List<Catalog_Disaggregated__c> cdList;
        //RecordType NYS = [SELECT Id from RecordType where name = 'Not Yet Submitted'  AND SobjectType ='Grant_Disaggregated__c' limit 1];
        //RecordType Submitted = [SELECT Id from RecordType where name = 'Submitted to MEPH'  AND SobjectType ='Grant_Disaggregated__c' limit 1];
        
        Id NYSId = Schema.SObjectType.Grant_Disaggregated__c.getRecordTypeInfosByName().get('Not Yet Submitted').getRecordTypeId();
        Id SubmittedId = Schema.SObjectType.Grant_Disaggregated__c.getRecordTypeInfosByName().get('Submitted to MEPH').getRecordTypeId();
        
        
        cdList = [SELECT Id, Disaggregation_Category__c, Disaggregation_Value__c, Catalog_Indicator__c
                  FROM Catalog_Disaggregated__c
                  WHERE Catalog_Indicator__c in (SELECT Indicator__c
                                                 FROM Grant_Indicator__c
                                                 WHERE id in: trigger.new)
                                                 ORDER BY Disaggregation_Category__c];
        System.debug('>>>>cdList'+trigger.new);
        for(Grant_Indicator__c gd : trigger.new) {
            for(Catalog_Disaggregated__c cd : cdList) {
                if(gd.Indicator__c == cd.Catalog_Indicator__c) {
                    grantDisaggregatedRec = new Grant_Disaggregated__c();
                    grantDisaggregatedRec.Catalog_Disaggregated__c = cd.id;
                    grantDisaggregatedRec.Grant_Indicator__c = gd.id;
                    //grantDisaggregatedRec.Performance_Framework__c = gd.Performance_Framework__c;
                    if(gd.Performance_Framework__r.PF_Status__c == 'Not Yet Submitted' || gd.Performance_Framework__r.PF_Status__c == 'Sent back to PR')
                        grantDisaggregatedRec.RecordTypeId = NYSId;
                    if(gd.Performance_Framework__r.PF_Status__c == 'Submitted to MEPH Specialist')
                        grantDisaggregatedRec.RecordTypeId = SubmittedId;
                    grantDisaggregatedRec.Component__c = gd.Component__c;
                    grantDisaggregatedList.add(grantDisaggregatedRec);
                }
            }
        }        
        if(grantDisaggregatedList != null && !grantDisaggregatedList.isEmpty())
            insert grantDisaggregatedList;
            
        List<sObject> sObjectList = new List<sObject>();
        for( Grant_Indicator__c ind : Trigger.new )
            sObjectList.add((sObject)ind );
        PFChangesTrackerHandler.CheckPFField(sObjectList);
    }
    
    if( Trigger.isBefore && Trigger.isUpdate ){
        List<sObject> sObjectList = new List<sObject>();
        for( Grant_Indicator__c ind : Trigger.new ){
          if(Trigger.oldMap.get(ind.Id).RecordTypeId == ind.RecordTypeId)
            sObjectList.add((sObject)ind);
         }   
        PFChangesTrackerHandler.CheckPFField(sObjectList);

    }
    
    if( Trigger.isBefore && Trigger.isDelete){
        List<sObject> sObjectList = new List<sObject>();
        for( Grant_Indicator__c ind : Trigger.old)
            sObjectList.add((sObject)ind);
        PFChangesTrackerHandler.CheckPFField(sObjectList);

    }
}