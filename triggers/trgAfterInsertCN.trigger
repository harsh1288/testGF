trigger trgAfterInsertCN on Concept_Note__c (after insert) {
    List<Page__c> lstCNPages = new List<Page__c>();
    
    Page__c objCNPage;
    CPF_Report__c objCPF_HIV;
    CPF_Report__c objCPF_TB;
    CPF_Report__c objCPF;
    Map<ID,set<ID>> mapCNTemplateID = new Map<ID,set<ID>>();
    Map<String,set<ID>> mapCNComponentID = new Map<String,set<ID>>();  
    List<CPF_Report__c> lstCPFReports = new List<CPF_Report__c>();
   
    for(Concept_Note__c objCN:Trigger.New){
        if(objCN.Component__c!=null){
            if(mapCNComponentID.get(objCN.Component__c)==null){
                mapCNComponentID.put(objCN.Component__c,new set<ID>{objCN.ID});
            }else{
                mapCNComponentID.get(objCN.Component__c).add(objCN.ID);
            }
            
        }
        if(objCN.Page_Template__c!=null){
            if(mapCNTemplateID.get(objCN.Page_Template__c)==null){
                mapCNTemplateID.put(objCN.Page_Template__c,new set<ID>{objCN.ID});
            }else{
                mapCNTemplateID.get(objCN.Page_Template__c).add(objCN.ID);
            }
        }
        
        if(objCN.Component__c != 'HIV/TB'){
            objCPF = New CPF_Report__c();
            objCPF.Concept_Note__c = objCN.id;
            objCPF.Component__c = objCN.Component__c;
            objCPF.Country__c = objCN.CountryId__c;
            objCPF.Currency__c = objCN.CurrencyISOCode;
            objCPF.CurrencyIsoCode = objCN.CurrencyISOCode;
            objCPF.Reporting_Cycle__c = 'Calendar Year';
            objCPF.Implementation_Cycle__c = 'January - December';
            lstCPFReports.add(objCPF);
        } else {
            objCPF_HIV = New CPF_Report__c();
            objCPF_HIV.Concept_Note__c = objCN.id;
            objCPF_HIV.Component__c = 'HIV/AIDS';
            objCPF_HIV.Country__c = objCN.CountryId__c;
            objCPF_HIV.Currency__c = objCN.CurrencyISOCode;
            objCPF_HIV.CurrencyIsoCode = objCN.CurrencyISOCode;
            objCPF_HIV.Reporting_Cycle__c = 'Calendar Year';
            objCPF_HIV.Implementation_Cycle__c = 'January - December';
            lstCPFReports.add(objCPF_HIV);
            
            objCPF_TB = New CPF_Report__c();
            objCPF_TB.Concept_Note__c = objCN.id;
            objCPF_TB.Component__c = 'Tuberculosis';
            objCPF_TB.Country__c = objCN.CountryId__c;
            objCPF_TB.Currency__c = objCN.CurrencyISOCode;
            objCPF_TB.CurrencyIsoCode = objCN.CurrencyISOCode;
            objCPF_TB.Reporting_Cycle__c = 'Calendar Year';
            objCPF_TB.Implementation_Cycle__c = 'January - December';
            lstCPFReports.add(objCPF_TB);
        }
    }   
    insert lstCPFReports;
    //Query the pages from the selected template and add these pages to the list to be inserted.
     
    for(Page__c objPage: [select Id,Name,URL_Prefix__c,Template__c,Order__c,modular__c, Guidance__c, Standard_Controller__c,French_Name__c,Spanish_Name__c,Russian_Name__c
                            from Page__c 
                            where Template__c IN:mapCNTemplateID.keyset()]){
        if(mapCNTemplateID.get(objPage.Template__c)!=null){                    
            for(ID CNid : mapCNTemplateID.get(objPage.Template__c)){
                objCNPage = New Page__c();
                objCNPage.Name = objPage.Name;
                objCNPage.Concept_Note__c = CNid;
                objCNPage.URL_Prefix__c = objPage.URL_Prefix__c;
                objCNPage.Order__c = objPage.Order__c;
                objCNPage.Status__c = 'Not yet ready for review';
                objCNPage.modular__c = objpage.modular__c;
                objCNPage.Guidance__c = objpage.Guidance__c;
                objCNPage.Standard_Controller__c = objpage.Standard_Controller__c;
                objCNPage.French_Name__c = objpage.French_Name__c;
                objCNPage.Spanish_Name__c = objpage.Spanish_Name__c;
                objCNPage.Russian_Name__c = objpage.Russian_Name__c;
                lstCNPages.add(objCNPage);
            }
        }
    }
    insert lstCNPages;
    /*Query the Catalog Impact/Outcome Indicators for this Component and create Grant Indicators for this
    that link them to this CN*/
    /*List<Grant_Indicator__c> lstGrantIndicator = new List<Grant_Indicator__c>();
    Grant_Indicator__c objGrantIndicator;
    For(Indicator__c objIndicator: [Select ID, name,Type_of_Data__c,Indicator_Type__c,Programme_Area__c
                                        from Indicator__c
                                        where Programme_Area__c IN: mapCNComponentID.keyset()
                                        and Default_Indicator__c=true
                                        and (Indicator_Type__c='Outcome' or Indicator_Type__c='Impact')])
    {
        if(mapCNComponentID.get(objIndicator.Programme_Area__c)!=null){
            for(ID CNid : mapCNComponentID.get(objIndicator.Programme_Area__c)){
                objGrantIndicator= new Grant_Indicator__c();
                objGrantIndicator .Indicator__c = objIndicator.ID;
                objGrantIndicator .Type__c = objIndicator.Type_of_data__c;
                objGrantIndicator .Data_Type__c = objIndicator.Type_of_Data__c;
                objGrantIndicator .Concept_Note__c = CNid;
                lstGrantIndicator.add(objGrantIndicator);
            }
        }
    }*/
    //insert lstGrantIndicator;
}