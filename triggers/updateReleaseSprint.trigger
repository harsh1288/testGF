trigger updateReleaseSprint on Work_Product__c (after insert, after update,after delete) 
{  
     If(trigger.isAfter && (trigger.isInsert || trigger.isUpdate )){
     PMToolKitWorkProductActions.updateReleaseSprintd(trigger.new);
     }
     If(trigger.isAfter && trigger.isDelete ){
     PMToolKitWorkProductActions.updateReleaseSprintd(trigger.old);
     }   
     
}