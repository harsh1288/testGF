/*
* $Author:      $ TCS
* $Description: $ Trigger to create an Affiliation for new Contact. RecordType of Affiliation is based on Account Record Type.
* $Date:        $ 03/03/2015
* $Revision:    $ 
*/

trigger AffiliationForNewContact on Contact (before insert, after insert, after update) {
    
    List<Id> lstContIds = new List<Id>();
    List<Contact> lstContact = new List<Contact>();
    List<Contact> lstTAPContacts = new List<Contact>();
    map<Id, Id> mapContactOldAcc = new map<Id, Id>();
    List<npe5__Affiliation__c> lstNewAffiliations =  new List<npe5__Affiliation__c>();    
    List<npe5__Affiliation__c > lstExistingAffiliations = new List<npe5__Affiliation__c>();
    List<npe5__Affiliation__c > lstUpdateAffiliation = new List<npe5__Affiliation__c>();
    
    map<String, Assigning_RT_to_Affiliation__c> mapOfAffiliationRTs = Assigning_RT_to_Affiliation__c.getAll();
    
    for(Contact Con: Trigger.new){
        if(Trigger.isBefore && con.AccountId == null && Con.recordtypeid == label.TAP_Contact_RT){
            Con.accountId = label.GF_TAP_recordID;
            lstTAPContacts.add(Con);
        }
        if(Trigger.isAfter){
        if(trigger.isInsert && con.AccountId != null){ 

            lstContIds.add(con.Id);
        }
        if(trigger.isUpdate && con.AccountId != null && con.AccountId != trigger.oldmap.get(con.Id).AccountId){
            
            lstContIds.add(con.Id);
            mapContactOldAcc.put(con.Id,trigger.oldmap.get(con.Id).AccountId);   
        }
        }
    } 
    
    
    if(trigger.isUpdate && lstContIds != null){

        lstExistingAffiliations = [select id, npe5__Contact__c, npe5__Organization__c, npe5__Contact__r.Id, npe5__Organization__r.Id from npe5__Affiliation__c where npe5__Contact__c =: lstContIds AND npe5__Status__c = 'Current'];     
    
    }
    
    lstContact = [SELECT id, name, recordtypeid, Accountid, Account.recordtypeid FROM Contact where id =: lstContIds];
      
    
    for(npe5__Affiliation__c objAffiliation: lstExistingAffiliations){
    
        for(Integer i = 0; i<lstContact.size(); i++){
    
            if(objAffiliation.npe5__Contact__c == lstContact[i].Id && objAffiliation.npe5__Organization__c == lstContact[i].accountId)
            lstContact.remove(i); // If an affiliation already exists for this contact and organization, remove this contact from list    
        }
    }
    
      
    //To update Affilation to Former is primary Organization on Contact is changed
    for(Id objCon: mapContactOldAcc.keyset()){
        
          for(npe5__Affiliation__c objAff: lstExistingAffiliations){
                 
                 if(objAff.npe5__Contact__r.Id == objCon && objAff.npe5__Organization__r.Id ==mapContactOldAcc.get(objCon)){
                 objAff.npe5__Status__c = 'Former'; 
                 lstUpdateAffiliation.add(objAff);
              }
          }
          
    }
   
     for(Contact Con: lstContact){          
                
                npe5__Affiliation__c Aff = new npe5__Affiliation__c();
                Aff.npe5__Contact__c = con.id;
                Aff.npe5__Organization__c = Con.Accountid; 
                Aff.npe5__StartDate__c = system.today(); 
                Aff.Access_Level__c = 'No Access';
                
                System.debug('mapOfAffiliationRTs.get(Con.Account.RecordtypeId)'+mapOfAffiliationRTs.get(Con.Account.RecordtypeId));
                
                // From the Account record type extract the record type for Affiliation using custom setting
                if(mapOfAffiliationRTs.get(Con.Account.RecordtypeId) != null){
                    if(mapOfAffiliationRTs.get(Con.Account.RecordtypeId).Affiliation_RT_Id__c != Null  && mapOfAffiliationRTs.get(Con.Account.RecordtypeId).Active__c == true)        
                        Aff.RecordTypeId = mapOfAffiliationRTs.get(Con.Account.RecordtypeId).Affiliation_RT_Id__c;
                } 
                lstNewAffiliations.add(Aff);  

     }    
    
        
        if(lstUpdateAffiliation != Null){
            try{
                update lstUpdateAffiliation;}
        
            catch(Exception e){
                System.debug('Failed to update old Affiliation:'+e);
            }
            }

         if(lstNewAffiliations != Null){
            try{
                insert lstNewAffiliations;
            }
            catch(Exception e){
            System.debug('Failed to create a new Affiliation:'+e);
            }
          
        }   
             

}