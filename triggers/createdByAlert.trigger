trigger createdByAlert on Task (before Update) {
    
    List<id> createdByList = new List<id>();
    user u;
    id ta;
    string ownername = '';
    string subject = '';
    string duedate = '';
    string priority = '';
    string orgname = '';
    String fullRecordURL = '';
    
    
    //EmailTemplate templateId = [Select id from EmailTemplate where name = 'Notify Task Completion'];
    
    for(Task T: trigger.new){   
        if(t.createdByid != t.ownerid && t.status== 'Completed' && t.status != trigger.oldmap.get(t.id).status){
             createdByList.add(t.createdByid);
             ownername = t.Assigned_owner__c;
             subject = t.subject;
             duedate = string.valueof(t.Activitydate);
             priority = t.priority;
             ta = t.id;
             fullRecordURL = URL.getSalesforceBaseUrl().toExternalForm() + '/' +t.Id;
             if(t.whatid != NULL){
                 if(string.valueof(t.whatid).substring(1,3)=='001'){
                     orgname = [select id,name from Account where id=:t.whatid].name; 
                 }    
             }
             
        }       
    }
    
    if(createdByList.size()>0 && createdByList !=Null){
       u = [select id,email,name from user where id IN: createdByList LIMIT 1];

    }   
       
       system.debug('u--'+u);
    
    if(u!= NULL){
         Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
         String[] toAddresses = new String[] {u.email};
         system.debug('Emailid'+u.email);
         mail.setToAddresses(toAddresses);
        // mail.setTemplateId(templateId.id);
        // mail.targetObjectId=u.id;
         //mail.saveAsActivity = false;
         //String subject='Task is Completed';
         String body= ownername +' has completed the following task:\n'+'Subject: '+Subject+'\nOrganization: '+Orgname+'\nDue Date: '+Duedate+'\nPriority: '+Priority+'\nFor more details, click the following link:\n'+fullRecordURL  ;
         mail.setPlainTextBody(body);
         mail.setSubject(subject);
         Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 
    }
}