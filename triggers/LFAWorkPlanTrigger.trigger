/**********************************************************************************
* Trigger: {LFAWorkPlanTrigger}
* Created by {LRT},{DateCreated 01/23/2014}
----------------------------------------------------------------------------------
*/
trigger LFAWorkPlanTrigger on LFA_Work_Plan__c (after update) {
    if(Trigger.isUpdate) {
        //On Update Call The Handler Method.
        System.debug('In Trigger' +Trigger.New );
        LFAWorkPlanHandler.auLFAWorkPlanStatus(Trigger.New,Trigger.Oldmap);
        System.debug('Trigger Completed');
    }
}