trigger DuplicateCustomGrantInterventionName on Grant_Intervention__c (before update,before delete, after insert, after update) {

if(Trigger.isBefore){
    if(Trigger.isUpdate){
List<string> newgintmod = new List<String>(); 
List<String> newcustname = new List<String>();
Set<String> custIntSet = new Set<String>();
     for (Grant_Intervention__c objgint :Trigger.new){
         if(Trigger.oldMap.get(objgint.id).Custom_Intervention_Name__c != objgint.Custom_Intervention_Name__c && objgint.Concept_Note__c == Null && objgint.Custom_Intervention_Name__c != Null && Trigger.oldMap.get(objgint.id).Performance_Framework__c != null){
            system.debug('@@objgint'+objgint.id);
            newgintmod.add(objgint.Module__c);
            newcustname.add(objgint.Custom_Intervention_Name__c);
         }
         system.debug('@@objgint'+newgintmod);
         system.debug('@@objgint'+newgintmod);
    } 
 
 if(newgintmod.size() != null && newcustname.size() != null){   
 //List of all Custom grant intervention names present for that module
  List<Grant_Intervention__c> custginname = new List<Grant_Intervention__c>();
  
  custginname  = [select Custom_Intervention_Name__c from Grant_Intervention__c  where Module__c =:newgintmod and Concept_Note__c =: Null and Custom_Intervention_Name__c !=: Null];
  
   Map <String,Grant_Intervention__c> custnametoobj  = new Map<String,Grant_Intervention__c>();
       
       if(custginname.size()>0){
              for(Grant_Intervention__c objgrant : custginname ){
                  if(objgrant.Custom_Intervention_Name__c!=null){
                     custIntSet.add(objgrant.Custom_Intervention_Name__c);
                     custnametoobj.put(objgrant.Custom_Intervention_Name__c,objgrant);
                  }
              }
           }
     }
     if(custIntSet !=null){   
       //Loop again through updatedIntervention Names
          for (Grant_Intervention__c objgint :Trigger.new){
                if (custIntSet.contains(objgint.Custom_Intervention_Name__c)){
                    objgint.Custom_Intervention_Name__c.addError('Duplicate Custom Intervention Name.Please enter Different Name For Custom Intervention'); 
                   
                }
          }
      }
  }   
  
       if (Trigger.isDelete){
                List<sObject> sObjectList = new List<sObject>();
                for( Grant_Intervention__c  rec : Trigger.old){
                 if(Trigger.oldMap.get(rec.Id).RecordTypeId == rec.RecordTypeId)
                    sObjectList.add((sObject)rec );
                }    
                PFChangesTrackerHandler.CheckPFField(sObjectList);
            }
    
                if ( Trigger.isInsert )
                {  
                    List<sObject> sObjectList = new List<sObject>();
                    for( Grant_Intervention__c  rec : Trigger.new ){
                    
                        sObjectList.add((sObject)rec );
                     }   
                    PFChangesTrackerHandler.CheckPFField(sObjectList);
               
                }
                else if ( Trigger.isUpdate )
                {
                    List<sObject> sObjectList = new List<sObject>();
                    for( Grant_Intervention__c  rec : Trigger.new ){
                      if(Trigger.oldMap.get(rec.Id).RecordTypeId == rec.RecordTypeId)
                        sObjectList.add((sObject)rec );
                     }   
                    PFChangesTrackerHandler.CheckPFField(sObjectList);
           
                } 
           }      
}