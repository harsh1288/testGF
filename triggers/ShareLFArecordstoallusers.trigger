trigger ShareLFArecordstoallusers on DocumentUpload_GM__c (after insert, after update) {
    
    set<Id> gipIds = new set<Id>();
    set<Id> accountIds = new set<Id>();
    set<Id> grantIds = new set<Id>();
    set<Id> lstConIds = new set<Id>();
    List<User> lstLFAUsers  = new List<User>();
    List<DocumentUpload_GM__Share> lstDocShare = new List<DocumentUpload_GM__Share>();
    List<DocumentUpload_GM__c> documents = [Select id, Security__c, Description__c, Download_File__c, 
                                            DownloadId__c, FeedItem_Id__c, File_Name__c, Final_detailed_budget__c,
                                            Final_list_of_health_products_and_costs__c, Grant_Status__c,
                                            Implementation_Period__c, Implementation_Period__r.principal_recipient__c, 
                                            Implementation_Period__r.grant__c,Language__c, Sub_Type__c, GMType__c, Type__c 
                                            FROM DocumentUpload_GM__c
                                            WHERE Id IN : Trigger.new]; 
                                            
    
    if( Trigger.isAfter){
        for(DocumentUpload_GM__c document : documents ){
            if(Trigger.isInsert){
                System.debug('==='+document.Implementation_Period__r.principal_recipient__c);
                if( document.Security__c.contains('Visible to LFA and TGF internal only') ) {
                    gipIds.add(document.Implementation_Period__c);
                    accountIds.add(document.Implementation_Period__r.principal_recipient__c);
                    grantIds.add(document.Implementation_Period__r.grant__c);
                    }    
                }else if( Trigger.isUpdate){ 
                    system.debug('**document.Security__c'+document.Security__c);
                    system.debug('**Trigger.oldMap.get(document.id).Security__c'+Trigger.oldMap.get(document.id).Security__c);
                    if( document.Security__c != Trigger.oldMap.get(document.id).Security__c && document.Security__c.contains('Visible to LFA and TGF internal only')) {                    
                        gipIds.add(document.Implementation_Period__c);
                        accountIds.add(document.Implementation_Period__r.principal_recipient__c);
                        grantIds.add(document.Implementation_Period__r.grant__c);
                    }
                }
            }
        
    }
    system.debug('*accountIds'+accountIds);
    system.debug('*grantIds'+grantIds);
    if( !gipIds.isEmpty() && !accountIds.isEmpty() ){
        if(trigger.isInsert || trigger.isUpdate){
            List<Grant__c> lstGrant = [Select Country__r.LFA__c,Country__r.LFA__r.parentID from Grant__c where ID IN : grantIds];
            set<Id> setAccIds = new set<Id>();
            set<Id> setAccParentIds = new set<Id>();
            System.debug('**lstGrant'+lstGrant);
            for(Grant__c objGrant : lstGrant){
                setAccIds.add(objGrant.Country__r.LFA__c);
                setAccParentIds.add(objGrant.Country__r.LFA__r.parentId);
            }
            system.debug('***setAccIds'+setAccIds+'**setAccParentIds'+setAccParentIds);
            //List<Account> lstAccounts = [SELECT Id, (SELECT Id from Contacts) FROM Account WHERE Id=:accountIds];
            List<npe5__Affiliation__c> lstAff = [Select id,npe5__Contact__c,npe5__Organization__c FROM npe5__Affiliation__c where (npe5__Organization__c IN:setAccIds OR npe5__Organization__c IN:setAccParentIds)];
            for(npe5__Affiliation__c aff : lstAff ){
                //contactIds.add();
                lstConIds.add(aff.npe5__Contact__c);
            }  
        }
        System.debug('-----lstConIds--'+lstConIds);
        //Query for Email of Users where Profile name is PR Admin or PR Read Write Edit
        if(lstConIds.size()>0){
            system.debug('UserInfo.getUserId() '+UserInfo.getUserId() );
            lstLFAUsers = [SELECT ID, Email, ContactId, Contact.Firstname, Contact.Lastname, ProfileId, Profile.Name FROM User WHERE ContactID in: lstConIds and ID <>: UserInfo.getUserId() and Profile.Name=:Label.LFA_Portal_User  And isActive =: True];
        }
        System.debug('----lstLFAUsers ---'+lstLFAUsers );
        
        for( DocumentUpload_GM__c doc: Trigger.new){
            for( User usr : lstLFAUsers ){
                System.debug('---usrid--'+usr.Id);
                DocumentUpload_GM__Share cns = new DocumentUpload_GM__Share();
                cns.ParentId = doc.ID;
                cns.AccessLevel = 'Edit';
                cns.UserOrGroupId = usr.Id ;
                lstDocShare.add(cns);
            }
        }
        if( !lstDocShare.isEmpty() ){
            insert lstDocShare;
        }
       
    }
    
    

}