/*
* $Author:      $ TCS - Debarghya Sen
* $Description: $ Trigger for Indicator_Goal_Junction object
* $Date:        $ 11-Nov-2014
* $Revision:    $ 
*/

trigger Indicator_Goal_junction_prevent_delete on Ind_Goal_Jxn__c (before delete) {
    private boolean executingBefDel = false;
        if(Trigger.isBefore){
            if(Trigger.isInsert){                                             
            }
            else if(Trigger.isUpdate){          
            }
            else if(Trigger.isDelete){
                if(!executingBefDel){
                    Indicator_Goal_junction_Trigger_handler.preventdeleteIndGoalJunc(Trigger.old);     
                    executingBefDel = true;  
                }
            }
        }
        
}