trigger PopulateVendorOnAccount on Vendor__c (after insert, after update) {
    
    List<Vendor__c> lstVendor = new List<Vendor__c>();  
    if(Trigger.isAfter && (Trigger.isBefore || Trigger.isInsert)) {
        for(Vendor__c vendor : Trigger.new) {
            if(isAccountIdChanged(vendor)) {
                lstVendor.add(vendor);
            }
        } 
    }
    
    if(lstVendor.size() > 0) {
        updateAccount(lstVendor);
    }
    
    public void updateAccount(List<Vendor__c> vendorLst){
        
        List<Account> accToUpdate = new List<Account>();
        for(Vendor__c vendor : vendorLst) {
            accToUpdate.add(new Account(Id = vendor.AccountId__c ,Vendor_ID__c = vendor.Id));
        } 
        if(accToUpdate.size() > 0) {
            update accToUpdate;
        }
    }
    
    public boolean isAccountIdChanged(Vendor__c vendor) {
        if(Trigger.isInsert && vendor.AccountId__c != NULL) {
            return true;
        }
        /*
        if(Trigger.isUpdate) {
            if(vendor.AccountId__c != NULL && (vendor.AccountId__c != Trigger.oldMap.get(vendor.Id).AccountId__c)) {
                return true;
            }
        }
        */
        return false;
    }
}