trigger InactiveUserContact on Contact (after Update) {
    
    List<id> ConList = new List<id>();
    List<id> ExternalConList = new List<id>();
    List<user> updateUserList = new List<User>();
    List<npe5__Affiliation__c> updateAffList = new List<npe5__Affiliation__c>();
    List<npe5__Affiliation__c> AffliationList = new List<npe5__Affiliation__c>();
    List<id> GMtabRevokeConList= new List<id>();
    string RevokeGMTabName = 'Make_GM_app_visible_to_LFA_user_PermissionSet';
   
   // Inactive User
    for(Contact c: trigger.new){
         if(c.No_of_Active_Affiliation__c == 0 && c.No_of_Active_Affiliation__c != trigger.oldmap.get(c.id).No_of_Active_Affiliation__c){
              ConList.add(c.id);   
         }
    }
    
    
    if(ConList.size()>0)
       FutureClassInactiveUser.updateContact(ConList);
       
    // Get records to revoke GM tab
    for(Contact cont: trigger.new){
        if(cont.Active_PR_Affiliation__c == 0 && cont.Active_PR_Affiliation__c!= trigger.oldmap.get(cont.id).Active_PR_Affiliation__c){
              GMtabRevokeConList.add(cont.id);   
         }
    }  
    
    if(GMtabRevokeConList.size()>0)
        AffiliationServices.RevokeGrantMakingTab(GMtabRevokeConList,RevokeGMTabName);
    
   
   // Update Access Level of Affiliation when  Access for external user becomes false in Contact
    for(Contact c: trigger.new){
         if(c.External_User__c == False && c.External_User__c != trigger.oldmap.get(c.id).External_User__c){
              ExternalConList.add(c.id);   
         }
    }
    
    if(ExternalConList.size()>0){
        AffliationList = [Select id,Access_Level__c from npe5__Affiliation__c where npe5__Contact__c IN: ExternalConList AND Access_Level__c != 'No Access'];
    
        for(npe5__Affiliation__c Aff: AffliationList){
            aff.Access_Level__c = 'No Access';
            aff.Manage_Contacts__c = false;
            updateAffList.add(aff);
        }
        
        if(updateAffList.size()>0)
            update updateAffList;
    }    
}