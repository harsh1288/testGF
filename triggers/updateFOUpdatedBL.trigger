/*Purpose:Update FO Updated Record GM detailed budget in HPC whenever Detailed Budget updated  */
trigger updateFOUpdatedBL on Budget_Line__c (after Update,after delete) {

List<Id> DBId = new List<Id>();
List<IP_Detail_Information__c> listDBToUpdate = new List<IP_Detail_Information__c>();
If(Trigger.IsUpdate){
 for(Budget_Line__c objBudgetline : Trigger.new){
      if( !(objBudgetline.trackField__c != Trigger.oldMap.get(objBudgetline.Id).trackField__c) ){   
         DBId.add(objBudgetline.Detailed_Budget_Framework__c);
      }
  }
 }
 If(Trigger.IsDelete){
 for(Budget_Line__c objBudgetline : Trigger.old){
     DBId.add(objBudgetline.Detailed_Budget_Framework__c);
  }
 }
 List<IP_Detail_Information__c> objDB = [Select IS_FO_Updated_BL__c From IP_Detail_Information__c where Id IN: DBId AND Budget_Status__c =: Label.IP_Sub_to_GF];
 if(objDB != null && objDB .size() >0){
     for(IP_Detail_Information__c DBToUpdate:objDB ){
     DBToUpdate.IS_FO_Updated_BL__c = true;
      listDBToUpdate.add(DBToUpdate);
     }
  }
  if(listDBToUpdate != null && listDBToUpdate.size() >0) update listDBToUpdate;
}