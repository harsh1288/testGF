trigger AccountTrigger on Account (before insert,after insert,before update,after update) {
   If(Trigger.isBefore)
   {
        if(!StopRecursion.hasAlreadyRunBeforeAcc())
            {
                //  for Share_Account_CT trigger :Sets the Account owner to equal the FPM of the country,before every insert or update, for PR Record Type.
                AccountTriggerHandler.UpdateAccountOwner(Trigger.new);
                StopRecursion.setAlreadyRunBeforeAcc();
            }
        If(Trigger.isInsert)
        {
            if(!StopRecursion.hasAlreadyRunBeforeInsertAcc())
            {
                StopRecursion.setAlreadyRunBeforeInsertAcc();
            }       
        }
        If(Trigger.isUpdate)
        {
            if(!StopRecursion.hasAlreadyRunBeforeUpdateAcc())
            {
                StopRecursion.setAlreadyRunBeforeUpdateAcc();
            }       
        }
   }
   If(Trigger.isAfter)
   {    
        if(!StopRecursion.hasAlreadyRunAfterAcc())
            {
                //  for Share_Account_CT :Upsert of AccountShare records are done on after Insert or after Update
                    AccountTriggerHandler.UpsertAccountShareRecords(Trigger.New);
                //  for UpdateAccountFinanceOfficer trigger : has an update on account object
                    AccountTriggerHandler.aiuSetAccountFinanceOfficer(Trigger.New,Trigger.OldMap,Trigger.IsInsert,Trigger.IsUpdate);
                    StopRecursion.setAlreadyRunAfterAcc();
            }
        If(Trigger.isInsert)
        {   
            if(!StopRecursion.hasAlreadyRunAfterInsertAcc())
            {
                StopRecursion.setAlreadyRunAfterInsertAcc();
            }
        }
        If(Trigger.isUpdate)
        {
            if(!StopRecursion.hasAlreadyRunAfterUpdateAcc())
            {
        //  for trgSubmitAccountCustomApproval trigger
                AccountTriggerHandler.SubmitAccountCustomApproval(Trigger.oldMap.keyset(),Trigger.oldMap);
        //  for trgSubmitForApproval trigger : has an update on account object
                AccountTriggerHandler.SubmitForApproval(Trigger.new);
                StopRecursion.setAlreadyRunAfterUpdateAcc();
            }
        }
   }    
}