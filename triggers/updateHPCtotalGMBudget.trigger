/*Purpose:Update Total GM detailed budget in HPC whenever Detailed Budget updated  */
trigger updateHPCtotalGMBudget on IP_Detail_Information__c (after Update) {

   List<Id> impid = new List<Id>();
   List<HPC_Framework__c> updatehpclst = new List<HPC_Framework__c>();
   Map<id,HPC_Framework__c> hpcmap = new Map<id,HPC_Framework__c>(); 
   boolean samevalue;
   List<Id> DBId = new List<Id>();
   List<IP_Detail_Information__c> listDBToUpdate = new List<IP_Detail_Information__c>();
    
   //Get record Id from Deatil Budget
   for(IP_Detail_Information__c objipdetail : Trigger.new){
       if(objipdetail.Budget_Status__c != trigger.oldmap.get(objipdetail.id).Budget_Status__c )
       impid.add(objipdetail.Implementation_Period__c);
       if(objipdetail.Budget_Status__c != trigger.oldmap.get(objipdetail.id).Budget_Status__c && objipdetail.Budget_Status__c == Label.IP_Accepted)
       DBId.add(objipdetail.Id);
      
   }
   
   if(impid.size()>0){
   List<HPC_Framework__c> hpc = [select Budget_Framework_Status__c,Budget_Framework_ID__c,Total_GM_Detailed_Budget__c,grant_Implementation_Period__c,updated__c from HPC_Framework__c where Grant_Implementation_Period__c In :impid];
   if(hpc.size()>0){ 
   //creating Map for HPC framework
       For(HPC_Framework__c objhpc :hpc){
           hpcmap.put(objhpc.grant_Implementation_Period__c, objhpc);
       }
         
       for(IP_Detail_Information__c objipdetail : Trigger.new){
           HPC_Framework__c objhpc = hpcmap.get(objipdetail.Implementation_Period__c);
           objhpc.Budget_Framework_ID__c = objipdetail.id;
           if(objipdetail.Budget_Status__c!=Trigger.oldMap.get(objipdetail.Id).Budget_Status__c)
                objhpc.Budget_Framework_Status__c = objipdetail.Budget_Status__c;    
           updatehpclst.add(objhpc); 
      }
  }
      /* IP_Detail_Information__c oldip = Trigger.oldMap.get(objipdetail.Id);
          if(oldip.Total_Budgeted_Amount__c == objipdetail.Total_Budgeted_Amount__c)
              objhpc.Updated__c = true;
          else{
            objhpc.Updated__c = false;
            objhpc.Total_GM_Detailed_Budget__c = objipdetail.Total_Budgeted_Amount__c;
            if(objipdetail.Budget_Status__c!=Trigger.oldMap.get(objipdetail.Id).Budget_Status__c)
                objhpc.Budget_Framework_Status__c = objipdetail.Budget_Status__c;    
            updatehpclst.add(objhpc); 
          }*/
       
         
     
    if(updatehpclst.size()>0 && updatehpclst!=NULL )
       update updatehpclst;  
   }    
   //Reset IS FO Updated Flag in Detailed Budget Framework When DB iS accepted
   List<IP_Detail_Information__c> objDB = [Select IS_FO_Updated_BL__c From IP_Detail_Information__c where Id IN: DBId AND IS_FO_Updated_BL__c =: true];
   if(objDB != null && objDB .size() >0){
     for(IP_Detail_Information__c DBToUpdate:objDB ){
     DBToUpdate.IS_FO_Updated_BL__c = false;
      listDBToUpdate.add(DBToUpdate);
     }
  }
  if(listDBToUpdate != null && listDBToUpdate.size() >0) update listDBToUpdate;
  
}