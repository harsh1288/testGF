/*
*Created: 05-Feb-2015,RK,TCS
*Purpose: maintain budget line as well as product records parallely with start/end date changes in implementation period
*Update:
*/
trigger ReportingPeriodDetailTrigger on Reporting_Period_Detail__c (before insert, before update,after insert,after update) 
    {
        if(trigger.isBefore)
        {
            if(trigger.isInsert) 
            {
            }
            if(trigger.isUpdate) 
            {
            }
        }
        if(trigger.isAfter)
        {            
            if(trigger.isInsert) 
            {               
            }
            if(trigger.isUpdate) 
            {                
            }
        }
    }