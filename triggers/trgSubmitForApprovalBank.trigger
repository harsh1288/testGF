trigger trgSubmitForApprovalBank on Bank_Account__c (after Update) {

Set<Id> setbankId = new Set<Id>();
List<Bank_Account__c> lstbank = new List<Bank_Account__c>();
Map<String,String> mapApproval = new Map<String,String>();
set<Id> bankIdSet = new set<Id>();
for (Bank_Account__c bank : trigger.new) {
     if(bank.Approval_Status__c == 'Finance Officer verification' && Trigger.oldMap.get(bank.Id).Approval_Status__c != 'Finance Officer verification' && !bank.Locked__c)
         setbankId.add(bank.Id);
              
}
    
    
for(ProcessInstance pi : [SELECT Status,TargetObjectId FROM ProcessInstance where Status = 'Pending'])
{
   mapApproval.put(pi.TargetObjectId,pi.Status);
}       

   lstbank = [Select Id, Name, Most_Recent_Submitter__c,CT_Finance_Officer__c, Approval_Status__c from Bank_Account__c where Id in :setbankId];
   list<Org_Bank_Account_Junc__c> juncList = new List<Org_Bank_Account_Junc__c >();
   for (Bank_Account__c bank : lstbank) {
     if(bank.Approval_Status__c == 'Finance Officer verification')
     {
        if(bank.CT_Finance_Officer__c != null) 
        { 
          bank.Most_Recent_Submitter__c = UserInfo.getUserId();
        }    
     }   
       //create junction record with current status = true 
   
    }
   
   if(!StopRecursion.hasAlreadyCreatedFollowUpTasks())
   {
       
    for(Bank_Account__c bank : lstbank) {
     if(bank.Approval_Status__c == 'Finance Officer verification')
     {
        if(bank.CT_Finance_Officer__c != null && !mapApproval.containskey(bank.Id)) 
        { 
            Approval.ProcessSubmitRequest app = new Approval.ProcessSubmitRequest();

            app.setObjectId(bank.id);

            Approval.ProcessResult result = Approval.process(app);
        }    
     }   

   }  
    
    
        if(!lstbank.isEmpty())
        {
           StopRecursion.setAlreadyCreatedFollowUpTasks();
           update lstbank;
        }  
    }
      
   
}