trigger updateCustIntNameOnGIP on Intervention__c (after Update) {
set<Id> CNIntId = new set<Id>();
List<Grant_Intervention__c> lstGrantIntToUpdate = new List<Grant_Intervention__c>();
    for(Intervention__c objInt : Trigger.new){
        if(Trigger.oldMap.get(objInt.Id).Custom_Intervention_Name__c!= objInt.Custom_Intervention_Name__c){
            CNIntId.add(objInt.Id);
        }
        
     if(CNIntId !=null){
        List<Grant_Intervention__c> lstGrantInt  = [Select Id,Custom_Intervention_Name__c from Grant_Intervention__c where 
                                                        CN_Intervention__c IN : CNIntId];
            for(Grant_Intervention__c objGint : lstGrantInt){
                objGint.Custom_Intervention_Name__c = objInt.Custom_Intervention_Name__c;
                lstGrantIntToUpdate.add(objGint);
            }
        }
    }
    
    update lstGrantIntToUpdate;
   
}