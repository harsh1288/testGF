trigger ShareCNAffliation on Concept_Note__c (after Insert) {
    
    Set<Id> setCNId = new Set<Id>();
    Set<Id> setAccId = new Set<Id>();
    List<Concept_Note__c> cnListShare = new List<Concept_Note__c>();
    AccountShare externalAFFRecord = new AccountShare();
    Set<Id> CNIds = new Set<Id>();
    Map<Id,Id> userConMap = new Map<Id,Id>(); 
    List<Concept_Note__Share> lstCNShare = new List<Concept_Note__Share>();
    List<AccountShare> externalAccShareCCM = new List<AccountShare>();
    List<AccountShare> lstAffShare  = new List<AccountShare>();
        
    for(Concept_Note__c objCN : trigger.new){
    setCNId.add(objCN.Id);
    setAccId.add(objCN.CCM_New__c);
    }
     List<User> lstUser = [Select ID,Profile.Name,ContactId from User where User.Profile.UserLicense.Name=:'Partner Community Login'];
     /*List<npe5__Affiliation__c> lstAff = [Select Id,Name,Is_Inactive_c__c,Access_Level__c,npe5__Organization__c,npe5__Contact__c from npe5__Affiliation__c  where npe5__Organization__c in : setAccId];*/
     List<npe5__Affiliation__c> lstAff = [Select Id,Name,Is_Inactive_c__c,Access_Level__c,npe5__Organization__c,npe5__Contact__c, npe5__Status__c, RecordTypeId from npe5__Affiliation__c  where npe5__Organization__c in : setAccId
                                                                                                                                                              AND (RecordTypeId =: label.CM_Affiliation_RT OR RecordTypeId =: label.PR_Affiliation_RT)
                                                                                                                                                            AND npe5__Status__c = 'Current'];  /*27-02-2015: Added condition for CRM change in Access Level values*/
    System.debug('**lstUser '+lstUser);
    System.debug('**lstAff '+lstAff );
    for(User objUser :lstUser ){
        userConMap.put(objUser.ContactId,objUser.id);
    }
    List<Concept_Note__c> lstCN = [Select Id,Name,CCM_New__c from Concept_Note__c Where Id in : setCNId ];
    System.debug('**lstCN '+lstCN );
    Map<id, List<Concept_Note__c>> conceptMap = new Map<id,List<Concept_Note__c>>();
     
    /*Get the Account and its related Concept Note */
    for(Concept_Note__c c : lstCN){
        if(conceptMap.get(c.CCM_New__c) != null){
            List<Concept_Note__c> DocList =conceptMap.get(c.CCM_New__c);
            DocList.add(c);
            system.debug('**DocList'+DocList);
            conceptMap.put(c.CCM_New__c,DocList);
        }
        else{
            List<Concept_Note__c> DocList = new List<Concept_Note__c>();
            DocList.add(c);
            conceptMap.put(c.CCM_New__c,DocList);
        }
        system.debug('**conceptMap'+conceptMap);
    }
    
      for(npe5__Affiliation__c objAff : lstAff){
        //if(objAff.Is_Inactive_c__c == false && (objAff.Access_Level__c=='CCM Admin' ||  objAff.Access_Level__c=='CCM Read Write' )){  
          if(objAff.npe5__Status__c == 'Current' && (objAff.Access_Level__c=='Admin' ||  objAff.Access_Level__c=='Read/Write' )  && objAff.RecordTypeId == label.CM_Affiliation_RT){ /*27-02-2015: Added condition for CRM change in Access Level values*/            
            /*externalAFFRecord.AccountID = objAff.npe5__Organization__c;
            
            externalAFFRecord.UserOrGroupId = userConMap.get(objAff.npe5__Contact__c);
            
            System.debug('**UserOrGroupId '+externalAFFRecord.UserOrGroupId);
            System.debug('**externalAFFRecord.AccountID'+externalAFFRecord.AccountID);
            externalAFFRecord.AccountAccessLevel= 'Edit';
            externalAFFRecord.OpportunityAccessLevel = 'Edit';*/
            
           /***********************Share concept Notes records related to CCM Organisation******************************************************/
              
            if(lstCN.size()>0){
                cnListShare = conceptMap.get(objAff.npe5__Organization__c);
                List<Implementation_Period__c> lstIP = [Select id,Principal_Recipient__c from Implementation_Period__c where Concept_Note__c in :cnListShare ];
                System.debug('****lstIP'+lstIP);
                for(Concept_Note__c cn :cnListShare){
                    Concept_Note__Share cns = new Concept_Note__Share();
                    cns.ParentId = cn.ID;
                    cns.AccessLevel = 'Edit';
                    cns.UserOrGroupId = userConMap.get(objAff.npe5__Contact__c);
                    System.debug('**UserOrGroupId of Concept NOte'+cns.UserOrGroupId);
                    System.debug('**Parent Id of the record'+cns.ParentId);
                    lstCNShare.add(cns);
                    
                }
                
                /******************* Share Implementation Period of Concept note which where shared for CCM organistaion**************/
                
                /*for(Implementation_Period__c ip : lstIP){
                    AccountShare accshare = new AccountShare();
                    accshare.AccountID = ip.Principal_Recipient__c;
                    accshare.UserOrGroupId = userConMap.get(objAff.npe5__Contact__c);
                    accshare.AccountAccessLevel= 'Edit';
                    accshare.OpportunityAccessLevel = 'Edit';
                    externalAccShareCCM.add(accshare);
                    
                }*/
            }
            
            
            //externalAFFRecord.RowCause = ;
            //lstAffShare.add(externalAFFRecord);
           
        }
        
         /************************* Share Implementation period of PR Organisation **********************/
             
        //if(objAff.Is_Inactive_c__c == false && (objAff.Access_Level__c == 'PR Admin' ||  objAff.Access_Level__c == 'PR Read Write')){
          if(objAff.npe5__Status__c == 'Current' && (objAff.Access_Level__c=='Admin' ||  objAff.Access_Level__c=='Read/Write' )  && objAff.RecordTypeId == label.PR_Affiliation_RT){ /*27-02-2015: Added condition for CRM change in Access Level values*/  
            externalAFFRecord = new AccountShare();
            externalAFFRecord.AccountID = objAff.npe5__Organization__c;
            externalAFFRecord.UserOrGroupId = userConMap.get(objAff.npe5__Contact__c);
            externalAFFRecord.AccountAccessLevel= 'Edit';
            externalAFFRecord.OpportunityAccessLevel = 'Edit';
            lstAffShare.add(externalAFFRecord);
            
            List<Implementation_Period__c> lstIP = [Select id,Principal_Recipient__c,Concept_Note__c from Implementation_Period__c where Principal_Recipient__c =:objAff.npe5__Organization__c];
            System.debug('**lstIP'+lstIP);
            
            for(Implementation_Period__c ip : lstIP){
                CNIds.add(ip.Concept_Note__c);
            }
            List<Concept_Note__c> lstCNPR = [Select Id,Name,CCM_New__c from Concept_Note__c Where id in : CNIds];
            System.debug('**lstCNPR'+lstCNPR);
            
            /************* Share Concept Note for which IP is created **************/
            
            for(Concept_Note__c cpr :lstCNPR){
                Concept_Note__Share cnps = new Concept_Note__Share();
                cnps.ParentId = cpr.ID;
                cnps.AccessLevel = 'Read';
                cnps.UserOrGroupId = userConMap.get(objAff.npe5__Contact__c);
                System.debug('**UserOrGroupId of Concept NOte'+cnps.UserOrGroupId);
                System.debug('**Parent Id of the record'+cnps.ParentId);
                lstCNShare.add(cnps);
                
            }
        }
        
        
        //if( objAff.Is_Inactive_c__c == false && (objAff.Access_Level__c=='CCM Read')){    
          if(objAff.npe5__Status__c == 'Current' && objAff.Access_Level__c=='Read'  && objAff.RecordTypeId == label.CM_Affiliation_RT){ /*27-02-2015: Added condition for CRM change in Access Level values*/ 
            if(lstCN.size()>0){
                cnListShare = conceptMap.get(objAff.npe5__Organization__c);
                List<Implementation_Period__c> lstIP = [Select id,Principal_Recipient__c from Implementation_Period__c where Concept_Note__c in :cnListShare ];
                System.debug('****lstIP'+lstIP);
                for(Concept_Note__c cn :cnListShare){
                    Concept_Note__Share cns = new Concept_Note__Share();
                    cns.ParentId = cn.ID;
                    cns.AccessLevel = 'Read';
                    cns.UserOrGroupId = userConMap.get(objAff.npe5__Contact__c);
                    System.debug('**UserOrGroupId of Concept NOte'+cns.UserOrGroupId);
                    System.debug('**Parent Id of the record'+cns.ParentId);
                    lstCNShare.add(cns);
                    //CNtoDeleteCCM.add(cn.ID);
                }
                /*for(Implementation_Period__c ip : lstIP){
                    AccountShare accshare = new AccountShare();
                    accshare.AccountID = ip.Principal_Recipient__c;
                    accshare.UserOrGroupId = userConMap.get(objAff.npe5__Contact__c);
                    accshare.AccountAccessLevel= 'Read';
                    accshare.OpportunityAccessLevel = 'Read';
                    externalAccShareCCM.add(accshare);
                    
                }*/
          }    
     }
     
     //if( objAff.Is_Inactive_c__c == false && (objAff.Access_Level__c == 'PR Read')){
       if(objAff.npe5__Status__c == 'Current' && objAff.Access_Level__c=='Read'  && objAff.RecordTypeId == label.PR_Affiliation_RT){ /*27-02-2015: Added condition for CRM change in Access Level values*/
            externalAFFRecord.AccountID = objAff.npe5__Organization__c;
            externalAFFRecord.UserOrGroupId = userConMap.get(objAff.npe5__Contact__c);
            externalAFFRecord.AccountAccessLevel= 'Read';
            externalAFFRecord.OpportunityAccessLevel = 'Read';
            lstAffShare.add(externalAFFRecord);
            //AcctoDeletePR.add(objAff.npe5__Organization__c);
            List<Implementation_Period__c> lstIP = [Select id,Principal_Recipient__c,Concept_Note__c from Implementation_Period__c where Principal_Recipient__c =:objAff.npe5__Organization__c];
            System.debug('**lstIP'+lstIP);
            for(Implementation_Period__c ip : lstIP){
                CNIds.add(ip.Concept_Note__c);
            }
            List<Concept_Note__c> lstCNPR = [Select Id,Name,CCM_New__c from Concept_Note__c Where id in : CNIds];
            System.debug('**lstCNPR'+lstCNPR);
            for(Concept_Note__c cpr :lstCNPR){
                Concept_Note__Share cnps = new Concept_Note__Share();
                cnps.ParentId = cpr.ID;
                cnps.AccessLevel = 'Read';
                cnps.UserOrGroupId = userConMap.get(objAff.npe5__Contact__c);
                System.debug('**UserOrGroupId of Concept NOte'+cnps.UserOrGroupId);
                System.debug('**Parent Id of the record'+cnps.ParentId);
                lstCNShare.add(cnps);
                //CNtoDeletePR.add(cpr.ID);
            }
        }
    
    }// lstAff loops ends here
        System.debug('lstAffShare'+lstAffShare);
     Database.SaveResult[] accShareInsertResult = Database.insert(externalAccShareCCM,false);
     Database.SaveResult[] accShareInsertResult2 = Database.insert(lstAffShare,false);
     Database.SaveResult[] cnShareInsertResult = Database.insert(lstCNShare,false);

     System.debug(accShareInsertResult);
     System.debug(cnShareInsertResult);
}