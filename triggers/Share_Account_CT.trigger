trigger Share_Account_CT on Account (after insert, after update) {
    /*************************************************************************
     We execute the trigger after a WP record has been inserted or updated
     because we need the Id of the WP record to already exist.
     return;
     AccountShare is the "Share" table that was created when the
     Organization Wide Default sharing setting is "Private".
     Allocate storage for a list of AccountShare records.
    ********************************************************************************/
    List<User> lstCustomerCommunity =[Select Id, Name, Profile.UserLicense.Name From User Where Profile.UserLicense.Name = 'Customer Community Login' and id = :Userinfo.getUserId()];
    if(lstCustomerCommunity==null || lstCustomerCommunity.size()==0){
        //For the CM User add-on
        List<AccountShare> acctShares  = new List<AccountShare>();
        List<User> lstCMUsers = new List<User>();
        Set<Id> setCountryIds = new Set<Id>();
        Map<Id, Set<Id>> mapCountryAccounts = new Map<Id, Set<Id>>();
        Id recordTypeIdPR;
        
        List<RecordType> lstRTs = [Select Id from RecordType where SObjectType = 'Account' AND DeveloperName = 'PR'];
        if(!lstRTs.isEmpty()){
          recordTypeIdPR = lstRTs[0].Id;
        }
        
    
        // For each of the Account records being inserted, do the following:
        for(Account acct : trigger.new){
    
            if(acct.CT_ID__c != null){
            // Create a new AccountShare record to be inserted in to the AccountShare table.
            AccountShare ctShare = new AccountShare();
                
            // Populate the AccountShare record with the ID of the record to be shared.
            ctShare.AccountId = acct.Id;
                
            // the CT ID formula field, which brings in the value of the CT ID on Country 
            ctShare.AccountAccessLevel = 'Edit';
            ctShare.CaseAccessLevel = 'None';
            ctShare.OpportunityAccessLevel = 'None';
            ctShare.UserOrGroupId = acct.CT_ID__c;               
            // Add the new Share record to the list of new Share records.
            acctShares.add(ctShare);
            }
            //Share Account as read-only with specified LFA Reviewer
            if(acct.LFA_Reviewer__c != null && acct.LFA_Reviewer__c != Userinfo.getuserId()){
                AccountShare LFAShare = new AccountShare();
                
                LFAShare.AccountId = acct.Id;
                LFAShare.AccountAccessLevel = 'Read';
                LFAShare.CaseAccessLevel = 'None';
                LFAShare.OpportunityAccessLevel = 'None';
                LFAShare.UserOrGroupId = acct.LFA_Reviewer__c; 
                
                acctShares.add(LFAShare);
            
            }
            //Change Account Owner to user specified in FPM lookup field on Country, if it isn't null
            /*if(acct.Country__r.FPM__c != null){
                String FPMId = acct.Country__r.FPM__c;
                Account acc = new Account(Id = acct.Id, OwnerId = FPMId);
                system.debug('%%%%%%%%%%%%%'+acc);
                update acc;
            }*/
            system.debug('%%%%%%%%%%%%%'+acctShares);
        // Insert all of the newly created Share records and capture save result 
        
        
         //This section is for sharing with CCMs and has been commented out due to the switch to customer community licenses
         /*if(acct.RecordTypeId == recordTypeIdPR && recordTypeIdPR != null){
            if(acct.Country__c != null){
              setCountryIds.add(acct.Country__c);  }
            if(mapCountryAccounts.get(acct.Country__c) == null){
              mapCountryAccounts.put(acct.Country__c, new Set<Id>{acct.Id});
            } else {
             mapCountryAccounts.get(acct.Country__c).add(acct.Id);
            }        
          }*/
        }  
        
        /*lstCMUsers = [Select Id, Contact.Account.Country__c from User 
            where Contact.Account.Country__c != null 
            AND Contact.Account.Country__c in :setCountryIds 
            AND Profile.Name LIKE '%CM%'
            AND isActive = true];
            
        for(User cmUser : lstCMUsers){
          if(mapCountryAccounts.get(cmUser.Contact.Account.Country__c) != null){
              for(Id acctId : mapCountryAccounts.get(cmUser.Contact.Account.Country__c)){
                  AccountShare cmShare = new AccountShare();
                    cmShare.AccountId = acctId;
                    cmShare.AccountAccessLevel = 'Edit';
                    cmShare.CaseAccessLevel = 'None';
                    cmShare.OpportunityAccessLevel = 'None';
                    cmShare.UserOrGroupId = cmUser.Id;               
                    // Add the new Share record to the list of new Share records
                    acctShares.add(cmShare);   
                }
            }
        }*/
        //
        upsert acctShares;  
        //AccountTriggerHandler.aiauShareWithCT(acctShares);
        // Error handling code omitted for readability.
    }
}