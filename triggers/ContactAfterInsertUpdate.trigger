trigger ContactAfterInsertUpdate on Contact (after insert, after update) {

//  Section 1
//  The goal of this section is to set the Record Type of the Contact to the same as that
//  of its parent Account. For example, if the Account is of Record Type PR, set Contact
//  to Record Type PR as well. (This is done through RecordType.DeveloperName, so, um,
//  if you change those values on either object, make sure they sync.

/*Set<Id> conIds = new Set<Id>();
List<RecordType> lstAccRTs = [Select Id, DeveloperName from RecordType where SObjectType = 'Account'];
List<RecordType> lstConRTs = [Select Id, DeveloperName from RecordType where SObjectType = 'Contact'];
Map<Id,Id> mapAccCon = new Map<Id,Id>();
Id conExternalRT;

for(RecordType ConRT : lstConRTs){
    for(RecordType AccRT : lstAccRTs){
        if(AccRT.DeveloperName == ConRT.DeveloperName || (AccRT.DeveloperName == 'PR_Read_only' && ConRT.DeveloperName == 'PR')){
            mapAccCon.put(AccRT.Id, ConRT.Id); }
      }
      if(ConRT.DeveloperName == 'External_Contact'){
          conExternalRT = ConRT.Id; }
   }
        
for(Contact con : Trigger.new){
   if(Trigger.isInsert){
       conIds.add(con.Id); }
   if(Trigger.isUpdate && Trigger.oldMap.get(con.Id).AccountId != con.AccountId){
       conIds.add(con.Id); }   
  }
  
  List<Contact> lstCons = [Select Id, RecordTypeId, Account.RecordTypeId from Contact where Id in: conIds];
  for(Contact con : lstCons){
      if(mapAccCon.get(con.Account.RecordTypeId) != null){
          con.RecordTypeId = mapAccCon.get(con.Account.RecordTypeId); }
      else if(conExternalRT != null){
          con.RecordTypeId = conExternalRT; }
  }
  update lstCons; */  
    
}