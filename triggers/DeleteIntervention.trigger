trigger DeleteIntervention on Module__c (Before Delete, After insert, After update,before insert,before update ) {

Set<Id> moduleId = new Set<Id>();
Set<Id> cnIds = new Set<Id>();

    
    
    if(Trigger.isBefore){
        if (Trigger.isDelete){
            List<sObject> sObjectList = new List<sObject>();
            for(Module__c objModule : trigger.old){
            sObjectList.add((sObject)objModule );
            moduleId.add(objModule.Id);
        }
        PFChangesTrackerHandler.CheckPFField(sObjectList);
        List<Grant_Intervention__c> lstGrntIntervention = [SELECT ID FROM Grant_Intervention__c where Module__c In: moduleId];
        try{
            if(lstGrntIntervention.size() >0){
                delete lstGrntIntervention;
            }
        }catch(exception e){
            system.debug('**Exception** '+e);
        }
            
            
        }
    
        if ( Trigger.isInsert ){  
            List<sObject> sObjectList = new List<sObject>();
            for( Module__c mod : Trigger.new ){
               
                sObjectList.add((sObject)mod );
             }   
            PFChangesTrackerHandler.CheckPFField(sObjectList);
       
        }
        if(trigger.isUpdate) {    
            List<sObject> sObjectList = new List<sObject>();
            for( Module__c mod : Trigger.new ){
             if(Trigger.oldMap.get(mod.Id).RecordTypeId == mod.RecordTypeId)
                sObjectList.add((sObject)mod );
            }    
            PFChangesTrackerHandler.CheckPFField(sObjectList);
        }
    }
}