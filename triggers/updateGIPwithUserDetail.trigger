/*********************************************************************************
* Trigger: updateIPDetailRelatedToAcc
  Test class:   TestUpdateIPDetailRelatedToAcc
*  DateCreated : 28/10/2014
----------------------------------------------------------------------------------
* Purpose/Methods:
* - To update email fields on IP Detail Information from Country
----------------------------------------------------------------------------------
* History:
* - VERSION  DEVELOPER NAME    DATE            DETAIL FEATURES
     1.0                      28/10/2014      INITIAL DEVELOPMENT
*********************************************************************************/
trigger updateGIPwithUserDetail on Country__c (after update) {

Set<Id> countryIds = new Set<Id>();
Set<Id> countryFPMIds = new Set<ID>();
Set<Id> ipIds = new Set<Id>();

Id prID = [Select ID from RecordType where SObjectType = 'Account' and Name = 'PR'].Id;
List<Account> lstAccts = new List<Account>();
List<Implementation_Period__c> lstIP = new List<Implementation_Period__c>();
List<IP_Detail_Information__c> lstIPDetail = new List<IP_Detail_Information__c>();
List<Performance_Framework__c> lstPF = new List<Performance_Framework__c> ();
List<HPC_Framework__c> lstHPC = new List<HPC_Framework__c>();
Set<Id> setIPID = new Set<Id>();

    for(Country__c co: Trigger.new){
    
        if(Trigger.isInsert){
           countryIds.add(co.Id); 
        }
        
        if(Trigger.isUpdate && co.FPM__c != System.Trigger.oldMap.get(co.Id).FPM__c){
            countryFPMIds.add(co.Id);
        }
        
        
        if( Trigger.isUpdate && (co.FPM__c != System.Trigger.oldMap.get(co.Id).FPM__c) 
                               || (co.PO_1__c != System.Trigger.oldMap.get(co.Id).PO_1__c || co.PO_2__c != System.Trigger.oldMap.get(co.Id).PO_2__c || co.PO_3__c != System.Trigger.oldMap.get(co.Id).PO_3__c || co.Finance_Officer__c != System.Trigger.oldMap.get(co.Id).Finance_Officer__c || co.MEPH_Specialist__c != System.Trigger.oldMap.get(co.Id).MEPH_Specialist__c || co.HPM_Specialist__c != System.Trigger.oldMap.get(co.Id).HPM_Specialist__c || co.Finance_Officer__c != System.Trigger.oldMap.get(co.Id).Finance_Officer__c)){
                countryIds.add(co.Id); 
        }
    }
    
    List<Country__c> lstCountries = [Select Id, FPM__c, (Select Id, RecordTypeId, OwnerId, Country__c from Accounts__r WHERE Locked__c = false) from Country__c WHERE Id in :countryIds];
    
    List<Country__c> lstCountriesFPM = [Select Id, FPM__c, (Select Id, RecordTypeId, OwnerId, Country__c from Accounts__r WHERE Locked__c = false) from Country__c WHERE Id in :countryFPMIds];
    Set<Id> accIds = new Set<Id>();    
   
    for(Country__c co: lstCountriesFPM){
        for(Account acct : co.Accounts__r){
            if(acct.RecordTypeId == prID){
                acct.OwnerId = co.FPM__c; 
                accIds.add(acct.Id);}
            }
            lstAccts.addAll(co.Accounts__r);
            system.debug('@@lst'+lstAccts);
        } 
        update lstAccts;  
        //lstIP = [Select Id, Principal_Recipient__r.Country__r.FPM__r.Email, Principal_Recipient__r.Country__r.PO_1__r.Email, Principal_Recipient__r.Country__r.PO_2__r.Email, Principal_Recipient__r.Country__r.PO_3__r.Email, Principal_Recipient__r.Country__r.Finance_Officer__r.Email, Principal_Recipient__r.Country__r.HPM_Specialist__r.Email, Principal_Recipient__r.Country__r.MEPH_Specialist__r.Email, (Select Id, FPM_Email__c, PO_1_Email__c, PO_2_Email__c, PO_3_Email__c, MEPH_Specialist_Email__c, HPM_Specialist_Email__c, Finance_Officer_Email__c from IP_Detail_Information__r ) from Implementation_Period__c WHERE Principal_Recipient__c in: lstAccts];
        lstIP = [Select Id from Implementation_Period__c where Principal_Recipient__c in :lstAccts AND Status__c = 'Grant-Making'];
        for(Implementation_Period__c ip: lstIP){
            setIPID.add(ip.Id);
        }
    
        List<IP_Detail_Information__c> lstIPDetailQry = [Select Id,Implementation_Period__r.Principal_Recipient__r.Country__r.PO_2__r.Email,Implementation_Period__r.Principal_Recipient__r.Country__r.Finance_Officer__r.Email,Implementation_Period__r.Principal_Recipient__r.Country__r.HPM_Specialist__r.Email,Implementation_Period__r.Principal_Recipient__r.Country__r.MEPH_Specialist__r.Email,Implementation_Period__r.Principal_Recipient__r.Country__r.PO_3__r.Email,Implementation_Period__r.Principal_Recipient__r.Country__r.PO_1__r.Email,Implementation_Period__r.Principal_Recipient__r.Country__r.FPM__r.Email, FPM_Email__c, PO_1_Email__c, PO_2_Email__c, PO_3_Email__c, MEPH_Specialist_Email__c, HPM_Specialist_Email__c, Finance_Officer_Email__c,Implementation_Period__c from IP_Detail_Information__c where Implementation_Period__c in : setIPID];
        for(IP_Detail_Information__c ipDetail: lstIPDetailQry ){
            ipDetail.FPM_Email__c = ipDetail.Implementation_Period__r.Principal_Recipient__r.Country__r.FPM__r.Email;
            ipDetail.PO_1_Email__c = ipDetail.Implementation_Period__r.Principal_Recipient__r.Country__r.PO_1__r.Email; 
            ipDetail.PO_2_Email__c = ipDetail.Implementation_Period__r.Principal_Recipient__r.Country__r.PO_2__r.Email;
            ipDetail.PO_3_Email__c = ipDetail.Implementation_Period__r.Principal_Recipient__r.Country__r.PO_3__r.Email;
            ipDetail.MEPH_Specialist_Email__c = ipDetail.Implementation_Period__r.Principal_Recipient__r.Country__r.MEPH_Specialist__r.Email;
            ipDetail.HPM_Specialist_Email__c = ipDetail.Implementation_Period__r.Principal_Recipient__r.Country__r.HPM_Specialist__r.Email;
            ipDetail.Finance_Officer_Email__c = ipDetail.Implementation_Period__r.Principal_Recipient__r.Country__r.Finance_Officer__r.Email;
            ipDetail.FPM_Email__c = ipDetail.Implementation_Period__r.Principal_Recipient__r.Country__r.FPM__r.Email;
            lstIPDetail.add(ipDetail);
         } 
    
        update lstIPDetail;
    
        List<Performance_Framework__c> lstPFQry = [Select id,Implementation_Period__r.Principal_Recipient__r.Country__r.Finance_Officer__r.Email,Implementation_Period__r.Principal_Recipient__r.Country__r.PO_3__r.Email,Implementation_Period__r.Principal_Recipient__r.Country__r.PO_2__r.Email,Implementation_Period__r.Principal_Recipient__r.Country__r.PO_1__r.Email,Implementation_Period__r.Principal_Recipient__r.Country__r.HPM_Specialist__r.Email,Implementation_Period__r.Principal_Recipient__r.Country__r.FPM__r.Email,Implementation_Period__r.Principal_Recipient__r.Country__r.MEPH_Specialist__c,MEPH_Specility__c,Implementation_Period__r.Principal_Recipient__r.Country__r.MEPH_Specialist__r.Email,MEPH_Specialist_Email__c From Performance_Framework__c where Implementation_Period__c IN: setIPID ];
        for(Performance_Framework__c objPF : lstPFQry ){
            objPF.MEPH_Specility__c = objPF.Implementation_Period__r.Principal_Recipient__r.Country__r.MEPH_Specialist__c ;
            objPF.MEPH_Specialist_Email__c = objPF.Implementation_Period__r.Principal_Recipient__r.Country__r.MEPH_Specialist__r.Email;
            objPF.Finance_Officer_Email__c = objPF.Implementation_Period__r.Principal_Recipient__r.Country__r.Finance_Officer__r.Email;
            objPF.HPM_Specialist_Email__c = objPF.Implementation_Period__r.Principal_Recipient__r.Country__r.HPM_Specialist__r.Email;
            objPF.FPM_Email__c = objPF.Implementation_Period__r.Principal_Recipient__r.Country__r.FPM__r.Email;
            objPF.PO_1_Email__c = objPF.Implementation_Period__r.Principal_Recipient__r.Country__r.PO_1__r.Email; 
            objPF.PO_2_Email__c = objPF.Implementation_Period__r.Principal_Recipient__r.Country__r.PO_2__r.Email;
            objPF.PO_3_Email__c = objPF.Implementation_Period__r.Principal_Recipient__r.Country__r.PO_3__r.Email;
            lstPF.add(objPF);
        }
        update lstPF;
    
        List<HPC_Framework__c> lstHPCQry = [Select id,Grant_Implementation_Period__r.Principal_Recipient__r.Country__r.Finance_Officer__r.Email,Grant_Implementation_Period__r.Principal_Recipient__r.Country__r.PO_1__r.Email,Grant_Implementation_Period__r.Principal_Recipient__r.Country__r.PO_2__r.Email,Grant_Implementation_Period__r.Principal_Recipient__r.Country__r.PO_3__r.Email,Grant_Implementation_Period__r.Principal_Recipient__r.Country__r.FPM__r.Email,Grant_Implementation_Period__r.Principal_Recipient__r.Country__r.MEPH_Specialist__r.Email,HPC_Specialist_Email__c,HPC_Specialist__c,Grant_Implementation_Period__r.Principal_Recipient__r.Country__r.HPM_Specialist__c,Grant_Implementation_Period__r.Principal_Recipient__r.Country__r.HPM_Specialist__r.Email From HPC_Framework__c where Grant_Implementation_Period__c IN: setIPID ];
        for(HPC_Framework__c objHPC : lstHPCQry ){
            objHPC.HPC_Specialist__c =  objHPC.Grant_Implementation_Period__r.Principal_Recipient__r.Country__r.HPM_Specialist__c;
            objHPC.HPC_Specialist_Email__c =  objHPC.Grant_Implementation_Period__r.Principal_Recipient__r.Country__r.HPM_Specialist__r.Email ;
            objHPC.M_E_Officer_Email__c = objHPC.Grant_Implementation_Period__r.Principal_Recipient__r.Country__r.MEPH_Specialist__r.Email;
            objHPC.FO_Email__c = objHPC.Grant_Implementation_Period__r.Principal_Recipient__r.Country__r.Finance_Officer__r.Email;
            objHPC.PO_1_Email__c = objHPC.Grant_Implementation_Period__r.Principal_Recipient__r.Country__r.PO_1__r.Email; 
            objHPC.PO_2_Email__c = objHPC.Grant_Implementation_Period__r.Principal_Recipient__r.Country__r.PO_2__r.Email;
            objHPC.PO_3_Email__c = objHPC.Grant_Implementation_Period__r.Principal_Recipient__r.Country__r.PO_3__r.Email;
            objHPC.FPM_Email__c = objHPC.Grant_Implementation_Period__r.Principal_Recipient__r.Country__r.FPM__r.Email;
            lstHPC.add( objHPC);
        }
        update lstHPC;
    
        /*** Merged from Another trigger **/
        //update exchange rate
        
        Decimal USD;
        Decimal EURO;
        boolean flag = false;
        list<Country__c> updateList = new List<Country__c>();
        List<Country__c> ExchrateCon = [Select id,Name,X200DEMA_Exch_Rate_in_USD__c,X200DEMA_Exch_Rate_in_EUR__c From Country__c];
        /*for(Country__c c: ExchrateCon){
            if(c.Name == Label.Country_EURO && trigger.newMap.get(c.id).X200DEMA_Exch_Rate_in_EUR__c != trigger.oldMap.get(c.id).X200DEMA_Exch_Rate_in_EUR__c ){
                EURO = trigger.newMap.get(c.id).X200DEMA_Exch_Rate_in_EUR__c ; 
                system.debug('Debug1'+Euro+' id '+c.id);
            }    
            if(c.Name == Label.Country_USD && trigger.newMap.get(c.id).X200DEMA_Exch_Rate_in_USD__c != trigger.oldMap.get(c.id).X200DEMA_Exch_Rate_in_USD__c ){ 
                USD = trigger.newMap.get(c.id).X200DEMA_Exch_Rate_in_USD__c ; 
                system.debug('Debug2'+USD+' id= '+c.id);   
            }        
        }*/
       
        for(Country__c c: Trigger.new){
            if(c.Name == Label.Country_EURO && c.X200DEMA_Exch_Rate_in_USD__c != trigger.oldMap.get(c.id).X200DEMA_Exch_Rate_in_USD__c ){
                EURO = c.X200DEMA_Exch_Rate_in_USD__c ; 
            }    
            if(c.Name == Label.Country_USD && c.X200DEMA_Exch_Rate_in_EUR__c != trigger.oldMap.get(c.id).X200DEMA_Exch_Rate_in_EUR__c){ 
                USD = c.X200DEMA_Exch_Rate_in_EUR__c ;   
            }
        }
        
        if(EURO != NULL || USD != NULL){ 
            for(Country__c con: ExchrateCon){
                if(EURO != NULL)
                    con.USD_to_Euro_Exchange_rate__c = EURO;
                if(USD != NULL)    
                con.Euro_to_USD_Exchange_rate__c = USD;  
                updateList.add(con);        
            }
            if(updateList!= NULL)
                update updateList;
        }      
        
        /*Update budget line when exchange rate is updated (Euro to USD OR USD to EURO 
          or the exchage rate for currecy 
          This is done specifically to get updated values for Summary budget, Integrated view values and Report values
          */
       
       
       boolean localCurrencyChangeEur = false;
       boolean localCurrencyChangeUSD = false;
       boolean USDtoEUR = false;
       boolean EURtoUSD = false;
       
       set<string> countryList = new set<string>();
       
       for(Country__c c: Trigger.new){
            if( c.X200DEMA_Exch_Rate_in_EUR__c!= trigger.oldMap.get(c.id).X200DEMA_Exch_Rate_in_EUR__c){
                localCurrencyChangeEur = true;            
                countryList.add(c.Name);
            }
            if( c.X200DEMA_Exch_Rate_in_USD__c!= trigger.oldMap.get(c.id).X200DEMA_Exch_Rate_in_USD__c){
                localCurrencyChangeUSD = true; 
                countryList.add(c.Name);
            }
            if( c.Euro_to_USD_Exchange_rate__c != trigger.oldMap.get(c.id).Euro_to_USD_Exchange_rate__c ) {
                EURtoUSD = true;
                countryList.add(c.Name);
            }
            if( c.USD_to_Euro_Exchange_rate__c != trigger.oldMap.get(c.id).USD_to_Euro_Exchange_rate__c ) {
                USDtoEUR = true;
                countryList.add(c.Name);
            }
            
       }
       System.debug('---countryList---'+countryList);
       System.debug('--localCurrencyChangeEur --'+localCurrencyChangeEur +localCurrencyChangeUSD );
       System.debug('--EURtoUSD --'+EURtoUSD +USDtoEUR );
       
       Boolean updateRec = false;
       List<Budget_Line__c> updateBLList = new List<Budget_Line__c>();
       List<Budget_Line__c> blList = [Select id, Name, Currency_Used__c, Implementation_Period__c,trackField__c,
                                       Implementation_Period__r.Currency_of_Grant_Agreement__c,
                                       Implementation_Period__r.status__c
                                       From Budget_Line__c 
                                       WHERE Implementation_Period__r.Country__c IN: countryList
                                       AND Implementation_Period__r.status__c <> 'Accepted' ];
       System.debug('--- blList ---'+blList );                               
       for(Budget_Line__c blLineObj : blList ){
           updateRec = false;
           if( localCurrencyChangeEur ){
               if( blLineObj.Currency_Used__c =='Local Currency' && blLineObj.Implementation_Period__r.Currency_of_Grant_Agreement__c == 'EUR' ){
                   updateRec = true;    
               }                
           }  
           if( localCurrencyChangeUSD ){
               if( blLineObj.Currency_Used__c =='Local Currency' && blLineObj.Implementation_Period__r.Currency_of_Grant_Agreement__c == 'USD' ){
                   updateRec = true;    
               }                
           }  
           if( EURtoUSD ){
               if( blLineObj.Currency_Used__c =='Other Currency' && blLineObj.Implementation_Period__r.Currency_of_Grant_Agreement__c == 'USD' ){
                    updateRec = true;
               }                
           } 
           if( USDtoEUR ){
               if( blLineObj.Currency_Used__c =='Other Currency' && blLineObj.Implementation_Period__r.Currency_of_Grant_Agreement__c == 'EUR' ){
                   updateRec = true;    
               }                
           }
           if( updateRec ){
               updateBLList.add( blLineObj );   
           }
       }
         System.debug('---updateBLList---'+updateBLList);
       
       if( !updateBLList.isEmpty() ){
           update updateBLList;
       }   
}