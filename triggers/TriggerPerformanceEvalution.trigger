/*********************************************************************************
* Trigger: TriggerPerformanceEvalution
* DateCreated: 11/22/2013
----------------------------------------------------------------------------------
* Purpose:
* - This class is used to insert 4 PET Response records based on type for
     each single PE referred.
----------------------------------------------------------------------------------
*********************************************************************************/
trigger TriggerPerformanceEvalution on Performance_Evaluation__c (After Insert,After Update) {
    if(Trigger.isInsert)
        PerformanceEvalutionHandlerNew.aiCreatePETResponse(Trigger.New);
    if(Trigger.isUpdate) {
        PerformanceEvalutionHandlerNew.auUpdatePETandResponse(Trigger.New,Trigger.Oldmap);
        PerformanceEvalutionHandlerNew.auUpdatePETandResponseStatus(Trigger.New,Trigger.Oldmap);
        PerformanceEvalutionHandlerNew.auUpdatePETandResponseStatusSkipped(Trigger.New,Trigger.Oldmap);
    }
        
}