trigger share_Contact_CT on Contact (after insert,after update) {

    List<id> Conlist = new List<id>();
    List<ContactShare> ContactShares  = new List<ContactShare>();
    
    for(Contact c: trigger.new){
        ConList.add(c.id);
    }
    
    List<Contact> ContactInfoList = [Select id,Accountid,Account.CT_ID__c from Contact where ID IN: ConList AND (Contact.recordType.developerName =: 'Applicant_Coordinating_Mechanism' OR Contact.recordType.developerName=:'PR')]; // Need to add filter for Contact RT
    
    for(contact con: ContactInfoList){
        if(con.Account.CT_ID__c != null){
            ContactShare ctShare = new ContactShare();
            ctShare.ContactAccessLevel = 'Edit';
            ctShare.ContactId = con.Id;
            ctShare.UserOrGroupId = con.Account.CT_ID__c;
            ContactShares.add(ctShare);
        }
    }
    
    if(!ContactShares.isEmpty())
        insert ContactShares; 
}