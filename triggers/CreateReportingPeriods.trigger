/* updated to stop recursion */
trigger CreateReportingPeriods on Implementation_Period__c (after Update) {
/*Public String selectopt;
Public String NewOption;
Integer LengthofYears;
public List<Period__c> lstRP {get;set;}
boolean blnRec;
Set<Id> ImpId = new Set<Id>();
Id PFId;
//if(!StopRecursion.hasalreadyRunRPAfterUpdateIP())
//{
for (Implementation_Period__c objIP : Trigger.new) {
 // if(((Trigger.oldMap.get(objIP.Id).Reporting__c != Trigger.newMap.get(objIP.Id).Reporting__c) || (Trigger.oldMap.get(objIP.Id).PR_Fiscal_Cycle__c != Trigger.newMap.get(objIP.Id).PR_Fiscal_Cycle__c)) && Trigger.oldMap.get(objIP.id).Status__c!= objIP.Status__c){
  if(Trigger.oldMap.get(objIP.id).Status__c!= objIP.Status__c && objIP.Status__c == 'Grant-Making'){
    ImpId.add(objIP.Id);
    NewOption = Trigger.newMap.get(objIP.Id).Reporting__c;
    system.debug('**NewOption'+NewOption);
    //blnRec = false;
  }
  }

if(impid !=null && impid.size()>0 ) {     
        lstRP = [Select Id, Period_Number__c,Start_Date__c,End_Date__c,Due_Date__c,Document__c,PU__c,DR__c,EFR__c,Period_Length__c,Audit_Report__c,AR_Due_Date__c,EFR_Due_Date__c,PU_Due_Date__c,Frequency__c,Performance_Framework__c From Period__c Where Implementation_Period__c =: impid And Type__c = 'Reporting' And Flow_to_GrantIndicator__c = false Order by Period_Number__c];
        System.debug('selected frequency value'+selectopt);
        
        List<Implementation_Period__c> lstImplementationPeriod = [Select Id,PR_Fiscal_Cycle__c,Concept_Note__r.Implementation_Period_Page_Template__c,
                                        Start_Date__c,End_Date__c,Length_Years__c,Principal_Recipient__r.Country__r.Country_Fiscal_Cycle_End_Date__c,Principal_Recipient__r.Country__r.Country_Fiscal_Cycle_Start_Date__c,Reporting__c
                                        From Implementation_Period__c Where Id =: impid];
        system.debug('**impid'+impid);
        
        List<Performance_Framework__c> lstPerformanceFr = [Select Id,Implementation_Period__c from Performance_Framework__c where Implementation_Period__c =:impid];
        if(lstPerformanceFr.size()>0){
            for(Performance_Framework__c objPer : lstPerformanceFr)
            PFId = objPer.Id;
        }
        if(lstImplementationPeriod.size() > 0 && lstPerformanceFr.size()>0 ){
        
        
        //system.debug('$$lstImplementationPeriod$$$'+lstImplementationPeriod);
        List<Period__c> lstPeriodToInsert = new List<Period__c>();
        List<Result__c> lstResulttoinsert = new List<Result__c>();
        Period__c objPeriod;
        for(Implementation_Period__c objIP : lstImplementationPeriod){
        if(objIP.Start_Date__c != null && objIP.End_Date__c != null){
             
             if( objIP.PR_Fiscal_Cycle__c !=null){
             //Date dCtrStart = objIP.Principal_Recipient__r.Country__r.Country_Fiscal_Cycle_Start_Date__c;
             //Date dCtrEnd = objIP.Principal_Recipient__r.Country__r.Country_Fiscal_Cycle_End_Date__c;
             String [] FiscalMonths;
             if(objIP.PR_Fiscal_Cycle__c.contains('-')){
             FiscalMonths = objIP.PR_Fiscal_Cycle__c.split('-');
             }
             system.debug('**FiscalMonths'+FiscalMonths);
             String FiscalStartMonth = FiscalMonths[0];
             system.debug('**FiscalStartMonth '+FiscalStartMonth );
             String FiscalEndMonth = FiscalMonths[1];
             system.debug('**FiscalEndMonth '+FiscalEndMonth );
             Date dIPStart = objIP.Start_Date__c;
             Date dIPEnd = objIP.End_Date__c;
             
             System.debug('**dIPEnd '+dIPEnd );
             
             Integer monthsBetween = dIPStart.monthsBetween(dIPEnd);
             LengthofYears = 6;
             Map<String,Integer> MonthNumberMap = new Map<String,Integer>{'January'=>1,'Febuary'=> 2,'March' => 3, 'April'=> 4,'May'=>5,'June' => 6,'July'=> 7,'August'=> 8,'September' => 9,'October'=>10,'November'=>11,'December'=>12};
             
              //MonthNumberMap.put('January'=>01,'Febuary'=> 02,'March' => 03, 'April'=> 04,'May'=>05,'June' => 06,'July'=> 07); 
            /* if(monthsBetween < 36 ){
                 LengthofYears = 3;
             }else{
                 
             }*/ 
             
             /*Date endDate;
             Integer Year = dIPStart.Year();
             System.debug('**Year '+Year );
             Integer day = dIPStart.day();
             System.debug('**day '+day );
             Integer Month = MonthNumberMap.get(FiscalStartMonth );//dCtrStart.month();
             System.debug('**Month '+Month );
             
             Date newDate =  date.newInstance(Year, Month, day);
             Date newDateEnd =  newDate.addMonths(11);
             
             Integer YearEnd = dIPStart.Year();
             Integer dayEnd = dIPStart.day();
             Integer MonthEnd = MonthNumberMap.get(FiscalEndMonth);
             
             Date dCtrEnd = date.newInstance(YearEnd, MonthEnd, dayEnd);
             
             //Date dCtrEnd = newDateEnd ;// dIPStart.addMonths(11);
              
             System.debug('**newDate**'+newDate);
             System.debug('**newDateEnd**'+newDateEnd);
             System.debug('**dCtrEnd**'+dCtrEnd);
             if(NewOption=='Yearly'){
             selectopt = '1';
             system.debug('@@@@@@@@@');
             }else
             if(NewOption=='Half-yearly'){
             selectopt = '2';
             }else if(NewOption == 'Quarterly' ){
             selectopt = '4';
             system.debug('@@######');
             }
             else{
               selectopt = '0';
             }
             
             Integer varGapMonths;
              if(selectopt=='1'){
                Date dCYFiscalStart = newDate ;
                Date dCYFiscalEnd = newDateEnd ;
                endDate = dCYFiscalEnd;
                if(dIPStart.monthsBetween(newDateEnd) > 12){
                //dIPStart<dCtrEnd && dIPStart.Year()==dCtrEnd.Year()){
                endDate = dCtrEnd;
                }
                
                System.debug('**endDate**'+endDate); 
                varGapMonths = 12;
              }
              else if(selectopt=='2'){
                Date dCYFiscalStart = newDate ;
                Date dCYFiscalEnd = newDateEnd;
                Date dCYMid = newDate.addMonths(5);
                
                Integer YearMid = dIPStart.Year();
                Integer dayMid = dCYMid.day();
                Integer MonthMid = dCYMid.month();
             
                Date MidMonthDateSpl = date.newInstance(YearMid, MonthMid, dayMid);
                
                
                System.debug('**DcyMid**'+dCYMid);
                System.debug('**Country End Date**'+dIPStart);
                System.debug('**Country End Date**'+dCtrEnd);
                if(dIPStart.monthsBetween(newDate) > 6){
                //dIPStart<dCtrEnd && dIPStart.Year()==dCtrEnd.Year() ){
                endDate = MidMonthDateSpl ;
                }else
                if(dIPStart.monthsBetween(newDate) < 6 && dIPStart<newDate){
                    endDate = newDate ;
                }
                else
                if(dIPStart > dCYMid){
                endDate  = dCYFiscalEnd;
                
                }
                else{
                endDate = dCYMid;
                }
                varGapMonths = 6;
              }
              else {
                Date dCYFiscalStart = newDate;
                Date dCYFiscalEnd = newDateEnd;
                Date dCYq1E = newDate.addMonths(2);
                System.debug('**dCYq1E**'+dCYq1E); 
                Date dCYq2E = dCYq1E.addMonths(3);
                System.debug('**dCYq2E**'+dCYq2E);
                Date dCYq3E =  dCYq2E.addMonths(3);
                System.debug('**dCYq3E**'+dCYq3E);
                
                Integer YearQuat = dIPStart.Year();
                Integer dayQuat = dCYq1E.day();
                Integer MonthQuat = dCYq1E.month();
             
                Date QuatDateSpl1 = date.newInstance(YearQuat, MonthQuat, dayQuat);
                
                Integer YearQuat2 = dIPStart.Year();
                Integer dayQuat2 = dCYq2E.day();
                Integer MonthQuat2 = dCYq2E.month();
                
                Date QuatDateSpl2 = date.newInstance(YearQuat2, MonthQuat2, dayQuat2);
                
                Integer YearQuat3 = dIPStart.Year();
                Integer dayQuat3 = dCYq3E.day();
                Integer MonthQuat3 = dCYq3E.month();
                
                Date QuatDateSpl3 = date.newInstance(YearQuat3, MonthQuat3, dayQuat3);
                
                if(dIPStart >newDate){
                if(dIPStart>dCYq1E){
                    if(dIPStart>dCYq2E){
                        if(dIPStart>dCYq3E){
                            if(dIPStart<dCtrEnd && dIPStart.Year()==dCtrEnd.Year()){
                                 endDate = dCtrEnd;
                                }
                                else{
                            endDate = dCYFiscalEnd;}
                        }else{
                            if(dIPStart<dCtrEnd && dIPStart.Year()==dCtrEnd.Year()){
                                 endDate = dCtrEnd;
                                }else{
                            endDate = dCYq3E;}
                        }
                    }else{
                        if(dIPStart<dCtrEnd && dIPStart.Year()==dCtrEnd.Year()){
                                 endDate = dCtrEnd;
                                }else{
                            endDate = dCYq2E;}
                        }
                }else{
                        if(dIPStart<dCtrEnd && dIPStart.Year()==dCtrEnd.Year()){
                                 endDate = dCtrEnd;
                                }else{
                            endDate = dCYq1E;}
                    }
                }else{
                    if(dIPStart.monthsBetween(QuatDateSpl2) <3 && dIPStart<QuatDateSpl2){
                        endDate = QuatDateSpl2;
                    }else 
                    if(dIPStart.monthsBetween(QuatDateSpl3) <3  && dIPStart<QuatDateSpl3){
                        endDate = QuatDateSpl3;
                    }else
                    if(dIPStart.monthsBetween(dCtrEnd) <3  && dIPStart<dCtrEnd){
                        endDate = dCtrEnd;
                    }else{
                        endDate = dCYq1E;
                    }
                }
                varGapMonths = 3;
              }     
                    
                    Integer endDays = Date.daysInMonth(endDate.year(), endDate.Month());
                    endDate =  date.newInstance(endDate.year(),  endDate.Month(), endDays);      
                    Date endDateY1;
                    System.debug('**endDateY1**'+endDateY1);
                for(integer i=1;i<= LengthofYears * integer.valueof(selectopt);i++){
                   if(i==1){    
                                  objPeriod = new Period__c(Implementation_Period__c = objIP.Id,Type__c = 'Reporting',Performance_Framework__c = PFId ,
                                              Start_Date__c = objIP.Start_Date__c,End_Date__c =endDate ,Period_Number__c = i,PU__c = true,PU_Due_Date__c=endDate.addDays(42));
                            
                            }    
                    if(i>1 && i!= LengthofYears * integer.valueof(selectopt)){
                        System.debug('**endDateY1**'+endDateY1);
                    Date startDateSecond = endDateY1.addDays(1); 
                    endDateY1 = endDateY1.addMonths(varGapMonths);
                    Integer endDaysSecond = Date.daysInMonth(endDateY1.year(), endDateY1.Month());
                    endDateY1 =  date.newInstance(endDateY1.year(),  endDateY1.Month(), endDaysSecond);
                    if(endDateY1 >= objIP.End_Date__c ){
                    endDateY1 = objIP.End_Date__c;
                    objPeriod = new Period__c(Implementation_Period__c = objIP.Id,Type__c = 'Reporting',Performance_Framework__c = PFId ,
                                Start_Date__c = startDateSecond ,End_Date__c = endDateY1,Period_Number__c = i,PU__c =true,PU_Due_Date__c=objIP.End_Date__c.addDays(42));
                    lstPeriodToInsert.add(objPeriod);       
                    break;
                    }
                     objPeriod = new Period__c(Implementation_Period__c = objIP.Id,Type__c = 'Reporting',Performance_Framework__c = PFId ,
                                Start_Date__c = startDateSecond ,End_Date__c = endDateY1,Period_Number__c = i,PU__c =true,PU_Due_Date__c=endDateY1.addDays(42));
                                //endDateY2 = endDateY1.addYears(i-2);
                    }
                    
                    
                    if(i == LengthofYears * integer.valueof(selectopt)){
                     objPeriod = new Period__c(Implementation_Period__c = objIP.Id,Type__c = 'Reporting',Performance_Framework__c = PFId ,
                                Start_Date__c = endDateY1.addDays(1),End_Date__c =objIP.End_Date__c ,Period_Number__c = i,PU__c =true,PU_Due_Date__c=objIP.End_Date__c.addDays(42));
                     
                    }
                    system.debug('**selectopt**'+selectopt);
                    objIP.Length_Years__c = String.valueof(LengthofYears);
                    
                    lstPeriodToInsert.add(objPeriod);
                   endDateY1 =objPeriod.End_Date__c ;
                  
                }
                    
        }else{
        Implementation_Period__c ipRec = Trigger.newMap.get(objIP.Id); 
        ipRec.addError('Kindly Select PR Fiscal Cycle');
        }
       }else{
        Implementation_Period__c ipRec = Trigger.newMap.get(objIP.Id); 
        ipRec.addError('Kindly Enter Implementation Start and End Dates');
        }
        
     }  
     
        if(lstPeriodToInsert.size() > 0){ 
            //blnRec = true;
            insert lstPeriodToInsert;
            update lstImplementationPeriod;
            
            }
    
            
        if(!Test.isRunningTest()){
            if(lstRP.size()>0){
                system.debug('**inside IF of period size greater than 0'+lstRP);
                delete lstRP;
            }
        }
        
             
        lstRP = new List<Period__c>();
          
        lstRP = [Select Id, Period_Number__c,Start_Date__c,End_Date__c,Performance_Framework__c ,Due_Date__c,PU__c,DR__c,EFR__c,Period_Length__c,Document__c, Audit_Report__c,AR_Due_Date__c,EFR_Due_Date__c,PU_Due_Date__c,Frequency__c  From Period__c Where Implementation_Period__c =: impid And Type__c = 'Reporting' And Flow_to_GrantIndicator__c =false Order by Period_Number__c];
      
      }  
    
    }*/
//    StopRecursion.setalreadyRunRPAfterUpdateIP();
 // }  
}