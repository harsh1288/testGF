trigger SendMailToPRs on IP_Detail_Information__c (after update) {

/*********************************************************************************
* Trigger: SendMailToPRs 
  Test class:   TestSendMailToPRs 
*  DateCreated : 30/10/2014
----------------------------------------------------------------------------------
* Purpose/Methods:
* - To add PR Admin and PR Read Write Edit Contacts to send Email
----------------------------------------------------------------------------------
* History:
* - VERSION  DEVELOPER NAME    DATE            DETAIL FEATURES
     1.0        TCS           28/10/2014      INITIAL DEVELOPMENT
*********************************************************************************/
Set<Id> accIds = new Set<Id>();
Set<Id> contactIds = new Set<Id>();
Set<Id> ipIds = new Set<Id>();
Set<Id> ipDetailIds = new Set<Id>();

//Set<Id>
List<IP_Detail_Information__c> lstIPDetail = new List<IP_Detail_Information__c>(); 
List<Account> lstAccount = new List<Account>();
List<Implementation_Period__c> lstIP = new List<Implementation_Period__c>();
List<String> lstEMailAdd = new List<String>(); 
List<Id> lstConIds = new List<Id>(); 
List<User> lstPRUsers = new List<User>();
for(IP_Detail_Information__c ipDetail: Trigger.new)
{
    /*if(Trigger.isInsert){
        ipDetailIds.add(ipDetail.Id);
    }*/
    system.debug('DB Status '+ipDetail.Budget_Status__c+' Trigger.oldMap.get(ipDetail.Id).RecordType '+System.Trigger.oldMap.get(ipDetail.Id).Budget_Status__c);
    if(Trigger.isUpdate && (ipDetail.Budget_Status__c != System.Trigger.oldMap.get(ipDetail.Id).Budget_Status__c && (ipDetail.Budget_Status__c == Label.IP_Return_back_to_PR || ipDetail.Budget_Status__c == Label.IP_Sub_to_GF || ipDetail.Budget_Status__c == Label.IP_Accepted)))
    {

    ipDetailIds.add(ipDetail.Id);
}
}
/*Query for Implementation Period of the IP Detail Information*/
if(ipDetailIds!=null)
{
    lstIPDetail = [SELECT Id, Budget_Status__c, Implementation_Period__c, Implementation_Period__r.Principal_Recipient__c FROM IP_Detail_Information__c WHERE Id=:ipDetailIds];

for(IP_Detail_Information__c ipDInfo: lstIPDetail)
{
    if(ipDInfo.Implementation_Period__c!=null)  
    ipIds.add(ipDInfo.Implementation_Period__c);

    if(ipDInfo.Implementation_Period__r.Principal_Recipient__c!=null)
    accIds.add(ipDInfo.Implementation_Period__r.Principal_Recipient__c);
}

        /*Query for all Contacts associated with the Principal Recipients*/
if(accIds!=null){

/*Query for all Contacts associated with the Principal Recipients*/
    List<Account> lstAccounts = [SELECT Id, (SELECT Id from Contacts) FROM Account WHERE Id=:accIds];
    List<npe5__Affiliation__c> lstAff = [Select id,npe5__Contact__c,npe5__Organization__c FROM npe5__Affiliation__c where npe5__Organization__c IN:lstAccounts and Access_Level__c !='Read' and npe5__Status__c='Current'];
    for(npe5__Affiliation__c aff : lstAff ){
            //contactIds.add();
            lstConIds.add(aff.npe5__Contact__c);
    }


/*List<Account> lstAccounts = [SELECT Id, (SELECT Id from Contacts) FROM Account WHERE Id=:accIds];
for(Account acc:lstAccounts)
{
    for(Contact con: acc.Contacts)
    {
    contactIds.add(con.Id);
    lstConIds.add(con.Id);
    }
}*/
}

/*Query for Email of Users where Profile name is PR Admin or PR Read Write Edit*/
   if(lstConIds.size()>0){
       lstPRUsers = [SELECT ID, Email, ContactId, Contact.Firstname, Contact.Lastname, ProfileId, Profile.Name FROM User WHERE ContactID in: lstConIds and Profile.Name in ('PR Read Write Edit', 'PR Admin') and isActive=: True];
        // Remove PR Read Only

        for(User objUser: lstPRUsers)
            {
            if(objUser.Email!=null)
            lstEMailAdd.add(objUser.Email);
            System.debug('lstEMailAdd'+lstEMailAdd);
            }
        System.debug('lstEMailAdd'+lstEMailAdd);
  }  
  
 if(lstPRUsers.size()>0)
 {
     String[] toAddresses = new String[] {};
    for(String email: lstEMailAdd)
    { toAddresses.add(email);}
    system.debug('Address '+toAddresses );
     Messaging.SingleEmailMessage mailToPRs = new Messaging.SingleEmailMessage();
      mailToPRs.setTargetObjectId(lstPRUsers[0].ContactId);
      //contactIds '005g0000001Io3u'//(Label.Admin_Id)
      mailToPRs.setToAddresses(toAddresses);
      //mailToPRs.setSenderDisplayName('GMP');
      mailToPRs.setUseSignature(false);
      mailToPRs.setBccSender(false);
      mailToPRs.setSaveAsActivity(false);
      mailToPRs.setOrgWideEmailAddressId(Label.OrgWideAddrss_GMP);
      EmailTemplate etPR= [Select id from EmailTemplate where DeveloperName=:'Detailed_Budget_Request_input_from_PR'];
     // EmailTemplate etCT=[Select id from EmailTemplate where DeveloperName=:'CT_agrees_with_final_Budget'];
      EmailTemplate etPRNotify=[Select id from EmailTemplate where DeveloperName=:'Detailed_Budget_PR_Notification_on_Sub'];
      EmailTemplate etPRNotifyAccpted=[Select id from EmailTemplate where DeveloperName=:'DB_Accepted_Mail_To_PR_CT_User'];
     
    
    for(IP_Detail_Information__c ipDet: lstIPDetail)
  {
    mailToPRs.setWhatId(ipDet.Id);
   /** Commenting for new Requirements of Sprint 12 **/
   /* if(!StopRecursion.hasAlreadyRunAfterUpdateDB())
      {
    if(ipDet.Budget_Status__c == Label.IP_Return_back_to_PR) {
    system.debug('Status Is '+Label.IP_Return_back_to_PR);
          mailToPRs.setTemplateId(etPR.id);
          Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mailToPRs});   
      }
       StopRecursion.setAlreadyRunAfterUpdateDB();
  }*/
  /*  if(ipDet.Budget_Status__c == 'CT agrees with final budget') {
          mailToPRs.setTemplateId(etCT.id);
          Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mailToPRs});   
      } */ 
      /** Commenting for new Requirements of Sprint 12 **/
      /* if(!StopRecursion.hasAlreadyRunAfterUpdateDBSubToGF())
         {
           if(ipDet.Budget_Status__c == Label.IP_Sub_to_GF) {
            system.debug('Status Is '+Label.IP_Sub_to_GF);
              mailToPRs.setTemplateId(etPRNotify.id);
              Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mailToPRs});   
          }
          StopRecursion.setAlreadyRunAfterUpdateDBSubToGF();
         }*/
      if(!StopRecursion.hasAlreadyRunAfterUpdateDBAccepted())
      {
          if(ipDet.Budget_Status__c == Label.IP_Accepted) {
           system.debug('Status Is '+Label.IP_Accepted);
              mailToPRs.setTemplateId(etPRNotifyAccpted.id);
              Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mailToPRs});   
          } 
      StopRecursion.setAlreadyRunAfterUpdateDBAccepted();
      }
   }
 }
}

}