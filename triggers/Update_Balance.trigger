/* 
 * On update to the Burn Report item, ensure all subsequent linked burn reports have the 
 * same Budget Balance Hours. 
 * 
 * Note that this trigger will cascade through the dependant burn reports
 */ 
trigger Update_Balance on Burn_Report__c (after update) {

/*  // Retrieve all burn reports linked to the updated items
    Set<ID> ids = Trigger.newMap.keySet();
    List<Burn_Report__c> brs = [SELECT Id, Balance_Budget_Hrs__c, Previous_Balance_Budget_Hrs__c FROM Burn_Report__c WHERE Previous_Burn_Report__c in :ids];    

    // update all subsequent burn reports to match the new balance
    for (Burn_Report__c br : brs) {
        
        br.Balance_Budget_Hrs__c = br.Previous_Balance_Budget_Hrs__c;
        update br;

    }
*/
    if (Update_Burn_Balance.flag == true){
        Update_Burn_Balance.flag = false; 
        Update_Burn_Balance.Update_Balance(); 
        Update_Burn_Balance.flag = true; 
    }
}