/*
* $Author:      $ TCS Developer
* $Description: $ Trigger for Goals and Objectives object
* $Date:        $ 07-Nov-2014
* $Revision:    $ 
*/

trigger GoalsObjectivesTrigger on Goals_Objectives__c (before delete, after insert,before update, after update,after delete,before insert)
{    
        if(Trigger.isBefore){
            if (Trigger.isDelete){
                List<sObject> sObjectList = new List<sObject>();
                for( Goals_Objectives__c rec : Trigger.old)
                    sObjectList.add((sObject)rec );
                //AssigningNumbertoGoalhandler.checkIfIndicatorLinked(trigger.old);    
                PFChangesTrackerHandler.CheckPFField(sObjectList);
            }
       
            if ( Trigger.isInsert ){  
                List<sObject> sObjectList = new List<sObject>();
                for( Goals_Objectives__c rec : Trigger.new ){
                    sObjectList.add((sObject)rec );
                } 
                List<Goals_Objectives__c> lstgoalobj = new List<Goals_Objectives__c>();
                for( Goals_Objectives__c rec : Trigger.new ){
                    lstgoalobj.add(rec );
                }  
                PFChangesTrackerHandler.CheckPFField(sObjectList);
                AssigningNumbertoGoalhandler.AssigningNumberinsert(lstgoalobj);
            }
            else if ( Trigger.isUpdate )
            {
                List<sObject> sObjectList = new List<sObject>();
                for( Goals_Objectives__c rec : Trigger.new ){
                  if(Trigger.oldMap.get(rec.Id).RecordTypeId == rec.RecordTypeId)
                    sObjectList.add((sObject)rec );
                 }   
                PFChangesTrackerHandler.CheckPFField(sObjectList);
       
            }
            
        }
        if(Trigger.isAfter && Trigger.isDelete){
                List<Goals_Objectives__c> lstgoalobj = new List<Goals_Objectives__c>();
                for( Goals_Objectives__c rec : Trigger.old){
                    lstgoalobj.add(rec );
                }
            AssigningNumbertoGoalhandler.AssigningNumberdelete(lstgoalobj);
        }
}