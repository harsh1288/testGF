trigger createOnlyYearly on Implementation_Period__c (after Update) {
/*Public String selectopt;
Public String NewOption;
Integer LengthofYears;
public Boolean blnRecOn = false;
public List<Period__c> lstRP {get;set;}

Set<Id> ImpId = new Set<Id>();
    for (Implementation_Period__c objIP : Trigger.new) {
              if(Trigger.oldMap.get(objIP.id).Status__c!= objIP.Status__c && objIP.Status__c == 'Grant-Making'){
                        ImpId.add(objIP.Id);
                        NewOption = Trigger.newMap.get(objIP.Id).Reporting__c;
                        //blnRecOn = true;
                        system.debug('**NewOption'+NewOption);
              }
      }

    if(ImpId !=null && ImpId.size()>0){
    
    lstRP = [Select Id, Period_Number__c,Start_Date__c,End_Date__c,Due_Date__c,Document__c,PU__c,DR__c,EFR__c,Period_Length__c,Audit_Report__c,AR_Due_Date__c,EFR_Due_Date__c,PU_Due_Date__c,Frequency__c,Performance_Framework__c From Period__c Where Implementation_Period__c =: impid And Type__c = 'Reporting' And Flow_to_GrantIndicator__c = true Order by Period_Number__c];  
      
     List<Implementation_Period__c> lstImplementationPeriod = [Select Id,PR_Fiscal_Cycle__c,Concept_Note__r.Implementation_Period_Page_Template__c,
                                        Start_Date__c,End_Date__c,Length_Years__c,Principal_Recipient__r.Country__r.Country_Fiscal_Cycle_End_Date__c,Principal_Recipient__r.Country__r.Country_Fiscal_Cycle_Start_Date__c,Reporting__c
                                        From Implementation_Period__c Where Id =: impid];
        system.debug('**impid'+impid);
        
    List<Period__c> lstPeriodToInsert = new List<Period__c>();
        List<Result__c> lstResulttoinsert = new List<Result__c>();
        Period__c objPeriod;
        
        for(Implementation_Period__c objIP : lstImplementationPeriod){
        if(objIP.Length_Years__c != null && objIP.Start_Date__c != null && objIP.End_Date__c != null ){
             
             if( objIP.Principal_Recipient__r.Country__r.Country_Fiscal_Cycle_Start_Date__c !=null && objIP.Principal_Recipient__r.Country__r.Country_Fiscal_Cycle_End_Date__c !=null){
             //Date dCtrStart = objIP.Principal_Recipient__r.Country__r.Country_Fiscal_Cycle_Start_Date__c;
             //Date dCtrEnd = objIP.Principal_Recipient__r.Country__r.Country_Fiscal_Cycle_End_Date__c;
              String [] FiscalMonths;
             if(objIP.PR_Fiscal_Cycle__c.contains('-')){
             FiscalMonths = objIP.PR_Fiscal_Cycle__c.split('-');
             }
             system.debug('**FiscalMonths'+FiscalMonths);
             String FiscalStartMonth = FiscalMonths[0];
             String FiscalEndMonth = FiscalMonths[1];
             
             Date dIPStart = objIP.Start_Date__c;
             Date dIPEnd = objIP.End_Date__c;
             Date endDate;
             LengthofYears = 6;
             Map<String,Integer> MonthNumberMap = new Map<String,Integer>{'January'=>1,'Febuary'=> 2,'March' => 3, 'April'=> 4,'May'=>5,'June' => 6,'July'=> 7,'August'=> 8,'September' => 9,'October'=>10,'November'=>11,'December'=>12};
             
             Integer Year = dIPStart.Year();
             Integer day = dIPStart.day();
             Integer Month = MonthNumberMap.get(FiscalStartMonth );
             
             
             Date newDate =  date.newInstance(Year, Month, day);
             Date newDateEnd =  newDate.addMonths(11);
             
             Integer YearEnd = dIPStart.Year();
             Integer dayEnd = dIPStart.day();
             Integer MonthEnd = MonthNumberMap.get(FiscalEndMonth);
             
             Date dCtrEnd = date.newInstance(YearEnd, MonthEnd, dayEnd);
             
             //Date dCtrEnd = newDateEnd ;// dIPStart.addMonths(11);
              
             System.debug('**newDate**'+newDate);
             System.debug('**newDateEnd**'+newDateEnd);
             if(NewOption=='Yearly'){
             selectopt = '1';
             system.debug('@@@@@@@@@');
             }else
             if(NewOption=='Half-yearly'){
             selectopt = '2';
             }else if(NewOption == 'Quarterly' ){
             selectopt = '4';
             system.debug('@@######');
             }
             else{
               selectopt = '0';
             }
             
             Integer varGapMonths;
                Date dCYFiscalStart = newDate ;
                Date dCYFiscalEnd = newDateEnd ;
                endDate = dCYFiscalEnd;
                if(dIPStart.monthsBetween(newDateEnd) > 12){
                //dIPStart<dCtrEnd && dIPStart.Year()==dCtrEnd.Year()){
                endDate = dCtrEnd;
                }
                
                System.debug('**endDate**'+endDate); 
                varGapMonths = 12;
                    Integer endDays = Date.daysInMonth(endDate.year(), endDate.Month());
                    endDate =  date.newInstance(endDate.year(),  endDate.Month(), endDays);      
                    Date endDateY1;
                for(integer i=1;i<=LengthofYears * integer.valueof(selectopt);i++){
                   if(i==1){    
                                  objPeriod = new Period__c(Implementation_Period__c = objIP.Id,Type__c = 'Reporting',
                                Start_Date__c = objIP.Start_Date__c,End_Date__c =endDate,Flow_to_GrantIndicator__c =true ); 
                            
                            }    
                    if(i>1 && i!= integer.valueof(objIP.Length_Years__c) * integer.valueof(selectopt)){
                    System.debug('**endDateY1**'+endDateY1);
                    Date startDateSecond = endDateY1.addDays(1); 
                    endDateY1 = endDateY1.addMonths(varGapMonths);
                    Integer endDaysSecond = Date.daysInMonth(endDateY1.year(), endDateY1.Month());
                    endDateY1 =  date.newInstance(endDateY1.year(),  endDateY1.Month(), endDaysSecond);
                    if(endDateY1 >= objIP.End_Date__c ){
                    endDateY1 = objIP.End_Date__c;
                    objPeriod = new Period__c(Implementation_Period__c = objIP.Id,Type__c = 'Reporting',
                                Start_Date__c = startDateSecond ,End_Date__c = endDateY1,Flow_to_GrantIndicator__c =true);
                    lstPeriodToInsert.add(objPeriod);       
                    break;
                    }
                     objPeriod = new Period__c(Implementation_Period__c = objIP.Id,Type__c = 'Reporting',
                                Start_Date__c = startDateSecond ,End_Date__c = endDateY1,Flow_to_GrantIndicator__c =true);
                                //endDateY2 = endDateY1.addYears(i-2);
                    }
                    
                    
                    if(i == integer.valueof(objIP.Length_Years__c) * integer.valueof(selectopt)){
                     objPeriod = new Period__c(Implementation_Period__c = objIP.Id,Type__c = 'Reporting',
                                Start_Date__c = endDateY1.addDays(1),End_Date__c =objIP.End_Date__c ,Flow_to_GrantIndicator__c =true);
                     
                    }
                    
                   // objIP.Is_Value_Changed__c = false;
                   // objIP.GenerateReportingPeriod__c = false;
                    lstPeriodToInsert.add(objPeriod);
                   endDateY1 =objPeriod.End_Date__c ;
                 }
                   if(lstPeriodToInsert.size() > 0){ 
                    insert lstPeriodToInsert;
                    //update lstImplementationPeriod;
                    //blnRecOn = true;    
                  }
                  
                   if(!Test.isRunningTest()){
                        if(lstRP.size()>0){
                        system.debug('**inside IF of period size greater than 0'+lstRP);
                        delete lstRP;
                      }
                  }   
           }
        }
    }
   }*/
  }