trigger trgBeforeInsertCN on Concept_Note__c (before insert) {

 List<Funding_Opportunity__c> lstFundOpps = [Select Id, Name, isCurrent__c from Funding_Opportunity__c where isCurrent__c = true];
 
 if(!lstFundOpps.isEmpty()){
   for(Concept_Note__c objCN: Trigger.New){
    if(objCN.Funding_Opportunity__c == null){
    objCN.Funding_Opportunity__c = lstFundOpps[0].Id; }
    }
  }
}